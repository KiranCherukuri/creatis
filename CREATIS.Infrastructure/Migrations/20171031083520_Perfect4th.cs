﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class Perfect4th : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InfoColumns_Customers_CustomerRefId",
                table: "InfoColumns");

            migrationBuilder.DropForeignKey(
                name: "FK_InfoColumns_InfoColumnMaster_ICM_Column_Ref_No",
                table: "InfoColumns");

            migrationBuilder.AlterColumn<int>(
                name: "IS_Active",
                table: "InfoColumns",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "IC_Column_Name",
                table: "InfoColumns",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ICM_Column_Ref_No",
                table: "InfoColumns",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CustomerRefId",
                table: "InfoColumns",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOn",
                table: "InfoColumns",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "InfoColumns",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "LanguageMaster",
                columns: table => new
                {
                    LanguageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    LanguageDescription = table.Column<int>(maxLength: 50, nullable: false),
                    Languagevalue = table.Column<int>(maxLength: 30, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LanguageMaster", x => x.LanguageId);
                });

            migrationBuilder.CreateTable(
                name: "OBCTypeMaster",
                columns: table => new
                {
                    OBCTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    OBCTypeDescription = table.Column<int>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OBCTypeMaster", x => x.OBCTypeId);
                });

            migrationBuilder.CreateTable(
                name: "UsedLanguages",
                columns: table => new
                {
                    UsedLanguagesId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    IsDefault = table.Column<int>(nullable: false),
                    LanguageRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsedLanguages", x => x.UsedLanguagesId);
                    table.ForeignKey(
                        name: "FK_UsedLanguages_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UsedLanguages_LanguageMaster_LanguageRefId",
                        column: x => x.LanguageRefId,
                        principalTable: "LanguageMaster",
                        principalColumn: "LanguageId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UsedLanguages_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                    //onDelete: ReferentialAction.Cascade
                });

            migrationBuilder.CreateTable(
                name: "OBCType",
                columns: table => new
                {
                    OBCTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    OBCTypeRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OBCType", x => x.OBCTypeId);
                    table.ForeignKey(
                        name: "FK_OBCType_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OBCType_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                    //onDelete: ReferentialAction.Cascade
                    table.ForeignKey(
                        name: "FK_OBCType_OBCTypeMaster_OBCTypeRefId",
                        column: x => x.OBCTypeRefId,
                        principalTable: "OBCTypeMaster",
                        principalColumn: "OBCTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OBCType_CustomerRefId",
                table: "OBCType",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_OBCType_LayoutRefId",
                table: "OBCType",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_OBCType_OBCTypeRefId",
                table: "OBCType",
                column: "OBCTypeRefId");

            migrationBuilder.CreateIndex(
                name: "IX_UsedLanguages_CustomerRefId",
                table: "UsedLanguages",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_UsedLanguages_LanguageRefId",
                table: "UsedLanguages",
                column: "LanguageRefId");

            migrationBuilder.CreateIndex(
                name: "IX_UsedLanguages_LayoutRefId",
                table: "UsedLanguages",
                column: "LayoutRefId");

            migrationBuilder.AddForeignKey(
                name: "FK_InfoColumns_Customers_CustomerRefId",
                table: "InfoColumns",
                column: "CustomerRefId",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InfoColumns_InfoColumnMaster_ICM_Column_Ref_No",
                table: "InfoColumns",
                column: "ICM_Column_Ref_No",
                principalTable: "InfoColumnMaster",
                principalColumn: "ICM_Column_No",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InfoColumns_Customers_CustomerRefId",
                table: "InfoColumns");

            migrationBuilder.DropForeignKey(
                name: "FK_InfoColumns_InfoColumnMaster_ICM_Column_Ref_No",
                table: "InfoColumns");

            migrationBuilder.DropTable(
                name: "OBCType");

            migrationBuilder.DropTable(
                name: "UsedLanguages");

            migrationBuilder.DropTable(
                name: "OBCTypeMaster");

            migrationBuilder.DropTable(
                name: "LanguageMaster");

            migrationBuilder.AlterColumn<int>(
                name: "IS_Active",
                table: "InfoColumns",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "IC_Column_Name",
                table: "InfoColumns",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "ICM_Column_Ref_No",
                table: "InfoColumns",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CustomerRefId",
                table: "InfoColumns",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOn",
                table: "InfoColumns",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "InfoColumns",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AddForeignKey(
                name: "FK_InfoColumns_Customers_CustomerRefId",
                table: "InfoColumns",
                column: "CustomerRefId",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfoColumns_InfoColumnMaster_ICM_Column_Ref_No",
                table: "InfoColumns",
                column: "ICM_Column_Ref_No",
                principalTable: "InfoColumnMaster",
                principalColumn: "ICM_Column_No",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

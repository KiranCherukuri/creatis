﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class lanAllowance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AllowancesRefId",
                table: "LangAllowances",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_LangAllowances_AllowancesRefId",
                table: "LangAllowances",
                column: "AllowancesRefId");

            migrationBuilder.AddForeignKey(
                name: "FK_LangAllowances_Allowances_AllowancesRefId",
                table: "LangAllowances",
                column: "AllowancesRefId",
                principalTable: "Allowances",
                principalColumn: "AllowancesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LangAllowances_Allowances_AllowancesRefId",
                table: "LangAllowances");

            migrationBuilder.DropIndex(
                name: "IX_LangAllowances_AllowancesRefId",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "AllowancesRefId",
                table: "LangAllowances");
        }
    }
}

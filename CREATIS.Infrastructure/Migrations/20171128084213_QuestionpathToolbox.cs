﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class QuestionpathToolbox : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MainToolBox",
                columns: table => new
                {
                    ToolboxId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ToolboxDescription = table.Column<string>(maxLength: 50, nullable: false),
                    ToolboxName = table.Column<string>(maxLength: 50, nullable: false),
                    ToolboxStatus = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainToolBox", x => x.ToolboxId);
                });

            migrationBuilder.CreateTable(
                name: "ToolBoxIcons",
                columns: table => new
                {
                    ToolboxIconId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ToolboxIconPath = table.Column<string>(maxLength: 500, nullable: false),
                    ToolboxIconStatus = table.Column<int>(nullable: false),
                    ToolboxRefId = table.Column<int>(nullable: false),
                    ToolboxType = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToolBoxIcons", x => x.ToolboxIconId);
                });

            migrationBuilder.CreateTable(
                name: "ToolBoxSubItem",
                columns: table => new
                {
                    SubToolboxId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    SubToolboxDescription = table.Column<string>(maxLength: 50, nullable: false),
                    SubToolboxName = table.Column<string>(maxLength: 50, nullable: false),
                    SubToolboxStatus = table.Column<int>(nullable: false),
                    ToolboxRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToolBoxSubItem", x => x.SubToolboxId);
                    table.ForeignKey(
                        name: "FK_ToolBoxSubItem_MainToolBox_ToolboxRefId",
                        column: x => x.ToolboxRefId,
                        principalTable: "MainToolBox",
                        principalColumn: "ToolboxId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ToolBoxSubItem_ToolboxRefId",
                table: "ToolBoxSubItem",
                column: "ToolboxRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ToolBoxIcons");

            migrationBuilder.DropTable(
                name: "ToolBoxSubItem");

            migrationBuilder.DropTable(
                name: "MainToolBox");
        }
    }
}

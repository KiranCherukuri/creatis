﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class lanAct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LangInfoColumns_Customers_CustomerRefId",
                table: "LangInfoColumns");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerRefId",
                table: "LangInfoColumns",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "ActivitiesRefId",
                table: "LangActReg",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_LangActReg_ActivitiesRefId",
                table: "LangActReg",
                column: "ActivitiesRefId");

            migrationBuilder.AddForeignKey(
                name: "FK_LangActReg_ActivitiesRegistration_ActivitiesRefId",
                table: "LangActReg",
                column: "ActivitiesRefId",
                principalTable: "ActivitiesRegistration",
                principalColumn: "ActivitiesRegistrationId");

            migrationBuilder.AddForeignKey(
                name: "FK_LangInfoColumns_Customers_CustomerRefId",
                table: "LangInfoColumns",
                column: "CustomerRefId",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LangActReg_ActivitiesRegistration_ActivitiesRefId",
                table: "LangActReg");

            migrationBuilder.DropForeignKey(
                name: "FK_LangInfoColumns_Customers_CustomerRefId",
                table: "LangInfoColumns");

            migrationBuilder.DropIndex(
                name: "IX_LangActReg_ActivitiesRefId",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "ActivitiesRefId",
                table: "LangActReg");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerRefId",
                table: "LangInfoColumns",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_LangInfoColumns_Customers_CustomerRefId",
                table: "LangInfoColumns",
                column: "CustomerRefId",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

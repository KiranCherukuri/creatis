﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class Propertiestable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CheckoffProperties",
                columns: table => new
                {
                    CheckoffId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Planninglevel = table.Column<string>(maxLength: 50, nullable: false),
                    QuestionRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckoffProperties", x => x.CheckoffId);
                    table.ForeignKey(
                        name: "FK_CheckoffProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CheckoffProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                    table.ForeignKey(
                        name: "FK_CheckoffProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId");
                });

            migrationBuilder.CreateTable(
                name: "ChioceProperties",
                columns: table => new
                {
                    ChioceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    FeedbackType = table.Column<string>(maxLength: 100, nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Planningfeedback = table.Column<string>(maxLength: 100, nullable: false),
                    Propose = table.Column<int>(nullable: false),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Recursiveloop = table.Column<int>(nullable: false),
                    Save = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChioceProperties", x => x.ChioceId);
                    table.ForeignKey(
                        name: "FK_ChioceProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChioceProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                    table.ForeignKey(
                        name: "FK_ChioceProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId");
                });

            migrationBuilder.CreateTable(
                name: "DocumetScanProperties",
                columns: table => new
                {
                    DocumetScanId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    PlanningFeedbackLevel = table.Column<string>(maxLength: 50, nullable: true),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumetScanProperties", x => x.DocumetScanId);
                    table.ForeignKey(
                        name: "FK_DocumetScanProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumetScanProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                    table.ForeignKey(
                        name: "FK_DocumetScanProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId");
                });

            migrationBuilder.CreateTable(
                name: "InfomessageProperties",
                columns: table => new
                {
                    InfomessageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Fontstyle = table.Column<string>(maxLength: 50, nullable: true),
                    Icontype = table.Column<string>(maxLength: 50, nullable: true),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Messagecontent = table.Column<string>(maxLength: 50, nullable: true),
                    Messagetype = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Titlestyle = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfomessageProperties", x => x.InfomessageId);
                    table.ForeignKey(
                        name: "FK_InfomessageProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InfomessageProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                    table.ForeignKey(
                        name: "FK_InfomessageProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId");
                });

            migrationBuilder.CreateTable(
                name: "PlanningselectProperties",
                columns: table => new
                {
                    PlanningselectId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    Chooseactivity = table.Column<string>(maxLength: 100, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Fixedvalue = table.Column<string>(maxLength: 100, nullable: true),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Other = table.Column<string>(maxLength: 100, nullable: false),
                    OtherTitle = table.Column<string>(maxLength: 100, nullable: true),
                    Planninglevel = table.Column<string>(maxLength: 100, nullable: false),
                    Planningreadout = table.Column<string>(maxLength: 100, nullable: true),
                    Planningspecific = table.Column<string>(maxLength: 100, nullable: false),
                    QP = table.Column<string>(maxLength: 100, nullable: true),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Startselecteditem = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningselectProperties", x => x.PlanningselectId);
                    table.ForeignKey(
                        name: "FK_PlanningselectProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanningselectProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                    table.ForeignKey(
                        name: "FK_PlanningselectProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId");
                });

            migrationBuilder.CreateTable(
                name: "TextMessageProperties",
                columns: table => new
                {
                    TextMessageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Messagecontent = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    QuestionRefId = table.Column<int>(nullable: false),
                    SendMethod = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TextMessageProperties", x => x.TextMessageId);
                    table.ForeignKey(
                        name: "FK_TextMessageProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TextMessageProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                    table.ForeignKey(
                        name: "FK_TextMessageProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId");
                });

            migrationBuilder.CreateTable(
                name: "VariableListProperties",
                columns: table => new
                {
                    VariableListId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Other = table.Column<string>(maxLength: 50, nullable: true),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    VarListID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VariableListProperties", x => x.VariableListId);
                    table.ForeignKey(
                        name: "FK_VariableListProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VariableListProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                    table.ForeignKey(
                        name: "FK_VariableListProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId");
                });

            migrationBuilder.CreateTable(
                name: "AddChioceProperties",
                columns: table => new
                {
                    AddChioceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChioceRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Endrecursiveloop = table.Column<int>(nullable: false),
                    PlanningfeedbackCode = table.Column<string>(maxLength: 100, nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AddChioceProperties", x => x.AddChioceId);
                    table.ForeignKey(
                        name: "FK_AddChioceProperties_ChioceProperties_ChioceRefId",
                        column: x => x.ChioceRefId,
                        principalTable: "ChioceProperties",
                        principalColumn: "ChioceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AddChioceProperties_ChioceRefId",
                table: "AddChioceProperties",
                column: "ChioceRefId");

            migrationBuilder.CreateIndex(
                name: "IX_CheckoffProperties_ActivitiesRefId",
                table: "CheckoffProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_CheckoffProperties_LayoutRefId",
                table: "CheckoffProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_CheckoffProperties_QuestionRefId",
                table: "CheckoffProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ChioceProperties_ActivitiesRefId",
                table: "ChioceProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ChioceProperties_LayoutRefId",
                table: "ChioceProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ChioceProperties_QuestionRefId",
                table: "ChioceProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumetScanProperties_ActivitiesRefId",
                table: "DocumetScanProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumetScanProperties_LayoutRefId",
                table: "DocumetScanProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumetScanProperties_QuestionRefId",
                table: "DocumetScanProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_InfomessageProperties_ActivitiesRefId",
                table: "InfomessageProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_InfomessageProperties_LayoutRefId",
                table: "InfomessageProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_InfomessageProperties_QuestionRefId",
                table: "InfomessageProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningselectProperties_ActivitiesRefId",
                table: "PlanningselectProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningselectProperties_LayoutRefId",
                table: "PlanningselectProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningselectProperties_QuestionRefId",
                table: "PlanningselectProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_TextMessageProperties_ActivitiesRefId",
                table: "TextMessageProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_TextMessageProperties_LayoutRefId",
                table: "TextMessageProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_TextMessageProperties_QuestionRefId",
                table: "TextMessageProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_VariableListProperties_ActivitiesRefId",
                table: "VariableListProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_VariableListProperties_LayoutRefId",
                table: "VariableListProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_VariableListProperties_QuestionRefId",
                table: "VariableListProperties",
                column: "QuestionRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AddChioceProperties");

            migrationBuilder.DropTable(
                name: "CheckoffProperties");

            migrationBuilder.DropTable(
                name: "DocumetScanProperties");

            migrationBuilder.DropTable(
                name: "InfomessageProperties");

            migrationBuilder.DropTable(
                name: "PlanningselectProperties");

            migrationBuilder.DropTable(
                name: "TextMessageProperties");

            migrationBuilder.DropTable(
                name: "VariableListProperties");

            migrationBuilder.DropTable(
                name: "ChioceProperties");
        }
    }
}

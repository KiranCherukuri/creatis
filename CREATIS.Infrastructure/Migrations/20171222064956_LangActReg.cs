﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class LangActReg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Arabic",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Bulgarian",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Croatian",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Czech",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Danish",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Dutch",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "English",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Estonian",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Finnish",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "French",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "German",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Greek",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Hungarian",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Italian",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Latvian",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Lithuanian",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Macedonian",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Norwegian",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Polish",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Portuguese",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Romanian",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Russian",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Slovak",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Slovene",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Spanish",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Swedish",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Turkish",
                table: "LangActReg",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Ukrainian",
                table: "LangActReg",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Arabic",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Bulgarian",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Croatian",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Czech",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Danish",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Dutch",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "English",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Estonian",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Finnish",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "French",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "German",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Greek",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Hungarian",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Italian",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Latvian",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Lithuanian",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Macedonian",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Norwegian",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Polish",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Portuguese",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Romanian",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Russian",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Slovak",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Slovene",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Spanish",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Swedish",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Turkish",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "Ukrainian",
                table: "LangActReg");
        }
    }
}

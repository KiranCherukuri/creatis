﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CREATIS.Infrastructure;

namespace CREATIS.Infrastructure.Migrations
{
    [DbContext(typeof(Creatis_Context))]
    [Migration("20171101065609_OBCTypeMasterColumnType")]
    partial class OBCTypeMasterColumnType
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.CustomerNotes", b =>
                {
                    b.Property<int>("CN_ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CN_Details");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int>("CustomerRefId");

                    b.Property<int>("IS_Active");

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("CN_ID");

                    b.HasIndex("CustomerRefId");

                    b.ToTable("CustomerNotes");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.Customers", b =>
                {
                    b.Property<int>("CustomerId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Active");

                    b.Property<DateTime>("CreatedDateTime");

                    b.Property<string>("Environment")
                        .HasMaxLength(50);

                    b.Property<DateTime>("ModifiedDateTime");

                    b.Property<string>("Name")
                        .HasMaxLength(250);

                    b.Property<string>("Source")
                        .HasMaxLength(20);

                    b.Property<int>("SourceId");

                    b.HasKey("CustomerId");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.InfoColumns", b =>
                {
                    b.Property<int>("IC_ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedOn");

                    b.Property<int?>("CustomerRefId");

                    b.Property<int?>("ICM_Column_Ref_No");

                    b.Property<string>("IC_Column_Name")
                        .IsRequired();

                    b.Property<int?>("IS_Active");

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("IC_ID");

                    b.HasIndex("CustomerRefId");

                    b.HasIndex("ICM_Column_Ref_No");

                    b.ToTable("InfoColumns");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.InfoColumnsMaster", b =>
                {
                    b.Property<int>("ICM_Column_No")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ICM_Name")
                        .HasMaxLength(50);

                    b.Property<int>("ISM_Active");

                    b.HasKey("ICM_Column_No");

                    b.ToTable("InfoColumnMaster");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.Integrator", b =>
                {
                    b.Property<int>("I_ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int>("CustomerRefId");

                    b.Property<int>("IS_Active");

                    b.Property<string>("I_Details");

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("I_ID");

                    b.HasIndex("CustomerRefId");

                    b.ToTable("Integrator");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.LanguageMaster", b =>
                {
                    b.Property<int>("LanguageId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("LanguageId");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasColumnName("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedOn")
                        .HasColumnName("CreatedOn");

                    b.Property<int>("IS_Active")
                        .HasColumnName("IS_Active");

                    b.Property<string>("LanguageCode")
                        .HasColumnName("LanguageCode")
                        .HasMaxLength(30);

                    b.Property<string>("LanguageDescription")
                        .HasColumnName("LanguageDescription")
                        .HasMaxLength(50);

                    b.Property<string>("UpdatedBy")
                        .HasColumnName("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("UpdatedOn")
                        .HasColumnName("UpdatedOn");

                    b.HasKey("LanguageId");

                    b.ToTable("LanguageMaster");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.Layout", b =>
                {
                    b.Property<int>("LayoutId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int>("CustomerRefId");

                    b.Property<int>("IsLocked");

                    b.Property<string>("LayoutName")
                        .HasMaxLength(100);

                    b.Property<string>("LayoutOrigin")
                        .HasMaxLength(100);

                    b.Property<int>("LayoutStatus");

                    b.Property<string>("LayoutType")
                        .HasMaxLength(50);

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("LayoutId");

                    b.HasIndex("CustomerRefId");

                    b.ToTable("Layout");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.OBCType", b =>
                {
                    b.Property<int>("OBCTypeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("OBCTypeId");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasColumnName("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedOn")
                        .HasColumnName("CreatedOn");

                    b.Property<int>("CustomerRefId")
                        .HasColumnName("CustomerRefId");

                    b.Property<int>("LayoutRefId")
                        .HasColumnName("LayoutRefId");

                    b.Property<int>("OBCTypeRefId")
                        .HasColumnName("OBCTypeRefId");

                    b.Property<string>("UpdatedBy")
                        .HasColumnName("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("UpdatedOn")
                        .HasColumnName("UpdatedOn");

                    b.HasKey("OBCTypeId");

                    b.HasIndex("CustomerRefId");

                    b.HasIndex("LayoutRefId");

                    b.HasIndex("OBCTypeRefId");

                    b.ToTable("OBCType");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.OBCTypeMaster", b =>
                {
                    b.Property<int>("OBCTypeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("OBCTypeId");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasColumnName("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedOn")
                        .HasColumnName("CreatedOn");

                    b.Property<int>("IS_Active")
                        .HasColumnName("IS_Active");

                    b.Property<string>("OBCTypeDescription")
                        .HasColumnName("OBCTypeDescription")
                        .HasMaxLength(50);

                    b.Property<string>("UpdatedBy")
                        .HasColumnName("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("UpdatedOn")
                        .HasColumnName("UpdatedOn");

                    b.HasKey("OBCTypeId");

                    b.ToTable("OBCTypeMaster");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.RecentActivity", b =>
                {
                    b.Property<int>("ActivityId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int>("CustomerRefId");

                    b.Property<int>("LayoutRefId");

                    b.Property<int>("Status");

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime>("UpdatedOn");

                    b.Property<int>("UserId");

                    b.HasKey("ActivityId");

                    b.HasIndex("CustomerRefId");

                    b.HasIndex("LayoutRefId");

                    b.ToTable("RecentActivity");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.Roles", b =>
                {
                    b.Property<int>("RoleId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("RoleName");

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("UpdatedOn");

                    b.HasKey("RoleId");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.StatusMaster", b =>
                {
                    b.Property<int>("Status_ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Status_Description");

                    b.Property<int>("Status_IsActive");

                    b.HasKey("Status_ID");

                    b.ToTable("StatusMaster");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.UsedLanguages", b =>
                {
                    b.Property<int>("UsedLanguagesId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("UsedLanguagesId");

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasColumnName("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedOn")
                        .HasColumnName("CreatedOn");

                    b.Property<int>("CustomerRefId")
                        .HasColumnName("CustomerRefId");

                    b.Property<int>("IsDefault")
                        .HasColumnName("IsDefault");

                    b.Property<int>("LanguageRefId")
                        .HasColumnName("LanguageRefId");

                    b.Property<int>("LayoutRefId")
                        .HasColumnName("LayoutRefId");

                    b.Property<string>("UpdatedBy")
                        .HasColumnName("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("UpdatedOn")
                        .HasColumnName("UpdatedOn");

                    b.HasKey("UsedLanguagesId");

                    b.HasIndex("CustomerRefId");

                    b.HasIndex("LanguageRefId");

                    b.HasIndex("LayoutRefId");

                    b.ToTable("UsedLanguages");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.Users", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("DomainId")
                        .HasMaxLength(50);

                    b.Property<string>("EmailId")
                        .HasMaxLength(100);

                    b.Property<string>("FirstName")
                        .HasMaxLength(50);

                    b.Property<string>("LastName")
                        .HasMaxLength(50);

                    b.Property<string>("RoleId")
                        .HasMaxLength(100);

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("UpdatedOn");

                    b.Property<int>("UserStatus");

                    b.HasKey("UserId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.CustomerNotes", b =>
                {
                    b.HasOne("CREATIS.ApplicationCore.Modals.Customers", "Customers")
                        .WithMany("CustomersNote")
                        .HasForeignKey("CustomerRefId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.InfoColumns", b =>
                {
                    b.HasOne("CREATIS.ApplicationCore.Modals.Customers", "Customers")
                        .WithMany("InfoCol")
                        .HasForeignKey("CustomerRefId");

                    b.HasOne("CREATIS.ApplicationCore.Modals.InfoColumnsMaster", "InfoColumnsMaster")
                        .WithMany("InfoColum")
                        .HasForeignKey("ICM_Column_Ref_No");
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.Integrator", b =>
                {
                    b.HasOne("CREATIS.ApplicationCore.Modals.Customers", "Customers")
                        .WithMany("IntegratorDetails")
                        .HasForeignKey("CustomerRefId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.Layout", b =>
                {
                    b.HasOne("CREATIS.ApplicationCore.Modals.Customers", "Customers")
                        .WithMany("LayoutDetails")
                        .HasForeignKey("CustomerRefId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.OBCType", b =>
                {
                    b.HasOne("CREATIS.ApplicationCore.Modals.Customers", "Customers")
                        .WithMany("OBCTypes")
                        .HasForeignKey("CustomerRefId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CREATIS.ApplicationCore.Modals.Layout", "Layout")
                        .WithMany("OBCTypes")
                        .HasForeignKey("LayoutRefId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CREATIS.ApplicationCore.Modals.OBCTypeMaster", "OBCTypeMaster")
                        .WithMany("OBCTypes")
                        .HasForeignKey("OBCTypeRefId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.RecentActivity", b =>
                {
                    b.HasOne("CREATIS.ApplicationCore.Modals.Customers", "Customers")
                        .WithMany("RecentActivities")
                        .HasForeignKey("CustomerRefId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CREATIS.ApplicationCore.Modals.Layout", "Layout")
                        .WithMany("RecentActivities")
                        .HasForeignKey("LayoutRefId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CREATIS.ApplicationCore.Modals.UsedLanguages", b =>
                {
                    b.HasOne("CREATIS.ApplicationCore.Modals.Customers", "Customers")
                        .WithMany("UsedLanguages")
                        .HasForeignKey("CustomerRefId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CREATIS.ApplicationCore.Modals.LanguageMaster", "LanguageMaster")
                        .WithMany("UsedLanguages")
                        .HasForeignKey("LanguageRefId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CREATIS.ApplicationCore.Modals.Layout", "Layout")
                        .WithMany("UsedLanguages")
                        .HasForeignKey("LayoutRefId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}

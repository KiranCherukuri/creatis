﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class Activitiesdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActivitiesRegistration_InterruptibleBy_InterruptibleByRefId",
                table: "ActivitiesRegistration");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivitiesRegistration_PTO_PTORefId",
                table: "ActivitiesRegistration");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivitiesRegistration_PlanningOverview_PlanningOverviewRefId",
                table: "ActivitiesRegistration");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivitiesRegistration_Planning_PlanningRefId",
                table: "ActivitiesRegistration");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivitiesRegistration_Specific_SpecificRefId",
                table: "ActivitiesRegistration");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivitiesRegistration_WorkingCode_WorkingCodeRefId",
                table: "ActivitiesRegistration");

            migrationBuilder.DropIndex(
                name: "IX_ActivitiesRegistration_InterruptibleByRefId",
                table: "ActivitiesRegistration");

            migrationBuilder.DropIndex(
                name: "IX_ActivitiesRegistration_PTORefId",
                table: "ActivitiesRegistration");

            migrationBuilder.DropIndex(
                name: "IX_ActivitiesRegistration_PlanningOverviewRefId",
                table: "ActivitiesRegistration");

            migrationBuilder.DropIndex(
                name: "IX_ActivitiesRegistration_PlanningRefId",
                table: "ActivitiesRegistration");

            migrationBuilder.DropIndex(
                name: "IX_ActivitiesRegistration_SpecificRefId",
                table: "ActivitiesRegistration");

            migrationBuilder.DropIndex(
                name: "IX_ActivitiesRegistration_WorkingCodeRefId",
                table: "ActivitiesRegistration");

            migrationBuilder.AlterColumn<string>(
                name: "WorkingCodeRefId",
                table: "ActivitiesRegistration",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<bool>(
                name: "Visible",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<bool>(
                name: "TransportType",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "SpecificRefId",
                table: "ActivitiesRegistration",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "PlanningRefId",
                table: "ActivitiesRegistration",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "PlanningOverviewRefId",
                table: "ActivitiesRegistration",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "PTORefId",
                table: "ActivitiesRegistration",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "InterruptibleByRefId",
                table: "ActivitiesRegistration",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<bool>(
                name: "Flexactivity",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<bool>(
                name: "EmptyFullSolo",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<bool>(
                name: "ConfirmButton",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "WorkingCodeRefId",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<int>(
                name: "Visible",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<int>(
                name: "TransportType",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<int>(
                name: "SpecificRefId",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<int>(
                name: "PlanningRefId",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<int>(
                name: "PlanningOverviewRefId",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<int>(
                name: "PTORefId",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<int>(
                name: "InterruptibleByRefId",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<int>(
                name: "Flexactivity",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<int>(
                name: "EmptyFullSolo",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<int>(
                name: "ConfirmButton",
                table: "ActivitiesRegistration",
                nullable: false,
                oldClrType: typeof(bool));

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_InterruptibleByRefId",
                table: "ActivitiesRegistration",
                column: "InterruptibleByRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_PTORefId",
                table: "ActivitiesRegistration",
                column: "PTORefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_PlanningOverviewRefId",
                table: "ActivitiesRegistration",
                column: "PlanningOverviewRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_PlanningRefId",
                table: "ActivitiesRegistration",
                column: "PlanningRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_SpecificRefId",
                table: "ActivitiesRegistration",
                column: "SpecificRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_WorkingCodeRefId",
                table: "ActivitiesRegistration",
                column: "WorkingCodeRefId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActivitiesRegistration_InterruptibleBy_InterruptibleByRefId",
                table: "ActivitiesRegistration",
                column: "InterruptibleByRefId",
                principalTable: "InterruptibleBy",
                principalColumn: "InterruptibleById",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivitiesRegistration_PTO_PTORefId",
                table: "ActivitiesRegistration",
                column: "PTORefId",
                principalTable: "PTO",
                principalColumn: "PTOId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivitiesRegistration_PlanningOverview_PlanningOverviewRefId",
                table: "ActivitiesRegistration",
                column: "PlanningOverviewRefId",
                principalTable: "PlanningOverview",
                principalColumn: "PlanningOverviewId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivitiesRegistration_Planning_PlanningRefId",
                table: "ActivitiesRegistration",
                column: "PlanningRefId",
                principalTable: "Planning",
                principalColumn: "PlanningId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivitiesRegistration_Specific_SpecificRefId",
                table: "ActivitiesRegistration",
                column: "SpecificRefId",
                principalTable: "Specific",
                principalColumn: "SpecificId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivitiesRegistration_WorkingCode_WorkingCodeRefId",
                table: "ActivitiesRegistration",
                column: "WorkingCodeRefId",
                principalTable: "WorkingCode",
                principalColumn: "WorkingCodeId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

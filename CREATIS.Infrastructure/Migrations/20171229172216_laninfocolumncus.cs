﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class laninfocolumncus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CustomerRefId",
                table: "LangInfoColumns",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_LangInfoColumns_CustomerRefId",
                table: "LangInfoColumns",
                column: "CustomerRefId");

            migrationBuilder.AddForeignKey(
                name: "FK_LangInfoColumns_Customers_CustomerRefId",
                table: "LangInfoColumns",
                column: "CustomerRefId",
                principalTable: "Customers",
                principalColumn: "CustomerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LangInfoColumns_Customers_CustomerRefId",
                table: "LangInfoColumns");

            migrationBuilder.DropIndex(
                name: "IX_LangInfoColumns_CustomerRefId",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "CustomerRefId",
                table: "LangInfoColumns");
        }
    }
}

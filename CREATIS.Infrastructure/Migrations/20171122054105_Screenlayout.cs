﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class Screenlayout : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ScreenLayout",
                columns: table => new
                {
                    ScreenLayoutId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRegistrationRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScreenLayout", x => x.ScreenLayoutId);
                    table.ForeignKey(
                        name: "FK_ScreenLayout_ActivitiesRegistration_ActivitiesRegistrationRefId",
                        column: x => x.ActivitiesRegistrationRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_ScreenLayout_Customers_CustomerRefId",
                    column: x => x.CustomerRefId,
                    principalTable: "Customers",
                    principalColumn: "CustomerId");
                table.ForeignKey(
                    name: "FK_ScreenLayout_Layout_LayoutRefId",
                    column: x => x.LayoutRefId,
                    principalTable: "Layout",
                    principalColumn: "LayoutId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_ScreenLayout_ActivitiesRegistrationRefId",
                table: "ScreenLayout",
                column: "ActivitiesRegistrationRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ScreenLayout_CustomerRefId",
                table: "ScreenLayout",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ScreenLayout_LayoutRefId",
                table: "ScreenLayout",
                column: "LayoutRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ScreenLayout");
        }
    }
}

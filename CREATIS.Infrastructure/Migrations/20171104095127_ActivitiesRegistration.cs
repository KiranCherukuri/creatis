﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class ActivitiesRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InterruptibleBy",
                columns: table => new
                {
                    InterruptibleById = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    InterruptibleByDescription = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InterruptibleBy", x => x.InterruptibleById);
                });

            migrationBuilder.CreateTable(
                name: "Planning",
                columns: table => new
                {
                    PlanningId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DefaultActivitiesRegistrationDescription = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Planning", x => x.PlanningId);
                });

            migrationBuilder.CreateTable(
                name: "PlanningOverview",
                columns: table => new
                {
                    PlanningOverviewId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    PlanningOverviewDescription = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningOverview", x => x.PlanningOverviewId);
                });

            migrationBuilder.CreateTable(
                name: "PTO",
                columns: table => new
                {
                    PTOId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    PTODescription = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PTO", x => x.PTOId);
                });

            migrationBuilder.CreateTable(
                name: "Specific",
                columns: table => new
                {
                    SpecificId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    SpecificDescription = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Specific", x => x.SpecificId);
                });

            migrationBuilder.CreateTable(
                name: "WorkingCode",
                columns: table => new
                {
                    WorkingCodeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    WorkingCodeDescription = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkingCode", x => x.WorkingCodeId);
                });

            migrationBuilder.CreateTable(
                name: "ActivitiesRegistration",
                columns: table => new
                {
                    ActivitiesRegistrationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRegistrationName = table.Column<string>(nullable: false),
                    ConfirmButton = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    EmptyFullSolo = table.Column<int>(nullable: false),
                    Flexactivity = table.Column<int>(nullable: false),
                    ISModification = table.Column<int>(nullable: false),
                    InterruptibleByRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    PTORefId = table.Column<int>(nullable: false),
                    PlanningOverviewRefId = table.Column<int>(nullable: false),
                    PlanningRefId = table.Column<int>(nullable: false),
                    SpecificRefId = table.Column<int>(nullable: false),
                    TransportType = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    Visible = table.Column<int>(nullable: false),
                    WorkingCodeRefId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivitiesRegistration", x => x.ActivitiesRegistrationId);
                    table.ForeignKey(
                        name: "FK_ActivitiesRegistration_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivitiesRegistration_InterruptibleBy_InterruptibleByRefId",
                        column: x => x.InterruptibleByRefId,
                        principalTable: "InterruptibleBy",
                        principalColumn: "InterruptibleById",
                        onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_ActivitiesRegistration_Layout_LayoutRefId",
                    column: x => x.LayoutRefId,
                    principalTable: "Layout",
                    principalColumn: "LayoutId");
                        //onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivitiesRegistration_PTO_PTORefId",
                        column: x => x.PTORefId,
                        principalTable: "PTO",
                        principalColumn: "PTOId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivitiesRegistration_PlanningOverview_PlanningOverviewRefId",
                        column: x => x.PlanningOverviewRefId,
                        principalTable: "PlanningOverview",
                        principalColumn: "PlanningOverviewId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivitiesRegistration_Planning_PlanningRefId",
                        column: x => x.PlanningRefId,
                        principalTable: "Planning",
                        principalColumn: "PlanningId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivitiesRegistration_Specific_SpecificRefId",
                        column: x => x.SpecificRefId,
                        principalTable: "Specific",
                        principalColumn: "SpecificId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivitiesRegistration_WorkingCode_WorkingCodeRefId",
                        column: x => x.WorkingCodeRefId,
                        principalTable: "WorkingCode",
                        principalColumn: "WorkingCodeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_CustomerRefId",
                table: "ActivitiesRegistration",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_InterruptibleByRefId",
                table: "ActivitiesRegistration",
                column: "InterruptibleByRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_LayoutRefId",
                table: "ActivitiesRegistration",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_PTORefId",
                table: "ActivitiesRegistration",
                column: "PTORefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_PlanningOverviewRefId",
                table: "ActivitiesRegistration",
                column: "PlanningOverviewRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_PlanningRefId",
                table: "ActivitiesRegistration",
                column: "PlanningRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_SpecificRefId",
                table: "ActivitiesRegistration",
                column: "SpecificRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_WorkingCodeRefId",
                table: "ActivitiesRegistration",
                column: "WorkingCodeRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivitiesRegistration");

            migrationBuilder.DropTable(
                name: "InterruptibleBy");

            migrationBuilder.DropTable(
                name: "PTO");

            migrationBuilder.DropTable(
                name: "PlanningOverview");

            migrationBuilder.DropTable(
                name: "Planning");

            migrationBuilder.DropTable(
                name: "Specific");

            migrationBuilder.DropTable(
                name: "WorkingCode");
        }
    }
}

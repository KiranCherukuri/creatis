﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class DocumentScanName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LangActReg_LanguageMaster_LanguageMasterRefId",
                table: "LangActReg");

            migrationBuilder.DropForeignKey(
                name: "FK_LangAllowances_LanguageMaster_LanguageMasterRefId",
                table: "LangAllowances");

            migrationBuilder.DropForeignKey(
                name: "FK_LangInfoColumns_LanguageMaster_LanguageMasterRefId",
                table: "LangInfoColumns");

            migrationBuilder.DropIndex(
                name: "IX_LangInfoColumns_LanguageMasterRefId",
                table: "LangInfoColumns");

            migrationBuilder.DropIndex(
                name: "IX_LangAllowances_LanguageMasterRefId",
                table: "LangAllowances");

            migrationBuilder.DropIndex(
                name: "IX_LangActReg_LanguageMasterRefId",
                table: "LangActReg");

            migrationBuilder.DropColumn(
                name: "LanguageMasterRefId",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "LanguageMasterRefId",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "LanguageMasterRefId",
                table: "LangActReg");

            migrationBuilder.AddColumn<string>(
                name: "Arabic",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Bulgarian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Croatian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Czech",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Danish",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Dutch",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "English",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Estonian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Finnish",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "French",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "German",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Greek",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Hungarian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Italian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Latvian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Lithuanian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Macedonian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Norwegian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Polish",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Portuguese",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Romanian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Russian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Slovak",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Slovene",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Spanish",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Swedish",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Turkish",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Ukrainian",
                table: "LangInfoColumns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Arabic",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Bulgarian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Croatian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Czech",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Danish",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Dutch",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "English",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Estonian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Finnish",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "French",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "German",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Greek",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Hungarian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Italian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Latvian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Lithuanian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Macedonian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Norwegian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Polish",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Portuguese",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Romanian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Russian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Slovak",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Slovene",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Spanish",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Swedish",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Turkish",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Ukrainian",
                table: "LangAllowances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocumentName",
                table: "DocumetScanProperties",
                maxLength: 50,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Arabic",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Bulgarian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Croatian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Czech",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Danish",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Dutch",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "English",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Estonian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Finnish",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "French",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "German",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Greek",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Hungarian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Italian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Latvian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Lithuanian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Macedonian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Norwegian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Polish",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Portuguese",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Romanian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Russian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Slovak",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Slovene",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Spanish",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Swedish",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Turkish",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Ukrainian",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "Arabic",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Bulgarian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Croatian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Czech",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Danish",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Dutch",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "English",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Estonian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Finnish",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "French",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "German",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Greek",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Hungarian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Italian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Latvian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Lithuanian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Macedonian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Norwegian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Polish",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Portuguese",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Romanian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Russian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Slovak",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Slovene",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Spanish",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Swedish",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Turkish",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "Ukrainian",
                table: "LangAllowances");

            migrationBuilder.DropColumn(
                name: "DocumentName",
                table: "DocumetScanProperties");

            migrationBuilder.AddColumn<int>(
                name: "LanguageMasterRefId",
                table: "LangInfoColumns",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LanguageMasterRefId",
                table: "LangAllowances",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LanguageMasterRefId",
                table: "LangActReg",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_LangInfoColumns_LanguageMasterRefId",
                table: "LangInfoColumns",
                column: "LanguageMasterRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangAllowances_LanguageMasterRefId",
                table: "LangAllowances",
                column: "LanguageMasterRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangActReg_LanguageMasterRefId",
                table: "LangActReg",
                column: "LanguageMasterRefId");

            migrationBuilder.AddForeignKey(
                name: "FK_LangActReg_LanguageMaster_LanguageMasterRefId",
                table: "LangActReg",
                column: "LanguageMasterRefId",
                principalTable: "LanguageMaster",
                principalColumn: "LanguageId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LangAllowances_LanguageMaster_LanguageMasterRefId",
                table: "LangAllowances",
                column: "LanguageMasterRefId",
                principalTable: "LanguageMaster",
                principalColumn: "LanguageId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LangInfoColumns_LanguageMaster_LanguageMasterRefId",
                table: "LangInfoColumns",
                column: "LanguageMasterRefId",
                principalTable: "LanguageMaster",
                principalColumn: "LanguageId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

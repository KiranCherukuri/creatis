﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class defaultactivitiestable1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DefaultActivities",
                columns: table => new
                {
                    ActivitiesRegistrationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRegistrationName = table.Column<string>(nullable: false),
                    ConfirmButton = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EmptyFullSolo = table.Column<bool>(nullable: false),
                    Flexactivity = table.Column<bool>(nullable: false),
                    ISModification = table.Column<int>(nullable: false),
                    InterruptibleByRefId = table.Column<string>(maxLength: 50, nullable: false),
                    OBCDefault = table.Column<int>(nullable: false),
                    PTORefId = table.Column<string>(maxLength: 50, nullable: false),
                    PlanningOverviewRefId = table.Column<string>(maxLength: 50, nullable: false),
                    PlanningRefId = table.Column<string>(maxLength: 50, nullable: false),
                    SpecificRefId = table.Column<string>(maxLength: 50, nullable: false),
                    TransportType = table.Column<bool>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    Visible = table.Column<bool>(nullable: false),
                    WorkingCodeRefId = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DefaultActivities", x => x.ActivitiesRegistrationId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DefaultActivities");
        }
    }
}

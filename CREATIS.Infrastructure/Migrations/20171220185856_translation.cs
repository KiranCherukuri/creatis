﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class translation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EntryType",
                table: "EntryProperties",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "LangActReg",
                columns: table => new
                {
                    ActivityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityName = table.Column<string>(maxLength: 150, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LanguageMasterRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LangActReg", x => x.ActivityId);
                    table.ForeignKey(
                        name: "FK_LangActReg_LanguageMaster_LanguageMasterRefId",
                        column: x => x.LanguageMasterRefId,
                        principalTable: "LanguageMaster",
                        principalColumn: "LanguageId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LangActReg_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LangAllowances",
                columns: table => new
                {
                    AllowanceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllowanceName = table.Column<string>(maxLength: 150, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LanguageMasterRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LangAllowances", x => x.AllowanceId);
                    table.ForeignKey(
                        name: "FK_LangAllowances_LanguageMaster_LanguageMasterRefId",
                        column: x => x.LanguageMasterRefId,
                        principalTable: "LanguageMaster",
                        principalColumn: "LanguageId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LangAllowances_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LangDriverInput",
                columns: table => new
                {
                    DriverInputId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DriverInputName = table.Column<string>(maxLength: 150, nullable: true),
                    LanguageMasterRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LangDriverInput", x => x.DriverInputId);
                    table.ForeignKey(
                        name: "FK_LangDriverInput_LanguageMaster_LanguageMasterRefId",
                        column: x => x.LanguageMasterRefId,
                        principalTable: "LanguageMaster",
                        principalColumn: "LanguageId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LangDriverInput_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LangInfoColumns",
                columns: table => new
                {
                    InfoColumnId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    InfoColumnName = table.Column<string>(maxLength: 150, nullable: true),
                    LanguageMasterRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LangInfoColumns", x => x.InfoColumnId);
                    table.ForeignKey(
                        name: "FK_LangInfoColumns_LanguageMaster_LanguageMasterRefId",
                        column: x => x.LanguageMasterRefId,
                        principalTable: "LanguageMaster",
                        principalColumn: "LanguageId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LangInfoColumns_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LangMessages",
                columns: table => new
                {
                    MessageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LanguageMasterRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    MessageName = table.Column<string>(maxLength: 150, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LangMessages", x => x.MessageId);
                    table.ForeignKey(
                        name: "FK_LangMessages_LanguageMaster_LanguageMasterRefId",
                        column: x => x.LanguageMasterRefId,
                        principalTable: "LanguageMaster",
                        principalColumn: "LanguageId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LangMessages_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PageMaster",
                columns: table => new
                {
                    PageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    PageName = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PageMaster", x => x.PageId);
                });

            migrationBuilder.CreateTable(
                name: "UIContentLanguage",
                columns: table => new
                {
                    LabelID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllLangauges = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UIContentLanguage", x => x.LabelID);
                });

            migrationBuilder.CreateTable(
                name: "UserLanguage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: false),
                    Language = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    UserRefId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLanguage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserLanguage_Users_UserRefId",
                        column: x => x.UserRefId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UIContentMaster",
                columns: table => new
                {
                    LabelId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LabelName = table.Column<string>(maxLength: 150, nullable: true),
                    PageRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UIContentMaster", x => x.LabelId);
                    table.ForeignKey(
                        name: "FK_UIContentMaster_PageMaster_PageRefId",
                        column: x => x.PageRefId,
                        principalTable: "PageMaster",
                        principalColumn: "PageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LangActReg_LanguageMasterRefId",
                table: "LangActReg",
                column: "LanguageMasterRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangActReg_LayoutRefId",
                table: "LangActReg",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangAllowances_LanguageMasterRefId",
                table: "LangAllowances",
                column: "LanguageMasterRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangAllowances_LayoutRefId",
                table: "LangAllowances",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangDriverInput_LanguageMasterRefId",
                table: "LangDriverInput",
                column: "LanguageMasterRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangDriverInput_LayoutRefId",
                table: "LangDriverInput",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangInfoColumns_LanguageMasterRefId",
                table: "LangInfoColumns",
                column: "LanguageMasterRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangInfoColumns_LayoutRefId",
                table: "LangInfoColumns",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangMessages_LanguageMasterRefId",
                table: "LangMessages",
                column: "LanguageMasterRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangMessages_LayoutRefId",
                table: "LangMessages",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_UIContentMaster_PageRefId",
                table: "UIContentMaster",
                column: "PageRefId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLanguage_UserRefId",
                table: "UserLanguage",
                column: "UserRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LangActReg");

            migrationBuilder.DropTable(
                name: "LangAllowances");

            migrationBuilder.DropTable(
                name: "LangDriverInput");

            migrationBuilder.DropTable(
                name: "LangInfoColumns");

            migrationBuilder.DropTable(
                name: "LangMessages");

            migrationBuilder.DropTable(
                name: "UIContentLanguage");

            migrationBuilder.DropTable(
                name: "UIContentMaster");

            migrationBuilder.DropTable(
                name: "UserLanguage");

            migrationBuilder.DropTable(
                name: "PageMaster");

            migrationBuilder.DropColumn(
                name: "EntryType",
                table: "EntryProperties");
        }
    }
}

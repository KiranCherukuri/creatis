﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class _1st : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    CustomerId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<int>(nullable: false),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    Environment = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 250, nullable: true),
                    Source = table.Column<string>(maxLength: 20, nullable: true),
                    SourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.CustomerId);
                });

            migrationBuilder.CreateTable(
                name: "DefaultActivities",
                columns: table => new
                {
                    ActivitiesRegistrationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRegistrationName = table.Column<string>(nullable: false),
                    ConfirmButton = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EmptyFullSolo = table.Column<bool>(nullable: false),
                    Flexactivity = table.Column<bool>(nullable: false),
                    ISModification = table.Column<int>(nullable: false),
                    ISPlanningSpec = table.Column<int>(nullable: false),
                    InterruptibleByRefId = table.Column<string>(maxLength: 50, nullable: false),
                    OBCDefault = table.Column<int>(nullable: false),
                    PTORefId = table.Column<string>(maxLength: 50, nullable: false),
                    PauseTrip = table.Column<int>(nullable: false),
                    PlanningOverviewRefId = table.Column<string>(maxLength: 50, nullable: false),
                    PlanningRefId = table.Column<string>(maxLength: 50, nullable: false),
                    SpecificRefId = table.Column<string>(maxLength: 50, nullable: false),
                    TransportType = table.Column<bool>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    Visible = table.Column<bool>(nullable: false),
                    WorkingCodeRefId = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DefaultActivities", x => x.ActivitiesRegistrationId);
                });

            migrationBuilder.CreateTable(
                name: "InfoColumnMaster",
                columns: table => new
                {
                    ICM_Column_No = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ICM_Name = table.Column<string>(maxLength: 50, nullable: true),
                    ISM_Active = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfoColumnMaster", x => x.ICM_Column_No);
                });

            migrationBuilder.CreateTable(
                name: "InterruptibleBy",
                columns: table => new
                {
                    InterruptibleById = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    InterruptibleByDescription = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InterruptibleBy", x => x.InterruptibleById);
                });

            migrationBuilder.CreateTable(
                name: "LanguageMaster",
                columns: table => new
                {
                    LanguageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    LanguageCode = table.Column<string>(maxLength: 30, nullable: true),
                    LanguageDescription = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LanguageMaster", x => x.LanguageId);
                });

            migrationBuilder.CreateTable(
                name: "MainToolBox",
                columns: table => new
                {
                    ToolboxId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ToolboxDescription = table.Column<string>(maxLength: 50, nullable: false),
                    ToolboxName = table.Column<string>(maxLength: 50, nullable: false),
                    ToolboxStatus = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainToolBox", x => x.ToolboxId);
                });

            migrationBuilder.CreateTable(
                name: "OBCTypeMaster",
                columns: table => new
                {
                    OBCTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    OBCTypeDescription = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OBCTypeMaster", x => x.OBCTypeId);
                });

            migrationBuilder.CreateTable(
                name: "PageMaster",
                columns: table => new
                {
                    PageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    PageName = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PageMaster", x => x.PageId);
                });

            migrationBuilder.CreateTable(
                name: "Planning",
                columns: table => new
                {
                    PlanningId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DefaultActivitiesRegistrationDescription = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Planning", x => x.PlanningId);
                });

            migrationBuilder.CreateTable(
                name: "PlanningOverview",
                columns: table => new
                {
                    PlanningOverviewId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    PlanningOverviewDescription = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningOverview", x => x.PlanningOverviewId);
                });

            migrationBuilder.CreateTable(
                name: "PlanningTypeMaster",
                columns: table => new
                {
                    PlanningTypeMasterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    PlanningTypeMasterDescription = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningTypeMaster", x => x.PlanningTypeMasterId);
                });

            migrationBuilder.CreateTable(
                name: "PTO",
                columns: table => new
                {
                    PTOId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    PTODescription = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PTO", x => x.PTOId);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    RoleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    RoleName = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "Specific",
                columns: table => new
                {
                    SpecificId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    SpecificDescription = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Specific", x => x.SpecificId);
                });

            migrationBuilder.CreateTable(
                name: "StatusMaster",
                columns: table => new
                {
                    Status_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Status_Description = table.Column<string>(nullable: true),
                    Status_IsActive = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatusMaster", x => x.Status_ID);
                });

            migrationBuilder.CreateTable(
                name: "ToolBoxIcons",
                columns: table => new
                {
                    ToolboxIconId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ToolboxIconPath = table.Column<string>(maxLength: 500, nullable: false),
                    ToolboxIconStatus = table.Column<int>(nullable: false),
                    ToolboxRefId = table.Column<int>(nullable: false),
                    ToolboxType = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToolBoxIcons", x => x.ToolboxIconId);
                });

            migrationBuilder.CreateTable(
                name: "UIContentLanguage",
                columns: table => new
                {
                    LabelID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllLangauges = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UIContentLanguage", x => x.LabelID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DomainId = table.Column<string>(maxLength: 50, nullable: true),
                    EmailId = table.Column<string>(maxLength: 100, nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    RoleId = table.Column<string>(maxLength: 100, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    UserStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "WorkingCode",
                columns: table => new
                {
                    WorkingCodeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    WorkingCodeDescription = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkingCode", x => x.WorkingCodeId);
                });

            migrationBuilder.CreateTable(
                name: "CustomerNotes",
                columns: table => new
                {
                    CN_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CN_Details = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerNotes", x => x.CN_ID);
                    table.ForeignKey(
                        name: "FK_CustomerNotes_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Integrator",
                columns: table => new
                {
                    I_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    I_Details = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Integrator", x => x.I_ID);
                    table.ForeignKey(
                        name: "FK_Integrator_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Layout",
                columns: table => new
                {
                    LayoutId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    IsLocked = table.Column<int>(nullable: false),
                    LayoutName = table.Column<string>(maxLength: 100, nullable: true),
                    LayoutOrigin = table.Column<string>(maxLength: 100, nullable: true),
                    LayoutStatus = table.Column<int>(nullable: false),
                    LayoutType = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Layout", x => x.LayoutId);
                    table.ForeignKey(
                        name: "FK_Layout_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfoColumns",
                columns: table => new
                {
                    IC_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CustomerRefId = table.Column<int>(nullable: true),
                    ICM_Column_Ref_No = table.Column<int>(nullable: true),
                    IC_Column_Name = table.Column<string>(nullable: false),
                    IS_Active = table.Column<int>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfoColumns", x => x.IC_ID);
                    table.ForeignKey(
                        name: "FK_InfoColumns_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InfoColumns_InfoColumnMaster_ICM_Column_Ref_No",
                        column: x => x.ICM_Column_Ref_No,
                        principalTable: "InfoColumnMaster",
                        principalColumn: "ICM_Column_No",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ToolBoxSubItem",
                columns: table => new
                {
                    SubToolboxId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    SubToolboxDescription = table.Column<string>(maxLength: 50, nullable: false),
                    SubToolboxName = table.Column<string>(maxLength: 50, nullable: false),
                    SubToolboxStatus = table.Column<int>(nullable: false),
                    ToolboxRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToolBoxSubItem", x => x.SubToolboxId);
                    table.ForeignKey(
                        name: "FK_ToolBoxSubItem_MainToolBox_ToolboxRefId",
                        column: x => x.ToolboxRefId,
                        principalTable: "MainToolBox",
                        principalColumn: "ToolboxId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AllowancesMaster",
                columns: table => new
                {
                    AllowancesMasterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllowancesCode = table.Column<string>(maxLength: 50, nullable: false),
                    AllowancesMasterDescription = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    OBCTypeRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllowancesMaster", x => x.AllowancesMasterId);
                    table.ForeignKey(
                        name: "FK_AllowancesMaster_OBCTypeMaster_OBCTypeRefId",
                        column: x => x.OBCTypeRefId,
                        principalTable: "OBCTypeMaster",
                        principalColumn: "OBCTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UIContentMaster",
                columns: table => new
                {
                    LabelId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LabelName = table.Column<string>(maxLength: 150, nullable: true),
                    PageRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UIContentMaster", x => x.LabelId);
                    table.ForeignKey(
                        name: "FK_UIContentMaster_PageMaster_PageRefId",
                        column: x => x.PageRefId,
                        principalTable: "PageMaster",
                        principalColumn: "PageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanningSpecificationMaster",
                columns: table => new
                {
                    PlanningSpecificationMasterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    PlanningSpecificationMasterDescription = table.Column<string>(maxLength: 50, nullable: false),
                    PlanningTypeMasterRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningSpecificationMaster", x => x.PlanningSpecificationMasterId);
                    table.ForeignKey(
                        name: "FK_PlanningSpecificationMaster_PlanningTypeMaster_PlanningTypeMasterRefId",
                        column: x => x.PlanningTypeMasterRefId,
                        principalTable: "PlanningTypeMaster",
                        principalColumn: "PlanningTypeMasterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLanguage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: false),
                    Language = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    UserRefId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLanguage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserLanguage_Users_UserRefId",
                        column: x => x.UserRefId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ActivitiesRegistration",
                columns: table => new
                {
                    ActivitiesRegistrationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActRegPosition = table.Column<int>(nullable: false),
                    ActivitiesRegistrationName = table.Column<string>(nullable: false),
                    ConfirmButton = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    EmptyFullSolo = table.Column<bool>(nullable: false),
                    Flexactivity = table.Column<bool>(nullable: false),
                    ISModification = table.Column<int>(nullable: false),
                    InterruptibleByRefId = table.Column<string>(maxLength: 50, nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    OBCDefault = table.Column<int>(nullable: true),
                    PTORefId = table.Column<string>(maxLength: 50, nullable: false),
                    PlanningOverviewRefId = table.Column<string>(maxLength: 50, nullable: false),
                    PlanningRefId = table.Column<string>(maxLength: 50, nullable: false),
                    SpecificRefId = table.Column<string>(maxLength: 50, nullable: false),
                    TransportType = table.Column<bool>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    Visible = table.Column<bool>(nullable: false),
                    WorkingCodeRefId = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivitiesRegistration", x => x.ActivitiesRegistrationId);
                    table.ForeignKey(
                        name: "FK_ActivitiesRegistration_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivitiesRegistration_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LangDriverInput",
                columns: table => new
                {
                    DriverInputId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DriverInputName = table.Column<string>(maxLength: 150, nullable: true),
                    LanguageMasterRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LangDriverInput", x => x.DriverInputId);
                    table.ForeignKey(
                        name: "FK_LangDriverInput_LanguageMaster_LanguageMasterRefId",
                        column: x => x.LanguageMasterRefId,
                        principalTable: "LanguageMaster",
                        principalColumn: "LanguageId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LangDriverInput_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LangMessages",
                columns: table => new
                {
                    MessageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LanguageMasterRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    MessageName = table.Column<string>(maxLength: 150, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LangMessages", x => x.MessageId);
                    table.ForeignKey(
                        name: "FK_LangMessages_LanguageMaster_LanguageMasterRefId",
                        column: x => x.LanguageMasterRefId,
                        principalTable: "LanguageMaster",
                        principalColumn: "LanguageId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LangMessages_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OBCType",
                columns: table => new
                {
                    OBCTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    OBCTypeRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OBCType", x => x.OBCTypeId);
                    table.ForeignKey(
                        name: "FK_OBCType_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OBCType_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OBCType_OBCTypeMaster_OBCTypeRefId",
                        column: x => x.OBCTypeRefId,
                        principalTable: "OBCTypeMaster",
                        principalColumn: "OBCTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanningType",
                columns: table => new
                {
                    PlanningTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    PlanningTypeRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningType", x => x.PlanningTypeId);
                    table.ForeignKey(
                        name: "FK_PlanningType_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanningType_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanningType_PlanningTypeMaster_PlanningTypeRefId",
                        column: x => x.PlanningTypeRefId,
                        principalTable: "PlanningTypeMaster",
                        principalColumn: "PlanningTypeMasterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RecentActivity",
                columns: table => new
                {
                    ActivityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecentActivity", x => x.ActivityId);
                    table.ForeignKey(
                        name: "FK_RecentActivity_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RecentActivity_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransportTypes",
                columns: table => new
                {
                    TransportTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    TransportTypes = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransportTypes", x => x.TransportTypeId);
                    table.ForeignKey(
                        name: "FK_TransportTypes_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransportTypes_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UsedLanguages",
                columns: table => new
                {
                    UsedLanguagesId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    IsDefault = table.Column<int>(nullable: false),
                    LanguageRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsedLanguages", x => x.UsedLanguagesId);
                    table.ForeignKey(
                        name: "FK_UsedLanguages_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UsedLanguages_LanguageMaster_LanguageRefId",
                        column: x => x.LanguageRefId,
                        principalTable: "LanguageMaster",
                        principalColumn: "LanguageId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UsedLanguages_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LangInfoColumns",
                columns: table => new
                {
                    InfoColumnId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Arabic = table.Column<string>(nullable: true),
                    Bulgarian = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Croatian = table.Column<string>(nullable: true),
                    CustomerRefId = table.Column<int>(nullable: true),
                    Czech = table.Column<string>(nullable: true),
                    Danish = table.Column<string>(nullable: true),
                    Dutch = table.Column<string>(nullable: true),
                    English = table.Column<string>(nullable: true),
                    Estonian = table.Column<string>(nullable: true),
                    Finnish = table.Column<string>(nullable: true),
                    French = table.Column<string>(nullable: true),
                    German = table.Column<string>(nullable: true),
                    Greek = table.Column<string>(nullable: true),
                    Hungarian = table.Column<string>(nullable: true),
                    ICRefId = table.Column<int>(nullable: false),
                    InfoColumnName = table.Column<string>(maxLength: 150, nullable: true),
                    Italian = table.Column<string>(nullable: true),
                    Latvian = table.Column<string>(nullable: true),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Lithuanian = table.Column<string>(nullable: true),
                    Macedonian = table.Column<string>(nullable: true),
                    Norwegian = table.Column<string>(nullable: true),
                    Polish = table.Column<string>(nullable: true),
                    Portuguese = table.Column<string>(nullable: true),
                    Romanian = table.Column<string>(nullable: true),
                    Russian = table.Column<string>(nullable: true),
                    Slovak = table.Column<string>(nullable: true),
                    Slovene = table.Column<string>(nullable: true),
                    Spanish = table.Column<string>(nullable: true),
                    Swedish = table.Column<string>(nullable: true),
                    Turkish = table.Column<string>(nullable: true),
                    Ukrainian = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LangInfoColumns", x => x.InfoColumnId);
                    table.ForeignKey(
                        name: "FK_LangInfoColumns_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LangInfoColumns_InfoColumns_ICRefId",
                        column: x => x.ICRefId,
                        principalTable: "InfoColumns",
                        principalColumn: "IC_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LangInfoColumns_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PropertyMaster",
                columns: table => new
                {
                    PropertyId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDefault = table.Column<int>(nullable: false),
                    PropertyName = table.Column<string>(nullable: true),
                    PropertyStatus = table.Column<int>(nullable: false),
                    SubToolboxRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyMaster", x => x.PropertyId);
                    table.ForeignKey(
                        name: "FK_PropertyMaster_ToolBoxSubItem_SubToolboxRefId",
                        column: x => x.SubToolboxRefId,
                        principalTable: "ToolBoxSubItem",
                        principalColumn: "SubToolboxId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Allowances",
                columns: table => new
                {
                    AllowancesId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllowancesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Allowances", x => x.AllowancesId);
                    table.ForeignKey(
                        name: "FK_Allowances_AllowancesMaster_AllowancesRefId",
                        column: x => x.AllowancesRefId,
                        principalTable: "AllowancesMaster",
                        principalColumn: "AllowancesMasterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Allowances_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Allowances_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanningSpecification",
                columns: table => new
                {
                    PlanningSpecificationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    PlanningSpecificationRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningSpecification", x => x.PlanningSpecificationId);
                    table.ForeignKey(
                        name: "FK_PlanningSpecification_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanningSpecification_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanningSpecification_PlanningSpecificationMaster_PlanningSpecificationRefId",
                        column: x => x.PlanningSpecificationRefId,
                        principalTable: "PlanningSpecificationMaster",
                        principalColumn: "PlanningSpecificationMasterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LangActReg",
                columns: table => new
                {
                    ActivityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    ActivityName = table.Column<string>(maxLength: 150, nullable: true),
                    Arabic = table.Column<string>(nullable: true),
                    Bulgarian = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Croatian = table.Column<string>(nullable: true),
                    Czech = table.Column<string>(nullable: true),
                    Danish = table.Column<string>(nullable: true),
                    Dutch = table.Column<string>(nullable: true),
                    English = table.Column<string>(nullable: true),
                    Estonian = table.Column<string>(nullable: true),
                    Finnish = table.Column<string>(nullable: true),
                    French = table.Column<string>(nullable: true),
                    German = table.Column<string>(nullable: true),
                    Greek = table.Column<string>(nullable: true),
                    Hungarian = table.Column<string>(nullable: true),
                    Italian = table.Column<string>(nullable: true),
                    Latvian = table.Column<string>(nullable: true),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Lithuanian = table.Column<string>(nullable: true),
                    Macedonian = table.Column<string>(nullable: true),
                    Norwegian = table.Column<string>(nullable: true),
                    Polish = table.Column<string>(nullable: true),
                    Portuguese = table.Column<string>(nullable: true),
                    Romanian = table.Column<string>(nullable: true),
                    Russian = table.Column<string>(nullable: true),
                    Slovak = table.Column<string>(nullable: true),
                    Slovene = table.Column<string>(nullable: true),
                    Spanish = table.Column<string>(nullable: true),
                    Swedish = table.Column<string>(nullable: true),
                    Turkish = table.Column<string>(nullable: true),
                    Ukrainian = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    WorkingCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LangActReg", x => x.ActivityId);
                    table.ForeignKey(
                        name: "FK_LangActReg_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LangActReg_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    QuestionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    ParentId = table.Column<int>(nullable: false),
                    PropertyName = table.Column<string>(nullable: true),
                    TransporttypeRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.QuestionId);
                    table.ForeignKey(
                        name: "FK_Questions_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Questions_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ScreenLayout",
                columns: table => new
                {
                    ScreenLayoutId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRegistrationRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    ScreenLayoutPosition = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScreenLayout", x => x.ScreenLayoutId);
                    table.ForeignKey(
                        name: "FK_ScreenLayout_ActivitiesRegistration_ActivitiesRegistrationRefId",
                        column: x => x.ActivitiesRegistrationRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ScreenLayout_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ScreenLayout_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PropertyDetails",
                columns: table => new
                {
                    PropertyDetailsId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDefault = table.Column<int>(nullable: false),
                    PropertyDetailsStatus = table.Column<int>(nullable: false),
                    PropertyRefId = table.Column<int>(nullable: false),
                    PropertyValue = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyDetails", x => x.PropertyDetailsId);
                    table.ForeignKey(
                        name: "FK_PropertyDetails_PropertyMaster_PropertyRefId",
                        column: x => x.PropertyRefId,
                        principalTable: "PropertyMaster",
                        principalColumn: "PropertyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LangAllowances",
                columns: table => new
                {
                    AllowanceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllowanceName = table.Column<string>(maxLength: 150, nullable: true),
                    AllowancesMRefId = table.Column<int>(nullable: false),
                    AllowancesRefId = table.Column<int>(nullable: false),
                    Arabic = table.Column<string>(nullable: true),
                    Bulgarian = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Croatian = table.Column<string>(nullable: true),
                    Czech = table.Column<string>(nullable: true),
                    Danish = table.Column<string>(nullable: true),
                    Dutch = table.Column<string>(nullable: true),
                    English = table.Column<string>(nullable: true),
                    Estonian = table.Column<string>(nullable: true),
                    Finnish = table.Column<string>(nullable: true),
                    French = table.Column<string>(nullable: true),
                    German = table.Column<string>(nullable: true),
                    Greek = table.Column<string>(nullable: true),
                    Hungarian = table.Column<string>(nullable: true),
                    Italian = table.Column<string>(nullable: true),
                    Latvian = table.Column<string>(nullable: true),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Lithuanian = table.Column<string>(nullable: true),
                    Macedonian = table.Column<string>(nullable: true),
                    Norwegian = table.Column<string>(nullable: true),
                    Polish = table.Column<string>(nullable: true),
                    Portuguese = table.Column<string>(nullable: true),
                    Romanian = table.Column<string>(nullable: true),
                    Russian = table.Column<string>(nullable: true),
                    Slovak = table.Column<string>(nullable: true),
                    Slovene = table.Column<string>(nullable: true),
                    Spanish = table.Column<string>(nullable: true),
                    Swedish = table.Column<string>(nullable: true),
                    Turkish = table.Column<string>(nullable: true),
                    Ukrainian = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LangAllowances", x => x.AllowanceId);
                    table.ForeignKey(
                        name: "FK_LangAllowances_Allowances_AllowancesRefId",
                        column: x => x.AllowancesRefId,
                        principalTable: "Allowances",
                        principalColumn: "AllowancesId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LangAllowances_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CheckoffProperties",
                columns: table => new
                {
                    CheckoffId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Planninglevel = table.Column<string>(maxLength: 50, nullable: false),
                    QuestionRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckoffProperties", x => x.CheckoffId);
                    table.ForeignKey(
                        name: "FK_CheckoffProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CheckoffProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CheckoffProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChioceProperties",
                columns: table => new
                {
                    ChioceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    FeedbackType = table.Column<string>(maxLength: 100, nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Planningfeedback = table.Column<string>(maxLength: 100, nullable: false),
                    Propose = table.Column<string>(maxLength: 100, nullable: true),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Recursiveloop = table.Column<int>(nullable: false),
                    Save = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChioceProperties", x => x.ChioceId);
                    table.ForeignKey(
                        name: "FK_ChioceProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChioceProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChioceProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DocumetScanProperties",
                columns: table => new
                {
                    DocumetScanId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DocumentName = table.Column<string>(maxLength: 50, nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    PlanningFeedbackLevel = table.Column<string>(maxLength: 50, nullable: true),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumetScanProperties", x => x.DocumetScanId);
                    table.ForeignKey(
                        name: "FK_DocumetScanProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumetScanProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumetScanProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EntryProperties",
                columns: table => new
                {
                    EntryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EntryType = table.Column<string>(maxLength: 100, nullable: false),
                    ExactCharacter = table.Column<int>(nullable: false),
                    FeedbackType = table.Column<string>(maxLength: 100, nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    MaskText = table.Column<string>(maxLength: 100, nullable: false),
                    MinimumofCharacter = table.Column<int>(nullable: false),
                    NotNull = table.Column<int>(nullable: false),
                    Planningfeedback = table.Column<string>(maxLength: 100, nullable: false),
                    PlanningfeedbackCode = table.Column<string>(maxLength: 100, nullable: false),
                    Predefined = table.Column<string>(maxLength: 100, nullable: false),
                    Propose = table.Column<int>(nullable: false),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Regex = table.Column<string>(maxLength: 100, nullable: false),
                    Save = table.Column<int>(nullable: false),
                    Size = table.Column<string>(maxLength: 100, nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    Zeronotallowed = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EntryProperties", x => x.EntryId);
                    table.ForeignKey(
                        name: "FK_EntryProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EntryProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EntryProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfomessageProperties",
                columns: table => new
                {
                    InfomessageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Fontstyle = table.Column<string>(maxLength: 50, nullable: true),
                    Icontype = table.Column<string>(maxLength: 50, nullable: true),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Messagecontent = table.Column<string>(maxLength: 50, nullable: true),
                    Messagetype = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Titlestyle = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfomessageProperties", x => x.InfomessageId);
                    table.ForeignKey(
                        name: "FK_InfomessageProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InfomessageProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InfomessageProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanningselectProperties",
                columns: table => new
                {
                    PlanningselectId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    Chooseactivity = table.Column<string>(maxLength: 100, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Fixedvalue = table.Column<string>(maxLength: 100, nullable: true),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Other = table.Column<string>(maxLength: 100, nullable: false),
                    OtherTitle = table.Column<string>(maxLength: 100, nullable: true),
                    Planninglevel = table.Column<string>(maxLength: 100, nullable: false),
                    Planningreadout = table.Column<string>(maxLength: 100, nullable: true),
                    Planningspecific = table.Column<string>(maxLength: 100, nullable: false),
                    QP = table.Column<string>(maxLength: 100, nullable: true),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Startselecteditem = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningselectProperties", x => x.PlanningselectId);
                    table.ForeignKey(
                        name: "FK_PlanningselectProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanningselectProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanningselectProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SetActionValueProperties",
                columns: table => new
                {
                    SetActionValueId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    AllowanceName = table.Column<string>(maxLength: 50, nullable: true),
                    CancelcurrentQP = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    InfoColumnName = table.Column<string>(maxLength: 50, nullable: true),
                    LayoutRefId = table.Column<int>(nullable: false),
                    NextQpName = table.Column<string>(maxLength: 50, nullable: true),
                    NextViewName = table.Column<string>(maxLength: 50, nullable: true),
                    QuestionRefId = table.Column<int>(nullable: false),
                    SelectAllowance = table.Column<string>(maxLength: 50, nullable: true),
                    SelectInfoColumn = table.Column<string>(maxLength: 50, nullable: true),
                    SelectQP = table.Column<string>(maxLength: 50, nullable: true),
                    SelectView = table.Column<string>(maxLength: 50, nullable: true),
                    SetReset = table.Column<string>(maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    Value = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SetActionValueProperties", x => x.SetActionValueId);
                    table.ForeignKey(
                        name: "FK_SetActionValueProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SetActionValueProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SetActionValueProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TextMessageProperties",
                columns: table => new
                {
                    TextMessageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Messagecontent = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    QuestionRefId = table.Column<int>(nullable: false),
                    SendMethod = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TextMessageProperties", x => x.TextMessageId);
                    table.ForeignKey(
                        name: "FK_TextMessageProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TextMessageProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TextMessageProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VariableListProperties",
                columns: table => new
                {
                    VariableListId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Other = table.Column<string>(maxLength: 50, nullable: true),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    VarListID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VariableListProperties", x => x.VariableListId);
                    table.ForeignKey(
                        name: "FK_VariableListProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VariableListProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VariableListProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AddChioceProperties",
                columns: table => new
                {
                    AddChioceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChioceRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Endrecursiveloop = table.Column<int>(nullable: false),
                    PlanningfeedbackCode = table.Column<string>(maxLength: 100, nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AddChioceProperties", x => x.AddChioceId);
                    table.ForeignKey(
                        name: "FK_AddChioceProperties_ChioceProperties_ChioceRefId",
                        column: x => x.ChioceRefId,
                        principalTable: "ChioceProperties",
                        principalColumn: "ChioceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_CustomerRefId",
                table: "ActivitiesRegistration",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitiesRegistration_LayoutRefId",
                table: "ActivitiesRegistration",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_AddChioceProperties_ChioceRefId",
                table: "AddChioceProperties",
                column: "ChioceRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Allowances_AllowancesRefId",
                table: "Allowances",
                column: "AllowancesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Allowances_CustomerRefId",
                table: "Allowances",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Allowances_LayoutRefId",
                table: "Allowances",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_AllowancesMaster_OBCTypeRefId",
                table: "AllowancesMaster",
                column: "OBCTypeRefId");

            migrationBuilder.CreateIndex(
                name: "IX_CheckoffProperties_ActivitiesRefId",
                table: "CheckoffProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_CheckoffProperties_LayoutRefId",
                table: "CheckoffProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_CheckoffProperties_QuestionRefId",
                table: "CheckoffProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ChioceProperties_ActivitiesRefId",
                table: "ChioceProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ChioceProperties_LayoutRefId",
                table: "ChioceProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ChioceProperties_QuestionRefId",
                table: "ChioceProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerNotes_CustomerRefId",
                table: "CustomerNotes",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumetScanProperties_ActivitiesRefId",
                table: "DocumetScanProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumetScanProperties_LayoutRefId",
                table: "DocumetScanProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumetScanProperties_QuestionRefId",
                table: "DocumetScanProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_EntryProperties_ActivitiesRefId",
                table: "EntryProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_EntryProperties_LayoutRefId",
                table: "EntryProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_EntryProperties_QuestionRefId",
                table: "EntryProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_InfoColumns_CustomerRefId",
                table: "InfoColumns",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_InfoColumns_ICM_Column_Ref_No",
                table: "InfoColumns",
                column: "ICM_Column_Ref_No");

            migrationBuilder.CreateIndex(
                name: "IX_InfomessageProperties_ActivitiesRefId",
                table: "InfomessageProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_InfomessageProperties_LayoutRefId",
                table: "InfomessageProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_InfomessageProperties_QuestionRefId",
                table: "InfomessageProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Integrator_CustomerRefId",
                table: "Integrator",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangActReg_ActivitiesRefId",
                table: "LangActReg",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangActReg_LayoutRefId",
                table: "LangActReg",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangAllowances_AllowancesRefId",
                table: "LangAllowances",
                column: "AllowancesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangAllowances_LayoutRefId",
                table: "LangAllowances",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangDriverInput_LanguageMasterRefId",
                table: "LangDriverInput",
                column: "LanguageMasterRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangDriverInput_LayoutRefId",
                table: "LangDriverInput",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangInfoColumns_CustomerRefId",
                table: "LangInfoColumns",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangInfoColumns_ICRefId",
                table: "LangInfoColumns",
                column: "ICRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangInfoColumns_LayoutRefId",
                table: "LangInfoColumns",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangMessages_LanguageMasterRefId",
                table: "LangMessages",
                column: "LanguageMasterRefId");

            migrationBuilder.CreateIndex(
                name: "IX_LangMessages_LayoutRefId",
                table: "LangMessages",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Layout_CustomerRefId",
                table: "Layout",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_OBCType_CustomerRefId",
                table: "OBCType",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_OBCType_LayoutRefId",
                table: "OBCType",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_OBCType_OBCTypeRefId",
                table: "OBCType",
                column: "OBCTypeRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningselectProperties_ActivitiesRefId",
                table: "PlanningselectProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningselectProperties_LayoutRefId",
                table: "PlanningselectProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningselectProperties_QuestionRefId",
                table: "PlanningselectProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningSpecification_CustomerRefId",
                table: "PlanningSpecification",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningSpecification_LayoutRefId",
                table: "PlanningSpecification",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningSpecification_PlanningSpecificationRefId",
                table: "PlanningSpecification",
                column: "PlanningSpecificationRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningSpecificationMaster_PlanningTypeMasterRefId",
                table: "PlanningSpecificationMaster",
                column: "PlanningTypeMasterRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningType_CustomerRefId",
                table: "PlanningType",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningType_LayoutRefId",
                table: "PlanningType",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningType_PlanningTypeRefId",
                table: "PlanningType",
                column: "PlanningTypeRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyDetails_PropertyRefId",
                table: "PropertyDetails",
                column: "PropertyRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyMaster_SubToolboxRefId",
                table: "PropertyMaster",
                column: "SubToolboxRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_ActivitiesRefId",
                table: "Questions",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_LayoutRefId",
                table: "Questions",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_RecentActivity_CustomerRefId",
                table: "RecentActivity",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_RecentActivity_LayoutRefId",
                table: "RecentActivity",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ScreenLayout_ActivitiesRegistrationRefId",
                table: "ScreenLayout",
                column: "ActivitiesRegistrationRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ScreenLayout_CustomerRefId",
                table: "ScreenLayout",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ScreenLayout_LayoutRefId",
                table: "ScreenLayout",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_SetActionValueProperties_ActivitiesRefId",
                table: "SetActionValueProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_SetActionValueProperties_LayoutRefId",
                table: "SetActionValueProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_SetActionValueProperties_QuestionRefId",
                table: "SetActionValueProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_TextMessageProperties_ActivitiesRefId",
                table: "TextMessageProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_TextMessageProperties_LayoutRefId",
                table: "TextMessageProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_TextMessageProperties_QuestionRefId",
                table: "TextMessageProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_ToolBoxSubItem_ToolboxRefId",
                table: "ToolBoxSubItem",
                column: "ToolboxRefId");

            migrationBuilder.CreateIndex(
                name: "IX_TransportTypes_CustomerRefId",
                table: "TransportTypes",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_TransportTypes_LayoutRefId",
                table: "TransportTypes",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_UIContentMaster_PageRefId",
                table: "UIContentMaster",
                column: "PageRefId");

            migrationBuilder.CreateIndex(
                name: "IX_UsedLanguages_CustomerRefId",
                table: "UsedLanguages",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_UsedLanguages_LanguageRefId",
                table: "UsedLanguages",
                column: "LanguageRefId");

            migrationBuilder.CreateIndex(
                name: "IX_UsedLanguages_LayoutRefId",
                table: "UsedLanguages",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLanguage_UserRefId",
                table: "UserLanguage",
                column: "UserRefId");

            migrationBuilder.CreateIndex(
                name: "IX_VariableListProperties_ActivitiesRefId",
                table: "VariableListProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_VariableListProperties_LayoutRefId",
                table: "VariableListProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_VariableListProperties_QuestionRefId",
                table: "VariableListProperties",
                column: "QuestionRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AddChioceProperties");

            migrationBuilder.DropTable(
                name: "CheckoffProperties");

            migrationBuilder.DropTable(
                name: "CustomerNotes");

            migrationBuilder.DropTable(
                name: "DefaultActivities");

            migrationBuilder.DropTable(
                name: "DocumetScanProperties");

            migrationBuilder.DropTable(
                name: "EntryProperties");

            migrationBuilder.DropTable(
                name: "InfomessageProperties");

            migrationBuilder.DropTable(
                name: "Integrator");

            migrationBuilder.DropTable(
                name: "InterruptibleBy");

            migrationBuilder.DropTable(
                name: "LangActReg");

            migrationBuilder.DropTable(
                name: "LangAllowances");

            migrationBuilder.DropTable(
                name: "LangDriverInput");

            migrationBuilder.DropTable(
                name: "LangInfoColumns");

            migrationBuilder.DropTable(
                name: "LangMessages");

            migrationBuilder.DropTable(
                name: "OBCType");

            migrationBuilder.DropTable(
                name: "Planning");

            migrationBuilder.DropTable(
                name: "PlanningOverview");

            migrationBuilder.DropTable(
                name: "PlanningselectProperties");

            migrationBuilder.DropTable(
                name: "PlanningSpecification");

            migrationBuilder.DropTable(
                name: "PlanningType");

            migrationBuilder.DropTable(
                name: "PropertyDetails");

            migrationBuilder.DropTable(
                name: "PTO");

            migrationBuilder.DropTable(
                name: "RecentActivity");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "ScreenLayout");

            migrationBuilder.DropTable(
                name: "SetActionValueProperties");

            migrationBuilder.DropTable(
                name: "Specific");

            migrationBuilder.DropTable(
                name: "StatusMaster");

            migrationBuilder.DropTable(
                name: "TextMessageProperties");

            migrationBuilder.DropTable(
                name: "ToolBoxIcons");

            migrationBuilder.DropTable(
                name: "TransportTypes");

            migrationBuilder.DropTable(
                name: "UIContentLanguage");

            migrationBuilder.DropTable(
                name: "UIContentMaster");

            migrationBuilder.DropTable(
                name: "UsedLanguages");

            migrationBuilder.DropTable(
                name: "UserLanguage");

            migrationBuilder.DropTable(
                name: "VariableListProperties");

            migrationBuilder.DropTable(
                name: "WorkingCode");

            migrationBuilder.DropTable(
                name: "ChioceProperties");

            migrationBuilder.DropTable(
                name: "Allowances");

            migrationBuilder.DropTable(
                name: "InfoColumns");

            migrationBuilder.DropTable(
                name: "PlanningSpecificationMaster");

            migrationBuilder.DropTable(
                name: "PropertyMaster");

            migrationBuilder.DropTable(
                name: "PageMaster");

            migrationBuilder.DropTable(
                name: "LanguageMaster");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "AllowancesMaster");

            migrationBuilder.DropTable(
                name: "InfoColumnMaster");

            migrationBuilder.DropTable(
                name: "PlanningTypeMaster");

            migrationBuilder.DropTable(
                name: "ToolBoxSubItem");

            migrationBuilder.DropTable(
                name: "ActivitiesRegistration");

            migrationBuilder.DropTable(
                name: "OBCTypeMaster");

            migrationBuilder.DropTable(
                name: "MainToolBox");

            migrationBuilder.DropTable(
                name: "Layout");

            migrationBuilder.DropTable(
                name: "Customers");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class PerfectLanguagemaster2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Languagevalue",
                table: "LanguageMaster");

            migrationBuilder.AddColumn<string>(
                name: "LanguageCode",
                table: "LanguageMaster",
                maxLength: 30,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LanguageCode",
                table: "LanguageMaster");

            migrationBuilder.AddColumn<int>(
                name: "Languagevalue",
                table: "LanguageMaster",
                maxLength: 30,
                nullable: false,
                defaultValue: 0);
        }
    }
}

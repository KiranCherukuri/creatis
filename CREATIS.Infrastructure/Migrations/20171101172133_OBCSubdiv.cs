﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class OBCSubdiv : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AllowancesMaster",
                columns: table => new
                {
                    AllowancesMasterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllowancesMasterDescription = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    OBCTypeRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllowancesMaster", x => x.AllowancesMasterId);
                    table.ForeignKey(
                        name: "FK_AllowancesMaster_OBCTypeMaster_OBCTypeRefId",
                        column: x => x.OBCTypeRefId,
                        principalTable: "OBCTypeMaster",
                        principalColumn: "OBCTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanningTypeMaster",
                columns: table => new
                {
                    PlanningTypeMasterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    PlanningTypeMasterDescription = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningTypeMaster", x => x.PlanningTypeMasterId);
                });

            migrationBuilder.CreateTable(
                name: "Allowances",
                columns: table => new
                {
                    AllowancesId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllowancesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Allowances", x => x.AllowancesId);
                    table.ForeignKey(
                        name: "FK_Allowances_AllowancesMaster_AllowancesRefId",
                        column: x => x.AllowancesRefId,
                        principalTable: "AllowancesMaster",
                        principalColumn: "AllowancesMasterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Allowances_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_Allowances_Layout_LayoutRefId",
                    column: x => x.LayoutRefId,
                    principalTable: "Layout",
                    principalColumn: "LayoutId");
                        //onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanningSpecificationMaster",
                columns: table => new
                {
                    PlanningSpecificationMasterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IS_Active = table.Column<int>(nullable: false),
                    PlanningSpecificationMasterDescription = table.Column<string>(maxLength: 50, nullable: false),
                    PlanningTypeMasterRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningSpecificationMaster", x => x.PlanningSpecificationMasterId);
                    table.ForeignKey(
                        name: "FK_PlanningSpecificationMaster_PlanningTypeMaster_PlanningTypeMasterRefId",
                        column: x => x.PlanningTypeMasterRefId,
                        principalTable: "PlanningTypeMaster",
                        principalColumn: "PlanningTypeMasterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanningType",
                columns: table => new
                {
                    PlanningTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    PlanningTypeRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningType", x => x.PlanningTypeId);
                    table.ForeignKey(
                        name: "FK_PlanningType_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_PlanningType_Layout_LayoutRefId",
                    column: x => x.LayoutRefId,
                    principalTable: "Layout",
                    principalColumn: "LayoutId");
                        //onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanningType_PlanningTypeMaster_PlanningTypeRefId",
                        column: x => x.PlanningTypeRefId,
                        principalTable: "PlanningTypeMaster",
                        principalColumn: "PlanningTypeMasterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanningSpecification",
                columns: table => new
                {
                    PlanningSpecificationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    PlanningSpecificationRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanningSpecification", x => x.PlanningSpecificationId);
                    table.ForeignKey(
                        name: "FK_PlanningSpecification_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                table.ForeignKey(
                    name: "FK_PlanningSpecification_Layout_LayoutRefId",
                    column: x => x.LayoutRefId,
                    principalTable: "Layout",
                    principalColumn: "LayoutId");
                        //onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanningSpecification_PlanningSpecificationMaster_PlanningSpecificationRefId",
                        column: x => x.PlanningSpecificationRefId,
                        principalTable: "PlanningSpecificationMaster",
                        principalColumn: "PlanningSpecificationMasterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Allowances_AllowancesRefId",
                table: "Allowances",
                column: "AllowancesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Allowances_CustomerRefId",
                table: "Allowances",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Allowances_LayoutRefId",
                table: "Allowances",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_AllowancesMaster_OBCTypeRefId",
                table: "AllowancesMaster",
                column: "OBCTypeRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningSpecification_CustomerRefId",
                table: "PlanningSpecification",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningSpecification_LayoutRefId",
                table: "PlanningSpecification",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningSpecification_PlanningSpecificationRefId",
                table: "PlanningSpecification",
                column: "PlanningSpecificationRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningSpecificationMaster_PlanningTypeMasterRefId",
                table: "PlanningSpecificationMaster",
                column: "PlanningTypeMasterRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningType_CustomerRefId",
                table: "PlanningType",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningType_LayoutRefId",
                table: "PlanningType",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanningType_PlanningTypeRefId",
                table: "PlanningType",
                column: "PlanningTypeRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Allowances");

            migrationBuilder.DropTable(
                name: "PlanningSpecification");

            migrationBuilder.DropTable(
                name: "PlanningType");

            migrationBuilder.DropTable(
                name: "AllowancesMaster");

            migrationBuilder.DropTable(
                name: "PlanningSpecificationMaster");

            migrationBuilder.DropTable(
                name: "PlanningTypeMaster");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class TransportTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TransportTypes",
                columns: table => new
                {
                    TransportTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CustomerRefId = table.Column<int>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    TransportTypes = table.Column<string>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransportTypes", x => x.TransportTypeId);
                    table.ForeignKey(
                        name: "FK_TransportTypes_Customers_CustomerRefId",
                        column: x => x.CustomerRefId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransportTypes_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                //onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TransportTypes_CustomerRefId",
                table: "TransportTypes",
                column: "CustomerRefId");

            migrationBuilder.CreateIndex(
                name: "IX_TransportTypes_LayoutRefId",
                table: "TransportTypes",
                column: "LayoutRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransportTypes");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class QuestionproChoice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PropertyMaster",
                columns: table => new
                {
                    PropertyId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDefault = table.Column<int>(nullable: false),
                    PropertyName = table.Column<string>(nullable: true),
                    PropertyStatus = table.Column<int>(nullable: false),
                    SubToolboxRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyMaster", x => x.PropertyId);
                    table.ForeignKey(
                        name: "FK_PropertyMaster_ToolBoxSubItem_SubToolboxRefId",
                        column: x => x.SubToolboxRefId,
                        principalTable: "ToolBoxSubItem",
                        principalColumn: "SubToolboxId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PropertyDetails",
                columns: table => new
                {
                    PropertyDetailsId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDefault = table.Column<int>(nullable: false),
                    PropertyDetailsStatus = table.Column<int>(nullable: false),
                    PropertyRefId = table.Column<int>(nullable: false),
                    PropertyValue = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyDetails", x => x.PropertyDetailsId);
                    table.ForeignKey(
                        name: "FK_PropertyDetails_PropertyMaster_PropertyRefId",
                        column: x => x.PropertyRefId,
                        principalTable: "PropertyMaster",
                        principalColumn: "PropertyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PropertyDetails_PropertyRefId",
                table: "PropertyDetails",
                column: "PropertyRefId");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyMaster_SubToolboxRefId",
                table: "PropertyMaster",
                column: "SubToolboxRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PropertyDetails");

            migrationBuilder.DropTable(
                name: "PropertyMaster");
        }
    }
}

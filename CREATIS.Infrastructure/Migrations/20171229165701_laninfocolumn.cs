﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class laninfocolumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ICRefId",
                table: "LangInfoColumns",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_LangInfoColumns_ICRefId",
                table: "LangInfoColumns",
                column: "ICRefId");

            migrationBuilder.AddForeignKey(
                name: "FK_LangInfoColumns_InfoColumns_ICRefId",
                table: "LangInfoColumns",
                column: "ICRefId",
                principalTable: "InfoColumns",
                principalColumn: "IC_ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LangInfoColumns_InfoColumns_ICRefId",
                table: "LangInfoColumns");

            migrationBuilder.DropIndex(
                name: "IX_LangInfoColumns_ICRefId",
                table: "LangInfoColumns");

            migrationBuilder.DropColumn(
                name: "ICRefId",
                table: "LangInfoColumns");
        }
    }
}

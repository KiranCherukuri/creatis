﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class QuestionProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    QuestionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    ParentId = table.Column<int>(nullable: false),
                    TransporttypeRefId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.QuestionId);
                    table.ForeignKey(
                        name: "FK_Questions_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Questions_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                });

            migrationBuilder.CreateTable(
                name: "EntryProperties",
                columns: table => new
                {
                    EntryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ExactCharacter = table.Column<int>(nullable: false),
                    FeedbackType = table.Column<string>(maxLength: 100, nullable: false),
                    LayoutRefId = table.Column<int>(nullable: false),
                    MaskText = table.Column<string>(maxLength: 100, nullable: false),
                    MinimumofCharacter = table.Column<int>(nullable: false),
                    NotNull = table.Column<int>(nullable: false),
                    Planningfeedback = table.Column<string>(maxLength: 100, nullable: false),
                    PlanningfeedbackCode = table.Column<string>(maxLength: 100, nullable: false),
                    Predefined = table.Column<string>(maxLength: 100, nullable: false),
                    Propose = table.Column<int>(nullable: false),
                    QuestionRefId = table.Column<int>(nullable: false),
                    Regex = table.Column<string>(maxLength: 100, nullable: false),
                    Save = table.Column<int>(nullable: false),
                    Size = table.Column<string>(maxLength: 100, nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    Zeronotallowed = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EntryProperties", x => x.EntryId);
                    table.ForeignKey(
                        name: "FK_EntryProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EntryProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                    table.ForeignKey(
                        name: "FK_EntryProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_EntryProperties_ActivitiesRefId",
                table: "EntryProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_EntryProperties_LayoutRefId",
                table: "EntryProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_EntryProperties_QuestionRefId",
                table: "EntryProperties",
                column: "QuestionRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_ActivitiesRefId",
                table: "Questions",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_LayoutRefId",
                table: "Questions",
                column: "LayoutRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EntryProperties");

            migrationBuilder.DropTable(
                name: "Questions");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CREATIS.Infrastructure.Migrations
{
    public partial class setaction1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SetActionValueProperties",
                columns: table => new
                {
                    SetActionValueId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivitiesRefId = table.Column<int>(nullable: false),
                    AllowanceName = table.Column<string>(maxLength: 50, nullable: true),
                    CancelcurrentQP = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    InfoColumnName = table.Column<string>(maxLength: 50, nullable: true),
                    LayoutRefId = table.Column<int>(nullable: false),
                    NextQpName = table.Column<string>(maxLength: 50, nullable: true),
                    NextViewName = table.Column<string>(maxLength: 50, nullable: true),
                    QuestionRefId = table.Column<int>(nullable: false),
                    SelectAllowance = table.Column<string>(maxLength: 50, nullable: true),
                    SelectInfoColumn = table.Column<string>(maxLength: 50, nullable: true),
                    SelectQP = table.Column<string>(maxLength: 50, nullable: true),
                    SelectView = table.Column<string>(maxLength: 50, nullable: true),
                    SetReset = table.Column<string>(maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    Value = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SetActionValueProperties", x => x.SetActionValueId);
                    table.ForeignKey(
                        name: "FK_SetActionValueProperties_ActivitiesRegistration_ActivitiesRefId",
                        column: x => x.ActivitiesRefId,
                        principalTable: "ActivitiesRegistration",
                        principalColumn: "ActivitiesRegistrationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SetActionValueProperties_Layout_LayoutRefId",
                        column: x => x.LayoutRefId,
                        principalTable: "Layout",
                        principalColumn: "LayoutId");
                    table.ForeignKey(
                        name: "FK_SetActionValueProperties_Questions_QuestionRefId",
                        column: x => x.QuestionRefId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_SetActionValueProperties_ActivitiesRefId",
                table: "SetActionValueProperties",
                column: "ActivitiesRefId");

            migrationBuilder.CreateIndex(
                name: "IX_SetActionValueProperties_LayoutRefId",
                table: "SetActionValueProperties",
                column: "LayoutRefId");

            migrationBuilder.CreateIndex(
                name: "IX_SetActionValueProperties_QuestionRefId",
                table: "SetActionValueProperties",
                column: "QuestionRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SetActionValueProperties");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CREATIS.ApplicationCore.Modals;
using CREATIS.Infrastructure.IRepository;

namespace CREATIS.Infrastructure.Repository
{
    public class LoginRepository : Utility, ILoginRepository
    {
        public Creatis_Context dbconext;
        public Users GetUserList(string domainname)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Users.Where(usersdata => usersdata.DomainId == domainname).Single();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<int> GetRolesList(int userId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var rolesList = context.RoleMapping.Where(x => x.UserId == userId && x.Status == 1).Select(x => x.RoleId ?? 0).Distinct().ToList();
                    return rolesList;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<string> GetRolesAccessList(List<string> roleList)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var rolesList = context.Roles.Where(x => roleList.Contains(x.RoleName)).Select(x => x.RoleId).Distinct().ToList();
                    var rolesAccessList = context.RoleAccessDetail.Where(x => rolesList.Contains(x.RolesRefId) && x.IsActive == true).Select(x => x.ModulesDetail.ModulesDetailCode).Distinct().ToList();
                    return rolesAccessList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<string> GetLayoutStatusAccessList(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var layoutStatusId = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.LayoutStatusRefId).FirstOrDefault();
                    var layoutStatusAccessList = context.LayoutStatusAccessDetail.Where(x => x.LayoutStatusRefId == layoutStatusId && x.IsActive == true).Select(x => x.ModulesDetail.ModulesDetailCode).Distinct().ToList();
                    return layoutStatusAccessList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<string> GetRoleWithLayoutStatusAccessList(int layoutId, List<string> roleList)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var rolesList = context.Roles.Where(x => roleList.Contains(x.RoleName)).Select(x => x.RoleId).Distinct().ToList();
                    var layoutStatusId = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.LayoutStatusRefId).FirstOrDefault();
                    var roleWithLayoutStatusAccessList = context.RoleWithLayoutStatusAccessDetail.Where(x => rolesList.Contains(x.RolesRefId) && x.LayoutStatusRefId == layoutStatusId && x.IsActive == true).Select(x => x.ModulesDetail.ModulesDetailCode).Distinct().ToList();
                    return roleWithLayoutStatusAccessList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string SaveRecentActivity(RecentActivity recentModel)
        {
            using (var context = new Creatis_Context())
            {
                try
                {
                    RecentActivity recentActivity = new RecentActivity();
                    recentActivity = context.RecentActivity.Where(x => x.LayoutRefId == recentModel.LayoutRefId && x.UpdatedBy == recentModel.UpdatedBy).FirstOrDefault();
                    if (recentActivity != null)
                    {
                        recentActivity.Status = 1;
                        recentActivity.UpdatedOn = recentModel.UpdatedOn;
                        context.SaveChanges();
                    }
                    else
                    {
                        recentActivity = new RecentActivity();

                        recentActivity.LayoutRefId = recentModel.LayoutRefId;
                        recentActivity.Status = 1;
                        recentActivity.CreatedBy = recentModel.CreatedBy;
                        recentActivity.CreatedOn = recentModel.CreatedOn;
                        recentActivity.UpdatedBy = recentModel.UpdatedBy;
                        recentActivity.UpdatedOn = recentModel.UpdatedOn;

                        context.RecentActivity.Add(recentActivity);
                        context.SaveChanges();
                    }
                    return "Success";
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }

            }
        }
        public int GetLoginCustomer(string company, int customerId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var customerRefId = context.Customers.Where(x => x.MyTrxCompanyId == company).Select(x => x.CustomerId).FirstOrDefault();
                    if (customerId > 0 && customerRefId != customerId && context.Customers.Where(x => x.CustomerId == customerId && x.CustomerSystemTypeRefId == (int)EnumCustomerSystemType.Standard).Any())
                        customerRefId = customerId;

                    return customerRefId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public List<int> GetCustomerLayoutList(string company, int customerId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<int> customerLayoutList = new List<int>();

                    var customerRefId = context.Customers.Where(x => x.MyTrxCompanyId == company).Select(x => x.CustomerId).FirstOrDefault();
                    if (customerRefId != customerId && customerId > 0)
                        customerLayoutList = context.Layout.Where(x => x.CustomerRefId == customerId && x.LayoutStatusRefId == (int)EnumLayoutStatus.Finished).Select(x => x.LayoutId).ToList();
                    else
                        customerLayoutList = context.Layout.Where(x => x.CustomerRefId == customerId).Select(x => x.LayoutId).ToList();

                    return customerLayoutList;
                }
            }
            catch (Exception ex)
            {
                return new List<int>();
            }
        }
        public int GetLayoutCustomerId(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var customerId = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.CustomerRefId).FirstOrDefault();
                    return customerId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public bool IsCustomerLogin(List<string> roleList)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (roleList.Contains("customer") || roleList.Contains("Customer"))
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.Infrastructure.IRepository;
using CREATIS.Infrastructure;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CREATIS.Infrastructure.Repository
{
    public class TranslatorRepository : Utility, ITranslatorRepository
    {
        public Creatis_Context dbconext;

        #region Venkat
        public LanguageMaster GetLanguageById(int Id)
        {
            using (var context = new Creatis_Context())
            {
                var Language = context.LanguageMaster.Where(x => x.LanguageId == Id).FirstOrDefault();

                return Language;
            }


        }
        public List<UsedLanguages> GetUsedLanguage(int LayoutId)
        {

            try
            {
                using (var context = new Creatis_Context())
                {

                    var usedLanguage = context.UsedLanguages.Where(x => x.LayoutRefId == LayoutId && x.IsActive).OrderByDescending(x => x.IsDefault).ToList();

                    return usedLanguage;
                }

            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public List<UsedLanguages> GetUsedLanguageByLayoutIdandLangId(int layoutId, int langId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var usedLanguage = context.UsedLanguages.Where(x => x.LayoutRefId == layoutId && x.LanguageRefId == langId).ToList();

                    return usedLanguage;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LanguageMaster> GetAllLanguage()
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var usedLanguage = context.LanguageMaster.ToList();

                    return usedLanguage;
                }

            }
            catch (Exception ex)
            {
                return null;
            }


        }
        public UsedLanguages SaveUsedLanguage(UsedLanguages UsedLanguages)
        {
            using (var context = new Creatis_Context())
            {
                var model = new UsedLanguages();

                model.LayoutRefId = UsedLanguages.LayoutRefId;
                model.LanguageRefId = UsedLanguages.LanguageRefId;
                model.CustomerRefId = context.Layout.Where(x => x.LayoutId == UsedLanguages.LayoutRefId).FirstOrDefault().CustomerRefId;
                model.CreatedBy = UsedLanguages.CreatedBy;
                model.CreatedOn = DateTime.Now;
                context.Add(model);
                context.SaveChanges();
                return UsedLanguages;

            }
        }
        public List<AllowancesMaster> GetAllowancesMaster(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from ALM in context.AllowancesMaster
                                join AL in context.Allowances
                                on ALM.AllowancesMasterId equals AL.AllowancesRefId
                                where AL.LayoutRefId == Layout_ID
                                select new AllowancesMaster()
                                {
                                    AllowancesMasterId = ALM.AllowancesMasterId,
                                    AllowancesMasterDescription = ALM.AllowancesMasterDescription,
                                    AllowancesCode = ALM.AllowancesCode,
                                    OBCTypeRefId = ALM.OBCTypeRefId,
                                    IS_Active = ALM.IS_Active,
                                    CreatedBy = ALM.CreatedBy,
                                    CreatedOn = ALM.CreatedOn,
                                    UpdatedBy = ALM.UpdatedBy,
                                    UpdatedOn = ALM.UpdatedOn
                                }).ToList();

                    List<AllowancesMaster> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion Venkat

        #region Selvam Thangaraj

        public List<LangDetail> GetLangDetail(int TableId, int LayoutId, int Id, bool IsCopyLayout, bool isLoadHtml)
        {
            List<LangDetail> langresult = new List<LangDetail>();
            using (var context = new Creatis_Context())
            {
                int CustomerId = 0;

                if (TableId == (int)LangRefTable.InfoColumns)
                    CustomerId = context.Layout.Where(x => x.LayoutId == LayoutId).Select(x => x.CustomerRefId).FirstOrDefault();
                else if (Id > 0)
                {
                    var CopyLayoutId = context.LangDetail.Where(x => x.Id == Id).Select(x => x.LayoutRefId).FirstOrDefault();
                    IsCopyLayout = context.Layout.Where(x => x.LayoutId == CopyLayoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                }
                else if (LayoutId > 0)
                {
                    CustomerId = context.Layout.Where(x => x.LayoutId == LayoutId).Select(x => x.CustomerRefId).FirstOrDefault();
                }

                SqlParameter layoutParam = new SqlParameter("@LayoutRefId", LayoutId);
                SqlParameter customerParam = new SqlParameter("@CustomerRefId", CustomerId);
                SqlParameter isLoadHtmlParam = new SqlParameter("@IsLoadHtml", (isLoadHtml ? 1 : 0));

                context.Database.SetCommandTimeout(180);
                var translationList = context.Translations.FromSql("GetTranslations @LayoutRefId,@CustomerRefId,@IsLoadHtml", layoutParam, customerParam, isLoadHtmlParam).Where(x => ((TableId == 999999 || TableId == 9999999) ? (1 == 1) : (Id == 0 ? (x.LangRefTableId == TableId) : (1 == 1))) && (Id == 0 ? (1 == 1) : (x.LangRefDetailId == Id))).ToList();

                langresult = translationList.Select(x => new LangDetail()
                {
                    QuestionRefId = x.QuestionRefId,
                    IsDefaultTranslation = x.IsDefaultTranslation,
                    Id = x.LangDetailId ?? 0,
                    LangRefDetailId = x.LangRefDetailId,
                    Arabic = x.Arabic,
                    Bulgarian = x.Bulgarian,
                    Croatian = x.Croatian,
                    Czech = x.Czech,
                    Danish = x.Danish,
                    Dutch = x.Dutch,
                    English = x.English,
                    Estonian = x.Estonian,
                    Finnish = x.Finnish,
                    French = x.French,
                    German = x.German,
                    Greek = x.Greek,
                    Hungarian = x.Hungarian,
                    Italian = x.Italian,
                    Latvian = x.Latvian,
                    Lithuanian = x.Lithuanian,
                    Macedonian = x.Macedonian,
                    Norwegian = x.Norwegian,
                    Polish = x.Polish,
                    Portuguese = x.Portuguese,
                    Romanian = x.Romanian,
                    Russian = x.Russian,
                    Slovak = x.Slovak,
                    Slovene = x.Slovene,
                    Spanish = x.Spanish,
                    Swedish = x.Swedish,
                    Turkish = x.Turkish,
                    Ukrainian = x.Ukrainian,
                    Belarusian = x.Belarusian,
                    LangRefColTableId = x.LangRefColTableId,
                    RefId = x.RefId ?? 0,
                    LayoutRefId = x.LayoutRefId,
                    CustomerRefId = x.CustomerRefId ?? 0,
                    LangRefTableId = x.LangRefTableId ?? 0,
                    ColumnName = x.ColumnName,
                    TableName = x.TableName,
                    PropertiesName = x.PropertiesName,
                    PropertiesPath = x.PropertiesPath,
                    IsModificationColorArabic = (IsCopyLayout && x.Arabic != null) ? GetCopyLayoutColor(x.IsModificationArabic) : "",
                    IsModificationColorBulgarian = (IsCopyLayout && x.Bulgarian != null) ? GetCopyLayoutColor(x.IsModificationBulgarian) : "",
                    IsModificationColorCroatian = (IsCopyLayout && x.Croatian != null) ? GetCopyLayoutColor(x.IsModificationCroatian) : "",
                    IsModificationColorCzech = (IsCopyLayout && x.Czech != null) ? GetCopyLayoutColor(x.IsModificationCzech) : "",
                    IsModificationColorDanish = (IsCopyLayout && x.Danish != null) ? GetCopyLayoutColor(x.IsModificationDanish) : "",
                    IsModificationColorDutch = (IsCopyLayout && x.Dutch != null) ? GetCopyLayoutColor(x.IsModificationDutch) : "",
                    IsModificationColorEnglish = (IsCopyLayout && x.English != null) ? GetCopyLayoutColor(x.IsModificationEnglish) : "",
                    IsModificationColorEstonian = (IsCopyLayout && x.Estonian != null) ? GetCopyLayoutColor(x.IsModificationEstonian) : "",
                    IsModificationColorFinnish = (IsCopyLayout && x.Finnish != null) ? GetCopyLayoutColor(x.IsModificationFinnish) : "",
                    IsModificationColorFrench = (IsCopyLayout && x.French != null) ? GetCopyLayoutColor(x.IsModificationFrench) : "",
                    IsModificationColorGerman = (IsCopyLayout && x.German != null) ? GetCopyLayoutColor(x.IsModificationGerman) : "",
                    IsModificationColorGreek = (IsCopyLayout && x.Greek != null) ? GetCopyLayoutColor(x.IsModificationGreek) : "",
                    IsModificationColorHungarian = (IsCopyLayout && x.Hungarian != null) ? GetCopyLayoutColor(x.IsModificationHungarian) : "",
                    IsModificationColorItalian = (IsCopyLayout && x.Italian != null) ? GetCopyLayoutColor(x.IsModificationItalian) : "",
                    IsModificationColorLatvian = (IsCopyLayout && x.Latvian != null) ? GetCopyLayoutColor(x.IsModificationLatvian) : "",
                    IsModificationColorLithuanian = (IsCopyLayout && x.Lithuanian != null) ? GetCopyLayoutColor(x.IsModificationLithuanian) : "",
                    IsModificationColorMacedonian = (IsCopyLayout && x.Macedonian != null) ? GetCopyLayoutColor(x.IsModificationMacedonian) : "",
                    IsModificationColorNorwegian = (IsCopyLayout && x.Norwegian != null) ? GetCopyLayoutColor(x.IsModificationNorwegian) : "",
                    IsModificationColorPolish = (IsCopyLayout && x.Polish != null) ? GetCopyLayoutColor(x.IsModificationPolish) : "",
                    IsModificationColorPortuguese = (IsCopyLayout && x.Portuguese != null) ? GetCopyLayoutColor(x.IsModificationPortuguese) : "",
                    IsModificationColorRomanian = (IsCopyLayout && x.Romanian != null) ? GetCopyLayoutColor(x.IsModificationRomanian) : "",
                    IsModificationColorRussian = (IsCopyLayout && x.Russian != null) ? GetCopyLayoutColor(x.IsModificationRussian) : "",
                    IsModificationColorSlovak = (IsCopyLayout && x.Slovak != null) ? GetCopyLayoutColor(x.IsModificationSlovak) : "",
                    IsModificationColorSlovene = (IsCopyLayout && x.Slovene != null) ? GetCopyLayoutColor(x.IsModificationSlovene) : "",
                    IsModificationColorSpanish = (IsCopyLayout && x.Spanish != null) ? GetCopyLayoutColor(x.IsModificationSpanish) : "",
                    IsModificationColorSwedish = (IsCopyLayout && x.Swedish != null) ? GetCopyLayoutColor(x.IsModificationSwedish) : "",
                    IsModificationColorTurkish = (IsCopyLayout && x.Turkish != null) ? GetCopyLayoutColor(x.IsModificationTurkish) : "",
                    IsModificationColorUkrainian = (IsCopyLayout && x.Ukrainian != null) ? GetCopyLayoutColor(x.IsModificationUkrainian) : "",
                    IsModificationColorBelarusian = (IsCopyLayout && x.Belarusian != null) ? GetCopyLayoutColor(x.IsModificationBelarusian) : ""

                }).Where(x => TableId == 999999 ? (x.LangRefColTableId != (int)LangRefColTable.InfoMessageContentDetail_MessageContent && x.LangRefColTableId != (int)LangRefColTable.Questions_Name) : (1 == 1)).ToList();
            }
            return langresult;
        }
        public List<LangDetail> GetLangActivities(int TableId, int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var ActivitiesList = context.ActivitiesRegistration.Where(x => x.WorkingCodeRefId != "Registration").Select(x => (int)x.ActivitiesRegistrationId).ToList();
                    var IsCopyLayout = context.Layout.Where(x => x.LayoutId == LayoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var langActReg = GetLangDetail(TableId, LayoutId, 0, IsCopyLayout, false);
                    return langActReg.Where(x => ActivitiesList.Contains(x.RefId)).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LangDetail> GetLangRegistration(int TableId, int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var ActivitiesList = context.ActivitiesRegistration.Where(x => x.WorkingCodeRefId == "Registration").Select(x => (int)x.ActivitiesRegistrationId).ToList();
                    var IsCopyLayout = context.Layout.Where(x => x.LayoutId == LayoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var langActReg = GetLangDetail(TableId, LayoutId, 0, IsCopyLayout, false);
                    return langActReg.Where(x => ActivitiesList.Contains(x.RefId)).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LangDetail> GetAllLangDetail(int TableId, int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var IsCopyLayout = context.Layout.Where(x => x.LayoutId == LayoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var langdetail = GetLangDetail(TableId, LayoutId, 0, IsCopyLayout, false);

                    return langdetail;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public LangDetail GetLangInfo(LangDetail LangInfo, int TableId, string ColumnName)
        {
            using (var context = new Creatis_Context())
            {
                if (TableId == (int)LangRefTable.ActivitiesRegistration && ColumnName == "ActivitiesRegistrationName")
                {
                    LangInfo.LangText = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == LangInfo.RefId).Select(x => x.ActivitiesRegistrationName).FirstOrDefault();
                    LangInfo.WorkingCode = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == LangInfo.RefId).Select(x => x.WorkingCodeRefId).FirstOrDefault();
                }
                if (TableId == (int)LangRefTable.InfoColumns && ColumnName == "IC_Column_Name")
                    LangInfo.LangText = context.InfoColumn.Where(x => x.IC_ID == LangInfo.RefId).Select(x => x.IC_Column_Name).FirstOrDefault();
                if (TableId == (int)LangRefTable.Allowances && ColumnName == "AllowancesMasterDescription")
                    LangInfo.LangText = context.Allowances.Where(x => x.AllowancesId == LangInfo.RefId).Select(x => x.AllowancesMaster.AllowancesMasterDescription).FirstOrDefault();
                if (TableId == (int)LangRefTable.ChioceProperties && ColumnName == "Title")
                    LangInfo.LangText = context.ChioceProperties.Where(x => x.ChioceId == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.AddChioceProperties && ColumnName == "Title")
                    LangInfo.LangText = context.AddChioceProperties.Where(x => x.AddChioceId == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.EntryProperties && ColumnName == "Title")
                    LangInfo.LangText = context.EntryProperties.Where(x => x.EntryId == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.EntryProperties && ColumnName == "MaskText")
                    LangInfo.LangText = context.EntryProperties.Where(x => x.EntryId == LangInfo.RefId).Select(x => x.MaskText).FirstOrDefault();
                if (TableId == (int)LangRefTable.InfomessageProperties && ColumnName == "Title")
                    LangInfo.LangText = context.InfomessageProperties.Where(x => x.InfomessageId == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.InfoMessageContentDetail && ColumnName == "MessageContent")
                    LangInfo.LangText = context.InfomessageProperties.Where(x => x.InfomessageId == LangInfo.RefId).Select(x => x.Messagecontent).FirstOrDefault();
                if (TableId == (int)LangRefTable.TextMessageProperties && ColumnName == "Messagecontent")
                    LangInfo.LangText = context.TextMessageProperties.Where(x => x.TextMessageId == LangInfo.RefId).Select(x => x.Messagecontent).FirstOrDefault();

                if (TableId == (int)LangRefTable.VariableListProperties && ColumnName == "Title")
                    LangInfo.LangText = context.VarListProperties.Where(x => x.VarListPropertiesId == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.VariableListProperties && ColumnName == "OtherTitle")
                    LangInfo.LangText = context.VarListProperties.Where(x => x.VarListPropertiesId == LangInfo.RefId).Select(x => x.OtherTitle).FirstOrDefault();
                if (TableId == (int)LangRefTable.VariableListProperties && ColumnName == "MaskText")
                    LangInfo.LangText = context.VarListProperties.Where(x => x.VarListPropertiesId == LangInfo.RefId).Select(x => x.OtherMaskText).FirstOrDefault();

                if (TableId == (int)LangRefTable.PlanningselectProperties && ColumnName == "Title")
                    LangInfo.LangText = context.PlanningselectProperties.Where(x => x.PlanningselectId == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.PlanningselectProperties && ColumnName == "OtherTitle")
                    LangInfo.LangText = context.PlanningselectProperties.Where(x => x.PlanningselectId == LangInfo.RefId).Select(x => x.OtherTitle).FirstOrDefault();
                if (TableId == (int)LangRefTable.PlanningselectProperties && ColumnName == "Fixedvalue")
                    LangInfo.LangText = context.PlanningselectProperties.Where(x => x.PlanningselectId == LangInfo.RefId).Select(x => x.Fixedvalue).FirstOrDefault();
                if (TableId == (int)LangRefTable.DocumetScanProperties && ColumnName == "Title")
                    LangInfo.LangText = context.DocumetScanProperties.Where(x => x.DocumetScanId == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.TransportTypeProperties && ColumnName == "Title")
                    LangInfo.LangText = context.TransportTypeProperties.Where(x => x.TransportTypeId == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.TransportTypePropertiesDetail && ColumnName == "Title")
                    LangInfo.LangText = context.TransportTypePropertiesDetail.Where(x => x.TransportTypeDetailId == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.TrailerProperties && ColumnName == "Title")
                    LangInfo.LangText = context.TrailerProperties.Where(x => x.TrailerId == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.TrailerProperties && ColumnName == "MaskText")
                    LangInfo.LangText = context.TrailerProperties.Where(x => x.TrailerId == LangInfo.RefId).Select(x => x.MaskText).FirstOrDefault();
                if (TableId == (int)LangRefTable.SetActionValueProperties && ColumnName == "Value")
                    LangInfo.LangText = context.SetActionValueProperties.Where(x => x.SetActionValueId == LangInfo.RefId).Select(x => x.Value).FirstOrDefault();
                if (TableId == (int)LangRefTable.SetActionValueProperties && ColumnName == "InfoColumnFixedText")
                    LangInfo.LangText = context.SetActionValueProperties.Where(x => x.SetActionValueId == LangInfo.RefId).Select(x => x.InfoCoumnFixedText).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "Name")
                    LangInfo.LangText = context.Questions.Where(x => x.QuestionId == LangInfo.RefId).Select(x => x.Name).FirstOrDefault();
                if (TableId == (int)LangRefTable.ExtraInfoProperties && ColumnName == "Title")
                    LangInfo.LangText = context.ExtraInfoProperties.Where(x => x.ExtraInfoPropertiesID == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnPlaceLevel")
                    LangInfo.LangText = context.ExtraInfoProperties.Where(x => x.ExtraInfoPropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonPlacelevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnJobLevel")
                    LangInfo.LangText = context.ExtraInfoProperties.Where(x => x.ExtraInfoPropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonJoblevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnProductLevel")
                    LangInfo.LangText = context.ExtraInfoProperties.Where(x => x.ExtraInfoPropertiesID == LangInfo.RefId).Select(x => x.Alternativetextonproductlevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.PalletsProperties && ColumnName == "Title")
                    LangInfo.LangText = context.PalletsProperties.Where(x => x.PalletsPropertiesID == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnPlaceLevel")
                    LangInfo.LangText = context.PalletsProperties.Where(x => x.PalletsPropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonPlacelevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnJobLevel")
                    LangInfo.LangText = context.PalletsProperties.Where(x => x.PalletsPropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonJoblevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnProductLevel")
                    LangInfo.LangText = context.PalletsProperties.Where(x => x.PalletsPropertiesID == LangInfo.RefId).Select(x => x.Alternativetextonproductlevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.ProblemsProperties && ColumnName == "Title")
                    LangInfo.LangText = context.ProblemsProperties.Where(x => x.ProblemsPropertiesID == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnPlaceLevel")
                    LangInfo.LangText = context.ProblemsProperties.Where(x => x.ProblemsPropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonPlacelevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnJobLevel")
                    LangInfo.LangText = context.ProblemsProperties.Where(x => x.ProblemsPropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonJoblevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnProductLevel")
                    LangInfo.LangText = context.ProblemsProperties.Where(x => x.ProblemsPropertiesID == LangInfo.RefId).Select(x => x.Alternativetextonproductlevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.SignaturesProperties && ColumnName == "Title")
                    LangInfo.LangText = context.SignaturesProperties.Where(x => x.SignaturesPropertiesID == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnPlaceLevel")
                    LangInfo.LangText = context.SignaturesProperties.Where(x => x.SignaturesPropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonPlacelevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnJobLevel")
                    LangInfo.LangText = context.SignaturesProperties.Where(x => x.SignaturesPropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonJoblevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnProductLevel")
                    LangInfo.LangText = context.SignaturesProperties.Where(x => x.SignaturesPropertiesID == LangInfo.RefId).Select(x => x.Alternativetextonproductlevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.PictureProperties && ColumnName == "Title")
                    LangInfo.LangText = context.PictureProperties.Where(x => x.PicturePropertiesID == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnPlaceLevel")
                    LangInfo.LangText = context.PictureProperties.Where(x => x.PicturePropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonPlacelevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnJobLevel")
                    LangInfo.LangText = context.PictureProperties.Where(x => x.PicturePropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonJoblevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnProductLevel")
                    LangInfo.LangText = context.PictureProperties.Where(x => x.PicturePropertiesID == LangInfo.RefId).Select(x => x.Alternativetextonproductlevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.DocumentScannerProperties && ColumnName == "Title")
                    LangInfo.LangText = context.DocumentScannerProperties.Where(x => x.DocumentScannerPropertiesID == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnPlaceLevel")
                    LangInfo.LangText = context.DocumentScannerProperties.Where(x => x.DocumentScannerPropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonPlacelevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnJobLevel")
                    LangInfo.LangText = context.DocumentScannerProperties.Where(x => x.DocumentScannerPropertiesID == LangInfo.RefId).Select(x => x.AlternativetextonJoblevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "AlternativeTextOnProductLevel")
                    LangInfo.LangText = context.DocumentScannerProperties.Where(x => x.DocumentScannerPropertiesID == LangInfo.RefId).Select(x => x.Alternativetextonproductlevel).FirstOrDefault();
                if (TableId == (int)LangRefTable.ExtraInfo && ColumnName == "Title")
                    LangInfo.LangText = context.ExtraInfo.Where(x => x.ExtraInfoID == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "MaskText")
                    LangInfo.LangText = context.ExtraInfo.Where(x => x.ExtraInfoID == LangInfo.RefId).Select(x => x.MaskText).FirstOrDefault();
                if (TableId == (int)LangRefTable.Problems && ColumnName == "Title")
                    LangInfo.LangText = context.Problems.Where(x => x.ProblemsID == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.Questions && ColumnName == "MaskText")
                    LangInfo.LangText = context.Problems.Where(x => x.ProblemsID == LangInfo.RefId).Select(x => x.MaskText).FirstOrDefault();
                if (TableId == (int)LangRefTable.Pallets && ColumnName == "Title")
                    LangInfo.LangText = context.Pallets.Where(x => x.PalletsID == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
                if (TableId == (int)LangRefTable.DocumentScanner && ColumnName == "Title")
                    LangInfo.LangText = context.DocumentScanner.Where(x => x.DocumentScannerID == LangInfo.RefId).Select(x => x.Title).FirstOrDefault();
            }

            return LangInfo;
        }
        public LangDetail UpdateLangDetail(LangDetail LangInfo)
        {
            using (var context = new Creatis_Context())
            {
                LangDetail returnmodal = new LangDetail();
                var langrefdetail = context.LangRefDetail.Where(x => x.Id == LangInfo.Id).FirstOrDefault();
                var IsCustomerSpecific = false;
                var IsCopyLayout = false;
                var isUseExistRecord = false;
                var isAddNewRecord = false;
                var isModification = 99999;
                var upgradeLangDetailId = 0;

                LangInfo.SelectedLanguage = LangInfo.SelectedLanguage.Replace("&nbsp;", " ");

                LangDetail addNewRecord = new LangDetail();

                addNewRecord.Arabic = "";
                addNewRecord.Bulgarian = "";
                addNewRecord.Croatian = "";
                addNewRecord.Czech = "";
                addNewRecord.Danish = "";
                addNewRecord.Dutch = "";
                addNewRecord.English = "";
                addNewRecord.Estonian = "";
                addNewRecord.Finnish = "";
                addNewRecord.French = "";
                addNewRecord.German = "";
                addNewRecord.Greek = "";
                addNewRecord.Hungarian = "";
                addNewRecord.Italian = "";
                addNewRecord.Latvian = "";
                addNewRecord.Lithuanian = "";
                addNewRecord.Macedonian = "";
                addNewRecord.Norwegian = "";
                addNewRecord.Polish = "";
                addNewRecord.Portuguese = "";
                addNewRecord.Romanian = "";
                addNewRecord.Russian = "";
                addNewRecord.Slovak = "";
                addNewRecord.Slovene = "";
                addNewRecord.Spanish = "";
                addNewRecord.Swedish = "";
                addNewRecord.Turkish = "";
                addNewRecord.Ukrainian = "";
                addNewRecord.Belarusian = "";
                addNewRecord.IsModificationEnglish = isModification;
                addNewRecord.IsModificationArabic = isModification;
                addNewRecord.IsModificationBulgarian = isModification;
                addNewRecord.IsModificationCroatian = isModification;
                addNewRecord.IsModificationCzech = isModification;
                addNewRecord.IsModificationDanish = isModification;
                addNewRecord.IsModificationDutch = isModification;
                addNewRecord.IsModificationEstonian = isModification;
                addNewRecord.IsModificationFinnish = isModification;
                addNewRecord.IsModificationFrench = isModification;
                addNewRecord.IsModificationGerman = isModification;
                addNewRecord.IsModificationGreek = isModification;
                addNewRecord.IsModificationHungarian = isModification;
                addNewRecord.IsModificationItalian = isModification;
                addNewRecord.IsModificationLatvian = isModification;
                addNewRecord.IsModificationLithuanian = isModification;
                addNewRecord.IsModificationMacedonian = isModification;
                addNewRecord.IsModificationNorwegian = isModification;
                addNewRecord.IsModificationPolish = isModification;
                addNewRecord.IsModificationPortuguese = isModification;
                addNewRecord.IsModificationRomanian = isModification;
                addNewRecord.IsModificationRussian = isModification;
                addNewRecord.IsModificationSlovak = isModification;
                addNewRecord.IsModificationSlovene = isModification;
                addNewRecord.IsModificationSpanish = isModification;
                addNewRecord.IsModificationSwedish = isModification;
                addNewRecord.IsModificationTurkish = isModification;
                addNewRecord.IsModificationUkrainian = isModification;
                addNewRecord.IsModificationBelarusian = isModification;
                var LangText = "";
                int IsModification = 0;

                if (langrefdetail != null)
                {
                    var langdetail = context.LangDetail.Where(x => x.Id == langrefdetail.LangDetailId).FirstOrDefault();
                    if (langdetail != null)
                    {
                        IsCopyLayout = context.Layout.Where(x => x.LayoutId == langrefdetail.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();

                        if (LanguagesEnum.English.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.English != null && langdetail.English != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.English != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.English, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.English, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.English = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationEnglish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.English = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationEnglish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationEnglish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.English = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Arabic.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Arabic != null && langdetail.Arabic != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Arabic != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Arabic, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Arabic, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Arabic = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationArabic = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Arabic = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationArabic == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationArabic = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Arabic = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Bulgarian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Bulgarian != null && langdetail.Bulgarian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Bulgarian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Bulgarian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Bulgarian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Bulgarian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationBulgarian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Bulgarian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationBulgarian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationBulgarian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Bulgarian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Croatian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Croatian != null && langdetail.Croatian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Croatian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Croatian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Croatian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Croatian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationCroatian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Croatian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationCroatian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationCroatian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Croatian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Czech.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Czech != null && langdetail.Czech != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Czech != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Czech, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Czech, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Czech = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationCzech = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Czech = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationCzech == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationCzech = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Czech = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Danish.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Danish != null && langdetail.Danish != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Danish != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Danish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Danish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Danish = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationDanish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Danish = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationDanish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationDanish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Danish = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Dutch.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Dutch != null && langdetail.Dutch != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Dutch != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Dutch, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Dutch, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Dutch = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationDutch = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Dutch = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationDutch == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationDutch = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Dutch = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Estonian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Estonian != null && langdetail.Estonian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Estonian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Estonian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Estonian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Estonian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationEstonian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Estonian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationEstonian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationEstonian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Estonian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Finnish.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Finnish != null && langdetail.Finnish != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Finnish != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Finnish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Finnish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Finnish = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationFinnish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Finnish = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationFinnish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationFinnish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Finnish = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.French.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.French != null && langdetail.French != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.French != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.French, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.French, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.French = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationFrench = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.French = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationFrench == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationFrench = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.French = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.German.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.German != null && langdetail.German != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.German != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.German, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.German, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.German = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationGerman = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.German = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationGerman == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationGerman = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.German = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Greek.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Greek != null && langdetail.Greek != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Greek != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Greek, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Greek, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Greek = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationGreek = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Greek = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationGreek == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationGreek = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Greek = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Hungarian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Hungarian != null && langdetail.Hungarian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Hungarian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Hungarian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Hungarian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Hungarian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationHungarian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Hungarian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationHungarian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationHungarian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Hungarian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Italian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Italian != null && langdetail.Italian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Italian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Italian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Italian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Italian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationItalian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Italian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationItalian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationItalian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Italian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Latvian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Latvian != null && langdetail.Latvian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Latvian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Latvian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Latvian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Latvian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationLatvian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Latvian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationLatvian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationLatvian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Latvian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Lithuanian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Lithuanian != null && langdetail.Lithuanian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Lithuanian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Lithuanian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Lithuanian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Lithuanian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationLithuanian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Lithuanian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationLithuanian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationLithuanian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Lithuanian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Macedonian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Macedonian != null && langdetail.Macedonian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            if (langdetail.IsDefaultTranslation == 0 && langdetail.Macedonian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Macedonian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Macedonian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Macedonian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationMacedonian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Macedonian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationMacedonian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationMacedonian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Macedonian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Norwegian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Norwegian != null && langdetail.Norwegian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Norwegian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Norwegian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Norwegian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Norwegian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationNorwegian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Norwegian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationNorwegian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationNorwegian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Norwegian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Polish.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Polish != null && langdetail.Polish != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Polish != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Polish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Polish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Polish = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationPolish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Polish = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationPolish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationPolish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Polish = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Portuguese.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Portuguese != null && langdetail.Portuguese != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Portuguese != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Portuguese, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Portuguese, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Portuguese = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationPortuguese = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Portuguese = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationPortuguese == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationPortuguese = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Portuguese = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Romanian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Romanian != null && langdetail.Romanian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Romanian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Romanian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Romanian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Romanian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationRomanian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Romanian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationRomanian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationRomanian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Romanian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Russian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Russian != null && langdetail.Russian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Russian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Russian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Russian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Russian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationRussian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Russian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationRussian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationRussian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Russian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Slovak.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Slovak != null && langdetail.Slovak != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Slovak != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Slovak, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Slovak, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Slovak = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationSlovak = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Slovak = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationSlovak == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationSlovak = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Slovak = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Slovene.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Slovene != null && langdetail.Slovene != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Slovene != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Slovene, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Slovene, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Slovene = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationSlovene = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Slovene = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationSlovene == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationSlovene = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Slovene = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Spanish.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Spanish != null && langdetail.Spanish != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Spanish != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Spanish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Spanish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Spanish = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationSpanish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Spanish = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationSpanish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationSpanish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Spanish = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Swedish.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Swedish != null && langdetail.Swedish != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Swedish != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Swedish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Swedish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Swedish = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationSwedish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Swedish = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationSwedish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationSwedish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Swedish = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Turkish.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Turkish != null && langdetail.Turkish != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Turkish != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Turkish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Turkish, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Turkish = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationTurkish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Turkish = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationTurkish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationTurkish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Turkish = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Ukrainian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Ukrainian != null && langdetail.Ukrainian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Ukrainian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Ukrainian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Ukrainian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Ukrainian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationUkrainian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Ukrainian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationUkrainian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationUkrainian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Ukrainian = LangInfo.SelectedLanguage;
                        }
                        else if (LanguagesEnum.Belarusian.ToString() == LangInfo.Language)
                        {
                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Belarusian != null && langdetail.Belarusian != LangInfo.SelectedLanguage)
                            {
                                LangText = LangInfo.SelectedLanguage;
                                if (IsCopyLayout)
                                    IsModification = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && langdetail.Belarusian != LangInfo.SelectedLanguage)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Belarusian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Belarusian, LangInfo.SelectedLanguage, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else
                                    {
                                        addNewRecord.Belarusian = LangInfo.SelectedLanguage;
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationBelarusian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Belarusian = LangInfo.SelectedLanguage;
                                    if (IsCopyLayout && langdetail.IsModificationBelarusian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationBelarusian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Belarusian = LangInfo.SelectedLanguage;
                        }

                        if (isAddNewRecord)
                        {

                            LangDetail addnewlangdetail = new LangDetail();

                            addnewlangdetail.Arabic = addNewRecord.Arabic.Length > 0 ? addNewRecord.Arabic : langdetail.Arabic;
                            addnewlangdetail.Bulgarian = addNewRecord.Bulgarian.Length > 0 ? addNewRecord.Bulgarian : langdetail.Bulgarian;
                            addnewlangdetail.Croatian = addNewRecord.Croatian.Length > 0 ? addNewRecord.Croatian : langdetail.Croatian;
                            addnewlangdetail.Czech = addNewRecord.Czech.Length > 0 ? addNewRecord.Czech : langdetail.Czech;
                            addnewlangdetail.Danish = addNewRecord.Danish.Length > 0 ? addNewRecord.Danish : langdetail.Danish;
                            addnewlangdetail.Dutch = addNewRecord.Dutch.Length > 0 ? addNewRecord.Dutch : langdetail.Dutch;
                            addnewlangdetail.English = addNewRecord.English.Length > 0 ? addNewRecord.English : langdetail.English;
                            addnewlangdetail.Estonian = addNewRecord.Estonian.Length > 0 ? addNewRecord.Estonian : langdetail.Estonian;
                            addnewlangdetail.Finnish = addNewRecord.Finnish.Length > 0 ? addNewRecord.Finnish : langdetail.Finnish;
                            addnewlangdetail.French = addNewRecord.French.Length > 0 ? addNewRecord.French : langdetail.French;
                            addnewlangdetail.German = addNewRecord.German.Length > 0 ? addNewRecord.German : langdetail.German;
                            addnewlangdetail.Greek = addNewRecord.Greek.Length > 0 ? addNewRecord.Greek : langdetail.Greek;
                            addnewlangdetail.Hungarian = addNewRecord.Hungarian.Length > 0 ? addNewRecord.Hungarian : langdetail.Hungarian;
                            addnewlangdetail.Italian = addNewRecord.Italian.Length > 0 ? addNewRecord.Italian : langdetail.Italian;
                            addnewlangdetail.Latvian = addNewRecord.Latvian.Length > 0 ? addNewRecord.Latvian : langdetail.Latvian;
                            addnewlangdetail.Lithuanian = addNewRecord.Lithuanian.Length > 0 ? addNewRecord.Lithuanian : langdetail.Lithuanian;
                            addnewlangdetail.Macedonian = addNewRecord.Macedonian.Length > 0 ? addNewRecord.Macedonian : langdetail.Macedonian;
                            addnewlangdetail.Norwegian = addNewRecord.Norwegian.Length > 0 ? addNewRecord.Norwegian : langdetail.Norwegian;
                            addnewlangdetail.Polish = addNewRecord.Polish.Length > 0 ? addNewRecord.Polish : langdetail.Polish;
                            addnewlangdetail.Portuguese = addNewRecord.Portuguese.Length > 0 ? addNewRecord.Portuguese : langdetail.Portuguese;
                            addnewlangdetail.Romanian = addNewRecord.Romanian.Length > 0 ? addNewRecord.Romanian : langdetail.Romanian;
                            addnewlangdetail.Russian = addNewRecord.Russian.Length > 0 ? addNewRecord.Russian : langdetail.Russian;
                            addnewlangdetail.Slovak = addNewRecord.Slovak.Length > 0 ? addNewRecord.Slovak : langdetail.Slovak;
                            addnewlangdetail.Slovene = addNewRecord.Slovene.Length > 0 ? addNewRecord.Slovene : langdetail.Slovene;
                            addnewlangdetail.Spanish = addNewRecord.Spanish.Length > 0 ? addNewRecord.Spanish : langdetail.Spanish;
                            addnewlangdetail.Swedish = addNewRecord.Swedish.Length > 0 ? addNewRecord.Swedish : langdetail.Swedish;
                            addnewlangdetail.Turkish = addNewRecord.Turkish.Length > 0 ? addNewRecord.Turkish : langdetail.Turkish;
                            addnewlangdetail.Ukrainian = addNewRecord.Ukrainian.Length > 0 ? addNewRecord.Ukrainian : langdetail.Ukrainian;
                            addnewlangdetail.Belarusian = addNewRecord.Belarusian.Length > 0 ? addNewRecord.Belarusian : langdetail.Belarusian;
                            addnewlangdetail.CreatedBy = LangInfo.UpdatedBy;
                            addnewlangdetail.CreatedOn = DateTime.Now;
                            addnewlangdetail.UpdatedBy = LangInfo.UpdatedBy;
                            addnewlangdetail.UpdatedOn = DateTime.Now;
                            addnewlangdetail.IsDefaultTranslation = 0;
                            addnewlangdetail.CustomerRefId = LangInfo.CustomerRefId;
                            addnewlangdetail.LangRefColTableId = LangInfo.LangRefColTableId;
                            addnewlangdetail.RefId = langrefdetail.RefId;
                            addnewlangdetail.LayoutRefId = LangInfo.LayoutRefId;

                            if (IsCopyLayout)
                            {
                                if (LanguagesEnum.English.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationEnglish = addNewRecord.IsModificationEnglish != 99999 ? addNewRecord.IsModificationEnglish : langdetail.IsModificationEnglish;
                                else if (LanguagesEnum.Arabic.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationArabic = addNewRecord.IsModificationArabic != 99999 ? addNewRecord.IsModificationArabic : langdetail.IsModificationArabic;
                                else if (LanguagesEnum.Bulgarian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationBulgarian = addNewRecord.IsModificationBulgarian != 99999 ? addNewRecord.IsModificationBulgarian : langdetail.IsModificationBulgarian;
                                else if (LanguagesEnum.Croatian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationCroatian = addNewRecord.IsModificationCroatian != 99999 ? addNewRecord.IsModificationCroatian : langdetail.IsModificationCroatian;
                                else if (LanguagesEnum.Czech.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationCzech = addNewRecord.IsModificationCzech != 99999 ? addNewRecord.IsModificationCzech : langdetail.IsModificationCzech;
                                else if (LanguagesEnum.Danish.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationDanish = addNewRecord.IsModificationDanish != 99999 ? addNewRecord.IsModificationDanish : langdetail.IsModificationDanish;
                                else if (LanguagesEnum.Dutch.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationDutch = addNewRecord.IsModificationDutch != 99999 ? addNewRecord.IsModificationDutch : langdetail.IsModificationDutch;
                                else if (LanguagesEnum.Estonian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationEstonian = addNewRecord.IsModificationEstonian != 99999 ? addNewRecord.IsModificationEstonian : langdetail.IsModificationEstonian;
                                else if (LanguagesEnum.Finnish.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationFinnish = addNewRecord.IsModificationFinnish != 99999 ? addNewRecord.IsModificationFinnish : langdetail.IsModificationFinnish;
                                else if (LanguagesEnum.French.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationFrench = addNewRecord.IsModificationFrench != 99999 ? addNewRecord.IsModificationFrench : langdetail.IsModificationFrench;
                                else if (LanguagesEnum.German.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationGerman = addNewRecord.IsModificationGerman != 99999 ? addNewRecord.IsModificationGerman : langdetail.IsModificationGerman;
                                else if (LanguagesEnum.Greek.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationGreek = addNewRecord.IsModificationGreek != 99999 ? addNewRecord.IsModificationGreek : langdetail.IsModificationGreek;
                                else if (LanguagesEnum.Hungarian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationHungarian = addNewRecord.IsModificationHungarian != 99999 ? addNewRecord.IsModificationHungarian : langdetail.IsModificationHungarian;
                                else if (LanguagesEnum.Italian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationItalian = addNewRecord.IsModificationItalian != 99999 ? addNewRecord.IsModificationItalian : langdetail.IsModificationItalian;
                                else if (LanguagesEnum.Latvian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationLatvian = addNewRecord.IsModificationLatvian != 99999 ? addNewRecord.IsModificationLatvian : langdetail.IsModificationLatvian;
                                else if (LanguagesEnum.Lithuanian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationLithuanian = addNewRecord.IsModificationLithuanian != 99999 ? addNewRecord.IsModificationLithuanian : langdetail.IsModificationLithuanian;
                                else if (LanguagesEnum.Macedonian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationMacedonian = addNewRecord.IsModificationMacedonian != 99999 ? addNewRecord.IsModificationMacedonian : langdetail.IsModificationMacedonian;
                                else if (LanguagesEnum.Norwegian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationNorwegian = addNewRecord.IsModificationNorwegian != 99999 ? addNewRecord.IsModificationNorwegian : langdetail.IsModificationNorwegian;
                                else if (LanguagesEnum.Polish.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationPolish = addNewRecord.IsModificationPolish != 99999 ? addNewRecord.IsModificationPolish : langdetail.IsModificationPolish;
                                else if (LanguagesEnum.Portuguese.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationPortuguese = addNewRecord.IsModificationPortuguese != 99999 ? addNewRecord.IsModificationPortuguese : langdetail.IsModificationPortuguese;
                                else if (LanguagesEnum.Romanian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationRomanian = addNewRecord.IsModificationRomanian != 99999 ? addNewRecord.IsModificationRomanian : langdetail.IsModificationRomanian;
                                else if (LanguagesEnum.Russian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationRussian = addNewRecord.IsModificationRussian != 99999 ? addNewRecord.IsModificationRussian : langdetail.IsModificationRussian;
                                else if (LanguagesEnum.Slovak.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationSlovak = addNewRecord.IsModificationSlovak != 99999 ? addNewRecord.IsModificationSlovak : langdetail.IsModificationSlovak;
                                else if (LanguagesEnum.Slovene.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationSlovene = addNewRecord.IsModificationSlovene != 99999 ? addNewRecord.IsModificationSlovene : langdetail.IsModificationSlovene;
                                else if (LanguagesEnum.Spanish.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationSpanish = addNewRecord.IsModificationSpanish != 99999 ? addNewRecord.IsModificationSpanish : langdetail.IsModificationSpanish;
                                else if (LanguagesEnum.Swedish.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationSwedish = addNewRecord.IsModificationSwedish != 99999 ? addNewRecord.IsModificationSwedish : langdetail.IsModificationSwedish;
                                else if (LanguagesEnum.Turkish.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationTurkish = addNewRecord.IsModificationTurkish != 99999 ? addNewRecord.IsModificationTurkish : langdetail.IsModificationTurkish;
                                else if (LanguagesEnum.Ukrainian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationUkrainian = addNewRecord.IsModificationUkrainian != 99999 ? addNewRecord.IsModificationUkrainian : langdetail.IsModificationUkrainian;
                                else if (LanguagesEnum.Belarusian.ToString() == LangInfo.Language)
                                    addnewlangdetail.IsModificationBelarusian = addNewRecord.IsModificationBelarusian != 99999 ? addNewRecord.IsModificationBelarusian : langdetail.IsModificationBelarusian;
                            }

                            context.LangDetail.Add(addnewlangdetail);
                            context.SaveChanges();

                            langrefdetail.LangDetailId = addnewlangdetail.Id;
                            langrefdetail.UpdatedBy = LangInfo.UpdatedBy;
                            langrefdetail.UpdatedOn = DateTime.Now;
                            context.SaveChanges();

                            return addnewlangdetail;
                        }
                        else if (IsCustomerSpecific)
                        {
                            LangDetail cusspecificlangdetail = new LangDetail();

                            cusspecificlangdetail.Arabic = (langdetail.IsDefaultTranslation == 1 && langdetail.Arabic != null && langdetail.Arabic != LangInfo.SelectedLanguage) ? LangText : langdetail.Arabic;
                            cusspecificlangdetail.Bulgarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Bulgarian != null && langdetail.Bulgarian != LangInfo.SelectedLanguage) ? LangText : langdetail.Bulgarian;
                            cusspecificlangdetail.Croatian = (langdetail.IsDefaultTranslation == 1 && langdetail.Croatian != null && langdetail.Croatian != LangInfo.SelectedLanguage) ? LangText : langdetail.Croatian;
                            cusspecificlangdetail.Czech = (langdetail.IsDefaultTranslation == 1 && langdetail.Czech != null && langdetail.Czech != LangInfo.SelectedLanguage) ? LangText : langdetail.Czech;
                            cusspecificlangdetail.Danish = (langdetail.IsDefaultTranslation == 1 && langdetail.Danish != null && langdetail.Danish != LangInfo.SelectedLanguage) ? LangText : langdetail.Danish;
                            cusspecificlangdetail.Dutch = (langdetail.IsDefaultTranslation == 1 && langdetail.Dutch != null && langdetail.Dutch != LangInfo.SelectedLanguage) ? LangText : langdetail.Dutch;
                            cusspecificlangdetail.English = (langdetail.IsDefaultTranslation == 1 && langdetail.English != null && langdetail.English != LangInfo.SelectedLanguage) ? LangText : langdetail.English;
                            cusspecificlangdetail.Estonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Estonian != null && langdetail.Estonian != LangInfo.SelectedLanguage) ? LangText : langdetail.Estonian;
                            cusspecificlangdetail.Finnish = (langdetail.IsDefaultTranslation == 1 && langdetail.Finnish != null && langdetail.Finnish != LangInfo.SelectedLanguage) ? LangText : langdetail.Finnish;
                            cusspecificlangdetail.French = (langdetail.IsDefaultTranslation == 1 && langdetail.French != null && langdetail.French != LangInfo.SelectedLanguage) ? LangText : langdetail.French;
                            cusspecificlangdetail.German = (langdetail.IsDefaultTranslation == 1 && langdetail.German != null && langdetail.German != LangInfo.SelectedLanguage) ? LangText : langdetail.German;
                            cusspecificlangdetail.Greek = (langdetail.IsDefaultTranslation == 1 && langdetail.Greek != null && langdetail.Greek != LangInfo.SelectedLanguage) ? LangText : langdetail.Greek;
                            cusspecificlangdetail.Hungarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Hungarian != null && langdetail.Hungarian != LangInfo.SelectedLanguage) ? LangText : langdetail.Hungarian;
                            cusspecificlangdetail.Italian = (langdetail.IsDefaultTranslation == 1 && langdetail.Italian != null && langdetail.Italian != LangInfo.SelectedLanguage) ? LangText : langdetail.Italian;
                            cusspecificlangdetail.Latvian = (langdetail.IsDefaultTranslation == 1 && langdetail.Latvian != null && langdetail.Latvian != LangInfo.SelectedLanguage) ? LangText : langdetail.Latvian;
                            cusspecificlangdetail.Lithuanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Lithuanian != null && langdetail.Lithuanian != LangInfo.SelectedLanguage) ? LangText : langdetail.Lithuanian;
                            cusspecificlangdetail.Macedonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Macedonian != null && langdetail.Macedonian != LangInfo.SelectedLanguage) ? LangText : langdetail.Macedonian;
                            cusspecificlangdetail.Norwegian = (langdetail.IsDefaultTranslation == 1 && langdetail.Norwegian != null && langdetail.Norwegian != LangInfo.SelectedLanguage) ? LangText : langdetail.Norwegian;
                            cusspecificlangdetail.Polish = (langdetail.IsDefaultTranslation == 1 && langdetail.Polish != null && langdetail.Polish != LangInfo.SelectedLanguage) ? LangText : langdetail.Polish;
                            cusspecificlangdetail.Portuguese = (langdetail.IsDefaultTranslation == 1 && langdetail.Portuguese != null && langdetail.Portuguese != LangInfo.SelectedLanguage) ? LangText : langdetail.Portuguese;
                            cusspecificlangdetail.Romanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Romanian != null && langdetail.Romanian != LangInfo.SelectedLanguage) ? LangText : langdetail.Romanian;
                            cusspecificlangdetail.Russian = (langdetail.IsDefaultTranslation == 1 && langdetail.Russian != null && langdetail.Russian != LangInfo.SelectedLanguage) ? LangText : langdetail.Russian;
                            cusspecificlangdetail.Slovak = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovak != null && langdetail.Slovak != LangInfo.SelectedLanguage) ? LangText : langdetail.Slovak;
                            cusspecificlangdetail.Slovene = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovene != null && langdetail.Slovene != LangInfo.SelectedLanguage) ? LangText : langdetail.Slovene;
                            cusspecificlangdetail.Spanish = (langdetail.IsDefaultTranslation == 1 && langdetail.Spanish != null && langdetail.Spanish != LangInfo.SelectedLanguage) ? LangText : langdetail.Spanish;
                            cusspecificlangdetail.Swedish = (langdetail.IsDefaultTranslation == 1 && langdetail.Swedish != null && langdetail.Swedish != LangInfo.SelectedLanguage) ? LangText : langdetail.Swedish;
                            cusspecificlangdetail.Turkish = (langdetail.IsDefaultTranslation == 1 && langdetail.Turkish != null && langdetail.Turkish != LangInfo.SelectedLanguage) ? LangText : langdetail.Turkish;
                            cusspecificlangdetail.Ukrainian = (langdetail.IsDefaultTranslation == 1 && langdetail.Ukrainian != null && langdetail.Ukrainian != LangInfo.SelectedLanguage) ? LangText : langdetail.Ukrainian;
                            cusspecificlangdetail.Belarusian = (langdetail.IsDefaultTranslation == 1 && langdetail.Belarusian != null && langdetail.Belarusian != LangInfo.SelectedLanguage) ? LangText : langdetail.Belarusian;
                            cusspecificlangdetail.CreatedBy = LangInfo.UpdatedBy;
                            cusspecificlangdetail.CreatedOn = DateTime.Now;
                            cusspecificlangdetail.UpdatedBy = LangInfo.UpdatedBy;
                            cusspecificlangdetail.UpdatedOn = DateTime.Now;
                            cusspecificlangdetail.IsDefaultTranslation = 0;
                            cusspecificlangdetail.CustomerRefId = LangInfo.CustomerRefId;
                            cusspecificlangdetail.RefId = langrefdetail.RefId;
                            if (IsCopyLayout)
                            {
                                cusspecificlangdetail.IsModificationArabic = (langdetail.IsDefaultTranslation == 1 && langdetail.Arabic != null && langdetail.Arabic != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationArabic;
                                cusspecificlangdetail.IsModificationBulgarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Bulgarian != null && langdetail.Bulgarian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationBulgarian;
                                cusspecificlangdetail.IsModificationCroatian = (langdetail.IsDefaultTranslation == 1 && langdetail.Croatian != null && langdetail.Croatian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationCroatian;
                                cusspecificlangdetail.IsModificationCzech = (langdetail.IsDefaultTranslation == 1 && langdetail.Czech != null && langdetail.Czech != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationCzech;
                                cusspecificlangdetail.IsModificationDanish = (langdetail.IsDefaultTranslation == 1 && langdetail.Danish != null && langdetail.Danish != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationDanish;
                                cusspecificlangdetail.IsModificationDutch = (langdetail.IsDefaultTranslation == 1 && langdetail.Dutch != null && langdetail.Dutch != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationDutch;
                                cusspecificlangdetail.IsModificationEnglish = (langdetail.IsDefaultTranslation == 1 && langdetail.English != null && langdetail.English != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationEnglish;
                                cusspecificlangdetail.IsModificationEstonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Estonian != null && langdetail.Estonian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationEstonian;
                                cusspecificlangdetail.IsModificationFinnish = (langdetail.IsDefaultTranslation == 1 && langdetail.Finnish != null && langdetail.Finnish != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationFinnish;
                                cusspecificlangdetail.IsModificationFrench = (langdetail.IsDefaultTranslation == 1 && langdetail.French != null && langdetail.French != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationFrench;
                                cusspecificlangdetail.IsModificationGerman = (langdetail.IsDefaultTranslation == 1 && langdetail.German != null && langdetail.German != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationGerman;
                                cusspecificlangdetail.IsModificationGreek = (langdetail.IsDefaultTranslation == 1 && langdetail.Greek != null && langdetail.Greek != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationGreek;
                                cusspecificlangdetail.IsModificationHungarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Hungarian != null && langdetail.Hungarian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationHungarian;
                                cusspecificlangdetail.IsModificationItalian = (langdetail.IsDefaultTranslation == 1 && langdetail.Italian != null && langdetail.Italian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationItalian;
                                cusspecificlangdetail.IsModificationLatvian = (langdetail.IsDefaultTranslation == 1 && langdetail.Latvian != null && langdetail.Latvian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationLatvian;
                                cusspecificlangdetail.IsModificationLithuanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Lithuanian != null && langdetail.Lithuanian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationLithuanian;
                                cusspecificlangdetail.IsModificationMacedonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Macedonian != null && langdetail.Macedonian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationMacedonian;
                                cusspecificlangdetail.IsModificationNorwegian = (langdetail.IsDefaultTranslation == 1 && langdetail.Norwegian != null && langdetail.Norwegian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationNorwegian;
                                cusspecificlangdetail.IsModificationPolish = (langdetail.IsDefaultTranslation == 1 && langdetail.Polish != null && langdetail.Polish != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationPolish;
                                cusspecificlangdetail.IsModificationPortuguese = (langdetail.IsDefaultTranslation == 1 && langdetail.Portuguese != null && langdetail.Portuguese != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationPortuguese;
                                cusspecificlangdetail.IsModificationRomanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Romanian != null && langdetail.Romanian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationRomanian;
                                cusspecificlangdetail.IsModificationRussian = (langdetail.IsDefaultTranslation == 1 && langdetail.Russian != null && langdetail.Russian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationRussian;
                                cusspecificlangdetail.IsModificationSlovak = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovak != null && langdetail.Slovak != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationSlovak;
                                cusspecificlangdetail.IsModificationSlovene = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovene != null && langdetail.Slovene != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationSlovene;
                                cusspecificlangdetail.IsModificationSpanish = (langdetail.IsDefaultTranslation == 1 && langdetail.Spanish != null && langdetail.Spanish != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationSpanish;
                                cusspecificlangdetail.IsModificationSwedish = (langdetail.IsDefaultTranslation == 1 && langdetail.Swedish != null && langdetail.Swedish != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationSwedish;
                                cusspecificlangdetail.IsModificationTurkish = (langdetail.IsDefaultTranslation == 1 && langdetail.Turkish != null && langdetail.Turkish != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationTurkish;
                                cusspecificlangdetail.IsModificationUkrainian = (langdetail.IsDefaultTranslation == 1 && langdetail.Ukrainian != null && langdetail.Ukrainian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationUkrainian;
                                cusspecificlangdetail.IsModificationBelarusian = (langdetail.IsDefaultTranslation == 1 && langdetail.Belarusian != null && langdetail.Belarusian != LangInfo.SelectedLanguage) ? IsModification : langdetail.IsModificationBelarusian;
                            }


                            cusspecificlangdetail.LangRefTableId = LangInfo.LangRefTableId;
                            cusspecificlangdetail.LayoutRefId = LangInfo.LayoutRefId;
                            cusspecificlangdetail.CustomerRefId = LangInfo.CustomerRefId;
                            cusspecificlangdetail.LangRefColTableId = langrefdetail.LangRefColTableId;

                            context.LangDetail.Add(cusspecificlangdetail);
                            context.SaveChanges();

                            langrefdetail.LangDetailId = cusspecificlangdetail.Id;
                            langrefdetail.UpdatedBy = LangInfo.UpdatedBy;
                            langrefdetail.UpdatedOn = DateTime.Now;
                            context.SaveChanges();

                            return cusspecificlangdetail;
                        }
                        else
                        {
                            if (isUseExistRecord)
                                UpgradeLanguageDetail(upgradeLangDetailId, langdetail.Id, LangInfo);
                            langdetail.RefId = langrefdetail.RefId;
                            langdetail.LangRefTableId = LangInfo.LangRefTableId;
                            langdetail.LayoutRefId = LangInfo.LayoutRefId;
                            langdetail.CustomerRefId = LangInfo.CustomerRefId;
                            langdetail.UpdatedBy = LangInfo.UpdatedBy;
                            langdetail.UpdatedOn = DateTime.Now;
                            context.SaveChanges();

                            if (isUseExistRecord)
                                langrefdetail.LangDetailId = upgradeLangDetailId;
                            langrefdetail.UpdatedBy = LangInfo.UpdatedBy;
                            langrefdetail.UpdatedOn = DateTime.Now;
                            context.SaveChanges();

                            return langdetail;
                        }
                    }
                    else
                        return returnmodal;
                }
                else
                    return returnmodal;
            }
        }
        public string SaveLangDetail(LangRefDetail LangRefInfo)
        {
            var result = "Success";
            try
            {
                using (var context = new Creatis_Context())
                {
                    LangRefDetail langrefdetail = new LangRefDetail();
                    LangDetail langdetail = new LangDetail();
                    var IsCopyLayout = false;
                    if (LangRefInfo.LayoutRefId > 0)
                    {
                        langrefdetail = context.LangRefDetail.Where(x => x.RefId == LangRefInfo.RefId && x.LayoutRefId == LangRefInfo.LayoutRefId && x.LangRefColTableId == LangRefInfo.LangRefColTableId).FirstOrDefault();
                        var LayoutInfo = context.Layout.Where(x => x.LayoutId == LangRefInfo.LayoutRefId).Select(x => new { x.IsCopyLayout, x.CustomerRefId }).FirstOrDefault();
                        IsCopyLayout = LayoutInfo.IsCopyLayout;
                        LangRefInfo.CustomerRefId = LayoutInfo.CustomerRefId;
                    }
                    else
                        langrefdetail = context.LangRefDetail.Where(x => x.RefId == LangRefInfo.RefId && x.CustomerRefId == LangRefInfo.CustomerRefId && x.LangRefColTableId == LangRefInfo.LangRefColTableId).FirstOrDefault();


                    if (langrefdetail == null)
                    {

                        if (LanguagesEnum.English.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.English, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.English, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Arabic.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Arabic, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Arabic, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();

                        }
                        else if (LanguagesEnum.Bulgarian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Bulgarian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Bulgarian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Croatian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Croatian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Croatian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();

                        }
                        else if (LanguagesEnum.Czech.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Czech, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Czech, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();

                        }
                        else if (LanguagesEnum.Danish.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Danish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Danish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();

                        }
                        else if (LanguagesEnum.Dutch.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Dutch, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Dutch, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Estonian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Estonian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Estonian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Finnish.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Finnish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Finnish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.French.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.French, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.French, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.German.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.German, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.German, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Greek.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Greek, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Greek, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Hungarian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Hungarian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Hungarian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Italian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Italian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Italian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Latvian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Latvian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Latvian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();

                        }
                        else if (LanguagesEnum.Lithuanian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Lithuanian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Lithuanian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Macedonian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Macedonian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Macedonian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Norwegian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Norwegian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Norwegian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Polish.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Polish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Polish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Portuguese.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Portuguese, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Portuguese, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Romanian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Romanian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Romanian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();

                        }
                        else if (LanguagesEnum.Russian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Russian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Russian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();

                        }
                        else if (LanguagesEnum.Slovak.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Slovak, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Slovak, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Slovene.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Slovene, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Slovene, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Spanish.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Spanish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Spanish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Swedish.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Swedish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Swedish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Turkish.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Turkish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Turkish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Ukrainian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Ukrainian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Ukrainian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }
                        else if (LanguagesEnum.Belarusian.ToString() == LangRefInfo.Language)
                        {
                            langdetail = context.LangDetail.Where(x => string.Equals(x.Belarusian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.IsDefaultTranslation == 1).FirstOrDefault();
                            if (langdetail == null)
                                langdetail = context.LangDetail.Where(x => string.Equals(x.Belarusian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).FirstOrDefault();
                        }

                        if (langdetail == null)
                        {
                            langdetail = new LangDetail();

                            langdetail.IsDefaultTranslation = 0;
                            langdetail.CustomerRefId = LangRefInfo.CustomerRefId ?? 0;

                            langdetail.LangRefColTableId = LangRefInfo.LangRefColTableId;

                            if (LanguagesEnum.English.ToString() == LangRefInfo.Language)
                            { langdetail.English = LangRefInfo.LangText; langdetail.IsModificationEnglish = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Arabic.ToString() == LangRefInfo.Language)
                            { langdetail.Arabic = LangRefInfo.LangText; langdetail.IsModificationArabic = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Bulgarian.ToString() == LangRefInfo.Language)
                            { langdetail.Bulgarian = LangRefInfo.LangText; langdetail.IsModificationBulgarian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Croatian.ToString() == LangRefInfo.Language)
                            { langdetail.Croatian = LangRefInfo.LangText; langdetail.IsModificationCroatian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Czech.ToString() == LangRefInfo.Language)
                            { langdetail.Czech = LangRefInfo.LangText; langdetail.IsModificationCzech = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Danish.ToString() == LangRefInfo.Language)
                            { langdetail.Danish = LangRefInfo.LangText; langdetail.IsModificationDanish = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Dutch.ToString() == LangRefInfo.Language)
                            { langdetail.Dutch = LangRefInfo.LangText; langdetail.IsModificationDutch = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Estonian.ToString() == LangRefInfo.Language)
                            { langdetail.Estonian = LangRefInfo.LangText; langdetail.IsModificationEstonian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Finnish.ToString() == LangRefInfo.Language)
                            { langdetail.Finnish = LangRefInfo.LangText; langdetail.IsModificationFinnish = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.French.ToString() == LangRefInfo.Language)
                            { langdetail.French = LangRefInfo.LangText; langdetail.IsModificationFrench = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.German.ToString() == LangRefInfo.Language)
                            { langdetail.German = LangRefInfo.LangText; langdetail.IsModificationGerman = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Greek.ToString() == LangRefInfo.Language)
                            { langdetail.Greek = LangRefInfo.LangText; langdetail.IsModificationGreek = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Hungarian.ToString() == LangRefInfo.Language)
                            { langdetail.Hungarian = LangRefInfo.LangText; langdetail.IsModificationHungarian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Italian.ToString() == LangRefInfo.Language)
                            { langdetail.Italian = LangRefInfo.LangText; langdetail.IsModificationItalian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Latvian.ToString() == LangRefInfo.Language)
                            { langdetail.Latvian = LangRefInfo.LangText; langdetail.IsModificationLatvian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Lithuanian.ToString() == LangRefInfo.Language)
                            { langdetail.Lithuanian = LangRefInfo.LangText; langdetail.IsModificationLithuanian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Macedonian.ToString() == LangRefInfo.Language)
                            { langdetail.Macedonian = LangRefInfo.LangText; langdetail.IsModificationMacedonian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Norwegian.ToString() == LangRefInfo.Language)
                            { langdetail.Norwegian = LangRefInfo.LangText; langdetail.IsModificationNorwegian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Polish.ToString() == LangRefInfo.Language)
                            { langdetail.Polish = LangRefInfo.LangText; langdetail.IsModificationPolish = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Portuguese.ToString() == LangRefInfo.Language)
                            { langdetail.Portuguese = LangRefInfo.LangText; langdetail.IsModificationPortuguese = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Romanian.ToString() == LangRefInfo.Language)
                            { langdetail.Romanian = LangRefInfo.LangText; langdetail.IsModificationRomanian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Russian.ToString() == LangRefInfo.Language)
                            { langdetail.Russian = LangRefInfo.LangText; langdetail.IsModificationRussian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Slovak.ToString() == LangRefInfo.Language)
                            { langdetail.Slovak = LangRefInfo.LangText; langdetail.IsModificationSlovak = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Slovene.ToString() == LangRefInfo.Language)
                            { langdetail.Slovene = LangRefInfo.LangText; langdetail.IsModificationSlovene = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Spanish.ToString() == LangRefInfo.Language)
                            { langdetail.Spanish = LangRefInfo.LangText; langdetail.IsModificationSpanish = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Swedish.ToString() == LangRefInfo.Language)
                            { langdetail.Swedish = LangRefInfo.LangText; langdetail.IsModificationSwedish = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Turkish.ToString() == LangRefInfo.Language)
                            { langdetail.Turkish = LangRefInfo.LangText; langdetail.IsModificationTurkish = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Ukrainian.ToString() == LangRefInfo.Language)
                            { langdetail.Ukrainian = LangRefInfo.LangText; langdetail.IsModificationUkrainian = LangRefInfo.IsModification; }
                            else if (LanguagesEnum.Belarusian.ToString() == LangRefInfo.Language)
                            { langdetail.Belarusian = LangRefInfo.LangText; langdetail.IsModificationBelarusian = LangRefInfo.IsModification; }

                            langdetail.CreatedOn = DateTime.Now;
                            langdetail.CreatedBy = LangRefInfo.CreatedBy;
                            langdetail.UpdatedOn = DateTime.Now;
                            langdetail.UpdatedBy = LangRefInfo.UpdatedBy;
                            context.LangDetail.Add(langdetail);
                            context.SaveChanges();
                        }

                        langrefdetail = new LangRefDetail();

                        langrefdetail.QuestionRefId = LangRefInfo.QuestionRefId;
                        langrefdetail.LangRefColTableId = LangRefInfo.LangRefColTableId;
                        langrefdetail.LayoutRefId = LangRefInfo.LayoutRefId;
                        langrefdetail.CustomerRefId = LangRefInfo.CustomerRefId;
                        langrefdetail.RefId = LangRefInfo.RefId;
                        langrefdetail.LangDetailId = langdetail.Id;
                        langrefdetail.CreatedBy = LangRefInfo.CreatedBy;
                        langrefdetail.CreatedOn = DateTime.Now;
                        langrefdetail.UpdatedBy = LangRefInfo.UpdatedBy;
                        langrefdetail.UpdatedOn = DateTime.Now;

                        context.LangRefDetail.Add(langrefdetail);
                        context.SaveChanges();
                    }
                    else
                    {
                        langdetail = context.LangDetail.Where(x => x.Id == langrefdetail.LangDetailId).FirstOrDefault();
                        var isCustomerSpecific = false;
                        var isUseExistRecord = false;
                        var isAddNewRecord = false;
                        var isModification = 99999;
                        var upgradeLanguageDetailId = 0;

                        LangDetail addNewRecord = new LangDetail();

                        addNewRecord.Arabic = "";
                        addNewRecord.Bulgarian = "";
                        addNewRecord.Croatian = "";
                        addNewRecord.Czech = "";
                        addNewRecord.Danish = "";
                        addNewRecord.Dutch = "";
                        addNewRecord.English = "";
                        addNewRecord.Estonian = "";
                        addNewRecord.Finnish = "";
                        addNewRecord.French = "";
                        addNewRecord.German = "";
                        addNewRecord.Greek = "";
                        addNewRecord.Hungarian = "";
                        addNewRecord.Italian = "";
                        addNewRecord.Latvian = "";
                        addNewRecord.Lithuanian = "";
                        addNewRecord.Macedonian = "";
                        addNewRecord.Norwegian = "";
                        addNewRecord.Polish = "";
                        addNewRecord.Portuguese = "";
                        addNewRecord.Romanian = "";
                        addNewRecord.Russian = "";
                        addNewRecord.Slovak = "";
                        addNewRecord.Slovene = "";
                        addNewRecord.Spanish = "";
                        addNewRecord.Swedish = "";
                        addNewRecord.Turkish = "";
                        addNewRecord.Ukrainian = "";
                        addNewRecord.Belarusian = "";
                        addNewRecord.IsModificationEnglish = isModification;
                        addNewRecord.IsModificationArabic = isModification;
                        addNewRecord.IsModificationBulgarian = isModification;
                        addNewRecord.IsModificationCroatian = isModification;
                        addNewRecord.IsModificationCzech = isModification;
                        addNewRecord.IsModificationDanish = isModification;
                        addNewRecord.IsModificationDutch = isModification;
                        addNewRecord.IsModificationEstonian = isModification;
                        addNewRecord.IsModificationFinnish = isModification;
                        addNewRecord.IsModificationFrench = isModification;
                        addNewRecord.IsModificationGerman = isModification;
                        addNewRecord.IsModificationGreek = isModification;
                        addNewRecord.IsModificationHungarian = isModification;
                        addNewRecord.IsModificationItalian = isModification;
                        addNewRecord.IsModificationLatvian = isModification;
                        addNewRecord.IsModificationLithuanian = isModification;
                        addNewRecord.IsModificationMacedonian = isModification;
                        addNewRecord.IsModificationNorwegian = isModification;
                        addNewRecord.IsModificationPolish = isModification;
                        addNewRecord.IsModificationPortuguese = isModification;
                        addNewRecord.IsModificationRomanian = isModification;
                        addNewRecord.IsModificationRussian = isModification;
                        addNewRecord.IsModificationSlovak = isModification;
                        addNewRecord.IsModificationSlovene = isModification;
                        addNewRecord.IsModificationSpanish = isModification;
                        addNewRecord.IsModificationSwedish = isModification;
                        addNewRecord.IsModificationTurkish = isModification;
                        addNewRecord.IsModificationUkrainian = isModification;
                        addNewRecord.IsModificationBelarusian = isModification;
                        var LangText = "";
                        int IsModification = 0;
                        if (langdetail != null)
                        {
                            if (LanguagesEnum.English.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.English != null && langdetail.English != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.English != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.English, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.English, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.English = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationEnglish = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.English = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationEnglish == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationEnglish = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.English = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Arabic.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Arabic != null && langdetail.Arabic != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Arabic != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Arabic, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Arabic, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Arabic = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationArabic = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Arabic = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationArabic == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationArabic = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Arabic = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Bulgarian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Bulgarian != null && langdetail.Bulgarian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Bulgarian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Bulgarian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Bulgarian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Bulgarian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationBulgarian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Bulgarian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationBulgarian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationBulgarian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Bulgarian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Croatian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Croatian != null && langdetail.Croatian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Croatian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Croatian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Croatian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Croatian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationCroatian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Croatian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationCroatian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationCroatian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Croatian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Czech.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Czech != null && langdetail.Czech != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Czech != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Czech, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Czech, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Czech = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationCzech = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Czech = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationCzech == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationCzech = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Czech = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Danish.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Danish != null && langdetail.Danish != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Danish != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Danish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Danish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Danish = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationDanish = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Danish = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationDanish == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationDanish = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Danish = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Dutch.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Dutch != null && langdetail.Dutch != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Dutch != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Dutch, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Dutch, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Dutch = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationDutch = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Dutch = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationDutch == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationDutch = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Dutch = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Estonian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Estonian != null && langdetail.Estonian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Estonian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Estonian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Estonian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Estonian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationEstonian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Estonian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationEstonian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationEstonian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Estonian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Finnish.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Finnish != null && langdetail.Finnish != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Finnish != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Finnish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Finnish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Finnish = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationFinnish = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Finnish = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationFinnish == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationFinnish = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Finnish = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.French.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.French != null && langdetail.French != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.French != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.French, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.French, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.French = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationFrench = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.French = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationFrench == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationFrench = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.French = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.German.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.German != null && langdetail.German != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.German != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.German, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.German, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.German = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationGerman = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.German = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationGerman == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationGerman = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.German = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Greek.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Greek != null && langdetail.Greek != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Greek != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Greek, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Greek, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Greek = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationGreek = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Greek = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationGreek == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationGreek = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Greek = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Hungarian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Hungarian != null && langdetail.Hungarian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Hungarian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Hungarian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Hungarian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Hungarian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationHungarian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Hungarian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationHungarian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationHungarian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Hungarian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Italian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Italian != null && langdetail.Italian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Italian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Italian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Italian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Italian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationItalian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Italian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationItalian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationItalian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Italian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Latvian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Latvian != null && langdetail.Latvian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Latvian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Latvian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Latvian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Latvian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationLatvian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Latvian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationLatvian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationLatvian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Latvian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Lithuanian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Lithuanian != null && langdetail.Lithuanian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Lithuanian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Lithuanian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Lithuanian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Lithuanian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationLithuanian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Lithuanian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationLithuanian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationLithuanian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Lithuanian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Macedonian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Macedonian != null && langdetail.Macedonian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Macedonian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Macedonian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Macedonian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Macedonian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationMacedonian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Macedonian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationMacedonian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationMacedonian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Macedonian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Norwegian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Norwegian != null && langdetail.Norwegian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Norwegian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Norwegian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Norwegian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Norwegian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationNorwegian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Norwegian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationNorwegian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationNorwegian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Norwegian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Polish.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Polish != null && langdetail.Polish != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Polish != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Polish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Polish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Polish = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationPolish = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Polish = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationPolish == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationPolish = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Polish = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Portuguese.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Portuguese != null && langdetail.Portuguese != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Portuguese != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Portuguese, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Portuguese, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Portuguese = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationPortuguese = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Portuguese = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationPortuguese == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationPortuguese = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Portuguese = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Romanian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Romanian != null && langdetail.Romanian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Romanian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Romanian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Romanian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Romanian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationRomanian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Romanian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationRomanian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationRomanian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Romanian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Russian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Russian != null && langdetail.Russian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Russian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Russian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Russian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Russian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationRussian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Russian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationRussian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationRussian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Russian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Slovak.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Slovak != null && langdetail.Slovak != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Slovak != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Slovak, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Slovak, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Slovak = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationSlovak = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Slovak = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationSlovak == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationSlovak = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Slovak = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Slovene.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Slovene != null && langdetail.Slovene != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Slovene != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Slovene, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Slovene, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Slovene = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationSlovene = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Slovene = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationSlovene == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationSlovene = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Slovene = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Spanish.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Spanish != null && langdetail.Spanish != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Spanish != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Spanish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Spanish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Spanish = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationSpanish = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Spanish = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationSpanish == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationSpanish = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Spanish = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Swedish.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Swedish != null && langdetail.Swedish != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Swedish != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Swedish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Swedish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Swedish = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationSwedish = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Swedish = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationSwedish == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationSwedish = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Swedish = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Turkish.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Turkish != null && langdetail.Turkish != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Turkish != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Turkish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Turkish, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Turkish = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationTurkish = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Turkish = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationTurkish == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationTurkish = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Turkish = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Ukrainian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Ukrainian != null && langdetail.Ukrainian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Ukrainian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Ukrainian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Ukrainian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Ukrainian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationUkrainian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Ukrainian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationUkrainian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationUkrainian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Ukrainian = LangRefInfo.LangText;
                            }
                            else if (LanguagesEnum.Belarusian.ToString() == LangRefInfo.Language)
                            {
                                if (langdetail.IsDefaultTranslation == 1 && langdetail.Belarusian != null && langdetail.Belarusian != LangRefInfo.LangText)
                                {
                                    LangText = LangRefInfo.LangText;
                                    if (IsCopyLayout)
                                        IsModification = (int)EnumCopyColorCode.Yellow;
                                    isCustomerSpecific = true;
                                }
                                else if (langdetail.IsDefaultTranslation == 0 && langdetail.Belarusian != LangRefInfo.LangText)
                                {
                                    if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                    {
                                        if (context.LangDetail.Where(x => string.Equals(x.Belarusian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                        {
                                            upgradeLanguageDetailId = context.LangDetail.Where(x => string.Equals(x.Belarusian, LangRefInfo.LangText, StringComparison.CurrentCulture) && x.LangRefColTableId == LangRefInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                            isUseExistRecord = true;
                                        }
                                        else if (!isUseExistRecord)
                                        {
                                            addNewRecord.Belarusian = LangRefInfo.LangText;
                                            if (IsCopyLayout)
                                                addNewRecord.IsModificationBelarusian = (int)EnumCopyColorCode.Yellow;
                                            isAddNewRecord = true;
                                        }
                                    }
                                    else
                                    {
                                        langdetail.Belarusian = LangRefInfo.LangText;
                                        if (IsCopyLayout && langdetail.IsModificationBelarusian == (int)EnumCopyColorCode.Empty)
                                            langdetail.IsModificationBelarusian = (int)EnumCopyColorCode.Yellow;
                                    }
                                }
                                else
                                    langdetail.Belarusian = LangRefInfo.LangText;
                            }

                            if (isAddNewRecord)
                            {

                                LangDetail addnewlangdetail = new LangDetail();

                                addnewlangdetail.Arabic = addNewRecord.Arabic.Length > 0 ? addNewRecord.Arabic : langdetail.Arabic;
                                addnewlangdetail.Bulgarian = addNewRecord.Bulgarian.Length > 0 ? addNewRecord.Bulgarian : langdetail.Bulgarian;
                                addnewlangdetail.Croatian = addNewRecord.Croatian.Length > 0 ? addNewRecord.Croatian : langdetail.Croatian;
                                addnewlangdetail.Czech = addNewRecord.Czech.Length > 0 ? addNewRecord.Czech : langdetail.Czech;
                                addnewlangdetail.Danish = addNewRecord.Danish.Length > 0 ? addNewRecord.Danish : langdetail.Danish;
                                addnewlangdetail.Dutch = addNewRecord.Dutch.Length > 0 ? addNewRecord.Dutch : langdetail.Dutch;
                                addnewlangdetail.English = addNewRecord.English.Length > 0 ? addNewRecord.English : langdetail.English;
                                addnewlangdetail.Estonian = addNewRecord.Estonian.Length > 0 ? addNewRecord.Estonian : langdetail.Estonian;
                                addnewlangdetail.Finnish = addNewRecord.Finnish.Length > 0 ? addNewRecord.Finnish : langdetail.Finnish;
                                addnewlangdetail.French = addNewRecord.French.Length > 0 ? addNewRecord.French : langdetail.French;
                                addnewlangdetail.German = addNewRecord.German.Length > 0 ? addNewRecord.German : langdetail.German;
                                addnewlangdetail.Greek = addNewRecord.Greek.Length > 0 ? addNewRecord.Greek : langdetail.Greek;
                                addnewlangdetail.Hungarian = addNewRecord.Hungarian.Length > 0 ? addNewRecord.Hungarian : langdetail.Hungarian;
                                addnewlangdetail.Italian = addNewRecord.Italian.Length > 0 ? addNewRecord.Italian : langdetail.Italian;
                                addnewlangdetail.Latvian = addNewRecord.Latvian.Length > 0 ? addNewRecord.Latvian : langdetail.Latvian;
                                addnewlangdetail.Lithuanian = addNewRecord.Lithuanian.Length > 0 ? addNewRecord.Lithuanian : langdetail.Lithuanian;
                                addnewlangdetail.Macedonian = addNewRecord.Macedonian.Length > 0 ? addNewRecord.Macedonian : langdetail.Macedonian;
                                addnewlangdetail.Norwegian = addNewRecord.Norwegian.Length > 0 ? addNewRecord.Norwegian : langdetail.Norwegian;
                                addnewlangdetail.Polish = addNewRecord.Polish.Length > 0 ? addNewRecord.Polish : langdetail.Polish;
                                addnewlangdetail.Portuguese = addNewRecord.Portuguese.Length > 0 ? addNewRecord.Portuguese : langdetail.Portuguese;
                                addnewlangdetail.Romanian = addNewRecord.Romanian.Length > 0 ? addNewRecord.Romanian : langdetail.Romanian;
                                addnewlangdetail.Russian = addNewRecord.Russian.Length > 0 ? addNewRecord.Russian : langdetail.Russian;
                                addnewlangdetail.Slovak = addNewRecord.Slovak.Length > 0 ? addNewRecord.Slovak : langdetail.Slovak;
                                addnewlangdetail.Slovene = addNewRecord.Slovene.Length > 0 ? addNewRecord.Slovene : langdetail.Slovene;
                                addnewlangdetail.Spanish = addNewRecord.Spanish.Length > 0 ? addNewRecord.Spanish : langdetail.Spanish;
                                addnewlangdetail.Swedish = addNewRecord.Swedish.Length > 0 ? addNewRecord.Swedish : langdetail.Swedish;
                                addnewlangdetail.Turkish = addNewRecord.Turkish.Length > 0 ? addNewRecord.Turkish : langdetail.Turkish;
                                addnewlangdetail.Ukrainian = addNewRecord.Ukrainian.Length > 0 ? addNewRecord.Ukrainian : langdetail.Ukrainian;
                                addnewlangdetail.Belarusian = addNewRecord.Belarusian.Length > 0 ? addNewRecord.Belarusian : langdetail.Belarusian;
                                addnewlangdetail.CreatedBy = LangRefInfo.CreatedBy;
                                addnewlangdetail.CreatedOn = DateTime.Now;
                                addnewlangdetail.UpdatedBy = LangRefInfo.CreatedBy;
                                addnewlangdetail.UpdatedOn = DateTime.Now;
                                addnewlangdetail.IsDefaultTranslation = 0;
                                addnewlangdetail.CustomerRefId = LangRefInfo.CustomerRefId ?? 0;
                                addnewlangdetail.LangRefColTableId = LangRefInfo.LangRefColTableId;
                                addnewlangdetail.LayoutRefId = LangRefInfo.LayoutRefId;
                                if (IsCopyLayout)
                                {
                                    if (LanguagesEnum.English.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationEnglish = addNewRecord.IsModificationEnglish != 99999 ? addNewRecord.IsModificationEnglish : langdetail.IsModificationEnglish;
                                    else if (LanguagesEnum.Arabic.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationArabic = addNewRecord.IsModificationArabic != 99999 ? addNewRecord.IsModificationArabic : langdetail.IsModificationArabic;
                                    else if (LanguagesEnum.Bulgarian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationBulgarian = addNewRecord.IsModificationBulgarian != 99999 ? addNewRecord.IsModificationBulgarian : langdetail.IsModificationBulgarian;
                                    else if (LanguagesEnum.Croatian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationCroatian = addNewRecord.IsModificationCroatian != 99999 ? addNewRecord.IsModificationCroatian : langdetail.IsModificationCroatian;
                                    else if (LanguagesEnum.Czech.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationCzech = addNewRecord.IsModificationCzech != 99999 ? addNewRecord.IsModificationCzech : langdetail.IsModificationCzech;
                                    else if (LanguagesEnum.Danish.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationDanish = addNewRecord.IsModificationDanish != 99999 ? addNewRecord.IsModificationDanish : langdetail.IsModificationDanish;
                                    else if (LanguagesEnum.Dutch.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationDutch = addNewRecord.IsModificationDutch != 99999 ? addNewRecord.IsModificationDutch : langdetail.IsModificationDutch;
                                    else if (LanguagesEnum.Estonian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationEstonian = addNewRecord.IsModificationEstonian != 99999 ? addNewRecord.IsModificationEstonian : langdetail.IsModificationEstonian;
                                    else if (LanguagesEnum.Finnish.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationFinnish = addNewRecord.IsModificationFinnish != 99999 ? addNewRecord.IsModificationFinnish : langdetail.IsModificationFinnish;
                                    else if (LanguagesEnum.French.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationFrench = addNewRecord.IsModificationFrench != 99999 ? addNewRecord.IsModificationFrench : langdetail.IsModificationFrench;
                                    else if (LanguagesEnum.German.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationGerman = addNewRecord.IsModificationGerman != 99999 ? addNewRecord.IsModificationGerman : langdetail.IsModificationGerman;
                                    else if (LanguagesEnum.Greek.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationGreek = addNewRecord.IsModificationGreek != 99999 ? addNewRecord.IsModificationGreek : langdetail.IsModificationGreek;
                                    else if (LanguagesEnum.Hungarian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationHungarian = addNewRecord.IsModificationHungarian != 99999 ? addNewRecord.IsModificationHungarian : langdetail.IsModificationHungarian;
                                    else if (LanguagesEnum.Italian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationItalian = addNewRecord.IsModificationItalian != 99999 ? addNewRecord.IsModificationItalian : langdetail.IsModificationItalian;
                                    else if (LanguagesEnum.Latvian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationLatvian = addNewRecord.IsModificationLatvian != 99999 ? addNewRecord.IsModificationLatvian : langdetail.IsModificationLatvian;
                                    else if (LanguagesEnum.Lithuanian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationLithuanian = addNewRecord.IsModificationLithuanian != 99999 ? addNewRecord.IsModificationLithuanian : langdetail.IsModificationLithuanian;
                                    else if (LanguagesEnum.Macedonian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationMacedonian = addNewRecord.IsModificationMacedonian != 99999 ? addNewRecord.IsModificationMacedonian : langdetail.IsModificationMacedonian;
                                    else if (LanguagesEnum.Norwegian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationNorwegian = addNewRecord.IsModificationNorwegian != 99999 ? addNewRecord.IsModificationNorwegian : langdetail.IsModificationNorwegian;
                                    else if (LanguagesEnum.Polish.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationPolish = addNewRecord.IsModificationPolish != 99999 ? addNewRecord.IsModificationPolish : langdetail.IsModificationPolish;
                                    else if (LanguagesEnum.Portuguese.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationPortuguese = addNewRecord.IsModificationPortuguese != 99999 ? addNewRecord.IsModificationPortuguese : langdetail.IsModificationPortuguese;
                                    else if (LanguagesEnum.Romanian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationRomanian = addNewRecord.IsModificationRomanian != 99999 ? addNewRecord.IsModificationRomanian : langdetail.IsModificationRomanian;
                                    else if (LanguagesEnum.Russian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationRussian = addNewRecord.IsModificationRussian != 99999 ? addNewRecord.IsModificationRussian : langdetail.IsModificationRussian;
                                    else if (LanguagesEnum.Slovak.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationSlovak = addNewRecord.IsModificationSlovak != 99999 ? addNewRecord.IsModificationSlovak : langdetail.IsModificationSlovak;
                                    else if (LanguagesEnum.Slovene.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationSlovene = addNewRecord.IsModificationSlovene != 99999 ? addNewRecord.IsModificationSlovene : langdetail.IsModificationSlovene;
                                    else if (LanguagesEnum.Spanish.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationSpanish = addNewRecord.IsModificationSpanish != 99999 ? addNewRecord.IsModificationSpanish : langdetail.IsModificationSpanish;
                                    else if (LanguagesEnum.Swedish.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationSwedish = addNewRecord.IsModificationSwedish != 99999 ? addNewRecord.IsModificationSwedish : langdetail.IsModificationSwedish;
                                    else if (LanguagesEnum.Turkish.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationTurkish = addNewRecord.IsModificationTurkish != 99999 ? addNewRecord.IsModificationTurkish : langdetail.IsModificationTurkish;
                                    else if (LanguagesEnum.Ukrainian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationUkrainian = addNewRecord.IsModificationUkrainian != 99999 ? addNewRecord.IsModificationUkrainian : langdetail.IsModificationUkrainian;
                                    else if (LanguagesEnum.Belarusian.ToString() == LangRefInfo.Language)
                                        addnewlangdetail.IsModificationBelarusian = addNewRecord.IsModificationBelarusian != 99999 ? addNewRecord.IsModificationBelarusian : langdetail.IsModificationBelarusian;
                                }

                                context.LangDetail.Add(addnewlangdetail);
                                context.SaveChanges();

                                langrefdetail.QuestionRefId = LangRefInfo.QuestionRefId;
                                langrefdetail.LangDetailId = addnewlangdetail.Id;
                                langrefdetail.UpdatedBy = LangRefInfo.UpdatedBy;
                                langrefdetail.UpdatedOn = DateTime.Now;
                                context.SaveChanges();
                            }
                            else if (isCustomerSpecific)
                            {
                                LangDetail cusspecificlangdetail = new LangDetail();
                                cusspecificlangdetail.Arabic = (langdetail.IsDefaultTranslation == 1 && langdetail.Arabic != null && langdetail.Arabic != LangRefInfo.LangText) ? LangText : langdetail.Arabic;
                                cusspecificlangdetail.Bulgarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Bulgarian != null && langdetail.Bulgarian != LangRefInfo.LangText) ? LangText : langdetail.Bulgarian;
                                cusspecificlangdetail.Croatian = (langdetail.IsDefaultTranslation == 1 && langdetail.Croatian != null && langdetail.Croatian != LangRefInfo.LangText) ? LangText : langdetail.Croatian;
                                cusspecificlangdetail.Czech = (langdetail.IsDefaultTranslation == 1 && langdetail.Czech != null && langdetail.Czech != LangRefInfo.LangText) ? LangText : langdetail.Czech;
                                cusspecificlangdetail.Danish = (langdetail.IsDefaultTranslation == 1 && langdetail.Danish != null && langdetail.Danish != LangRefInfo.LangText) ? LangText : langdetail.Danish;
                                cusspecificlangdetail.Dutch = (langdetail.IsDefaultTranslation == 1 && langdetail.Dutch != null && langdetail.Dutch != LangRefInfo.LangText) ? LangText : langdetail.Dutch;
                                cusspecificlangdetail.English = (langdetail.IsDefaultTranslation == 1 && langdetail.English != null && langdetail.English != LangRefInfo.LangText) ? LangText : langdetail.English;
                                cusspecificlangdetail.Estonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Estonian != null && langdetail.Estonian != LangRefInfo.LangText) ? LangText : langdetail.Estonian;
                                cusspecificlangdetail.Finnish = (langdetail.IsDefaultTranslation == 1 && langdetail.Finnish != null && langdetail.Finnish != LangRefInfo.LangText) ? LangText : langdetail.Finnish;
                                cusspecificlangdetail.French = (langdetail.IsDefaultTranslation == 1 && langdetail.French != null && langdetail.French != LangRefInfo.LangText) ? LangText : langdetail.French;
                                cusspecificlangdetail.German = (langdetail.IsDefaultTranslation == 1 && langdetail.German != null && langdetail.German != LangRefInfo.LangText) ? LangText : langdetail.German;
                                cusspecificlangdetail.Greek = (langdetail.IsDefaultTranslation == 1 && langdetail.Greek != null && langdetail.Greek != LangRefInfo.LangText) ? LangText : langdetail.Greek;
                                cusspecificlangdetail.Hungarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Hungarian != null && langdetail.Hungarian != LangRefInfo.LangText) ? LangText : langdetail.Hungarian;
                                cusspecificlangdetail.Italian = (langdetail.IsDefaultTranslation == 1 && langdetail.Italian != null && langdetail.Italian != LangRefInfo.LangText) ? LangText : langdetail.Italian;
                                cusspecificlangdetail.Latvian = (langdetail.IsDefaultTranslation == 1 && langdetail.Latvian != null && langdetail.Latvian != LangRefInfo.LangText) ? LangText : langdetail.Latvian;
                                cusspecificlangdetail.Lithuanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Lithuanian != null && langdetail.Lithuanian != LangRefInfo.LangText) ? LangText : langdetail.Lithuanian;
                                cusspecificlangdetail.Macedonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Macedonian != null && langdetail.Macedonian != LangRefInfo.LangText) ? LangText : langdetail.Macedonian;
                                cusspecificlangdetail.Norwegian = (langdetail.IsDefaultTranslation == 1 && langdetail.Norwegian != null && langdetail.Norwegian != LangRefInfo.LangText) ? LangText : langdetail.Norwegian;
                                cusspecificlangdetail.Polish = (langdetail.IsDefaultTranslation == 1 && langdetail.Polish != null && langdetail.Polish != LangRefInfo.LangText) ? LangText : langdetail.Polish;
                                cusspecificlangdetail.Portuguese = (langdetail.IsDefaultTranslation == 1 && langdetail.Portuguese != null && langdetail.Portuguese != LangRefInfo.LangText) ? LangText : langdetail.Portuguese;
                                cusspecificlangdetail.Romanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Romanian != null && langdetail.Romanian != LangRefInfo.LangText) ? LangText : langdetail.Romanian;
                                cusspecificlangdetail.Russian = (langdetail.IsDefaultTranslation == 1 && langdetail.Russian != null && langdetail.Russian != LangRefInfo.LangText) ? LangText : langdetail.Russian;
                                cusspecificlangdetail.Slovak = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovak != null && langdetail.Slovak != LangRefInfo.LangText) ? LangText : langdetail.Slovak;
                                cusspecificlangdetail.Slovene = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovene != null && langdetail.Slovene != LangRefInfo.LangText) ? LangText : langdetail.Slovene;
                                cusspecificlangdetail.Spanish = (langdetail.IsDefaultTranslation == 1 && langdetail.Spanish != null && langdetail.Spanish != LangRefInfo.LangText) ? LangText : langdetail.Spanish;
                                cusspecificlangdetail.Swedish = (langdetail.IsDefaultTranslation == 1 && langdetail.Swedish != null && langdetail.Swedish != LangRefInfo.LangText) ? LangText : langdetail.Swedish;
                                cusspecificlangdetail.Turkish = (langdetail.IsDefaultTranslation == 1 && langdetail.Turkish != null && langdetail.Turkish != LangRefInfo.LangText) ? LangText : langdetail.Turkish;
                                cusspecificlangdetail.Ukrainian = (langdetail.IsDefaultTranslation == 1 && langdetail.Ukrainian != null && langdetail.Ukrainian != LangRefInfo.LangText) ? LangText : langdetail.Ukrainian;
                                cusspecificlangdetail.Belarusian = (langdetail.IsDefaultTranslation == 1 && langdetail.Belarusian != null && langdetail.Belarusian != LangRefInfo.LangText) ? LangText : langdetail.Belarusian;
                                cusspecificlangdetail.CreatedBy = LangRefInfo.CreatedBy;
                                cusspecificlangdetail.CreatedOn = DateTime.Now;
                                cusspecificlangdetail.UpdatedBy = LangRefInfo.CreatedBy;
                                cusspecificlangdetail.UpdatedOn = DateTime.Now;
                                cusspecificlangdetail.IsDefaultTranslation = 0;
                                cusspecificlangdetail.CustomerRefId = LangRefInfo.CustomerRefId ?? 0;
                                cusspecificlangdetail.LangRefColTableId = LangRefInfo.LangRefColTableId;
                                if (IsCopyLayout)
                                {
                                    cusspecificlangdetail.IsModificationArabic = (langdetail.IsDefaultTranslation == 1 && langdetail.Arabic != null && langdetail.Arabic != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationArabic;
                                    cusspecificlangdetail.IsModificationBulgarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Bulgarian != null && langdetail.Bulgarian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationBulgarian;
                                    cusspecificlangdetail.IsModificationCroatian = (langdetail.IsDefaultTranslation == 1 && langdetail.Croatian != null && langdetail.Croatian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationCroatian;
                                    cusspecificlangdetail.IsModificationCzech = (langdetail.IsDefaultTranslation == 1 && langdetail.Czech != null && langdetail.Czech != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationCzech;
                                    cusspecificlangdetail.IsModificationDanish = (langdetail.IsDefaultTranslation == 1 && langdetail.Danish != null && langdetail.Danish != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationDanish;
                                    cusspecificlangdetail.IsModificationDutch = (langdetail.IsDefaultTranslation == 1 && langdetail.Dutch != null && langdetail.Dutch != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationDutch;
                                    cusspecificlangdetail.IsModificationEnglish = (langdetail.IsDefaultTranslation == 1 && langdetail.English != null && langdetail.English != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationEnglish;
                                    cusspecificlangdetail.IsModificationEstonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Estonian != null && langdetail.Estonian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationEstonian;
                                    cusspecificlangdetail.IsModificationFinnish = (langdetail.IsDefaultTranslation == 1 && langdetail.Finnish != null && langdetail.Finnish != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationFinnish;
                                    cusspecificlangdetail.IsModificationFrench = (langdetail.IsDefaultTranslation == 1 && langdetail.French != null && langdetail.French != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationFrench;
                                    cusspecificlangdetail.IsModificationGerman = (langdetail.IsDefaultTranslation == 1 && langdetail.German != null && langdetail.German != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationGerman;
                                    cusspecificlangdetail.IsModificationGreek = (langdetail.IsDefaultTranslation == 1 && langdetail.Greek != null && langdetail.Greek != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationGreek;
                                    cusspecificlangdetail.IsModificationHungarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Hungarian != null && langdetail.Hungarian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationHungarian;
                                    cusspecificlangdetail.IsModificationItalian = (langdetail.IsDefaultTranslation == 1 && langdetail.Italian != null && langdetail.Italian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationItalian;
                                    cusspecificlangdetail.IsModificationLatvian = (langdetail.IsDefaultTranslation == 1 && langdetail.Latvian != null && langdetail.Latvian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationLatvian;
                                    cusspecificlangdetail.IsModificationLithuanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Lithuanian != null && langdetail.Lithuanian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationLithuanian;
                                    cusspecificlangdetail.IsModificationMacedonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Macedonian != null && langdetail.Macedonian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationMacedonian;
                                    cusspecificlangdetail.IsModificationNorwegian = (langdetail.IsDefaultTranslation == 1 && langdetail.Norwegian != null && langdetail.Norwegian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationNorwegian;
                                    cusspecificlangdetail.IsModificationPolish = (langdetail.IsDefaultTranslation == 1 && langdetail.Polish != null && langdetail.Polish != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationPolish;
                                    cusspecificlangdetail.IsModificationPortuguese = (langdetail.IsDefaultTranslation == 1 && langdetail.Portuguese != null && langdetail.Portuguese != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationPortuguese;
                                    cusspecificlangdetail.IsModificationRomanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Romanian != null && langdetail.Romanian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationRomanian;
                                    cusspecificlangdetail.IsModificationRussian = (langdetail.IsDefaultTranslation == 1 && langdetail.Russian != null && langdetail.Russian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationRussian;
                                    cusspecificlangdetail.IsModificationSlovak = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovak != null && langdetail.Slovak != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationSlovak;
                                    cusspecificlangdetail.IsModificationSlovene = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovene != null && langdetail.Slovene != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationSlovene;
                                    cusspecificlangdetail.IsModificationSpanish = (langdetail.IsDefaultTranslation == 1 && langdetail.Spanish != null && langdetail.Spanish != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationSpanish;
                                    cusspecificlangdetail.IsModificationSwedish = (langdetail.IsDefaultTranslation == 1 && langdetail.Swedish != null && langdetail.Swedish != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationSwedish;
                                    cusspecificlangdetail.IsModificationTurkish = (langdetail.IsDefaultTranslation == 1 && langdetail.Turkish != null && langdetail.Turkish != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationTurkish;
                                    cusspecificlangdetail.IsModificationUkrainian = (langdetail.IsDefaultTranslation == 1 && langdetail.Ukrainian != null && langdetail.Ukrainian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationUkrainian;
                                    cusspecificlangdetail.IsModificationBelarusian = (langdetail.IsDefaultTranslation == 1 && langdetail.Belarusian != null && langdetail.Belarusian != LangRefInfo.LangText) ? IsModification : langdetail.IsModificationBelarusian;
                                }

                                context.LangDetail.Add(cusspecificlangdetail);
                                context.SaveChanges();

                                langrefdetail.QuestionRefId = LangRefInfo.QuestionRefId;
                                langrefdetail.LangDetailId = cusspecificlangdetail.Id;
                                langrefdetail.UpdatedBy = LangRefInfo.UpdatedBy;
                                langrefdetail.UpdatedOn = DateTime.Now;
                                context.SaveChanges();
                            }
                            else
                            {
                                if (isUseExistRecord)
                                    UpgradeLanguageDetail(upgradeLanguageDetailId, langdetail.Id, langdetail);

                                langdetail.LayoutRefId = LangRefInfo.LayoutRefId;
                                langdetail.CustomerRefId = LangRefInfo.CustomerRefId ?? 0;
                                langdetail.UpdatedBy = LangRefInfo.UpdatedBy;
                                langdetail.UpdatedOn = DateTime.Now;
                                context.SaveChanges();

                                if (isUseExistRecord)
                                    langrefdetail.LangDetailId = upgradeLanguageDetailId;
                                langrefdetail.UpdatedBy = LangRefInfo.UpdatedBy;
                                langrefdetail.UpdatedOn = DateTime.Now;
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public string SaveCopyLayoutLangDetail(LangRefDetail LangRefInfo)
        {
            var result = "Success";
            try
            {
                using (var context = new Creatis_Context())
                {
                    LangRefDetail langrefdetail = new LangRefDetail();
                    LangDetail langdetail = new LangDetail();
                    if (LangRefInfo.LayoutRefId > 0)
                        langrefdetail = context.LangRefDetail.Where(x => x.RefId == LangRefInfo.RefId && x.LayoutRefId == LangRefInfo.LayoutRefId && x.LangRefColTableId == LangRefInfo.LangRefColTableId).FirstOrDefault();
                    else
                        langrefdetail = context.LangRefDetail.Where(x => x.RefId == LangRefInfo.RefId && x.CustomerRefId == LangRefInfo.CustomerRefId && x.LangRefColTableId == LangRefInfo.LangRefColTableId).FirstOrDefault();

                    if (langrefdetail != null)
                    {
                        langdetail = context.LangDetail.Where(x => x.IsDefaultTranslation == 0 && x.Id == langrefdetail.LangDetailId).FirstOrDefault();
                        if (langdetail != null)
                        {
                            List<string> usedLanguages = new List<string>();
                            if (LangRefInfo.LayoutRefId > 0)
                                usedLanguages = context.UsedLanguages.Where(x => x.LayoutRefId == LangRefInfo.LayoutRefId && x.IsActive).Select(x => x.LanguageMaster.LanguageDescription).Distinct().ToList();
                            else if (LangRefInfo.CustomerRefId > 0)
                            {
                                var layoutIds = context.Layout.Where(x => x.CustomerRefId == LangRefInfo.CustomerRefId).Select(x => x.LayoutId).ToList();
                                usedLanguages = context.UsedLanguages.Where(x => layoutIds.Contains(x.LayoutRefId) && x.IsActive).Select(x => x.LanguageMaster.LanguageDescription).Distinct().ToList();
                            }

                            LangDetail cusspecificlangdetail = new LangDetail();

                            cusspecificlangdetail.Arabic = LangRefInfo.Language == LanguagesEnum.Arabic.ToString() ? LangRefInfo.LangText : langdetail.Arabic;
                            cusspecificlangdetail.Bulgarian = LangRefInfo.Language == LanguagesEnum.Bulgarian.ToString() ? LangRefInfo.LangText : langdetail.Bulgarian;
                            cusspecificlangdetail.Croatian = LangRefInfo.Language == LanguagesEnum.Croatian.ToString() ? LangRefInfo.LangText : langdetail.Croatian;
                            cusspecificlangdetail.Czech = LangRefInfo.Language == LanguagesEnum.Czech.ToString() ? LangRefInfo.LangText : langdetail.Czech;
                            cusspecificlangdetail.Danish = LangRefInfo.Language == LanguagesEnum.Danish.ToString() ? LangRefInfo.LangText : langdetail.Danish;
                            cusspecificlangdetail.Dutch = LangRefInfo.Language == LanguagesEnum.Dutch.ToString() ? LangRefInfo.LangText : langdetail.Dutch;
                            cusspecificlangdetail.English = LangRefInfo.Language == LanguagesEnum.English.ToString() ? LangRefInfo.LangText : langdetail.English;
                            cusspecificlangdetail.Estonian = LangRefInfo.Language == LanguagesEnum.Estonian.ToString() ? LangRefInfo.LangText : langdetail.Estonian;
                            cusspecificlangdetail.Finnish = LangRefInfo.Language == LanguagesEnum.Finnish.ToString() ? LangRefInfo.LangText : langdetail.Finnish;
                            cusspecificlangdetail.French = LangRefInfo.Language == LanguagesEnum.French.ToString() ? LangRefInfo.LangText : langdetail.French;
                            cusspecificlangdetail.German = LangRefInfo.Language == LanguagesEnum.German.ToString() ? LangRefInfo.LangText : langdetail.German;
                            cusspecificlangdetail.Greek = LangRefInfo.Language == LanguagesEnum.Greek.ToString() ? LangRefInfo.LangText : langdetail.Greek;
                            cusspecificlangdetail.Hungarian = LangRefInfo.Language == LanguagesEnum.Hungarian.ToString() ? LangRefInfo.LangText : langdetail.Hungarian;
                            cusspecificlangdetail.Italian = LangRefInfo.Language == LanguagesEnum.Italian.ToString() ? LangRefInfo.LangText : langdetail.Italian;
                            cusspecificlangdetail.Latvian = LangRefInfo.Language == LanguagesEnum.Latvian.ToString() ? LangRefInfo.LangText : langdetail.Latvian;
                            cusspecificlangdetail.Lithuanian = LangRefInfo.Language == LanguagesEnum.Lithuanian.ToString() ? LangRefInfo.LangText : langdetail.Lithuanian;
                            cusspecificlangdetail.Macedonian = LangRefInfo.Language == LanguagesEnum.Macedonian.ToString() ? LangRefInfo.LangText : langdetail.Macedonian;
                            cusspecificlangdetail.Norwegian = LangRefInfo.Language == LanguagesEnum.Norwegian.ToString() ? LangRefInfo.LangText : langdetail.Norwegian;
                            cusspecificlangdetail.Polish = LangRefInfo.Language == LanguagesEnum.Polish.ToString() ? LangRefInfo.LangText : langdetail.Polish;
                            cusspecificlangdetail.Portuguese = LangRefInfo.Language == LanguagesEnum.Portuguese.ToString() ? LangRefInfo.LangText : langdetail.Portuguese;
                            cusspecificlangdetail.Romanian = LangRefInfo.Language == LanguagesEnum.Romanian.ToString() ? LangRefInfo.LangText : langdetail.Romanian;
                            cusspecificlangdetail.Russian = LangRefInfo.Language == LanguagesEnum.Russian.ToString() ? LangRefInfo.LangText : langdetail.Russian;
                            cusspecificlangdetail.Slovak = LangRefInfo.Language == LanguagesEnum.Slovak.ToString() ? LangRefInfo.LangText : langdetail.Slovak;
                            cusspecificlangdetail.Slovene = LangRefInfo.Language == LanguagesEnum.Slovene.ToString() ? LangRefInfo.LangText : langdetail.Slovene;
                            cusspecificlangdetail.Spanish = LangRefInfo.Language == LanguagesEnum.Spanish.ToString() ? LangRefInfo.LangText : langdetail.Spanish;
                            cusspecificlangdetail.Swedish = LangRefInfo.Language == LanguagesEnum.Swedish.ToString() ? LangRefInfo.LangText : langdetail.Swedish;
                            cusspecificlangdetail.Turkish = LangRefInfo.Language == LanguagesEnum.Turkish.ToString() ? LangRefInfo.LangText : langdetail.Turkish;
                            cusspecificlangdetail.Ukrainian = LangRefInfo.Language == LanguagesEnum.Ukrainian.ToString() ? LangRefInfo.LangText : langdetail.Ukrainian;
                            cusspecificlangdetail.Belarusian = LangRefInfo.Language == LanguagesEnum.Belarusian.ToString() ? LangRefInfo.LangText : langdetail.Belarusian;
                            cusspecificlangdetail.CreatedBy = LangRefInfo.CreatedBy;
                            cusspecificlangdetail.CreatedOn = DateTime.Now;
                            cusspecificlangdetail.IsDefaultTranslation = 0;
                            cusspecificlangdetail.CustomerRefId = LangRefInfo.CustomerRefId ?? 0;
                            cusspecificlangdetail.LangRefColTableId = LangRefInfo.LangRefColTableId;

                            if (usedLanguages.Contains(LanguagesEnum.English.ToString()))
                                cusspecificlangdetail.IsModificationEnglish = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Arabic.ToString()))
                                cusspecificlangdetail.IsModificationArabic = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Bulgarian.ToString()))
                                cusspecificlangdetail.IsModificationBulgarian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Croatian.ToString()))
                                cusspecificlangdetail.IsModificationCroatian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Czech.ToString()))
                                cusspecificlangdetail.IsModificationCzech = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Danish.ToString()))
                                cusspecificlangdetail.IsModificationDanish = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Dutch.ToString()))
                                cusspecificlangdetail.IsModificationDutch = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Estonian.ToString()))
                                cusspecificlangdetail.IsModificationEstonian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Finnish.ToString()))
                                cusspecificlangdetail.IsModificationFinnish = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.French.ToString()))
                                cusspecificlangdetail.IsModificationFrench = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.German.ToString()))
                                cusspecificlangdetail.IsModificationGerman = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Greek.ToString()))
                                cusspecificlangdetail.IsModificationGreek = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Hungarian.ToString()))
                                cusspecificlangdetail.IsModificationHungarian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Italian.ToString()))
                                cusspecificlangdetail.IsModificationItalian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Latvian.ToString()))
                                cusspecificlangdetail.IsModificationLatvian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Lithuanian.ToString()))
                                cusspecificlangdetail.IsModificationLithuanian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Macedonian.ToString()))
                                cusspecificlangdetail.IsModificationMacedonian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Norwegian.ToString()))
                                cusspecificlangdetail.IsModificationNorwegian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Polish.ToString()))
                                cusspecificlangdetail.IsModificationPolish = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Portuguese.ToString()))
                                cusspecificlangdetail.IsModificationPortuguese = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Romanian.ToString()))
                                cusspecificlangdetail.IsModificationRomanian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Russian.ToString()))
                                cusspecificlangdetail.IsModificationRussian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Slovak.ToString()))
                                cusspecificlangdetail.IsModificationSlovak = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Slovene.ToString()))
                                cusspecificlangdetail.IsModificationSlovene = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Spanish.ToString()))
                                cusspecificlangdetail.IsModificationSpanish = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Swedish.ToString()))
                                cusspecificlangdetail.IsModificationSwedish = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Turkish.ToString()))
                                cusspecificlangdetail.IsModificationTurkish = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Ukrainian.ToString()))
                                cusspecificlangdetail.IsModificationUkrainian = LangRefInfo.IsModification;
                            if (usedLanguages.Contains(LanguagesEnum.Belarusian.ToString()))
                                cusspecificlangdetail.IsModificationBelarusian = LangRefInfo.IsModification;

                            context.LangDetail.Add(cusspecificlangdetail);
                            context.SaveChanges();

                            langrefdetail = new LangRefDetail();

                            langrefdetail.QuestionRefId = LangRefInfo.QuestionRefId;
                            langrefdetail.LangRefColTableId = LangRefInfo.LangRefColTableId;
                            langrefdetail.LayoutRefId = LangRefInfo.NewLatoutRefId;
                            langrefdetail.CustomerRefId = LangRefInfo.CustomerRefId;
                            langrefdetail.RefId = LangRefInfo.NewRefId ?? 0;
                            langrefdetail.LangDetailId = cusspecificlangdetail.Id;
                            langrefdetail.CreatedBy = LangRefInfo.CreatedBy;
                            langrefdetail.CreatedOn = DateTime.Now;
                            langrefdetail.UpdatedBy = LangRefInfo.UpdatedBy;
                            langrefdetail.UpdatedOn = DateTime.Now;

                            context.LangRefDetail.Add(langrefdetail);
                            context.SaveChanges();
                        }
                        else
                        {
                            langdetail = context.LangDetail.Where(x => x.IsDefaultTranslation == 1 && x.Id == langrefdetail.LangDetailId).FirstOrDefault();
                            if (langdetail != null)
                            {
                                langrefdetail = new LangRefDetail();

                                langrefdetail.QuestionRefId = LangRefInfo.QuestionRefId;
                                langrefdetail.LangRefColTableId = LangRefInfo.LangRefColTableId;
                                langrefdetail.LayoutRefId = LangRefInfo.NewLatoutRefId;
                                langrefdetail.CustomerRefId = LangRefInfo.CustomerRefId;
                                langrefdetail.RefId = LangRefInfo.NewRefId ?? 0;
                                langrefdetail.LangDetailId = langdetail.Id;
                                langrefdetail.CreatedBy = LangRefInfo.CreatedBy;
                                langrefdetail.CreatedOn = DateTime.Now;
                                langrefdetail.UpdatedBy = LangRefInfo.UpdatedBy;
                                langrefdetail.UpdatedOn = DateTime.Now;

                                context.LangRefDetail.Add(langrefdetail);
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public string DeleteLangDetail(LangRefDetail LangRefInfo)
        {
            var result = "Success";

            try
            {
                using (var context = new Creatis_Context())
                {
                    LangRefDetail langrefdetail = new LangRefDetail();
                    if (LangRefInfo.LayoutRefId > 0)
                        langrefdetail = context.LangRefDetail.Where(x => x.RefId == LangRefInfo.RefId && x.LayoutRefId == LangRefInfo.LayoutRefId && x.LangRefColTableId == LangRefInfo.LangRefColTableId).FirstOrDefault();
                    else
                        langrefdetail = context.LangRefDetail.Where(x => x.RefId == LangRefInfo.RefId && x.CustomerRefId == LangRefInfo.CustomerRefId && x.LangRefColTableId == LangRefInfo.LangRefColTableId).FirstOrDefault();

                    if (langrefdetail != null)
                    {
                        var langdetail = context.LangDetail.Where(x => x.Id == langrefdetail.LangDetailId).FirstOrDefault();
                        if (langdetail != null)
                        {
                            context.LangRefDetail.Remove(langrefdetail);
                            context.SaveChanges();

                            if (langdetail.IsDefaultTranslation == 0 && (context.LangRefDetail.Where(x => x.LangDetailId == langdetail.Id).Select(x => x.Id).Count() == 0))
                            {
                                context.LangDetail.Remove(langdetail);
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public List<LangDetail> GetLaunaguageDetailsInDashboard(int langRefColTableId, int layoutId)
        {
            try
            {
                List<LangDetail> langresult = new List<LangDetail>();
                using (var context = new Creatis_Context())
                {
                    var langdetail = (from lrd in context.LangRefDetail.Where(x => x.LangRefColTableId == langRefColTableId && x.LayoutRefId == layoutId).ToList()
                                      join ld in context.LangDetail.ToList() on lrd.LangDetailId equals ld.Id
                                      select new LangDetail()
                                      {
                                          Id = ld.Id,
                                          RefId = lrd.RefId,
                                          LangRefDetailId = lrd.Id,
                                          Arabic = ld.Arabic,
                                          Bulgarian = ld.Bulgarian,
                                          Croatian = ld.Croatian,
                                          Czech = ld.Czech,
                                          Danish = ld.Danish,
                                          Dutch = ld.Dutch,
                                          English = ld.English,
                                          Estonian = ld.Estonian,
                                          Finnish = ld.Finnish,
                                          French = ld.French,
                                          German = ld.German,
                                          Greek = ld.Greek,
                                          Hungarian = ld.Hungarian,
                                          Italian = ld.Italian,
                                          Latvian = ld.Latvian,
                                          Lithuanian = ld.Lithuanian,
                                          Macedonian = ld.Macedonian,
                                          Norwegian = ld.Norwegian,
                                          Polish = ld.Polish,
                                          Portuguese = ld.Portuguese,
                                          Romanian = ld.Romanian,
                                          Russian = ld.Russian,
                                          Slovak = ld.Slovak,
                                          Slovene = ld.Slovene,
                                          Spanish = ld.Spanish,
                                          Swedish = ld.Swedish,
                                          Turkish = ld.Turkish,
                                          Ukrainian = ld.Ukrainian,
                                          Belarusian = ld.Belarusian,
                                          CreatedBy = ld.CreatedBy,
                                          CreatedOn = ld.CreatedOn,
                                          UpdatedBy = ld.UpdatedBy,
                                          UpdatedOn = ld.UpdatedOn
                                      }).ToList();

                    return langdetail;
                }
            }
            catch (Exception ex)
            { throw ex; }
        }
        public string GetLanguageContent(LangDetail langdetail, string languagename, int Id, string TableCode)
        {

            var LanguageContent = string.Empty;

            if (langdetail != null)
            {
                if (LanguagesEnum.English.ToString() == languagename)
                {
                    LanguageContent = langdetail.English;
                }
                else if (LanguagesEnum.Arabic.ToString() == languagename)
                {
                    LanguageContent = langdetail.Arabic;
                }
                else if (LanguagesEnum.Bulgarian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Bulgarian;
                }
                else if (LanguagesEnum.Croatian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Croatian;
                }
                else if (LanguagesEnum.Czech.ToString() == languagename)
                {
                    LanguageContent = langdetail.Czech;
                }
                else if (LanguagesEnum.Danish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Danish;
                }
                else if (LanguagesEnum.Dutch.ToString() == languagename)
                {
                    LanguageContent = langdetail.Dutch;
                }
                else if (LanguagesEnum.Estonian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Estonian;
                }
                else if (LanguagesEnum.Finnish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Finnish;
                }
                else if (LanguagesEnum.French.ToString() == languagename)
                {
                    LanguageContent = langdetail.French;
                }
                else if (LanguagesEnum.German.ToString() == languagename)
                {
                    LanguageContent = langdetail.German;
                }
                else if (LanguagesEnum.Greek.ToString() == languagename)
                {
                    LanguageContent = langdetail.Greek;
                }
                else if (LanguagesEnum.Hungarian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Hungarian;
                }
                else if (LanguagesEnum.Italian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Italian;
                }
                else if (LanguagesEnum.Latvian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Latvian;
                }
                else if (LanguagesEnum.Lithuanian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Lithuanian;
                }
                else if (LanguagesEnum.Macedonian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Macedonian;
                }
                else if (LanguagesEnum.Norwegian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Norwegian;
                }
                else if (LanguagesEnum.Polish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Polish;
                }
                else if (LanguagesEnum.Portuguese.ToString() == languagename)
                {
                    LanguageContent = langdetail.Portuguese;
                }
                else if (LanguagesEnum.Romanian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Romanian;
                }
                else if (LanguagesEnum.Russian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Russian;
                }
                else if (LanguagesEnum.Slovak.ToString() == languagename)
                {
                    LanguageContent = langdetail.Slovak;
                }
                else if (LanguagesEnum.Slovene.ToString() == languagename)
                {
                    LanguageContent = langdetail.Slovene;
                }
                else if (LanguagesEnum.Spanish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Spanish;
                }
                else if (LanguagesEnum.Swedish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Swedish;
                }
                else if (LanguagesEnum.Turkish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Turkish;
                }
                else if (LanguagesEnum.Ukrainian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Ukrainian;
                }
                else if (LanguagesEnum.Belarusian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Belarusian;
                }
                else
                {
                    LanguageContent = "";
                }

                if (LanguageContent == null || LanguageContent == "")
                {
                    using (var context = new Creatis_Context())
                    {
                        if (TableCode == "AR")
                            languagename = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == Id).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();
                        else if (TableCode == "Q")
                            languagename = context.Questions.Where(x => x.QuestionId == Id).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

                        if (LanguagesEnum.English.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.English;
                        }
                        else if (LanguagesEnum.Arabic.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Arabic;
                        }
                        else if (LanguagesEnum.Bulgarian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Bulgarian;
                        }
                        else if (LanguagesEnum.Croatian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Croatian;
                        }
                        else if (LanguagesEnum.Czech.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Czech;
                        }
                        else if (LanguagesEnum.Danish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Danish;
                        }
                        else if (LanguagesEnum.Dutch.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Dutch;
                        }
                        else if (LanguagesEnum.Estonian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Estonian;
                        }
                        else if (LanguagesEnum.Finnish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Finnish;
                        }
                        else if (LanguagesEnum.French.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.French;
                        }
                        else if (LanguagesEnum.German.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.German;
                        }
                        else if (LanguagesEnum.Greek.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Greek;
                        }
                        else if (LanguagesEnum.Hungarian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Hungarian;
                        }
                        else if (LanguagesEnum.Italian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Italian;
                        }
                        else if (LanguagesEnum.Latvian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Latvian;
                        }
                        else if (LanguagesEnum.Lithuanian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Lithuanian;
                        }
                        else if (LanguagesEnum.Macedonian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Macedonian;
                        }
                        else if (LanguagesEnum.Norwegian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Norwegian;
                        }
                        else if (LanguagesEnum.Polish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Polish;
                        }
                        else if (LanguagesEnum.Portuguese.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Portuguese;
                        }
                        else if (LanguagesEnum.Romanian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Romanian;
                        }
                        else if (LanguagesEnum.Russian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Russian;
                        }
                        else if (LanguagesEnum.Slovak.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Slovak;
                        }
                        else if (LanguagesEnum.Slovene.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Slovene;
                        }
                        else if (LanguagesEnum.Spanish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Spanish;
                        }
                        else if (LanguagesEnum.Swedish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Swedish;
                        }
                        else if (LanguagesEnum.Turkish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Turkish;
                        }
                        else if (LanguagesEnum.Ukrainian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Ukrainian;
                        }
                        else if (LanguagesEnum.Belarusian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Belarusian;
                        }
                        else
                        {
                            LanguageContent = "@" + langdetail.English;
                        }
                    }
                }
            }
            else
            {
                LanguageContent = "";
            }

            return LanguageContent;
        }
        public string GetDefaultLanguage(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var languagename = context.UsedLanguages.Where(x => x.LayoutRefId == layoutId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();
                    return languagename;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        #region root path function

        //public string GetQuestionPath(int TableId, int ColTableId, int RefId)
        //{
        //    var PropertiesName = "";
        //    try
        //    {
        //        using (var context = new Creatis_Context())
        //        {
        //            if ((int)LangRefTable.ActivitiesRegistration == TableId && (int)LangRefColTable.ActivitiesRegistration_ActivitiesRegistrationName == ColTableId)
        //                PropertiesName = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == RefId).Select(x => x.ActivitiesRegistrationName).FirstOrDefault() + " -- ";
        //            else if ((int)LangRefTable.InfoColumns == TableId && (int)LangRefColTable.InfoColumns_IC_Column_Name == ColTableId)
        //                PropertiesName = context.InfoColumn.Where(x => x.IC_ID == RefId).Select(x => x.IC_Column_Name).FirstOrDefault() + " -- ";
        //            else if ((int)LangRefTable.Allowances == TableId && (int)LangRefColTable.Allowances_AllowancesMasterDescription == ColTableId)
        //                PropertiesName = context.Allowances.Where(x => x.AllowancesId == RefId).Select(x => x.AllowancesMaster.AllowancesMasterDescription).FirstOrDefault() + " -- ";
        //            else if ((int)LangRefTable.ChioceProperties == TableId && (int)LangRefColTable.ChioceProperties_Title == ColTableId)
        //                GetPropertiesPath(context.ChioceProperties.Where(x => x.ChioceId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.AddChioceProperties == TableId && (int)LangRefColTable.AddChioceProperties_Title == ColTableId)
        //                GetPropertiesPath(context.AddChioceProperties.Where(x => x.AddChioceId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.EntryProperties == TableId && (int)LangRefColTable.EntryProperties_Title == ColTableId)
        //                GetPropertiesPath(context.EntryProperties.Where(x => x.EntryId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.EntryProperties == TableId && (int)LangRefColTable.EntryProperties_MaskText == ColTableId)
        //                GetPropertiesPath(context.EntryProperties.Where(x => x.EntryId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.EntryProperties == TableId && (int)LangRefColTable.EntryProperties_FixedValue == ColTableId)
        //                GetPropertiesPath(context.EntryProperties.Where(x => x.EntryId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.InfomessageProperties == TableId && (int)LangRefColTable.InfomessageProperties_Title == ColTableId)
        //                GetPropertiesPath(context.InfomessageProperties.Where(x => x.InfomessageId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.InfoMessageContentDetail == TableId && (int)LangRefColTable.InfoMessageContentDetail_MessageContent == ColTableId)
        //            {
        //                var infoMessageId = context.InfoMessageContentDetail.Where(x => x.InfoMessageContentDetailId == RefId).Select(x => x.InfomessagePropertiesRefId).FirstOrDefault();
        //                GetPropertiesPath(context.InfomessageProperties.Where(x => x.InfomessageId == infoMessageId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            }
        //            else if ((int)LangRefTable.TextMessageProperties == TableId && (int)LangRefColTable.TextMessageProperties_Messagecontent == ColTableId)
        //                GetPropertiesPath(context.TextMessageProperties.Where(x => x.TextMessageId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.VariableListProperties == TableId && (int)LangRefColTable.VariableListProperties_Title == ColTableId)
        //                GetPropertiesPath(context.VariableListProperties.Where(x => x.VariableListId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.PlanningselectProperties == TableId && (int)LangRefColTable.PlanningselectProperties_Title == ColTableId)
        //                GetPropertiesPath(context.PlanningselectProperties.Where(x => x.PlanningselectId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.PlanningselectProperties == TableId && (int)LangRefColTable.PlanningselectProperties_OtherTitle == ColTableId)
        //                GetPropertiesPath(context.PlanningselectProperties.Where(x => x.PlanningselectId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.PlanningselectProperties == TableId && (int)LangRefColTable.PlanningselectProperties_Fixedvalue == ColTableId)
        //                GetPropertiesPath(context.PlanningselectProperties.Where(x => x.PlanningselectId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.DocumetScanProperties == TableId && (int)LangRefColTable.DocumetScanProperties_Title == ColTableId)
        //                GetPropertiesPath(context.DocumetScanProperties.Where(x => x.DocumetScanId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.TransportTypeProperties == TableId && (int)LangRefColTable.TransportTypeProperties_Title == ColTableId)
        //                GetPropertiesPath(context.TransportTypeProperties.Where(x => x.TransportTypeId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.TransportTypePropertiesDetail == TableId && (int)LangRefColTable.TransportTypePropertiesDetail_Title == ColTableId)
        //                GetPropertiesPath(context.TransportTypePropertiesDetail.Where(x => x.TransportTypeDetailId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.TrailerProperties == TableId && (int)LangRefColTable.TrailerProperties_Title == ColTableId)
        //                GetPropertiesPath(context.TrailerProperties.Where(x => x.TrailerId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.TrailerProperties == TableId && (int)LangRefColTable.TrailerProperties_MaskText == ColTableId)
        //                GetPropertiesPath(context.TrailerProperties.Where(x => x.TrailerId == RefId).Select(x => x.QuestionRefId).FirstOrDefault(), ref PropertiesName);
        //            else if ((int)LangRefTable.Questions == TableId && (int)LangRefColTable.Questions_Name == ColTableId)
        //                GetPropertiesPath(RefId, ref PropertiesName);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return PropertiesName;
        //    }

        //    return PropertiesName ?? "";
        //}
        //public string GetPropertiesPath(int Id, ref string PropertiesName)
        //{
        //    using (var context = new Creatis_Context())
        //    {
        //        var QuestionInfo = context.Questions.Where(x => x.QuestionId == Id).Select(x => new { x.ParentId, x.Name }).FirstOrDefault();
        //        if (QuestionInfo != null)
        //        {
        //            PropertiesName = QuestionInfo.Name + " -- " + PropertiesName;
        //            if (QuestionInfo.ParentId > 0)
        //            {
        //                GetPropertiesPath(QuestionInfo.ParentId, ref PropertiesName);
        //            }
        //        }
        //    }
        //    return "";
        //}

        #endregion

        public string GetCopyLayoutColor(int IsModification)
        {
            var colorname = "";
            if (IsModification == (int)EnumCopyColorCode.Green)
                colorname = "Green";
            else if (IsModification == (int)EnumCopyColorCode.Yellow)
                colorname = "Yellow";
            else if (IsModification == (int)EnumCopyColorCode.Red)
                colorname = "Red";
            else
                colorname = "Grey";

            return colorname;
        }
        public string SaveTranslations(LangDetail LangInfo)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    LangDetail returnmodal = new LangDetail();
                    var langrefdetail = context.LangRefDetail.Where(x => x.Id == LangInfo.LangRefDetailId).FirstOrDefault();
                    var IsCustomerSpecific = false;
                    var IsCopyLayout = false;
                    var isUseExistRecord = false;
                    var isAddNewRecord = false;
                    var isModification = 99999;
                    var upgradeLangDetailId = 0;

                    LangDetail addNewRecord = new LangDetail();

                    addNewRecord.Arabic = "";
                    addNewRecord.Bulgarian = "";
                    addNewRecord.Croatian = "";
                    addNewRecord.Czech = "";
                    addNewRecord.Danish = "";
                    addNewRecord.Dutch = "";
                    addNewRecord.English = "";
                    addNewRecord.Estonian = "";
                    addNewRecord.Finnish = "";
                    addNewRecord.French = "";
                    addNewRecord.German = "";
                    addNewRecord.Greek = "";
                    addNewRecord.Hungarian = "";
                    addNewRecord.Italian = "";
                    addNewRecord.Latvian = "";
                    addNewRecord.Lithuanian = "";
                    addNewRecord.Macedonian = "";
                    addNewRecord.Norwegian = "";
                    addNewRecord.Polish = "";
                    addNewRecord.Portuguese = "";
                    addNewRecord.Romanian = "";
                    addNewRecord.Russian = "";
                    addNewRecord.Slovak = "";
                    addNewRecord.Slovene = "";
                    addNewRecord.Spanish = "";
                    addNewRecord.Swedish = "";
                    addNewRecord.Turkish = "";
                    addNewRecord.Ukrainian = "";
                    addNewRecord.Belarusian = "";
                    addNewRecord.IsModificationEnglish = isModification;
                    addNewRecord.IsModificationArabic = isModification;
                    addNewRecord.IsModificationBulgarian = isModification;
                    addNewRecord.IsModificationCroatian = isModification;
                    addNewRecord.IsModificationCzech = isModification;
                    addNewRecord.IsModificationDanish = isModification;
                    addNewRecord.IsModificationDutch = isModification;
                    addNewRecord.IsModificationEstonian = isModification;
                    addNewRecord.IsModificationFinnish = isModification;
                    addNewRecord.IsModificationFrench = isModification;
                    addNewRecord.IsModificationGerman = isModification;
                    addNewRecord.IsModificationGreek = isModification;
                    addNewRecord.IsModificationHungarian = isModification;
                    addNewRecord.IsModificationItalian = isModification;
                    addNewRecord.IsModificationLatvian = isModification;
                    addNewRecord.IsModificationLithuanian = isModification;
                    addNewRecord.IsModificationMacedonian = isModification;
                    addNewRecord.IsModificationNorwegian = isModification;
                    addNewRecord.IsModificationPolish = isModification;
                    addNewRecord.IsModificationPortuguese = isModification;
                    addNewRecord.IsModificationRomanian = isModification;
                    addNewRecord.IsModificationRussian = isModification;
                    addNewRecord.IsModificationSlovak = isModification;
                    addNewRecord.IsModificationSlovene = isModification;
                    addNewRecord.IsModificationSpanish = isModification;
                    addNewRecord.IsModificationSwedish = isModification;
                    addNewRecord.IsModificationTurkish = isModification;
                    addNewRecord.IsModificationUkrainian = isModification;
                    addNewRecord.IsModificationBelarusian = isModification;
                    LangDetail customerSpecific = new LangDetail();

                    if (langrefdetail != null)
                    {
                        var langdetail = context.LangDetail.Where(x => x.Id == LangInfo.Id).FirstOrDefault();
                        if (langdetail != null)
                        {
                            IsCopyLayout = context.Layout.Where(x => x.LayoutId == langrefdetail.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.English != null && StripHTML(langdetail.English) != LangInfo.English)
                            {
                                customerSpecific.English = LangInfo.English;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationEnglish = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.English) != LangInfo.English)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.English, LangInfo.English, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.English, LangInfo.English, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.English = LangInfo.English ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationEnglish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.English = LangInfo.English;
                                    if (IsCopyLayout && langdetail.IsModificationEnglish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationEnglish = (int)EnumCopyColorCode.Yellow;
                                }

                            }
                            else
                                langdetail.English = langdetail.English ?? LangInfo.English;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Arabic != null && StripHTML(langdetail.Arabic) != LangInfo.Arabic)
                            {
                                customerSpecific.Arabic = LangInfo.Arabic;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationArabic = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Arabic) != LangInfo.Arabic)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Arabic, LangInfo.Arabic, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Arabic, LangInfo.Arabic, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Arabic = LangInfo.Arabic ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationArabic = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Arabic = LangInfo.Arabic;
                                    if (IsCopyLayout && langdetail.IsModificationArabic == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationArabic = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Arabic = langdetail.Arabic ?? LangInfo.Arabic;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Bulgarian != null && StripHTML(langdetail.Bulgarian) != LangInfo.Bulgarian)
                            {
                                customerSpecific.Bulgarian = LangInfo.Bulgarian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationBulgarian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Bulgarian) != LangInfo.Bulgarian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Bulgarian, LangInfo.Bulgarian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Bulgarian, LangInfo.Bulgarian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Bulgarian = LangInfo.Bulgarian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationBulgarian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Bulgarian = LangInfo.Bulgarian;
                                    if (IsCopyLayout && langdetail.IsModificationBulgarian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationBulgarian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Bulgarian = langdetail.Bulgarian ?? LangInfo.Bulgarian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Croatian != null && StripHTML(langdetail.Croatian) != LangInfo.Croatian)
                            {
                                customerSpecific.Croatian = LangInfo.Croatian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationCroatian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Croatian) != LangInfo.Croatian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Croatian, LangInfo.Croatian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Croatian, LangInfo.Croatian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Croatian = LangInfo.Croatian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationCroatian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Croatian = LangInfo.Croatian;
                                    if (IsCopyLayout && langdetail.IsModificationCroatian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationCroatian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Croatian = langdetail.Croatian ?? LangInfo.Croatian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Czech != null && StripHTML(langdetail.Czech) != LangInfo.Czech)
                            {
                                customerSpecific.Czech = LangInfo.Czech;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationCzech = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Czech) != LangInfo.Czech)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Czech, LangInfo.Czech, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Czech, LangInfo.Czech, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Czech = LangInfo.Czech ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationCzech = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Czech = LangInfo.Czech;
                                    if (IsCopyLayout && langdetail.IsModificationCzech == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationCzech = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Czech = langdetail.Czech ?? LangInfo.Czech;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Danish != null && StripHTML(langdetail.Danish) != LangInfo.Danish)
                            {
                                customerSpecific.Danish = LangInfo.Danish;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationDanish = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Danish) != LangInfo.Danish)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Danish, LangInfo.Danish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Danish, LangInfo.Danish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Danish = LangInfo.Danish ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationDanish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Danish = LangInfo.Danish;
                                    if (IsCopyLayout && langdetail.IsModificationDanish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationDanish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Danish = langdetail.Danish ?? LangInfo.Danish;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Dutch != null && StripHTML(langdetail.Dutch) != LangInfo.Dutch)
                            {
                                customerSpecific.Dutch = LangInfo.Dutch;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationDutch = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Dutch) != LangInfo.Dutch)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Dutch, LangInfo.Dutch, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Dutch, LangInfo.Dutch, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Dutch = LangInfo.Dutch ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationDutch = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Dutch = LangInfo.Dutch;
                                    if (IsCopyLayout && langdetail.IsModificationDutch == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationDutch = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Dutch = langdetail.Dutch ?? LangInfo.Dutch;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Estonian != null && StripHTML(langdetail.Estonian) != LangInfo.Estonian)
                            {
                                customerSpecific.Estonian = LangInfo.Estonian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationEstonian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Estonian) != LangInfo.Estonian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Estonian, LangInfo.Estonian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Estonian, LangInfo.Estonian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Estonian = LangInfo.Estonian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationEstonian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Estonian = LangInfo.Estonian;
                                    if (IsCopyLayout && langdetail.IsModificationEstonian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationEstonian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Estonian = langdetail.Estonian ?? LangInfo.Estonian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Finnish != null && StripHTML(langdetail.Finnish) != LangInfo.Finnish)
                            {
                                customerSpecific.Finnish = LangInfo.Finnish;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationFinnish = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Finnish) != LangInfo.Finnish)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Finnish, LangInfo.Finnish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Finnish, LangInfo.Finnish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Finnish = LangInfo.Finnish ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationFinnish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Finnish = LangInfo.Finnish;
                                    if (IsCopyLayout && langdetail.IsModificationFinnish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationFinnish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Finnish = langdetail.Finnish ?? LangInfo.Finnish;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.French != null && StripHTML(langdetail.French) != LangInfo.French)
                            {
                                customerSpecific.French = LangInfo.French;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationFrench = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.French) != LangInfo.French)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.French, LangInfo.French, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.French, LangInfo.French, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.French = LangInfo.French ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationFrench = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.French = LangInfo.French;
                                    if (IsCopyLayout && langdetail.IsModificationFrench == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationFrench = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.French = langdetail.French ?? LangInfo.French;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.German != null && StripHTML(langdetail.German) != LangInfo.German)
                            {
                                customerSpecific.German = LangInfo.German;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationGerman = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.German) != LangInfo.German)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.German, LangInfo.German, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.German, LangInfo.German, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.German = LangInfo.German ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationGerman = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.German = LangInfo.German;
                                    if (IsCopyLayout && langdetail.IsModificationGerman == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationGerman = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.German = langdetail.German ?? LangInfo.German;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Greek != null && StripHTML(langdetail.Greek) != LangInfo.Greek)
                            {
                                customerSpecific.Greek = LangInfo.Greek;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationGreek = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Greek) != LangInfo.Greek)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Greek, LangInfo.Greek, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Greek, LangInfo.Greek, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Greek = LangInfo.Greek ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationGreek = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Greek = LangInfo.Greek;
                                    if (IsCopyLayout && langdetail.IsModificationGreek == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationGreek = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Greek = langdetail.Greek ?? LangInfo.Greek;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Hungarian != null && StripHTML(langdetail.Hungarian) != LangInfo.Hungarian)
                            {
                                customerSpecific.Hungarian = LangInfo.Hungarian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationHungarian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Hungarian) != LangInfo.Hungarian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Hungarian, LangInfo.Hungarian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Hungarian, LangInfo.Hungarian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Hungarian = LangInfo.Hungarian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationHungarian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Hungarian = LangInfo.Hungarian;
                                    if (IsCopyLayout && langdetail.IsModificationHungarian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationHungarian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Hungarian = langdetail.Hungarian ?? LangInfo.Hungarian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Italian != null && StripHTML(langdetail.Italian) != LangInfo.Italian)
                            {
                                customerSpecific.Italian = LangInfo.Italian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationItalian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Italian) != LangInfo.Italian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Italian, LangInfo.Italian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Italian, LangInfo.Italian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Italian = LangInfo.Italian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationItalian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Italian = LangInfo.Italian;
                                    if (IsCopyLayout && langdetail.IsModificationItalian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationItalian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Italian = langdetail.Italian ?? LangInfo.Italian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Latvian != null && StripHTML(langdetail.Latvian) != LangInfo.Latvian)
                            {
                                customerSpecific.Latvian = LangInfo.Latvian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationLatvian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Latvian) != LangInfo.Latvian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Latvian, LangInfo.Latvian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Latvian, LangInfo.Latvian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Latvian = LangInfo.Latvian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationLatvian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Latvian = LangInfo.Latvian;
                                    if (IsCopyLayout && langdetail.IsModificationLatvian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationLatvian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Latvian = langdetail.Latvian ?? LangInfo.Latvian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Lithuanian != null && StripHTML(langdetail.Lithuanian) != LangInfo.Lithuanian)
                            {
                                customerSpecific.Lithuanian = LangInfo.Lithuanian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationLithuanian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Lithuanian) != LangInfo.Lithuanian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Lithuanian, LangInfo.Lithuanian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Lithuanian, LangInfo.Lithuanian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Lithuanian = LangInfo.Lithuanian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationLithuanian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Lithuanian = LangInfo.Lithuanian;
                                    if (IsCopyLayout && langdetail.IsModificationLithuanian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationLithuanian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Lithuanian = langdetail.Lithuanian ?? LangInfo.Lithuanian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Macedonian != null && StripHTML(langdetail.Macedonian) != LangInfo.Macedonian)
                            {
                                customerSpecific.Macedonian = LangInfo.Macedonian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationMacedonian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Macedonian) != LangInfo.Macedonian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Macedonian, LangInfo.Macedonian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Macedonian, LangInfo.Macedonian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Macedonian = LangInfo.Macedonian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationMacedonian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Macedonian = LangInfo.Macedonian;
                                    if (IsCopyLayout && langdetail.IsModificationMacedonian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationMacedonian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Macedonian = langdetail.Macedonian ?? LangInfo.Macedonian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Norwegian != null && StripHTML(langdetail.Norwegian) != LangInfo.Norwegian)
                            {
                                customerSpecific.Norwegian = LangInfo.Norwegian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationNorwegian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Norwegian) != LangInfo.Norwegian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Norwegian, LangInfo.Norwegian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Norwegian, LangInfo.Norwegian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Norwegian = LangInfo.Norwegian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationNorwegian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Norwegian = LangInfo.Norwegian;
                                    if (IsCopyLayout && langdetail.IsModificationNorwegian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationNorwegian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Norwegian = langdetail.Norwegian ?? LangInfo.Norwegian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Polish != null && StripHTML(langdetail.Polish) != LangInfo.Polish)
                            {
                                customerSpecific.Polish = LangInfo.Polish;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationPolish = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Polish) != LangInfo.Polish)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Polish, LangInfo.Polish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Polish, LangInfo.Polish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Polish = LangInfo.Polish ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationPolish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Polish = LangInfo.Polish;
                                    if (IsCopyLayout && langdetail.IsModificationPolish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationPolish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Polish = langdetail.Polish ?? LangInfo.Polish;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Portuguese != null && StripHTML(langdetail.Portuguese) != LangInfo.Portuguese)
                            {
                                customerSpecific.Portuguese = LangInfo.Portuguese;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationPortuguese = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Portuguese) != LangInfo.Portuguese)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Portuguese, LangInfo.Portuguese, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Portuguese, LangInfo.Portuguese, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Portuguese = LangInfo.Portuguese ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationPortuguese = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Portuguese = LangInfo.Portuguese;
                                    if (IsCopyLayout && langdetail.IsModificationPortuguese == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationPortuguese = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Portuguese = langdetail.Portuguese ?? LangInfo.Portuguese;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Romanian != null && StripHTML(langdetail.Romanian) != LangInfo.Romanian)
                            {
                                customerSpecific.Romanian = LangInfo.Romanian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationRomanian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Romanian) != LangInfo.Romanian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Romanian, LangInfo.Romanian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Romanian, LangInfo.Romanian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Romanian = LangInfo.Romanian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationRomanian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Romanian = LangInfo.Romanian;
                                    if (IsCopyLayout && langdetail.IsModificationRomanian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationRomanian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Romanian = langdetail.Romanian ?? LangInfo.Romanian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Russian != null && StripHTML(langdetail.Russian) != LangInfo.Russian)
                            {
                                customerSpecific.Russian = LangInfo.Russian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationRussian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Russian) != LangInfo.Russian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Russian, LangInfo.Russian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Russian, LangInfo.Russian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Russian = LangInfo.Russian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationRussian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Russian = LangInfo.Russian;
                                    if (IsCopyLayout && langdetail.IsModificationRussian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationRussian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Russian = langdetail.Russian ?? LangInfo.Russian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Slovak != null && StripHTML(langdetail.Slovak) != LangInfo.Slovak)
                            {
                                customerSpecific.Slovak = LangInfo.Slovak;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationSlovak = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Slovak) != LangInfo.Slovak)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Slovak, LangInfo.Slovak, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Slovak, LangInfo.Slovak, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Slovak = LangInfo.Slovak ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationSlovak = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Slovak = LangInfo.Slovak;
                                    if (IsCopyLayout && langdetail.IsModificationSlovak == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationSlovak = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Slovak = langdetail.Slovak ?? LangInfo.Slovak;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Slovene != null && StripHTML(langdetail.Slovene) != LangInfo.Slovene)
                            {
                                customerSpecific.Slovene = LangInfo.Slovene;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationSlovene = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Slovene) != LangInfo.Slovene)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Slovene, LangInfo.Slovene, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Slovene, LangInfo.Slovene, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Slovene = LangInfo.Slovene ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationSlovene = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Slovene = LangInfo.Slovene;
                                    if (IsCopyLayout && langdetail.IsModificationSlovene == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationSlovene = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Slovene = langdetail.Slovene ?? LangInfo.Slovene;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Spanish != null && StripHTML(langdetail.Spanish) != LangInfo.Spanish)
                            {
                                customerSpecific.Spanish = LangInfo.Spanish;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationSpanish = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Spanish) != LangInfo.Spanish)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Spanish, LangInfo.Spanish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Spanish, LangInfo.Spanish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Spanish = LangInfo.Spanish ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationSpanish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Spanish = LangInfo.Spanish;
                                    if (IsCopyLayout && langdetail.IsModificationSpanish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationSpanish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Spanish = langdetail.Spanish ?? LangInfo.Spanish;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Swedish != null && StripHTML(langdetail.Swedish) != LangInfo.Swedish)
                            {
                                customerSpecific.Swedish = LangInfo.Swedish;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationSwedish = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Swedish) != LangInfo.Swedish)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Swedish, LangInfo.Swedish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Swedish, LangInfo.Swedish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Swedish = LangInfo.Swedish ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationSwedish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Swedish = LangInfo.Swedish;
                                    if (IsCopyLayout && langdetail.IsModificationSwedish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationSwedish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Swedish = langdetail.Swedish ?? LangInfo.Swedish;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Turkish != null && StripHTML(langdetail.Turkish) != LangInfo.Turkish)
                            {
                                customerSpecific.Turkish = LangInfo.Turkish;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationTurkish = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Turkish) != LangInfo.Turkish)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Turkish, LangInfo.Turkish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Turkish, LangInfo.Turkish, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Turkish = LangInfo.Turkish ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationTurkish = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Turkish = LangInfo.Turkish;
                                    if (IsCopyLayout && langdetail.IsModificationTurkish == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationTurkish = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Turkish = langdetail.Turkish ?? LangInfo.Turkish;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Ukrainian != null && StripHTML(langdetail.Ukrainian) != LangInfo.Ukrainian)
                            {
                                customerSpecific.Ukrainian = LangInfo.Ukrainian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationUkrainian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Ukrainian) != LangInfo.Ukrainian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Ukrainian, LangInfo.Ukrainian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Ukrainian, LangInfo.Ukrainian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Ukrainian = LangInfo.Ukrainian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationUkrainian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Ukrainian = LangInfo.Ukrainian;
                                    if (IsCopyLayout && langdetail.IsModificationUkrainian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationUkrainian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Ukrainian = langdetail.Ukrainian ?? LangInfo.Ukrainian;

                            if (langdetail.IsDefaultTranslation == 1 && langdetail.Belarusian != null && StripHTML(langdetail.Belarusian) != LangInfo.Belarusian)
                            {
                                customerSpecific.Belarusian = LangInfo.Belarusian;
                                if (IsCopyLayout)
                                    customerSpecific.IsModificationBelarusian = (int)EnumCopyColorCode.Yellow;
                                IsCustomerSpecific = true;
                            }
                            else if (langdetail.IsDefaultTranslation == 0 && StripHTML(langdetail.Belarusian) != LangInfo.Belarusian)
                            {
                                if (context.LangRefDetail.Where(x => x.Id != langrefdetail.Id && x.LangDetailId == langrefdetail.LangDetailId).Select(x => x.Id).Count() > 0)
                                {
                                    if (context.LangDetail.Where(x => string.Equals(x.Belarusian, LangInfo.Belarusian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).Count() > 0 && !isUseExistRecord && !isAddNewRecord)
                                    {
                                        upgradeLangDetailId = context.LangDetail.Where(x => string.Equals(x.Belarusian, LangInfo.Belarusian, StringComparison.CurrentCulture) && x.LangRefColTableId == LangInfo.LangRefColTableId && x.IsDefaultTranslation == 0 && x.CustomerRefId == LangInfo.CustomerRefId).Select(x => x.Id).FirstOrDefault();
                                        isUseExistRecord = true;
                                    }
                                    else if (!isUseExistRecord)
                                    {
                                        addNewRecord.Belarusian = LangInfo.Belarusian ?? "";
                                        if (IsCopyLayout)
                                            addNewRecord.IsModificationBelarusian = (int)EnumCopyColorCode.Yellow;
                                        isAddNewRecord = true;
                                    }
                                }
                                else
                                {
                                    langdetail.Belarusian = LangInfo.Belarusian;
                                    if (IsCopyLayout && langdetail.IsModificationBelarusian == (int)EnumCopyColorCode.Empty)
                                        langdetail.IsModificationBelarusian = (int)EnumCopyColorCode.Yellow;
                                }
                            }
                            else
                                langdetail.Belarusian = langdetail.Belarusian ?? LangInfo.Belarusian;

                            if (isAddNewRecord)
                            {

                                LangDetail addnewlangdetail = new LangDetail();

                                addnewlangdetail.Arabic = (addNewRecord.Arabic.Length > 0 ? addNewRecord.Arabic : langdetail.Arabic);
                                addnewlangdetail.Bulgarian = (addNewRecord.Bulgarian.Length > 0 ? addNewRecord.Bulgarian : langdetail.Bulgarian);
                                addnewlangdetail.Croatian = (addNewRecord.Croatian.Length > 0 ? addNewRecord.Croatian : langdetail.Croatian);
                                addnewlangdetail.Czech = (addNewRecord.Czech.Length > 0 ? addNewRecord.Czech : langdetail.Czech);
                                addnewlangdetail.Danish = (addNewRecord.Danish.Length > 0 ? addNewRecord.Danish : langdetail.Danish);
                                addnewlangdetail.Dutch = (addNewRecord.Dutch.Length > 0 ? addNewRecord.Dutch : langdetail.Dutch);
                                addnewlangdetail.English = (addNewRecord.English.Length > 0 ? addNewRecord.English : langdetail.English);
                                addnewlangdetail.Estonian = (addNewRecord.Estonian.Length > 0 ? addNewRecord.Estonian : langdetail.Estonian);
                                addnewlangdetail.Finnish = (addNewRecord.Finnish.Length > 0 ? addNewRecord.Finnish : langdetail.Finnish);
                                addnewlangdetail.French = (addNewRecord.French.Length > 0 ? addNewRecord.French : langdetail.French);
                                addnewlangdetail.German = (addNewRecord.German.Length > 0 ? addNewRecord.German : langdetail.German);
                                addnewlangdetail.Greek = (addNewRecord.Greek.Length > 0 ? addNewRecord.Greek : langdetail.Greek);
                                addnewlangdetail.Hungarian = (addNewRecord.Hungarian.Length > 0 ? addNewRecord.Hungarian : langdetail.Hungarian);
                                addnewlangdetail.Italian = (addNewRecord.Italian.Length > 0 ? addNewRecord.Italian : langdetail.Italian);
                                addnewlangdetail.Latvian = (addNewRecord.Latvian.Length > 0 ? addNewRecord.Latvian : langdetail.Latvian);
                                addnewlangdetail.Lithuanian = (addNewRecord.Lithuanian.Length > 0 ? addNewRecord.Lithuanian : langdetail.Lithuanian);
                                addnewlangdetail.Macedonian = (addNewRecord.Macedonian.Length > 0 ? addNewRecord.Macedonian : langdetail.Macedonian);
                                addnewlangdetail.Norwegian = (addNewRecord.Norwegian.Length > 0 ? addNewRecord.Norwegian : langdetail.Norwegian);
                                addnewlangdetail.Polish = (addNewRecord.Polish.Length > 0 ? addNewRecord.Polish : langdetail.Polish);
                                addnewlangdetail.Portuguese = (addNewRecord.Portuguese.Length > 0 ? addNewRecord.Portuguese : langdetail.Portuguese);
                                addnewlangdetail.Romanian = (addNewRecord.Romanian.Length > 0 ? addNewRecord.Romanian : langdetail.Romanian);
                                addnewlangdetail.Russian = (addNewRecord.Russian.Length > 0 ? addNewRecord.Russian : langdetail.Russian);
                                addnewlangdetail.Slovak = (addNewRecord.Slovak.Length > 0 ? addNewRecord.Slovak : langdetail.Slovak);
                                addnewlangdetail.Slovene = (addNewRecord.Slovene.Length > 0 ? addNewRecord.Slovene : langdetail.Slovene);
                                addnewlangdetail.Spanish = (addNewRecord.Spanish.Length > 0 ? addNewRecord.Spanish : langdetail.Spanish);
                                addnewlangdetail.Swedish = (addNewRecord.Swedish.Length > 0 ? addNewRecord.Swedish : langdetail.Swedish);
                                addnewlangdetail.Turkish = (addNewRecord.Turkish.Length > 0 ? addNewRecord.Turkish : langdetail.Turkish);
                                addnewlangdetail.Ukrainian = (addNewRecord.Ukrainian.Length > 0 ? addNewRecord.Ukrainian : langdetail.Ukrainian);
                                addnewlangdetail.Belarusian = (addNewRecord.Belarusian.Length > 0 ? addNewRecord.Belarusian : langdetail.Belarusian);
                                addnewlangdetail.CreatedBy = LangInfo.CreatedBy;
                                addnewlangdetail.CreatedOn = DateTime.Now;
                                addnewlangdetail.UpdatedBy = LangInfo.CreatedBy;
                                addnewlangdetail.UpdatedOn = DateTime.Now;
                                addnewlangdetail.IsDefaultTranslation = 0;
                                addnewlangdetail.CustomerRefId = LangInfo.CustomerRefId;
                                addnewlangdetail.LangRefColTableId = LangInfo.LangRefColTableId;
                                addnewlangdetail.LayoutRefId = LangInfo.LayoutRefId;
                                if (IsCopyLayout)
                                {
                                    addnewlangdetail.IsModificationEnglish = addNewRecord.IsModificationEnglish != 99999 ? addNewRecord.IsModificationEnglish : langdetail.IsModificationEnglish;
                                    addnewlangdetail.IsModificationArabic = addNewRecord.IsModificationArabic != 99999 ? addNewRecord.IsModificationArabic : langdetail.IsModificationArabic;
                                    addnewlangdetail.IsModificationBulgarian = addNewRecord.IsModificationBulgarian != 99999 ? addNewRecord.IsModificationBulgarian : langdetail.IsModificationBulgarian;
                                    addnewlangdetail.IsModificationCroatian = addNewRecord.IsModificationCroatian != 99999 ? addNewRecord.IsModificationCroatian : langdetail.IsModificationCroatian;
                                    addnewlangdetail.IsModificationCzech = addNewRecord.IsModificationCzech != 99999 ? addNewRecord.IsModificationCzech : langdetail.IsModificationCzech;
                                    addnewlangdetail.IsModificationDanish = addNewRecord.IsModificationDanish != 99999 ? addNewRecord.IsModificationDanish : langdetail.IsModificationDanish;
                                    addnewlangdetail.IsModificationDutch = addNewRecord.IsModificationDutch != 99999 ? addNewRecord.IsModificationDutch : langdetail.IsModificationDutch;
                                    addnewlangdetail.IsModificationEstonian = addNewRecord.IsModificationEstonian != 99999 ? addNewRecord.IsModificationEstonian : langdetail.IsModificationEstonian;
                                    addnewlangdetail.IsModificationFinnish = addNewRecord.IsModificationFinnish != 99999 ? addNewRecord.IsModificationFinnish : langdetail.IsModificationFinnish;
                                    addnewlangdetail.IsModificationFrench = addNewRecord.IsModificationFrench != 99999 ? addNewRecord.IsModificationFrench : langdetail.IsModificationFrench;
                                    addnewlangdetail.IsModificationGerman = addNewRecord.IsModificationGerman != 99999 ? addNewRecord.IsModificationGerman : langdetail.IsModificationGerman;
                                    addnewlangdetail.IsModificationGreek = addNewRecord.IsModificationGreek != 99999 ? addNewRecord.IsModificationGreek : langdetail.IsModificationGreek;
                                    addnewlangdetail.IsModificationHungarian = addNewRecord.IsModificationHungarian != 99999 ? addNewRecord.IsModificationHungarian : langdetail.IsModificationHungarian;
                                    addnewlangdetail.IsModificationItalian = addNewRecord.IsModificationItalian != 99999 ? addNewRecord.IsModificationItalian : langdetail.IsModificationItalian;
                                    addnewlangdetail.IsModificationLatvian = addNewRecord.IsModificationLatvian != 99999 ? addNewRecord.IsModificationLatvian : langdetail.IsModificationLatvian;
                                    addnewlangdetail.IsModificationLithuanian = addNewRecord.IsModificationLithuanian != 99999 ? addNewRecord.IsModificationLithuanian : langdetail.IsModificationLithuanian;
                                    addnewlangdetail.IsModificationMacedonian = addNewRecord.IsModificationMacedonian != 99999 ? addNewRecord.IsModificationMacedonian : langdetail.IsModificationMacedonian;
                                    addnewlangdetail.IsModificationNorwegian = addNewRecord.IsModificationNorwegian != 99999 ? addNewRecord.IsModificationNorwegian : langdetail.IsModificationNorwegian;
                                    addnewlangdetail.IsModificationPolish = addNewRecord.IsModificationPolish != 99999 ? addNewRecord.IsModificationPolish : langdetail.IsModificationPolish;
                                    addnewlangdetail.IsModificationPortuguese = addNewRecord.IsModificationPortuguese != 99999 ? addNewRecord.IsModificationPortuguese : langdetail.IsModificationPortuguese;
                                    addnewlangdetail.IsModificationRomanian = addNewRecord.IsModificationRomanian != 99999 ? addNewRecord.IsModificationRomanian : langdetail.IsModificationRomanian;
                                    addnewlangdetail.IsModificationRussian = addNewRecord.IsModificationRussian != 99999 ? addNewRecord.IsModificationRussian : langdetail.IsModificationRussian;
                                    addnewlangdetail.IsModificationSlovak = addNewRecord.IsModificationSlovak != 99999 ? addNewRecord.IsModificationSlovak : langdetail.IsModificationSlovak;
                                    addnewlangdetail.IsModificationSlovene = addNewRecord.IsModificationSlovene != 99999 ? addNewRecord.IsModificationSlovene : langdetail.IsModificationSlovene;
                                    addnewlangdetail.IsModificationSpanish = addNewRecord.IsModificationSpanish != 99999 ? addNewRecord.IsModificationSpanish : langdetail.IsModificationSpanish;
                                    addnewlangdetail.IsModificationSwedish = addNewRecord.IsModificationSwedish != 99999 ? addNewRecord.IsModificationSwedish : langdetail.IsModificationSwedish;
                                    addnewlangdetail.IsModificationTurkish = addNewRecord.IsModificationTurkish != 99999 ? addNewRecord.IsModificationTurkish : langdetail.IsModificationTurkish;
                                    addnewlangdetail.IsModificationUkrainian = addNewRecord.IsModificationUkrainian != 99999 ? addNewRecord.IsModificationUkrainian : langdetail.IsModificationUkrainian;
                                    addnewlangdetail.IsModificationBelarusian = addNewRecord.IsModificationBelarusian != 99999 ? addNewRecord.IsModificationBelarusian : langdetail.IsModificationBelarusian;
                                }

                                context.LangDetail.Add(addnewlangdetail);
                                context.SaveChanges();

                                langrefdetail.LangDetailId = addnewlangdetail.Id;
                                langrefdetail.UpdatedBy = LangInfo.UpdatedBy;
                                langrefdetail.UpdatedOn = DateTime.Now;
                                context.SaveChanges();

                                return "";
                            }
                            else if (IsCustomerSpecific)
                            {
                                LangDetail cusspecificlangdetail = new LangDetail();

                                cusspecificlangdetail.Arabic = (langdetail.IsDefaultTranslation == 1 && langdetail.Arabic != null && StripHTML(langdetail.Arabic) != LangInfo.Arabic) ? customerSpecific.Arabic : langdetail.Arabic;
                                cusspecificlangdetail.Bulgarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Bulgarian != null && StripHTML(langdetail.Bulgarian) != LangInfo.Bulgarian) ? customerSpecific.Bulgarian : langdetail.Bulgarian;
                                cusspecificlangdetail.Croatian = (langdetail.IsDefaultTranslation == 1 && langdetail.Croatian != null && StripHTML(langdetail.Croatian) != LangInfo.Croatian) ? customerSpecific.Croatian : langdetail.Croatian;
                                cusspecificlangdetail.Czech = (langdetail.IsDefaultTranslation == 1 && langdetail.Czech != null && StripHTML(langdetail.Czech) != LangInfo.Czech) ? customerSpecific.Czech : langdetail.Czech;
                                cusspecificlangdetail.Danish = (langdetail.IsDefaultTranslation == 1 && langdetail.Danish != null && StripHTML(langdetail.Danish) != LangInfo.Danish) ? customerSpecific.Danish : langdetail.Danish;
                                cusspecificlangdetail.Dutch = (langdetail.IsDefaultTranslation == 1 && langdetail.Dutch != null && StripHTML(langdetail.Dutch) != LangInfo.Dutch) ? customerSpecific.Dutch : langdetail.Dutch;
                                cusspecificlangdetail.English = (langdetail.IsDefaultTranslation == 1 && langdetail.English != null && StripHTML(langdetail.English) != LangInfo.English) ? customerSpecific.English : langdetail.English;
                                cusspecificlangdetail.Estonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Estonian != null && StripHTML(langdetail.Estonian) != LangInfo.Estonian) ? customerSpecific.Estonian : langdetail.Estonian;
                                cusspecificlangdetail.Finnish = (langdetail.IsDefaultTranslation == 1 && langdetail.Finnish != null && StripHTML(langdetail.Finnish) != LangInfo.Finnish) ? customerSpecific.Finnish : langdetail.Finnish;
                                cusspecificlangdetail.French = (langdetail.IsDefaultTranslation == 1 && langdetail.French != null && StripHTML(langdetail.French) != LangInfo.French) ? customerSpecific.French : langdetail.French;
                                cusspecificlangdetail.German = (langdetail.IsDefaultTranslation == 1 && langdetail.German != null && StripHTML(langdetail.German) != LangInfo.German) ? customerSpecific.German : langdetail.German;
                                cusspecificlangdetail.Greek = (langdetail.IsDefaultTranslation == 1 && langdetail.Greek != null && StripHTML(langdetail.Greek) != LangInfo.Greek) ? customerSpecific.Greek : langdetail.Greek;
                                cusspecificlangdetail.Hungarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Hungarian != null && StripHTML(langdetail.Hungarian) != LangInfo.Hungarian) ? customerSpecific.Hungarian : langdetail.Hungarian;
                                cusspecificlangdetail.Italian = (langdetail.IsDefaultTranslation == 1 && langdetail.Italian != null && StripHTML(langdetail.Italian) != LangInfo.Italian) ? customerSpecific.Italian : langdetail.Italian;
                                cusspecificlangdetail.Latvian = (langdetail.IsDefaultTranslation == 1 && langdetail.Latvian != null && StripHTML(langdetail.Latvian) != LangInfo.Latvian) ? customerSpecific.Latvian : langdetail.Latvian;
                                cusspecificlangdetail.Lithuanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Lithuanian != null && StripHTML(langdetail.Lithuanian) != LangInfo.Lithuanian) ? customerSpecific.Lithuanian : langdetail.Lithuanian;
                                cusspecificlangdetail.Macedonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Macedonian != null && StripHTML(langdetail.Macedonian) != LangInfo.Macedonian) ? customerSpecific.Macedonian : langdetail.Macedonian;
                                cusspecificlangdetail.Norwegian = (langdetail.IsDefaultTranslation == 1 && langdetail.Norwegian != null && StripHTML(langdetail.Norwegian) != LangInfo.Norwegian) ? customerSpecific.Norwegian : langdetail.Norwegian;
                                cusspecificlangdetail.Polish = (langdetail.IsDefaultTranslation == 1 && langdetail.Polish != null && StripHTML(langdetail.Polish) != LangInfo.Polish) ? customerSpecific.Polish : langdetail.Polish;
                                cusspecificlangdetail.Portuguese = (langdetail.IsDefaultTranslation == 1 && langdetail.Portuguese != null && StripHTML(langdetail.Portuguese) != LangInfo.Portuguese) ? customerSpecific.Portuguese : langdetail.Portuguese;
                                cusspecificlangdetail.Romanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Romanian != null && StripHTML(langdetail.Romanian) != LangInfo.Romanian) ? customerSpecific.Romanian : langdetail.Romanian;
                                cusspecificlangdetail.Russian = (langdetail.IsDefaultTranslation == 1 && langdetail.Russian != null && StripHTML(langdetail.Russian) != LangInfo.Russian) ? customerSpecific.Russian : langdetail.Russian;
                                cusspecificlangdetail.Slovak = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovak != null && StripHTML(langdetail.Slovak) != LangInfo.Slovak) ? customerSpecific.Slovak : langdetail.Slovak;
                                cusspecificlangdetail.Slovene = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovene != null && StripHTML(langdetail.Slovene) != LangInfo.Slovene) ? customerSpecific.Slovene : langdetail.Slovene;
                                cusspecificlangdetail.Spanish = (langdetail.IsDefaultTranslation == 1 && langdetail.Spanish != null && StripHTML(langdetail.Spanish) != LangInfo.Spanish) ? customerSpecific.Spanish : langdetail.Spanish;
                                cusspecificlangdetail.Swedish = (langdetail.IsDefaultTranslation == 1 && langdetail.Swedish != null && StripHTML(langdetail.Swedish) != LangInfo.Swedish) ? customerSpecific.Swedish : langdetail.Swedish;
                                cusspecificlangdetail.Turkish = (langdetail.IsDefaultTranslation == 1 && langdetail.Turkish != null && StripHTML(langdetail.Turkish) != LangInfo.Turkish) ? customerSpecific.Turkish : langdetail.Turkish;
                                cusspecificlangdetail.Ukrainian = (langdetail.IsDefaultTranslation == 1 && langdetail.Ukrainian != null && StripHTML(langdetail.Ukrainian) != LangInfo.Ukrainian) ? customerSpecific.Ukrainian : langdetail.Ukrainian;
                                cusspecificlangdetail.Belarusian = (langdetail.IsDefaultTranslation == 1 && langdetail.Belarusian != null && StripHTML(langdetail.Belarusian) != LangInfo.Belarusian) ? customerSpecific.Belarusian : langdetail.Belarusian;
                                cusspecificlangdetail.CreatedBy = LangInfo.CreatedBy;
                                cusspecificlangdetail.CreatedOn = DateTime.Now;
                                cusspecificlangdetail.UpdatedBy = LangInfo.CreatedBy;
                                cusspecificlangdetail.UpdatedOn = DateTime.Now;
                                cusspecificlangdetail.IsDefaultTranslation = 0;
                                cusspecificlangdetail.CustomerRefId = LangInfo.CustomerRefId;
                                if (IsCopyLayout)
                                {
                                    cusspecificlangdetail.IsModificationArabic = (langdetail.IsDefaultTranslation == 1 && langdetail.Arabic != null && langdetail.Arabic != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationArabic : langdetail.IsModificationArabic;
                                    cusspecificlangdetail.IsModificationBulgarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Bulgarian != null && langdetail.Bulgarian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationBulgarian : langdetail.IsModificationBulgarian;
                                    cusspecificlangdetail.IsModificationCroatian = (langdetail.IsDefaultTranslation == 1 && langdetail.Croatian != null && langdetail.Croatian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationCroatian : langdetail.IsModificationCroatian;
                                    cusspecificlangdetail.IsModificationCzech = (langdetail.IsDefaultTranslation == 1 && langdetail.Czech != null && langdetail.Czech != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationCzech : langdetail.IsModificationCzech;
                                    cusspecificlangdetail.IsModificationDanish = (langdetail.IsDefaultTranslation == 1 && langdetail.Danish != null && langdetail.Danish != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationDanish : langdetail.IsModificationDanish;
                                    cusspecificlangdetail.IsModificationDutch = (langdetail.IsDefaultTranslation == 1 && langdetail.Dutch != null && langdetail.Dutch != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationDutch : langdetail.IsModificationDutch;
                                    cusspecificlangdetail.IsModificationEnglish = (langdetail.IsDefaultTranslation == 1 && langdetail.English != null && langdetail.English != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationEnglish : langdetail.IsModificationEnglish;
                                    cusspecificlangdetail.IsModificationEstonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Estonian != null && langdetail.Estonian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationEstonian : langdetail.IsModificationEstonian;
                                    cusspecificlangdetail.IsModificationFinnish = (langdetail.IsDefaultTranslation == 1 && langdetail.Finnish != null && langdetail.Finnish != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationFinnish : langdetail.IsModificationFinnish;
                                    cusspecificlangdetail.IsModificationFrench = (langdetail.IsDefaultTranslation == 1 && langdetail.French != null && langdetail.French != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationFrench : langdetail.IsModificationFrench;
                                    cusspecificlangdetail.IsModificationGerman = (langdetail.IsDefaultTranslation == 1 && langdetail.German != null && langdetail.German != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationGerman : langdetail.IsModificationGerman;
                                    cusspecificlangdetail.IsModificationGreek = (langdetail.IsDefaultTranslation == 1 && langdetail.Greek != null && langdetail.Greek != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationGreek : langdetail.IsModificationGreek;
                                    cusspecificlangdetail.IsModificationHungarian = (langdetail.IsDefaultTranslation == 1 && langdetail.Hungarian != null && langdetail.Hungarian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationHungarian : langdetail.IsModificationHungarian;
                                    cusspecificlangdetail.IsModificationItalian = (langdetail.IsDefaultTranslation == 1 && langdetail.Italian != null && langdetail.Italian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationItalian : langdetail.IsModificationItalian;
                                    cusspecificlangdetail.IsModificationLatvian = (langdetail.IsDefaultTranslation == 1 && langdetail.Latvian != null && langdetail.Latvian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationLatvian : langdetail.IsModificationLatvian;
                                    cusspecificlangdetail.IsModificationLithuanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Lithuanian != null && langdetail.Lithuanian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationLithuanian : langdetail.IsModificationLithuanian;
                                    cusspecificlangdetail.IsModificationMacedonian = (langdetail.IsDefaultTranslation == 1 && langdetail.Macedonian != null && langdetail.Macedonian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationMacedonian : langdetail.IsModificationMacedonian;
                                    cusspecificlangdetail.IsModificationNorwegian = (langdetail.IsDefaultTranslation == 1 && langdetail.Norwegian != null && langdetail.Norwegian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationNorwegian : langdetail.IsModificationNorwegian;
                                    cusspecificlangdetail.IsModificationPolish = (langdetail.IsDefaultTranslation == 1 && langdetail.Polish != null && langdetail.Polish != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationPolish : langdetail.IsModificationPolish;
                                    cusspecificlangdetail.IsModificationPortuguese = (langdetail.IsDefaultTranslation == 1 && langdetail.Portuguese != null && langdetail.Portuguese != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationPortuguese : langdetail.IsModificationPortuguese;
                                    cusspecificlangdetail.IsModificationRomanian = (langdetail.IsDefaultTranslation == 1 && langdetail.Romanian != null && langdetail.Romanian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationRomanian : langdetail.IsModificationRomanian;
                                    cusspecificlangdetail.IsModificationRussian = (langdetail.IsDefaultTranslation == 1 && langdetail.Russian != null && langdetail.Russian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationRussian : langdetail.IsModificationRussian;
                                    cusspecificlangdetail.IsModificationSlovak = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovak != null && langdetail.Slovak != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationSlovak : langdetail.IsModificationSlovak;
                                    cusspecificlangdetail.IsModificationSlovene = (langdetail.IsDefaultTranslation == 1 && langdetail.Slovene != null && langdetail.Slovene != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationSlovene : langdetail.IsModificationSlovene;
                                    cusspecificlangdetail.IsModificationSpanish = (langdetail.IsDefaultTranslation == 1 && langdetail.Spanish != null && langdetail.Spanish != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationSpanish : langdetail.IsModificationSpanish;
                                    cusspecificlangdetail.IsModificationSwedish = (langdetail.IsDefaultTranslation == 1 && langdetail.Swedish != null && langdetail.Swedish != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationSwedish : langdetail.IsModificationSwedish;
                                    cusspecificlangdetail.IsModificationTurkish = (langdetail.IsDefaultTranslation == 1 && langdetail.Turkish != null && langdetail.Turkish != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationTurkish : langdetail.IsModificationTurkish;
                                    cusspecificlangdetail.IsModificationUkrainian = (langdetail.IsDefaultTranslation == 1 && langdetail.Ukrainian != null && langdetail.Ukrainian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationUkrainian : langdetail.IsModificationUkrainian;
                                    cusspecificlangdetail.IsModificationBelarusian = (langdetail.IsDefaultTranslation == 1 && langdetail.Belarusian != null && langdetail.Belarusian != LangInfo.SelectedLanguage) ? customerSpecific.IsModificationBelarusian : langdetail.IsModificationBelarusian;
                                }

                                cusspecificlangdetail.LangRefTableId = LangInfo.LangRefTableId;
                                cusspecificlangdetail.LayoutRefId = LangInfo.LayoutRefId;
                                cusspecificlangdetail.CustomerRefId = LangInfo.CustomerRefId;
                                cusspecificlangdetail.LangRefColTableId = langrefdetail.LangRefColTableId;
                                cusspecificlangdetail.RefId = langrefdetail.RefId;

                                context.LangDetail.Add(cusspecificlangdetail);
                                context.SaveChanges();

                                langrefdetail.LangDetailId = cusspecificlangdetail.Id;
                                langrefdetail.UpdatedBy = LangInfo.UpdatedBy;
                                langrefdetail.UpdatedOn = DateTime.Now;
                                context.SaveChanges();

                                return "";
                            }
                            else
                            {
                                if (isUseExistRecord)
                                    UpgradeLanguageDetail(upgradeLangDetailId, LangInfo.Id, LangInfo);

                                langdetail.RefId = langrefdetail.RefId;
                                langdetail.LangRefTableId = LangInfo.LangRefTableId;
                                langdetail.LayoutRefId = LangInfo.LayoutRefId;
                                langdetail.CustomerRefId = LangInfo.CustomerRefId;
                                langdetail.UpdatedBy = LangInfo.UpdatedBy;
                                langdetail.UpdatedOn = DateTime.Now;
                                context.SaveChanges();

                                if (isUseExistRecord)
                                    langrefdetail.LangDetailId = upgradeLangDetailId;
                                langrefdetail.UpdatedBy = LangInfo.UpdatedBy;
                                langrefdetail.UpdatedOn = DateTime.Now;
                                context.SaveChanges();

                                return "";
                            }
                        }
                        else
                            return "Error";
                    }
                    else
                        return "Error";
                }
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }
        public bool GetIsCopyLayout(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return (context.Layout.Where(x => x.LayoutId == LayoutId).Select(x => x.IsCopyLayout).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<LanguageMaster> GetSelectedLanguages(string LanguageIds, int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defaultLanguageId = context.UsedLanguages.Where(x => x.LayoutRefId == LayoutId && x.IsDefault == 1).Select(x => x.LanguageRefId).FirstOrDefault();
                    var LanguageIdValue = LanguageIds.Split("-").Where(x => x.Length > 0).Select(x => Int32.Parse(x)).ToList();

                    return (context.LanguageMaster.Where(x => LanguageIdValue.Contains(x.LanguageId)).Select(x => new LanguageMaster() { LanguageDescription = x.LanguageDescription, IsDefault = (x.LanguageId == defaultLanguageId ? 1 : 0) }).ToList());
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public LangRefDetail GetLangRefDetail(int Id)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return (context.LangRefDetail.Where(x => x.Id == Id).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public LangDetail GetLangDetail(int Id)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return (context.LangDetail.Where(x => x.Id == Id).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetExportFileName(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return (context.Layout.Where(x => x.LayoutId == LayoutId).Select(x => x.Customers.Name + "_" + x.LayoutName).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetExportFileNameByCustomer(int customerId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return (context.Customers.Where(x => x.CustomerId == customerId).Select(x => x.Name).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public LangDetail GetIsDefaultLayoutLanguage(int LayoutId)
        {
            try
            {
                LangDetail langdetail = new LangDetail();
                using (var context = new Creatis_Context())
                {
                    var languagename = context.UsedLanguages.Where(x => x.LayoutRefId == LayoutId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();
                    if (LanguagesEnum.English.ToString() == languagename)
                    { langdetail.IsDefaultEnglish = 1; }
                    else if (LanguagesEnum.Arabic.ToString() == languagename)
                    { langdetail.IsDefaultArabic = 1; }
                    else if (LanguagesEnum.Bulgarian.ToString() == languagename)
                    { langdetail.IsDefaultBulgarian = 1; }
                    else if (LanguagesEnum.Croatian.ToString() == languagename)
                    { langdetail.IsDefaultCroatian = 1; }
                    else if (LanguagesEnum.Czech.ToString() == languagename)
                    { langdetail.IsDefaultCzech = 1; }
                    else if (LanguagesEnum.Danish.ToString() == languagename)
                    { langdetail.IsDefaultDanish = 1; }
                    else if (LanguagesEnum.Dutch.ToString() == languagename)
                    { langdetail.IsDefaultDutch = 1; }
                    else if (LanguagesEnum.Estonian.ToString() == languagename)
                    { langdetail.IsDefaultEstonian = 1; }
                    else if (LanguagesEnum.Finnish.ToString() == languagename)
                    { langdetail.IsDefaultFinnish = 1; }
                    else if (LanguagesEnum.French.ToString() == languagename)
                    { langdetail.IsDefaultFrench = 1; }
                    else if (LanguagesEnum.German.ToString() == languagename)
                    { langdetail.IsDefaultGerman = 1; }
                    else if (LanguagesEnum.Greek.ToString() == languagename)
                    { langdetail.IsDefaultGreek = 1; }
                    else if (LanguagesEnum.Hungarian.ToString() == languagename)
                    { langdetail.IsDefaultHungarian = 1; }
                    else if (LanguagesEnum.Italian.ToString() == languagename)
                    { langdetail.IsDefaultItalian = 1; }
                    else if (LanguagesEnum.Latvian.ToString() == languagename)
                    { langdetail.IsDefaultLatvian = 1; }
                    else if (LanguagesEnum.Lithuanian.ToString() == languagename)
                    { langdetail.IsDefaultLithuanian = 1; }
                    else if (LanguagesEnum.Macedonian.ToString() == languagename)
                    { langdetail.IsDefaultMacedonian = 1; }
                    else if (LanguagesEnum.Norwegian.ToString() == languagename)
                    { langdetail.IsDefaultNorwegian = 1; }
                    else if (LanguagesEnum.Polish.ToString() == languagename)
                    { langdetail.IsDefaultPolish = 1; }
                    else if (LanguagesEnum.Portuguese.ToString() == languagename)
                    { langdetail.IsDefaultPortuguese = 1; }
                    else if (LanguagesEnum.Romanian.ToString() == languagename)
                    { langdetail.IsDefaultRomanian = 1; }
                    else if (LanguagesEnum.Russian.ToString() == languagename)
                    { langdetail.IsDefaultRussian = 1; }
                    else if (LanguagesEnum.Slovak.ToString() == languagename)
                    { langdetail.IsDefaultSlovak = 1; }
                    else if (LanguagesEnum.Slovene.ToString() == languagename)
                    { langdetail.IsDefaultSlovene = 1; }
                    else if (LanguagesEnum.Spanish.ToString() == languagename)
                    { langdetail.IsDefaultSpanish = 1; }
                    else if (LanguagesEnum.Swedish.ToString() == languagename)
                    { langdetail.IsDefaultSwedish = 1; }
                    else if (LanguagesEnum.Turkish.ToString() == languagename)
                    { langdetail.IsDefaultTurkish = 1; }
                    else if (LanguagesEnum.Ukrainian.ToString() == languagename)
                    { langdetail.IsDefaultUkrainian = 1; }
                    else if (LanguagesEnum.Belarusian.ToString() == languagename)
                    { langdetail.IsDefaultBelarusian = 1; }

                    return langdetail;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LangDetail> GetExportLangDetail(int LayoutId)
        {
            List<LangDetail> langresult = new List<LangDetail>();
            using (var context = new Creatis_Context())
            {
                int CustomerId = 0;
                if (LayoutId > 0)
                    CustomerId = context.Layout.Where(x => x.LayoutId == LayoutId).Select(x => x.CustomerRefId).FirstOrDefault();

                SqlParameter layoutParam = new SqlParameter("@LayoutRefId", LayoutId);
                SqlParameter customerParam = new SqlParameter("@CustomerRefId", CustomerId);
                SqlParameter isLoadHtmlParam = new SqlParameter("@IsLoadHtml", 0);

                var translationList = context.Translations.FromSql("GetTranslations @LayoutRefId,@CustomerRefId,@IsLoadHtml", layoutParam, customerParam, isLoadHtmlParam).ToList(); //.Where(x => (Id == 0 ? (1 == 1) : (x.LangRefDetailId == Id)))

                langresult = translationList.Select(x => new LangDetail()
                {
                    QuestionRefId = x.QuestionRefId,
                    IsDefaultTranslation = x.IsDefaultTranslation,
                    Id = x.LangDetailId ?? 0,
                    LangRefDetailId = x.LangRefDetailId,
                    Arabic = x.Arabic,
                    Bulgarian = x.Bulgarian,
                    Croatian = x.Croatian,
                    Czech = x.Czech,
                    Danish = x.Danish,
                    Dutch = x.Dutch,
                    English = x.English,
                    Estonian = x.Estonian,
                    Finnish = x.Finnish,
                    French = x.French,
                    German = x.German,
                    Greek = x.Greek,
                    Hungarian = x.Hungarian,
                    Italian = x.Italian,
                    Latvian = x.Latvian,
                    Lithuanian = x.Lithuanian,
                    Macedonian = x.Macedonian,
                    Norwegian = x.Norwegian,
                    Polish = x.Polish,
                    Portuguese = x.Portuguese,
                    Romanian = x.Romanian,
                    Russian = x.Russian,
                    Slovak = x.Slovak,
                    Slovene = x.Slovene,
                    Spanish = x.Spanish,
                    Swedish = x.Swedish,
                    Turkish = x.Turkish,
                    Ukrainian = x.Ukrainian,
                    Belarusian = x.Belarusian,
                    LangRefColTableId = x.LangRefColTableId,
                    RefId = x.RefId ?? 0,
                    LayoutRefId = x.LayoutRefId,
                    CustomerRefId = x.CustomerRefId ?? 0,
                    LangRefTableId = x.LangRefTableId ?? 0,
                    ColumnName = x.ColumnName,
                    TableName = x.TableName,
                    PropertiesName = x.PropertiesName,
                    PropertiesPath = x.PropertiesPath

                }).ToList();
            }
            return langresult;
        }
        public bool GetExportExcelValidation(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var languagedetail = GetExportLangDetail(LayoutId);
                    var languagename = context.UsedLanguages.Where(x => x.LayoutRefId == LayoutId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();
                    var result = false;
                    var LanguageContent = "";
                    foreach (var langdetail in languagedetail)
                    {
                        if (LanguagesEnum.English.ToString() == languagename)
                        {
                            LanguageContent = langdetail.English;
                        }
                        else if (LanguagesEnum.Arabic.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Arabic;
                        }
                        else if (LanguagesEnum.Bulgarian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Bulgarian;
                        }
                        else if (LanguagesEnum.Croatian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Croatian;
                        }
                        else if (LanguagesEnum.Czech.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Czech;
                        }
                        else if (LanguagesEnum.Danish.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Danish;
                        }
                        else if (LanguagesEnum.Dutch.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Dutch;
                        }
                        else if (LanguagesEnum.Estonian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Estonian;
                        }
                        else if (LanguagesEnum.Finnish.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Finnish;
                        }
                        else if (LanguagesEnum.French.ToString() == languagename)
                        {
                            LanguageContent = langdetail.French;
                        }
                        else if (LanguagesEnum.German.ToString() == languagename)
                        {
                            LanguageContent = langdetail.German;
                        }
                        else if (LanguagesEnum.Greek.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Greek;
                        }
                        else if (LanguagesEnum.Hungarian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Hungarian;
                        }
                        else if (LanguagesEnum.Italian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Italian;
                        }
                        else if (LanguagesEnum.Latvian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Latvian;
                        }
                        else if (LanguagesEnum.Lithuanian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Lithuanian;
                        }
                        else if (LanguagesEnum.Macedonian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Macedonian;
                        }
                        else if (LanguagesEnum.Norwegian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Norwegian;
                        }
                        else if (LanguagesEnum.Polish.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Polish;
                        }
                        else if (LanguagesEnum.Portuguese.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Portuguese;
                        }
                        else if (LanguagesEnum.Romanian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Romanian;
                        }
                        else if (LanguagesEnum.Russian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Russian;
                        }
                        else if (LanguagesEnum.Slovak.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Slovak;
                        }
                        else if (LanguagesEnum.Slovene.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Slovene;
                        }
                        else if (LanguagesEnum.Spanish.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Spanish;
                        }
                        else if (LanguagesEnum.Swedish.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Swedish;
                        }
                        else if (LanguagesEnum.Turkish.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Turkish;
                        }
                        else if (LanguagesEnum.Ukrainian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Ukrainian;
                        }
                        else if (LanguagesEnum.Belarusian.ToString() == languagename)
                        {
                            LanguageContent = langdetail.Belarusian;
                        }

                        if (LanguageContent != null && LanguageContent.Trim().Length > 0 && result == false)
                            result = false;
                        else
                        {
                            result = true;
                            break;
                        }

                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public int GetLayoutDefaultLanguageId(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return (context.UsedLanguages.Where(x => x.LayoutRefId == LayoutId && x.IsDefault == 1).Select(x => x.LanguageRefId).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int GetTemplateLayoutId(int CustomerId, int OBCTypeId)
        {
            var LayoutId = 0;

            try
            {
                using (var context = new Creatis_Context())
                {
                    LayoutId = context.OBCType.Where(x => x.CustomerRefId == CustomerId && (OBCTypeId == 999999 ? (1 == 1) : x.OBCTypeRefId == OBCTypeId)).Select(x => x.LayoutRefId).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }

            return LayoutId;
        }
        public bool ValidateTemplateLayoutId(int CustomerId, int LayoutId)
        {
            var result = false;

            try
            {
                using (var context = new Creatis_Context())
                {
                    var LayoutIds = context.Layout.Where(x => x.CustomerRefId == CustomerId).Select(x => x.LayoutId).ToList();
                    if (LayoutIds.Contains(LayoutId))
                        result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }
        public string StripHTML(string source)
        {
            try
            {
                string result;

                // Replace line breaks with space
                // because browsers inserts space
                result = source.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV>, <span> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*b([^>])*>", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*u([^>])*>", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*span([^>])*>", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                        @"&nbsp;", " ",
                        System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                            System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // make line breaking consistent
                result = result.Replace("\n", "");

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4.
                // Prepare first to remove any whitespaces in between
                // the escaped characters and remove redundant tabs in between line breaks
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove multiple tabs following a line break with just one tab
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Initial replacement target string for line breaks
                string breaks = "\r\r\r";
                // Initial replacement target string for tabs
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "");
                    result = result.Replace(tabs, "");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }

                // That's it.
                return result;
            }
            catch (Exception ex)
            {
                return source;
            }
        }
        public List<LangDetail> GetGeneralTranslation(int Id)
        {
            List<LangDetail> langdetail = new List<LangDetail>();
            using (var context = new Creatis_Context())
            {
                SqlParameter idparam = new SqlParameter("@Id", Id);

                context.Database.SetCommandTimeout(180);
                var generalTranslations = context.GeneralTranslation.FromSql("GetGeneralTranslations @Id", idparam).ToList();
                langdetail = generalTranslations.Select(x => new LangDetail()
                {
                    Id = x.Id,
                    Arabic = x.Arabic,
                    Bulgarian = x.Bulgarian,
                    Croatian = x.Croatian,
                    Czech = x.Czech,
                    Danish = x.Danish,
                    Dutch = x.Dutch,
                    English = x.English,
                    Estonian = x.Estonian,
                    Finnish = x.Finnish,
                    French = x.French,
                    German = x.German,
                    Greek = x.Greek,
                    Hungarian = x.Hungarian,
                    Italian = x.Italian,
                    Latvian = x.Latvian,
                    Lithuanian = x.Lithuanian,
                    Macedonian = x.Macedonian,
                    Norwegian = x.Norwegian,
                    Polish = x.Polish,
                    Portuguese = x.Portuguese,
                    Romanian = x.Romanian,
                    Russian = x.Russian,
                    Slovak = x.Slovak,
                    Slovene = x.Slovene,
                    Spanish = x.Spanish,
                    Swedish = x.Swedish,
                    Turkish = x.Turkish,
                    Ukrainian = x.Ukrainian,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDefaultTranslation = x.IsDefaultTranslation,
                    Belarusian = x.Belarusian,
                    Code = x.Code
                }).OrderByDescending(x => x.UpdatedOn).ToList();
            }

            return langdetail;
        }
        public string SaveCentralTranslation(LangDetail LangDetailModel)
        {
            var result = "success";
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (LangDetailModel.Id > 0)
                    {
                        var target = context.LangDetail.Where(x => x.Id == LangDetailModel.Id).FirstOrDefault();
                        if (target != null)
                        {
                            target.IsDefaultTranslation = LangDetailModel.IsDefaultTranslation;
                            target.Arabic = LangDetailModel.Arabic;
                            target.Bulgarian = LangDetailModel.Bulgarian;
                            target.Croatian = LangDetailModel.Croatian;
                            target.Czech = LangDetailModel.Czech;
                            target.Danish = LangDetailModel.Danish;
                            target.Dutch = LangDetailModel.Dutch;
                            target.English = LangDetailModel.English;
                            target.Estonian = LangDetailModel.Estonian;
                            target.Finnish = LangDetailModel.Finnish;
                            target.French = LangDetailModel.French;
                            target.German = LangDetailModel.German;
                            target.Greek = LangDetailModel.Greek;
                            target.Hungarian = LangDetailModel.Hungarian;
                            target.Italian = LangDetailModel.Italian;
                            target.Latvian = LangDetailModel.Latvian;
                            target.Lithuanian = LangDetailModel.Lithuanian;
                            target.Macedonian = LangDetailModel.Macedonian;
                            target.Norwegian = LangDetailModel.Norwegian;
                            target.Polish = LangDetailModel.Polish;
                            target.Portuguese = LangDetailModel.Portuguese;
                            target.Romanian = LangDetailModel.Romanian;
                            target.Russian = LangDetailModel.Russian;
                            target.Slovak = LangDetailModel.Slovak;
                            target.Slovene = LangDetailModel.Slovene;
                            target.Spanish = LangDetailModel.Spanish;
                            target.Swedish = LangDetailModel.Swedish;
                            target.Turkish = LangDetailModel.Turkish;
                            target.Ukrainian = LangDetailModel.Ukrainian;
                            target.Belarusian = LangDetailModel.Belarusian;
                            target.UpdatedBy = LangDetailModel.UpdatedBy;
                            target.UpdatedOn = LangDetailModel.UpdatedOn;
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        context.LangDetail.Add(LangDetailModel);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public List<Customers> GetCustomerSpecificList()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var CustomerSpecificList = context.LangDetail.Where(x => x.CustomerRefId > 0).Select(x => x.CustomerRefId).Distinct().ToList();
                    List<Customers> returnList;
                    return returnList = context.Customers.Where(x => CustomerSpecificList.Contains(x.CustomerId)).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LangDetail> GetCustomerSpecificTranslation(int CustomerId)
        {
            List<LangDetail> langdetail = new List<LangDetail>();
            using (var context = new Creatis_Context())
            {
                var customerList = context.Customers.Where(x => x.CustomerId == CustomerId).Select(x => new { x.CustomerId, CustomerName = string.Format("{0} ({1}-{2})", x.Name, x.Source, x.Environment) }).ToList();

                langdetail = (from ld in context.LangDetail.ToList()
                              join cl in customerList on ld.CustomerRefId equals cl.CustomerId
                              where ld.CustomerRefId == CustomerId && ld.IsDefaultTranslation == 0
                              select new LangDetail()
                              {
                                  Id = ld.Id,
                                  CustomerRefId = ld.CustomerRefId,
                                  IsDefaultTranslation = ld.IsDefaultTranslation,
                                  Arabic = ld.Arabic,
                                  Bulgarian = ld.Bulgarian,
                                  Croatian = ld.Croatian,
                                  Czech = ld.Czech,
                                  Danish = ld.Danish,
                                  Dutch = ld.Dutch,
                                  English = ld.English,
                                  Estonian = ld.Estonian,
                                  Finnish = ld.Finnish,
                                  French = ld.French,
                                  German = ld.German,
                                  Greek = ld.Greek,
                                  Hungarian = ld.Hungarian,
                                  Italian = ld.Italian,
                                  Latvian = ld.Latvian,
                                  Lithuanian = ld.Lithuanian,
                                  Macedonian = ld.Macedonian,
                                  Norwegian = ld.Norwegian,
                                  Polish = ld.Polish,
                                  Portuguese = ld.Portuguese,
                                  Romanian = ld.Romanian,
                                  Russian = ld.Russian,
                                  Slovak = ld.Slovak,
                                  Slovene = ld.Slovene,
                                  Spanish = ld.Spanish,
                                  Swedish = ld.Swedish,
                                  Turkish = ld.Turkish,
                                  Ukrainian = ld.Ukrainian,
                                  Belarusian = ld.Belarusian,
                                  CustomerName = cl.CustomerName,
                                  CreatedOn = ld.CreatedOn,
                                  UpdatedOn = ld.UpdatedOn
                              }).Distinct().OrderByDescending(x => x.UpdatedOn).ToList();

            }

            return langdetail;
        }
        public string CopyTranslationFromCustomerSpecificToGeneral(int langDetailId, string userName, DateTime createdDate)
        {
            var result = "success";
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (langDetailId > 0)
                    {
                        var LangDetailModel = context.LangDetail.Where(x => x.Id == langDetailId).FirstOrDefault();

                        if (LangDetailModel != null)
                        {
                            if (context.LangDetail.Where(x => (x.English ?? "").Trim().ToLower() == (LangDetailModel.English ?? "").Trim().ToLower() && x.IsDefaultTranslation == 1).Count() == 0)
                            {
                                LangDetail target = new LangDetail();

                                target.IsDefaultTranslation = 1;
                                target.Arabic = LangDetailModel.Arabic;
                                target.Bulgarian = LangDetailModel.Bulgarian;
                                target.Croatian = LangDetailModel.Croatian;
                                target.Czech = LangDetailModel.Czech;
                                target.Danish = LangDetailModel.Danish;
                                target.Dutch = LangDetailModel.Dutch;
                                target.English = LangDetailModel.English;
                                target.Estonian = LangDetailModel.Estonian;
                                target.Finnish = LangDetailModel.Finnish;
                                target.French = LangDetailModel.French;
                                target.German = LangDetailModel.German;
                                target.Greek = LangDetailModel.Greek;
                                target.Hungarian = LangDetailModel.Hungarian;
                                target.Italian = LangDetailModel.Italian;
                                target.Latvian = LangDetailModel.Latvian;
                                target.Lithuanian = LangDetailModel.Lithuanian;
                                target.Macedonian = LangDetailModel.Macedonian;
                                target.Norwegian = LangDetailModel.Norwegian;
                                target.Polish = LangDetailModel.Polish;
                                target.Portuguese = LangDetailModel.Portuguese;
                                target.Romanian = LangDetailModel.Romanian;
                                target.Russian = LangDetailModel.Russian;
                                target.Slovak = LangDetailModel.Slovak;
                                target.Slovene = LangDetailModel.Slovene;
                                target.Spanish = LangDetailModel.Spanish;
                                target.Swedish = LangDetailModel.Swedish;
                                target.Turkish = LangDetailModel.Turkish;
                                target.Ukrainian = LangDetailModel.Ukrainian;
                                target.Belarusian = LangDetailModel.Belarusian;
                                target.CreatedBy = userName;
                                target.CreatedOn = createdDate;
                                target.UpdatedBy = userName;
                                target.UpdatedOn = createdDate;
                                context.LangDetail.Add(target);
                                context.SaveChanges();
                            }
                            else
                            {
                                var target = context.LangDetail.Where(x => (x.English ?? "").Trim().ToLower() == (LangDetailModel.English ?? "").Trim().ToLower() && x.IsDefaultTranslation == 1).FirstOrDefault();
                                if (target != null)
                                {
                                    target.Arabic = string.IsNullOrEmpty(target.Arabic) ? LangDetailModel.Arabic : target.Arabic;
                                    target.Bulgarian = string.IsNullOrEmpty(target.Bulgarian) ? LangDetailModel.Bulgarian : target.Bulgarian;
                                    target.Croatian = string.IsNullOrEmpty(target.Croatian) ? LangDetailModel.Croatian : target.Croatian;
                                    target.Czech = string.IsNullOrEmpty(target.Czech) ? LangDetailModel.Czech : target.Czech;
                                    target.Danish = string.IsNullOrEmpty(target.Danish) ? LangDetailModel.Danish : target.Danish;
                                    target.Dutch = string.IsNullOrEmpty(target.Dutch) ? LangDetailModel.Dutch : target.Dutch;
                                    target.English = string.IsNullOrEmpty(target.English) ? LangDetailModel.English : target.English;
                                    target.Estonian = string.IsNullOrEmpty(target.Estonian) ? LangDetailModel.Estonian : target.Estonian;
                                    target.Finnish = string.IsNullOrEmpty(target.Finnish) ? LangDetailModel.Finnish : target.Finnish;
                                    target.French = string.IsNullOrEmpty(target.French) ? LangDetailModel.French : target.French;
                                    target.German = string.IsNullOrEmpty(target.German) ? LangDetailModel.German : target.German;
                                    target.Greek = string.IsNullOrEmpty(target.Greek) ? LangDetailModel.Greek : target.Greek;
                                    target.Hungarian = string.IsNullOrEmpty(target.Hungarian) ? LangDetailModel.Hungarian : target.Hungarian;
                                    target.Italian = string.IsNullOrEmpty(target.Italian) ? LangDetailModel.Italian : target.Italian;
                                    target.Latvian = string.IsNullOrEmpty(target.Latvian) ? LangDetailModel.Latvian : target.Latvian;
                                    target.Lithuanian = string.IsNullOrEmpty(target.Lithuanian) ? LangDetailModel.Lithuanian : target.Lithuanian;
                                    target.Macedonian = string.IsNullOrEmpty(target.Macedonian) ? LangDetailModel.Macedonian : target.Macedonian;
                                    target.Norwegian = string.IsNullOrEmpty(target.Norwegian) ? LangDetailModel.Norwegian : target.Norwegian;
                                    target.Polish = string.IsNullOrEmpty(target.Polish) ? LangDetailModel.Polish : target.Polish;
                                    target.Portuguese = string.IsNullOrEmpty(target.Portuguese) ? LangDetailModel.Portuguese : target.Portuguese;
                                    target.Romanian = string.IsNullOrEmpty(target.Romanian) ? LangDetailModel.Romanian : target.Romanian;
                                    target.Russian = string.IsNullOrEmpty(target.Russian) ? LangDetailModel.Russian : target.Russian;
                                    target.Slovak = string.IsNullOrEmpty(target.Slovak) ? LangDetailModel.Slovak : target.Slovak;
                                    target.Slovene = string.IsNullOrEmpty(target.Slovene) ? LangDetailModel.Slovene : target.Slovene;
                                    target.Spanish = string.IsNullOrEmpty(target.Spanish) ? LangDetailModel.Spanish : target.Spanish;
                                    target.Swedish = string.IsNullOrEmpty(target.Swedish) ? LangDetailModel.Swedish : target.Swedish;
                                    target.Turkish = string.IsNullOrEmpty(target.Turkish) ? LangDetailModel.Turkish : target.Turkish;
                                    target.Ukrainian = string.IsNullOrEmpty(target.Ukrainian) ? LangDetailModel.Ukrainian : target.Ukrainian;
                                    target.Belarusian = string.IsNullOrEmpty(target.Belarusian) ? LangDetailModel.Belarusian : target.Belarusian;
                                    target.UpdatedBy = userName;
                                    target.UpdatedOn = createdDate;
                                    context.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public LangDetail GetPropertiesLangDetail(LangDetail LangInfo)
        {
            LangDetail updatedresult = new LangDetail();

            using (var context = new Creatis_Context())
            {

                var propertyName = context.Questions.Where(x => x.QuestionId == LangInfo.RefId).Select(x => x.PropertyName).FirstOrDefault();
                propertyName = propertyName.Replace(" ", "_").Replace("/", "_");
                int? refId = 0;
                int langRefColTableId = 0;
                int langRefTableId = 0;
                if (propertyName == EnumToolBoxItems.Choice.ToString())
                {
                    refId = context.ChioceProperties.Where(x => x.QuestionRefId == LangInfo.RefId).Select(x => x.ChioceId).FirstOrDefault();
                    langRefColTableId = (int)LangRefColTable.ChioceProperties_Title;
                    langRefTableId = (int)LangRefTable.ChioceProperties;
                }
                else if (propertyName == EnumToolBoxItems.Add_Chioce.ToString())
                {
                    refId = context.AddChioceProperties.Where(x => x.QuestionRefId == LangInfo.RefId).Select(x => x.AddChioceId).FirstOrDefault();
                    langRefColTableId = (int)LangRefColTable.AddChioceProperties_Title;
                    langRefTableId = (int)LangRefTable.AddChioceProperties;
                }
                else if (propertyName == EnumToolBoxItems.Entry.ToString())
                {
                    refId = context.EntryProperties.Where(x => x.QuestionRefId == LangInfo.RefId).Select(x => x.EntryId).FirstOrDefault();
                    langRefColTableId = (int)LangRefColTable.EntryProperties_Title;
                    langRefTableId = (int)LangRefTable.EntryProperties;
                }
                else if (propertyName == EnumToolBoxItems.Transport_Type.ToString())
                {
                    refId = context.TransportTypeProperties.Where(x => x.QuestionRefId == LangInfo.RefId).Select(x => x.TransportTypeId).FirstOrDefault();
                    langRefColTableId = (int)LangRefColTable.TransportTypeProperties_Title;
                    langRefTableId = (int)LangRefTable.TransportTypeProperties;
                }
                else if (propertyName == EnumToolBoxItems.Transport_Type_Detail.ToString())
                {
                    refId = context.TransportTypePropertiesDetail.Where(x => x.QuestionRefId == LangInfo.RefId).Select(x => x.TransportTypeDetailId).FirstOrDefault();
                    langRefColTableId = (int)LangRefColTable.TransportTypePropertiesDetail_Title;
                    langRefTableId = (int)LangRefTable.TransportTypePropertiesDetail;
                }

                if (refId > 0 && langRefColTableId > 0)
                {
                    var langRefDetail = context.LangRefDetail.Where(x => x.RefId == refId && x.LangRefColTableId == langRefColTableId && x.LayoutRefId == LangInfo.LayoutRefId).Select(x => new { x.Id, x.LangDetailId }).FirstOrDefault();
                    if (langRefDetail != null)
                    {
                        updatedresult = context.LangDetail.Where(x => x.Id == langRefDetail.LangDetailId).FirstOrDefault();

                        updatedresult.LangRefDetailId = langRefDetail.Id;
                        updatedresult.LangRefTableId = langRefTableId;
                        updatedresult.LangRefColTableId = langRefColTableId;
                        updatedresult.Arabic = LangInfo.Arabic;
                        updatedresult.Bulgarian = LangInfo.Bulgarian;
                        updatedresult.Croatian = LangInfo.Croatian;
                        updatedresult.Czech = LangInfo.Czech;
                        updatedresult.Danish = LangInfo.Danish;
                        updatedresult.Dutch = LangInfo.Dutch;
                        updatedresult.English = LangInfo.English;
                        updatedresult.Estonian = LangInfo.Estonian;
                        updatedresult.Finnish = LangInfo.Finnish;
                        updatedresult.French = LangInfo.French;
                        updatedresult.German = LangInfo.German;
                        updatedresult.Greek = LangInfo.Greek;
                        updatedresult.Hungarian = LangInfo.Hungarian;
                        updatedresult.Italian = LangInfo.Italian;
                        updatedresult.Latvian = LangInfo.Latvian;
                        updatedresult.Lithuanian = LangInfo.Lithuanian;
                        updatedresult.Macedonian = LangInfo.Macedonian;
                        updatedresult.Norwegian = LangInfo.Norwegian;
                        updatedresult.Polish = LangInfo.Polish;
                        updatedresult.Portuguese = LangInfo.Portuguese;
                        updatedresult.Romanian = LangInfo.Romanian;
                        updatedresult.Russian = LangInfo.Russian;
                        updatedresult.Slovak = LangInfo.Slovak;
                        updatedresult.Slovene = LangInfo.Slovene;
                        updatedresult.Spanish = LangInfo.Spanish;
                        updatedresult.Swedish = LangInfo.Swedish;
                        updatedresult.Turkish = LangInfo.Turkish;
                        updatedresult.Ukrainian = LangInfo.Ukrainian;
                        updatedresult.Belarusian = LangInfo.Belarusian;
                    }
                }
            }

            return updatedresult;
        }
        public LangDetail GetQuestionsLangDetail(LangDetail LangInfo, int QuestionRefId)
        {
            LangDetail updatedresult = new LangDetail();

            using (var context = new Creatis_Context())
            {
                List<int> smeQuestionsTitleList = new List<int>() {
                    (int)LangRefColTable.ChioceProperties_Title,
                    (int)LangRefColTable.AddChioceProperties_Title,
                    (int)LangRefColTable.EntryProperties_Title,
                    (int)LangRefColTable.TrailerProperties_Title,
                    (int)LangRefColTable.TransportTypeProperties_Title,
                    (int)LangRefColTable.TransportTypePropertiesDetail_Title,
                    (int)LangRefColTable.ExtraInfoProperties_Title,
                    (int)LangRefColTable.PictureProperties_Title,
                    (int)LangRefColTable.ProblemsProperties_Title,
                    (int)LangRefColTable.SignaturesProperties_Title,
                    (int)LangRefColTable.DocumentScanner_Title,
                    (int)LangRefColTable.PalletsProperties_Title,
                    (int)LangRefColTable.ExtraInfo_Title,
                    (int)LangRefColTable.Pallets_Title,
                    (int)LangRefColTable.Problems_Title,
                    (int)LangRefColTable.DocumentScanner_Title,
                     (int)LangRefColTable.PlanningselectProperties_OtherTitle,
                      (int)LangRefColTable.DocumetScanProperties_Title
                };

                if (smeQuestionsTitleList.Any(x => x == LangInfo.LangRefColTableId))
                {
                    var langRefDetail = context.LangRefDetail.Where(x => x.RefId == QuestionRefId && x.LangRefColTableId == (int)LangRefColTable.Questions_Name && x.LayoutRefId == LangInfo.LayoutRefId).Select(x => new { x.Id, x.LangDetailId, x.CustomerRefId }).FirstOrDefault();
                    if (langRefDetail != null)
                    {
                        updatedresult = context.LangDetail.Where(x => x.Id == langRefDetail.LangDetailId).FirstOrDefault();

                        updatedresult.CustomerRefId = langRefDetail.CustomerRefId ?? 0;
                        updatedresult.LangRefDetailId = langRefDetail.Id;
                        updatedresult.LangRefTableId = (int)LangRefTable.Questions;
                        updatedresult.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                        updatedresult.Arabic = LangInfo.Arabic;
                        updatedresult.Bulgarian = LangInfo.Bulgarian;
                        updatedresult.Croatian = LangInfo.Croatian;
                        updatedresult.Czech = LangInfo.Czech;
                        updatedresult.Danish = LangInfo.Danish;
                        updatedresult.Dutch = LangInfo.Dutch;
                        updatedresult.English = LangInfo.English;
                        updatedresult.Estonian = LangInfo.Estonian;
                        updatedresult.Finnish = LangInfo.Finnish;
                        updatedresult.French = LangInfo.French;
                        updatedresult.German = LangInfo.German;
                        updatedresult.Greek = LangInfo.Greek;
                        updatedresult.Hungarian = LangInfo.Hungarian;
                        updatedresult.Italian = LangInfo.Italian;
                        updatedresult.Latvian = LangInfo.Latvian;
                        updatedresult.Lithuanian = LangInfo.Lithuanian;
                        updatedresult.Macedonian = LangInfo.Macedonian;
                        updatedresult.Norwegian = LangInfo.Norwegian;
                        updatedresult.Polish = LangInfo.Polish;
                        updatedresult.Portuguese = LangInfo.Portuguese;
                        updatedresult.Romanian = LangInfo.Romanian;
                        updatedresult.Russian = LangInfo.Russian;
                        updatedresult.Slovak = LangInfo.Slovak;
                        updatedresult.Slovene = LangInfo.Slovene;
                        updatedresult.Spanish = LangInfo.Spanish;
                        updatedresult.Swedish = LangInfo.Swedish;
                        updatedresult.Turkish = LangInfo.Turkish;
                        updatedresult.Ukrainian = LangInfo.Ukrainian;
                        updatedresult.Belarusian = LangInfo.Belarusian;
                    }
                }

            }

            return updatedresult;
        }
        public LangDetail GetPlanningSelectLangDetail(LangDetail LangInfo, int QuestionRefId)
        {
            LangDetail updatedresult = new LangDetail();

            using (var context = new Creatis_Context())
            {
                var planningLevel = context.PlanningselectProperties.Where(x => x.QuestionRefId == QuestionRefId).Select(x => x.Planninglevel).FirstOrDefault();

                var langRefDetail = context.LangRefDetail.Where(x => x.RefId == QuestionRefId && x.LangRefColTableId == (int)LangRefColTable.Questions_Name && x.LayoutRefId == LangInfo.LayoutRefId).Select(x => new { x.Id, x.LangDetailId, x.CustomerRefId }).FirstOrDefault();
                if (langRefDetail != null)
                {
                    updatedresult = context.LangDetail.Where(x => x.Id == langRefDetail.LangDetailId).FirstOrDefault();

                    updatedresult.CustomerRefId = langRefDetail.CustomerRefId ?? 0;
                    updatedresult.LangRefDetailId = langRefDetail.Id;
                    updatedresult.LangRefTableId = (int)LangRefTable.Questions;
                    updatedresult.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                    updatedresult.Arabic = (LangInfo.Arabic != null && LangInfo.Arabic.Length > 0) ? LangInfo.Arabic + " [ " + planningLevel + " ]" : LangInfo.Arabic;
                    updatedresult.Bulgarian = (LangInfo.Bulgarian != null && LangInfo.Bulgarian.Length > 0) ? LangInfo.Bulgarian + " [ " + planningLevel + " ]" : LangInfo.Bulgarian;
                    updatedresult.Croatian = (LangInfo.Croatian != null && LangInfo.Croatian.Length > 0) ? LangInfo.Croatian + " [ " + planningLevel + " ]" : LangInfo.Croatian;
                    updatedresult.Czech = (LangInfo.Czech != null && LangInfo.Czech.Length > 0) ? LangInfo.Czech + " [ " + planningLevel + " ]" : LangInfo.Czech;
                    updatedresult.Danish = (LangInfo.Danish != null && LangInfo.Danish.Length > 0) ? LangInfo.Danish + " [ " + planningLevel + " ]" : LangInfo.Danish;
                    updatedresult.Dutch = (LangInfo.Dutch != null && LangInfo.Dutch.Length > 0) ? LangInfo.Dutch + " [ " + planningLevel + " ]" : LangInfo.Dutch;
                    updatedresult.English = (LangInfo.English != null && LangInfo.English.Length > 0) ? LangInfo.English + " [ " + planningLevel + " ]" : LangInfo.English;
                    updatedresult.Estonian = (LangInfo.Estonian != null && LangInfo.Estonian.Length > 0) ? LangInfo.Estonian + " [ " + planningLevel + " ]" : LangInfo.Estonian;
                    updatedresult.Finnish = (LangInfo.Finnish != null && LangInfo.Finnish.Length > 0) ? LangInfo.Finnish + " [ " + planningLevel + " ]" : LangInfo.Finnish;
                    updatedresult.French = (LangInfo.French != null && LangInfo.French.Length > 0) ? LangInfo.French + " [ " + planningLevel + " ]" : LangInfo.French;
                    updatedresult.German = (LangInfo.German != null && LangInfo.German.Length > 0) ? LangInfo.German + " [ " + planningLevel + " ]" : LangInfo.German;
                    updatedresult.Greek = (LangInfo.Greek != null && LangInfo.Greek.Length > 0) ? LangInfo.Greek + " [ " + planningLevel + " ]" : LangInfo.Greek;
                    updatedresult.Hungarian = (LangInfo.Hungarian != null && LangInfo.Hungarian.Length > 0) ? LangInfo.Hungarian + " [ " + planningLevel + " ]" : LangInfo.Hungarian;
                    updatedresult.Italian = (LangInfo.Italian != null && LangInfo.Italian.Length > 0) ? LangInfo.Italian + " [ " + planningLevel + " ]" : LangInfo.Italian;
                    updatedresult.Latvian = (LangInfo.Latvian != null && LangInfo.Latvian.Length > 0) ? LangInfo.Latvian + " [ " + planningLevel + " ]" : LangInfo.Latvian;
                    updatedresult.Lithuanian = (LangInfo.Lithuanian != null && LangInfo.Lithuanian.Length > 0) ? LangInfo.Lithuanian + " [ " + planningLevel + " ]" : LangInfo.Lithuanian;
                    updatedresult.Macedonian = (LangInfo.Macedonian != null && LangInfo.Macedonian.Length > 0) ? LangInfo.Macedonian + " [ " + planningLevel + " ]" : LangInfo.Macedonian;
                    updatedresult.Norwegian = (LangInfo.Norwegian != null && LangInfo.Norwegian.Length > 0) ? LangInfo.Norwegian + " [ " + planningLevel + " ]" : LangInfo.Norwegian;
                    updatedresult.Polish = (LangInfo.Polish != null && LangInfo.Polish.Length > 0) ? LangInfo.Polish + " [ " + planningLevel + " ]" : LangInfo.Polish;
                    updatedresult.Portuguese = (LangInfo.Portuguese != null && LangInfo.Portuguese.Length > 0) ? LangInfo.Portuguese + " [ " + planningLevel + " ]" : LangInfo.Portuguese;
                    updatedresult.Romanian = (LangInfo.Romanian != null && LangInfo.Romanian.Length > 0) ? LangInfo.Romanian + " [ " + planningLevel + " ]" : LangInfo.Romanian;
                    updatedresult.Russian = (LangInfo.Russian != null && LangInfo.Russian.Length > 0) ? LangInfo.Russian + " [ " + planningLevel + " ]" : LangInfo.Russian;
                    updatedresult.Slovak = (LangInfo.Slovak != null && LangInfo.Slovak.Length > 0) ? LangInfo.Slovak + " [ " + planningLevel + " ]" : LangInfo.Slovak;
                    updatedresult.Slovene = (LangInfo.Slovene != null && LangInfo.Slovene.Length > 0) ? LangInfo.Slovene + " [ " + planningLevel + " ]" : LangInfo.Slovene;
                    updatedresult.Spanish = (LangInfo.Spanish != null && LangInfo.Spanish.Length > 0) ? LangInfo.Spanish + " [ " + planningLevel + " ]" : LangInfo.Spanish;
                    updatedresult.Swedish = (LangInfo.Swedish != null && LangInfo.Swedish.Length > 0) ? LangInfo.Swedish + " [ " + planningLevel + " ]" : LangInfo.Swedish;
                    updatedresult.Turkish = (LangInfo.Turkish != null && LangInfo.Turkish.Length > 0) ? LangInfo.Turkish + " [ " + planningLevel + " ]" : LangInfo.Turkish;
                    updatedresult.Ukrainian = (LangInfo.Ukrainian != null && LangInfo.Ukrainian.Length > 0) ? LangInfo.Ukrainian + " [ " + planningLevel + " ]" : LangInfo.Ukrainian;
                    updatedresult.Belarusian = (LangInfo.Belarusian != null && LangInfo.Belarusian.Length > 0) ? LangInfo.Belarusian + " [ " + planningLevel + " ]" : LangInfo.Belarusian;
                }

            }

            return updatedresult;
        }
        public List<UITranslationDetail> GetCommonUITranslation()
        {
            List<UITranslationDetail> translationdetail = new List<UITranslationDetail>();
            using (var context = new Creatis_Context())
            {
                translationdetail = context.UITranslationDetail.Select(x => new UITranslationDetail()
                {
                    UITranslationDetailId = x.UITranslationDetailId,
                    Arabic = x.Arabic,
                    Bulgarian = x.Bulgarian,
                    Croatian = x.Croatian,
                    Czech = x.Czech,
                    Danish = x.Danish,
                    Dutch = x.Dutch,
                    English = x.English,
                    Estonian = x.Estonian,
                    Finnish = x.Finnish,
                    French = x.French,
                    German = x.German,
                    Greek = x.Greek,
                    Hungarian = x.Hungarian,
                    Italian = x.Italian,
                    Latvian = x.Latvian,
                    Lithuanian = x.Lithuanian,
                    Macedonian = x.Macedonian,
                    Norwegian = x.Norwegian,
                    Polish = x.Polish,
                    Portuguese = x.Portuguese,
                    Romanian = x.Romanian,
                    Russian = x.Russian,
                    Slovak = x.Slovak,
                    Slovene = x.Slovene,
                    Spanish = x.Spanish,
                    Swedish = x.Swedish,
                    Turkish = x.Turkish,
                    Ukrainian = x.Ukrainian,
                    Belarusian = x.Belarusian,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn
                }).Distinct().OrderByDescending(x => x.UpdatedOn).ToList();
            }

            return translationdetail;
        }
        public string SaveCommonUITranslation(UITranslationDetail LangDetailModel)
        {
            var result = "success";
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (LangDetailModel.UITranslationDetailId > 0)
                    {
                        var target = context.UITranslationDetail.Where(x => x.UITranslationDetailId == LangDetailModel.UITranslationDetailId).FirstOrDefault();
                        if (target != null)
                        {
                            target.Arabic = LangDetailModel.Arabic;
                            target.Bulgarian = LangDetailModel.Bulgarian;
                            target.Croatian = LangDetailModel.Croatian;
                            target.Czech = LangDetailModel.Czech;
                            target.Danish = LangDetailModel.Danish;
                            target.Dutch = LangDetailModel.Dutch;
                            target.English = LangDetailModel.English;
                            target.Estonian = LangDetailModel.Estonian;
                            target.Finnish = LangDetailModel.Finnish;
                            target.French = LangDetailModel.French;
                            target.German = LangDetailModel.German;
                            target.Greek = LangDetailModel.Greek;
                            target.Hungarian = LangDetailModel.Hungarian;
                            target.Italian = LangDetailModel.Italian;
                            target.Latvian = LangDetailModel.Latvian;
                            target.Lithuanian = LangDetailModel.Lithuanian;
                            target.Macedonian = LangDetailModel.Macedonian;
                            target.Norwegian = LangDetailModel.Norwegian;
                            target.Polish = LangDetailModel.Polish;
                            target.Portuguese = LangDetailModel.Portuguese;
                            target.Romanian = LangDetailModel.Romanian;
                            target.Russian = LangDetailModel.Russian;
                            target.Slovak = LangDetailModel.Slovak;
                            target.Slovene = LangDetailModel.Slovene;
                            target.Spanish = LangDetailModel.Spanish;
                            target.Swedish = LangDetailModel.Swedish;
                            target.Turkish = LangDetailModel.Turkish;
                            target.Ukrainian = LangDetailModel.Ukrainian;
                            target.Belarusian = LangDetailModel.Belarusian;
                            target.UpdatedBy = LangDetailModel.UpdatedBy;
                            target.UpdatedOn = LangDetailModel.UpdatedOn ?? DateTime.Now;
                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public List<UILangDetail> GetSpecificUITranslation()
        {
            List<UILangDetail> specificTranslationDetail = new List<UILangDetail>();
            using (var context = new Creatis_Context())
            {
                specificTranslationDetail = context.UILangDetail.FromSql("GetUITranslations").ToList();

                specificTranslationDetail = specificTranslationDetail.Select(x => new UILangDetail()
                {
                    UITranslationId = x.UITranslationId,
                    UITranslationDetailRefId = x.UITranslationDetailRefId,
                    Title = x.Title,
                    Arabic = x.Arabic,
                    Bulgarian = x.Bulgarian,
                    Croatian = x.Croatian,
                    Czech = x.Czech,
                    Danish = x.Danish,
                    Dutch = x.Dutch,
                    English = x.English,
                    Estonian = x.Estonian,
                    Finnish = x.Finnish,
                    French = x.French,
                    German = x.German,
                    Greek = x.Greek,
                    Hungarian = x.Hungarian,
                    Italian = x.Italian,
                    Latvian = x.Latvian,
                    Lithuanian = x.Lithuanian,
                    Macedonian = x.Macedonian,
                    Norwegian = x.Norwegian,
                    Polish = x.Polish,
                    Portuguese = x.Portuguese,
                    Romanian = x.Romanian,
                    Russian = x.Russian,
                    Slovak = x.Slovak,
                    Slovene = x.Slovene,
                    Spanish = x.Spanish,
                    Swedish = x.Swedish,
                    Turkish = x.Turkish,
                    Ukrainian = x.Ukrainian,
                    Belarusian = x.Belarusian
                }).Distinct().OrderByDescending(x => x.English).ToList();
            }

            return specificTranslationDetail;
        }
        public string SaveSpecificUITranslation(UILangDetail UILangDetailModel)
        {
            var result = "success";
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (UILangDetailModel.UITranslationDetailRefId > 0)
                    {
                        var uILangDetailCount = context.UITranslation.Where(x => x.UITranslationDetailRefId == UILangDetailModel.UITranslationDetailRefId && x.UITranslationDetail.English != UILangDetailModel.English).Count();
                        if (uILangDetailCount > 1)
                        {
                            UITranslationDetail uITranslationModel = new UITranslationDetail();

                            uITranslationModel.Arabic = UILangDetailModel.Arabic;
                            uITranslationModel.Bulgarian = UILangDetailModel.Bulgarian;
                            uITranslationModel.Croatian = UILangDetailModel.Croatian;
                            uITranslationModel.Czech = UILangDetailModel.Czech;
                            uITranslationModel.Danish = UILangDetailModel.Danish;
                            uITranslationModel.Dutch = UILangDetailModel.Dutch;
                            uITranslationModel.English = UILangDetailModel.English;
                            uITranslationModel.Estonian = UILangDetailModel.Estonian;
                            uITranslationModel.Finnish = UILangDetailModel.Finnish;
                            uITranslationModel.French = UILangDetailModel.French;
                            uITranslationModel.German = UILangDetailModel.German;
                            uITranslationModel.Greek = UILangDetailModel.Greek;
                            uITranslationModel.Hungarian = UILangDetailModel.Hungarian;
                            uITranslationModel.Italian = UILangDetailModel.Italian;
                            uITranslationModel.Latvian = UILangDetailModel.Latvian;
                            uITranslationModel.Lithuanian = UILangDetailModel.Lithuanian;
                            uITranslationModel.Macedonian = UILangDetailModel.Macedonian;
                            uITranslationModel.Norwegian = UILangDetailModel.Norwegian;
                            uITranslationModel.Polish = UILangDetailModel.Polish;
                            uITranslationModel.Portuguese = UILangDetailModel.Portuguese;
                            uITranslationModel.Romanian = UILangDetailModel.Romanian;
                            uITranslationModel.Russian = UILangDetailModel.Russian;
                            uITranslationModel.Slovak = UILangDetailModel.Slovak;
                            uITranslationModel.Slovene = UILangDetailModel.Slovene;
                            uITranslationModel.Spanish = UILangDetailModel.Spanish;
                            uITranslationModel.Swedish = UILangDetailModel.Swedish;
                            uITranslationModel.Turkish = UILangDetailModel.Turkish;
                            uITranslationModel.Ukrainian = UILangDetailModel.Ukrainian;
                            uITranslationModel.Belarusian = UILangDetailModel.Belarusian;
                            uITranslationModel.CreatedBy = UILangDetailModel.UpdatedBy;
                            uITranslationModel.CreatedOn = UILangDetailModel.UpdatedOn ?? DateTime.Now;
                            uITranslationModel.UpdatedBy = UILangDetailModel.UpdatedBy;
                            uITranslationModel.UpdatedOn = UILangDetailModel.UpdatedOn ?? DateTime.Now;
                            context.UITranslationDetail.Add(uITranslationModel);
                            context.SaveChanges();

                            var targetUITranslation = context.UITranslation.Where(x => x.UITranslationId == UILangDetailModel.UITranslationId).FirstOrDefault();

                            if (targetUITranslation != null)
                            {
                                targetUITranslation.UITranslationDetailRefId = uITranslationModel.UITranslationDetailId;
                                targetUITranslation.UpdatedBy = UILangDetailModel.UpdatedBy;
                                targetUITranslation.UpdatedOn = UILangDetailModel.UpdatedOn ?? DateTime.Now;
                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            var target = context.UITranslationDetail.Where(x => x.UITranslationDetailId == UILangDetailModel.UITranslationDetailRefId).FirstOrDefault();
                            if (target != null)
                            {
                                target.Arabic = UILangDetailModel.Arabic;
                                target.Bulgarian = UILangDetailModel.Bulgarian;
                                target.Croatian = UILangDetailModel.Croatian;
                                target.Czech = UILangDetailModel.Czech;
                                target.Danish = UILangDetailModel.Danish;
                                target.Dutch = UILangDetailModel.Dutch;
                                target.English = UILangDetailModel.English;
                                target.Estonian = UILangDetailModel.Estonian;
                                target.Finnish = UILangDetailModel.Finnish;
                                target.French = UILangDetailModel.French;
                                target.German = UILangDetailModel.German;
                                target.Greek = UILangDetailModel.Greek;
                                target.Hungarian = UILangDetailModel.Hungarian;
                                target.Italian = UILangDetailModel.Italian;
                                target.Latvian = UILangDetailModel.Latvian;
                                target.Lithuanian = UILangDetailModel.Lithuanian;
                                target.Macedonian = UILangDetailModel.Macedonian;
                                target.Norwegian = UILangDetailModel.Norwegian;
                                target.Polish = UILangDetailModel.Polish;
                                target.Portuguese = UILangDetailModel.Portuguese;
                                target.Romanian = UILangDetailModel.Romanian;
                                target.Russian = UILangDetailModel.Russian;
                                target.Slovak = UILangDetailModel.Slovak;
                                target.Slovene = UILangDetailModel.Slovene;
                                target.Spanish = UILangDetailModel.Spanish;
                                target.Swedish = UILangDetailModel.Swedish;
                                target.Turkish = UILangDetailModel.Turkish;
                                target.Ukrainian = UILangDetailModel.Ukrainian;
                                target.Belarusian = UILangDetailModel.Belarusian;
                                target.UpdatedBy = UILangDetailModel.UpdatedBy;
                                target.UpdatedOn = UILangDetailModel.UpdatedOn ?? DateTime.Now;
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public string ImportCentralTranslations(LangDetail langdetail)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    LangDetail returnmodal = new LangDetail();

                    returnmodal = context.LangDetail.Where(x => x.Id == langdetail.Id).FirstOrDefault();
                    if (returnmodal != null)
                    {
                        returnmodal.Arabic = langdetail.Arabic;
                        returnmodal.Bulgarian = langdetail.Bulgarian;
                        returnmodal.Croatian = langdetail.Croatian;
                        returnmodal.Czech = langdetail.Czech;
                        returnmodal.Danish = langdetail.Danish;
                        returnmodal.Dutch = langdetail.Dutch;
                        returnmodal.English = langdetail.English;
                        returnmodal.Estonian = langdetail.Estonian;
                        returnmodal.Finnish = langdetail.Finnish;
                        returnmodal.French = langdetail.French;
                        returnmodal.German = langdetail.German;
                        returnmodal.Greek = langdetail.Greek;
                        returnmodal.Hungarian = langdetail.Hungarian;
                        returnmodal.Italian = langdetail.Italian;
                        returnmodal.Latvian = langdetail.Latvian;
                        returnmodal.Lithuanian = langdetail.Lithuanian;
                        returnmodal.Macedonian = langdetail.Macedonian;
                        returnmodal.Norwegian = langdetail.Norwegian;
                        returnmodal.Polish = langdetail.Polish;
                        returnmodal.Portuguese = langdetail.Portuguese;
                        returnmodal.Romanian = langdetail.Romanian;
                        returnmodal.Russian = langdetail.Russian;
                        returnmodal.Slovak = langdetail.Slovak;
                        returnmodal.Slovene = langdetail.Slovene;
                        returnmodal.Spanish = langdetail.Spanish;
                        returnmodal.Swedish = langdetail.Swedish;
                        returnmodal.Turkish = langdetail.Turkish;
                        returnmodal.Ukrainian = langdetail.Ukrainian;
                        returnmodal.Belarusian = langdetail.Belarusian;
                        returnmodal.UpdatedBy = langdetail.CreatedBy;
                        returnmodal.UpdatedOn = DateTime.Now;

                        context.SaveChanges();

                        return "";
                    }
                    else
                        return "Error";
                }
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }
        public UITranslationDetail GetUITranslationDetail(int id)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return (context.UITranslationDetail.Where(x => x.UITranslationDetailId == id).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                return new UITranslationDetail();
            }
        }
        public UILangDetail GetUILangDetail(int id)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var specificTranslationDetail = context.UILangDetail.FromSql("GetUITranslations").ToList();
                    var uiLangDetail = specificTranslationDetail.Where(x => x.UITranslationId == id).FirstOrDefault();
                    return uiLangDetail;
                }
            }
            catch (Exception ex)
            {
                return new UILangDetail();
            }
        }
        public int UpgradeLanguageDetail(int upgradeLangDetailId, int existingLangDetailId, LangDetail langDetail)
        {
            using (var context = new Creatis_Context())
            {
                var upgradeLangDetail = context.LangDetail.FirstOrDefault(x => x.Id == upgradeLangDetailId);
                var existingLangDetail = context.LangDetail.FirstOrDefault(x => x.Id == existingLangDetailId);

                if ((existingLangDetail.Arabic != null && existingLangDetail.Arabic.Length > 0) && (upgradeLangDetail.Arabic == null || (upgradeLangDetail.Arabic != null && upgradeLangDetail.Arabic.Length == 0)))
                    upgradeLangDetail.Arabic = existingLangDetail.Arabic;
                if ((existingLangDetail.Bulgarian != null && existingLangDetail.Bulgarian.Length > 0) && (upgradeLangDetail.Bulgarian == null || (upgradeLangDetail.Bulgarian != null && upgradeLangDetail.Bulgarian.Length == 0)))
                    upgradeLangDetail.Bulgarian = existingLangDetail.Bulgarian;
                if ((existingLangDetail.Croatian != null && existingLangDetail.Croatian.Length > 0) && (upgradeLangDetail.Croatian == null || (upgradeLangDetail.Croatian != null && upgradeLangDetail.Croatian.Length == 0)))
                    upgradeLangDetail.Croatian = existingLangDetail.Croatian;
                if ((existingLangDetail.Czech != null && existingLangDetail.Czech.Length > 0) && (upgradeLangDetail.Czech == null || (upgradeLangDetail.Czech != null && upgradeLangDetail.Czech.Length == 0)))
                    upgradeLangDetail.Czech = existingLangDetail.Czech;
                if ((existingLangDetail.Danish != null && existingLangDetail.Danish.Length > 0) && (upgradeLangDetail.Danish == null || (upgradeLangDetail.Danish != null && upgradeLangDetail.Danish.Length == 0)))
                    upgradeLangDetail.Danish = existingLangDetail.Danish;
                if ((existingLangDetail.Dutch != null && existingLangDetail.Dutch.Length > 0) && (upgradeLangDetail.Dutch == null || (upgradeLangDetail.Dutch != null && upgradeLangDetail.Dutch.Length == 0)))
                    upgradeLangDetail.Dutch = existingLangDetail.Dutch;
                if ((existingLangDetail.English != null && existingLangDetail.English.Length > 0) && (upgradeLangDetail.English == null || (upgradeLangDetail.English != null && upgradeLangDetail.English.Length == 0)))
                    upgradeLangDetail.English = existingLangDetail.English;
                if ((existingLangDetail.Estonian != null && existingLangDetail.Estonian.Length > 0) && (upgradeLangDetail.Estonian == null || (upgradeLangDetail.Estonian != null && upgradeLangDetail.Estonian.Length == 0)))
                    upgradeLangDetail.Estonian = existingLangDetail.Estonian;
                if ((existingLangDetail.Finnish != null && existingLangDetail.Finnish.Length > 0) && (upgradeLangDetail.Finnish == null || (upgradeLangDetail.Finnish != null && upgradeLangDetail.Finnish.Length == 0)))
                    upgradeLangDetail.Finnish = existingLangDetail.Finnish;
                if ((existingLangDetail.Finnish != null && existingLangDetail.Finnish.Length > 0) && (upgradeLangDetail.Finnish == null || (upgradeLangDetail.Finnish != null && upgradeLangDetail.Finnish.Length == 0)))
                    upgradeLangDetail.French = existingLangDetail.French;
                if ((existingLangDetail.German != null && existingLangDetail.German.Length > 0) && (upgradeLangDetail.German == null || (upgradeLangDetail.German != null && upgradeLangDetail.German.Length == 0)))
                    upgradeLangDetail.German = existingLangDetail.German;
                if ((existingLangDetail.Greek != null && existingLangDetail.Greek.Length > 0) && (upgradeLangDetail.Greek == null || (upgradeLangDetail.Greek != null && upgradeLangDetail.Greek.Length == 0)))
                    upgradeLangDetail.Greek = existingLangDetail.Greek;
                if ((existingLangDetail.Hungarian != null && existingLangDetail.Hungarian.Length > 0) && (upgradeLangDetail.Hungarian == null || (upgradeLangDetail.Hungarian != null && upgradeLangDetail.Hungarian.Length == 0)))
                    upgradeLangDetail.Hungarian = existingLangDetail.Hungarian;
                if ((existingLangDetail.Italian != null && existingLangDetail.Italian.Length > 0) && (upgradeLangDetail.Italian == null || (upgradeLangDetail.Italian != null && upgradeLangDetail.Italian.Length == 0)))
                    upgradeLangDetail.Italian = existingLangDetail.Italian;
                if ((existingLangDetail.Latvian != null && existingLangDetail.Latvian.Length > 0) && (upgradeLangDetail.Latvian == null || (upgradeLangDetail.Latvian != null && upgradeLangDetail.Latvian.Length == 0)))
                    upgradeLangDetail.Latvian = existingLangDetail.Latvian;
                if ((existingLangDetail.Lithuanian != null && existingLangDetail.Lithuanian.Length > 0) && (upgradeLangDetail.Lithuanian == null || (upgradeLangDetail.Lithuanian != null && upgradeLangDetail.Lithuanian.Length == 0)))
                    upgradeLangDetail.Lithuanian = existingLangDetail.Lithuanian;
                if ((existingLangDetail.Macedonian != null && existingLangDetail.Macedonian.Length > 0) && (upgradeLangDetail.Macedonian == null || (upgradeLangDetail.Macedonian != null && upgradeLangDetail.Macedonian.Length == 0)))
                    upgradeLangDetail.Macedonian = existingLangDetail.Macedonian;
                if ((existingLangDetail.Norwegian != null && existingLangDetail.Norwegian.Length > 0) && (upgradeLangDetail.Norwegian == null || (upgradeLangDetail.Norwegian != null && upgradeLangDetail.Norwegian.Length == 0)))
                    upgradeLangDetail.Norwegian = existingLangDetail.Norwegian;
                if ((existingLangDetail.Polish != null && existingLangDetail.Polish.Length > 0) && (upgradeLangDetail.Polish == null || (upgradeLangDetail.Polish != null && upgradeLangDetail.Polish.Length == 0)))
                    upgradeLangDetail.Polish = existingLangDetail.Polish;
                if ((existingLangDetail.Portuguese != null && existingLangDetail.Portuguese.Length > 0) && (upgradeLangDetail.Portuguese == null || (upgradeLangDetail.Portuguese != null && upgradeLangDetail.Portuguese.Length == 0)))
                    upgradeLangDetail.Portuguese = existingLangDetail.Portuguese;
                if ((existingLangDetail.Romanian != null && existingLangDetail.Romanian.Length > 0) && (upgradeLangDetail.Romanian == null || (upgradeLangDetail.Romanian != null && upgradeLangDetail.Romanian.Length == 0)))
                    upgradeLangDetail.Romanian = existingLangDetail.Romanian;
                if ((existingLangDetail.Russian != null && existingLangDetail.Russian.Length > 0) && (upgradeLangDetail.Russian == null || (upgradeLangDetail.Russian != null && upgradeLangDetail.Russian.Length == 0)))
                    upgradeLangDetail.Russian = existingLangDetail.Russian;
                if ((existingLangDetail.Slovak != null && existingLangDetail.Slovak.Length > 0) && (upgradeLangDetail.Slovak == null || (upgradeLangDetail.Slovak != null && upgradeLangDetail.Slovak.Length == 0)))
                    upgradeLangDetail.Slovak = existingLangDetail.Slovak;
                if ((existingLangDetail.Slovene != null && existingLangDetail.Slovene.Length > 0) && (upgradeLangDetail.Slovene == null || (upgradeLangDetail.Slovene != null && upgradeLangDetail.Slovene.Length == 0)))
                    upgradeLangDetail.Slovene = existingLangDetail.Slovene;
                if ((existingLangDetail.Spanish != null && existingLangDetail.Spanish.Length > 0) && (upgradeLangDetail.Spanish == null || (upgradeLangDetail.Spanish != null && upgradeLangDetail.Spanish.Length == 0)))
                    upgradeLangDetail.Spanish = existingLangDetail.Spanish;
                if ((existingLangDetail.Swedish != null && existingLangDetail.Swedish.Length > 0) && (upgradeLangDetail.Swedish == null || (upgradeLangDetail.Swedish != null && upgradeLangDetail.Swedish.Length == 0)))
                    upgradeLangDetail.Swedish = existingLangDetail.Swedish;
                if ((existingLangDetail.Turkish != null && existingLangDetail.Turkish.Length > 0) && (upgradeLangDetail.Turkish == null || (upgradeLangDetail.Turkish != null && upgradeLangDetail.Turkish.Length == 0)))
                    upgradeLangDetail.Turkish = existingLangDetail.Turkish;
                if ((existingLangDetail.Ukrainian != null && existingLangDetail.Ukrainian.Length > 0) && (upgradeLangDetail.Ukrainian == null || (upgradeLangDetail.Ukrainian != null && upgradeLangDetail.Ukrainian.Length == 0)))
                    upgradeLangDetail.Ukrainian = existingLangDetail.Ukrainian;
                if ((existingLangDetail.Belarusian != null && existingLangDetail.Belarusian.Length > 0) && (upgradeLangDetail.Belarusian == null || (upgradeLangDetail.Belarusian != null && upgradeLangDetail.Belarusian.Length == 0)))
                    upgradeLangDetail.Belarusian = existingLangDetail.Belarusian;


                if ((langDetail.Arabic != null && langDetail.Arabic.Length > 0) && (upgradeLangDetail.Arabic == null || (upgradeLangDetail.Arabic != null && upgradeLangDetail.Arabic.Length == 0)))
                    upgradeLangDetail.Arabic = langDetail.Arabic;
                if ((langDetail.Bulgarian != null && langDetail.Bulgarian.Length > 0) && (upgradeLangDetail.Bulgarian == null || (upgradeLangDetail.Bulgarian != null && upgradeLangDetail.Bulgarian.Length == 0)))
                    upgradeLangDetail.Bulgarian = langDetail.Bulgarian;
                if ((langDetail.Croatian != null && langDetail.Croatian.Length > 0) && (upgradeLangDetail.Croatian == null || (upgradeLangDetail.Croatian != null && upgradeLangDetail.Croatian.Length == 0)))
                    upgradeLangDetail.Croatian = langDetail.Croatian;
                if ((langDetail.Czech != null && langDetail.Czech.Length > 0) && (upgradeLangDetail.Czech == null || (upgradeLangDetail.Czech != null && upgradeLangDetail.Czech.Length == 0)))
                    upgradeLangDetail.Czech = langDetail.Czech;
                if ((langDetail.Danish != null && langDetail.Danish.Length > 0) && (upgradeLangDetail.Danish == null || (upgradeLangDetail.Danish != null && upgradeLangDetail.Danish.Length == 0)))
                    upgradeLangDetail.Danish = langDetail.Danish;
                if ((langDetail.Dutch != null && langDetail.Dutch.Length > 0) && (upgradeLangDetail.Dutch == null || (upgradeLangDetail.Dutch != null && upgradeLangDetail.Dutch.Length == 0)))
                    upgradeLangDetail.Dutch = langDetail.Dutch;
                if ((langDetail.English != null && langDetail.English.Length > 0) && (upgradeLangDetail.English == null || (upgradeLangDetail.English != null && upgradeLangDetail.English.Length == 0)))
                    upgradeLangDetail.English = langDetail.English;
                if ((langDetail.Estonian != null && langDetail.Estonian.Length > 0) && (upgradeLangDetail.Estonian == null || (upgradeLangDetail.Estonian != null && upgradeLangDetail.Estonian.Length == 0)))
                    upgradeLangDetail.Estonian = langDetail.Estonian;
                if ((langDetail.Finnish != null && langDetail.Finnish.Length > 0) && (upgradeLangDetail.Finnish == null || (upgradeLangDetail.Finnish != null && upgradeLangDetail.Finnish.Length == 0)))
                    upgradeLangDetail.Finnish = langDetail.Finnish;
                if ((langDetail.Finnish != null && langDetail.Finnish.Length > 0) && (upgradeLangDetail.Finnish == null || (upgradeLangDetail.Finnish != null && upgradeLangDetail.Finnish.Length == 0)))
                    upgradeLangDetail.French = langDetail.French;
                if ((langDetail.German != null && langDetail.German.Length > 0) && (upgradeLangDetail.German == null || (upgradeLangDetail.German != null && upgradeLangDetail.German.Length == 0)))
                    upgradeLangDetail.German = langDetail.German;
                if ((langDetail.Greek != null && langDetail.Greek.Length > 0) && (upgradeLangDetail.Greek == null || (upgradeLangDetail.Greek != null && upgradeLangDetail.Greek.Length == 0)))
                    upgradeLangDetail.Greek = langDetail.Greek;
                if ((langDetail.Hungarian != null && langDetail.Hungarian.Length > 0) && (upgradeLangDetail.Hungarian == null || (upgradeLangDetail.Hungarian != null && upgradeLangDetail.Hungarian.Length == 0)))
                    upgradeLangDetail.Hungarian = langDetail.Hungarian;
                if ((langDetail.Italian != null && langDetail.Italian.Length > 0) && (upgradeLangDetail.Italian == null || (upgradeLangDetail.Italian != null && upgradeLangDetail.Italian.Length == 0)))
                    upgradeLangDetail.Italian = langDetail.Italian;
                if ((langDetail.Latvian != null && langDetail.Latvian.Length > 0) && (upgradeLangDetail.Latvian == null || (upgradeLangDetail.Latvian != null && upgradeLangDetail.Latvian.Length == 0)))
                    upgradeLangDetail.Latvian = langDetail.Latvian;
                if ((langDetail.Lithuanian != null && langDetail.Lithuanian.Length > 0) && (upgradeLangDetail.Lithuanian == null || (upgradeLangDetail.Lithuanian != null && upgradeLangDetail.Lithuanian.Length == 0)))
                    upgradeLangDetail.Lithuanian = langDetail.Lithuanian;
                if ((langDetail.Macedonian != null && langDetail.Macedonian.Length > 0) && (upgradeLangDetail.Macedonian == null || (upgradeLangDetail.Macedonian != null && upgradeLangDetail.Macedonian.Length == 0)))
                    upgradeLangDetail.Macedonian = langDetail.Macedonian;
                if ((langDetail.Norwegian != null && langDetail.Norwegian.Length > 0) && (upgradeLangDetail.Norwegian == null || (upgradeLangDetail.Norwegian != null && upgradeLangDetail.Norwegian.Length == 0)))
                    upgradeLangDetail.Norwegian = langDetail.Norwegian;
                if ((langDetail.Polish != null && langDetail.Polish.Length > 0) && (upgradeLangDetail.Polish == null || (upgradeLangDetail.Polish != null && upgradeLangDetail.Polish.Length == 0)))
                    upgradeLangDetail.Polish = langDetail.Polish;
                if ((langDetail.Portuguese != null && langDetail.Portuguese.Length > 0) && (upgradeLangDetail.Portuguese == null || (upgradeLangDetail.Portuguese != null && upgradeLangDetail.Portuguese.Length == 0)))
                    upgradeLangDetail.Portuguese = langDetail.Portuguese;
                if ((langDetail.Romanian != null && langDetail.Romanian.Length > 0) && (upgradeLangDetail.Romanian == null || (upgradeLangDetail.Romanian != null && upgradeLangDetail.Romanian.Length == 0)))
                    upgradeLangDetail.Romanian = langDetail.Romanian;
                if ((langDetail.Russian != null && langDetail.Russian.Length > 0) && (upgradeLangDetail.Russian == null || (upgradeLangDetail.Russian != null && upgradeLangDetail.Russian.Length == 0)))
                    upgradeLangDetail.Russian = langDetail.Russian;
                if ((langDetail.Slovak != null && langDetail.Slovak.Length > 0) && (upgradeLangDetail.Slovak == null || (upgradeLangDetail.Slovak != null && upgradeLangDetail.Slovak.Length == 0)))
                    upgradeLangDetail.Slovak = langDetail.Slovak;
                if ((langDetail.Slovene != null && langDetail.Slovene.Length > 0) && (upgradeLangDetail.Slovene == null || (upgradeLangDetail.Slovene != null && upgradeLangDetail.Slovene.Length == 0)))
                    upgradeLangDetail.Slovene = langDetail.Slovene;
                if ((langDetail.Spanish != null && langDetail.Spanish.Length > 0) && (upgradeLangDetail.Spanish == null || (upgradeLangDetail.Spanish != null && upgradeLangDetail.Spanish.Length == 0)))
                    upgradeLangDetail.Spanish = langDetail.Spanish;
                if ((langDetail.Swedish != null && langDetail.Swedish.Length > 0) && (upgradeLangDetail.Swedish == null || (upgradeLangDetail.Swedish != null && upgradeLangDetail.Swedish.Length == 0)))
                    upgradeLangDetail.Swedish = langDetail.Swedish;
                if ((langDetail.Turkish != null && langDetail.Turkish.Length > 0) && (upgradeLangDetail.Turkish == null || (upgradeLangDetail.Turkish != null && upgradeLangDetail.Turkish.Length == 0)))
                    upgradeLangDetail.Turkish = langDetail.Turkish;
                if ((langDetail.Ukrainian != null && langDetail.Ukrainian.Length > 0) && (upgradeLangDetail.Ukrainian == null || (upgradeLangDetail.Ukrainian != null && upgradeLangDetail.Ukrainian.Length == 0)))
                    upgradeLangDetail.Ukrainian = langDetail.Ukrainian;
                if ((langDetail.Belarusian != null && langDetail.Belarusian.Length > 0) && (upgradeLangDetail.Belarusian == null || (upgradeLangDetail.Belarusian != null && upgradeLangDetail.Belarusian.Length == 0)))
                    upgradeLangDetail.Belarusian = langDetail.Belarusian;


                context.SaveChanges();
            }

            return upgradeLangDetailId;
        }
        public string GetInfoMessageType(int id)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var refId = context.LangRefDetail.Where(x => x.Id == id).Select(x => x.RefId).FirstOrDefault();
                    var messageType = context.InfomessageProperties.Where(x => x.InfomessageId == refId).Select(x => x.Messagetype).FirstOrDefault();
                    return messageType;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string DeleteGeneralTranslation(int id)
        {
            using (var context = new Creatis_Context())
            {
                var result = "";
                if (!(context.LangRefDetail.Where(x => x.LangDetailId == id).Any()))
                {
                    var existLangDetail = context.LangDetail.FirstOrDefault(x => x.Id == id);
                    if (existLangDetail != null)
                    {
                        context.LangDetail.Remove(existLangDetail);
                        context.SaveChanges();
                    }
                    else
                        result = "Not Exist";
                }
                else
                    result = "Used";

                return result;
            }
        }

        public int GetPlanningSelectEntryQuestionId(int questionId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    int questionRefId = 0;
                    var otherresult = context.Questions.Where(x => x.Name == "Other" && x.ParentId == questionId).FirstOrDefault();
                    if (otherresult != null)
                    {
                        var othertitleresult = context.Questions.Where(x => x.ParentId == otherresult.QuestionId && x.PropertyName == "Planning Select Other Title").FirstOrDefault();
                        if (othertitleresult != null)
                        {
                            questionRefId = othertitleresult.QuestionId;
                        }
                    }
                    return questionRefId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion Selvam Thangaraj

    }
}

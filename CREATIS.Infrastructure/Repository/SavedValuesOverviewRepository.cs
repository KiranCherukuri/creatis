﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using CREATIS.Infrastructure.IRepository;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using System.Linq;

namespace CREATIS.Infrastructure.Repository
{
    public class SavedValuesOverviewRepository : ISavedValuesOverviewRepository
    {
        public List<SavedValues> GetSavedValues(int customerRefId, int layoutRefId)
        {
            List<SavedValues> inList;
            try
            {
                using (var context = new Creatis_Context())
                {
                    if(customerRefId == 999999 && layoutRefId == 999999)
                    {
                        inList = new List<SavedValues>();
                    }
                    else
                    {
                        SqlParameter layoutParam = new SqlParameter("@LayoutRefId", layoutRefId);
                        SqlParameter customerParam = new SqlParameter("@CustomerRefId", customerRefId);
                        inList = context.SavedValues.FromSql("GetOverAllSavedValues @LayoutRefId,@CustomerRefId", customerParam, layoutParam).ToList();
                    }
                  
                }
            }
            catch (Exception ex)
            {
                return new List<SavedValues>();
            }

            return inList;
        }

        public string SaveAlias(AliasNameDetails AliasNameDetailsModel)
        {
            var result = "";
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (AliasNameDetailsModel.AliasNameDetailsId > 0)
                    {
                        var target = context.AliasNameDetails.Where(x => x.AliasNameDetailsId == AliasNameDetailsModel.AliasNameDetailsId).FirstOrDefault();
                        if (target != null)
                        {
                            target.AliasName = AliasNameDetailsModel.AliasName;
                            target.Comment = AliasNameDetailsModel.Comment;
                            target.UpdatedBy = AliasNameDetailsModel.UpdatedBy;
                            target.UpdatedOn = AliasNameDetailsModel.UpdatedOn;
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        var target = context.AliasNameDetails.Where(x => x.AliasName == AliasNameDetailsModel.AliasName && x.LayoutRefId == AliasNameDetailsModel.LayoutRefId).FirstOrDefault();
                        if (target == null)
                        {
                            AliasNameDetailsModel.PropertyName = "Setup";
                            AliasNameDetailsModel.PropertyTableId = 0;
                            AliasNameDetailsModel.ActivitiesRefId = 0;
                            AliasNameDetailsModel.IsTempData = false;

                            context.AliasNameDetails.Add(AliasNameDetailsModel);
                            context.SaveChanges();
                        }
                        else
                            result = "error";
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public List<AliasNameDetails> GetAliasList(int layoutId)
        {
            List<AliasNameDetails> AliasNameDetailsList;
            try
            {
                using (var context = new Creatis_Context())
                {
                    AliasNameDetailsList = context.AliasNameDetails.Where(x=> x.LayoutRefId == layoutId).Select(x => new AliasNameDetails()
                    {
                        LayoutRefId = x.LayoutRefId,
                        AliasNameDetailsId = x.AliasNameDetailsId,
                        AliasName = x.AliasName,
                        Comment = x.Comment
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<AliasNameDetails>();
            }
            return AliasNameDetailsList;
        }
    }
}

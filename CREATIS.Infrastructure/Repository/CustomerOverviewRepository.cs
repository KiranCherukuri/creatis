﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.Infrastructure.IRepository;
using CREATIS.Infrastructure;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CREATIS.Infrastructure.Repository
{
    public class CustomerOverviewRepository : Utility, ICustomerOverviewRepository
    {
        public readonly ITranslatorRepository _translatorRepository;
        public CustomerOverviewRepository(ITranslatorRepository translatorRepository)
        {
            this._translatorRepository = translatorRepository;
        }

        public Creatis_Context dbconext;
        public List<CustomerOverviewDetail> GetLayoutList(int CustomerId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    SqlParameter customerParam = new SqlParameter("@CustomerRefId", CustomerId);
                    List<CustomerOverviewDetail> returnList = context.CustomerOverviewDetail.FromSql("GetCustomerOverviewDetail @CustomerRefId", customerParam).OrderByDescending(x => x.UpdatedOn).ToList();
                    return returnList;
                }

            }
            catch (Exception ex)
            {
                return new List<CustomerOverviewDetail>();
            }
        }
        public List<InfocolumnDetails> GetInfoColumn(int customerId, string createdBy)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (!context.InfoColumn.Where(x => x.CustomerRefId == customerId).Any())
                        SaveDefaultInfoColumn(customerId, createdBy);

                    var returnList = (from ICM in context.InfoColumnMaster.ToList()
                                      join IC in context.InfoColumn.Where(x => x.CustomerRefId == customerId).Select(x => new { ICM_Column_Ref_No = (x.ICM_Column_Ref_No ?? 0), x.IC_Column_Name, x.IC_ID, CustomerRefId = (x.CustomerRefId ?? 0) }).ToList() on ICM.ICM_Column_No equals IC.ICM_Column_Ref_No
                                      into joinedT
                                      from IC in joinedT.DefaultIfEmpty(new { ICM_Column_Ref_No = ICM.ICM_Column_No, IC_Column_Name = "", IC_ID = 0, CustomerRefId = 0 })
                                      select new InfocolumnDetails()
                                      {
                                          IC_ID = (IC.IC_ID == 0 ? ICM.ICM_Column_No : IC.IC_ID),
                                          ICM_Column_Name = IC.IC_Column_Name == "" ? ICM.ICM_Name : IC.IC_Column_Name,
                                          CustomerRefId = IC.CustomerRefId
                                      }).ToList();



                    return returnList;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public InfoColumns SaveInfoColumn(InfoColumns InfoColumn)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(InfoColumn);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = InfoColumn.IC_ID;
                    langrefdetail.LayoutRefId = null;
                    langrefdetail.QuestionRefId = null;
                    langrefdetail.CustomerRefId = InfoColumn.CustomerRefId;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.InfoColumns_IC_Column_Name;
                    langrefdetail.LangText = InfoColumn.IC_Column_Name;
                    langrefdetail.Language = LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = InfoColumn.CreatedBy;
                    langrefdetail.UpdatedBy = InfoColumn.CreatedBy;

                    _translatorRepository.SaveLangDetail(langrefdetail);

                    /* Written By Selvam Thangaraj - Stop */

                    return InfoColumn;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateInfoColumn(int ID, string Columnname, string Updatedby, DateTime Updatedon)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.InfoColumn.SingleOrDefault(IC => IC.IC_ID == ID);
                    if (result != null)
                    {
                        result.IC_Column_Name = Columnname;
                        result.UpdatedBy = Updatedby;
                        result.UpdatedOn = Updatedon;
                        context.SaveChanges();
                    }

                    /* Written By Selvam Thangaraj - Go */

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = result.IC_ID;
                    langrefdetail.QuestionRefId = null;
                    langrefdetail.LayoutRefId = null;
                    langrefdetail.CustomerRefId = result.CustomerRefId;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.InfoColumns_IC_Column_Name;
                    langrefdetail.LangText = result.IC_Column_Name;
                    langrefdetail.Language = LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = result.UpdatedBy;
                    langrefdetail.UpdatedBy = result.UpdatedBy;

                    _translatorRepository.SaveLangDetail(langrefdetail);

                    /* Written By Selvam Thangaraj - Stop */

                    return "Updated";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string DeleteInfoColumn(int ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.InfoColumn.SingleOrDefault(IC => IC.IC_ID == ID);
                    if (result != null)
                    {
                        context.InfoColumn.Remove(result);
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.IC_ID;
                        langrefdetail.LayoutRefId = null;
                        langrefdetail.CustomerRefId = result.CustomerRefId;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.InfoColumns_IC_Column_Name;

                        _translatorRepository.DeleteLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */
                    }

                    return "Deleted";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<Integrator> GetIntegrator(int CustomerId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from IR in context.Integrator
                                where IR.CustomerRefId == CustomerId
                                select new Integrator()
                                {
                                    I_ID = IR.I_ID,
                                    I_Details = IR.I_Details,
                                    CustomerRefId = IR.CustomerRefId,
                                    IS_Active = IR.IS_Active,
                                    CreatedBy = IR.CreatedBy,
                                    CreatedOn = IR.CreatedOn,
                                    UpdatedBy = IR.UpdatedBy,
                                    UpdatedOn = IR.UpdatedOn

                                }).ToList();
                    List<Integrator> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Integrator SaveIntegrator(Integrator Integrator)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(Integrator);
                    context.SaveChanges();
                    return Integrator;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateIntegrator(int I_ID, string I_Details, string Updatedby, DateTime Updatedon)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Integrator.SingleOrDefault(IR => IR.I_ID == I_ID);
                    if (result != null)
                    {
                        result.I_Details = I_Details;
                        result.UpdatedBy = Updatedby;
                        result.UpdatedOn = Updatedon;
                        context.SaveChanges();
                    }
                    return "Updated";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string DeleteIntegrator(int I_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Integrator.SingleOrDefault(IR => IR.I_ID == I_ID);
                    if (result != null)
                    {
                        context.Integrator.Remove(result);
                        context.SaveChanges();
                    }
                    //context.Update(InfoColumn);
                    //context.SaveChanges();
                    return "Deleted";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<CustomerNotes> GetCustomerNotes(int CustomerId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from CN in context.CustomerNote
                                where CN.CustomerRefId == CustomerId
                                select new CustomerNotes()
                                {
                                    CN_ID = CN.CN_ID,
                                    CN_Details = CN.CN_Details,
                                    CustomerRefId = CN.CustomerRefId,
                                    IS_Active = CN.IS_Active,
                                    CreatedBy = CN.CreatedBy,
                                    CreatedOn = CN.CreatedOn,
                                    UpdatedBy = CN.UpdatedBy,
                                    UpdatedOn = CN.UpdatedOn

                                }).ToList();
                    List<CustomerNotes> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public CustomerNotes SaveCustomerNotes(CustomerNotes CustomerNotes)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(CustomerNotes);
                    context.SaveChanges();
                    return CustomerNotes;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateCustomerNotes(int CN_ID, string CN_Details, string Updatedby, DateTime Updatedon)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.CustomerNote.SingleOrDefault(CN => CN.CN_ID == CN_ID);
                    if (result != null)
                    {
                        result.CN_Details = CN_Details;
                        result.UpdatedBy = Updatedby;
                        result.UpdatedOn = Updatedon;
                        context.SaveChanges();
                    }
                    return "Updated";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string DeleteCustomerNotes(int CN_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.CustomerNote.SingleOrDefault(CN => CN.CN_ID == CN_ID);
                    if (result != null)
                    {
                        context.CustomerNote.Remove(result);
                        context.SaveChanges();
                    }
                    //context.Update(InfoColumn);
                    //context.SaveChanges();
                    return "Deleted";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }


        public List<LanguageMaster> GetLanguages()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<LanguageMaster> returnList = context.LanguageMaster.OrderBy(x => x.LanguageDescription).ToList();
                    return returnList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Layout SaveLayout(Layout LayoutDetails)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(LayoutDetails);
                    context.SaveChanges();

                    if (!context.InfoColumn.Where(x => x.CustomerRefId == LayoutDetails.CustomerRefId).Any())
                        SaveDefaultInfoColumn(LayoutDetails.CustomerRefId, LayoutDetails.CreatedBy);

                    return LayoutDetails;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string SaveDefaultInfoColumn(int customerRefId, string createdBy)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var infoColumnDetails = context.InfoColumnMaster.ToList();
                    int i = 1;
                    foreach (var infocolumnmaster in infoColumnDetails)
                    {
                        if (context.InfoColumn.Where(x => x.CustomerRefId == customerRefId && x.ICM_Column_Ref_No == infocolumnmaster.ICM_Column_No).Count() == 0)
                        {
                            InfoColumns infocolumns = new InfoColumns();
                            infocolumns.CreatedBy = createdBy;
                            infocolumns.CreatedOn = DateTime.Now;
                            infocolumns.CustomerRefId = customerRefId;
                            infocolumns.ICM_Column_Ref_No = infocolumnmaster.ICM_Column_No;
                            infocolumns.IC_Column_Name = "Info " + i;
                            infocolumns.IS_Active = 1;
                            context.InfoColumn.Add(infocolumns);
                            context.SaveChanges();

                            i++;

                            /* Written By Selvam Thangaraj - Go */

                            LangRefDetail langrefdetail = new LangRefDetail();
                            langrefdetail.RefId = infocolumns.IC_ID;
                            langrefdetail.QuestionRefId = null;
                            langrefdetail.LayoutRefId = null;
                            langrefdetail.CustomerRefId = infocolumns.CustomerRefId;
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.InfoColumns_IC_Column_Name;
                            langrefdetail.LangText = infocolumns.IC_Column_Name;
                            langrefdetail.Language = LanguagesEnum.English.ToString();
                            langrefdetail.CreatedBy = infocolumns.CreatedBy;
                            langrefdetail.UpdatedBy = infocolumns.CreatedBy;

                            _translatorRepository.SaveLangDetail(langrefdetail);

                            /* Written By Selvam Thangaraj - Stop */
                        }
                    }
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        public Layout updateLayout(Layout LayoutDetails)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Layout.SingleOrDefault(IC => IC.LayoutId == LayoutDetails.LayoutId && IC.CustomerRefId == LayoutDetails.CustomerRefId);
                    if (result != null)
                    {
                        result.UpdatedBy = LayoutDetails.UpdatedBy;
                        result.UpdatedOn = LayoutDetails.UpdatedOn;
                        context.SaveChanges();
                    }
                    return LayoutDetails;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public UsedLanguages SaveUsedLanguage(UsedLanguages UsedLanguage)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var existingCount = context.UsedLanguages.Count(a => a.LayoutRefId == UsedLanguage.LayoutRefId && a.LanguageRefId == UsedLanguage.LanguageRefId);
                    if (existingCount == 0)
                    {
                        if (UsedLanguage.LanguageRefId != 66)
                        {
                            int english = 66;
                            int CustomerRefId = UsedLanguage.CustomerRefId;
                            int LayoutRefId = UsedLanguage.LayoutRefId;
                            string CreatedBy = UsedLanguage.CreatedBy;
                            DateTime CreatedOn = UsedLanguage.CreatedOn;
                            int IsDefault = 0;
                            context.Add(UsedLanguage);
                            context.SaveChanges();
                            var existingAuthorCount = context.UsedLanguages.Count(a => a.LayoutRefId == UsedLanguage.LayoutRefId && a.LanguageRefId == 66);
                            if (existingAuthorCount == 0)
                            {
                                UsedLanguages UsedLan = new UsedLanguages();
                                UsedLan.CustomerRefId = CustomerRefId;
                                UsedLan.LayoutRefId = LayoutRefId;
                                UsedLan.LanguageRefId = english;
                                UsedLan.CreatedBy = CreatedBy;
                                UsedLan.CreatedOn = CreatedOn;
                                UsedLan.IsDefault = IsDefault;
                                UsedLan.IsModification = (int)EnumCopyColorCode.Green;
                                UsedLan.IsActive = true;
                                context.Add(UsedLan);
                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            context.Add(UsedLanguage);
                            context.SaveChanges();
                        }
                    }
                    return UsedLanguage;
                }

            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public string GetCustomerName(int Customerid)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from C in context.Customers

                                where C.CustomerId == Customerid

                                select C.Name

                                ).SingleOrDefault();
                    return data == null ? string.Empty : data.ToString();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<InfoColumns> GetAssignedInfoColumn(int CustomerId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var infocolumnList =  context.InfoColumn.Where(x => x.CustomerRefId == CustomerId).ToList();
                    return infocolumnList;
                }

            }
            catch (Exception ex)
            {
                return new List<InfoColumns>();
            }
        }

        public List<InfocolumnList> GetInfoColumnQuestionpath(int customerId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    int i = 0;
                    List<InfocolumnList> objInfocolumnDetails = new List<InfocolumnList>();
                    SqlParameter sqlLayoutId = new SqlParameter("@CustomerRefId", customerId);
                    SqlParameter infoColumnId = new SqlParameter("@InfoColumnId", i);
                    objInfocolumnDetails = context.InfocolumnList.FromSql("GetInfoColumns @CustomerRefId,@InfoColumnId", sqlLayoutId, infoColumnId).ToList();

                    return objInfocolumnDetails;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool ValidateLayoutName(string LayoutName, int CustomerId, int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var LayoutCount = context.Layout.Where(x => x.LayoutId != LayoutId && x.CustomerRefId == CustomerId && x.LayoutName.Trim().ToLower() == LayoutName.Trim().ToLower()).Count();
                    return (LayoutCount > 0 ? true : false);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}

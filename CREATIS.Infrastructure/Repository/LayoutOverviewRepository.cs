﻿using CREATIS.ApplicationCore.Modals;
using CREATIS.Infrastructure.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CREATIS.Infrastructure.Repository
{
    public class LayoutOverviewRepository : Utility, ILayoutOverviewRepository
    {
        public List<LayoutOverviewValues> GetOverallLayoutValues()
        {
            List<LayoutOverviewValues> objLayoutValues = new List<LayoutOverviewValues>();
            using (var context = new Creatis_Context())
            {
                objLayoutValues=context.OverallLayoutValues.FromSql("OverAllLayoutValues").ToList();
            }

            return objLayoutValues;
        }
    }
}

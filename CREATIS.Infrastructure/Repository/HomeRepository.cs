﻿using System;
using System.Collections.Generic;
using System.Linq;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.Infrastructure.IRepository;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CREATIS.Infrastructure.Repository
{
    public class HomeRepository : Utility, IHomeRepository
    {
        public Creatis_Context dbconext;
        public List<Customers> Customer()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Customers.Where(x=> x.Name.Length > 0).ToList(); 
                }

            }
            catch (Exception ex)
            {
                return new List<Customers>();
            }
        }
        public List<UserRecentActivities> GetRecentActivities(string userId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    SqlParameter userIdParam = new SqlParameter("@UserId", userId);
                    context.Database.SetCommandTimeout(180);
                    var recentActivitiesList = context.UserRecentActivities.FromSql("GetRecentActivities @UserId", userIdParam).OrderByDescending(x => x.RecentActivityOn).ToList();

                    return recentActivitiesList.Take(15).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteDashboard(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var CustomerId = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.CustomerRefId).FirstOrDefault();
                    var LangRefDetailIds = context.LangRefDetail.Where(x => (x.LayoutRefId == layoutId && x.CustomerRefId == CustomerId) || (x.CustomerRefId == CustomerId && x.Layout == null)).Select(x => x.Id).ToList();
                    if (LangRefDetailIds.Any())
                    {
                        var LangRefDetails = context.LangRefDetail.Where(x => LangRefDetailIds.Contains(x.Id));
                        var LangDetailIds = LangRefDetails.Select(x => x.LangDetailId).Distinct().ToList();

                        if (LangRefDetails.Any())
                        {
                            context.LangRefDetail.RemoveRange(LangRefDetails);
                            context.SaveChanges();
                        }

                        var LangDetails = context.LangDetail.Where(x => LangDetailIds.Contains(x.Id)).Distinct().ToList();
                        if (LangDetails.Any())
                        {
                            foreach (var langdetail in LangDetails)
                            {
                                if (langdetail.IsDefaultTranslation == 0 && (context.LangRefDetail.Where(x => x.LangDetailId == langdetail.Id).Select(x => x.Id).Count() == 0))
                                {
                                    context.LangDetail.Remove(langdetail);
                                    context.SaveChanges();
                                }
                            }
                        }
                    }

                    var aliasNameList = context.AliasNameDetails.Where(x => x.LayoutRefId == layoutId);
                    if (aliasNameList.Any())
                    {
                        context.AliasNameDetails.RemoveRange(aliasNameList);
                        context.SaveChanges();
                    }

                    var Usedlanguage = context.UsedLanguages.Where(UL => UL.LayoutRefId == layoutId);
                    if (Usedlanguage.Any())
                    {
                        context.UsedLanguages.RemoveRange(Usedlanguage);
                        context.SaveChanges();
                    }

                    var TransportType = context.TransportTypes.Where(TT => TT.LayoutRefId == layoutId);
                    if (TransportType.Any())
                    {
                        context.TransportTypes.RemoveRange(TransportType);
                        context.SaveChanges();
                    }

                    var PlanningTypes = context.PlanningType.Where(PT => PT.LayoutRefId == layoutId);
                    if (PlanningTypes.Any())
                    {
                        context.PlanningType.RemoveRange(PlanningTypes);
                        context.SaveChanges();
                    }

                    var PlanningSpecifications = context.PlanningSpecification.Where(PS => PS.LayoutRefId == layoutId);
                    if (PlanningSpecifications.Any())
                    {
                        context.PlanningSpecification.RemoveRange(PlanningSpecifications);
                        context.SaveChanges();
                    }

                    var Allowance = context.Allowances.Where(A => A.LayoutRefId == layoutId);
                    if (Allowance.Any())
                    {
                        context.Allowances.RemoveRange(Allowance);
                        context.SaveChanges();
                    }

                    var OBCTypes = context.OBCType.Where(OT => OT.LayoutRefId == layoutId);
                    if (OBCTypes.Any())
                    {
                        context.OBCType.RemoveRange(OBCTypes);
                        context.SaveChanges();
                    }
                    var DocumentTypeFlexs = context.DocumentTypeFlex.Where(x => x.LayoutRefId == layoutId);
                    if (DocumentTypeFlexs.Any())
                    {
                        context.DocumentTypeFlex.RemoveRange(DocumentTypeFlexs);
                        context.SaveChanges();
                    }

                    
                    var chiocePropertiesList = context.ChioceProperties.Where(x => x.LayoutRefId == layoutId);
                    var chiocePropertiesIdList = chiocePropertiesList.Select(x => x.ChioceId);
                    var addChiocePropertiesList = context.AddChioceProperties.Where(x => chiocePropertiesIdList.Contains(x.ChioceRefId));
                    if (addChiocePropertiesList.Any())
                    {
                        context.AddChioceProperties.RemoveRange(addChiocePropertiesList);
                        context.SaveChanges();
                    }
                    if (chiocePropertiesList.Any())
                    {
                       
                        context.ChioceProperties.RemoveRange(chiocePropertiesList);
                        context.SaveChanges();
                    }

                    var transportTypePropertiesList = context.TransportTypeProperties.Where(x => x.LayoutRefId == layoutId);
                    var transportTypePropertiesIdList = transportTypePropertiesList.Select(x => x.TransportTypeId);
                    var addTransportTypePropertiesList = context.TransportTypePropertiesDetail.Where(x => transportTypePropertiesIdList.Contains(x.TransportTypeRefId));
                    if (addTransportTypePropertiesList.Any())
                    {
                        context.TransportTypePropertiesDetail.RemoveRange(addTransportTypePropertiesList);
                        context.SaveChanges();
                    }
                    if (transportTypePropertiesList.Any())
                    {
                        context.TransportTypeProperties.RemoveRange(transportTypePropertiesList);
                        context.SaveChanges();
                    }

                    var GetSystemValuesPropertiesList = context.GetSystemValuesProperties.Where(x => x.LayoutRefId == layoutId);
                    if (GetSystemValuesPropertiesList.Any())
                    {
                        context.GetSystemValuesProperties.RemoveRange(GetSystemValuesPropertiesList);
                        context.SaveChanges();
                    }

                    var entryPropertiesList = context.EntryProperties.Where(x => x.LayoutRefId == layoutId);
                    if (entryPropertiesList.Any())
                    {
                        context.EntryProperties.RemoveRange(entryPropertiesList);
                        context.SaveChanges();
                    }

                    var TrailerPropertiesList = context.TrailerProperties.Where(x => x.LayoutRefId == layoutId);
                    if (TrailerPropertiesList.Any())
                    {
                        var TrailerPropertiesIds = TrailerPropertiesList.Select(x => x.TrailerId).Distinct();
                        var AliasNameList = aliasNameList.Where(x => TrailerPropertiesIds.Contains(x.PropertyTableId) && x.PropertyName == "Trailer");
                        if (AliasNameList.Any())
                        {
                            context.AliasNameDetails.RemoveRange(AliasNameList);
                            context.SaveChanges();
                        }
                        context.TrailerProperties.RemoveRange(TrailerPropertiesList);
                        context.SaveChanges();
                    }

                    var planningselectPropertiesList = context.PlanningselectProperties.Where(x => x.LayoutRefId == layoutId);
                    if (planningselectPropertiesList.Any())
                    {
                        var planningselectids = planningselectPropertiesList.Select(x => x.PlanningselectId).ToList();
                        var readoutlists = context.PlanningSelectReadoutDetail.Where(x => planningselectids.Contains(x.PlanningSelectRefId)).ToList();
                        context.PlanningSelectReadoutDetail.RemoveRange(readoutlists);
                        context.SaveChanges();

                        context.PlanningselectProperties.RemoveRange(planningselectPropertiesList);
                        context.SaveChanges();
                    }
                    var checkoffPropertiesList = context.CheckoffProperties.Where(x => x.LayoutRefId == layoutId);
                    if (checkoffPropertiesList.Any())
                    {
                        context.CheckoffProperties.RemoveRange(checkoffPropertiesList);
                        context.SaveChanges();
                    }
                    var conditionalPropertiesList = context.ConditionalProperties.Where(x => x.LayoutRefId == layoutId);
                    if (conditionalPropertiesList.Any())
                    {
                        context.ConditionalProperties.RemoveRange(conditionalPropertiesList);
                        context.SaveChanges();
                    }

                    var infomessagePropertiesList = context.InfomessageProperties.Where(x => x.LayoutRefId == layoutId);
                    if (infomessagePropertiesList.Any())
                    {
                        var infoMessageIds = infomessagePropertiesList.Select(x => x.InfomessageId).Distinct().ToList();
                        var infomessagecontentList = context.InfoMessageContentDetail.Where(x => infoMessageIds.Contains(x.InfomessagePropertiesRefId)).ToList();
                        if (infomessagecontentList.Any())
                        {
                            context.InfoMessageContentDetail.RemoveRange(infomessagecontentList);
                            context.SaveChanges();
                        }

                        context.InfomessageProperties.RemoveRange(infomessagePropertiesList);
                        context.SaveChanges();
                    }
                    var textMessagePropertiesList = context.TextMessageProperties.Where(x => x.LayoutRefId == layoutId);
                    if (textMessagePropertiesList.Any())
                    {
                        context.TextMessageProperties.RemoveRange(textMessagePropertiesList);
                        context.SaveChanges();
                    }
                    var variableListPropertiesList = context.VariableListProperties.Where(x => x.LayoutRefId == layoutId);
                    if (variableListPropertiesList.Any())
                    {
                        context.VariableListProperties.RemoveRange(variableListPropertiesList);
                        context.SaveChanges();
                    }

                    var VarListPropertiesList = context.VarListProperties.Where(x => x.LayoutRefId == layoutId);
                    if (VarListPropertiesList.Any())
                    {
                        var VarListPropertiesIds = VarListPropertiesList.Select(x => x.VarListPropertiesId).Distinct();

                        var readoutlists = context.VarListCommentReadoutDetail.Where(x => VarListPropertiesIds.Contains(x.VarListPropertiesRefId)).ToList();
                        context.VarListCommentReadoutDetail.RemoveRange(readoutlists);
                        context.SaveChanges();

                        var filterlists = context.VarListFilterDetail.Where(x => VarListPropertiesIds.Contains(x.VarListPropertiesRefId)).ToList();
                        context.VarListFilterDetail.RemoveRange(filterlists);
                        context.SaveChanges();

                        context.VarListProperties.RemoveRange(VarListPropertiesList);
                        context.SaveChanges();
                    }

                    var setActionValuePropertiesList = context.SetActionValueProperties.Where(x => x.LayoutRefId == layoutId);
                    if (setActionValuePropertiesList.Any())
                    {
                        context.SetActionValueProperties.RemoveRange(setActionValuePropertiesList);
                        context.SaveChanges();
                    }
                    var documetScanPropertiesList = context.DocumetScanProperties.Where(x => x.LayoutRefId == layoutId);
                    if (documetScanPropertiesList.Any())
                    {
                        context.DocumetScanProperties.RemoveRange(documetScanPropertiesList);
                        context.SaveChanges();
                    }

                    var DocumentScanTitleList = context.DocumentScanTitle.Where(x => x.LayoutRefId == layoutId);
                    var DocumentScanTitleIds = DocumentScanTitleList.Select(x => x.DocumetScanTitleId).ToList();
                    var DocumentScanTypeList = context.DocumentScanType.Where(x => DocumentScanTitleIds.Contains(x.DocumentScanTitleRefId));
                    if (DocumentScanTypeList.Any())
                    {
                        context.DocumentScanType.RemoveRange(DocumentScanTypeList);
                        context.SaveChanges();
                    }
                    if (DocumentScanTitleList.Any())
                    {
                        context.DocumentScanTitle.RemoveRange(DocumentScanTitleList);
                        context.SaveChanges();
                    }
                    var flexList = context.Flex.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (flexList.Any())
                    {
                        context.Flex.RemoveRange(flexList);
                        context.SaveChanges();
                    }
                    var extraInfoPropertiesList = context.ExtraInfoProperties.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (extraInfoPropertiesList.Any())
                    {
                        context.ExtraInfoProperties.RemoveRange(extraInfoPropertiesList);
                        context.SaveChanges();
                    }
                    var extraInfoList = context.ExtraInfo.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (extraInfoList.Any())
                    {
                        context.ExtraInfo.RemoveRange(extraInfoList);
                        context.SaveChanges();
                    }
                    var palletsPropertiesList = context.PalletsProperties.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (palletsPropertiesList.Any())
                    {
                        context.PalletsProperties.RemoveRange(palletsPropertiesList);
                        context.SaveChanges();
                    }
                    var palletsList = context.Pallets.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (palletsList.Any())
                    {
                        context.Pallets.RemoveRange(palletsList);
                        context.SaveChanges();
                    }
                    var problemsPropertiesList = context.ProblemsProperties.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (problemsPropertiesList.Any())
                    {
                        context.ProblemsProperties.RemoveRange(problemsPropertiesList);
                        context.SaveChanges();
                    }
                    var picturePropertiesList = context.PictureProperties.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (picturePropertiesList.Any())
                    {
                        context.PictureProperties.RemoveRange(picturePropertiesList);
                        context.SaveChanges();
                    }
                    var problemsList = context.Problems.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (problemsList.Any())
                    {
                        context.Problems.RemoveRange(problemsList);
                        context.SaveChanges();
                    }
                    var signaturesPropertiesList = context.SignaturesProperties.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (signaturesPropertiesList.Any())
                    {
                        context.SignaturesProperties.RemoveRange(signaturesPropertiesList);
                        context.SaveChanges();
                    }
                    var signaturesList = context.Signatures.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (signaturesList.Any())
                    {
                        context.Signatures.RemoveRange(signaturesList);
                        context.SaveChanges();
                    }
                    var pictureList = context.Picure.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (pictureList.Any())
                    {
                        context.Picure.RemoveRange(pictureList);
                        context.SaveChanges();
                    }
                    var smartExtraInfoList = context.SmartExtraInfo.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (smartExtraInfoList.Any())
                    {
                        context.SmartExtraInfo.RemoveRange(smartExtraInfoList);
                        context.SaveChanges();
                    }
                    var smartPalletsList = context.SmartPallets.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (smartPalletsList.Any())
                    {
                        context.SmartPallets.RemoveRange(smartPalletsList);
                        context.SaveChanges();
                    }
                    var smartProblemsList = context.SmartProblems.Where(x => x.LayoutRefID == layoutId).ToList();
                    if (smartProblemsList.Any())
                    {
                        context.SmartProblems.RemoveRange(smartProblemsList);
                        context.SaveChanges();
                    }
                    var questionsList = context.Questions.Where(x => x.LayoutRefId == layoutId).ToList();
                    if (questionsList.Any())
                    {
                        context.Questions.RemoveRange(questionsList);
                        context.SaveChanges();
                    }

                    var ActivitiesRegistrations = context.ActivitiesRegistration.Where(L => L.LayoutRefId == layoutId);
                    if (ActivitiesRegistrations.Any())
                    {
                        context.ActivitiesRegistration.RemoveRange(ActivitiesRegistrations);
                        context.SaveChanges();
                    }

                    var recentActivityList = context.RecentActivity.Where(x => x.LayoutRefId == layoutId).ToList();
                    if (recentActivityList.Any())
                    {
                        context.RecentActivity.RemoveRange(recentActivityList);
                        context.SaveChanges();
                    }

                    var screenLayoutList = context.ScreenLayout.Where(x => x.LayoutRefId == layoutId).ToList();
                    if (screenLayoutList.Any())
                    {
                        context.ScreenLayout.RemoveRange(screenLayoutList);
                        context.SaveChanges();
                    }

                    var Layouts = context.Layout.Where(L => L.LayoutId == layoutId);
                    if (Layouts.Any())
                    {
                        context.Layout.RemoveRange(Layouts);
                        context.SaveChanges();
                    }
                    return "Deleted";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string UpdateLayout(int LayoutId, string LayoutName)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var targetlayout = context.Layout.Where(x => x.LayoutId == LayoutId).FirstOrDefault();
                    if (targetlayout != null)
                    {
                        if (targetlayout.LayoutStatusRefId == (int)EnumLayoutStatus.InProgress || targetlayout.LayoutStatusRefId == (int)EnumLayoutStatus.CheckNeeded)
                        {
                            targetlayout.LayoutName = LayoutName;
                            context.SaveChanges();

                            return "";
                        }
                        else
                            return "Process Failed, This Layout Status Not in InProgress (or) CheckNeeded";

                    }
                    else
                        return "Layout Does Not Exist";

                }
            }
            catch (Exception ex)
            {
                return "Internal Server Error";
            }

        }
        public string RemoveRecentActivities(int layoutId, string userId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var recentactivitytarget = context.RecentActivity.Where(x => x.LayoutRefId == layoutId && x.UpdatedBy == userId).FirstOrDefault();

                    if (recentactivitytarget != null)
                    {
                        recentactivitytarget.Status = 0;
                        context.SaveChanges();
                    }

                    return "";
                }
            }
            catch (Exception ex)
            {
                return "Internal Server Error";
            }

        }


    }
}

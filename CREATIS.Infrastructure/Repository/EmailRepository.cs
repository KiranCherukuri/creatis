﻿using System;
using System.Collections.Generic;
using System.Text;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using System.Net.Mail;
using System.Linq;

namespace CREATIS.Infrastructure.Repository
{
    public class EmailRepository : IRepository.IEmailRepository
    {
        public EmailConfiguration emailConfiguration { get; }
        public EmailRepository(Microsoft.Extensions.Options.IOptions<EmailConfiguration> emailSettings)
        {
            emailConfiguration = emailSettings.Value;
        }


        public void SendLayoutUpdateStatusMail(LayoutNotes layoutNotes, string fromAddress, string toAddress, string customername, string layoutname, string SelectedStatus, string urlString, int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var CreatedByName = context.Layout.Where(x => x.LayoutId == LayoutId).Select(x => x.CreatedBy).FirstOrDefault();
                    if (CreatedByName != null && CreatedByName.Trim().Length > 0)
                    {
                        var Email = context.Users.Where(x => x.DomainId == CreatedByName).Select(x => x.EmailId).FirstOrDefault();
                        MailMessage message = new MailMessage();

                        if (toAddress.Contains("|"))
                        {
                            var toaddess = toAddress.Split("|").Where(x => x.Trim().Length > 0).ToList();
                            foreach (var mailid in toaddess[0].Split("ƒ").Where(x => x.Trim().Length > 0))
                                message.To.Add(mailid);
                            if (toaddess.Count() > 1)
                            {
                                foreach (var mailid in toaddess[1].Split("ƒ").Where(x => x.Trim().Length > 0))
                                    message.CC.Add(mailid);
                            }
                        }
                        else
                        {
                            message.To.Add(toAddress.Replace("|", ""));
                        }

                        message.Subject = string.Format("TX-CREATIS:{0}-{1},status was set to {2}", customername, layoutname, SelectedStatus);
                        message.From = new System.Net.Mail.MailAddress(fromAddress, fromAddress);
                        message.IsBodyHtml = true;

                        string strContent = @" 
<p>Hi,</p><br/><p>Please find the below Url for the updated Layout.</p>
                    <table> 
                       <tr>  
                            <td style='width:100px;'> DashBoard Url : </td>  
	                        <td> " + urlString + @" </td>  
                       </tr> 
<tr>  
                            <td style='width:100px;'> Notes : </td>  
	                        <td> " + layoutNotes.Notes + @" </td>  
                       </tr> 
                    </table>";

                        message.Body = strContent;

                        message.BodyEncoding = Encoding.Default;
                        message.Priority = MailPriority.High;

                        SmtpClient SmtpMail = new SmtpClient();
                        SmtpMail.Host = emailConfiguration.EmailServer;
                        SmtpMail.Port = emailConfiguration.EmailPort;
                        SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpMail.EnableSsl = false;
                        SmtpMail.ServicePoint.MaxIdleTime = 0;
                        SmtpMail.ServicePoint.SetTcpKeepAlive(true, 2000, 2000);


                        SmtpMail.Send(message);
                    }
                }
            }
            catch (Exception ex)
            {
                //return "Error";
            }
        }
        public void SendRequestMail(LayoutNotes layoutNotes, string fromAddress, string toAddress, string customername, string layoutname, string SelectedStatus, string urlString, string username)
        {
            try
            {
                MailMessage message = new MailMessage();

                if (toAddress.Contains("|"))
                {
                    var toaddess = toAddress.Split("|").Where(x => x.Trim().Length > 0).ToList();
                    foreach (var mailid in toaddess[0].Split("ƒ").Where(x => x.Trim().Length > 0))
                        message.To.Add(mailid);
                    if(toaddess.Count() > 1)
                    {
                        foreach (var mailid in toaddess[1].Split("ƒ").Where(x => x.Trim().Length > 0))
                            message.CC.Add(mailid);
                    }
                }
                else
                {
                    message.To.Add(toAddress.Replace("|", ""));
                }

                message.Subject = string.Format("TX-CREATIS:{0} , requesting {1} for {2} - {3}", username, SelectedStatus, customername, layoutname);
                message.From = new System.Net.Mail.MailAddress(fromAddress, fromAddress);
                message.IsBodyHtml = true;

                string strContent = @" 
<p>Hi,</p><p>Please find the below Url for the request layout.</p>
                    <table> 
                       <tr>  
                            <td style='width:100px;background-color: #ccc'> DashBoard Url : </td>  
	                        <td> " + urlString + @" </td>  
                       </tr> 
<tr>  
                            <td style='width:100px;background-color: #ccc'> Notes : </td>  
	                        <td> " + layoutNotes.Notes + @" </td>  
                       </tr> 
                    </table>";

                message.Body = strContent;

                message.BodyEncoding = Encoding.Default;
                message.Priority = MailPriority.High;

                SmtpClient SmtpMail = new SmtpClient();
                SmtpMail.Host = emailConfiguration.EmailServer;
                SmtpMail.Port = emailConfiguration.EmailPort;
                SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpMail.EnableSsl = false;
                SmtpMail.ServicePoint.MaxIdleTime = 0;
                SmtpMail.ServicePoint.SetTcpKeepAlive(true, 2000, 2000);


                SmtpMail.Send(message);
            }
            catch (Exception ex)
            {
                //return "Error";
            }
        }
    }
}

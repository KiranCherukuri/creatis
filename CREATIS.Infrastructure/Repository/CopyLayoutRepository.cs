﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CREATIS.ApplicationCore.Modals;
using System.Data.SqlClient;
using CREATIS.Infrastructure.IRepository;
using CREATIS.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace CREATIS.Infrastructure.Repository
{
    public class CopyLayoutRepository : Utility, ICopyLayoutRepository
    {
        public readonly ITranslatorRepository _translatorRepository;
        public readonly IQuestionPathRepository _questionPathRepository;

        public CopyLayoutRepository(ITranslatorRepository translatorRepository, IQuestionPathRepository questionPathRepository)
        {
            this._translatorRepository = translatorRepository;
            this._questionPathRepository = questionPathRepository;
        }

        public Creatis_Context dbconext;
        public string SaveCopyLayout(int LayoutId, string NewLayoutName, int CustomerId, string LanguageIds, string UserName, bool InfoColumn, string EmailId,string displayName)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var layoutInfo = context.Layout.Where(x => x.LayoutId == LayoutId).Select(x => new { x.LayoutName, x.LayoutStatusRefId, x.LayoutType, x.CustomerRefId, x.EmailId }).FirstOrDefault();
                    var customerInfo = context.Customers.Where(x => x.CustomerId == (layoutInfo == null ? 0 : layoutInfo.CustomerRefId)).Select(x => x.Name).FirstOrDefault();
                    var layoutOBCInfo = context.OBCType.Where(x => x.LayoutRefId == LayoutId).Select(x => new { x.OBCTypeRefId }).FirstOrDefault();
                    var layoutDocumentScanFlexInfo = context.DocumentTypeFlex.Where(x => x.LayoutRefId == LayoutId && x.IsActive).Select(x => new { x.DocumentTypeRefId }).ToList();
                    var layoutPlanningTypeInfo = context.PlanningType.Where(x => x.LayoutRefId == LayoutId && x.IsActive).Select(x => new { x.PlanningTypeRefId }).ToList();
                    var layoutPlanningSpecificationInfo = context.PlanningSpecification.Where(x => x.LayoutRefId == LayoutId && x.IsActive).Select(x => new { x.PlanningSpecificationRefId }).ToList();
                    var layoutTransportTypeInfo = context.TransportTypes.Where(x => x.LayoutRefId == LayoutId && x.IsActive).Select(x => new { x.TransportTypeId, x.TransportTypeName }).ToList();
                    var layoutAllowancesInfo = context.Allowances.Where(x => x.LayoutRefId == LayoutId && x.IsActive).Select(x => new { x.AllowancesId, x.AllowancesRefId }).ToList();
                    var layoutAllowancesMasterInfo = context.AllowancesMaster.Where(x => x.CustomerRefId == layoutInfo.CustomerRefId).Select(x => new { x.IS_Active, x.AllowancesMasterId, x.AllowancesCode, x.AllowancesMasterDescription, x.OBCTypeRefId, x.CustomerRefId }).ToList();
                    var layoutActivitiesRegistrationInfo = context.ActivitiesRegistration.Where(x => x.LayoutRefId == LayoutId && x.ISModification != (int)EnumCopyColorCode.Red).ToList();

                    Layout modal = new Layout();
                    OBCType modalobc = new OBCType();
                    PlanningType modalpt = new PlanningType();
                    PlanningSpecification modalps = new PlanningSpecification();
                    DocumentTypeFlex modaldsf = new DocumentTypeFlex();
                    TransportTypes modaltt = new TransportTypes();
                    Allowances modalal = new Allowances();
                    AllowancesMaster modalalm = new AllowancesMaster();
                    UsedLanguages modalul = new UsedLanguages();
                    ActivitiesRegistration modalar = new ActivitiesRegistration();

                    if (layoutInfo != null)
                    {
                        modal.CustomerRefId = CustomerId;
                        modal.IsLocked = 1;
                        modal.LayoutName = NewLayoutName;
                        modal.LayoutOrigin = (customerInfo ?? "") + " - " + layoutInfo.LayoutName;
                        modal.LayoutStatusRefId = (int)EnumLayoutStatus.InProgress;
                        modal.LayoutType = layoutInfo.LayoutType;
                        modal.EmailId = EmailId;
                        modal.IsCopyLayout = true;
                        modal.CreatedBy = UserName;
                        modal.CreatedOn = DateTime.Now;
                        modal.UpdatedBy = UserName;
                        modal.UpdatedOn = DateTime.Now;

                        context.Layout.Add(modal);
                        context.SaveChanges();


                        LayoutNotes layoutNotes = new LayoutNotes();
                        layoutNotes.LayoutRefId = modal.LayoutId;
                        layoutNotes.Notes = "Layout Created (Copy " + (customerInfo ?? "") + " - " + layoutInfo.LayoutName + ")";
                        layoutNotes.LayoutStatusRefId = (int)EnumLayoutStatus.InProgress;
                        layoutNotes.CreatedBy = UserName;
                        layoutNotes.CreatedOn = DateTime.Now;
                        layoutNotes.CreatedByName = displayName;
                        UpdateLayoutNotes(layoutNotes);

                        if (InfoColumn)
                        {
                            InfoColumns modelic = new InfoColumns();
                            var infoCloumnsList = context.InfoColumn.Where(x => x.CustomerRefId == layoutInfo.CustomerRefId).ToList();
                            if (infoCloumnsList.Any())
                            {
                                foreach (var infocolumn in infoCloumnsList)
                                {
                                    modelic = new InfoColumns();
                                    modelic.CustomerRefId = CustomerId;
                                    modelic.ICM_Column_Ref_No = infocolumn.ICM_Column_Ref_No;
                                    modelic.IC_Column_Name = infocolumn.IC_Column_Name;
                                    modelic.IS_Active = infocolumn.IS_Active;
                                    modelic.CreatedBy = UserName;
                                    modelic.CreatedOn = DateTime.Now;
                                    modelic.UpdatedBy = UserName;
                                    modelic.UpdatedOn = DateTime.Now;

                                    context.InfoColumn.Add(modelic);
                                    context.SaveChanges();

                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = infocolumn.IC_ID;
                                    langrefdetail.QuestionRefId = null;
                                    langrefdetail.NewRefId = modelic.IC_ID;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = modal.LayoutId;
                                    langrefdetail.CustomerRefId = modelic.CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.InfoColumns_IC_Column_Name;
                                    langrefdetail.IsModification = (int)EnumCopyColorCode.Empty;
                                    langrefdetail.CreatedBy = modelic.CreatedBy;
                                    langrefdetail.UpdatedBy = modelic.CreatedBy;

                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */
                                }
                            }
                        }
                    }

                    if (layoutOBCInfo != null)
                    {
                        modalobc.CustomerRefId = CustomerId;
                        modalobc.LayoutRefId = modal.LayoutId;
                        modalobc.OBCTypeRefId = layoutOBCInfo.OBCTypeRefId;
                        modalobc.IsModification = (int)EnumCopyColorCode.Empty;
                        modalobc.CreatedBy = UserName;
                        modalobc.CreatedOn = DateTime.Now;
                        modalobc.UpdatedBy = UserName;
                        modalobc.UpdatedOn = DateTime.Now;

                        context.OBCType.Add(modalobc);
                        context.SaveChanges();
                    }
                    if (layoutPlanningTypeInfo.Any())
                    {
                        foreach (var LayoutPlanningType in layoutPlanningTypeInfo)
                        {
                            modalpt = new PlanningType();
                            modalpt.CustomerRefId = CustomerId;
                            modalpt.LayoutRefId = modal.LayoutId;
                            modalpt.PlanningTypeRefId = LayoutPlanningType.PlanningTypeRefId;
                            modalpt.CreatedBy = UserName;
                            modalpt.CreatedOn = DateTime.Now;
                            modalpt.UpdatedBy = UserName;
                            modalpt.UpdatedOn = DateTime.Now;
                            modalpt.IsModification = (int)EnumCopyColorCode.Empty;
                            modalpt.IsActive = true;

                            context.PlanningType.Add(modalpt);
                            context.SaveChanges();
                        }
                    }
                    if (layoutPlanningSpecificationInfo.Any())
                    {
                        foreach (var LayoutPlanningSpecification in layoutPlanningSpecificationInfo)
                        {
                            modalps = new PlanningSpecification();

                            modalps.CustomerRefId = CustomerId;
                            modalps.LayoutRefId = modal.LayoutId;
                            modalps.PlanningSpecificationRefId = LayoutPlanningSpecification.PlanningSpecificationRefId;
                            modalps.CreatedBy = UserName;
                            modalps.CreatedOn = DateTime.Now;
                            modalps.UpdatedBy = UserName;
                            modalps.UpdatedOn = DateTime.Now;
                            modalps.IsModification = (int)EnumCopyColorCode.Empty;
                            modalps.IsActive = true;

                            context.PlanningSpecification.Add(modalps);
                            context.SaveChanges();
                        }

                    }

                    if (layoutDocumentScanFlexInfo.Any())
                    {
                        foreach (var layoutDocumentType in layoutDocumentScanFlexInfo)
                        {
                            modaldsf = new DocumentTypeFlex();

                            modaldsf.LayoutRefId = modal.LayoutId;
                            modaldsf.DocumentTypeRefId = layoutDocumentType.DocumentTypeRefId;
                            modaldsf.CreatedBy = UserName;
                            modaldsf.CreatedOn = DateTime.Now;
                            modaldsf.UpdatedBy = UserName;
                            modaldsf.UpdatedOn = DateTime.Now;
                            modaldsf.IsModification = (int)EnumCopyColorCode.Empty;
                            modaldsf.IsActive = true;

                            context.DocumentTypeFlex.Add(modaldsf);
                            context.SaveChanges();
                        }
                    }

                    if (layoutTransportTypeInfo.Any())
                    {
                        foreach (var LayoutTransportType in layoutTransportTypeInfo)
                        {
                            modaltt = new TransportTypes();

                            modaltt.CustomerRefId = CustomerId;
                            modaltt.LayoutRefId = modal.LayoutId;
                            modaltt.TransportTypeName = LayoutTransportType.TransportTypeName;
                            modaltt.CreatedBy = UserName;
                            modaltt.CreatedOn = DateTime.Now;
                            modaltt.UpdatedBy = UserName;
                            modaltt.UpdatedOn = DateTime.Now;
                            modaltt.CopyLayoutRefId = LayoutTransportType.TransportTypeId;
                            modaltt.IsModification = (int)EnumCopyColorCode.Empty;
                            modaltt.IsActive = true;

                            context.TransportTypes.Add(modaltt);
                            context.SaveChanges();
                        }

                    }


                    if (LanguageIds.Trim().Length > 0)
                    {
                        var LanguageIdList = LanguageIds.Split('-').Where(x => x.Length > 0).ToList().Select(int.Parse).ToList();

                        if (LanguageIdList.Where(x => x == 66).Count() == 0)
                            LanguageIdList.Add(66);

                        foreach (var LanguageId in LanguageIdList)
                        {
                            var isDefault = context.UsedLanguages.Where(x => x.LayoutRefId == LayoutId && x.LanguageRefId == LanguageId).Select(x => x.IsDefault).FirstOrDefault();

                            modalul = new UsedLanguages();

                            modalul.CustomerRefId = CustomerId;
                            modalul.LayoutRefId = modal.LayoutId;
                            modalul.LanguageRefId = LanguageId;
                            modalul.IsDefault = isDefault;
                            modalul.IsModification = (int)EnumCopyColorCode.Empty;
                            modalul.IsActive = true;
                            modalul.CreatedBy = UserName;
                            modalul.CreatedOn = DateTime.Now;
                            modalul.UpdatedBy = UserName;
                            modalul.UpdatedOn = DateTime.Now;

                            context.UsedLanguages.Add(modalul);
                            context.SaveChanges();
                        }
                    }

                    if (layoutAllowancesMasterInfo.Count() > 0 && layoutInfo.CustomerRefId != CustomerId)
                    {
                        foreach (var layoutAllowancesMaster in layoutAllowancesMasterInfo)
                        {
                            modalalm = new AllowancesMaster();

                            modalalm.CustomerRefId = CustomerId;
                            modalalm.AllowancesMasterDescription = layoutAllowancesMaster.AllowancesMasterDescription;
                            modalalm.AllowancesCode = layoutAllowancesMaster.AllowancesCode;
                            modalalm.IS_Active = layoutAllowancesMaster.IS_Active;
                            modalalm.OBCTypeRefId = layoutAllowancesMaster.OBCTypeRefId;
                            modalalm.CopyLayoutRefId = layoutAllowancesMaster.AllowancesMasterId;
                            modalalm.CreatedBy = UserName;
                            modalalm.CreatedOn = DateTime.Now;
                            modalalm.UpdatedBy = UserName;
                            modalalm.UpdatedOn = DateTime.Now;

                            context.AllowancesMaster.Add(modalalm);
                            context.SaveChanges();

                        }
                    }

                    if (layoutAllowancesInfo.Any())
                    {
                        foreach (var LayoutAllowances in layoutAllowancesInfo)
                        {
                            var allowancesRefId = context.AllowancesMaster.Where(x => x.CopyLayoutRefId == LayoutAllowances.AllowancesRefId).Count();

                            modalal = new Allowances();

                            modalal.CustomerRefId = CustomerId;
                            modalal.LayoutRefId = modal.LayoutId;
                            modalal.AllowancesRefId = (allowancesRefId == 0 ? LayoutAllowances.AllowancesRefId : context.AllowancesMaster.Where(x => x.CopyLayoutRefId == LayoutAllowances.AllowancesRefId).Select(x => x.AllowancesMasterId).FirstOrDefault());
                            modalal.CreatedBy = UserName;
                            modalal.CreatedOn = DateTime.Now;
                            modalal.UpdatedBy = UserName;
                            modalal.UpdatedOn = DateTime.Now;
                            modalal.IsModification = (int)EnumCopyColorCode.Empty;
                            modalal.IsActive = true;

                            context.Allowances.Add(modalal);
                            context.SaveChanges();

                            /* Written By Selvam Thangaraj - Go */

                            LangRefDetail langrefdetail = new LangRefDetail();
                            langrefdetail.RefId = LayoutAllowances.AllowancesId;
                            langrefdetail.QuestionRefId = null;
                            langrefdetail.NewRefId = modalal.AllowancesId;
                            langrefdetail.LayoutRefId = LayoutId;
                            langrefdetail.NewLatoutRefId = modal.LayoutId;
                            langrefdetail.CustomerRefId = modalal.CustomerRefId;
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.Allowances_AllowancesMasterDescription;
                            langrefdetail.IsModification = (int)EnumCopyColorCode.Empty;
                            langrefdetail.CreatedBy = modalal.CreatedBy;
                            langrefdetail.UpdatedBy = modalal.CreatedBy;

                            _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                            /* Written By Selvam Thangaraj - Stop */
                        }

                        var layoutAllowancesId = layoutAllowancesInfo.Select(x => x.AllowancesRefId).ToList();
                        var layoutAllowancesMaster = context.AllowancesMaster.Where(x => layoutAllowancesId.Contains(x.CopyLayoutRefId ?? 0)).ToList();
                        foreach (var target in layoutAllowancesMaster)
                        {
                            target.CopyLayoutRefId = null;
                            context.SaveChanges();
                        }
                    }

                    if (layoutActivitiesRegistrationInfo.Any())
                    {
                        foreach (var LayoutActivitiesRegistration in layoutActivitiesRegistrationInfo)
                        {
                            modalar = new ActivitiesRegistration();
                            modalar.CustomerRefId = CustomerId;
                            modalar.LayoutRefId = modal.LayoutId;
                            if (LayoutActivitiesRegistration.ISModification == 0)
                            {
                                modalar.ISModification = 1;
                            }
                            else
                            {
                                if (LayoutActivitiesRegistration.ISModification != 2)
                                {
                                    modalar.ISModification = LayoutActivitiesRegistration.ISModification;
                                }
                                else
                                {
                                    modalar.ISModification = 1;
                                }
                            }
                            modalar.CreatedBy = UserName;
                            modalar.CreatedOn = DateTime.Now;
                            modalar.UpdatedBy = UserName;
                            modalar.UpdatedOn = DateTime.Now;

                            modalar.ActivitiesRegistrationName = LayoutActivitiesRegistration.ActivitiesRegistrationName;
                            modalar.ConfirmButton = LayoutActivitiesRegistration.ConfirmButton;
                            modalar.EmptyFullSolo = LayoutActivitiesRegistration.EmptyFullSolo;
                            modalar.Flexactivity = LayoutActivitiesRegistration.Flexactivity;
                            modalar.InterruptibleByRefId = LayoutActivitiesRegistration.InterruptibleByRefId;
                            modalar.OBCDefault = LayoutActivitiesRegistration.OBCDefault;
                            modalar.PTORefId = LayoutActivitiesRegistration.PTORefId;
                            modalar.PlanningOverviewRefId = LayoutActivitiesRegistration.PlanningOverviewRefId;
                            modalar.PlanningRefId = LayoutActivitiesRegistration.PlanningRefId;
                            modalar.SpecificRefId = LayoutActivitiesRegistration.SpecificRefId;
                            modalar.TransportType = LayoutActivitiesRegistration.TransportType;
                            modalar.Visible = LayoutActivitiesRegistration.Visible;
                            modalar.WorkingCodeRefId = LayoutActivitiesRegistration.WorkingCodeRefId;
                            modalar.ActRegPosition = LayoutActivitiesRegistration.ActRegPosition;
                            modalar.CopyLayoutRefId = LayoutActivitiesRegistration.ActivitiesRegistrationId;
                            modalar.DefaultActivitiesRefId = LayoutActivitiesRegistration.DefaultActivitiesRefId;
                            modalar.DefaultLanguageId = LayoutActivitiesRegistration.DefaultLanguageId;
                            modalar.ScreenLayoutIsModification = (int)EnumCopyColorCode.Empty;


                            modalar.IsModificationActivitiesRegistrationName = (int)EnumCopyColorCode.Empty;
                            modalar.IsModificationWorkingCodeRefId = (int)EnumCopyColorCode.Empty;
                            modalar.IsModificationPlanningRefId = (int)EnumCopyColorCode.Empty;
                            modalar.IsModificationPlanningOverviewRefId = (int)EnumCopyColorCode.Empty;
                            modalar.IsModificationSpecificRefId = (int)EnumCopyColorCode.Empty;
                            modalar.IsModificationVisible = (int)EnumCopyColorCode.Empty;
                            modalar.IsModificationConfirmButton = (int)EnumCopyColorCode.Empty;
                            modalar.IsModificationTransportType = (int)EnumCopyColorCode.Empty;
                            modalar.IsModificationEmptyFullSolo = (int)EnumCopyColorCode.Empty;
                            modalar.IsModificationFlexactivity = (int)EnumCopyColorCode.Empty;
                            modalar.IsModificationPTORefId = (int)EnumCopyColorCode.Empty;
                            modalar.IsModificationInterruptibleByRefId = (int)EnumCopyColorCode.Empty;

                            context.ActivitiesRegistration.Add(modalar);
                            context.SaveChanges();

                            /* Written By Selvam Thangaraj - Go */

                            LangRefDetail langrefdetail = new LangRefDetail();
                            langrefdetail.RefId = LayoutActivitiesRegistration.ActivitiesRegistrationId;
                            langrefdetail.NewRefId = modalar.ActivitiesRegistrationId;
                            langrefdetail.QuestionRefId = null;
                            langrefdetail.LayoutRefId = LayoutId;
                            langrefdetail.NewLatoutRefId = modal.LayoutId;
                            langrefdetail.CustomerRefId = modalar.CustomerRefId;
                            langrefdetail.IsModification = (int)EnumCopyColorCode.Empty;
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.ActivitiesRegistration_ActivitiesRegistrationName;
                            langrefdetail.CreatedBy = modalar.CreatedBy;
                            langrefdetail.UpdatedBy = modalar.CreatedBy;

                            _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                            /* Written By Selvam Thangaraj - Stop */
                        }

                    }

                    SaveQuestionPath(LayoutId, 0, 0, new List<int>(), modal.LayoutId, 0, 0, 0, modal.CreatedBy, "LC");

                    return modal.LayoutId.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string SaveQuestionPath(int LayoutId, int ActivitiesRegistrationId, int TransportTypeId, List<int> QuestionIds, int NewLayoutId, int NewActivitiesRegistrationId, int NewTransportTypeId, int NewQuestionId, string CreatedBy, string Type)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (Type == "MC")
                        LayoutId = context.Questions.Where(x => x.ISModification != (int)EnumCopyColorCode.Red && QuestionIds.Contains(x.QuestionId)).Select(x => x.LayoutRefId).FirstOrDefault();

                    var questionsList = context.Questions.Where(x => x.ISModification != (int)EnumCopyColorCode.Red && (Type == "MC" ? QuestionIds.Contains(x.QuestionId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).OrderBy(x => x.Position).ToList();
                    var chiocePropertiesList = context.ChioceProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var chiocePropertiesIdList = chiocePropertiesList.Select(x => x.ChioceId).ToList();
                    var addChiocePropertiesList = context.AddChioceProperties.Where(x => chiocePropertiesIdList.Contains(x.ChioceRefId)).ToList();
                    var entryPropertiesList = context.EntryProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var planningselectPropertiesList = context.PlanningselectProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var planningselectids = planningselectPropertiesList.Select(x => x.PlanningselectId).ToList();
                    var planningselectreadoutlist = context.PlanningSelectReadoutDetail.Where(x => planningselectids.Contains(x.PlanningSelectRefId)).ToList();
                    var checkoffPropertiesList = context.CheckoffProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var conditionalPropertiesList = context.ConditionalProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var infomessagePropertiesList = context.InfomessageProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var textMessagePropertiesList = context.TextMessageProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var varListPropertiesList = context.VarListProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var varListPropertiesIds = varListPropertiesList.Select(x => x.VarListPropertiesId).ToList();
                    var varListCommentReadoutDetailList = context.VarListCommentReadoutDetail.Where(x => varListPropertiesIds.Contains(x.VarListPropertiesRefId)).ToList();
                    var varListFilterDetailList = context.VarListFilterDetail.Where(x => varListPropertiesIds.Contains(x.VarListPropertiesRefId)).ToList();
                    var setActionValuePropertiesList = context.SetActionValueProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var documetScanPropertiesList = context.DocumetScanProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var documetScanTitleList = context.DocumentScanTitle.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var documetScanTypeList = context.DocumentScanType.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1))).ToList();

                    var transportTypePropertiesList = context.TransportTypeProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var transportTypePropertiesIdList = transportTypePropertiesList.Select(x => x.TransportTypeId).ToList();
                    var transportTypePropertiesDetailList = context.TransportTypePropertiesDetail.Where(x => transportTypePropertiesIdList.Contains(x.TransportTypeRefId)).ToList();
                    var trailerPropertiesList = context.TrailerProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var getSystemValuesPropertiesList = context.GetSystemValuesProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefId) : (1 == 1)) && x.LayoutRefId == LayoutId && (Type == "TC" ? (x.ActivitiesRefId == ActivitiesRegistrationId) : (1 == 1))).ToList();


                    var actRegList = context.ActivitiesRegistration.Where(x => x.LayoutRefId == NewLayoutId).Select(x => new { x.ActivitiesRegistrationId, x.CopyLayoutRefId }).ToList();
                    var transportTypeList = context.TransportTypes.Where(x => x.LayoutRefId == NewLayoutId && x.IsActive).Select(x => new { x.TransportTypeId, x.CopyLayoutRefId }).ToList();

                    Questions modalque = new Questions();
                    ConditionalProperties modalcaq = new ConditionalProperties();
                    InfomessageProperties modalimq = new InfomessageProperties();
                    TextMessageProperties modaltmq = new TextMessageProperties();
                    SetActionValueProperties modalsaq = new SetActionValueProperties();
                    VarListProperties modalvlq = new VarListProperties();
                    VarListCommentReadoutDetail modalvlcr = new VarListCommentReadoutDetail();
                    VarListFilterDetail modalvlf = new VarListFilterDetail();
                    PlanningselectProperties modalpsq = new PlanningselectProperties();
                    CheckoffProperties modalcoq = new CheckoffProperties();
                    DocumetScanProperties modaldsq = new DocumetScanProperties();
                    DocumentScanTitle modaldstq = new DocumentScanTitle();
                    DocumentScanType modaldstyq = new DocumentScanType();

                    EntryProperties modaleq = new EntryProperties();
                    ChioceProperties modalcq = new ChioceProperties();
                    AddChioceProperties modalacq = new AddChioceProperties();

                    TransportTypeProperties modalttq = new TransportTypeProperties();
                    TransportTypePropertiesDetail modalattdq = new TransportTypePropertiesDetail();
                    TrailerProperties modaltq = new TrailerProperties();
                    GetSystemValuesProperties modalgsv = new GetSystemValuesProperties();

                    //AliasNameDetails modaland = new AliasNameDetails();

                    List<int> planningSelectIds = new List<int>();
                    List<int> setActionValueIds = new List<int>();

                    var flexPropertiesList = context.Flex.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var extraInfoPropertiesList = context.ExtraInfoProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var palletsPropertiesList = context.PalletsProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var problemsPropertiesList = context.ProblemsProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var signaturesPropertiesList = context.SignaturesProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var DocumentScannerPropertiesList = context.DocumentScannerProperties.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();

                    var flexList = context.Flex.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var extraInfoList = context.ExtraInfo.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var palletsList = context.Pallets.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var problemsList = context.Problems.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var signaturesList = context.Signatures.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var pictureList = context.Picure.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var DocumentScannerList = context.DocumentScanner.Where(x => (Type == "MC" ? QuestionIds.Contains(x.QuestionRefID ?? 0) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();


                    Flex modalf = new Flex();
                    ExtraInfo modalei = new ExtraInfo();
                    Pallets modalp = new Pallets();
                    Problems modalplm = new Problems();
                    Signatures modals = new Signatures();
                    DocumentScanner modalds = new DocumentScanner();
                    Picure modalpcr = new Picure();

                    ExtraInfoProperties modaleiprop = new ExtraInfoProperties();
                    PalletsProperties modalpprop = new PalletsProperties();
                    ProblemsProperties modalplmprop = new ProblemsProperties();
                    SignaturesProperties modalsprop = new SignaturesProperties();
                    DocumentScannerProperties modaldsprop = new DocumentScannerProperties();

                    SmartExtraInfo modalsei = new SmartExtraInfo();
                    SmartPallets modalsps = new SmartPallets();
                    SmartProblems modalsplm = new SmartProblems();

                    var smartExraInfoPropertiesList = context.SmartExtraInfo.Where(x => (Type == "MC" ? QuestionIds.Contains(x.SmartExtraInfoRefID ?? 0) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var smartPalletsPropertiesList = context.SmartPallets.Where(x => (Type == "MC" ? QuestionIds.Contains(x.SmartPalletsRefID ?? 0) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();
                    var smartProblemsPropertiesList = context.SmartProblems.Where(x => (Type == "MC" ? QuestionIds.Contains(x.SmartProblemsRefID ?? 0) : (1 == 1)) && x.LayoutRefID == LayoutId && (Type == "TC" ? (x.ActivityRefID == ActivitiesRegistrationId) : (1 == 1))).ToList();

                    var CustomerRefId = context.Layout.Where(x => x.LayoutId == NewLayoutId).Select(x => x.CustomerRefId).FirstOrDefault();
                    var languageName = context.UsedLanguages.Where(x => x.LayoutRefId == NewLayoutId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();
                    var isCopyLayout = false;
                    if (Type == "MC")
                        isCopyLayout = context.Layout.Where(x => x.LayoutId == NewLayoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    if (questionsList.Any())
                    {
                        int position = 0;
                        
                        foreach (var questions in questionsList)
                        {
                            if (position == 0)
                                position = (context.Questions.Where(x => x.LayoutRefId == questions.LayoutRefId && x.ActivitiesRefId == questions.ActivitiesRefId).Count() > 0 ? context.Questions.Where(x => x.LayoutRefId == questions.LayoutRefId && x.ActivitiesRefId == questions.ActivitiesRefId).Max(x => x.Position ?? 0) : 0);

                            modalque = new Questions();
                            modalque.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == questions.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                            if (questions.PropertyName == EnumToolBoxItems.Info_Message.ToString().Replace("_", " "))
                            {
                                string[] strActivityData = new string[2];
                                strActivityData = GetCountofInfoMessage(modalque.ActivitiesRefId, "QP");
                                questions.Name = "Info " + strActivityData[1] + strActivityData[0];
                            }
                            if (questions.PropertyName == EnumToolBoxItems.Text_Message.ToString().Replace("_", " "))
                            {
                                string[] strTextActivityData = new string[2];
                                strTextActivityData = GetCountofTextMessage(modalque.ActivitiesRefId, "QP");
                                questions.Name = "SMS " + strTextActivityData[1] + " # " + strTextActivityData[0];
                            }

                            modalque.CreatedBy = CreatedBy;
                            modalque.CreatedOn = DateTime.Now;
                            modalque.LayoutRefId = NewLayoutId;
                            modalque.Name = questions.Name;
                            modalque.ParentId = questions.ParentId;
                            modalque.TransporttypeRefId = (Type == "LC" ? transportTypeList.Where(x => x.CopyLayoutRefId == questions.TransporttypeRefId).Select(x => x.TransportTypeId).FirstOrDefault() : NewTransportTypeId);
                            modalque.UpdatedBy = CreatedBy;
                            modalque.UpdatedOn = DateTime.Now;
                            modalque.PropertyName = questions.PropertyName;
                            modalque.CopyLayoutRefId = questions.QuestionId;
                            modalque.ISModification = (Type == "LC" ? (int)EnumCopyColorCode.Empty : ((isCopyLayout && Type == "MC") ? (int)EnumCopyColorCode.Green : questions.ISModification));
                            modalque.DefaultLanguageId = questions.DefaultLanguageId;
                            modalque.IsQuestionPath = questions.IsQuestionPath;
                            modalque.IsFlex = questions.IsFlex;
                            modalque.IsSmart = questions.IsSmart;
                            modalque.IsParent = questions.IsParent;
                            modalque.IsDefault = questions.IsDefault;
                            modalque.IsChild = questions.IsChild;
                            modalque.Position = position + 1;
                            context.Questions.Add(modalque);
                            context.SaveChanges();

                            /* Written By Selvam Thangaraj - Go */

                            LangRefDetail langrefdetail = new LangRefDetail();
                            langrefdetail.RefId = questions.QuestionId;
                            langrefdetail.NewRefId = modalque.QuestionId;
                            langrefdetail.QuestionRefId = modalque.QuestionId;
                            langrefdetail.LayoutRefId = LayoutId;
                            langrefdetail.NewLatoutRefId = NewLayoutId;
                            langrefdetail.CustomerRefId = CustomerRefId;
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                            langrefdetail.LangText = modalque.Name;
                            langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                            langrefdetail.CreatedBy = modalque.CreatedBy;
                            langrefdetail.UpdatedBy = modalque.CreatedBy;
                            langrefdetail.Language = languageName;

                            _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                            /* Written By Selvam Thangaraj - Stop */

                            position++;

                            var PropertyName = questions.PropertyName.Replace(" ", "_").Replace("/", "_");
                            if (PropertyName == EnumToolBoxItems.Flex.ToString())
                            {
                                var targetf = flexList.Where(x => x.QuestionRefID == questions.QuestionId).FirstOrDefault();
                                if (targetf != null)
                                {
                                    modalf = new Flex();

                                    modalf.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetf.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalf.LayoutRefID = NewLayoutId;
                                    modalf.Title = targetf.Title;
                                    modalf.AlternativetextonPlacelevel = targetf.AlternativetextonPlacelevel;
                                    modalf.AlternativetextonJoblevel = targetf.AlternativetextonJoblevel;
                                    modalf.Alternativetextonproductlevel = targetf.Alternativetextonproductlevel;
                                    modalf.Status = targetf.Status;
                                    modalf.CreatedBy = CreatedBy;
                                    modalf.CreatedOn = DateTime.Now;
                                    modalf.UpdatedBy = CreatedBy;
                                    modalf.UpdatedOn = DateTime.Now;
                                    modalf.QuestionRefID = modalque.QuestionId;

                                    context.Flex.Add(modalf);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Smart_Extra_Infop.ToString())
                            {
                                var targetsei = smartExraInfoPropertiesList.Where(x => x.SmartExtraInfoRefID == questions.QuestionId).FirstOrDefault();
                                if (targetsei != null)
                                {
                                    modalsei = new SmartExtraInfo();

                                    modalsei.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetsei.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalsei.LayoutRefID = NewLayoutId;
                                    modalsei.MaskText = targetsei.MaskText;
                                    modalsei.Format = targetsei.Format;
                                    modalsei.PlaceLevel = targetsei.PlaceLevel;
                                    modalsei.JobLevel = targetsei.JobLevel;
                                    modalsei.ProductLevel = targetsei.ProductLevel;
                                    modalsei.PlanningFeedbackcode = targetsei.PlanningFeedbackcode;
                                    modalsei.CreatedBy = CreatedBy;
                                    modalsei.CreatedOn = DateTime.Now;
                                    modalsei.UpdatedBy = CreatedBy;
                                    modalsei.UpdatedOn = DateTime.Now;
                                    modalsei.QuestionRefID = context.Questions.Where(x => x.CopyLayoutRefId == targetsei.QuestionRefID).Select(x => x.QuestionId).FirstOrDefault();
                                    modalsei.Title = targetsei.Title;
                                    modalsei.Size = targetsei.Size;
                                    modalsei.RegEx = targetsei.RegEx;
                                    modalsei.Partnercode = targetsei.Partnercode;
                                    modalsei.SmartExtraInfoRefID = modalque.QuestionId;

                                    context.SmartExtraInfo.Add(modalsei);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Smart_Palletsp.ToString())
                            {
                                var targetsps = smartPalletsPropertiesList.Where(x => x.SmartPalletsRefID == questions.QuestionId).FirstOrDefault();
                                if (targetsps != null)
                                {
                                    modalsps = new SmartPallets();

                                    modalsps.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetsps.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalsps.LayoutRefID = NewLayoutId;
                                    modalsps.PlaceLevel = targetsps.PlaceLevel;
                                    modalsps.JobLevel = targetsps.JobLevel;
                                    modalsps.ProductLevel = targetsps.ProductLevel;
                                    modalsps.PlanningFeedbackcode = targetsps.PlanningFeedbackcode;
                                    modalsps.CreatedBy = CreatedBy;
                                    modalsps.CreatedOn = DateTime.Now;
                                    modalsps.Updatedby = CreatedBy;
                                    modalsps.UpdatedOn = DateTime.Now;
                                    modalsps.QuestionRefID = context.Questions.Where(x => x.CopyLayoutRefId == targetsps.QuestionRefID).Select(x => x.QuestionId).FirstOrDefault();
                                    modalsps.Title = targetsps.Title;
                                    modalsps.Partnercode = targetsps.Partnercode;
                                    modalsps.SmartPalletsRefID = modalque.QuestionId;

                                    context.SmartPallets.Add(modalsps);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Smart_Problemp.ToString())
                            {
                                var targetsplm = smartProblemsPropertiesList.Where(x => x.SmartProblemsRefID == questions.QuestionId).FirstOrDefault();
                                if (targetsplm != null)
                                {
                                    modalsplm = new SmartProblems();

                                    modalsplm.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetsplm.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalsplm.LayoutRefID = NewLayoutId;
                                    modalsplm.MaskText = targetsplm.MaskText;
                                    modalsplm.Format = targetsplm.Format;
                                    modalsplm.PlaceLevel = targetsplm.PlaceLevel;
                                    modalsplm.JobLevel = targetsplm.JobLevel;
                                    modalsplm.ProductLevel = targetsplm.ProductLevel;
                                    modalsplm.PlanningFeedbackcode = targetsplm.PlanningFeedbackcode;
                                    modalsplm.CreatedBy = CreatedBy;
                                    modalsplm.CreatedOn = DateTime.Now;
                                    modalsplm.UpdatedBy = CreatedBy;
                                    modalsplm.UpdatedOn = DateTime.Now;
                                    modalsplm.QuestionRefID = context.Questions.Where(x => x.CopyLayoutRefId == targetsplm.QuestionRefID).Select(x => x.QuestionId).FirstOrDefault();
                                    modalsplm.Title = targetsplm.Title;
                                    modalsplm.Size = targetsplm.Size;
                                    modalsplm.RegEx = targetsplm.RegEx;
                                    modalsplm.Partnercode = targetsplm.Partnercode;
                                    modalsplm.SmartProblemsRefID = modalque.QuestionId;

                                    context.SmartExtraInfo.Add(modalsei);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Extra_Info.ToString())
                            {
                                var targeteipList = extraInfoPropertiesList.Where(x => x.QuestionRefID == questions.QuestionId).ToList();
                                foreach (var targeteiprop in targeteipList)
                                {
                                    modaleiprop = new ExtraInfoProperties();

                                    modaleiprop.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targeteiprop.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modaleiprop.LayoutRefID = NewLayoutId;
                                    modaleiprop.CreatedBy = CreatedBy;
                                    modaleiprop.CreatedOn = DateTime.Now;
                                    modaleiprop.Updatedby = CreatedBy;
                                    modaleiprop.UpdatedOn = DateTime.Now;
                                    modaleiprop.QuestionRefID = modalque.QuestionId;
                                    modaleiprop.Title = targeteiprop.Title;
                                    modaleiprop.AlternativetextonPlacelevel = targeteiprop.AlternativetextonPlacelevel;
                                    modaleiprop.AlternativetextonJoblevel = targeteiprop.AlternativetextonJoblevel;
                                    modaleiprop.Alternativetextonproductlevel = targeteiprop.Alternativetextonproductlevel;
                                    modaleiprop.CopyLayoutRefId = targeteiprop.QuestionRefID;

                                    context.ExtraInfoProperties.Add(modaleiprop);
                                    context.SaveChanges();

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targeteiprop.ExtraInfoPropertiesID ?? 0;
                                    langrefdetail.NewRefId = modaleiprop.ExtraInfoPropertiesID ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_Title;
                                    langrefdetail.LangText = modaleiprop.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modaleiprop.CreatedBy;
                                    langrefdetail.UpdatedBy = modaleiprop.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if (modaleiprop.AlternativetextonPlacelevel != null && modaleiprop.AlternativetextonPlacelevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnPlaceLevel;
                                        langrefdetail.LangText = modaleiprop.AlternativetextonPlacelevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modaleiprop.AlternativetextonJoblevel != null && modaleiprop.AlternativetextonJoblevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnJobLevel;
                                        langrefdetail.LangText = modaleiprop.AlternativetextonJoblevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modaleiprop.Alternativetextonproductlevel != null && modaleiprop.Alternativetextonproductlevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnProductLevel;
                                        langrefdetail.LangText = modaleiprop.Alternativetextonproductlevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Extra_Infop.ToString())
                            {
                                var targeteiList = extraInfoList.Where(x => x.ExtraInfoRefId == questions.QuestionId).ToList();
                                foreach (var targetei in targeteiList)
                                {
                                    modalei = new ExtraInfo();

                                    modalei.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetei.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalei.LayoutRefID = NewLayoutId;
                                    modalei.MaskText = targetei.MaskText;
                                    modalei.Format = targetei.Format;
                                    modalei.Size = targetei.Size;
                                    modalei.RegEx = targetei.RegEx;
                                    modalei.PartnerCode = targetei.PartnerCode;
                                    modalei.PlaceLevel = targetei.PlaceLevel;
                                    modalei.JobLevel = targetei.JobLevel;
                                    modalei.ProductLevel = targetei.ProductLevel;
                                    modalei.PlanningFeedbackcode = targetei.PlanningFeedbackcode;
                                    modalei.CreatedBy = CreatedBy;
                                    modalei.CreatedOn = DateTime.Now;
                                    modalei.UpdatedBy = CreatedBy;
                                    modalei.UpdatedOn = DateTime.Now;
                                    modalei.QuestionRefID = context.ExtraInfoProperties.Where(x => x.CopyLayoutRefId == targetei.QuestionRefID).Select(x => x.QuestionRefID).FirstOrDefault();
                                    modalei.Title = targetei.Title;
                                    modalei.ExtraInfoRefId = modalque.QuestionId;

                                    context.ExtraInfo.Add(modalei);
                                    context.SaveChanges();

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetei.ExtraInfoID ?? 0;
                                    langrefdetail.NewRefId = modalei.ExtraInfoID ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfo_Title;
                                    langrefdetail.LangText = modalei.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalei.CreatedBy;
                                    langrefdetail.UpdatedBy = modalei.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if (modalei.MaskText != null && modalei.MaskText.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfo_MaskText;
                                        langrefdetail.LangText = modalei.MaskText ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Pallets.ToString())
                            {
                                var targetppList = palletsPropertiesList.Where(x => x.QuestionRefID == questions.QuestionId).ToList();
                                foreach (var targetppprop in targetppList)
                                {
                                    modalpprop = new PalletsProperties();

                                    modalpprop.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetppprop.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalpprop.LayoutRefID = NewLayoutId;
                                    modalpprop.CreatedBy = CreatedBy;
                                    modalpprop.CreatedOn = DateTime.Now;
                                    modalpprop.UpdatedBy = CreatedBy;
                                    modalpprop.UpdatedOn = DateTime.Now;
                                    modalpprop.QuestionRefID = modalque.QuestionId;
                                    modalpprop.Title = targetppprop.Title;
                                    modalpprop.AlternativetextonPlacelevel = targetppprop.AlternativetextonPlacelevel;
                                    modalpprop.AlternativetextonJoblevel = targetppprop.AlternativetextonJoblevel;
                                    modalpprop.Alternativetextonproductlevel = targetppprop.Alternativetextonproductlevel;
                                    modalpprop.CopyLayoutRefId = targetppprop.QuestionRefID;

                                    context.PalletsProperties.Add(modalpprop);
                                    context.SaveChanges();

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetppprop.PalletsPropertiesID ?? 0;
                                    langrefdetail.NewRefId = modalpprop.PalletsPropertiesID ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_Title;
                                    langrefdetail.LangText = modalpprop.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalpprop.CreatedBy;
                                    langrefdetail.UpdatedBy = modalpprop.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if (modalpprop.AlternativetextonPlacelevel != null && modalpprop.AlternativetextonPlacelevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnPlaceLevel;
                                        langrefdetail.LangText = modalpprop.AlternativetextonPlacelevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modalpprop.AlternativetextonJoblevel != null && modalpprop.AlternativetextonJoblevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnJobLevel;
                                        langrefdetail.LangText = modalpprop.AlternativetextonJoblevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modalpprop.Alternativetextonproductlevel != null && modalpprop.Alternativetextonproductlevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnProductLevel;
                                        langrefdetail.LangText = modalpprop.Alternativetextonproductlevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Palletsp.ToString())
                            {
                                var targetpList = palletsList.Where(x => x.PalletsRefId == questions.QuestionId).ToList();
                                foreach (var targetp in targetpList)
                                {
                                    modalp = new Pallets();

                                    modalp.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetp.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalp.LayoutRefID = NewLayoutId;
                                    modalp.ScanLevel = targetp.ScanLevel;
                                    modalp.PartnerCode = targetp.PartnerCode;
                                    modalp.PlaceLevel = targetp.PlaceLevel;
                                    modalp.JobLevel = targetp.JobLevel;
                                    modalp.ProductLevel = targetp.ProductLevel;
                                    modalp.PlanningFeedbackcode = targetp.PlanningFeedbackcode;
                                    modalp.CreatedBy = CreatedBy;
                                    modalp.CreatedOn = DateTime.Now;
                                    modalp.UpdatedBy = CreatedBy;
                                    modalp.UpdatedOn = DateTime.Now;
                                    modalp.QuestionRefID = context.PalletsProperties.Where(x => x.CopyLayoutRefId == targetp.QuestionRefID).Select(x => x.QuestionRefID).FirstOrDefault();
                                    modalp.Title = targetp.Title;
                                    modalp.PalletsRefId = modalque.QuestionId;

                                    context.Pallets.Add(modalp);
                                    context.SaveChanges();

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetp.PalletsID ?? 0;
                                    langrefdetail.NewRefId = modalp.PalletsID ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.Pallets_Title;
                                    langrefdetail.LangText = modalp.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalp.CreatedBy;
                                    langrefdetail.UpdatedBy = modalp.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Problem.ToString())
                            {
                                var targetppList = problemsPropertiesList.Where(x => x.QuestionRefID == questions.QuestionId).ToList();
                                foreach (var targetproprop in targetppList)
                                {
                                    modalplmprop = new ProblemsProperties();

                                    modalplmprop.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetproprop.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalplmprop.LayoutRefID = NewLayoutId;
                                    modalplmprop.CreatedBy = CreatedBy;
                                    modalplmprop.CreatedOn = DateTime.Now;
                                    modalplmprop.UpdatedBy = CreatedBy;
                                    modalplmprop.UpdatedOn = DateTime.Now;
                                    modalplmprop.QuestionRefID = modalque.QuestionId;
                                    modalplmprop.Title = targetproprop.Title;
                                    modalplmprop.AlternativetextonPlacelevel = targetproprop.AlternativetextonPlacelevel;
                                    modalplmprop.AlternativetextonJoblevel = targetproprop.AlternativetextonJoblevel;
                                    modalplmprop.Alternativetextonproductlevel = targetproprop.Alternativetextonproductlevel;
                                    modalplmprop.CopyLayoutRefId = targetproprop.QuestionRefID;

                                    context.ProblemsProperties.Add(modalplmprop);
                                    context.SaveChanges();

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetproprop.ProblemsPropertiesID ?? 0;
                                    langrefdetail.NewRefId = modalplmprop.ProblemsPropertiesID ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_Title;
                                    langrefdetail.LangText = modalplmprop.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalplmprop.CreatedBy;
                                    langrefdetail.UpdatedBy = modalplmprop.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if (modalplmprop.AlternativetextonPlacelevel != null && modalplmprop.AlternativetextonPlacelevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnPlaceLevel;
                                        langrefdetail.LangText = modalplmprop.AlternativetextonPlacelevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modalplmprop.AlternativetextonJoblevel != null && modalplmprop.AlternativetextonJoblevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnJobLevel;
                                        langrefdetail.LangText = modalplmprop.AlternativetextonJoblevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modalplmprop.Alternativetextonproductlevel != null && modalplmprop.Alternativetextonproductlevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnProductLevel;
                                        langrefdetail.LangText = modalplmprop.Alternativetextonproductlevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Problemp.ToString())
                            {
                                var targetplmList = problemsList.Where(x => x.ProblemsRefId == questions.QuestionId).ToList();
                                foreach (var targetplm in targetplmList)
                                {
                                    modalplm = new Problems();

                                    modalplm.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetplm.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalplm.LayoutRefID = NewLayoutId;
                                    modalplm.MaskText = targetplm.MaskText;
                                    modalplm.Format = targetplm.Format;
                                    modalplm.Size = targetplm.Size;
                                    modalplm.RegEx = targetplm.RegEx;
                                    modalplm.PartnerCode = targetplm.PartnerCode;
                                    modalplm.PlaceLevel = targetplm.PlaceLevel;
                                    modalplm.JobLevel = targetplm.JobLevel;
                                    modalplm.ProductLevel = targetplm.ProductLevel;
                                    modalplm.PlanningFeedbackcode = targetplm.PlanningFeedbackcode;
                                    modalplm.CreatedBy = CreatedBy;
                                    modalplm.CreatedOn = DateTime.Now;
                                    modalplm.UpdatedBy = CreatedBy;
                                    modalplm.UpdatedOn = DateTime.Now;
                                    modalplm.QuestionRefID = context.ProblemsProperties.Where(x => x.CopyLayoutRefId == targetplm.QuestionRefID).Select(x => x.QuestionRefID).FirstOrDefault();
                                    modalplm.Title = targetplm.Title;
                                    modalplm.ProblemsRefId = modalque.QuestionId;

                                    context.Problems.Add(modalplm);
                                    context.SaveChanges();

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetplm.ProblemsID ?? 0;
                                    langrefdetail.NewRefId = modalplm.ProblemsID ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.Problems_Title;
                                    langrefdetail.LangText = modalplm.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalplm.CreatedBy;
                                    langrefdetail.UpdatedBy = modalplm.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if (modalplm.MaskText != null && modalplm.MaskText.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.Problems_MaskText;
                                        langrefdetail.LangText = modalplm.MaskText ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Signature.ToString())
                            {
                                var targetsList = signaturesPropertiesList.Where(x => x.QuestionRefID == questions.QuestionId).ToList();
                                foreach (var targetsprop in targetsList)
                                {
                                    modalsprop = new SignaturesProperties();

                                    modalsprop.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetsprop.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalsprop.LayoutRefID = NewLayoutId;
                                    modalsprop.CreatedBy = CreatedBy;
                                    modalsprop.CreatedOn = DateTime.Now;
                                    modalsprop.UpdatedBy = CreatedBy;
                                    modalsprop.UpdatedOn = DateTime.Now;
                                    modalsprop.QuestionRefID = modalque.QuestionId;
                                    modalsprop.Title = targetsprop.Title;
                                    modalsprop.AlternativetextonPlacelevel = targetsprop.AlternativetextonPlacelevel;
                                    modalsprop.AlternativetextonJoblevel = targetsprop.AlternativetextonJoblevel;
                                    modalsprop.Alternativetextonproductlevel = targetsprop.Alternativetextonproductlevel;
                                    modalsprop.CopyLayoutRefId = targetsprop.QuestionRefID;

                                    context.SignaturesProperties.Add(modalsprop);
                                    context.SaveChanges();

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetsprop.SignaturesPropertiesID ?? 0;
                                    langrefdetail.NewRefId = modalsprop.SignaturesPropertiesID ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_Title;
                                    langrefdetail.LangText = modalsprop.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalsprop.CreatedBy;
                                    langrefdetail.UpdatedBy = modalsprop.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if (modalsprop.AlternativetextonPlacelevel != null && modalsprop.AlternativetextonPlacelevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnPlaceLevel;
                                        langrefdetail.LangText = modalsprop.AlternativetextonPlacelevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modalsprop.AlternativetextonJoblevel != null && modalsprop.AlternativetextonJoblevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnJobLevel;
                                        langrefdetail.LangText = modalsprop.AlternativetextonJoblevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modalsprop.Alternativetextonproductlevel != null && modalsprop.Alternativetextonproductlevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnProductLevel;
                                        langrefdetail.LangText = modalsprop.Alternativetextonproductlevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Signaturep.ToString())
                            {
                                var targetsList = signaturesList.Where(x => x.SignaturesRefId == questions.QuestionId).ToList();
                                foreach (var targets in targetsList)
                                {
                                    modals = new Signatures();

                                    modals.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targets.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modals.LayoutRefID = NewLayoutId;
                                    modals.PlaceLevel = targets.PlaceLevel;
                                    modals.JobLevel = targets.JobLevel;
                                    modals.CreatedBy = CreatedBy;
                                    modals.CreatedOn = DateTime.Now;
                                    modals.UpdatedBy = CreatedBy;
                                    modals.UpdatedOn = DateTime.Now;
                                    modals.QuestionRefID = context.SignaturesProperties.Where(x => x.CopyLayoutRefId == targets.QuestionRefID).Select(x => x.QuestionRefID).FirstOrDefault();
                                    modals.Title = targets.Title;
                                    modals.SignaturesRefId = modalque.QuestionId;

                                    context.Signatures.Add(modals);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.DocumentScanner.ToString())
                            {
                                var targetdspList = DocumentScannerPropertiesList.Where(x => x.QuestionRefID == questions.QuestionId).ToList();
                                foreach (var targetdsprop in targetdspList)
                                {
                                    modaldsprop = new DocumentScannerProperties();

                                    modaldsprop.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetdsprop.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modaldsprop.LayoutRefID = NewLayoutId;
                                    modaldsprop.CreatedBy = CreatedBy;
                                    modaldsprop.CreatedOn = DateTime.Now;
                                    modaldsprop.UpdatedBy = CreatedBy;
                                    modaldsprop.UpdatedOn = DateTime.Now;
                                    modaldsprop.QuestionRefID = modalque.QuestionId;
                                    modaldsprop.Title = targetdsprop.Title;
                                    modaldsprop.AlternativetextonPlacelevel = targetdsprop.AlternativetextonPlacelevel;
                                    modaldsprop.AlternativetextonJoblevel = targetdsprop.AlternativetextonJoblevel;
                                    modaldsprop.Alternativetextonproductlevel = targetdsprop.Alternativetextonproductlevel;
                                    modaldsprop.CopyLayoutRefId = targetdsprop.QuestionRefID;

                                    context.DocumentScannerProperties.Add(modaldsprop);
                                    context.SaveChanges();

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetdsprop.DocumentScannerPropertiesID ?? 0;
                                    langrefdetail.NewRefId = modaldsprop.DocumentScannerPropertiesID ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_Title;
                                    langrefdetail.LangText = modaldsprop.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modaldsprop.CreatedBy;
                                    langrefdetail.UpdatedBy = modaldsprop.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if (modaldsprop.AlternativetextonPlacelevel != null && modaldsprop.AlternativetextonPlacelevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnPlaceLevel;
                                        langrefdetail.LangText = modaldsprop.AlternativetextonPlacelevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modaldsprop.AlternativetextonJoblevel != null && modaldsprop.AlternativetextonJoblevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnJobLevel;
                                        langrefdetail.LangText = modaldsprop.AlternativetextonJoblevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modaldsprop.Alternativetextonproductlevel != null && modaldsprop.Alternativetextonproductlevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnProductLevel;
                                        langrefdetail.LangText = modaldsprop.Alternativetextonproductlevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.DocumentScannerp.ToString())
                            {
                                var targetdsList = DocumentScannerList.Where(x => x.DocumentScannerRefId == questions.QuestionId).ToList();
                                foreach (var targetds in targetdsList)
                                {
                                    modalds = new DocumentScanner();

                                    modalds.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetds.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalds.LayoutRefID = NewLayoutId;
                                    modalds.PlaceLevel = targetds.PlaceLevel;
                                    modalds.JobLevel = targetds.JobLevel;
                                    modalds.ProductLevel = targetds.ProductLevel;
                                    modalds.DocumentType = targetds.DocumentType;
                                    modalds.CreatedBy = CreatedBy;
                                    modalds.CreatedOn = DateTime.Now;
                                    modalds.UpdatedBy = CreatedBy;
                                    modalds.UpdatedOn = DateTime.Now;
                                    modalds.QuestionRefID = context.DocumentScannerProperties.Where(x => x.CopyLayoutRefId == targetds.QuestionRefID).Select(x => x.QuestionRefID).FirstOrDefault();
                                    modalds.Title = targetds.Title;
                                    modalds.DocumentScannerRefId = modalque.QuestionId;

                                    context.DocumentScanner.Add(modalds);
                                    context.SaveChanges();

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetds.DocumentScannerID ?? 0;
                                    langrefdetail.NewRefId = modalds.DocumentScannerID ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScanner_Title;
                                    langrefdetail.LangText = modalds.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalds.CreatedBy;
                                    langrefdetail.UpdatedBy = modalds.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Picture.ToString())
                            {
                                var targetsList = pictureList.Where(x => x.QuestionRefID == questions.QuestionId).ToList();
                                foreach (var targets in targetsList)
                                {
                                    modalpcr = new Picure();

                                    modalpcr.ActivityRefID = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == modals.ActivityRefID).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalpcr.LayoutRefID = NewLayoutId;
                                    modalpcr.Title = targets.Title;
                                    modalpcr.AlternativetextonPlacelevel = targets.AlternativetextonPlacelevel;
                                    modalpcr.AlternativetextonJoblevel = targets.AlternativetextonJoblevel;
                                    modalpcr.Alternativetextonproductlevel = targets.Alternativetextonproductlevel;
                                    modalpcr.CreatedBy = CreatedBy;
                                    modalpcr.CreatedOn = DateTime.Now;
                                    modalpcr.UpdatedBy = CreatedBy;
                                    modalpcr.UpdatedOn = DateTime.Now;
                                    modalpcr.QuestionRefID = modalque.QuestionId;

                                    context.Picure.Add(modalpcr);
                                    context.SaveChanges();

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targets.PicureID ?? 0;
                                    langrefdetail.NewRefId = modalpcr.PicureID ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_Title;
                                    langrefdetail.LangText = modalpcr.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalpcr.CreatedBy;
                                    langrefdetail.UpdatedBy = modalpcr.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if (modalpcr.AlternativetextonPlacelevel != null && modalpcr.AlternativetextonPlacelevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnPlaceLevel;
                                        langrefdetail.LangText = modalpcr.AlternativetextonPlacelevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modalpcr.AlternativetextonJoblevel != null && modalpcr.AlternativetextonJoblevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnJobLevel;
                                        langrefdetail.LangText = modalpcr.AlternativetextonJoblevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modalpcr.Alternativetextonproductlevel != null && modalpcr.Alternativetextonproductlevel.Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnProductLevel;
                                        langrefdetail.LangText = modalpcr.Alternativetextonproductlevel ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Conditional_Action.ToString())
                            {
                                var targetcaq = conditionalPropertiesList.FirstOrDefault(x => x.QuestionRefId == questions.QuestionId);
                                if (targetcaq != null)
                                {
                                    modalcaq = new ConditionalProperties();

                                    modalcaq.Name = targetcaq.Name;
                                    modalcaq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetcaq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalcaq.LayoutRefId = NewLayoutId;
                                    modalcaq.CreatedBy = CreatedBy;
                                    modalcaq.CreatedOn = DateTime.Now;
                                    modalcaq.UpdatedBy = CreatedBy;
                                    modalcaq.UpdatedOn = DateTime.Now;
                                    modalcaq.ConditionValue = targetcaq.ConditionValue;
                                    modalcaq.ConCompareMethod = targetcaq.ConCompareMethod;
                                    modalcaq.ConCompareTo = targetcaq.ConCompareTo;
                                    int valueCount = 2;
                                    int?[] conActionSavedValues = new int?[2];
                                    int?[] conActionAliasIds = new int?[2];

                                    conActionSavedValues[0] = targetcaq.ConSavedValued;
                                    conActionSavedValues[1] = targetcaq.comtoSavedValue;

                                    if (conActionSavedValues.Any())
                                    {
                                        conActionAliasIds = GetAliasids(conActionSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Conditional_Action.ToString(), modalcaq.LayoutRefId, modalcaq.ActivitiesRefId);
                                    }
                                    modalcaq.ConSavedValued = conActionAliasIds[0];
                                    modalcaq.comtoSavedValue = conActionAliasIds[1];
                                    modalcaq.ComFixedText = targetcaq.ComFixedText;
                                    modalcaq.QuestionRefId = modalque.QuestionId;
                                    modalcaq.Comment = targetcaq.Comment;
                                    modalcaq.Repeat = targetcaq.Repeat;

                                    context.ConditionalProperties.Add(modalcaq);
                                    context.SaveChanges();
                                    if (conActionAliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(conActionAliasIds, modalcaq.ConditionalID.GetValueOrDefault());
                                    }
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Info_Message.ToString())
                            {
                                var targetimq = infomessagePropertiesList.FirstOrDefault(x => x.QuestionRefId == questions.QuestionId);
                                if (targetimq != null)
                                {
                                    modalimq = new InfomessageProperties();

                                    modalimq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetimq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    string[] strActivityData = new string[2];
                                    strActivityData = GetCountofInfoMessage(modalimq.ActivitiesRefId, "INFO");
                                    modalimq.CreatedBy = CreatedBy;
                                    modalimq.CreatedOn = DateTime.Now;
                                    modalimq.Fontstyle = targetimq.Fontstyle;
                                    modalimq.InfoMessageIconTypeRefId = targetimq.InfoMessageIconTypeRefId;
                                    modalimq.LayoutRefId = NewLayoutId;
                                    modalimq.Messagecontent = targetimq.Messagecontent;
                                    modalimq.Messagetype = targetimq.Messagetype;
                                    modalimq.Name = "Info " + strActivityData[1] + strActivityData[0];
                                    modalimq.QuestionRefId = modalque.QuestionId;
                                    modalimq.Title = targetimq.Title;
                                    modalimq.Titlestyle = targetimq.Titlestyle;
                                    modalimq.UpdatedBy = CreatedBy;
                                    modalimq.UpdatedOn = DateTime.Now;
                                    modalimq.Comment = targetimq.Comment;
                                    modalimq.Conditionitem = targetimq.Conditionitem;
                                    modalimq.ConCompareMethod = targetimq.ConCompareMethod;
                                    modalimq.Convalue = targetimq.Convalue;
                                    int valueCount = 2;
                                    int?[] infoMessageSavedValues = new int?[2];
                                    int?[] infoMessageAliasIds = new int?[2];

                                    infoMessageSavedValues[0] = targetimq.ConItemSavedValued;
                                    infoMessageSavedValues[1] = targetimq.ConvalueSavedValue;

                                    if (infoMessageSavedValues.Any())
                                    {
                                        infoMessageAliasIds = GetAliasids(infoMessageSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Info_Message.ToString(), modalimq.LayoutRefId, modalimq.ActivitiesRefId);
                                    }
                                    modalimq.ConItemSavedValued = infoMessageAliasIds[0];
                                    modalimq.ConvalueSavedValue = infoMessageAliasIds[1];
                                    modalimq.ComFixedText = targetimq.ComFixedText;

                                    context.InfomessageProperties.Add(modalimq);
                                    context.SaveChanges();

                                    if (infoMessageAliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(infoMessageAliasIds, modalimq.InfomessageId.GetValueOrDefault()); ;
                                    }
                                    /* Written By Selvam Thangaraj - Go */


                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetimq.InfomessageId ?? 0;
                                    langrefdetail.NewRefId = modalimq.InfomessageId ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.InfomessageProperties_Title;
                                    langrefdetail.LangText = modalimq.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalimq.CreatedBy;
                                    langrefdetail.UpdatedBy = modalimq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if (modalimq.Messagecontent != null && modalimq.Messagecontent.Trim().Length > 0)
                                    {

                                        langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.InfoMessageContentDetail_MessageContent;
                                        langrefdetail.LangText = modalimq.Messagecontent ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    /* Written By Selvam Thangaraj - Stop */
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Text_Message.ToString())
                            {

                                var targettmq = textMessagePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targettmq != null)
                                {
                                    modaltmq = new TextMessageProperties();

                                    modaltmq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targettmq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    string[] strTextActivityData = new string[2];
                                    strTextActivityData = GetCountofTextMessage(modaltmq.ActivitiesRefId, "TEXT");
                                    modaltmq.CreatedBy = CreatedBy;
                                    modaltmq.CreatedOn = DateTime.Now;
                                    modaltmq.LayoutRefId = NewLayoutId;
                                    modaltmq.Messagecontent = targettmq.Messagecontent;
                                    modaltmq.Name = "SMS " + strTextActivityData[1] + " # " + strTextActivityData[0];
                                    modaltmq.QuestionRefId = modalque.QuestionId;
                                    modaltmq.SendMethod = targettmq.SendMethod;
                                    modaltmq.UpdatedBy = CreatedBy;
                                    modaltmq.UpdatedOn = DateTime.Now;
                                    modaltmq.Comment = targettmq.Comment;
                                    modaltmq.Conditionitem = targettmq.Conditionitem;
                                    modaltmq.ConCompareMethod = targettmq.ConCompareMethod;
                                    modaltmq.Convalue = targettmq.Convalue;
                                    int valueCount = 2;
                                    int?[] textMessageSavedValues = new int?[2];
                                    int?[] textMessageAliasIds = new int?[2];

                                    textMessageSavedValues[0] = targettmq.ConItemSavedValued;
                                    textMessageSavedValues[1] = targettmq.ConvalueSavedValue;

                                    if (textMessageSavedValues.Any())
                                    {
                                        textMessageAliasIds = GetAliasids(textMessageSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Text_Message.ToString(), modaltmq.LayoutRefId, modaltmq.ActivitiesRefId);
                                    }
                                    modaltmq.ConItemSavedValued = textMessageAliasIds[0];
                                    modaltmq.ConvalueSavedValue = textMessageAliasIds[1];
                                    modaltmq.ComFixedText = targettmq.ComFixedText;

                                    context.TextMessageProperties.Add(modaltmq);
                                    context.SaveChanges();

                                    if (textMessageAliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(textMessageAliasIds, modaltmq.TextMessageId.GetValueOrDefault());
                                    }


                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targettmq.TextMessageId ?? 0;
                                    langrefdetail.NewRefId = modaltmq.TextMessageId ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TextMessageProperties_Messagecontent;
                                    langrefdetail.LangText = modaltmq.Messagecontent ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modaltmq.CreatedBy;
                                    langrefdetail.UpdatedBy = modaltmq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);


                                    /* Written By Selvam Thangaraj - Stop */
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Set_Action___Value.ToString())
                            {

                                var targetsaq = setActionValuePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetsaq != null)
                                {
                                    int?[] setActionSavedValues = new int?[5];
                                    modalsaq = new SetActionValueProperties();

                                    modalsaq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetsaq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalsaq.CancelcurrentQP = targetsaq.CancelcurrentQP;
                                    modalsaq.CreatedBy = CreatedBy;
                                    modalsaq.CreatedOn = DateTime.Now;
                                    modalsaq.LayoutRefId = NewLayoutId;
                                    modalsaq.QuestionRefId = modalque.QuestionId;
                                    modalsaq.SetActionValueTypeId = targetsaq.SetActionValueTypeId;

                                    if (modalsaq.SetActionValueTypeId == 1)
                                    {
                                        modalsaq.SelectQP = targetsaq.SelectQP;
                                        modalsaq.CancelcurrentQP = targetsaq.CancelcurrentQP;

                                    }
                                    else if (modalsaq.SetActionValueTypeId == 2)
                                    {
                                        modalsaq.SelectView = targetsaq.SelectView;
                                        modalsaq.PlanningIDOBC = targetsaq.PlanningIDOBC == null ? null : targetsaq.PlanningIDOBC;
                                        modalsaq.Longitude = targetsaq.Longitude == null ? null : targetsaq.Longitude;
                                        modalsaq.Latitude = targetsaq.Latitude == null ? null : targetsaq.Latitude;
                                    }
                                    else if (modalsaq.SetActionValueTypeId == 3)
                                    {
                                        modalsaq.SelectAllowance = targetsaq.SelectAllowance;
                                    }
                                    else if (modalsaq.SetActionValueTypeId == 4)
                                    {
                                        modalsaq.SelectInfoColumn = targetsaq.SelectInfoColumn;
                                        modalsaq.SetReset = targetsaq.SetReset;
                                        modalsaq.InfoCoumnFixedText = targetsaq.InfoCoumnFixedText;
                                        setActionSavedValues[0] = targetsaq.InfoColumnSavedValue;
                                    }
                                    else if (modalsaq.SetActionValueTypeId == 5)
                                    {
                                        modalsaq.Value = targetsaq.Value;
                                        setActionSavedValues[1] = targetsaq.SelectTargetSavedValue;
                                        if (targetsaq.CopySavedValue > 0)
                                            setActionSavedValues[4] = targetsaq.CopySavedValue;
                                        modalsaq.CopyRange = targetsaq.CopyRange;
                                        modalsaq.StartChar = targetsaq.StartChar;
                                        modalsaq.CharLength = targetsaq.CharLength;
                                    }
                                    else if (modalsaq.SetActionValueTypeId == 6)
                                    {
                                        modalsaq.SetState = targetsaq.SetState;
                                        if (targetsaq.SetState != "" && targetsaq.SetState != null)
                                        {
                                            var activity = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == targetsaq.ActivitiesRefId).FirstOrDefault();
                                            if (activity != null)
                                            {
                                                activity.EmptyFullSolo = true;
                                                context.SaveChanges();
                                            }
                                        }
                                    }

                                    modalsaq.Title = targetsaq.Title;
                                    modalsaq.UpdatedBy = CreatedBy;
                                    modalsaq.UpdatedOn = DateTime.Now;
                                    modalsaq.Comment = targetsaq.Comment;
                                    modalsaq.Conditionitem = targetsaq.Conditionitem;
                                    modalsaq.ConCompareMethod = targetsaq.ConCompareMethod;
                                    modalsaq.Convalue = targetsaq.Convalue;
                                    setActionSavedValues[2] = targetsaq.ConItemSavedValued;
                                    setActionSavedValues[3] = targetsaq.ConvalueSavedValue;
                                    modalsaq.ComFixedText = targetsaq.ComFixedText;
                                    modalsaq.SavedValue = targetsaq.SavedValue;
                                    int valueCount = 5;
                                    int?[] aliasIds = new int?[4];
                                    if (setActionSavedValues.Any())
                                    {
                                        aliasIds = GetAliasids(setActionSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Set_Action___Value.ToString(), modalsaq.LayoutRefId, modalsaq.ActivitiesRefId);
                                    }
                                    if (modalsaq.SetActionValueTypeId == 4)
                                    {
                                        modalsaq.InfoColumnSavedValue = aliasIds[0];
                                    }
                                    else if (modalsaq.SetActionValueTypeId == 5)
                                    {
                                        modalsaq.SelectTargetSavedValue = aliasIds[1];
                                        if (targetsaq.CopySavedValue > 0)
                                            modalsaq.CopySavedValue = aliasIds[4];
                                    }
                                    modalsaq.ConItemSavedValued = aliasIds[2];
                                    modalsaq.ConvalueSavedValue = aliasIds[3];

                                    context.SetActionValueProperties.Add(modalsaq);
                                    context.SaveChanges();

                                    if (modalsaq.SelectQP > 0)
                                        setActionValueIds.Add(modalsaq.SetActionValueId ?? 0);

                                    if (aliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(aliasIds, modalsaq.SetActionValueId.GetValueOrDefault());
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetsaq.SetActionValueId ?? 0;
                                    langrefdetail.NewRefId = modalsaq.SetActionValueId ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalsaq.CreatedBy;
                                    langrefdetail.UpdatedBy = modalsaq.CreatedBy;
                                    if (modalsaq.Value != null && modalsaq.Value.Trim().Length > 0)
                                    {
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.SetActionValueProperties_Value;
                                        langrefdetail.LangText = modalsaq.Value ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }
                                    else if (modalsaq.InfoCoumnFixedText != null && modalsaq.InfoCoumnFixedText.Trim().Length > 0)
                                    {
                                        langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.SetActionValueProperties_InfoColumnFixedText;
                                        langrefdetail.LangText = modalsaq.InfoCoumnFixedText ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    /* Written By Selvam Thangaraj - Stop */
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Variable_List.ToString())
                            {
                                var targetvlq = varListPropertiesList.FirstOrDefault(x => x.QuestionRefId == questions.QuestionId);
                                if (targetvlq != null)
                                {
                                    modalvlq = new VarListProperties();

                                    modalvlq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetvlq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalvlq.QuestionRefId = modalque.QuestionId;
                                    modalvlq.LayoutRefId = NewLayoutId;
                                    modalvlq.Name = targetvlq.Title;
                                    modalvlq.Title = targetvlq.Title;
                                    modalvlq.VarListId = targetvlq.VarListId;
                                    modalvlq.Other = targetvlq.Other;
                                    modalvlq.OtherTitle = targetvlq.OtherTitle;
                                    modalvlq.OtherMaskText = targetvlq.OtherMaskText;
                                    modalvlq.SaveDisplay = targetvlq.SaveDisplay;
                                    modalvlq.Comment = targetvlq.Comment;
                                    modalvlq.ConditionItemValue = targetvlq.ConditionItemValue;
                                    modalvlq.CompareMethod = targetvlq.CompareMethod;
                                    modalvlq.CompareTo = targetvlq.CompareTo;
                                    modalvlq.CompareToFixedText = targetvlq.CompareToFixedText;
                                    modalvlq.PlanningFeedback = targetvlq.PlanningFeedback;
                                    modalvlq.FeedbackType = targetvlq.FeedbackType;
                                    modalvlq.PlanningFeedbackCode = targetvlq.PlanningFeedbackCode;
                                    modalvlq.Infocolumn = targetvlq.Infocolumn;
                                    modalvlq.InfoColumnAction = targetvlq.InfoColumnAction;
                                    modalvlq.FileNumber = targetvlq.FileNumber;
                                    modalvlq.PartnerCode = targetvlq.PartnerCode;
                                    modalvlq.RegisterAsTrailer = targetvlq.RegisterAsTrailer;
                                    modalvlq.CreatedBy = CreatedBy;
                                    modalvlq.CreatedOn = DateTime.Now;
                                    modalvlq.UpdatedBy = CreatedBy;
                                    modalvlq.UpdatedOn = DateTime.Now;

                                    int valueCount = 3;
                                    int?[] varListSavedValues = new int?[3];
                                    int?[] varListAliasIds = new int?[3];
                                    varListSavedValues[0] = targetvlq.ConditionSavedValue;
                                    varListSavedValues[1] = targetvlq.CompareToSavedValue;
                                    varListSavedValues[2] = targetvlq.SaveDisplayTo;
                                    if (varListSavedValues.Any())
                                        varListAliasIds = GetAliasids(varListSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Variable_List.ToString(), modalvlq.LayoutRefId, modalvlq.ActivitiesRefId);

                                    modalvlq.ConditionSavedValue = varListAliasIds[0];
                                    modalvlq.CompareToSavedValue = varListAliasIds[1];
                                    modalvlq.SaveDisplayTo = varListAliasIds[2] ?? 0;
                                    modalvlq.CompareToFixedText = targetvlq.CompareToFixedText;

                                    context.VarListProperties.Add(modalvlq);
                                    context.SaveChanges();

                                    if (varListAliasIds.Any())
                                        UpdateAliasPropertyTableId(varListAliasIds, modalvlq.VarListPropertiesId.GetValueOrDefault());

                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetvlq.VarListPropertiesId ?? 0;
                                    langrefdetail.NewRefId = modalvlq.VarListPropertiesId ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.VariableListProperties_Title;
                                    langrefdetail.LangText = modalvlq.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalvlq.CreatedBy;
                                    langrefdetail.UpdatedBy = modalvlq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if (modalvlq.OtherTitle != null && modalvlq.OtherTitle.Trim().Length > 0)
                                    {
                                        langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherTitle;
                                        langrefdetail.LangText = modalvlq.OtherTitle ?? "";
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modalvlq.OtherMaskText != null && modalvlq.OtherMaskText.Trim().Length > 0)
                                    {
                                        langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherMaskText;
                                        langrefdetail.LangText = modalvlq.OtherMaskText ?? "";
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    /* Written By Selvam Thangaraj - Stop */
                                    var readoutlists = varListCommentReadoutDetailList.Where(x => x.VarListPropertiesRefId == targetvlq.VarListPropertiesId).ToList();
                                    int?[] vlReadOutSavedValues = new int?[1];
                                    int?[] vlReadOutAliasIds = new int?[1];

                                    foreach (var readoutDetail in readoutlists)
                                    {
                                        VarListCommentReadoutDetail objReadout = new VarListCommentReadoutDetail();
                                        objReadout.VarListPropertiesRefId = modalvlq.VarListPropertiesId ?? 0;
                                        objReadout.CommentId = readoutDetail.CommentId;
                                        objReadout.ReadoutRange = readoutDetail.ReadoutRange;
                                        objReadout.StartChar = readoutDetail.StartChar;
                                        objReadout.CharLength = readoutDetail.CharLength;
                                        objReadout.IsDefaultAlaisName = readoutDetail.IsDefaultAlaisName;
                                        vlReadOutSavedValues[0] = readoutDetail.SaveTo;
                                        if (vlReadOutSavedValues.Any())
                                            vlReadOutAliasIds = GetAliasids(vlReadOutSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Variable_List.ToString(), modalvlq.LayoutRefId, modalvlq.ActivitiesRefId);
                                        objReadout.SaveTo = vlReadOutAliasIds[0];
                                        objReadout.CreatedBy = modalpsq.CreatedBy;
                                        objReadout.CreatedOn = DateTime.Now;

                                        context.VarListCommentReadoutDetail.Add(objReadout);
                                        context.SaveChanges();
                                        if (vlReadOutAliasIds.Any())
                                            UpdateAliasPropertyTableId(vlReadOutAliasIds, objReadout.VarListCommentReadoutDetailId);
                                    }

                                    var filterlists = varListFilterDetailList.Where(x => x.VarListPropertiesRefId == targetvlq.VarListPropertiesId).ToList();
                                    int?[] vlFilterSavedValues = new int?[1];
                                    int?[] vlFilterAliasIds = new int?[1];

                                    foreach (var readoutDetail in filterlists)
                                    {
                                        VarListFilterDetail objReadout = new VarListFilterDetail();
                                        objReadout.VarListPropertiesRefId = modalvlq.VarListPropertiesId ?? 0;
                                        objReadout.CommentId = readoutDetail.CommentId;
                                        objReadout.FilterMethod = readoutDetail.FilterMethod;
                                        vlFilterSavedValues[0] = readoutDetail.SavedValue;
                                        if (vlFilterSavedValues.Any())
                                            vlFilterAliasIds = GetAliasids(vlFilterSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Variable_List.ToString(), modalvlq.LayoutRefId, modalvlq.ActivitiesRefId);
                                        objReadout.SavedValue = vlFilterAliasIds[0] ?? 0;
                                        objReadout.CreatedBy = modalpsq.CreatedBy;
                                        objReadout.CreatedOn = DateTime.Now;

                                        context.VarListFilterDetail.Add(objReadout);
                                        context.SaveChanges();
                                        if (vlFilterAliasIds.Any())
                                            UpdateAliasPropertyTableId(vlFilterAliasIds, objReadout.VarListFilterDetailId);
                                    }

                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Planning_Select.ToString())
                            {

                                var targetpsq = planningselectPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetpsq != null)
                                {
                                    modalpsq = new PlanningselectProperties();

                                    modalpsq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetpsq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalpsq.Chooseactivity = targetpsq.Chooseactivity;
                                    modalpsq.CreatedBy = CreatedBy;
                                    modalpsq.CreatedOn = DateTime.Now;
                                    modalpsq.Fixedvalue = targetpsq.Fixedvalue;
                                    modalpsq.LayoutRefId = NewLayoutId;
                                    modalpsq.Other = targetpsq.Other;
                                    modalpsq.OtherTitle = targetpsq.OtherTitle;
                                    modalpsq.Planninglevel = targetpsq.Planninglevel;
                                    modalpsq.Planningspecific = targetpsq.Planningspecific;
                                    modalpsq.Planningreadout = targetpsq.Planningreadout;
                                    modalpsq.Planningspecific = targetpsq.Planningspecific;
                                    modalpsq.QP = targetpsq.QP;
                                    modalpsq.QuestionRefId = modalque.QuestionId;
                                    modalpsq.Startselecteditem = targetpsq.Startselecteditem;
                                    modalpsq.Title = targetpsq.Title;
                                    modalpsq.UpdatedBy = CreatedBy;
                                    modalpsq.UpdatedOn = DateTime.Now;
                                    modalpsq.Comment = targetpsq.Comment;
                                    modalpsq.CheckOffPlanning = targetpsq.CheckOffPlanning;
                                    modalpsq.FilterRefId = targetpsq.FilterRefId;
                                    modalpsq.CompareMethodRefId = targetpsq.CompareMethodRefId;
                                    modalpsq.PSComparetoRefId = targetpsq.PSComparetoRefId;

                                    int valueCount = 1;
                                    int?[] psSavedValues = new int?[1];
                                    int?[] psAliasIds = new int?[1];

                                    psSavedValues[0] = targetpsq.InstructionSetSavedValues;

                                    if (psSavedValues.Any())
                                    {
                                        psAliasIds = GetAliasids(psSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Planning_Select.ToString(), modalpsq.LayoutRefId, modalpsq.ActivitiesRefId);
                                    }
                                    modalpsq.OtherMaskText = targetpsq.OtherMaskText;
                                    modalpsq.StartSelection = targetpsq.StartSelection;

                                    context.PlanningselectProperties.Add(modalpsq);
                                    context.SaveChanges();

                                    if (modalpsq.Chooseactivity != null && modalpsq.Chooseactivity.Length > 0)
                                        planningSelectIds.Add(modalpsq.PlanningselectId ?? 0);

                                    if (psAliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(psAliasIds, modalpsq.PlanningselectId.GetValueOrDefault());
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetpsq.PlanningselectId ?? 0;
                                    langrefdetail.NewRefId = modalpsq.PlanningselectId ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_Title;
                                    langrefdetail.LangText = modalpsq.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalpsq.CreatedBy;
                                    langrefdetail.UpdatedBy = modalpsq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if (modalpsq.OtherTitle != null && modalpsq.OtherTitle.Trim().Length > 0)
                                    {
                                        langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_OtherTitle;
                                        langrefdetail.LangText = modalpsq.OtherTitle ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    if (modalpsq.Fixedvalue != null && modalpsq.Fixedvalue.Trim().Length > 0)
                                    {
                                        langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_Fixedvalue;
                                        langrefdetail.LangText = modalpsq.Fixedvalue ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    /* Written By Selvam Thangaraj - Stop */
                                    var readoutlists = planningselectreadoutlist.Where(x => x.PlanningSelectRefId == targetpsq.PlanningselectId).ToList();
                                    int?[] psReadOutSavedValues = new int?[1];
                                    int?[] psReadOutAliasIds = new int?[1];

                                    if (psSavedValues.Any())
                                    {
                                        psAliasIds = GetAliasids(psSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Planning_Select.ToString(), modalpsq.LayoutRefId, modalpsq.ActivitiesRefId);
                                    }

                                    foreach (var readoutDetail in readoutlists)
                                    {
                                        PlanningSelectReadoutDetail objReadout = new PlanningSelectReadoutDetail();
                                        objReadout.PlanningSelectRefId = modalpsq.PlanningselectId ?? 0;
                                        objReadout.ReadoutValueRefId = readoutDetail.ReadoutValueRefId;
                                        objReadout.CommentID = readoutDetail.CommentID;
                                        objReadout.ConfigID = readoutDetail.ConfigID;
                                        objReadout.ReadoutRange = readoutDetail.ReadoutRange;
                                        objReadout.StartChar = readoutDetail.StartChar;
                                        objReadout.CharLength = readoutDetail.CharLength;
                                        objReadout.IsDefaultAlaisName = readoutDetail.IsDefaultAlaisName;
                                        objReadout.ReadoutExample = readoutDetail.ReadoutExample;
                                        psReadOutSavedValues[0] = readoutDetail.SaveTo;
                                        if (psReadOutSavedValues.Any())
                                        {
                                            psReadOutAliasIds = GetAliasids(psReadOutSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Planning_Select.ToString(), modalpsq.LayoutRefId, modalpsq.ActivitiesRefId);
                                        }
                                        objReadout.SaveTo = psReadOutAliasIds[0];
                                        objReadout.CreatedBy = modalpsq.CreatedBy;
                                        objReadout.CreatedOn = DateTime.Now;

                                        context.PlanningSelectReadoutDetail.Add(objReadout);
                                        context.SaveChanges();
                                        if (psReadOutAliasIds.Any())
                                        {
                                            UpdateAliasPropertyTableId(psReadOutAliasIds, objReadout.PlanningSelectReadoutDetailId);
                                        }
                                    }
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Check_Off.ToString())
                            {

                                var targetcoq = checkoffPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetcoq != null)
                                {
                                    modalcoq = new CheckoffProperties();

                                    modalcoq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetcoq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalcoq.CreatedBy = CreatedBy;
                                    modalcoq.CreatedOn = DateTime.Now;
                                    modalcoq.LayoutRefId = NewLayoutId;
                                    modalcoq.Name = targetcoq.Name;
                                    modalcoq.Planninglevel = targetcoq.Planninglevel;
                                    modalcoq.QuestionRefId = modalque.QuestionId;
                                    modalcoq.UpdatedBy = CreatedBy;
                                    modalcoq.UpdatedOn = DateTime.Now;
                                    modalcoq.Comment = targetcoq.Comment;

                                    context.CheckoffProperties.Add(modalcoq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Document_Scan.ToString())
                            {
                                var targetdsq = documetScanPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetdsq != null)
                                {
                                    modaldsq = new DocumetScanProperties();

                                    modaldsq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetdsq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modaldsq.CreatedBy = CreatedBy;
                                    modaldsq.CreatedOn = DateTime.Now;
                                    modaldsq.LayoutRefId = NewLayoutId;
                                    modaldsq.PlanningFeedbackLevel = targetdsq.PlanningFeedbackLevel;
                                    modaldsq.QuestionRefId = modalque.QuestionId;
                                    modaldsq.Title = targetdsq.Title;
                                    modaldsq.UpdatedBy = CreatedBy;
                                    modaldsq.UpdatedOn = DateTime.Now;
                                    modaldsq.DocumentName = targetdsq.DocumentName;
                                    modaldsq.Comment = targetdsq.Comment;
                                    modaldsq.Conditionitem = targetdsq.Conditionitem;
                                    modaldsq.ConCompareMethod = targetdsq.ConCompareMethod;
                                    modaldsq.Convalue = targetdsq.Convalue;
                                    int valueCount = 2;
                                    int?[] docScanSavedValues = new int?[2];
                                    int?[] docScanAliasIds = new int?[2];
                                    docScanSavedValues[0] = targetdsq.ConItemSavedValued;
                                    docScanSavedValues[1] = targetdsq.ConvalueSavedValue;
                                    if (docScanSavedValues.Any())
                                    {
                                        docScanAliasIds = GetAliasids(docScanSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Document_Scan.ToString(), modaldsq.LayoutRefId, modaldsq.ActivitiesRefId);
                                    }
                                    modaldsq.ConItemSavedValued = docScanAliasIds[0];
                                    modaldsq.ConvalueSavedValue = docScanAliasIds[1];
                                    modaldsq.ComFixedText = targetdsq.ComFixedText;

                                    context.DocumetScanProperties.Add(modaldsq);
                                    context.SaveChanges();

                                    if (docScanAliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(docScanAliasIds, modaldsq.DocumetScanId.GetValueOrDefault());
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetdsq.DocumetScanId ?? 0;
                                    langrefdetail.NewRefId = modaldsq.DocumetScanId ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumetScanProperties_Title;
                                    langrefdetail.LangText = modaldsq.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modaldsq.CreatedBy;
                                    langrefdetail.UpdatedBy = modaldsq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Type_Of_Document.ToString())
                            {
                                var targetdstq = documetScanTitleList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetdstq != null)
                                {
                                    modaldstq = new DocumentScanTitle();

                                    modaldstq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetdstq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modaldstq.CreatedBy = CreatedBy;
                                    modaldstq.CreatedOn = DateTime.Now;
                                    modaldstq.LayoutRefId = NewLayoutId;
                                    modaldstq.QuestionRefId = modalque.QuestionId;
                                    modaldstq.Title = targetdstq.Title;
                                    modaldstq.UpdatedBy = CreatedBy;
                                    modaldstq.UpdatedOn = DateTime.Now;
                                    modaldstq.CopyLayoutRefId = targetdstq.DocumetScanTitleId;

                                    context.DocumentScanTitle.Add(modaldstq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Add_Document_Scan.ToString())
                            {
                                var targetdstyq = documetScanTypeList.Where(x => x.QuestionRefID == questions.QuestionId).FirstOrDefault();
                                if (targetdstyq != null)
                                {
                                    var DocumentScanTitleRefId = context.DocumentScanTitle.Where(x => x.CopyLayoutRefId == targetdstyq.DocumentScanTitleRefId).Select(x => x.DocumetScanTitleId).FirstOrDefault();
                                    modaldstyq = new DocumentScanType();
                                    modaldstyq.DocumentScanTitleRefId = DocumentScanTitleRefId;
                                    modaldstyq.CreatedBy = CreatedBy;
                                    modaldstyq.CreatedOn = DateTime.Now;
                                    modaldstyq.QuestionRefID = modalque.QuestionId;
                                    modaldstyq.DocumentTypeTitle = targetdstyq.DocumentTypeTitle;
                                    modaldstyq.DocumentType = targetdstyq.DocumentType;
                                    modaldstyq.UpdatedBy = CreatedBy;
                                    modaldstyq.UpdatedOn = DateTime.Now;

                                    context.DocumentScanType.Add(modaldstyq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Entry.ToString() || PropertyName == EnumToolBoxItems.Entry_From_Variable_List.ToString())
                            {
                                var targeteq = entryPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targeteq != null)
                                {
                                    modaleq = new EntryProperties();

                                    modaleq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targeteq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modaleq.CreatedBy = CreatedBy;
                                    modaleq.CreatedOn = DateTime.Now;
                                    modaleq.EntryType = targeteq.EntryType;
                                    modaleq.ExactCharacter = targeteq.ExactCharacter;
                                    modaleq.FeedbackType = targeteq.FeedbackType;
                                    modaleq.LayoutRefId = NewLayoutId;
                                    modaleq.MaskText = targeteq.MaskText;
                                    modaleq.MinimumofCharacter = targeteq.MinimumofCharacter;
                                    modaleq.MinimumOfCharterValue = targeteq.MinimumOfCharterValue;
                                    modaleq.NotNull = targeteq.NotNull;
                                    modaleq.Planningfeedback = targeteq.Planningfeedback;
                                    modaleq.PlanningfeedbackCode = targeteq.PlanningfeedbackCode;
                                    modaleq.Predefined = targeteq.Predefined;
                                    modaleq.Propose = targeteq.Propose;
                                    modaleq.QuestionRefId = modalque.QuestionId;
                                    modaleq.Regex = targeteq.Regex;
                                    modaleq.Save = targeteq.Save;
                                    modaleq.Size = targeteq.Size;
                                    modaleq.Title = targeteq.Title;
                                    modaleq.UpdatedBy = CreatedBy;
                                    modaleq.UpdatedOn = DateTime.Now;
                                    modaleq.Zeronotallowed = targeteq.Zeronotallowed;
                                    modaleq.Comment = targeteq.Comment;
                                    modaleq.Conditionitem = targeteq.Conditionitem;
                                    modaleq.ConCompareMethod = targeteq.ConCompareMethod;
                                    modaleq.Convalue = targeteq.Convalue;
                                    modaleq.Infocolumn = targeteq.Infocolumn;
                                    modaleq.InfoColumnAction = targeteq.InfoColumnAction;
                                    modaleq.FileNumber = targeteq.FileNumber;
                                    int valueCount = 3;
                                    int?[] entrySavedValues = new int?[3];
                                    int?[] entryAliasIds = new int?[3];
                                    entrySavedValues[0] = targeteq.SaveTo;
                                    entrySavedValues[1] = targeteq.ConItemSavedValued;
                                    entrySavedValues[2] = targeteq.ConvalueSavedValue;
                                    if (entrySavedValues.Any())
                                    {
                                        entryAliasIds = GetAliasids(entrySavedValues, valueCount, CreatedBy, EnumToolBoxItems.Entry.ToString(), modaleq.LayoutRefId, modaleq.ActivitiesRefId);
                                    }
                                    modaleq.SaveTo = entryAliasIds[0];
                                    modaleq.ConItemSavedValued = entryAliasIds[1];
                                    modaleq.ConvalueSavedValue = entryAliasIds[2];
                                    modaleq.PartnerCode = targeteq.PartnerCode;
                                    modaleq.SaveValue = targeteq.SaveValue;
                                    modaleq.DatetimeFormat = targeteq.DatetimeFormat;
                                    modaleq.AutoGenerate = targeteq.AutoGenerate;
                                    modaleq.ComFixedText = targeteq.ComFixedText;
                                    modaleq.CopyLayoutRefId = targeteq.EntryId;
                                    modaleq.ProposeFixedValue = targeteq.ProposeFixedValue;
                                    modaleq.IsDefaultAlaisName = targeteq.IsDefaultAlaisName;
                                    context.EntryProperties.Add(modaleq);
                                    context.SaveChanges();

                                    if (entryAliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(entryAliasIds, modaleq.EntryId.GetValueOrDefault());
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targeteq.EntryId ?? 0;
                                    langrefdetail.NewRefId = modaleq.EntryId ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_Title;
                                    langrefdetail.LangText = modaleq.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modaleq.CreatedBy;
                                    langrefdetail.UpdatedBy = modaleq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_MaskText;
                                    langrefdetail.LangText = modaleq.MaskText ?? "";
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if ((modaleq.ProposeFixedValue ?? "").Length > 0)
                                    {
                                        langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_FixedValue;
                                        langrefdetail.LangText = modaleq.ProposeFixedValue ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }

                                    /* Written By Selvam Thangaraj - Stop */


                                    //var targetand = aliasNameList.Where(x => x.PropertyTableId == targeteq.EntryId && x.PropertyName == "Entry").FirstOrDefault();
                                    //if (targetand != null)
                                    //{
                                    //    modaland = new AliasNameDetails();

                                    //    modaland.LayoutRefId = NewLayoutId;
                                    //    modaland.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetand.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    //    modaland.AliasName = targetand.AliasName;
                                    //    modaland.PropertyName = targetand.PropertyName;
                                    //    modaland.PropertyTableId = modaleq.EntryId ?? 0;
                                    //    modaland.CreatedBy = CreatedBy;
                                    //    modaland.CreatedOn = DateTime.Now;
                                    //    modaland.UpdatedBy = CreatedBy;
                                    //    modaland.UpdatedOn = DateTime.Now;

                                    //    context.AliasNameDetails.Add(modaland);
                                    //    context.SaveChanges();
                                    //}
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Planning_Select_Other_Title.ToString())
                            {
                                var targeteq = entryPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targeteq != null)
                                {
                                    modaleq = new EntryProperties();

                                    modaleq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targeteq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modaleq.CreatedBy = CreatedBy;
                                    modaleq.CreatedOn = DateTime.Now;
                                    modaleq.EntryType = targeteq.EntryType;
                                    modaleq.ExactCharacter = targeteq.ExactCharacter;
                                    modaleq.FeedbackType = targeteq.FeedbackType;
                                    modaleq.LayoutRefId = NewLayoutId;
                                    modaleq.MaskText = targeteq.MaskText;
                                    modaleq.MinimumofCharacter = targeteq.MinimumofCharacter;
                                    modaleq.MinimumOfCharterValue = targeteq.MinimumOfCharterValue;
                                    modaleq.NotNull = targeteq.NotNull;
                                    modaleq.Planningfeedback = targeteq.Planningfeedback;
                                    modaleq.PlanningfeedbackCode = targeteq.PlanningfeedbackCode;
                                    modaleq.Predefined = targeteq.Predefined;
                                    modaleq.Propose = targeteq.Propose;
                                    modaleq.QuestionRefId = modalque.QuestionId;
                                    modaleq.Regex = targeteq.Regex;
                                    modaleq.Save = targeteq.Save;
                                    modaleq.Size = targeteq.Size;
                                    modaleq.Title = targeteq.Title;
                                    modaleq.UpdatedBy = CreatedBy;
                                    modaleq.UpdatedOn = DateTime.Now;
                                    modaleq.Zeronotallowed = targeteq.Zeronotallowed;
                                    modaleq.Comment = targeteq.Comment;
                                    modaleq.Conditionitem = targeteq.Conditionitem;
                                    modaleq.ConCompareMethod = targeteq.ConCompareMethod;
                                    modaleq.Convalue = targeteq.Convalue;
                                    modaleq.Infocolumn = targeteq.Infocolumn;
                                    modaleq.InfoColumnAction = targeteq.InfoColumnAction;
                                    modaleq.FileNumber = targeteq.FileNumber;
                                    int valueCount = 3;
                                    int?[] entrySavedValues = new int?[3];
                                    int?[] entryAliasIds = new int?[3];
                                    entrySavedValues[0] = targeteq.SaveTo;
                                    entrySavedValues[1] = targeteq.ConItemSavedValued;
                                    entrySavedValues[2] = targeteq.ConvalueSavedValue;
                                    if (entrySavedValues.Any())
                                    {
                                        entryAliasIds = GetAliasids(entrySavedValues, valueCount, CreatedBy, EnumToolBoxItems.Entry.ToString(), modaleq.LayoutRefId, modaleq.ActivitiesRefId);
                                    }
                                    modaleq.SaveTo = entryAliasIds[0];
                                    modaleq.ConItemSavedValued = entryAliasIds[1];
                                    modaleq.ConvalueSavedValue = entryAliasIds[2];
                                    modaleq.PartnerCode = targeteq.PartnerCode;
                                    modaleq.SaveValue = targeteq.SaveValue;
                                    modaleq.DatetimeFormat = targeteq.DatetimeFormat;
                                    modaleq.AutoGenerate = targeteq.AutoGenerate;
                                    modaleq.ComFixedText = targeteq.ComFixedText;
                                    modaleq.CopyLayoutRefId = targeteq.EntryId;

                                    context.EntryProperties.Add(modaleq);
                                    context.SaveChanges();

                                    if (entryAliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(entryAliasIds, modaleq.EntryId.GetValueOrDefault());
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targeteq.EntryId ?? 0;
                                    langrefdetail.NewRefId = modaleq.EntryId ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_Title;
                                    langrefdetail.LangText = modaleq.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modaleq.CreatedBy;
                                    langrefdetail.UpdatedBy = modaleq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_MaskText;
                                    langrefdetail.LangText = modaleq.MaskText ?? "";
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    if ((modaleq.ProposeFixedValue ?? "").Length > 0)
                                    {
                                        langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                        langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_FixedValue;
                                        langrefdetail.LangText = modaleq.ProposeFixedValue ?? "";
                                        langrefdetail.Language = languageName;
                                        _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                    }
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Choice.ToString())
                            {
                                var targetcq = chiocePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetcq != null)
                                {
                                    modalcq = new ChioceProperties();

                                    modalcq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetcq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalcq.CreatedBy = CreatedBy;
                                    modalcq.CreatedOn = DateTime.Now;
                                    modalcq.FeedbackType = targetcq.FeedbackType;
                                    modalcq.LayoutRefId = NewLayoutId;
                                    modalcq.Planningfeedback = targetcq.Planningfeedback;
                                    modalcq.Propose = targetcq.Propose;
                                    modalcq.QuestionRefId = modalque.QuestionId;
                                    modalcq.Recursiveloop = targetcq.Recursiveloop;
                                    modalcq.Save = targetcq.Save;
                                    modalcq.Title = targetcq.Title;
                                    modalcq.UpdatedBy = CreatedBy;
                                    modalcq.UpdatedOn = DateTime.Now;
                                    modalcq.ConditionValue = targetcq.ConditionValue;
                                    modalcq.ConCompareMethod = targetcq.ConCompareMethod;
                                    modalcq.ConCompareTo = targetcq.ConCompareTo;
                                    modalcq.Infocolumn = targetcq.Infocolumn;
                                    modalcq.InfoColumnAction = targetcq.InfoColumnAction;
                                    modalcq.FileNumber = targetcq.FileNumber;
                                    int valueCount = 3;
                                    int?[] choiceSavedValues = new int?[3];
                                    choiceSavedValues[0] = targetcq.Saveto;
                                    choiceSavedValues[1] = targetcq.ConSavedValued;
                                    choiceSavedValues[2] = targetcq.comtoSavedValue;
                                    int?[] choiceAliasIds = new int?[3];
                                    if (choiceSavedValues.Any())
                                    {
                                        choiceAliasIds = GetAliasids(choiceSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Choice.ToString(), modalcq.LayoutRefId, modalcq.ActivitiesRefId);
                                    }
                                    modalcq.Saveto = choiceAliasIds[0];
                                    modalcq.ConSavedValued = choiceAliasIds[1];
                                    modalcq.comtoSavedValue = choiceAliasIds[2];
                                    modalcq.ComFixedText = targetcq.ComFixedText;
                                    modalcq.SaveValue = targetcq.SaveValue;
                                    modalcq.CopyLayoutRefId = targetcq.ChioceId;

                                    modalcq.PartnerCode = targetcq.PartnerCode;
                                    modalcq.Comment = targetcq.Comment;
                                    modalcq.AutoGenerate = targetcq.AutoGenerate;
                                    modalcq.Planningfeedbackcode = targetcq.Planningfeedbackcode;
                                    modalcq.IsDefaultAlaisName = targetcq.IsDefaultAlaisName;

                                    context.ChioceProperties.Add(modalcq);
                                    context.SaveChanges();
                                    if (choiceAliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(choiceAliasIds, modalcq.ChioceId.GetValueOrDefault());
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetcq.ChioceId ?? 0;
                                    langrefdetail.NewRefId = modalcq.ChioceId ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ChioceProperties_Title;
                                    langrefdetail.LangText = modalcq.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modalcq.CreatedBy;
                                    langrefdetail.UpdatedBy = modalcq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Add_Chioce.ToString())
                            {
                                var targetacq = addChiocePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();

                                if (targetacq != null)
                                {
                                    var choiceid = context.ChioceProperties.Where(x => x.CopyLayoutRefId == targetacq.ChioceRefId).Select(x => x.ChioceId).FirstOrDefault();

                                    modalacq = new AddChioceProperties();

                                    modalacq.ChioceRefId = choiceid ?? 0;
                                    modalacq.CreatedBy = CreatedBy;
                                    modalacq.CreatedOn = DateTime.Now;
                                    modalacq.Endrecursiveloop = targetacq.Endrecursiveloop;
                                    modalacq.PlanningfeedbackCode = targetacq.PlanningfeedbackCode;
                                    modalacq.Title = targetacq.Title;
                                    modalacq.UpdatedBy = CreatedBy;
                                    modalacq.UpdatedOn = DateTime.Now;
                                    modalacq.QuestionRefId = modalque.QuestionId;
                                    modalacq.Infocolumn = targetacq.Infocolumn;
                                    //modalacq.InfoColumnAction = targetacq.InfoColumnAction;

                                    modalacq.Infocolumn = null;
                                    modalacq.InfoColumnAction = null;
                                    modalacq.FileNumber = targetacq.FileNumber;
                                    modalacq.PartCode = targetacq.PartCode;
                                    modalacq.Dynamicp = targetacq.Dynamicp;
                                    if (modalacq.Dynamicp != "Not Set")
                                    {
                                        var choiceActivityRefId = context.ChioceProperties.Where(x => x.ChioceId == modalacq.ChioceRefId).Select(x => x.ActivitiesRefId).FirstOrDefault();
                                        var activity = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == choiceActivityRefId).FirstOrDefault();
                                        if (activity != null)
                                        {
                                            activity.EmptyFullSolo = true;
                                            context.SaveChanges();
                                        }
                                    }

                                    context.AddChioceProperties.Add(modalacq);
                                    context.SaveChanges();

                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetacq.AddChioceId ?? 0;
                                    langrefdetail.NewRefId = modalacq.AddChioceId ?? 0;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.AddChioceProperties_Title;
                                    langrefdetail.LangText = modalacq.Title ?? "";
                                    langrefdetail.CreatedBy = modalacq.CreatedBy;
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.UpdatedBy = modalacq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Transport_Type.ToString())
                            {
                                var targetttq = transportTypePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetttq != null)
                                {
                                    modalttq = new TransportTypeProperties();
                                    modalttq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetttq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalttq.Title = targetttq.Title;
                                    modalttq.LayoutRefId = NewLayoutId;
                                    modalttq.QuestionRefId = modalque.QuestionId;
                                    modalttq.Save = targetttq.Save;
                                    modalttq.Comment = targetttq.Comment;
                                    modalttq.PlanningFeedback = targetttq.PlanningFeedback ?? "";
                                    modalttq.FeedbackType = targetttq.FeedbackType ?? "";
                                    modalttq.PlanningFeedbackCode = targetttq.PlanningFeedbackCode;
                                    modalttq.ConditionValue = targetttq.ConditionValue;
                                    modalttq.CompareMethod = targetttq.CompareMethod;
                                    modalttq.CompareTo = targetttq.CompareTo;
                                    modalttq.InfoColumn = targetttq.InfoColumn;
                                    modalttq.InfoColumnAction = targetttq.InfoColumnAction;
                                    int valueCount = 3;
                                    int?[] transportSavedValues = new int?[3];
                                    int?[] transportAliasIds = new int?[3];
                                    transportSavedValues[0] = targetttq.SaveTo;
                                    transportSavedValues[1] = targetttq.ConditionSavedValue;
                                    transportSavedValues[2] = targetttq.CompareSavedValue;
                                    if (transportSavedValues.Any())
                                    {
                                        transportAliasIds = GetAliasids(transportSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Transport_Type.ToString(), modalttq.LayoutRefId, modalttq.ActivitiesRefId);
                                    }
                                    modalttq.SaveTo = transportAliasIds[0].GetValueOrDefault();
                                    modalttq.ConditionSavedValue = transportAliasIds[1];
                                    modalttq.CompareSavedValue = transportAliasIds[2];
                                    modalttq.CompareFixedText = targetttq.CompareFixedText;
                                    modalttq.Action = targetttq.Action;
                                    modalttq.CreatedBy = CreatedBy;
                                    modalttq.CreatedOn = DateTime.Now;
                                    modalttq.PartnerCode = targetttq.PartnerCode;
                                    modalttq.DashboardTransportTypeId = 0;
                                    modalttq.CopyLayoutRefId = targetttq.TransportTypeId;
                                    modalttq.AddDefault = true;

                                    context.TransportTypeProperties.Add(modalttq);
                                    context.SaveChanges();

                                    if (transportAliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(transportAliasIds, modalttq.TransportTypeId);
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = modalttq.TransportTypeId;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.NewRefId = modalattdq.TransportTypeDetailId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TransportTypeProperties_Title;
                                    langrefdetail.LangText = modalacq.Title ?? "";
                                    langrefdetail.CreatedBy = modalacq.CreatedBy;
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.UpdatedBy = modalacq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Transport_Type_Detail.ToString())
                            {
                                var targetattq = transportTypePropertiesDetailList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();

                                if (targetattq != null)
                                {
                                    var transportTypeId = context.TransportTypeProperties.Where(x => x.CopyLayoutRefId == targetattq.TransportTypeRefId).Select(x => x.TransportTypeId).FirstOrDefault();

                                    modalattdq = new TransportTypePropertiesDetail();

                                    modalattdq.TransportTypeRefId = transportTypeId;
                                    modalattdq.CreatedBy = CreatedBy;
                                    modalattdq.CreatedOn = DateTime.Now;
                                    modalattdq.PlanningFeedbackCode = targetattq.PlanningFeedbackCode;
                                    modalattdq.Title = targetattq.Title;
                                    modalattdq.UpdatedBy = CreatedBy;
                                    modalattdq.UpdatedOn = DateTime.Now;
                                    modalattdq.QuestionRefId = modalque.QuestionId;
                                    modalattdq.InfoColumn = targetattq.InfoColumn;
                                    //modalattdq.InfoColumnAction = targetattq.InfoColumnAction;
                                    modalattdq.PartnerCode = targetattq.PartnerCode;

                                    context.TransportTypePropertiesDetail.Add(modalattdq);
                                    context.SaveChanges();

                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetattq.TransportTypeDetailId;
                                    langrefdetail.NewRefId = modalattdq.TransportTypeDetailId;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TransportTypePropertiesDetail_Title;
                                    langrefdetail.LangText = modalattdq.Title ?? "";
                                    langrefdetail.CreatedBy = modalattdq.CreatedBy;
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.UpdatedBy = modalattdq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Trailer.ToString() || PropertyName == EnumToolBoxItems.Trailer_From_Variable_List.ToString())
                            {
                                var targettq = trailerPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targettq != null)
                                {
                                    modaltq = new TrailerProperties();

                                    modaltq.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targettq.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modaltq.CreatedBy = CreatedBy;
                                    modaltq.CreatedOn = DateTime.Now;
                                    modaltq.UpdatedBy = CreatedBy;
                                    modaltq.UpdatedOn = DateTime.Now;
                                    modaltq.TrailerFormat = targettq.TrailerFormat;
                                    modaltq.ExactOfCharacter = targettq.ExactOfCharacter;
                                    modaltq.FeedbackType = targettq.FeedbackType;
                                    modaltq.LayoutRefId = NewLayoutId;
                                    modaltq.MaskText = targettq.MaskText;
                                    modaltq.MinimumofCharacter = targettq.MinimumofCharacter;
                                    modaltq.MinimumofCharacterValue = targettq.MinimumofCharacterValue;
                                    modaltq.NullNotAllowed = targettq.NullNotAllowed;
                                    modaltq.PlanningFeedback = targettq.PlanningFeedback;
                                    modaltq.PlanningFeedbackCode = targettq.PlanningFeedbackCode;
                                    modaltq.ActionType = targettq.ActionType;
                                    modaltq.Propose = targettq.Propose;
                                    modaltq.QuestionRefId = modalque.QuestionId;
                                    modaltq.Regex = targettq.Regex;
                                    modaltq.Save = targettq.Save;
                                    int valueCount = 1;
                                    int?[] trailerSavedValues = new int?[1];
                                    int?[] trailerAliasIds = new int?[1];
                                    trailerSavedValues[0] = targettq.SaveTo;
                                    if (trailerSavedValues.Any())
                                    {
                                        trailerAliasIds = GetAliasids(trailerSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Entry.ToString(), modaleq.LayoutRefId, modaleq.ActivitiesRefId);
                                    }
                                    modaltq.SaveTo = trailerAliasIds[0];
                                    modaltq.Size = targettq.Size;
                                    modaltq.Title = targettq.Title;
                                    modaltq.UpdatedBy = CreatedBy;
                                    modaltq.UpdatedOn = DateTime.Now;
                                    modaltq.ZeroNotAllowed = targettq.ZeroNotAllowed;
                                    modaltq.Comment = targettq.Comment;
                                    modaltq.PartnerCode = targettq.PartnerCode;
                                    modaltq.Other = targettq.Other;
                                    modaltq.Name = targettq.Name;
                                    modaltq.AutoGenerate = targettq.AutoGenerate;
                                    modaltq.CopyLayoutRefId = targettq.TrailerId;


                                    context.TrailerProperties.Add(modaltq);
                                    context.SaveChanges();

                                    if (trailerAliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(trailerAliasIds, modaltq.TrailerId);
                                    }


                                    /* Written By Selvam Thangaraj - Go */

                                    langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targettq.TrailerId;
                                    langrefdetail.QuestionRefId = modalque.QuestionId;
                                    langrefdetail.NewRefId = modaltq.TrailerId;
                                    langrefdetail.LayoutRefId = LayoutId;
                                    langrefdetail.NewLatoutRefId = NewLayoutId;
                                    langrefdetail.CustomerRefId = CustomerRefId;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TrailerProperties_Title;
                                    langrefdetail.LangText = modaltq.Title ?? "";
                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.CreatedBy = modaltq.CreatedBy;
                                    langrefdetail.UpdatedBy = modaltq.CreatedBy;
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    langrefdetail.IsModification = (Type == "LC" ? 1 : 0);
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TrailerProperties_MaskText;
                                    langrefdetail.LangText = modaltq.MaskText ?? "";
                                    langrefdetail.Language = languageName;
                                    _translatorRepository.SaveCopyLayoutLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Get_System_Values.ToString())
                            {
                                var targetgsv = getSystemValuesPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetgsv != null)
                                {
                                    modalgsv = new GetSystemValuesProperties();

                                    modalgsv.ActivitiesRefId = NewActivitiesRegistrationId > 0 ? NewActivitiesRegistrationId : actRegList.Where(x => x.CopyLayoutRefId == targetgsv.ActivitiesRefId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                    modalgsv.CreatedBy = CreatedBy;
                                    modalgsv.CreatedOn = DateTime.Now;
                                    modalgsv.LayoutRefId = NewLayoutId;
                                    modalgsv.QuestionRefId = modalque.QuestionId;
                                    modalgsv.Name = targetgsv.Name;
                                    modalgsv.GetSystemValuesRefId = targetgsv.GetSystemValuesRefId;
                                    modalgsv.GetSystemValuesFormatRefId = targetgsv.GetSystemValuesFormatRefId;
                                    int valueCount = 1;
                                    int?[] systemValSavedValues = new int?[1];
                                    int?[] systemValAliasIds = new int?[1];
                                    systemValSavedValues[0] = targetgsv.SaveTo;
                                    if (systemValSavedValues.Any())
                                    {
                                        systemValAliasIds = GetAliasids(systemValSavedValues, valueCount, CreatedBy, EnumToolBoxItems.Get_System_Values.ToString(), modalgsv.LayoutRefId, modalgsv.ActivitiesRefId);
                                    }
                                    modalgsv.SaveTo = systemValAliasIds[0].GetValueOrDefault();
                                    modalgsv.UpdatedBy = CreatedBy;
                                    modalgsv.UpdatedOn = DateTime.Now;
                                    modalgsv.Comment = targetgsv.Comment;

                                    context.GetSystemValuesProperties.Add(modalgsv);
                                    context.SaveChanges();
                                    if (systemValAliasIds.Any())
                                    {
                                        UpdateAliasPropertyTableId(systemValAliasIds, modalgsv.GetSystemValuesId);
                                    }
                                }
                            }
                        }
                        context.SaveChanges();
                        foreach (var questions in context.Questions.Where(x => (Type == "MC" ? QuestionIds.Contains(x.CopyLayoutRefId ?? 0) : (1 == 1)) && x.LayoutRefId == NewLayoutId && (Type == "TC" ? (x.ActivitiesRefId == NewActivitiesRegistrationId) : (1 == 1))))
                        {
                            var parentid = context.Questions.Where(x => x.CopyLayoutRefId == questions.ParentId && x.LayoutRefId == NewLayoutId).Select(x => x.QuestionId).FirstOrDefault();
                            if (parentid > 0)
                                questions.ParentId = parentid;
                            else if (Type == "MC")
                                questions.ParentId = NewQuestionId;

                            context.SaveChanges();
                        }

                        var removeCopyLayoutRefIds = context.Questions.Where(x => x.UpdatedBy == CreatedBy && (Type == "MC" ? QuestionIds.Contains(x.CopyLayoutRefId ?? 0) : (1 == 1)) && x.LayoutRefId == NewLayoutId && (Type == "TC" ? (x.ActivitiesRefId == NewActivitiesRegistrationId) : (1 == 1))).ToList();
                        foreach (var questions in removeCopyLayoutRefIds)
                        {
                            questions.CopyLayoutRefId = null;
                            context.SaveChanges();
                        }

                        if (removeCopyLayoutRefIds.Select(x => x.QuestionId).Distinct().Any())
                        {
                            var questionids = removeCopyLayoutRefIds.Select(x => x.QuestionId).Distinct().ToList().Select(x => x.ToString()).Aggregate((a, b) => a + "," + b);
                            context.Database.ExecuteSqlCommand("Update ExtraInfoProperties set CopyLayoutRefId = null where CopyLayoutRefId is not null and QuestionRefID in (" + questionids + ")");
                            context.Database.ExecuteSqlCommand("Update PalletsProperties set CopyLayoutRefId = null where CopyLayoutRefId is not null and QuestionRefID in (" + questionids + ")");
                            context.Database.ExecuteSqlCommand("Update ProblemsProperties set CopyLayoutRefId = null where CopyLayoutRefId is not null and QuestionRefID in (" + questionids + ")");
                            context.Database.ExecuteSqlCommand("Update SignaturesProperties set CopyLayoutRefId = null where CopyLayoutRefId is not null and QuestionRefID in (" + questionids + ")");
                            context.Database.ExecuteSqlCommand("Update DocumentScannerProperties set CopyLayoutRefId = null where CopyLayoutRefId is not null and QuestionRefID in (" + questionids + ")");
                            context.Database.ExecuteSqlCommand("Update DocumentScanTitle set CopyLayoutRefId = null where CopyLayoutRefId is not null and QuestionRefId in (" + questionids + ")");
                            context.Database.ExecuteSqlCommand("Update EntryProperties set CopyLayoutRefId = null where CopyLayoutRefId is not null and QuestionRefId in (" + questionids + ")");
                            context.Database.ExecuteSqlCommand("Update ChioceProperties set CopyLayoutRefId = null where CopyLayoutRefId is not null and QuestionRefId in (" + questionids + ")");
                            context.Database.ExecuteSqlCommand("Update TrailerProperties set CopyLayoutRefId = null where CopyLayoutRefId is not null and QuestionRefId in (" + questionids + ")");
                            context.Database.ExecuteSqlCommand("Update TransportTypeProperties set CopyLayoutRefId = null where CopyLayoutRefId is not null and QuestionRefId in (" + questionids + ")");
                            context.SaveChanges();
                        }


                        if (planningSelectIds.Any())
                        {
                            foreach (var psId in planningSelectIds)
                            {
                                var psmodal = context.PlanningselectProperties.FirstOrDefault(x => x.PlanningselectId == psId);
                                var activityIds = psmodal.Chooseactivity.Split('-').Where(x => x.Length > 0).Select(x => int.Parse(x)).ToList();
                                var activityId = context.ActivitiesRegistration.Where(x => activityIds.Contains(x.CopyLayoutRefId ?? 0) && x.LayoutRefId == NewLayoutId).Select(x => x.ActivitiesRegistrationId.ToString()).AsEnumerable().Aggregate((a, b) => a + "-" + b);
                                psmodal.Chooseactivity = activityId;
                                context.SaveChanges();
                            }
                        }
                        if (setActionValueIds.Any())
                        {
                            foreach (var savId in setActionValueIds)
                            {
                                var savmodal = context.SetActionValueProperties.FirstOrDefault(x => x.SetActionValueId == savId);
                                var activityId = context.ActivitiesRegistration.Where(x => x.CopyLayoutRefId == savmodal.SelectQP && x.LayoutRefId == NewLayoutId).Select(x => x.ActivitiesRegistrationId).FirstOrDefault();
                                if (activityId > 0)
                                {
                                    savmodal.SelectQP = activityId;
                                    context.SaveChanges();
                                }
                            }
                        }

                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public int?[] GetAliasids(int?[] savedValues, int valueCount, string createdBy, string propertyName, int layoutRefId, int activitiesRefId)
        {
            int?[] aliasNameId = new int?[valueCount];
            try
            {

                using (var context = new Creatis_Context())
                {
                    for (int i = 0; i < savedValues.Length; i++)
                    {
                        if (savedValues[i] != 0 && savedValues[i] != null)
                        {
                            var aliasNameDetails = context.AliasNameDetails.Where(x => x.AliasNameDetailsId == savedValues[i]).FirstOrDefault();
                            if (aliasNameDetails != null)
                            {
                                AliasNameDetails AND = new AliasNameDetails();
                                AND.LayoutRefId = layoutRefId;
                                AND.ActivitiesRefId = activitiesRefId;
                                AND.AliasName = aliasNameDetails.AliasName;
                                AND.PropertyName = propertyName;
                                AND.CreatedBy = createdBy;
                                AND.CreatedOn = DateTime.Now;
                                AND.IsTempData = false;
                                var aliases = _questionPathRepository.SaveAliasNameDetails(AND);
                                aliasNameId[i] = aliases.AliasNameDetailsId;
                            }
                        }
                    }
                }
                return aliasNameId;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateAliasPropertyTableId(int?[] aliasNameDetailsIds, int propertyTableId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    for (int i = 0; i < aliasNameDetailsIds.Length; i++)
                    {
                        if (aliasNameDetailsIds[i] != 0)
                        {
                            var result = context.AliasNameDetails.Where(x => x.AliasNameDetailsId == aliasNameDetailsIds[i]).FirstOrDefault();
                            if (result != null)
                            {
                                result.PropertyTableId = propertyTableId;
                                context.SaveChanges();
                            }
                        }
                    }
                }
                return "Updated";
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string[] GetCountofInfoMessage(int activityRegistrationID, string code)
        {

            try
            {
                using (var context = new Creatis_Context())
                {
                    string[] strValues = new string[2];
                    if (code == "QP")
                        strValues[0] = (context.Questions.Where(x => x.ActivitiesRefId == activityRegistrationID && x.PropertyName == "Info Message").Count() + 1).ToString();
                    else
                        strValues[0] = (context.InfomessageProperties.Where(x => x.ActivitiesRefId == activityRegistrationID).Count() + 1).ToString();
                    strValues[1] = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == activityRegistrationID).Select(x => x.ActivitiesRegistrationName).FirstOrDefault();
                    return strValues;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string[] GetCountofTextMessage(int activityRegistrationID, string code)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    string[] strValues = new string[2];
                    if (code == "QP")
                        strValues[0] = (context.Questions.Where(x => x.ActivitiesRefId == activityRegistrationID && x.PropertyName == "Text Message").Count() + 1).ToString();
                    else
                        strValues[0] = (context.TextMessageProperties.Where(x => x.ActivitiesRefId == activityRegistrationID).Count() + 1).ToString();
                    strValues[1] = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == activityRegistrationID).Select(x => x.ActivitiesRegistrationName).FirstOrDefault();

                    return strValues;
                }
            }
            catch (Exception ex)
            {
                return new string[0];
            }
        }

        public string UpdateLayoutNotes(LayoutNotes layoutNotes)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (layoutNotes.LayoutStatusRefId == 0)
                    {
                        var layoutStatusRefId = context.Layout.Where(x => x.LayoutId == layoutNotes.LayoutRefId).Select(x => x.LayoutStatusRefId).FirstOrDefault();
                        layoutNotes.LayoutStatusRefId = layoutStatusRefId;
                    }

                    context.Add(layoutNotes);
                    context.SaveChanges();
                    return layoutNotes.Notes;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.Infrastructure.Repository;
using CREATIS.Infrastructure.IRepository;
using CREATIS.Infrastructure;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CREATIS.Infrastructure.Repository
{
    public class QuestionPathRepository : Utility, IQuestionPathRepository
    {
        public readonly ITranslatorRepository _translatorRepository;
        public QuestionPathRepository(ITranslatorRepository translatorRepository)
        {
            this._translatorRepository = translatorRepository;
        }

        public List<ToolBoxItems> GetToolbox(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var transportTypeCount = context.TransportTypes.Where(x => x.LayoutRefId == layoutId && x.IsActive).Count();
                    var oBCType = context.OBCType.Where(x => x.LayoutRefId == layoutId).Select(x => x.OBCTypeRefId).FirstOrDefault();

                    var isSmart = (oBCType == (int)EnumOBCType.TX_SMART ? 1 : 0);
                    var isFlex = (oBCType == (int)EnumOBCType.TX_GO_FLEX ? 1 : 0);
                    var isLoadTransportType = (transportTypeCount < 2 ? "Transport Type" : "All");

                    SqlParameter isSmartParam = new SqlParameter("@IsSmart", isSmart);
                    SqlParameter isFlexParam = new SqlParameter("@IsFlex", isFlex);
                    SqlParameter isLoadTransportTypeParam = new SqlParameter("@IsLoadTransportType", isLoadTransportType);
                    var toolboxItemsList = context.ToolBoxItems.FromSql("GetToolBoxItems @IsSmart,@IsFlex,@IsLoadTransportType", isSmartParam, isFlexParam, isLoadTransportTypeParam).ToList();
                    var Toolbox = toolboxItemsList.Where(x => x.SubToolboxId == 0).ToList();

                    if (oBCType == (int)EnumOBCType.TX_SMART)
                    {
                        foreach (var toolbx in Toolbox)
                        {
                            if (toolbx.ToolboxName == "Planning")
                            {
                                toolboxItemsList.Remove(toolbx);
                                break;
                            }
                        }
                    }

                    if (isLoadTransportType == "Transport Type" && oBCType == (int)EnumOBCType.TX_GO_FLEX)
                    {
                        foreach (var toolbx in Toolbox)
                        {
                            if (toolbx.ToolboxName == "Specific")
                            {
                                toolboxItemsList.Remove(toolbx);
                                break;
                            }
                        }
                    }

                    return toolboxItemsList.OrderBy(x => x.Position).ThenBy(x => x.SubPosition).ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<MainToolBox> GetToolBoxParentNodes()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<MainToolBox> RootNodeList;
                    return RootNodeList = context.MainToolBox.Where(x => x.ToolboxStatus == 1).OrderBy(x => x.Position).ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ToolBoxSubItem> GetToolBoxChildNodes()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<ToolBoxSubItem> ChildNodeList;
                    return ChildNodeList = context.ToolBoxSubItem.Where(x => x.SubToolboxStatus == 1).OrderBy(x => x.Position).ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<Questions> Questions(int layoutId, int activityId, int transportId, int questionId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    //var IsCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault(); // (IsCopyLayout ? (1 == 1) : x.ISModification != (int)EnumCopyColorCode.Red)
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", layoutId);
                    SqlParameter activityParam = new SqlParameter("@ActivityRefId", activityId);
                    SqlParameter transportTypeParam = new SqlParameter("@TransporttypeRefId", transportId);
                    SqlParameter questionParam = new SqlParameter("@questionId", questionId);

                    var questionTranslationDetails = context.QuestionsTranslation.FromSql("GetQuestions @LayoutRefId,@ActivityRefId,@TransporttypeRefId,@questionId", layoutParam, activityParam, transportTypeParam, questionParam).Where(x => x.IsQuestionPath == true && x.ISModification != (int)EnumCopyColorCode.Red).ToList();
                    List<Questions> questionsList = new List<Questions>();

                    foreach (var questioninfo in questionTranslationDetails)
                    {
                        Questions questions = new Questions();

                        questions.QuestionId = questioninfo.QuestionId;
                        questions.LayoutRefId = questioninfo.LayoutRefId;
                        questions.ActivitiesRefId = questioninfo.ActivitiesRefId;
                        questions.TransporttypeRefId = questioninfo.TransporttypeRefId;
                        questions.ParentId = questioninfo.ParentId;
                        questions.PropertyName = questioninfo.PropertyName;
                        questions.IsQuestionPath = questioninfo.IsQuestionPath;
                        questions.IsFlex = questioninfo.IsFlex;
                        questions.IsSmart = questioninfo.IsSmart;
                        questions.Position = questioninfo.Position;
                        questions.IsParent = questioninfo.IsParent;
                        questions.IsDefault = questioninfo.IsDefault;
                        questions.IsChild = questioninfo.IsChild;
                        questions.Expanded = questioninfo.Expanded;
                        questions.Name = GetQuestionsName(questioninfo, defualtLanguage);
                        questionsList.Add(questions);
                    }

                    return questionsList.OrderBy(x => x.Position).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<Questions>();
            }
        }
        public string GetQuestionsName(QuestionsTranslation questionsTranslation, string defaultLanguage)
        {
            var questionName = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Belarusian;
            }
            else
            {
                questionName = "";
            }

            if (questionName == null || questionName == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Belarusian;
                    }
                    else
                    {
                        questionName = "@" + questionsTranslation.English;
                    }
                }
            }
            return questionName;
        }
        public List<Questions> FlexQuestions(int layoutId, int activityId, int transportId, int questionId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    //var IsCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault(); //(IsCopyLayout ? (1 == 1) : x.ISModification != (int)EnumCopyColorCode.Red)
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", layoutId);
                    SqlParameter activityParam = new SqlParameter("@ActivityRefId", activityId);
                    SqlParameter transportTypeParam = new SqlParameter("@TransporttypeRefId", transportId);
                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionId);

                    var questionTranslationDetails = context.QuestionsTranslation.FromSql("GetQuestions @LayoutRefId,@ActivityRefId,@TransporttypeRefId,@QuestionRefId", layoutParam, activityParam, transportTypeParam, questionParam).Where(x => x.IsFlex == true && x.ISModification != (int)EnumCopyColorCode.Red).ToList();
                    List<Questions> questionsList = new List<Questions>();

                    foreach (var questioninfo in questionTranslationDetails)
                    {
                        Questions questions = new Questions();

                        questions.QuestionId = questioninfo.QuestionId;
                        questions.LayoutRefId = questioninfo.LayoutRefId;
                        questions.ActivitiesRefId = questioninfo.ActivitiesRefId;
                        questions.TransporttypeRefId = questioninfo.TransporttypeRefId;
                        questions.ParentId = questioninfo.ParentId;
                        questions.PropertyName = questioninfo.PropertyName;
                        questions.IsQuestionPath = questioninfo.IsQuestionPath;
                        questions.IsFlex = questioninfo.IsFlex;
                        questions.IsSmart = questioninfo.IsSmart;
                        questions.Position = questioninfo.Position;
                        questions.IsParent = questioninfo.IsParent;
                        questions.IsDefault = questioninfo.IsDefault;
                        questions.IsChild = questioninfo.IsChild;
                        questions.Name = GetQuestionsName(questioninfo, defualtLanguage);
                        questionsList.Add(questions);
                    }

                    return questionsList.OrderBy(x => x.Position).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<Questions> GetSmartQuestions(int layoutId, int activityId, int transportId, int questionId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    //var IsCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault(); //&& (IsCopyLayout ? (1 == 1) : x.ISModification != (int)EnumCopyColorCode.Red)
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);

                    var activitiesRefId = 0; //activityId not used here

                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", layoutId);
                    SqlParameter activityParam = new SqlParameter("@ActivityRefId", activitiesRefId);
                    SqlParameter transportTypeParam = new SqlParameter("@TransporttypeRefId", transportId);
                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionId);

                    var questionTranslationDetails = context.QuestionsTranslation.FromSql("GetQuestions @LayoutRefId,@ActivityRefId,@TransporttypeRefId,@QuestionRefId", layoutParam, activityParam, transportTypeParam, questionParam).Where(x => x.IsSmart == true && x.ISModification != (int)EnumCopyColorCode.Red).ToList();
                    List<Questions> questionsList = new List<Questions>();

                    foreach (var questioninfo in questionTranslationDetails)
                    {
                        Questions questions = new Questions();

                        questions.QuestionId = questioninfo.QuestionId;
                        questions.LayoutRefId = questioninfo.LayoutRefId;
                        questions.ActivitiesRefId = questioninfo.ActivitiesRefId;
                        questions.TransporttypeRefId = questioninfo.TransporttypeRefId;
                        questions.ParentId = questioninfo.ParentId;
                        questions.PropertyName = questioninfo.PropertyName;
                        questions.IsQuestionPath = questioninfo.IsQuestionPath;
                        questions.IsFlex = questioninfo.IsFlex;
                        questions.IsSmart = questioninfo.IsSmart;
                        questions.Position = questioninfo.Position;
                        questions.IsParent = questioninfo.IsParent;
                        questions.IsDefault = questioninfo.IsDefault;
                        questions.IsChild = questioninfo.IsChild;
                        questions.Name = GetQuestionsName(questioninfo, defualtLanguage);
                        questionsList.Add(questions);
                    }

                    return questionsList.OrderBy(x => x.Position).ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateQuestions(boQuestion Questions)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Questions.SingleOrDefault(IC => IC.QuestionId == Questions.QuestionId);
                    if (result != null)
                    {
                        result.Name = Questions.Name;
                        result.UpdatedBy = Questions.UpdatedBy;
                        result.UpdatedOn = Questions.UpdatedOn;
                        context.SaveChanges();


                        /* Written By Selvam Thangaraj - Go */

                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.QuestionId;
                        langrefdetail.QuestionRefId = result.QuestionId;
                        langrefdetail.LayoutRefId = result.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                        langrefdetail.LangText = result.Name ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;

                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */

                    }
                    return "updated";
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Questions SaveQuestions(Questions Questions)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var positionId = (context.Questions.Where(u => u.ActivitiesRefId == Questions.ActivitiesRefId && u.LayoutRefId == Questions.LayoutRefId).Any() ? context.Questions.Where(u => u.ActivitiesRefId == Questions.ActivitiesRefId && u.LayoutRefId == Questions.LayoutRefId).Max(x => x.Position ?? 0) : 0);
                    Questions.Position = positionId + 1;
                    var LanguageRefId = context.UsedLanguages.Where(x => x.LayoutRefId == Questions.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageRefId).FirstOrDefault();
                    Questions.DefaultLanguageId = LanguageRefId;

                    context.Add(Questions);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(Questions.LayoutRefId);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = Questions.QuestionId;
                    langrefdetail.QuestionRefId = Questions.QuestionId;
                    langrefdetail.LayoutRefId = Questions.LayoutRefId;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                    langrefdetail.LangText = Questions.Name ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = Questions.CreatedBy;
                    langrefdetail.UpdatedBy = Questions.CreatedBy;

                    _translatorRepository.SaveLangDetail(langrefdetail);

                    /* Written By Selvam Thangaraj - Stop */

                    return Questions;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Questions SaveSmartQuestions(Questions Questions)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (context.Questions.Where(x => x.LayoutRefId == Questions.LayoutRefId && x.IsSmart == true && x.Name == Questions.Name).Select(x => x.QuestionId).Count() == 0)
                    {
                        var positionId = (context.Questions.Where(u => u.ActivitiesRefId == Questions.ActivitiesRefId && u.LayoutRefId == Questions.LayoutRefId).Any() ? context.Questions.Where(u => u.ActivitiesRefId == Questions.ActivitiesRefId && u.LayoutRefId == Questions.LayoutRefId).Max(x => x.Position ?? 0) : 0);
                        Questions.Position = positionId + 1;
                        var LanguageRefId = context.UsedLanguages.Where(x => x.LayoutRefId == Questions.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageRefId).FirstOrDefault();
                        Questions.DefaultLanguageId = LanguageRefId;

                        context.Add(Questions);
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */

                        var LanguageName = _translatorRepository.GetDefaultLanguage(Questions.LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = Questions.QuestionId;
                        langrefdetail.QuestionRefId = Questions.QuestionId;
                        langrefdetail.LayoutRefId = Questions.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                        langrefdetail.LangText = Questions.Name ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = Questions.CreatedBy;
                        langrefdetail.UpdatedBy = Questions.CreatedBy;

                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */

                        return Questions;
                    }
                    else
                        return context.Questions.Where(x => x.LayoutRefId == Questions.LayoutRefId && x.IsSmart == true && x.Name == Questions.Name).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public EntryProperties SaveEntryProperties(EntryProperties EntryProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(EntryProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(EntryProperties.LayoutRefId);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = EntryProperties.EntryId ?? 0;
                    langrefdetail.LayoutRefId = EntryProperties.LayoutRefId;
                    langrefdetail.QuestionRefId = EntryProperties.QuestionRefId;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_Title;
                    langrefdetail.LangText = EntryProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = EntryProperties.CreatedBy;
                    langrefdetail.UpdatedBy = EntryProperties.CreatedBy;

                    _translatorRepository.SaveLangDetail(langrefdetail);

                    if (EntryProperties.MaskText != null && EntryProperties.MaskText.Trim().Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_MaskText;
                        langrefdetail.LangText = EntryProperties.MaskText ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (EntryProperties.ProposeFixedValue != null && EntryProperties.ProposeFixedValue.Trim().Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_FixedValue;
                        langrefdetail.LangText = EntryProperties.ProposeFixedValue ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return EntryProperties;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public EntryProperties UpdateEntryProperties(EntryProperties EntryProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.EntryProperties.SingleOrDefault(IC => IC.EntryId == EntryProperties.EntryId);
                    if (result != null)
                    {
                        var Title = result.Title;
                        var MaskText = result.MaskText;
                        var ProposeFixedValue = result.ProposeFixedValue;
                        var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == result.QuestionRefId);
                        if (Question.ISModification == 1)
                        {
                            Question.ISModification = 2;
                            context.SaveChanges();
                        }

                        result.Title = EntryProperties.Title;
                        result.MaskText = EntryProperties.MaskText;
                        result.Size = EntryProperties.Size;
                        result.NotNull = EntryProperties.NotNull;
                        result.MinimumofCharacter = EntryProperties.MinimumofCharacter;
                        result.MinimumOfCharterValue = EntryProperties.MinimumOfCharterValue;
                        result.ExactCharacter = EntryProperties.ExactCharacter;
                        result.Zeronotallowed = EntryProperties.Zeronotallowed;
                        result.Regex = EntryProperties.Regex;
                        result.Predefined = EntryProperties.Predefined;
                        result.Save = EntryProperties.Save;
                        result.SaveTo = EntryProperties.SaveTo;
                        result.Propose = EntryProperties.Propose;
                        result.ProposeFixedValue = EntryProperties.ProposeFixedValue;
                        result.Planningfeedback = EntryProperties.Planningfeedback;
                        result.FeedbackType = EntryProperties.FeedbackType;
                        result.PlanningfeedbackCode = EntryProperties.PlanningfeedbackCode;
                        result.EntryType = EntryProperties.EntryType;
                        result.UpdatedBy = EntryProperties.UpdatedBy;
                        result.UpdatedOn = EntryProperties.UpdatedOn;
                        result.Comment = EntryProperties.Comment;
                        result.DatetimeFormat = EntryProperties.DatetimeFormat;
                        result.Conditionitem = EntryProperties.Conditionitem;
                        result.ConCompareMethod = EntryProperties.ConCompareMethod;
                        result.Convalue = EntryProperties.Convalue;
                        result.Infocolumn = EntryProperties.Infocolumn;
                        result.InfoColumnAction = EntryProperties.InfoColumnAction;
                        result.FileNumber = EntryProperties.FileNumber;
                        result.AutoGenerate = EntryProperties.AutoGenerate;
                        result.ComFixedText = EntryProperties.ComFixedText;
                        result.ConItemSavedValued = EntryProperties.ConItemSavedValued;
                        result.ConvalueSavedValue = EntryProperties.ConvalueSavedValue;
                        result.PartnerCode = EntryProperties.PartnerCode;
                        result.SaveValue = EntryProperties.SaveValue;
                        result.IsDefaultAlaisName = EntryProperties.IsDefaultAlaisName;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = EntryProperties.EntryId ?? 0;
                        langrefdetail.QuestionRefId = EntryProperties.QuestionRefId;
                        langrefdetail.LayoutRefId = EntryProperties.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_Title;
                        langrefdetail.LangText = EntryProperties.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = EntryProperties.UpdatedBy;
                        langrefdetail.UpdatedBy = EntryProperties.UpdatedBy;

                        _translatorRepository.SaveLangDetail(langrefdetail);

                        //if (MaskText != EntryProperties.MaskText && EntryProperties.MaskText != null && EntryProperties.MaskText.Trim().Length > 0)
                        if (EntryProperties.MaskText != null && EntryProperties.MaskText.Trim().Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_MaskText;
                            langrefdetail.LangText = EntryProperties.MaskText ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }
                        else if (MaskText != null && MaskText.Trim().Length > 0)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = result.EntryId ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefId;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_MaskText;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }

                        //if (ProposeFixedValue != EntryProperties.ProposeFixedValue && EntryProperties.ProposeFixedValue != null && EntryProperties.ProposeFixedValue.Trim().Length > 0)
                        if (EntryProperties.ProposeFixedValue != null && EntryProperties.ProposeFixedValue.Trim().Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_FixedValue;
                            langrefdetail.LangText = EntryProperties.ProposeFixedValue ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }
                        else
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = result.EntryId ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefId;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_FixedValue;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */
                        //kiran Statrs

                        if (EntryProperties.PropertyName == "Planning Select Other Title" && EntryProperties.Title != Title)
                        {
                            var psSelectotherTitleParentId = context.Questions.Where(x => x.QuestionId == EntryProperties.QuestionRefId).Select(x => x.ParentId).FirstOrDefault();
                            if (psSelectotherTitleParentId != 0)
                            {
                                var otherParentId = context.Questions.Where(x => x.QuestionId == psSelectotherTitleParentId).Select(x => x.ParentId).FirstOrDefault();
                                if (otherParentId != 0)
                                {
                                    var planningSelect = context.PlanningselectProperties.Where(x => x.QuestionRefId == otherParentId).FirstOrDefault();
                                    if (planningSelect != null)
                                    {
                                        var OtherTitle = planningSelect.OtherTitle;

                                        planningSelect.OtherMaskText = EntryProperties.MaskText;
                                        planningSelect.OtherTitle = EntryProperties.Title;
                                        context.SaveChanges();

                                        if (OtherTitle != planningSelect.OtherTitle && planningSelect.OtherTitle != null && planningSelect.OtherTitle.Trim().Length > 0)
                                        {
                                            langrefdetail.RefId = EntryProperties.QuestionRefId;
                                            langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                                            langrefdetail.LangText = result.Title ?? "";
                                            _translatorRepository.SaveLangDetail(langrefdetail);

                                            langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_OtherTitle;
                                            langrefdetail.RefId = planningSelect.PlanningselectId ?? 0;
                                            langrefdetail.LangText = planningSelect.OtherTitle ?? "";
                                            _translatorRepository.SaveLangDetail(langrefdetail);
                                        }
                                        else if (OtherTitle != null && OtherTitle.Trim().Length > 0 && planningSelect.OtherTitle == null)
                                        {
                                            LangRefDetail dlangrefdetail = new LangRefDetail();
                                            dlangrefdetail.RefId = planningSelect.PlanningselectId ?? 0;
                                            dlangrefdetail.LayoutRefId = planningSelect.LayoutRefId;
                                            dlangrefdetail.CustomerRefId = null;
                                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_OtherTitle;
                                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                                        }
                                    }
                                }
                            }
                        }

                        //Kiran Ends

                        if ((EntryProperties.Title != Title || EntryProperties.MaskText != MaskText) && EntryProperties.PropertyName == "Entry From Variable List")
                        {
                            var trailerOtherTitleParentId = context.Questions.Where(x => x.QuestionId == EntryProperties.QuestionRefId).Select(x => x.ParentId).FirstOrDefault();
                            if (trailerOtherTitleParentId > 0)
                            {
                                var otherParentId = context.Questions.Where(x => x.QuestionId == trailerOtherTitleParentId).Select(x => x.ParentId).FirstOrDefault();
                                if (otherParentId > 0)
                                {
                                    var varListObj = context.VarListProperties.Where(x => x.QuestionRefId == otherParentId).FirstOrDefault();
                                    if (varListObj != null)
                                    {
                                        var OtherTitle = varListObj.OtherTitle;
                                        var OtherMaskText = varListObj.OtherMaskText;

                                        varListObj.OtherMaskText = EntryProperties.MaskText;
                                        varListObj.OtherTitle = EntryProperties.Title;
                                        context.SaveChanges();

                                        if (OtherTitle != varListObj.OtherTitle && varListObj.OtherTitle != null && varListObj.OtherTitle.Trim().Length > 0)
                                        {
                                            langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherTitle;
                                            langrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                                            langrefdetail.LangText = varListObj.OtherTitle ?? "";
                                            _translatorRepository.SaveLangDetail(langrefdetail);
                                        }
                                        else if (OtherTitle != null && OtherTitle.Trim().Length > 0 && varListObj.OtherTitle == null)
                                        {
                                            LangRefDetail dlangrefdetail = new LangRefDetail();
                                            dlangrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                                            dlangrefdetail.LayoutRefId = varListObj.LayoutRefId;
                                            dlangrefdetail.CustomerRefId = null;
                                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherTitle;
                                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                                        }

                                        if (OtherMaskText != varListObj.OtherMaskText && varListObj.OtherMaskText != null && varListObj.OtherMaskText.Trim().Length > 0)
                                        {
                                            langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherMaskText;
                                            langrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                                            langrefdetail.LangText = varListObj.OtherMaskText ?? "";
                                            _translatorRepository.SaveLangDetail(langrefdetail);
                                        }
                                        else if (OtherMaskText != null && OtherMaskText.Trim().Length > 0 && varListObj.OtherMaskText == null)
                                        {
                                            LangRefDetail dlangrefdetail = new LangRefDetail();
                                            dlangrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                                            dlangrefdetail.LayoutRefId = varListObj.LayoutRefId;
                                            dlangrefdetail.CustomerRefId = null;
                                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherMaskText;
                                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<EntryProperties> GetEntryProperties(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var entryTranslationDetails = context.EntryPropertiesTranslation.FromSql("GetEntryProperties @QuestionRefId", questionParam).ToList();
                    var entryMaskTextTranslationDetails = context.EntryPropertiesMaskTextTranslation.FromSql("GetEntryPropertiesMaskText @QuestionRefId", questionParam).ToList();

                    List<EntryProperties> entryPropertiesList = new List<EntryProperties>();

                    foreach (var entryinfo in entryTranslationDetails)
                    {
                        var maskText = entryMaskTextTranslationDetails.FirstOrDefault(x => x.EntryId == entryinfo.EntryId && x.LangRefColTableId == (int)LangRefColTable.EntryProperties_MaskText);
                        var fixedValue = entryMaskTextTranslationDetails.FirstOrDefault(x => x.EntryId == entryinfo.EntryId && x.LangRefColTableId == (int)LangRefColTable.EntryProperties_FixedValue);

                        EntryProperties entry = new EntryProperties();
                        entry.EntryId = entryinfo.EntryId;
                        entry.ActivitiesRefId = entryinfo.ActivitiesRefId;
                        entry.CreatedBy = entryinfo.CreatedBy;
                        entry.CreatedOn = entryinfo.CreatedOn;
                        entry.ExactCharacter = entryinfo.ExactCharacter ?? 0;
                        entry.FeedbackType = entryinfo.FeedbackType;
                        entry.LayoutRefId = entryinfo.LayoutRefId;
                        entry.MaskText = maskText == null ? "" : GetEntryMaskTextTranslationText(maskText, defualtLanguage);
                        entry.MinimumofCharacter = entryinfo.MinimumofCharacter ?? 0;
                        entry.NotNull = entryinfo.NotNull ?? 0;
                        entry.Planningfeedback = entryinfo.Planningfeedback;
                        entry.PlanningfeedbackCode = entryinfo.PlanningfeedbackCode;
                        entry.Predefined = entryinfo.Predefined;
                        entry.Propose = entryinfo.Propose;
                        entry.QuestionRefId = entryinfo.QuestionRefId;
                        entry.Regex = entryinfo.Regex;
                        entry.Save = entryinfo.Save ?? 0;
                        entry.Size = entryinfo.Size;
                        entry.Title = GetEntryTranslationText(entryinfo, defualtLanguage);
                        entry.UpdatedBy = entryinfo.UpdatedBy;
                        entry.UpdatedOn = entryinfo.UpdatedOn;
                        entry.Zeronotallowed = entryinfo.Zeronotallowed ?? 0;
                        entry.EntryType = entryinfo.EntryType;
                        entry.Comment = entryinfo.Comment;
                        entry.Conditionitem = entryinfo.Conditionitem;
                        entry.ConCompareMethod = entryinfo.ConCompareMethod;
                        entry.Convalue = entryinfo.Convalue;
                        entry.Infocolumn = entryinfo.Infocolumn;
                        entry.InfoColumnAction = entryinfo.InfoColumnAction;
                        entry.FileNumber = entryinfo.FileNumber;
                        entry.ConItemSavedValued = entryinfo.ConItemSavedValued;
                        entry.ConvalueSavedValue = entryinfo.ConvalueSavedValue;
                        entry.PartnerCode = entryinfo.PartnerCode;
                        entry.SaveValue = entryinfo.SaveValue;
                        entry.ComFixedText = entryinfo.ComFixedText;
                        entry.AutoGenerate = entryinfo.AutoGenerate;
                        entry.DatetimeFormat = entryinfo.DatetimeFormat;
                        entry.CopyLayoutRefId = entryinfo.CopyLayoutRefId;
                        entry.SaveTo = entryinfo.SaveTo;
                        entry.MinimumOfCharterValue = entryinfo.MinimumOfCharterValue;
                        entry.IsDefaultAlaisName = entryinfo.IsDefaultAlaisName;
                        entry.ProposeFixedValue = fixedValue == null ? "" : GetEntryMaskTextTranslationText(fixedValue, defualtLanguage);

                        entryPropertiesList.Add(entry);
                    }

                    return entryPropertiesList.ToList();
                }

            }
            catch (Exception ex)
            {
                return new List<EntryProperties>();
            }
        }
        public string GetEntryTranslationText(EntryPropertiesTranslation entryTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = entryTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == entryTranslation.LanguageName)
                    {
                        translationText = "@" + entryTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + entryTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public string GetEntryMaskTextTranslationText(EntryPropertiesMaskTextTranslation entryMaskTextTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = entryMaskTextTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == entryMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + entryMaskTextTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + entryMaskTextTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public List<ActivitiesRegistration> GetParticularActivitiesRegistration(int activitiesId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == activitiesId).ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DocumetScanProperties SaveDocumetScanProperties(DocumetScanProperties DocumetScanProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(DocumetScanProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */
                    var LanguageName = _translatorRepository.GetDefaultLanguage(DocumetScanProperties.LayoutRefId);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = DocumetScanProperties.DocumetScanId ?? 0;
                    langrefdetail.QuestionRefId = DocumetScanProperties.QuestionRefId;
                    langrefdetail.LayoutRefId = DocumetScanProperties.LayoutRefId;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumetScanProperties_Title;
                    langrefdetail.LangText = DocumetScanProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = DocumetScanProperties.CreatedBy;
                    langrefdetail.UpdatedBy = DocumetScanProperties.CreatedBy;

                    _translatorRepository.SaveLangDetail(langrefdetail);

                    /* Written By Selvam Thangaraj - Stop */

                    return DocumetScanProperties;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DocumetScanProperties UpdateDocumetScanProperties(DocumetScanProperties DocumetScanProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.DocumetScanProperties.SingleOrDefault(IC => IC.DocumetScanId == DocumetScanProperties.DocumetScanId);
                    if (result != null)
                    {
                        var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == result.QuestionRefId);
                        if (Question.ISModification == 1)
                        {
                            Question.ISModification = 2;
                            context.SaveChanges();
                        }

                        result.Title = DocumetScanProperties.Title;
                        result.PlanningFeedbackLevel = DocumetScanProperties.PlanningFeedbackLevel;
                        result.UpdatedBy = DocumetScanProperties.UpdatedBy;
                        result.UpdatedOn = DocumetScanProperties.UpdatedOn;
                        result.Comment = DocumetScanProperties.Comment;

                        result.Conditionitem = DocumetScanProperties.Conditionitem;
                        result.ConCompareMethod = DocumetScanProperties.ConCompareMethod;
                        result.Convalue = DocumetScanProperties.Convalue;
                        result.ComFixedText = DocumetScanProperties.ComFixedText;



                        result.ConItemSavedValued = DocumetScanProperties.ConItemSavedValued;
                        result.ConvalueSavedValue = DocumetScanProperties.ConvalueSavedValue;

                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */

                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.DocumetScanId ?? 0;
                        langrefdetail.QuestionRefId = result.QuestionRefId;
                        langrefdetail.LayoutRefId = result.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumetScanProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;

                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */
                    }

                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<DocumetScanProperties> GetDocumetScanProperties(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);

                    var documentScanTranslationDetails = context.DocumetScanPropertiesTranslation.FromSql("GetDocumetScanProperties @QuestionRefId", questionParam).ToList();
                    List<DocumetScanProperties> documentScanPropertiesList = new List<DocumetScanProperties>();

                    foreach (var documentScaninfo in documentScanTranslationDetails)
                    {
                        DocumetScanProperties documentScan = new DocumetScanProperties();

                        documentScan.DocumetScanId = documentScaninfo.DocumetScanId;
                        documentScan.ActivitiesRefId = documentScaninfo.ActivitiesRefId;
                        documentScan.CreatedBy = documentScaninfo.CreatedBy;
                        documentScan.CreatedOn = documentScaninfo.CreatedOn;
                        documentScan.LayoutRefId = documentScaninfo.LayoutRefId;
                        documentScan.PlanningFeedbackLevel = documentScaninfo.PlanningFeedbackLevel;
                        documentScan.QuestionRefId = documentScaninfo.QuestionRefId;
                        documentScan.Title = GetDocumentScanPropertiesTitle(documentScaninfo, defualtLanguage);
                        documentScan.UpdatedBy = documentScaninfo.UpdatedBy;
                        documentScan.UpdatedOn = documentScaninfo.UpdatedOn;
                        documentScan.DocumentName = documentScaninfo.DocumentName;
                        documentScan.Comment = documentScaninfo.Comment;
                        documentScan.Conditionitem = documentScaninfo.Conditionitem;
                        documentScan.ConCompareMethod = documentScaninfo.ConCompareMethod;
                        documentScan.Convalue = documentScaninfo.Convalue;
                        documentScan.ConItemSavedValued = documentScaninfo.ConItemSavedValued;
                        documentScan.ConvalueSavedValue = documentScaninfo.ConvalueSavedValue;
                        documentScan.ComFixedText = documentScaninfo.ComFixedText;

                        documentScanPropertiesList.Add(documentScan);
                    }
                    return documentScanPropertiesList;
                }
            }
            catch (Exception ex)
            {
                return new List<DocumetScanProperties>();
            }
        }
        public string GetDocumentScanPropertiesTitle(DocumetScanPropertiesTranslation documentScanTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = documentScanTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == documentScanTranslation.LanguageName)
                    {
                        translationText = "@" + documentScanTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + documentScanTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public TextMessageProperties SaveTextMessageProperties(TextMessageProperties TextMessageProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(TextMessageProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */
                    var LanguageName = _translatorRepository.GetDefaultLanguage(TextMessageProperties.LayoutRefId);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = TextMessageProperties.TextMessageId ?? 0;
                    langrefdetail.QuestionRefId = TextMessageProperties.QuestionRefId;
                    langrefdetail.LayoutRefId = TextMessageProperties.LayoutRefId;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TextMessageProperties_Messagecontent;
                    langrefdetail.LangText = TextMessageProperties.Messagecontent ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = TextMessageProperties.CreatedBy;
                    langrefdetail.UpdatedBy = TextMessageProperties.CreatedBy;

                    _translatorRepository.SaveLangDetail(langrefdetail);

                    /* Written By Selvam Thangaraj - Stop */

                    return TextMessageProperties;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public TextMessageProperties UpdateTextMessageProperties(TextMessageProperties TextMessageProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.TextMessageProperties.SingleOrDefault(IC => IC.TextMessageId == TextMessageProperties.TextMessageId);
                    if (result != null)
                    {
                        var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == result.QuestionRefId);
                        if (Question.ISModification == 1)
                        {
                            Question.ISModification = 2;
                            context.SaveChanges();
                        }
                        result.Name = TextMessageProperties.Name;
                        result.Messagecontent = TextMessageProperties.Messagecontent;
                        result.SendMethod = TextMessageProperties.SendMethod;
                        result.UpdatedBy = TextMessageProperties.UpdatedBy;
                        result.UpdatedOn = TextMessageProperties.UpdatedOn;
                        result.Comment = TextMessageProperties.Comment;

                        result.Conditionitem = TextMessageProperties.Conditionitem;
                        result.ConCompareMethod = TextMessageProperties.ConCompareMethod;
                        result.Convalue = TextMessageProperties.Convalue;
                        result.ComFixedText = TextMessageProperties.ComFixedText;

                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.TextMessageId ?? 0;
                        langrefdetail.QuestionRefId = result.QuestionRefId;
                        langrefdetail.LayoutRefId = result.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.TextMessageProperties_Messagecontent;
                        langrefdetail.LangText = result.Messagecontent ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;

                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<TextMessageProperties> GetTextMessageProperties(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);

                    var textMessageTranslationDetails = context.TextMessagePropertiesTranslation.FromSql("GetTextMessageProperties @QuestionRefId", questionParam).ToList();
                    List<TextMessageProperties> textMessagePropertiesList = new List<TextMessageProperties>();

                    foreach (var textMessageinfo in textMessageTranslationDetails)
                    {
                        TextMessageProperties textMessage = new TextMessageProperties();

                        textMessage.TextMessageId = textMessageinfo.TextMessageId;
                        textMessage.ActivitiesRefId = textMessageinfo.ActivitiesRefId;
                        textMessage.CreatedBy = textMessageinfo.CreatedBy;
                        textMessage.CreatedOn = textMessageinfo.CreatedOn;
                        textMessage.LayoutRefId = textMessageinfo.LayoutRefId;
                        textMessage.Messagecontent = GetTextMessagePropertiesMessageContent(textMessageinfo, defualtLanguage);
                        textMessage.Name = textMessageinfo.Name;
                        textMessage.QuestionRefId = textMessageinfo.QuestionRefId;
                        textMessage.SendMethod = textMessageinfo.SendMethod;
                        textMessage.UpdatedBy = textMessageinfo.UpdatedBy;
                        textMessage.UpdatedOn = textMessageinfo.UpdatedOn;
                        textMessage.Comment = textMessageinfo.Comment;
                        textMessage.Conditionitem = textMessageinfo.Conditionitem;
                        textMessage.ConCompareMethod = textMessageinfo.ConCompareMethod;
                        textMessage.Convalue = textMessageinfo.Convalue;
                        textMessage.ConItemSavedValued = textMessageinfo.ConItemSavedValued;
                        textMessage.ConvalueSavedValue = textMessageinfo.ConvalueSavedValue;
                        textMessage.ComFixedText = textMessageinfo.ComFixedText;

                        textMessagePropertiesList.Add(textMessage);
                    }
                    return textMessagePropertiesList;
                }
            }
            catch (Exception ex)
            {
                return new List<TextMessageProperties>();
            }
        }
        public string GetTextMessagePropertiesMessageContent(TextMessagePropertiesTranslation textMessagePropertiesTranslation, string defaultLanguage)
        {
            var textMessageTitle = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                textMessageTitle = textMessagePropertiesTranslation.Belarusian;
            }
            else
            {
                textMessageTitle = "";
            }

            if (textMessageTitle == null || textMessageTitle == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == textMessagePropertiesTranslation.LanguageName)
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.Belarusian;
                    }
                    else
                    {
                        textMessageTitle = "@" + textMessagePropertiesTranslation.English;
                    }
                }
            }
            return textMessageTitle;
        }
        public InfomessageProperties SaveInfomessageProperties(InfomessageProperties InfomessageProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(InfomessageProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */
                    var LanguageName = _translatorRepository.GetDefaultLanguage(InfomessageProperties.LayoutRefId);
                    var CustomerId = context.Layout.Where(x => x.LayoutId == InfomessageProperties.LayoutRefId).Select(x => x.CustomerRefId).FirstOrDefault();
                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = InfomessageProperties.InfomessageId ?? 0;
                    langrefdetail.QuestionRefId = InfomessageProperties.QuestionRefId;
                    langrefdetail.LayoutRefId = InfomessageProperties.LayoutRefId;
                    langrefdetail.CustomerRefId = CustomerId;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.InfomessageProperties_Title;
                    langrefdetail.LangText = InfomessageProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = InfomessageProperties.CreatedBy;
                    langrefdetail.UpdatedBy = InfomessageProperties.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);




                    if (_translatorRepository.StripHTML(InfomessageProperties.Messagecontent).Trim().Length > 0 && InfomessageProperties.InfomessageId > 0)
                    {

                        langrefdetail.LangRefColTableId = (int)LangRefColTable.InfoMessageContentDetail_MessageContent;
                        langrefdetail.LangText = InfomessageProperties.Messagecontent ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return InfomessageProperties;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public InfomessageProperties UpdateInfomessageProperties(InfomessageProperties InfomessageProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.InfomessageProperties.SingleOrDefault(IC => IC.InfomessageId == InfomessageProperties.InfomessageId);
                    if (result != null)
                    {
                        var Messagecontent = result.Messagecontent;
                        var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == result.QuestionRefId);
                        if (Question.ISModification == 1)
                        {
                            Question.ISModification = 2;
                            context.SaveChanges();
                        }
                        result.Name = InfomessageProperties.Name;
                        result.Messagecontent = InfomessageProperties.Messagecontent.Replace("@", "");
                        result.Messagetype = InfomessageProperties.Messagetype;
                        result.Title = InfomessageProperties.Title.Replace("@", "");
                        result.Titlestyle = InfomessageProperties.Titlestyle;
                        result.InfoMessageIconTypeRefId = InfomessageProperties.InfoMessageIconTypeRefId;
                        result.Fontstyle = InfomessageProperties.Fontstyle;
                        result.UpdatedBy = InfomessageProperties.UpdatedBy;
                        result.UpdatedOn = InfomessageProperties.UpdatedOn;
                        result.Comment = InfomessageProperties.Comment;

                        result.Conditionitem = InfomessageProperties.Conditionitem;
                        result.ConCompareMethod = InfomessageProperties.ConCompareMethod;
                        result.Convalue = InfomessageProperties.Convalue;
                        result.ComFixedText = InfomessageProperties.ComFixedText;
                        result.ConItemSavedValued = InfomessageProperties.ConItemSavedValued;
                        result.ConvalueSavedValue = InfomessageProperties.ConvalueSavedValue;

                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);
                        var CustomerId = context.Layout.Where(x => x.LayoutId == InfomessageProperties.LayoutRefId).Select(x => x.CustomerRefId).FirstOrDefault();
                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.InfomessageId ?? 0;
                        langrefdetail.QuestionRefId = result.QuestionRefId;
                        langrefdetail.LayoutRefId = result.LayoutRefId;
                        langrefdetail.CustomerRefId = CustomerId;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.InfomessageProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if (Messagecontent != InfomessageProperties.Messagecontent.Replace("@", "") && InfomessageProperties.Messagecontent.Replace("@", "") != null && InfomessageProperties.Messagecontent.Replace("@", "").Trim().Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.InfoMessageContentDetail_MessageContent;
                            langrefdetail.LangText = InfomessageProperties.Messagecontent.Replace("@", "") ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }
                        else if (Messagecontent != null && Messagecontent.Trim().Length > 0 && InfomessageProperties.Messagecontent == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = result.InfomessageId ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefId;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.InfoMessageContentDetail_MessageContent;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<InfomessageProperties> GetInfomessageProperties(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var languageName = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var infoMessageTranslationDetails = context.InfoMessagePropertiesTranslation.FromSql("GetInfoMessageProperties @QuestionRefId", questionParam).ToList();
                    var infoMessageContentTranslationDetails = context.InfoMessageContentTranslation.FromSql("GetInfoMessagePropertiesMessageContent @QuestionRefId", questionParam).ToList();

                    List<InfomessageProperties> infomessagePropertiesList = new List<InfomessageProperties>();

                    foreach (var infoMessageDetail in infoMessageTranslationDetails)
                    {
                        var messageContent = infoMessageContentTranslationDetails.Where(x => x.InfomessageId == infoMessageDetail.InfomessageId && x.LangRefColTableId == (int)LangRefColTable.InfoMessageContentDetail_MessageContent).FirstOrDefault();

                        InfomessageProperties infoMessage = new InfomessageProperties();
                        infoMessage.InfomessageId = infoMessageDetail.InfomessageId;
                        infoMessage.ActivitiesRefId = infoMessageDetail.ActivitiesRefId;
                        infoMessage.CreatedBy = infoMessageDetail.CreatedBy;
                        infoMessage.CreatedOn = infoMessageDetail.CreatedOn;
                        infoMessage.Fontstyle = infoMessageDetail.Fontstyle;
                        infoMessage.InfoMessageIconTypeRefId = infoMessageDetail.InfoMessageIconTypeRefId;
                        infoMessage.LayoutRefId = infoMessageDetail.LayoutRefId;
                        infoMessage.Messagecontent = (messageContent == null ? "" : GetInfoMessageContentTranslationText(messageContent, languageName).Replace("@<p>", "<p>@"));
                        infoMessage.Messagetype = infoMessageDetail.Messagetype;
                        infoMessage.Name = infoMessageDetail.Name;
                        infoMessage.QuestionRefId = infoMessageDetail.QuestionRefId;
                        infoMessage.Title = GetInfoMessageTranslationText(infoMessageDetail, languageName).Replace("@<p>", "<p>@");
                        infoMessage.Titlestyle = infoMessageDetail.Titlestyle;
                        infoMessage.UpdatedBy = infoMessageDetail.UpdatedBy;
                        infoMessage.UpdatedOn = infoMessageDetail.UpdatedOn;
                        infoMessage.Comment = infoMessageDetail.Comment;
                        infoMessage.Conditionitem = infoMessageDetail.Conditionitem;
                        infoMessage.ConCompareMethod = infoMessageDetail.ConCompareMethod;
                        infoMessage.Convalue = infoMessageDetail.Convalue;
                        infoMessage.ConItemSavedValued = infoMessageDetail.ConItemSavedValued;
                        infoMessage.ConvalueSavedValue = infoMessageDetail.ConvalueSavedValue;
                        infoMessage.ComFixedText = infoMessageDetail.ComFixedText;
                        infomessagePropertiesList.Add(infoMessage);
                    }

                    return infomessagePropertiesList.ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetInfoMessageTranslationText(InfoMessagePropertiesTranslation infoMessageTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = infoMessageTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == infoMessageTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + infoMessageTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public string GetInfoMessageContentTranslationText(InfoMessageContentTranslation infoMessageContentTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = infoMessageContentTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == infoMessageContentTranslation.LanguageName)
                    {
                        translationText = "@" + infoMessageContentTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + infoMessageContentTranslation.English;
                    }
                }
            }
            return translationText;
        }
        // To DO
        public CheckoffProperties SaveCheckoffProperties(CheckoffProperties CheckoffProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(CheckoffProperties);
                    context.SaveChanges();
                    return CheckoffProperties;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public CheckoffProperties UpdateCheckoffProperties(CheckoffProperties CheckoffProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.CheckoffProperties.SingleOrDefault(IC => IC.CheckoffId == CheckoffProperties.CheckoffId);
                    if (result != null)
                    {
                        var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == result.QuestionRefId);
                        if (Question.ISModification == 1)
                        {
                            Question.ISModification = 2;
                            context.SaveChanges();
                        }
                        result.Name = CheckoffProperties.Name;
                        result.Planninglevel = CheckoffProperties.Planninglevel;
                        result.UpdatedBy = CheckoffProperties.UpdatedBy;
                        result.UpdatedOn = CheckoffProperties.UpdatedOn;
                        result.Comment = CheckoffProperties.Comment;
                        context.SaveChanges();
                    }
                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<CheckoffProperties> GetCheckoffProperties(int questionRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.CheckoffProperties.Where(x => x.QuestionRefId == questionRefId).ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public PlanningselectProperties SavePlanningselectProperties(PlanningselectProperties PlanningselectProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    PlanningselectProperties model = new PlanningselectProperties();
                    model = PlanningselectProperties;

                    context.PlanningselectProperties.Add(model);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */
                    var LanguageName = _translatorRepository.GetDefaultLanguage(PlanningselectProperties.LayoutRefId);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = PlanningselectProperties.PlanningselectId ?? 0;
                    langrefdetail.QuestionRefId = PlanningselectProperties.QuestionRefId;
                    langrefdetail.LayoutRefId = PlanningselectProperties.LayoutRefId;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_Title;
                    langrefdetail.LangText = PlanningselectProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = PlanningselectProperties.CreatedBy;
                    langrefdetail.UpdatedBy = PlanningselectProperties.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    if (PlanningselectProperties.OtherTitle != null && PlanningselectProperties.OtherTitle.Trim().Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_OtherTitle;
                        langrefdetail.LangText = PlanningselectProperties.OtherTitle ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (PlanningselectProperties.Fixedvalue != null && PlanningselectProperties.Fixedvalue.Trim().Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_Fixedvalue;
                        langrefdetail.LangText = PlanningselectProperties.Fixedvalue ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return PlanningselectProperties;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public PlanningselectProperties UpdatePlanningselectProperties(PlanningselectProperties PlanningselectProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.PlanningselectProperties.SingleOrDefault(IC => IC.PlanningselectId == PlanningselectProperties.PlanningselectId);
                    if (result != null)
                    {
                        var OtherTitle = result.OtherTitle;
                        var Fixedvalue = result.Fixedvalue;

                        var Question = context.Questions.Where(IC => IC.QuestionId == result.QuestionRefId).FirstOrDefault();
                        if (Question.ISModification == 1)
                        {
                            Question.ISModification = 2;
                            context.SaveChanges();
                        }
                        result.Title = PlanningselectProperties.Title;
                        result.Planninglevel = PlanningselectProperties.Planninglevel;
                        result.Planningspecific = PlanningselectProperties.Planningspecific;
                        result.Chooseactivity = PlanningselectProperties.Chooseactivity;
                        result.Startselecteditem = PlanningselectProperties.Startselecteditem;
                        result.StartSelection = PlanningselectProperties.StartSelection;

                        if ((result.Other == "Driver entry (A50)" && PlanningselectProperties.Other == "Driver entry (A50)") || (result.Other == "No other Driver entry (A50)" && PlanningselectProperties.Other == "No other Driver entry (A50)") || (result.Other != "Driver entry (A50)" && PlanningselectProperties.Other == "Driver entry (A50)") || (result.Other != "No other Driver entry (A50)" && PlanningselectProperties.Other == "No other Driver entry (A50)"))
                        {
                            var otherresult = context.Questions.Where(x => x.Name == "Other" && x.ParentId == result.QuestionRefId).FirstOrDefault();
                            if (otherresult != null)
                            {
                                var othertitleresult = context.Questions.Where(x => x.ParentId == otherresult.QuestionId && x.PropertyName == "Planning Select Other Title").FirstOrDefault();
                                if (othertitleresult != null)
                                {
                                    othertitleresult.Name = PlanningselectProperties.OtherTitle;
                                    othertitleresult.UpdatedBy = PlanningselectProperties.UpdatedBy;
                                    othertitleresult.UpdatedOn = PlanningselectProperties.UpdatedOn;

                                    var planningSelectEntry = context.EntryProperties.Where(x => x.QuestionRefId == othertitleresult.QuestionId).FirstOrDefault();
                                    if (planningSelectEntry != null)
                                    {
                                        boQuestion objQuestions = new boQuestion();
                                        objQuestions.QuestionId = othertitleresult.QuestionId;
                                        objQuestions.Name = PlanningselectProperties.OtherTitle;
                                        objQuestions.UpdatedBy = PlanningselectProperties.UpdatedBy;
                                        objQuestions.UpdatedOn = DateTime.Now;
                                        UpdateQuestions(objQuestions);

                                        planningSelectEntry.Title = PlanningselectProperties.OtherTitle;
                                        planningSelectEntry.MaskText = PlanningselectProperties.OtherMaskText;
                                        UpdateEntryProperties(planningSelectEntry);
                                    }
                                }
                                else
                                {
                                    Questions objQuestion = new Questions();

                                    objQuestion.ActivitiesRefId = otherresult.ActivitiesRefId;
                                    objQuestion.CreatedBy = PlanningselectProperties.UpdatedBy;
                                    objQuestion.CreatedOn = PlanningselectProperties.UpdatedOn ?? DateTime.Now;
                                    objQuestion.LayoutRefId = otherresult.LayoutRefId;
                                    objQuestion.Name = PlanningselectProperties.OtherTitle;
                                    objQuestion.ParentId = otherresult.QuestionId;
                                    objQuestion.TransporttypeRefId = otherresult.TransporttypeRefId;
                                    objQuestion.PropertyName = "Planning Select Other Title";
                                    objQuestion.ISModification = 0;
                                    objQuestion.IsQuestionPath = true;
                                    var entryplanningSelect = SaveQuestions(objQuestion);

                                    //Kiran Starts
                                    //Planning Select Defaulty Entry
                                    int ANameDetailsId = 0;
                                    AliasNameDetails AND = new AliasNameDetails();
                                    AND.LayoutRefId = PlanningselectProperties.LayoutRefId;
                                    AND.ActivitiesRefId = PlanningselectProperties.ActivitiesRefId;
                                    AND.AliasName = "DriverCreated";
                                    AND.PropertyName = "Planning Select Other Title";
                                    AND.CreatedBy = PlanningselectProperties.UpdatedBy;
                                    AND.CreatedOn = PlanningselectProperties.UpdatedOn ?? DateTime.Now;
                                    AND.IsTempData = false;
                                    var aliasNameId = SaveAliasNameDetails(AND);
                                    ANameDetailsId = aliasNameId.AliasNameDetailsId;

                                    EntryProperties objEntries = new EntryProperties();
                                    objEntries.QuestionRefId = entryplanningSelect.QuestionId;
                                    objEntries.LayoutRefId = PlanningselectProperties.LayoutRefId; ;
                                    objEntries.ActivitiesRefId = PlanningselectProperties.ActivitiesRefId;
                                    objEntries.Title = PlanningselectProperties.OtherTitle;
                                    objEntries.MaskText = PlanningselectProperties.OtherMaskText;
                                    objEntries.Size = "50";
                                    objEntries.NotNull = 1;
                                    objEntries.Save = 1;
                                    objEntries.Propose = 0;
                                    objEntries.EntryType = "Alphanumeric";
                                    objEntries.CreatedBy = PlanningselectProperties.UpdatedBy;
                                    objEntries.CreatedOn = PlanningselectProperties.UpdatedOn ?? DateTime.Now;
                                    objEntries.FileNumber = "Not Set";
                                    objEntries.AutoGenerate = 0;
                                    objEntries.Conditionitem = 1;

                                    objEntries.SaveTo = ANameDetailsId;

                                    var entries = SaveEntryProperties(objEntries);
                                    UpdateAliasPropertyTableId(ANameDetailsId, entries.EntryId.GetValueOrDefault());

                                    //kiran Ends
                                }
                            }
                        }
                        if ((result.Other == "Driver entry (A50)" && PlanningselectProperties.Other != "Driver entry (A50)") || (result.Other == "No other Driver entry (A50)" && PlanningselectProperties.Other != "No other Driver entry (A50)"))
                        {
                            var otherresult = context.Questions.Where(x => x.Name == "Other" && x.ParentId == result.QuestionRefId).FirstOrDefault();
                            if (otherresult != null)
                            {
                                var othertitleresult = context.Questions.Where(x => x.ParentId == otherresult.QuestionId && x.PropertyName == "Planning Select Other Title").FirstOrDefault();
                                if (othertitleresult != null)
                                {
                                    var otherTitleEntryProperty = context.EntryProperties.Where(x => x.QuestionRefId == othertitleresult.QuestionId).FirstOrDefault();
                                    if (otherTitleEntryProperty != null)
                                    {
                                        var targetand = context.AliasNameDetails.Where(x => x.PropertyTableId == otherTitleEntryProperty.EntryId && x.PropertyName == "Entry").FirstOrDefault();

                                        bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Entry", otherTitleEntryProperty.EntryId ?? 0, otherresult.ActivitiesRefId);
                                        if (isAliasNameDeleted)
                                        {
                                            if (targetand != null)
                                            {
                                                var aliasresult = context.AliasNameDetails.Where(x => x.PropertyTableId == otherTitleEntryProperty.EntryId && x.PropertyName == "Entry").ToList();
                                                context.AliasNameDetails.RemoveRange(aliasresult);
                                                context.SaveChanges();
                                            }
                                        }

                                        /* Written By Selvam Thangaraj - Go */

                                        LangRefDetail elangrefdetail = new LangRefDetail();
                                        elangrefdetail.RefId = otherTitleEntryProperty.EntryId ?? 0;
                                        elangrefdetail.LayoutRefId = otherTitleEntryProperty.LayoutRefId;
                                        elangrefdetail.CustomerRefId = null;
                                        elangrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_Title;
                                        _translatorRepository.DeleteLangDetail(elangrefdetail);

                                        elangrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_MaskText;
                                        _translatorRepository.DeleteLangDetail(elangrefdetail);

                                        elangrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_FixedValue;
                                        _translatorRepository.DeleteLangDetail(elangrefdetail);

                                        /* Written By Selvam Thangaraj - Stop */

                                        context.EntryProperties.Remove(otherTitleEntryProperty);
                                        context.SaveChanges();
                                    }

                                    LangRefDetail dlangrefdetail = new LangRefDetail();
                                    dlangrefdetail.RefId = othertitleresult.QuestionId;
                                    dlangrefdetail.LayoutRefId = othertitleresult.LayoutRefId;
                                    dlangrefdetail.CustomerRefId = null;
                                    dlangrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                                    _translatorRepository.DeleteLangDetail(dlangrefdetail);

                                    context.Questions.Remove(othertitleresult);
                                    context.SaveChanges();
                                }
                            }
                        }
                        if ((result.Other != "QP (save to DriverCreated)" && PlanningselectProperties.Other == "QP (save to DriverCreated)") || (result.Other != "No other QP (save to DriverCreated)" && PlanningselectProperties.Other == "No other QP (save to DriverCreated)"))
                        {
                            var listActivitiesRegistrations = GetActivitiesRegistration(PlanningselectProperties.ActivitiesRefId);

                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = PlanningselectProperties.LayoutRefId;
                            AND.ActivitiesRefId = PlanningselectProperties.ActivitiesRefId;
                            AND.AliasName = listActivitiesRegistrations + "." + PlanningselectProperties.Planninglevel + ".DriverCreated";
                            AND.PropertyName = "Planning Select";
                            AND.PropertyTableId = PlanningselectProperties.PlanningselectId ?? 0;
                            AND.CreatedBy = PlanningselectProperties.UpdatedBy;
                            AND.CreatedOn = PlanningselectProperties.UpdatedOn ?? DateTime.Now;
                            context.AliasNameDetails.Add(AND);
                        }

                        result.Other = PlanningselectProperties.Other;
                        result.PlanningSelectionMethod = PlanningselectProperties.PlanningSelectionMethod;
                        result.OtherTitle = PlanningselectProperties.OtherTitle;
                        result.OtherMaskText = PlanningselectProperties.OtherMaskText;
                        result.Fixedvalue = PlanningselectProperties.Fixedvalue;
                        result.QP = PlanningselectProperties.QP;
                        result.Planningreadout = PlanningselectProperties.Planningreadout;
                        result.UpdatedBy = PlanningselectProperties.UpdatedBy;
                        result.UpdatedOn = PlanningselectProperties.UpdatedOn;
                        result.Comment = PlanningselectProperties.Comment;

                        result.CheckOffPlanning = PlanningselectProperties.CheckOffPlanning;
                        result.FilterRefId = PlanningselectProperties.FilterRefId;
                        result.CompareMethodRefId = PlanningselectProperties.CompareMethodRefId;
                        result.PSComparetoRefId = PlanningselectProperties.PSComparetoRefId;
                        result.InstructionSetSavedValues = PlanningselectProperties.InstructionSetSavedValues;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.PlanningselectId ?? 0;
                        langrefdetail.QuestionRefId = result.QuestionRefId;
                        langrefdetail.LayoutRefId = result.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if (OtherTitle != result.OtherTitle && result.OtherTitle != null && result.OtherTitle.Trim().Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_OtherTitle;
                            langrefdetail.RefId = result.PlanningselectId ?? 0;
                            langrefdetail.LangText = result.OtherTitle ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }
                        else if (OtherTitle != null && OtherTitle.Trim().Length > 0 && result.OtherTitle == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = result.PlanningselectId ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefId;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_OtherTitle;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }

                        if (Fixedvalue != result.Fixedvalue && result.Fixedvalue != null && result.Fixedvalue.Trim().Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_Fixedvalue;
                            langrefdetail.LangText = result.Fixedvalue ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }
                        else if (Fixedvalue != null && Fixedvalue.Trim().Length > 0 && result.Fixedvalue == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = result.PlanningselectId ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefId;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_Fixedvalue;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PlanningselectProperties> GetPlanningselectProperties(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var languageName = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var planningSelectTranslationDetails = context.PlanningSelectPropertiesTranslation.FromSql("GetPlanningSelectProperties @QuestionRefId", questionParam).ToList();
                    var planningSelectOtherTranslationDetails = context.PlanningSelectPropertiesOtherTranslation.FromSql("GetPlanningSelectPropertiesOtherTranslation @QuestionRefId", questionParam).ToList();

                    List<PlanningselectProperties> planningSelectPropertiesList = new List<PlanningselectProperties>();

                    foreach (var planningSelectInfo in planningSelectTranslationDetails)
                    {
                        var otherTitle = planningSelectOtherTranslationDetails.FirstOrDefault(x => x.PlanningselectId == planningSelectInfo.PlanningselectId && x.LangRefColTableId == (int)LangRefColTable.PlanningselectProperties_OtherTitle);
                        var otherFixedvalue = planningSelectOtherTranslationDetails.FirstOrDefault(x => x.PlanningselectId == planningSelectInfo.PlanningselectId && x.LangRefColTableId == (int)LangRefColTable.PlanningselectProperties_Fixedvalue);

                        PlanningselectProperties planningSelect = new PlanningselectProperties();

                        planningSelect.PlanningselectId = planningSelectInfo.PlanningselectId;
                        planningSelect.ActivitiesRefId = planningSelectInfo.ActivitiesRefId;
                        planningSelect.Chooseactivity = planningSelectInfo.Chooseactivity;
                        planningSelect.CreatedBy = planningSelectInfo.CreatedBy;
                        planningSelect.CreatedOn = planningSelectInfo.CreatedOn;
                        planningSelect.Fixedvalue = otherFixedvalue == null ? "" : GetPlanningSelectOtherTranslationText(otherFixedvalue, languageName);
                        planningSelect.LayoutRefId = planningSelectInfo.LayoutRefId;
                        planningSelect.Other = planningSelectInfo.Other;
                        planningSelect.OtherTitle = otherTitle == null ? "" : GetPlanningSelectOtherTranslationText(otherTitle, languageName);
                        planningSelect.Planninglevel = planningSelectInfo.Planninglevel;
                        planningSelect.Planningreadout = planningSelectInfo.Planningreadout;
                        planningSelect.Planningspecific = planningSelectInfo.Planningspecific;
                        planningSelect.QP = planningSelectInfo.QP;
                        planningSelect.QuestionRefId = planningSelectInfo.QuestionRefId;
                        planningSelect.Startselecteditem = planningSelectInfo.Startselecteditem;
                        planningSelect.Title = GetPlanningSelectTranslationText(planningSelectInfo, languageName);
                        planningSelect.UpdatedBy = planningSelectInfo.UpdatedBy;
                        planningSelect.UpdatedOn = planningSelectInfo.UpdatedOn;
                        planningSelect.Comment = planningSelectInfo.Comment;
                        planningSelect.FilterRefId = planningSelectInfo.FilterRefId;
                        planningSelect.CompareMethodRefId = planningSelectInfo.CompareMethodRefId;
                        planningSelect.InstructionSetSavedValues = planningSelectInfo.InstructionSetSavedValues;
                        planningSelect.PSComparetoRefId = planningSelectInfo.PSComparetoRefId;
                        planningSelect.CheckOffPlanning = planningSelectInfo.CheckOffPlanning;
                        planningSelect.StartSelection = planningSelectInfo.StartSelection;
                        planningSelect.OtherMaskText = planningSelectInfo.OtherMaskText;
                        planningSelect.PlanningSelectionMethod = planningSelectInfo.PlanningSelectionMethod;

                        planningSelectPropertiesList.Add(planningSelect);
                    }

                    return planningSelectPropertiesList.ToList();
                }

            }
            catch (Exception ex)
            {
                return new List<PlanningselectProperties>();
            }
        }
        public string GetPlanningSelectTranslationText(PlanningSelectPropertiesTranslation planningSelectTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = planningSelectTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == planningSelectTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + planningSelectTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public string GetPlanningSelectOtherTranslationText(PlanningSelectPropertiesOtherTranslation planningSelectOtherTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = planningSelectOtherTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == planningSelectOtherTranslation.LanguageName)
                    {
                        translationText = "@" + planningSelectOtherTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + planningSelectOtherTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public SetActionValueProperties SaveSetActionValueProperties(SetActionValueProperties SetActionValueProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (SetActionValueProperties.SetState != "" && SetActionValueProperties.SetState != null)
                    {
                        var activity = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == SetActionValueProperties.ActivitiesRefId).FirstOrDefault();
                        if (activity != null)
                        {
                            activity.EmptyFullSolo = true;
                            context.SaveChanges();
                        }
                    }

                    context.Add(SetActionValueProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(SetActionValueProperties.LayoutRefId);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = SetActionValueProperties.SetActionValueId ?? 0;
                    langrefdetail.QuestionRefId = SetActionValueProperties.QuestionRefId;
                    langrefdetail.LayoutRefId = SetActionValueProperties.LayoutRefId;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = SetActionValueProperties.CreatedBy;
                    langrefdetail.UpdatedBy = SetActionValueProperties.CreatedBy;

                    if (SetActionValueProperties.Value != null && SetActionValueProperties.Value.Trim().Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.SetActionValueProperties_Value;
                        langrefdetail.LangText = SetActionValueProperties.Value ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }
                    else if (SetActionValueProperties.InfoCoumnFixedText != null && SetActionValueProperties.InfoCoumnFixedText.Trim().Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.SetActionValueProperties_InfoColumnFixedText;
                        langrefdetail.LangText = SetActionValueProperties.InfoCoumnFixedText ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return SetActionValueProperties;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public SetActionValueProperties UpdateSetActionValueProperties(SetActionValueProperties SetActionValueProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var result = context.SetActionValueProperties.SingleOrDefault(IC => IC.SetActionValueId == SetActionValueProperties.SetActionValueId);
                    if (result != null)
                    {
                        var ExistValue = result.Value;
                        var ExistInfoColumnFixedText = result.InfoCoumnFixedText;

                        var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == result.QuestionRefId);
                        if (Question.ISModification == 1)
                        {
                            Question.ISModification = 2;
                            context.SaveChanges();
                        }
                        result.Title = SetActionValueProperties.Title;
                        result.SelectQP = SetActionValueProperties.SelectQP;
                        result.CancelcurrentQP = SetActionValueProperties.CancelcurrentQP;
                        result.SelectView = SetActionValueProperties.SelectView;
                        result.SavedValue = SetActionValueProperties.SavedValue;
                        result.SelectAllowance = SetActionValueProperties.SelectAllowance;
                        result.SelectInfoColumn = SetActionValueProperties.SelectInfoColumn;
                        result.SetReset = SetActionValueProperties.SetReset;
                        result.Value = SetActionValueProperties.Value;
                        result.CopySavedValue = SetActionValueProperties.CopySavedValue;
                        result.UpdatedBy = SetActionValueProperties.UpdatedBy;
                        result.UpdatedOn = SetActionValueProperties.UpdatedOn;
                        result.Comment = SetActionValueProperties.Comment;

                        result.SetActionValueTypeId = SetActionValueProperties.SetActionValueTypeId;
                        result.InfoCoumnFixedText = SetActionValueProperties.InfoCoumnFixedText;
                        result.InfoColumnSavedValue = SetActionValueProperties.InfoColumnSavedValue;
                        result.SelectTargetSavedValue = SetActionValueProperties.SelectTargetSavedValue;
                        result.SetState = SetActionValueProperties.SetState;
                        if (SetActionValueProperties.SetState != "" && SetActionValueProperties.SetState != null)
                        {
                            var activity = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == SetActionValueProperties.ActivitiesRefId).FirstOrDefault();
                            if (activity != null)
                            {
                                activity.EmptyFullSolo = true;
                                context.SaveChanges();
                            }
                        }

                        result.PlanningIDOBC = SetActionValueProperties.PlanningIDOBC;
                        result.Longitude = SetActionValueProperties.Longitude;
                        result.Latitude = SetActionValueProperties.Latitude;


                        result.Conditionitem = SetActionValueProperties.Conditionitem;
                        result.ConCompareMethod = SetActionValueProperties.ConCompareMethod;
                        result.Convalue = SetActionValueProperties.Convalue;
                        result.ComFixedText = SetActionValueProperties.ComFixedText;



                        result.ConItemSavedValued = SetActionValueProperties.ConItemSavedValued;
                        result.ConvalueSavedValue = SetActionValueProperties.ConvalueSavedValue;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */

                        if ((SetActionValueProperties.Value != null && SetActionValueProperties.Value.Trim().Length > 0) || (SetActionValueProperties.InfoCoumnFixedText != null && SetActionValueProperties.InfoCoumnFixedText.Trim().Length > 0))
                        {
                            var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);

                            LangRefDetail langrefdetail = new LangRefDetail();
                            langrefdetail.RefId = result.SetActionValueId ?? 0;
                            langrefdetail.QuestionRefId = result.QuestionRefId;
                            langrefdetail.LayoutRefId = result.LayoutRefId;
                            langrefdetail.CustomerRefId = null;
                            langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                            langrefdetail.CreatedBy = result.UpdatedBy;
                            langrefdetail.UpdatedBy = result.UpdatedBy;

                            if (SetActionValueProperties.Value != null && SetActionValueProperties.Value.Trim().Length > 0)
                            {
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.SetActionValueProperties_Value;
                                langrefdetail.LangText = SetActionValueProperties.Value ?? "";
                                _translatorRepository.SaveLangDetail(langrefdetail);
                            }
                            else if (SetActionValueProperties.InfoCoumnFixedText != null && SetActionValueProperties.InfoCoumnFixedText.Trim().Length > 0)
                            {
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.SetActionValueProperties_InfoColumnFixedText;
                                langrefdetail.LangText = SetActionValueProperties.InfoCoumnFixedText ?? "";
                                _translatorRepository.SaveLangDetail(langrefdetail);
                            }
                        }

                        if (ExistValue != null && ExistValue.Trim().Length > 0 && (result.Value == null || result.Value == ""))
                        {
                            LangRefDetail langrefdetail = new LangRefDetail();
                            langrefdetail.RefId = result.SetActionValueId ?? 0;
                            langrefdetail.LayoutRefId = result.LayoutRefId;
                            langrefdetail.CustomerRefId = null;
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.SetActionValueProperties_Value;
                            _translatorRepository.DeleteLangDetail(langrefdetail);
                        }
                        if (ExistInfoColumnFixedText != null && ExistInfoColumnFixedText.Trim().Length > 0 && (result.InfoCoumnFixedText == null || result.InfoCoumnFixedText == ""))
                        {
                            LangRefDetail langrefdetail = new LangRefDetail();
                            langrefdetail.RefId = result.SetActionValueId ?? 0;
                            langrefdetail.LayoutRefId = result.LayoutRefId;
                            langrefdetail.CustomerRefId = null;
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.SetActionValueProperties_InfoColumnFixedText;
                            _translatorRepository.DeleteLangDetail(langrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */


                    }
                    return result;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<SetActionValueProperties> GetSetActionValueProperties(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var setActionValueTranslationDetails = context.SetActionValuePropertiesTranslation.FromSql("GetSetActionValueProperties @QuestionRefId", questionParam).ToList();
                    var setActionValueMaskTextTranslationDetails = context.SetActionValuePropertiesInfoCoumnFixedTextTranslation.FromSql("GetSetActionValuePropertiesInfoCoumnFixedText @QuestionRefId", questionParam).ToList();

                    List<SetActionValueProperties> setActionValuePropertiesList = new List<SetActionValueProperties>();

                    foreach (var setActionValueinfo in setActionValueTranslationDetails)
                    {
                        var fixedText = setActionValueMaskTextTranslationDetails.FirstOrDefault(x => x.SetActionValueId == setActionValueinfo.SetActionValueId && x.LangRefColTableId == (int)LangRefColTable.SetActionValueProperties_InfoColumnFixedText);

                        SetActionValueProperties setActionValue = new SetActionValueProperties();

                        setActionValue.SetActionValueId = setActionValueinfo.SetActionValueId;
                        setActionValue.ActivitiesRefId = setActionValueinfo.ActivitiesRefId;
                        setActionValue.CancelcurrentQP = setActionValueinfo.CancelcurrentQP;
                        setActionValue.CreatedBy = setActionValueinfo.CreatedBy;
                        setActionValue.CreatedOn = setActionValueinfo.CreatedOn;
                        setActionValue.LayoutRefId = setActionValueinfo.LayoutRefId;
                        setActionValue.QuestionRefId = setActionValueinfo.QuestionRefId;
                        setActionValue.SelectAllowance = setActionValueinfo.SelectAllowance;
                        setActionValue.SelectInfoColumn = setActionValueinfo.SelectInfoColumn;
                        setActionValue.SelectQP = setActionValueinfo.SelectQP;
                        setActionValue.SelectView = setActionValueinfo.SelectView;
                        setActionValue.SetReset = setActionValueinfo.SetReset;
                        setActionValue.Title = setActionValueinfo.Title;
                        setActionValue.UpdatedBy = setActionValueinfo.UpdatedBy;
                        setActionValue.UpdatedOn = setActionValueinfo.UpdatedOn;
                        setActionValue.Value = GetSetActionValueTranslation(setActionValueinfo, defualtLanguage);
                        setActionValue.Conditionitem = setActionValueinfo.Conditionitem;
                        setActionValue.ConCompareMethod = setActionValueinfo.ConCompareMethod;
                        setActionValue.Convalue = setActionValueinfo.Convalue;
                        setActionValue.ConItemSavedValued = setActionValueinfo.ConItemSavedValued;
                        setActionValue.ConvalueSavedValue = setActionValueinfo.ConvalueSavedValue;
                        setActionValue.ComFixedText = setActionValueinfo.ComFixedText;
                        setActionValue.Comment = setActionValueinfo.Comment;
                        setActionValue.SetActionValueTypeId = setActionValueinfo.SetActionValueTypeId ?? 0;
                        setActionValue.InfoCoumnFixedText = fixedText == null ? "" : GetSetActionValueFixedTextTranslation(fixedText, defualtLanguage);
                        setActionValue.InfoColumnSavedValue = setActionValueinfo.InfoColumnSavedValue;
                        setActionValue.SelectTargetSavedValue = setActionValueinfo.SelectTargetSavedValue;
                        setActionValue.SetState = setActionValueinfo.SetState;
                        setActionValue.PlanningIDOBC = setActionValueinfo.PlanningIDOBC;
                        setActionValue.Longitude = setActionValueinfo.Longitude;
                        setActionValue.Latitude = setActionValueinfo.Latitude;
                        setActionValue.CopySavedValue = setActionValueinfo.CopySavedValue;
                        setActionValue.SavedValue = setActionValueinfo.SavedValue;
                        setActionValue.CopyRange = setActionValueinfo.CopyRange;
                        setActionValue.StartChar = setActionValueinfo.StartChar;
                        setActionValue.CharLength = setActionValueinfo.CharLength;

                        setActionValuePropertiesList.Add(setActionValue);
                    }

                    return setActionValuePropertiesList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<SetActionValueProperties>();
            }
        }
        public string GetSetActionValueTranslation(SetActionValuePropertiesTranslation setActionValueTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = setActionValueTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == setActionValueTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + setActionValueTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public string GetSetActionValueFixedTextTranslation(SetActionValuePropertiesInfoCoumnFixedTextTranslation setActionValueFixedTextTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = setActionValueFixedTextTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == setActionValueFixedTextTranslation.LanguageName)
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + setActionValueFixedTextTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public ChioceProperties SaveChioceProperties(ChioceProperties ChioceProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(ChioceProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */
                    var LanguageName = _translatorRepository.GetDefaultLanguage(ChioceProperties.LayoutRefId);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = ChioceProperties.ChioceId ?? 0;
                    langrefdetail.QuestionRefId = ChioceProperties.QuestionRefId;
                    langrefdetail.LayoutRefId = ChioceProperties.LayoutRefId;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ChioceProperties_Title;
                    langrefdetail.LangText = ChioceProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = ChioceProperties.CreatedBy;
                    langrefdetail.UpdatedBy = ChioceProperties.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    /* Written By Selvam Thangaraj - Stop */
                    return ChioceProperties;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ChioceProperties UpdateChioceProperties(BOChioceProperties ChioceProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ChioceProperties.SingleOrDefault(IC => IC.ChioceId == ChioceProperties.ChioceId);
                    if (result != null)
                    {
                        var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == result.QuestionRefId);
                        if (Question.ISModification == 1)
                        {
                            Question.ISModification = 2;
                            context.SaveChanges();
                        }
                        result.Title = ChioceProperties.Title;
                        result.Recursiveloop = ChioceProperties.Recursiveloop;
                        result.Save = ChioceProperties.Save;
                        result.Propose = ChioceProperties.Propose;
                        result.Planningfeedback = ChioceProperties.Planningfeedback;
                        result.FeedbackType = ChioceProperties.FeedbackType;
                        result.ConditionValue = ChioceProperties.ConditionValue;
                        result.ConCompareMethod = ChioceProperties.ConCompareMethod;
                        result.ConCompareTo = ChioceProperties.ConCompareTo;
                        result.Infocolumn = ChioceProperties.Infocolumn;
                        result.InfoColumnAction = ChioceProperties.InfoColumnAction;
                        result.FileNumber = ChioceProperties.FileNumber;
                        result.ConSavedValued = ChioceProperties.ConSavedValued;
                        result.comtoSavedValue = ChioceProperties.comtoSavedValue;
                        result.ComFixedText = ChioceProperties.ComFixedText;
                        result.AutoGenerate = ChioceProperties.AutoGenerate;
                        result.SaveValue = ChioceProperties.SaveValue;
                        result.UpdatedBy = ChioceProperties.UpdatedBy;
                        result.UpdatedOn = ChioceProperties.UpdatedOn;
                        result.Planningfeedbackcode = ChioceProperties.Planningfeedbackcode;
                        result.IsDefaultAlaisName = ChioceProperties.IsDefaultAlaisName;
                        result.PartnerCode = ChioceProperties.PartnerCode;
                        result.Saveto = Convert.ToInt32(ChioceProperties.Saveto);
                        result.Comment = ChioceProperties.Comment;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.ChioceId ?? 0;
                        langrefdetail.QuestionRefId = result.QuestionRefId;
                        langrefdetail.LayoutRefId = result.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ChioceProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ChioceProperties> GetChioceProperties(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);

                    var choiceTranslationDetails = context.ChoicePropertiesTranslation.FromSql("GetChoiceProperties @QuestionRefId", questionParam).ToList();
                    List<ChioceProperties> choicePropertiesList = new List<ChioceProperties>();

                    foreach (var choiceinfo in choiceTranslationDetails)
                    {
                        ChioceProperties choice = new ChioceProperties();

                        choice.ChioceId = choiceinfo.ChioceId;
                        choice.ActivitiesRefId = choiceinfo.ActivitiesRefId;
                        choice.CreatedBy = choiceinfo.CreatedBy;
                        choice.CreatedOn = choiceinfo.CreatedOn;
                        choice.FeedbackType = choiceinfo.FeedbackType;
                        choice.LayoutRefId = choiceinfo.LayoutRefId;
                        choice.Planningfeedback = choiceinfo.Planningfeedback;
                        choice.Propose = choiceinfo.Propose;
                        choice.QuestionRefId = choiceinfo.QuestionRefId;
                        choice.Recursiveloop = choiceinfo.Recursiveloop;
                        choice.Save = choiceinfo.Save;
                        choice.Title = GetChoicePropertiesTitle(choiceinfo, defualtLanguage);
                        choice.UpdatedBy = choiceinfo.UpdatedBy;
                        choice.UpdatedOn = choiceinfo.UpdatedOn;
                        choice.ConditionValue = choiceinfo.ConditionValue;
                        choice.ConCompareMethod = choiceinfo.ConCompareMethod;
                        choice.ConCompareTo = choiceinfo.ConCompareTo;
                        choice.Infocolumn = choiceinfo.Infocolumn;
                        choice.InfoColumnAction = choiceinfo.InfoColumnAction;
                        choice.FileNumber = choiceinfo.FileNumber;
                        choice.ConSavedValued = choiceinfo.ConSavedValued;
                        choice.comtoSavedValue = choiceinfo.comtoSavedValue;
                        choice.ComFixedText = choiceinfo.ComFixedText;
                        choice.InfoActionQP = choiceinfo.InfoActionQP;
                        choice.SaveValue = choiceinfo.SaveValue;
                        choice.CopyLayoutRefId = choiceinfo.CopyLayoutRefId;
                        choice.AutoGenerate = choiceinfo.AutoGenerate;
                        choice.Comment = choiceinfo.Comment;
                        choice.Planningfeedbackcode = choiceinfo.Planningfeedbackcode;
                        choice.Saveto = choiceinfo.Saveto;
                        choice.PartnerCode = choiceinfo.PartnerCode;
                        choice.IsDefaultAlaisName = choiceinfo.IsDefaultAlaisName;
                        choicePropertiesList.Add(choice);
                    }
                    return choicePropertiesList;
                }
            }
            catch (Exception ex)
            {
                return new List<ChioceProperties>();
            }
        }
        public string GetChoicePropertiesTitle(ChoicePropertiesTranslation choicePropertiesTranslation, string defaultLanguage)
        {
            var choiceTitle = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                choiceTitle = choicePropertiesTranslation.Belarusian;
            }
            else
            {
                choiceTitle = "";
            }

            if (choiceTitle == null || choiceTitle == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == choicePropertiesTranslation.LanguageName)
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.Belarusian;
                    }
                    else
                    {
                        choiceTitle = "@" + choicePropertiesTranslation.English;
                    }
                }
            }
            return choiceTitle;
        }
        public AddChioceProperties SaveAddChioceProperties(AddChioceProperties AddChioceProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    if (AddChioceProperties.Dynamicp != "Not Set")
                    {
                        var choiceActivityRefId = context.ChioceProperties.Where(x => x.ChioceId == AddChioceProperties.ChioceRefId).Select(x => x.ActivitiesRefId).FirstOrDefault();
                        var activity = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == choiceActivityRefId).FirstOrDefault();
                        if (activity != null)
                        {
                            activity.EmptyFullSolo = true;
                            context.SaveChanges();
                        }
                    }

                    context.Add(AddChioceProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */
                    var LayoutRefId = context.ChioceProperties.Where(x => x.ChioceId == AddChioceProperties.ChioceRefId).Select(x => x.LayoutRefId).FirstOrDefault();
                    var LanguageName = _translatorRepository.GetDefaultLanguage(LayoutRefId);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = AddChioceProperties.AddChioceId ?? 0;
                    langrefdetail.QuestionRefId = AddChioceProperties.QuestionRefId;
                    langrefdetail.LayoutRefId = LayoutRefId;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.AddChioceProperties_Title;
                    langrefdetail.LangText = AddChioceProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = AddChioceProperties.CreatedBy;
                    langrefdetail.UpdatedBy = AddChioceProperties.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    /* Written By Selvam Thangaraj - Stop */

                    return AddChioceProperties;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public AddChioceProperties UpdateAddChioceProperties(AddChioceProperties AddChioceProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Questions.SingleOrDefault(IC => IC.QuestionId == AddChioceProperties.QuestionRefId);
                    if (result != null)
                    {

                        result.Name = AddChioceProperties.Title;

                        context.SaveChanges();

                        if (result.ISModification == 1)
                        {
                            result.ISModification = 2;
                            context.SaveChanges();
                        }
                        if (AddChioceProperties.Dynamicp != "Not Set")
                        {
                            var choiceActivityRefId = context.ChioceProperties.Where(x => x.ChioceId == AddChioceProperties.ChioceRefId).Select(x => x.ActivitiesRefId).FirstOrDefault();
                            var activity = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == choiceActivityRefId).FirstOrDefault();
                            if (activity != null)
                            {
                                activity.EmptyFullSolo = true;
                                context.SaveChanges();
                            }
                        }
                        //AddChioceProperties.Infocolumn = null;
                        AddChioceProperties.InfoColumnAction = null;
                        context.Update(AddChioceProperties);
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LayoutRefId = context.ChioceProperties.Where(x => x.ChioceId == AddChioceProperties.ChioceRefId).Select(x => x.LayoutRefId).FirstOrDefault();
                        var LanguageName = _translatorRepository.GetDefaultLanguage(LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = AddChioceProperties.AddChioceId ?? 0;
                        langrefdetail.QuestionRefId = AddChioceProperties.QuestionRefId;
                        langrefdetail.LayoutRefId = LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.AddChioceProperties_Title;
                        langrefdetail.LangText = AddChioceProperties.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = AddChioceProperties.UpdatedBy;
                        langrefdetail.UpdatedBy = AddChioceProperties.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */
                    }

                    return AddChioceProperties;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<AddChioceProperties> GetAddChioceProperties(int ChioceRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    bool isFeedBackType = false;
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);
                    var choice = context.ChioceProperties.Where(x => x.ChioceId == ChioceRefId).FirstOrDefault();
                    if (choice != null)
                    {
                        if (choice.FeedbackType != "" && choice.FeedbackType != null)
                        {
                            if (choice.Planningfeedbackcode == "" || choice.Planningfeedbackcode == null)
                                isFeedBackType = true;
                        }
                    }

                    SqlParameter choiceRefid = new SqlParameter("@ChioceRefId", ChioceRefId);
                    var addChoiceDetailsPropertiesList = context.AddChiocePropertyTranslations.FromSql("GetAddChioceProperties @ChioceRefId", choiceRefid).OrderBy(x => x.Position).ToList();
                    List<AddChioceProperties> objFinalResult = new List<AddChioceProperties>();
                    foreach (var addChoiceProperties in addChoiceDetailsPropertiesList)
                    {
                        AddChioceProperties objAddChioceProperties = new AddChioceProperties();
                        objAddChioceProperties.AddChioceId = addChoiceProperties.AddChioceId;
                        objAddChioceProperties.ChioceRefId = addChoiceProperties.ChioceRefId;
                        objAddChioceProperties.CreatedBy = addChoiceProperties.CreatedBy;
                        objAddChioceProperties.CreatedOn = addChoiceProperties.CreatedOn;
                        objAddChioceProperties.Dynamicp = addChoiceProperties.Dynamicp;
                        objAddChioceProperties.Endrecursiveloop = addChoiceProperties.Endrecursiveloop;
                        objAddChioceProperties.FileNumber = addChoiceProperties.FileNumber;
                        objAddChioceProperties.Infocolumn = addChoiceProperties.Infocolumn;
                        objAddChioceProperties.InfoColumnAction = addChoiceProperties.InfoColumnAction;
                        objAddChioceProperties.PartCode = addChoiceProperties.PartCode;
                        objAddChioceProperties.PlanningfeedbackCode = addChoiceProperties.PlanningfeedbackCode;
                        objAddChioceProperties.Title = GetAddChiocePropertyTranslation(addChoiceProperties, defualtLanguage);
                        objAddChioceProperties.QuestionRefId = addChoiceProperties.QuestionRefId;
                        objAddChioceProperties.UpdatedBy = addChoiceProperties.UpdatedBy;
                        objAddChioceProperties.UpdatedOn = addChoiceProperties.UpdatedOn;
                        objAddChioceProperties.Comment = addChoiceProperties.Comment;
                        objAddChioceProperties.IsFeedbackType = isFeedBackType;
                        objFinalResult.Add(objAddChioceProperties);
                    }
                    return objFinalResult;
                }
            }
            catch (Exception ex)
            {
                return new List<AddChioceProperties>();
            }
        }
        public List<AddChioceProperties> GetAddChiocePopup(int ChioceRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var addChoiceDetailsPropertiesList = context.AddChioceProperties.Where(x => x.ChioceRefId == ChioceRefId).Select(cp => new AddChioceProperties()
                    {
                        AddChioceId = cp.AddChioceId,
                        ChioceRefId = cp.ChioceRefId,
                        CreatedBy = cp.CreatedBy,
                        CreatedOn = cp.CreatedOn,
                        Endrecursiveloop = cp.Endrecursiveloop,
                        PlanningfeedbackCode = cp.PlanningfeedbackCode,
                        Title = cp.Title,
                        UpdatedBy = cp.UpdatedBy,
                        UpdatedOn = cp.UpdatedOn,
                        QuestionRefId = cp.QuestionRefId,
                        Infocolumn = cp.Infocolumn,
                        InfoColumnAction = cp.InfoColumnAction,
                        FileNumber = cp.FileNumber,
                        PartCode = cp.PartCode,
                        Dynamicp = cp.Dynamicp
                    }).ToList();

                    return addChoiceDetailsPropertiesList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetAddChiocePropertyTranslation(AddChiocePropertyTranslations addChiocePropertyTranslations, string defaultLanguage)
        {
            var addChioceTitle = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                addChioceTitle = addChiocePropertyTranslations.Belarusian;
            }
            else
            {
                addChioceTitle = "";
            }

            if (addChioceTitle == null || addChioceTitle == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.French;
                    }
                    else if (LanguagesEnum.German.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == addChiocePropertyTranslations.LanguageName)
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.Belarusian;
                    }
                    else
                    {
                        addChioceTitle = "@" + addChiocePropertyTranslations.English;
                    }
                }
            }
            return addChioceTitle;
        }
        public List<OBCTypeMaster> OBCTypes(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from L in context.Layout
                                join OBC in context.OBCType
                                on L.LayoutId equals OBC.LayoutRefId
                                join OBCM in context.OBCTypeMaster
                                on OBC.OBCTypeRefId equals OBCM.OBCTypeId
                                where L.LayoutId == Layout_ID
                                select new OBCTypeMaster()
                                {
                                    OBCTypeId = OBCM.OBCTypeId,
                                    OBCTypeDescription = OBCM.OBCTypeDescription,
                                    IsActive = OBCM.IsActive,
                                    CreatedBy = OBCM.CreatedBy,
                                    CreatedOn = OBCM.CreatedOn,
                                    UpdatedBy = OBCM.UpdatedBy,
                                    UpdatedOn = OBCM.UpdatedOn
                                }).ToList();

                    List<OBCTypeMaster> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<AllowancesMaster> GetAllowancesDescription(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var OBCTypeRefId = context.OBCType.Where(y => y.LayoutRefId == Layout_ID).Select(x => x.OBCTypeRefId).FirstOrDefault();
                    var CustomerId = context.Layout.Where(y => y.LayoutId == Layout_ID).Select(x => x.CustomerRefId).FirstOrDefault();
                    var allowanceList = context.Allowances.Where(x => x.LayoutRefId == Layout_ID && !x.IsActive).Select(x => x.AllowancesRefId).ToList();
                    var allowanceMasterList = context.AllowancesMaster.Where(AL => !allowanceList.Contains(AL.AllowancesMasterId) && ((AL.OBCTypeRefId == OBCTypeRefId && AL.CustomerRefId == null) || (AL.OBCTypeRefId == OBCTypeRefId && AL.CustomerRefId == CustomerId))).Select(AL => new AllowancesMaster()
                    {
                        AllowancesMasterId = AL.AllowancesMasterId,
                        AllowancesMasterDescription = AL.AllowancesMasterDescription,
                        AllowancesMasterName = AL.AllowancesMasterDescription + " (" + AL.AllowancesCode + ")",
                        AllowancesCode = AL.AllowancesCode,
                        OBCTypeRefId = AL.OBCTypeRefId,
                        IS_Active = AL.IS_Active,
                        CreatedBy = AL.CreatedBy,
                        CreatedOn = AL.CreatedOn,
                        UpdatedBy = AL.UpdatedBy,
                        UpdatedOn = AL.UpdatedOn
                    }).OrderBy(x => x.AllowancesMasterName).ToList();

                    return allowanceMasterList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<InfoColumns> GetInfoColumns(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from L in context.Layout
                                join IC in context.InfoColumn
                                on L.CustomerRefId equals IC.CustomerRefId
                                where L.LayoutId == Layout_ID
                                select new InfoColumns()
                                {
                                    IC_ID = IC.IC_ID,
                                    CustomerRefId = IC.CustomerRefId,
                                    ICM_Column_Ref_No = IC.ICM_Column_Ref_No,
                                    IC_Column_Name = IC.IC_Column_Name,
                                    IS_Active = IC.IS_Active,
                                    CreatedBy = IC.CreatedBy,
                                    CreatedOn = IC.CreatedOn,
                                    UpdatedBy = IC.UpdatedBy,
                                    UpdatedOn = IC.UpdatedOn
                                }).ToList();

                    List<InfoColumns> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool ISPlanningType(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var data = (from PT in context.PlanningType
                                join PTM in context.PlanningTypeMaster
                                on PT.PlanningTypeRefId equals PTM.PlanningTypeMasterId
                                where PT.LayoutRefId == Layout_ID &&
                                (PTM.PlanningTypeMasterDescription == "Place" || PTM.PlanningTypeMasterDescription == "Product" ||
                                PTM.PlanningTypeMasterDescription == "Job")

                                select new PlanningType()
                                {
                                    PlanningTypeId = PT.PlanningTypeId,
                                    CustomerRefId = PT.CustomerRefId,
                                    LayoutRefId = PT.LayoutRefId,
                                    PlanningTypeRefId = PT.PlanningTypeRefId,
                                    CreatedBy = PT.CreatedBy,
                                    CreatedOn = PT.CreatedOn,
                                    UpdatedBy = PT.UpdatedBy,
                                    UpdatedOn = PT.UpdatedOn
                                }).ToList();

                    bool returnList;
                    return returnList = data.Any() ? true : false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<AliasNameDetails> GetAliasNameDetails(int activitiesId, int layoutId, string Code)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    SqlParameter sqlparam = new SqlParameter("@LayoutRefId", layoutId);
                    var aliasNameDetails = context.AliasNameDetails.FromSql("GetUsedAliasNameDetails @LayoutRefId", sqlparam).ToList();
                    var aliasNameDetailIds = aliasNameDetails.Select(x => x.AliasNameDetailsId).ToList();
                    var alaisname = context.AliasNameDetails.Where(x => (Code == "A" ? (x.AliasName.Trim().Length > 0 && x.LayoutRefId == layoutId && x.IsTempData == false) : (x.AliasName.Trim().Length > 0 && aliasNameDetailIds.Contains(x.AliasNameDetailsId)))).Distinct().ToList(); //x.ActivitiesRefId == activitiesId &&
                    return alaisname;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<AliasNameDetails> GetAliasNameDetailsForReadOut(int activitiesId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    SqlParameter sqlparam = new SqlParameter("@LayoutRefId", layoutId);
                    var aliasNameDetails = context.AliasNameDetails.FromSql("GetUsedAliasNameDetails @LayoutRefId", sqlparam).ToList();
                    var aliasNameDetailIds = aliasNameDetails.Select(x => x.AliasNameDetailsId).ToList();
                    var alaisname = context.AliasNameDetails.Where(x => (x.AliasName.Trim().Length > 0 && x.LayoutRefId == layoutId) || (x.AliasName.Trim().Length > 0 && aliasNameDetailIds.Contains(x.AliasNameDetailsId))).ToList(); //x.ActivitiesRefId == activitiesId &&
                    return alaisname;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<conditionCompareMethod> GetConditionCompareMethodWithOutSavedVal()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var commethod = context.ConditionCompareMethod.Where(x => x.IsSavedValue == false).ToList();

                    return commethod;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<conditionCompareMethod> GetConditionCompareMethodWithSavedVal()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var commethod = context.ConditionCompareMethod.ToList();

                    return commethod;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<conditionCompareMethod> GetConditionActionCompareMethodWithSavedVal()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var commethod = context.ConditionCompareMethod.Where(x => x.IsConditionalSavedValue == true).ToList();

                    return commethod;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<conditionCompareMethod> GetConditionActionCompareMethodWithOutSavedVal()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var commethod = context.ConditionCompareMethod.Where(x => x.IsConditionalSavedValue == false).ToList();

                    return commethod;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<conditionCompareTo> GetConditionCompareTo()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.ConditionCompareTo.ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<conditionCompareTo> GetConditionCompareToSavedValue()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.ConditionCompareTo.Where(x => x.IsSavedValue == true).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<conditionCompareTo> GetConditionCompareToNotSavedValue()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.ConditionCompareTo.Where(x => x.IsSavedValue == false).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<conditionvalue> GetConditionvalue(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var oBCType = context.OBCType.Where(x => x.LayoutRefId == layoutId).Select(x => x.OBCTypeRefId).FirstOrDefault();
                    if (oBCType == (int)EnumOBCType.TX_GO_FLEX)
                        return context.Conditionvalue.Where(x => x.IsFlexValue == true).ToList();
                    else
                        return context.Conditionvalue.ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<InfoColumnAction> GetInfoColumnAction()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.InfoColumnAction.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public AliasNameDetails SaveAliasNameDetails(AliasNameDetails AliasNameDetails)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    // var aliasmodal = context.AliasNameDetails.Where(x => x.LayoutRefId == AliasNameDetails.LayoutRefId && x.AliasName == AliasNameDetails.AliasName && x.PropertyName == AliasNameDetails.PropertyName).FirstOrDefault();
                    var aliasmodal = context.AliasNameDetails.Where(x => x.LayoutRefId == AliasNameDetails.LayoutRefId && x.AliasName == AliasNameDetails.AliasName).FirstOrDefault();
                    if (aliasmodal == null)
                    {
                        context.Add(AliasNameDetails);
                        context.SaveChanges();
                        return AliasNameDetails;
                    }
                    else
                        return aliasmodal;

                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public AliasNameDetails UpdateAliasNameDetails(int AliasNameDetailsId, int PropertyTableId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.AliasNameDetails.SingleOrDefault(IC => IC.AliasNameDetailsId == AliasNameDetailsId);
                    if (result != null)
                    {
                        result.PropertyTableId = PropertyTableId;
                        context.SaveChanges();
                    }
                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetActivitiesRegistration(int ActivitiesRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var activities = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == ActivitiesRefId).FirstOrDefault();
                    return activities.ActivitiesRegistrationName;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteAliasName(int savevalue)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ChioceProperties.Where(x => x.Save == savevalue);
                    if (result == null)
                    {
                        var AliasNamedata = context.AliasNameDetails.FirstOrDefault(x => x.AliasNameDetailsId == savevalue);
                        context.AliasNameDetails.Remove(AliasNamedata);
                        context.SaveChanges();
                        return "Deleted";
                    }
                    else
                    {
                        //context.Update(InfoColumn);
                        //context.SaveChanges();
                        return "Not Deleted";
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string DeleteAddchioce(int AddChioceId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.AddChioceProperties.FirstOrDefault(x => x.AddChioceId == AddChioceId);
                    if (result != null)
                    {
                        var questions = context.Questions.Where(x => x.QuestionId == result.QuestionRefId).FirstOrDefault();
                        if (questions != null)
                        {
                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == questions.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                            if (IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green)
                            {
                                questions.ISModification = (int)EnumCopyColorCode.Red;
                                context.SaveChanges();
                            }
                            else
                            {
                                /* Written By Selvam Thangaraj - Go */

                                LangRefDetail langrefdetail = new LangRefDetail();
                                langrefdetail.RefId = result.QuestionRefId;
                                langrefdetail.LayoutRefId = questions.LayoutRefId;
                                langrefdetail.CustomerRefId = null;
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;

                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                langrefdetail = new LangRefDetail();
                                langrefdetail.RefId = result.AddChioceId ?? 0;
                                langrefdetail.LayoutRefId = questions.LayoutRefId;
                                langrefdetail.CustomerRefId = null;
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.AddChioceProperties_Title;

                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                context.AddChioceProperties.Remove(result);
                                context.SaveChanges();

                                context.Questions.Remove(questions);
                                context.SaveChanges();

                                /* Written By Selvam Thangaraj - Stop */
                            }
                        }
                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<ConditionItem> GetConditionItemvalue(int LayoutRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    int[] layoutids = new int[] { LayoutRefId, 0 };
                    return context.ConditionItem.Where(x => layoutids.Contains(x.LayoutRefId)).ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ConditionalProperties SaveConditionalProperties(ConditionalProperties ConditionalProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(ConditionalProperties);
                    context.SaveChanges();



                    return ConditionalProperties;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ConditionalProperties UpdateConditionalProperties(ConditionalProperties ConditionalProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var result = context.ConditionalProperties.SingleOrDefault(IC => IC.ConditionalID == ConditionalProperties.ConditionalID);
                    if (result != null)
                    {

                        result.Name = ConditionalProperties.Name;
                        result.ConditionValue = ConditionalProperties.ConditionValue;
                        result.ConCompareMethod = ConditionalProperties.ConCompareMethod;
                        result.ConCompareTo = ConditionalProperties.ConCompareTo;
                        result.ConSavedValued = ConditionalProperties.ConSavedValued;
                        result.comtoSavedValue = ConditionalProperties.comtoSavedValue;
                        result.ComFixedText = ConditionalProperties.ComFixedText;
                        result.Comment = ConditionalProperties.Comment;
                        result.Repeat = ConditionalProperties.Repeat;
                        result.UpdatedBy = ConditionalProperties.UpdatedBy;
                        result.UpdatedOn = ConditionalProperties.UpdatedOn;
                        context.SaveChanges();

                    }

                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ConditionalProperties> GetConditionalProperties(int questionRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.ConditionalProperties.Where(x => x.QuestionRefId == questionRefId).ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        //public string GetSpecificQuestionTree(int QuestionID, int newParentID)
        //{
        //    try
        //    {
        //        using (var context = new Creatis_Context())
        //        {
        //            SqlParameter param1 = new SqlParameter("@pQuestionId", QuestionID);
        //            var QuestionTree = context.Questions.FromSql("GetSpecificQuestionTree @pQuestionId", param1).ToList();


        //            Questions modalque = new Questions();
        //            ConditionalProperties modalcaq = new ConditionalProperties();
        //            InfomessageProperties modalimq = new InfomessageProperties();
        //            TextMessageProperties modaltmq = new TextMessageProperties();
        //            SetActionValueProperties modalsaq = new SetActionValueProperties();
        //            VariableListProperties modalvlq = new VariableListProperties();
        //            PlanningselectProperties modalpsq = new PlanningselectProperties();
        //            CheckoffProperties modalcoq = new CheckoffProperties();
        //            DocumetScanProperties modaldsq = new DocumetScanProperties();
        //            EntryProperties modaleq = new EntryProperties();
        //            ChioceProperties modalcq = new ChioceProperties();
        //            AddChioceProperties modalacq = new AddChioceProperties();
        //            AliasNameDetails modaland = new AliasNameDetails();
        //            int firstnodeinsereted = 0;

        //            Flex modalf = new Flex();
        //            ExtraInfo modalei = new ExtraInfo();
        //            Pallets modalp = new Pallets();
        //            Problems modalplm = new Problems();

        //            Signatures modals = new Signatures();

        //            if (QuestionTree.Any())
        //            {
        //                List<int> AddedQuestionsIds = new List<int>();
        //                foreach (var questions in QuestionTree)
        //                {
        //                    modalque = new Questions();
        //                    //if () { }
        //                    firstnodeinsereted = firstnodeinsereted + 1;
        //                    var aliasNameList = context.AliasNameDetails.Where(x => x.LayoutRefId == questions.LayoutRefId).ToList();

        //                    modalque.ActivitiesRefId = questions.ActivitiesRefId;
        //                    modalque.CreatedBy = questions.CreatedBy;
        //                    modalque.CreatedOn = DateTime.Now;
        //                    modalque.LayoutRefId = questions.LayoutRefId;
        //                    modalque.Name = questions.Name;
        //                    if (firstnodeinsereted == 1)
        //                    {
        //                        modalque.ParentId = newParentID;
        //                    }
        //                    else
        //                    {
        //                        modalque.ParentId = questions.ParentId;
        //                    }
        //                    modalque.TransporttypeRefId = questions.TransporttypeRefId;
        //                    modalque.UpdatedBy = questions.CreatedBy;
        //                    modalque.UpdatedOn = DateTime.Now;
        //                    modalque.PropertyName = questions.PropertyName;
        //                    modalque.CopyLayoutRefId = questions.QuestionId;
        //                    modalque.ISModification = 0;
        //                    context.Questions.Add(modalque);
        //                    context.SaveChanges();
        //                    AddedQuestionsIds.Add(modalque.QuestionId);

        //                    var PropertyName = questions.PropertyName.Replace(" ", "_").Replace("/", "_");
        //                    if (PropertyName == EnumToolBoxItems.Flex.ToString())
        //                    {
        //                        var targetf = context.Flex.Where(x => x.QuestionRefID == questions.QuestionId).FirstOrDefault();
        //                        if (targetf != null)
        //                        {
        //                            modalf = new Flex();

        //                            modalf.ActivityRefID = targetf.ActivityRefID;
        //                            modalf.LayoutRefID = targetf.LayoutRefID;
        //                            modalf.Title = targetf.Title;
        //                            modalf.AlternativetextonPlacelevel = targetf.AlternativetextonPlacelevel;
        //                            modalf.AlternativetextonJoblevel = targetf.AlternativetextonJoblevel;
        //                            modalf.Alternativetextonproductlevel = targetf.Alternativetextonproductlevel;
        //                            modalf.Status = targetf.Status;
        //                            modalf.CreatedBy = targetf.CreatedBy;
        //                            modalf.CreatedOn = DateTime.Now;
        //                            modalf.UpdatedBy = targetf.CreatedBy;
        //                            modalf.UpdatedOn = DateTime.Now;
        //                            modalf.QuestionRefID = modalque.QuestionId;

        //                            context.Flex.Add(modalf);
        //                            context.SaveChanges();
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Extra_Info.ToString())
        //                    {
        //                        var targeteiList = context.ExtraInfo.Where(x => x.QuestionRefID == questions.QuestionId).ToList();
        //                        foreach (var targetei in targeteiList)
        //                        {
        //                            modalei = new ExtraInfo();

        //                            modalei.ActivityRefID = targetei.ActivityRefID;
        //                            modalei.LayoutRefID = targetei.LayoutRefID;
        //                            modalei.MaskText = targetei.MaskText;
        //                            modalei.Format = targetei.Format;
        //                            modalei.Size = targetei.Size;
        //                            modalei.RegEx = targetei.RegEx;
        //                            modalei.PartnerCode = targetei.PartnerCode;
        //                            modalei.PlaceLevel = targetei.PlaceLevel;
        //                            modalei.JobLevel = targetei.JobLevel;
        //                            modalei.ProductLevel = targetei.ProductLevel;
        //                            modalei.PlanningFeedbackcode = targetei.PlanningFeedbackcode;
        //                            modalei.CreatedBy = targetei.CreatedBy;
        //                            modalei.CreatedOn = DateTime.Now;
        //                            modalei.UpdatedBy = targetei.CreatedBy;
        //                            modalei.UpdatedOn = DateTime.Now;
        //                            modalei.QuestionRefID = modalque.QuestionId;

        //                            context.ExtraInfo.Add(modalei);
        //                            context.SaveChanges();
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Pallets.ToString())
        //                    {
        //                        var targetpList = context.Pallets.Where(x => x.QuestionRefID == questions.QuestionId).ToList();
        //                        foreach (var targetp in targetpList)
        //                        {
        //                            modalp = new Pallets();

        //                            modalp.ActivityRefID = targetp.ActivityRefID;
        //                            modalp.LayoutRefID = targetp.LayoutRefID;
        //                            modalp.PartnerCode = targetp.PartnerCode;
        //                            modalp.ScanLevel = targetp.ScanLevel;
        //                            modalp.PlaceLevel = targetp.PlaceLevel;
        //                            modalp.JobLevel = targetp.JobLevel;
        //                            modalp.ProductLevel = targetp.ProductLevel;
        //                            modalp.PlanningFeedbackcode = targetp.PlanningFeedbackcode;
        //                            modalp.CreatedBy = targetp.CreatedBy;
        //                            modalp.CreatedOn = DateTime.Now;
        //                            modalp.UpdatedBy = targetp.CreatedBy;
        //                            modalp.UpdatedOn = DateTime.Now;
        //                            modalp.QuestionRefID = modalque.QuestionId;

        //                            context.Pallets.Add(modalp);
        //                            context.SaveChanges();
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Problem.ToString())
        //                    {
        //                        var targetplmList = context.Problems.Where(x => x.QuestionRefID == questions.QuestionId).ToList();
        //                        foreach (var targetplm in targetplmList)
        //                        {
        //                            modalplm = new Problems();

        //                            modalplm.ActivityRefID = targetplm.ActivityRefID;
        //                            modalplm.LayoutRefID = targetplm.LayoutRefID;
        //                            modalplm.MaskText = targetplm.MaskText;
        //                            modalplm.Format = targetplm.Format;
        //                            modalplm.Size = targetplm.Size;
        //                            modalplm.RegEx = targetplm.RegEx;
        //                            modalplm.PartnerCode = targetplm.PartnerCode;
        //                            modalplm.PlaceLevel = targetplm.PlaceLevel;
        //                            modalplm.JobLevel = targetplm.JobLevel;
        //                            modalplm.ProductLevel = targetplm.ProductLevel;
        //                            modalplm.PlanningFeedbackcode = targetplm.PlanningFeedbackcode;
        //                            modalplm.CreatedBy = targetplm.CreatedBy;
        //                            modalplm.CreatedOn = DateTime.Now;
        //                            modalplm.UpdatedBy = targetplm.CreatedBy;
        //                            modalplm.UpdatedOn = DateTime.Now;
        //                            modalplm.QuestionRefID = modalque.QuestionId;

        //                            context.Problems.Add(modalplm);
        //                            context.SaveChanges();
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Signature.ToString())
        //                    {
        //                        var targetsList = context.Signatures.Where(x => x.QuestionRefID == questions.QuestionId).ToList();
        //                        foreach (var targets in targetsList)
        //                        {
        //                            modals = new Signatures();

        //                            modals.ActivityRefID = modals.ActivityRefID;
        //                            modals.LayoutRefID = modals.LayoutRefID;
        //                            modals.PlaceLevel = targets.PlaceLevel;
        //                            modals.JobLevel = targets.JobLevel;
        //                            modals.CreatedBy = modals.CreatedBy;
        //                            modals.CreatedOn = DateTime.Now;
        //                            modals.UpdatedBy = modals.CreatedBy;
        //                            modals.UpdatedOn = DateTime.Now;
        //                            modals.QuestionRefID = modalque.QuestionId;

        //                            context.Signatures.Add(modals);
        //                            context.SaveChanges();
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Conditional_Action.ToString())
        //                    {
        //                        var conditionalPropertiesList = context.ConditionalProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
        //                        var targetcaq = conditionalPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
        //                        if (targetcaq != null)
        //                        {
        //                            modalcaq = new ConditionalProperties();

        //                            modalcaq.Name = targetcaq.Name;
        //                            modalcaq.ActivitiesRefId = targetcaq.ActivitiesRefId;
        //                            modalcaq.LayoutRefId = targetcaq.LayoutRefId;
        //                            modalcaq.CreatedBy = targetcaq.CreatedBy;
        //                            modalcaq.CreatedOn = DateTime.Now;
        //                            modalcaq.UpdatedBy = targetcaq.CreatedBy;
        //                            modalcaq.UpdatedOn = DateTime.Now;
        //                            modalcaq.ConditionValue = targetcaq.ConditionValue;
        //                            modalcaq.ConCompareMethod = targetcaq.ConCompareMethod;
        //                            modalcaq.ConCompareTo = targetcaq.ConCompareTo;
        //                            modalcaq.ConSavedValued = targetcaq.ConSavedValued;
        //                            modalcaq.comtoSavedValue = targetcaq.comtoSavedValue;
        //                            modalcaq.ComFixedText = targetcaq.ComFixedText;
        //                            modalcaq.QuestionRefId = modalque.QuestionId;

        //                            context.ConditionalProperties.Add(modalcaq);
        //                            context.SaveChanges();
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Info_Message.ToString())
        //                    {
        //                        var infomessagePropertiesList = context.InfomessageProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
        //                        var targetimq = infomessagePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
        //                        if (targetimq != null)
        //                        {
        //                            modalimq = new InfomessageProperties();

        //                            modalimq.ActivitiesRefId = targetimq.ActivitiesRefId;
        //                            modalimq.CreatedBy = targetimq.CreatedBy;
        //                            modalimq.CreatedOn = DateTime.Now;
        //                            modalimq.Fontstyle = targetimq.Fontstyle;
        //                            modalimq.InfoMessageIconTypeRefId = targetimq.InfoMessageIconTypeRefId;
        //                            modalimq.LayoutRefId = targetimq.LayoutRefId;
        //                            modalimq.Messagecontent = targetimq.Messagecontent;
        //                            modalimq.Messagetype = targetimq.Messagetype;
        //                            modalimq.Name = targetimq.Name;
        //                            modalimq.QuestionRefId = modalque.QuestionId;
        //                            modalimq.Title = targetimq.Title;
        //                            modalimq.Titlestyle = targetimq.Titlestyle;
        //                            modalimq.UpdatedBy = targetimq.UpdatedBy;
        //                            modalimq.UpdatedOn = DateTime.Now;
        //                            modalimq.Comment = targetimq.Comment;
        //                            modalimq.Conditionitem = targetimq.Conditionitem;
        //                            modalimq.ConCompareMethod = targetimq.ConCompareMethod;
        //                            modalimq.Convalue = targetimq.Convalue;
        //                            modalimq.ConItemSavedValued = targetimq.ConItemSavedValued;
        //                            modalimq.ConvalueSavedValue = targetimq.ConvalueSavedValue;
        //                            modalimq.ComFixedText = targetimq.ComFixedText;

        //                            context.InfomessageProperties.Add(modalimq);
        //                            context.SaveChanges();

        //                            /* Written By Selvam Thangaraj - Go */
        //                            var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == modalimq.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

        //                            LangRefDetail langrefdetail = new LangRefDetail();
        //                            langrefdetail.RefId = modalimq.InfomessageId ?? 0;
        //                            langrefdetail.LayoutRefId = modalimq.LayoutRefId;
        //                            langrefdetail.CustomerRefId = null;
        //                            langrefdetail.LangRefColTableId = (int)LangRefColTable.InfomessageProperties_Title;
        //                            langrefdetail.LangText = modalimq.Title ?? "";
        //                            langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
        //                            langrefdetail.CreatedBy = modalimq.CreatedBy;
        //                            langrefdetail.UpdatedBy = modalimq.CreatedBy;
        //                            _translatorRepository.SaveLangDetail(langrefdetail);

        //                            if (modalimq.Messagecontent != null && modalimq.Messagecontent.Trim().Length > 0)
        //                            {
        //                                langrefdetail.LangRefColTableId = (int)LangRefColTable.InfoMessageContentDetail_MessageContent;
        //                                langrefdetail.LangText = modalimq.Messagecontent ?? "";
        //                                _translatorRepository.SaveLangDetail(langrefdetail);
        //                            }

        //                            /* Written By Selvam Thangaraj - Stop */
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Text_Message.ToString())
        //                    {
        //                        var textMessagePropertiesList = context.TextMessageProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
        //                        var targettmq = textMessagePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
        //                        if (targettmq != null)
        //                        {
        //                            modaltmq = new TextMessageProperties();

        //                            modaltmq.ActivitiesRefId = targettmq.ActivitiesRefId;
        //                            modaltmq.CreatedBy = targettmq.CreatedBy;
        //                            modaltmq.CreatedOn = DateTime.Now;
        //                            modaltmq.LayoutRefId = targettmq.LayoutRefId;
        //                            modaltmq.Messagecontent = targettmq.Messagecontent;
        //                            modaltmq.Name = targettmq.Name;
        //                            modaltmq.QuestionRefId = modalque.QuestionId;
        //                            modaltmq.SendMethod = targettmq.SendMethod;
        //                            modaltmq.UpdatedBy = targettmq.CreatedBy;
        //                            modaltmq.UpdatedOn = DateTime.Now;
        //                            modaltmq.Comment = targettmq.Comment;
        //                            modaltmq.Conditionitem = targettmq.Conditionitem;
        //                            modaltmq.ConCompareMethod = targettmq.ConCompareMethod;
        //                            modaltmq.Convalue = targettmq.Convalue;
        //                            modaltmq.ConItemSavedValued = targettmq.ConItemSavedValued;
        //                            modaltmq.ConvalueSavedValue = targettmq.ConvalueSavedValue;
        //                            modaltmq.ComFixedText = targettmq.ComFixedText;

        //                            context.TextMessageProperties.Add(modaltmq);
        //                            context.SaveChanges();

        //                            /* Written By Selvam Thangaraj - Go */
        //                            var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == modaltmq.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

        //                            LangRefDetail langrefdetail = new LangRefDetail();
        //                            langrefdetail.RefId = modaltmq.TextMessageId ?? 0;
        //                            langrefdetail.LayoutRefId = modaltmq.LayoutRefId;
        //                            langrefdetail.CustomerRefId = null;
        //                            langrefdetail.LangRefColTableId = (int)LangRefColTable.TextMessageProperties_Messagecontent;
        //                            langrefdetail.LangText = modaltmq.Messagecontent ?? "";
        //                            langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
        //                            langrefdetail.CreatedBy = modaltmq.CreatedBy;
        //                            langrefdetail.UpdatedBy = modaltmq.CreatedBy;

        //                            _translatorRepository.SaveLangDetail(langrefdetail);

        //                            /* Written By Selvam Thangaraj - Stop */
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Set_Action___Value.ToString())
        //                    {
        //                        var setActionValuePropertiesList = context.SetActionValueProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
        //                        var targetsaq = setActionValuePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
        //                        if (targetsaq != null)
        //                        {
        //                            modalsaq = new SetActionValueProperties();

        //                            modalsaq.ActivitiesRefId = targetsaq.ActivitiesRefId;
        //                            modalsaq.AllowanceName = targetsaq.AllowanceName;
        //                            modalsaq.CancelcurrentQP = targetsaq.CancelcurrentQP;
        //                            modalsaq.CreatedBy = targetsaq.CreatedBy;
        //                            modalsaq.CreatedOn = DateTime.Now;
        //                            modalsaq.InfoColumnName = targetsaq.InfoColumnName;
        //                            modalsaq.LayoutRefId = targetsaq.LayoutRefId;
        //                            modalsaq.NextQpName = targetsaq.NextQpName;
        //                            modalsaq.NextViewName = targetsaq.NextViewName;
        //                            modalsaq.QuestionRefId = modalque.QuestionId;
        //                            modalsaq.SelectAllowance = targetsaq.SelectAllowance;
        //                            modalsaq.SelectInfoColumn = targetsaq.SelectInfoColumn;
        //                            modalsaq.SelectQP = targetsaq.SelectQP;
        //                            modalsaq.SelectView = targetsaq.SelectView;
        //                            modalsaq.SetReset = targetsaq.SetReset;
        //                            modalsaq.Title = targetsaq.Title;
        //                            modalsaq.UpdatedBy = targetsaq.CreatedBy;
        //                            modalsaq.UpdatedOn = DateTime.Now;
        //                            modalsaq.Value = targetsaq.Value;
        //                            modalsaq.Comment = targetsaq.Comment;
        //                            modalsaq.Conditionitem = targetsaq.Conditionitem;
        //                            modalsaq.ConCompareMethod = targetsaq.ConCompareMethod;
        //                            modalsaq.Convalue = targetsaq.Convalue;
        //                            modalsaq.ConItemSavedValued = targetsaq.ConItemSavedValued;
        //                            modalsaq.ConvalueSavedValue = targetsaq.ConvalueSavedValue;
        //                            modalsaq.ComFixedText = targetsaq.ComFixedText;

        //                            context.SetActionValueProperties.Add(modalsaq);
        //                            context.SaveChanges();
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Variable_List.ToString())
        //                    {
        //                        var variableListPropertiesList = context.VariableListProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
        //                        var targetvlq = variableListPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
        //                        if (targetvlq != null)
        //                        {
        //                            modalvlq = new VariableListProperties();

        //                            modalvlq.ActivitiesRefId = targetvlq.ActivitiesRefId;
        //                            modalvlq.CreatedBy = targetvlq.CreatedBy;
        //                            modalvlq.CreatedOn = DateTime.Now;
        //                            modalvlq.LayoutRefId = targetvlq.LayoutRefId;
        //                            modalvlq.Other = targetvlq.Other;
        //                            modalvlq.QuestionRefId = modalque.QuestionId;
        //                            modalvlq.Title = targetvlq.Title;
        //                            modalvlq.UpdatedBy = targetvlq.CreatedBy;
        //                            modalvlq.UpdatedOn = DateTime.Now;
        //                            modalvlq.VarListID = targetvlq.VarListID;
        //                            modalvlq.Comment = targetvlq.Comment;
        //                            modalvlq.ConditionItemValue = targetvlq.ConditionItemValue;
        //                            modalvlq.CompareMethod = targetvlq.CompareMethod;
        //                            modalvlq.CompareTo = targetvlq.CompareTo;
        //                            modalvlq.CompareToSavedValue = targetvlq.CompareToSavedValue;
        //                            modalvlq.ConditionSavedValue = targetvlq.ConditionSavedValue;
        //                            modalvlq.CompareToFixedText = targetvlq.CompareToFixedText;

        //                            context.VariableListProperties.Add(modalvlq);
        //                            context.SaveChanges();

        //                            /* Written By Selvam Thangaraj - Go */
        //                            var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == modalvlq.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

        //                            LangRefDetail langrefdetail = new LangRefDetail();
        //                            langrefdetail.RefId = modalvlq.VariableListId ?? 0;
        //                            langrefdetail.LayoutRefId = modalvlq.LayoutRefId;
        //                            langrefdetail.CustomerRefId = null;
        //                            langrefdetail.LangRefColTableId = (int)LangRefColTable.VariableListProperties_Title;
        //                            langrefdetail.LangText = modalvlq.Title ?? "";
        //                            langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
        //                            langrefdetail.CreatedBy = modalvlq.CreatedBy;
        //                            langrefdetail.UpdatedBy = modalvlq.CreatedBy;

        //                            _translatorRepository.SaveLangDetail(langrefdetail);

        //                            /* Written By Selvam Thangaraj - Stop */
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Planning_Select.ToString())
        //                    {
        //                        var planningselectPropertiesList = context.PlanningselectProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
        //                        var targetpsq = planningselectPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
        //                        if (targetpsq != null)
        //                        {
        //                            modalpsq = new PlanningselectProperties();

        //                            modalpsq.ActivitiesRefId = targetpsq.ActivitiesRefId;
        //                            modalpsq.Chooseactivity = targetpsq.Chooseactivity;
        //                            modalpsq.CreatedBy = targetpsq.CreatedBy;
        //                            modalpsq.CreatedOn = DateTime.Now;
        //                            modalpsq.Fixedvalue = targetpsq.Fixedvalue;
        //                            modalpsq.LayoutRefId = targetpsq.LayoutRefId;
        //                            modalpsq.Other = targetpsq.Other;
        //                            modalpsq.OtherTitle = targetpsq.OtherTitle;
        //                            modalpsq.Planninglevel = targetpsq.Planninglevel;
        //                            modalpsq.Planningreadout = targetpsq.Planningreadout;
        //                            modalpsq.Planningspecific = targetpsq.Planningspecific;
        //                            modalpsq.QP = targetpsq.QP;
        //                            modalpsq.QuestionRefId = modalque.QuestionId;
        //                            modalpsq.Startselecteditem = targetpsq.Startselecteditem;
        //                            modalpsq.Title = targetpsq.Title;
        //                            modalpsq.UpdatedBy = targetpsq.CreatedBy;
        //                            modalpsq.UpdatedOn = DateTime.Now;
        //                            modalpsq.Comment = targetpsq.Comment;

        //                            context.PlanningselectProperties.Add(modalpsq);
        //                            context.SaveChanges();

        //                            /* Written By Selvam Thangaraj - Go */
        //                            var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == modalpsq.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

        //                            LangRefDetail langrefdetail = new LangRefDetail();
        //                            langrefdetail.RefId = modalpsq.PlanningselectId ?? 0;
        //                            langrefdetail.LayoutRefId = modalpsq.LayoutRefId;
        //                            langrefdetail.CustomerRefId = null;
        //                            langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_Title;
        //                            langrefdetail.LangText = modalpsq.Title ?? "";
        //                            langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
        //                            langrefdetail.CreatedBy = modalpsq.CreatedBy;
        //                            langrefdetail.UpdatedBy = modalpsq.CreatedBy;
        //                            _translatorRepository.SaveLangDetail(langrefdetail);

        //                            if (modalpsq.OtherTitle != null && modalpsq.OtherTitle.Trim().Length > 0)
        //                            {
        //                                langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_OtherTitle;
        //                                langrefdetail.LangText = modalpsq.OtherTitle ?? "";
        //                                _translatorRepository.SaveLangDetail(langrefdetail);
        //                            }

        //                            if (modalpsq.Fixedvalue != null && modalpsq.Fixedvalue.Trim().Length > 0)
        //                            {
        //                                langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_Fixedvalue;
        //                                langrefdetail.LangText = modalpsq.Fixedvalue ?? "";
        //                                _translatorRepository.SaveLangDetail(langrefdetail);
        //                            }

        //                            /* Written By Selvam Thangaraj - Stop */
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Check_Off.ToString())
        //                    {
        //                        var checkoffPropertiesList = context.CheckoffProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
        //                        var targetcoq = checkoffPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
        //                        if (targetcoq != null)
        //                        {
        //                            modalcoq = new CheckoffProperties();

        //                            modalcoq.ActivitiesRefId = targetcoq.ActivitiesRefId;
        //                            modalcoq.CreatedBy = targetcoq.CreatedBy;
        //                            modalcoq.CreatedOn = DateTime.Now;
        //                            modalcoq.LayoutRefId = targetcoq.LayoutRefId;
        //                            modalcoq.Name = targetcoq.Name;
        //                            modalcoq.Planninglevel = targetcoq.Planninglevel;
        //                            modalcoq.QuestionRefId = modalque.QuestionId;
        //                            modalcoq.UpdatedBy = targetcoq.CreatedBy;
        //                            modalcoq.UpdatedOn = DateTime.Now;
        //                            modalcoq.Comment = targetcoq.Comment;

        //                            context.CheckoffProperties.Add(modalcoq);
        //                            context.SaveChanges();
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Document_Scan.ToString())
        //                    {
        //                        var documetScanPropertiesList = context.DocumetScanProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
        //                        var targetdsq = documetScanPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
        //                        if (targetdsq != null)
        //                        {
        //                            modaldsq = new DocumetScanProperties();

        //                            modaldsq.ActivitiesRefId = targetdsq.ActivitiesRefId;
        //                            modaldsq.CreatedBy = targetdsq.CreatedBy;
        //                            modaldsq.CreatedOn = DateTime.Now;
        //                            modaldsq.LayoutRefId = targetdsq.LayoutRefId;
        //                            modaldsq.PlanningFeedbackLevel = targetdsq.PlanningFeedbackLevel;
        //                            modaldsq.QuestionRefId = modalque.QuestionId;
        //                            modaldsq.Title = targetdsq.Title;
        //                            modaldsq.UpdatedBy = targetdsq.CreatedBy;
        //                            modaldsq.UpdatedOn = DateTime.Now;
        //                            modaldsq.DocumentName = targetdsq.DocumentName;
        //                            modaldsq.Comment = targetdsq.Comment;
        //                            modaldsq.Conditionitem = targetdsq.Conditionitem;
        //                            modaldsq.ConCompareMethod = targetdsq.ConCompareMethod;
        //                            modaldsq.Convalue = targetdsq.Convalue;
        //                            modaldsq.ConItemSavedValued = targetdsq.ConItemSavedValued;
        //                            modaldsq.ConvalueSavedValue = targetdsq.ConvalueSavedValue;
        //                            modaldsq.ComFixedText = targetdsq.ComFixedText;

        //                            context.DocumetScanProperties.Add(modaldsq);
        //                            context.SaveChanges();

        //                            /* Written By Selvam Thangaraj - Go */
        //                            var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == modaldsq.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

        //                            LangRefDetail langrefdetail = new LangRefDetail();
        //                            langrefdetail.RefId = modaldsq.DocumetScanId ?? 0;
        //                            langrefdetail.LayoutRefId = modaldsq.LayoutRefId;
        //                            langrefdetail.CustomerRefId = null;
        //                            langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumetScanProperties_Title;
        //                            langrefdetail.LangText = modaldsq.Title ?? "";
        //                            langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
        //                            langrefdetail.CreatedBy = modaldsq.CreatedBy;
        //                            langrefdetail.UpdatedBy = modaldsq.CreatedBy;

        //                            _translatorRepository.SaveLangDetail(langrefdetail);

        //                            /* Written By Selvam Thangaraj - Stop */
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Entry.ToString())
        //                    {
        //                        var entryPropertiesList = context.EntryProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
        //                        var targeteq = entryPropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
        //                        if (targeteq != null)
        //                        {
        //                            modaleq = new EntryProperties();

        //                            modaleq.ActivitiesRefId = targeteq.ActivitiesRefId;
        //                            modaleq.CreatedBy = targeteq.CreatedBy;
        //                            modaleq.CreatedOn = DateTime.Now;
        //                            modaleq.EntryType = targeteq.EntryType;
        //                            modaleq.ExactCharacter = targeteq.ExactCharacter;
        //                            modaleq.FeedbackType = targeteq.FeedbackType;
        //                            modaleq.LayoutRefId = targeteq.LayoutRefId;
        //                            modaleq.MaskText = targeteq.MaskText;
        //                            modaleq.MinimumofCharacter = targeteq.MinimumofCharacter;
        //                            modaleq.NotNull = targeteq.NotNull;
        //                            modaleq.Planningfeedback = targeteq.Planningfeedback;
        //                            modaleq.PlanningfeedbackCode = targeteq.PlanningfeedbackCode;
        //                            modaleq.Predefined = targeteq.Predefined;
        //                            modaleq.Propose = targeteq.Propose;
        //                            modaleq.QuestionRefId = modalque.QuestionId;
        //                            modaleq.Regex = targeteq.Regex;
        //                            modaleq.Save = targeteq.Save;
        //                            modaleq.Size = targeteq.Size;
        //                            modaleq.Title = targeteq.Title;
        //                            modaleq.UpdatedBy = targeteq.CreatedBy;
        //                            modaleq.UpdatedOn = DateTime.Now;
        //                            modaleq.Zeronotallowed = targeteq.Zeronotallowed;
        //                            modaleq.Comment = targeteq.Comment;
        //                            modaleq.Conditionitem = targeteq.Conditionitem;
        //                            modaleq.ConCompareMethod = targeteq.ConCompareMethod;
        //                            modaleq.Convalue = targeteq.Convalue;
        //                            modaleq.Infocolumn = targeteq.Infocolumn;
        //                            modaleq.InfoColumnAction = targeteq.InfoColumnAction;
        //                            modaleq.FileNumber = targeteq.FileNumber;
        //                            modaleq.ConItemSavedValued = targeteq.ConItemSavedValued;
        //                            modaleq.ConvalueSavedValue = targeteq.ConvalueSavedValue;
        //                            modaleq.PartnerCode = targeteq.PartnerCode;
        //                            modaleq.SaveValue = targeteq.SaveValue;
        //                            modaleq.DatetimeFormat = targeteq.DatetimeFormat;
        //                            modaleq.AutoGenerate = targeteq.AutoGenerate;
        //                            modaleq.ComFixedText = targeteq.ComFixedText;
        //                            modaleq.CopyLayoutRefId = targeteq.EntryId;

        //                            context.EntryProperties.Add(modaleq);
        //                            context.SaveChanges();

        //                            /* Written By Selvam Thangaraj - Go */
        //                            var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == modaleq.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

        //                            LangRefDetail langrefdetail = new LangRefDetail();
        //                            langrefdetail.RefId = modaleq.EntryId ?? 0;
        //                            langrefdetail.LayoutRefId = modaleq.LayoutRefId;
        //                            langrefdetail.CustomerRefId = null;
        //                            langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_Title;
        //                            langrefdetail.LangText = modaleq.Title ?? "";
        //                            langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
        //                            langrefdetail.CreatedBy = modaleq.CreatedBy;
        //                            langrefdetail.UpdatedBy = modaleq.CreatedBy;

        //                            _translatorRepository.SaveLangDetail(langrefdetail);

        //                            langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_MaskText;
        //                            langrefdetail.LangText = modaleq.MaskText ?? "";
        //                            _translatorRepository.SaveLangDetail(langrefdetail);

        //                            /* Written By Selvam Thangaraj - Stop */


        //                            var targetand = aliasNameList.Where(x => x.PropertyTableId == targeteq.EntryId && x.PropertyName == "Entry").FirstOrDefault();
        //                            if (targetand != null)
        //                            {
        //                                modaland = new AliasNameDetails();

        //                                modaland.LayoutRefId = targetand.LayoutRefId;
        //                                modaland.ActivitiesRefId = targetand.ActivitiesRefId;
        //                                modaland.AliasName = targetand.AliasName;
        //                                modaland.PropertyName = targetand.PropertyName;
        //                                modaland.PropertyTableId = modaleq.EntryId ?? 0;
        //                                modaland.CreatedBy = targetand.CreatedBy;
        //                                modaland.CreatedOn = DateTime.Now;
        //                                modaland.UpdatedBy = targetand.CreatedBy;
        //                                modaland.UpdatedOn = DateTime.Now;

        //                                context.AliasNameDetails.Add(modaland);
        //                                context.SaveChanges();
        //                            }
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Choice.ToString())
        //                    {
        //                        var chiocePropertiesList = context.ChioceProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
        //                        var targetcq = chiocePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
        //                        if (targetcq != null)
        //                        {
        //                            modalcq = new ChioceProperties();

        //                            modalcq.ActivitiesRefId = targetcq.ActivitiesRefId;
        //                            modalcq.CreatedBy = targetcq.CreatedBy;
        //                            modalcq.CreatedOn = DateTime.Now;
        //                            modalcq.FeedbackType = targetcq.FeedbackType;
        //                            modalcq.LayoutRefId = targetcq.LayoutRefId;
        //                            modalcq.Planningfeedback = targetcq.Planningfeedback;
        //                            modalcq.Propose = targetcq.Propose;
        //                            modalcq.QuestionRefId = modalque.QuestionId;
        //                            modalcq.Recursiveloop = targetcq.Recursiveloop;
        //                            modalcq.Save = targetcq.Save;
        //                            modalcq.Title = targetcq.Title;
        //                            modalcq.UpdatedBy = targetcq.CreatedBy;
        //                            modalcq.UpdatedOn = DateTime.Now;
        //                            modalcq.ConditionValue = targetcq.ConditionValue;
        //                            modalcq.ConCompareMethod = targetcq.ConCompareMethod;
        //                            modalcq.ConCompareTo = targetcq.ConCompareTo;
        //                            modalcq.Infocolumn = targetcq.Infocolumn;
        //                            modalcq.InfoColumnAction = targetcq.InfoColumnAction;
        //                            modalcq.FileNumber = targetcq.FileNumber;
        //                            modalcq.ConSavedValued = targetcq.ConSavedValued;
        //                            modalcq.comtoSavedValue = targetcq.comtoSavedValue;
        //                            modalcq.ComFixedText = targetcq.ComFixedText;
        //                            //modalcq.InfoActionQP = targetcq.InfoActionQP;
        //                            modalcq.AutoGenerate = targetcq.AutoGenerate;
        //                            modalcq.SaveValue = targetcq.SaveValue;
        //                            modalcq.CopyLayoutRefId = targetcq.ChioceId;

        //                            context.ChioceProperties.Add(modalcq);
        //                            context.SaveChanges();

        //                            /* Written By Selvam Thangaraj - Go */
        //                            var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == modalcq.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

        //                            LangRefDetail langrefdetail = new LangRefDetail();
        //                            langrefdetail.RefId = modalcq.ChioceId ?? 0;
        //                            langrefdetail.LayoutRefId = modalcq.LayoutRefId;
        //                            langrefdetail.CustomerRefId = null;
        //                            langrefdetail.LangRefColTableId = (int)LangRefColTable.ChioceProperties_Title;
        //                            langrefdetail.LangText = modalcq.Title ?? "";
        //                            langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
        //                            langrefdetail.CreatedBy = modalcq.CreatedBy;
        //                            langrefdetail.UpdatedBy = modalcq.CreatedBy;
        //                            _translatorRepository.SaveLangDetail(langrefdetail);

        //                            /* Written By Selvam Thangaraj - Stop */

        //                            var targetand = aliasNameList.Where(x => x.PropertyTableId == targetcq.ChioceId && x.PropertyName == "Choice").FirstOrDefault();
        //                            if (targetand != null)
        //                            {
        //                                modaland = new AliasNameDetails();

        //                                modaland.LayoutRefId = targetand.LayoutRefId;
        //                                modaland.ActivitiesRefId = targetand.ActivitiesRefId;
        //                                modaland.AliasName = targetand.AliasName;
        //                                modaland.PropertyName = targetand.PropertyName;
        //                                modaland.PropertyTableId = modalcq.ChioceId ?? 0;
        //                                modaland.CreatedBy = targetand.CreatedBy;
        //                                modaland.CreatedOn = DateTime.Now;
        //                                modaland.UpdatedBy = targetand.CreatedBy;
        //                                modaland.UpdatedOn = DateTime.Now;

        //                                context.AliasNameDetails.Add(modaland);
        //                                context.SaveChanges();
        //                            }
        //                        }
        //                    }
        //                    else if (PropertyName == EnumToolBoxItems.Add_Chioce.ToString())
        //                    {
        //                        var addChiocePropertiesList = context.AddChioceProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
        //                        var targetacq = addChiocePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();

        //                        if (targetacq != null)
        //                        {
        //                            var choiceid = context.ChioceProperties.Where(x => x.CopyLayoutRefId == targetacq.ChioceRefId).Select(x => x.ChioceId).FirstOrDefault();

        //                            modalacq = new AddChioceProperties();

        //                            modalacq.ChioceRefId = choiceid ?? 0;
        //                            modalacq.CreatedBy = modalque.CreatedBy;
        //                            modalacq.CreatedOn = DateTime.Now;
        //                            modalacq.Endrecursiveloop = targetacq.Endrecursiveloop;
        //                            modalacq.PlanningfeedbackCode = targetacq.PlanningfeedbackCode;
        //                            modalacq.Title = targetacq.Title;
        //                            modalacq.UpdatedBy = modalque.CreatedBy;
        //                            modalacq.UpdatedOn = DateTime.Now;
        //                            modalacq.QuestionRefId = modalque.QuestionId;
        //                            modalacq.Infocolumn = targetacq.Infocolumn;
        //                            modalacq.InfoColumnAction = targetacq.InfoColumnAction;
        //                            modalacq.FileNumber = targetacq.FileNumber;

        //                            context.AddChioceProperties.Add(modalacq);
        //                            context.SaveChanges();

        //                            /* Written By Selvam Thangaraj - Go */
        //                            var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == questions.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

        //                            LangRefDetail langrefdetail = new LangRefDetail();
        //                            langrefdetail.RefId = modalacq.AddChioceId ?? 0;
        //                            langrefdetail.LayoutRefId = questions.LayoutRefId;
        //                            langrefdetail.CustomerRefId = null;
        //                            langrefdetail.LangRefColTableId = (int)LangRefColTable.AddChioceProperties_Title;
        //                            langrefdetail.LangText = modalacq.Title ?? "";
        //                            langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
        //                            langrefdetail.CreatedBy = modalacq.CreatedBy;
        //                            langrefdetail.UpdatedBy = modalacq.CreatedBy;
        //                            _translatorRepository.SaveLangDetail(langrefdetail);

        //                            /* Written By Selvam Thangaraj - Stop */
        //                        }
        //                    }
        //                }

        //                var questionList = context.Questions.Where(x => AddedQuestionsIds.Contains(x.QuestionId)).ToList();
        //                foreach (var questions in questionList)
        //                {
        //                    var parentid = context.Questions.Where(x => x.CopyLayoutRefId == questions.ParentId).Select(x => x.QuestionId).FirstOrDefault();
        //                    if (parentid > 0)
        //                    {
        //                        questions.ParentId = parentid;
        //                        questions.CopyLayoutRefId = null;
        //                        context.SaveChanges();
        //                    }

        //                }
        //            }


        //            return "Copied";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}
        public string DeleteSpecificQuestionTree(int QuestionID)
        {

            using (var context = new Creatis_Context())
            {
                SqlParameter param1 = new SqlParameter("@pQuestionId", QuestionID);
                var QuestionTree = context.Questions.FromSql("GetSpecificQuestionTree @pQuestionId", param1).ToList();

                if (QuestionTree.Any())
                {
                    var LayoutId = QuestionTree.Where(x => x.LayoutRefId > 0).Select(x => x.LayoutRefId).FirstOrDefault();
                    var IsCopyLayout = context.Layout.Where(x => x.LayoutId == LayoutId).Select(x => x.IsCopyLayout).FirstOrDefault();

                    var result = DeleteQuestions(QuestionTree, IsCopyLayout, 0);

                }
                return "Deleted";
            }

        }
        public string DeleteQuestions(List<Questions> QuestionTree, bool IsCopyLayout, int ActivityId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var QuestionIds = QuestionTree.Select(x => x.QuestionId).Distinct().ToList();
                    QuestionTree = context.Questions.Where(x => QuestionIds.Contains(x.QuestionId)).ToList();
                    foreach (var questions in QuestionTree)
                    {
                        if (!IsCopyLayout || (IsCopyLayout && questions.ISModification == (int)EnumCopyColorCode.Green))
                        {
                            var aliasNameList = context.AliasNameDetails.Where(x => x.LayoutRefId == questions.LayoutRefId).ToList();

                            var PropertyName = questions.PropertyName.Replace(" ", "_").Replace("/", "_");
                            if (PropertyName == EnumToolBoxItems.Flex.ToString())
                            {
                                var targetf = context.Flex.Where(x => x.QuestionRefID == questions.QuestionId).FirstOrDefault();
                                if (targetf != null)
                                {
                                    context.Flex.Remove(targetf);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Smart_Extra_Infop.ToString())
                            {
                                var smartExtraInfoList = context.SmartExtraInfo.Where(x => x.SmartExtraInfoRefID == questions.QuestionId).ToList();
                                if (smartExtraInfoList.Any())
                                {
                                    context.SmartExtraInfo.RemoveRange(smartExtraInfoList);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Smart_Palletsp.ToString())
                            {
                                var smartPalletsList = context.SmartPallets.Where(x => x.SmartPalletsRefID == questions.QuestionId).ToList();
                                if (smartPalletsList.Any())
                                {
                                    context.SmartPallets.RemoveRange(smartPalletsList);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Smart_Problemp.ToString())
                            {
                                var smartProblemsList = context.SmartProblems.Where(x => x.SmartProblemsRefID == questions.QuestionId).ToList();
                                if (smartProblemsList.Any())
                                {
                                    context.SmartProblems.RemoveRange(smartProblemsList);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Extra_Info.ToString())
                            {
                                var targetexlist = context.ExtraInfoProperties.Where(x => x.QuestionRefID == questions.QuestionId).FirstOrDefault();
                                if (targetexlist != null)
                                {
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetexlist.ExtraInfoPropertiesID ?? 0;
                                    langrefdetail.LayoutRefId = targetexlist.LayoutRefID;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnPlaceLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnJobLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnProductLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.ExtraInfoProperties.Remove(targetexlist);
                                    context.SaveChanges();

                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Extra_Infop.ToString())
                            {
                                var targetexlist = context.ExtraInfo.Where(x => x.ExtraInfoRefId == questions.QuestionId).FirstOrDefault();
                                if (targetexlist != null)
                                {

                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetexlist.ExtraInfoID ?? 0;
                                    langrefdetail.LayoutRefId = targetexlist.LayoutRefID;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfo_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfo_MaskText;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.ExtraInfo.Remove(targetexlist);
                                    context.SaveChanges();

                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Pallets.ToString())
                            {
                                var targetplist = context.PalletsProperties.Where(x => x.QuestionRefID == questions.QuestionId).FirstOrDefault();
                                if (targetplist != null)
                                {

                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetplist.PalletsPropertiesID ?? 0;
                                    langrefdetail.LayoutRefId = targetplist.LayoutRefID;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnPlaceLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnJobLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnProductLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.PalletsProperties.Remove(targetplist);
                                    context.SaveChanges();

                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Palletsp.ToString())
                            {
                                var targetplist = context.Pallets.Where(x => x.PalletsRefId == questions.QuestionId).FirstOrDefault();
                                if (targetplist != null)
                                {
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetplist.PalletsID ?? 0;
                                    langrefdetail.LayoutRefId = targetplist.LayoutRefID;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.Pallets_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.Pallets.Remove(targetplist);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Problem.ToString())
                            {
                                var targetplist = context.ProblemsProperties.Where(x => x.QuestionRefID == questions.QuestionId).FirstOrDefault();
                                if (targetplist != null)
                                {
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetplist.ProblemsPropertiesID ?? 0;
                                    langrefdetail.LayoutRefId = targetplist.LayoutRefID;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnPlaceLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnJobLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnProductLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.ProblemsProperties.Remove(targetplist);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Problemp.ToString())
                            {
                                var targetplist = context.Problems.Where(x => x.ProblemsRefId == questions.QuestionId).FirstOrDefault();
                                if (targetplist != null)
                                {
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetplist.ProblemsID ?? 0;
                                    langrefdetail.LayoutRefId = targetplist.LayoutRefID;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.Problems_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.Problems_MaskText;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.Problems.Remove(targetplist);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Signature.ToString())
                            {
                                var targetslist = context.SignaturesProperties.Where(x => x.QuestionRefID == questions.QuestionId).FirstOrDefault();
                                if (targetslist != null)
                                {
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetslist.SignaturesPropertiesID ?? 0;
                                    langrefdetail.LayoutRefId = targetslist.LayoutRefID;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnPlaceLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnJobLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnProductLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.SignaturesProperties.Remove(targetslist);
                                    context.SaveChanges();

                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Signaturep.ToString())
                            {
                                var targetslist = context.Signatures.Where(x => x.SignaturesRefId == questions.QuestionId).ToList();
                                if (targetslist.Any())
                                {
                                    context.Signatures.RemoveRange(targetslist);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Picture.ToString())
                            {
                                var targetslist = context.PictureProperties.Where(x => x.QuestionRefID == questions.QuestionId).FirstOrDefault();
                                if (targetslist != null)
                                {
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetslist.PicturePropertiesID ?? 0;
                                    langrefdetail.LayoutRefId = targetslist.LayoutRefID;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnPlaceLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnJobLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnProductLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.PictureProperties.RemoveRange(targetslist);
                                    context.SaveChanges();

                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.DocumentScanner.ToString())
                            {
                                var targetslist = context.DocumentScannerProperties.Where(x => x.QuestionRefID == questions.QuestionId).FirstOrDefault();
                                if (targetslist != null)
                                {
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetslist.DocumentScannerPropertiesID ?? 0;
                                    langrefdetail.LayoutRefId = targetslist.LayoutRefID;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnPlaceLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnJobLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnProductLevel;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.DocumentScannerProperties.Remove(targetslist);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.DocumentScannerp.ToString())
                            {
                                var targetslist = context.DocumentScanner.Where(x => x.DocumentScannerRefId == questions.QuestionId).FirstOrDefault();
                                if (targetslist != null)
                                {
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetslist.DocumentScannerID ?? 0;
                                    langrefdetail.LayoutRefId = targetslist.LayoutRefID;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScanner_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.DocumentScanner.Remove(targetslist);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Conditional_Action.ToString())
                            {
                                var targetcaq = context.ConditionalProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetcaq != null)
                                {
                                    var targetand = aliasNameList.Where(x => x.PropertyTableId == targetcaq.ConditionalID && x.PropertyName == "Conditional Action").FirstOrDefault();
                                    bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Conditional Action", targetcaq.ConditionalID ?? 0, ActivityId);
                                    if (isAliasNameDeleted)
                                    {
                                        if (targetand != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == targetcaq.ConditionalID && x.PropertyName == "Conditional Action").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                    }
                                    context.ConditionalProperties.Remove(targetcaq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Info_Message.ToString())
                            {
                                var targetimq = context.InfomessageProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetimq != null)
                                {
                                    var targetand = aliasNameList.Where(x => x.PropertyTableId == targetimq.InfomessageId && x.PropertyName == "Info Message").FirstOrDefault();
                                    bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Info Message", targetimq.InfomessageId ?? 0, ActivityId);
                                    if (isAliasNameDeleted)
                                    {
                                        if (targetand != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == targetimq.InfomessageId && x.PropertyName == "Info Message").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                    }
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetimq.InfomessageId ?? 0;
                                    langrefdetail.LayoutRefId = targetimq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.InfomessageProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.InfoMessageContentDetail_MessageContent;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.InfomessageProperties.Remove(targetimq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Text_Message.ToString())
                            {
                                var targettmq = context.TextMessageProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targettmq != null)
                                {

                                    var targetand = aliasNameList.Where(x => x.PropertyTableId == targettmq.TextMessageId && x.PropertyName == "Text Message").FirstOrDefault();
                                    bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Text Message", targettmq.TextMessageId ?? 0, ActivityId);
                                    if (isAliasNameDeleted)
                                    {
                                        if (targetand != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == targettmq.TextMessageId && x.PropertyName == "Text Message").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                    }
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targettmq.TextMessageId ?? 0;
                                    langrefdetail.LayoutRefId = targettmq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TextMessageProperties_Messagecontent;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.TextMessageProperties.Remove(targettmq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Set_Action___Value.ToString())
                            {
                                var targetsaq = context.SetActionValueProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetsaq != null)
                                {
                                    var targetand = aliasNameList.Where(x => x.PropertyTableId == targetsaq.SetActionValueId && x.PropertyName == "Set Action/value").FirstOrDefault();
                                    bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Set Action/value", targetsaq.SetActionValueId ?? 0, ActivityId);
                                    if (isAliasNameDeleted)
                                    {
                                        if (targetand != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == targetsaq.SetActionValueId && x.PropertyName == "Set Action/value").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetsaq.SetActionValueId ?? 0;
                                    langrefdetail.LayoutRefId = targetsaq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.SetActionValueProperties_Value;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.SetActionValueProperties_InfoColumnFixedText;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.SetActionValueProperties.Remove(targetsaq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Variable_List.ToString())
                            {
                                var targetvlq = context.VariableListProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetvlq != null)
                                {
                                    if (targetvlq.SaveDisplay == 1)
                                    {
                                        var targetand = aliasNameList.Where(x => x.PropertyTableId == targetvlq.VariableListId && x.PropertyName == "Variable List").FirstOrDefault();
                                        bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Variable List", targetvlq.VariableListId ?? 0, ActivityId);
                                        if (isAliasNameDeleted)
                                        {
                                            if (targetand != null)
                                            {
                                                var result = aliasNameList.Where(x => x.PropertyTableId == targetvlq.VariableListId && x.PropertyName == "Variable List").ToList();
                                                context.AliasNameDetails.RemoveRange(result);
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                    if (targetvlq.SaveComment > 0)
                                    {
                                        var targetand = aliasNameList.Where(x => x.PropertyTableId == targetvlq.VariableListId && x.PropertyName == "Variable List").FirstOrDefault();
                                        bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Variable List", targetvlq.VariableListId ?? 0, ActivityId);
                                        if (isAliasNameDeleted)
                                        {
                                            if (targetand != null)
                                            {
                                                var result = aliasNameList.Where(x => x.PropertyTableId == targetvlq.VariableListId && x.PropertyName == "Variable List").ToList();
                                                context.AliasNameDetails.RemoveRange(result);
                                                context.SaveChanges();
                                            }
                                        }
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetvlq.VariableListId ?? 0;
                                    langrefdetail.LayoutRefId = targetvlq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.VariableListProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.VariableListProperties.Remove(targetvlq);
                                    context.SaveChanges();
                                }

                                var targetvarlistq = context.VarListProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetvarlistq != null)
                                {
                                    if (targetvarlistq.SaveDisplay)
                                    {
                                        var targetand = aliasNameList.FirstOrDefault(x => x.PropertyTableId == targetvarlistq.VarListPropertiesId && x.PropertyName == "Variable List");
                                        bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Variable List", targetvarlistq.VarListPropertiesId ?? 0, ActivityId);
                                        if (isAliasNameDeleted && targetand != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == targetvarlistq.VarListPropertiesId && x.PropertyName == "Variable List").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                    }
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetvarlistq.VarListPropertiesId ?? 0;
                                    langrefdetail.LayoutRefId = targetvarlistq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.VariableListProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherTitle;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherMaskText;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */


                                    var readoutlists = context.VarListCommentReadoutDetail.Where(x => x.VarListPropertiesRefId == targetvarlistq.VarListPropertiesId).ToList();
                                    foreach (var readoutDetail in readoutlists)
                                    {

                                        var targetROD = aliasNameList.FirstOrDefault(x => x.PropertyTableId == readoutDetail.VarListCommentReadoutDetailId && x.PropertyName == "Variable List Comment");
                                        bool isAliasNameDeletedRD = DeleteUnAliasNames(targetROD, "Variable List Comment", readoutDetail.VarListCommentReadoutDetailId, ActivityId);
                                        if (isAliasNameDeletedRD && targetROD != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == readoutDetail.VarListCommentReadoutDetailId && x.PropertyName == "Variable List Comment").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                        context.VarListCommentReadoutDetail.Remove(readoutDetail);
                                        context.SaveChanges();
                                    }

                                    var filterlists = context.VarListFilterDetail.Where(x => x.VarListPropertiesRefId == targetvarlistq.VarListPropertiesId).ToList();
                                    if (filterlists.Any())
                                    {
                                        context.VarListFilterDetail.RemoveRange(filterlists);
                                        context.SaveChanges();
                                    }

                                    context.VarListProperties.Remove(targetvarlistq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Planning_Select.ToString())
                            {
                                var targetpsq = context.PlanningselectProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetpsq != null)
                                {
                                    var targetand = aliasNameList.Where(x => x.PropertyTableId == targetpsq.PlanningselectId && x.PropertyName == "Planning Select").FirstOrDefault();
                                    bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Planning Readout", targetpsq.PlanningselectId ?? 0, ActivityId);
                                    if (isAliasNameDeleted)
                                    {
                                        if (targetand != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == targetpsq.PlanningselectId && x.PropertyName == "Planning Select").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                    }
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetpsq.PlanningselectId ?? 0;
                                    langrefdetail.LayoutRefId = targetpsq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_OtherTitle;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PlanningselectProperties_Fixedvalue;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */


                                    var readoutlists = context.PlanningSelectReadoutDetail.Where(x => x.PlanningSelectRefId == targetpsq.PlanningselectId).ToList();
                                    foreach (var readoutDetail in readoutlists)
                                    {

                                        var targetROD = aliasNameList.Where(x => x.PropertyTableId == readoutDetail.PlanningSelectReadoutDetailId && x.PropertyName == "Planning Select").FirstOrDefault();
                                        bool isAliasNameDeletedRD = DeleteUnAliasNames(targetROD, "Planning Readout", readoutDetail.PlanningSelectReadoutDetailId, ActivityId);
                                        if (isAliasNameDeletedRD && targetROD != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == readoutDetail.PlanningSelectReadoutDetailId && x.PropertyName == "Planning Select").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                        context.PlanningSelectReadoutDetail.Remove(readoutDetail);
                                        context.SaveChanges();
                                    }
                                    context.PlanningselectProperties.Remove(targetpsq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Check_Off.ToString())
                            {
                                var targetcoq = context.CheckoffProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetcoq != null)
                                {
                                    context.CheckoffProperties.Remove(targetcoq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Document_Scan.ToString())
                            {
                                var targetdsq = context.DocumetScanProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetdsq != null)
                                {
                                    var targetand = aliasNameList.Where(x => x.PropertyTableId == targetdsq.DocumetScanId && x.PropertyName == "Document Scan").FirstOrDefault();
                                    bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Document Scan", targetdsq.DocumetScanId ?? 0, ActivityId);
                                    if (isAliasNameDeleted)
                                    {
                                        if (targetand != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == targetdsq.DocumetScanId && x.PropertyName == "Document Scan").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                    }
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetdsq.DocumetScanId ?? 0;
                                    langrefdetail.LayoutRefId = targetdsq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumetScanProperties_Title;

                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.DocumetScanProperties.Remove(targetdsq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Type_Of_Document.ToString())
                            {
                                var targetdstq = context.DocumentScanTitle.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetdstq != null)
                                {
                                    context.DocumentScanTitle.Remove(targetdstq);
                                    context.SaveChanges();

                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Add_Document_Scan.ToString())
                            {
                                var targetdstyq = context.DocumentScanType.Where(x => x.QuestionRefID == questions.QuestionId).FirstOrDefault();
                                if (targetdstyq != null)
                                {
                                    context.DocumentScanType.Remove(targetdstyq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Entry.ToString() || PropertyName == EnumToolBoxItems.Entry_From_Variable_List.ToString())
                            {
                                var targeteq = context.EntryProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targeteq != null)
                                {
                                    var targetand = aliasNameList.Where(x => x.PropertyTableId == targeteq.EntryId && x.PropertyName == "Entry").FirstOrDefault();

                                    bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Entry", targeteq.EntryId ?? 0, ActivityId);
                                    if (isAliasNameDeleted)
                                    {
                                        if (targetand != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == targeteq.EntryId && x.PropertyName == "Entry").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targeteq.EntryId ?? 0;
                                    langrefdetail.LayoutRefId = targeteq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_MaskText;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_FixedValue;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.EntryProperties.Remove(targeteq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Planning_Select_Other_Title.ToString())
                            {
                                var targeteq = context.EntryProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targeteq != null)
                                {
                                    var targetand = aliasNameList.Where(x => x.PropertyTableId == targeteq.EntryId && x.PropertyName == "Planning Select Other Title").FirstOrDefault();

                                    bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Planning Select Other Title", targeteq.EntryId ?? 0, ActivityId);
                                    if (isAliasNameDeleted)
                                    {
                                        if (targetand != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == targeteq.EntryId && x.PropertyName == "Planning Select Other Title").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targeteq.EntryId ?? 0;
                                    langrefdetail.LayoutRefId = targeteq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_MaskText;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.EntryProperties_FixedValue;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.EntryProperties.Remove(targeteq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Choice.ToString())
                            {
                                var chiocePropertiesList = context.ChioceProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
                                var targetcq = chiocePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetcq != null)
                                {
                                    var targetand = aliasNameList.Where(x => x.PropertyTableId == targetcq.ChioceId && x.PropertyName == "Choice").FirstOrDefault();
                                    bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Choice", targetcq.ChioceId ?? 0, ActivityId);
                                    if (isAliasNameDeleted)
                                    {
                                        if (targetand != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == targetcq.ChioceId && x.PropertyName == "Choice").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                    }
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetcq.ChioceId ?? 0;
                                    langrefdetail.LayoutRefId = targetcq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ChioceProperties_Title;

                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.ChioceProperties.Remove(targetcq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Add_Chioce.ToString())
                            {
                                var targetacq = context.AddChioceProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();

                                if (targetacq != null)
                                {
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetacq.AddChioceId ?? 0;
                                    langrefdetail.LayoutRefId = questions.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.AddChioceProperties_Title;

                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.AddChioceProperties.Remove(targetacq);
                                    context.SaveChanges();

                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Transport_Type.ToString())
                            {
                                var transportTypePropertiesList = context.TransportTypeProperties.Where(x => x.QuestionRefId == questions.QuestionId).ToList();
                                var targetttq = transportTypePropertiesList.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targetttq != null)
                                {
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetttq.TransportTypeId;
                                    langrefdetail.LayoutRefId = targetttq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TransportTypeProperties_Title;

                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.TransportTypeProperties.Remove(targetttq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Transport_Type_Detail.ToString())
                            {
                                var targetattdq = context.TransportTypePropertiesDetail.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();

                                if (targetattdq != null)
                                {
                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targetattdq.TransportTypeDetailId;
                                    langrefdetail.LayoutRefId = questions.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TransportTypePropertiesDetail_Title;

                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.TransportTypePropertiesDetail.Remove(targetattdq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Get_System_Values.ToString())
                            {
                                var targetgsvq = context.GetSystemValuesProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();

                                if (targetgsvq != null)
                                {
                                    context.GetSystemValuesProperties.Remove(targetgsvq);
                                    context.SaveChanges();
                                }
                            }
                            else if (PropertyName == EnumToolBoxItems.Trailer.ToString() || PropertyName == EnumToolBoxItems.Trailer_From_Variable_List.ToString())
                            {
                                var targettq = context.TrailerProperties.Where(x => x.QuestionRefId == questions.QuestionId).FirstOrDefault();
                                if (targettq != null)
                                {
                                    var targetand = aliasNameList.Where(x => x.PropertyTableId == targettq.TrailerId && x.PropertyName == "Trailer").FirstOrDefault();

                                    bool isAliasNameDeleted = DeleteUnAliasNames(targetand, "Trailer", targettq.TrailerId, ActivityId);
                                    if (isAliasNameDeleted)
                                    {
                                        if (targetand != null)
                                        {
                                            var result = aliasNameList.Where(x => x.PropertyTableId == targettq.TrailerId && x.PropertyName == "Trailer").ToList();
                                            context.AliasNameDetails.RemoveRange(result);
                                            context.SaveChanges();
                                        }
                                    }

                                    /* Written By Selvam Thangaraj - Go */

                                    LangRefDetail langrefdetail = new LangRefDetail();
                                    langrefdetail.RefId = targettq.TrailerId;
                                    langrefdetail.LayoutRefId = targettq.LayoutRefId;
                                    langrefdetail.CustomerRefId = null;
                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TrailerProperties_Title;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TrailerProperties_MaskText;
                                    _translatorRepository.DeleteLangDetail(langrefdetail);

                                    /* Written By Selvam Thangaraj - Stop */

                                    context.TrailerProperties.Remove(targettq);
                                    context.SaveChanges();
                                }
                            }
                        }
                    }
                    foreach (var questions in QuestionTree)
                    {
                        if ((IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green))
                        {
                            questions.ISModification = (int)EnumCopyColorCode.Red;
                            context.SaveChanges();
                        }
                        else
                        {
                            LangRefDetail langrefdetail = new LangRefDetail();
                            langrefdetail.RefId = questions.QuestionId;
                            langrefdetail.LayoutRefId = questions.LayoutRefId;
                            langrefdetail.CustomerRefId = null;
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                            _translatorRepository.DeleteLangDetail(langrefdetail);

                            context.Questions.Remove(questions);
                            context.SaveChanges();
                        }
                    }
                    if (ActivityId > 0)
                    {
                        var deleteresult = context.ActivitiesRegistration.Where(IC => IC.ActivitiesRegistrationId == ActivityId).FirstOrDefault();
                        if (deleteresult != null)
                        {

                            /* Written By Selvam Thangaraj - Go */

                            LangRefDetail langrefdetail = new LangRefDetail();
                            langrefdetail.RefId = deleteresult.ActivitiesRegistrationId;
                            langrefdetail.LayoutRefId = deleteresult.LayoutRefId;
                            langrefdetail.CustomerRefId = null;
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.ActivitiesRegistration_ActivitiesRegistrationName;
                            _translatorRepository.DeleteLangDetail(langrefdetail);

                            /* Written By Selvam Thangaraj - Stop */

                            context.ActivitiesRegistration.Remove(deleteresult);
                            context.SaveChanges();
                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<PlanningTypeMaster> GetPlanningType(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var data = (from PT in context.PlanningType
                                join PTM in context.PlanningTypeMaster
                                on PT.PlanningTypeRefId equals PTM.PlanningTypeMasterId
                                where PT.LayoutRefId == Layout_ID
                                select new PlanningTypeMaster()
                                {
                                    PlanningTypeMasterId = PTM.PlanningTypeMasterId,
                                    PlanningTypeMasterDescription = PTM.PlanningTypeMasterDescription,
                                    IsActive = PTM.IsActive,
                                    CreatedBy = PTM.CreatedBy,
                                    CreatedOn = PTM.CreatedOn,
                                    UpdatedBy = PTM.UpdatedBy,
                                    UpdatedOn = PTM.UpdatedOn
                                }).ToList();

                    List<PlanningTypeMaster> returnList;
                    return returnList = data.ToList();
                }

            }


            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PlanningTypeMaster> GetPlanningTypeLevelMaster()
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var data = context.PlanningTypeMaster.ToList();

                    List<PlanningTypeMaster> returnList;
                    return returnList = data.ToList();
                }

            }


            catch (Exception ex)
            {
                return null;
            }
        }
        public List<boPlanningLevelDrop> PlanningFeedbackLevel(int ActivityRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    string[] planningtypes = new string[] { "Not Set", "Place", "Trip" };
                    string[] PlanningselectPropertiestypes = new string[] { "Product", "Job" };
                    var Planningtypemaster = context.PlanningTypeMaster.Where(x => planningtypes.Contains(x.PlanningTypeMasterDescription)).Select(Y => new boPlanningLevelDrop() { Planningfeedback = Y.PlanningTypeMasterDescription }).ToList();
                    var PlanningselectPropertiesvar = context.PlanningselectProperties.Where(x => PlanningselectPropertiestypes.Contains(x.Planninglevel) && x.ActivitiesRefId == ActivityRefId).Select(Y => new boPlanningLevelDrop() { Planningfeedback = Y.Planninglevel }).Distinct().ToList();
                    var data = Planningtypemaster.Union(PlanningselectPropertiesvar);
                    if (data == null)
                    {
                        string[] emptyPlanningTypes = new string[] { "Not Set" };
                        data = context.PlanningTypeMaster.Where(x => emptyPlanningTypes.Contains(x.PlanningTypeMasterDescription)).Select(Y => new boPlanningLevelDrop() { Planningfeedback = Y.PlanningTypeMasterDescription }).ToList();
                    }
                    List<boPlanningLevelDrop> returnList;
                    return returnList = data.OrderByDescending(x => x.Planningfeedback == "Not Set").ToList();
                }

            }


            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PlanningTypeMaster> GetUsedPlanningType(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var data = (from PT in context.PlanningType
                                join PTM in context.PlanningTypeMaster
                                on PT.PlanningTypeRefId equals PTM.PlanningTypeMasterId
                                where PT.LayoutRefId == Layout_ID && PTM.IsActive == 1
                                select new PlanningTypeMaster()
                                {
                                    PlanningTypeMasterId = PTM.PlanningTypeMasterId,
                                    PlanningTypeMasterDescription = PTM.PlanningTypeMasterDescription,
                                    IsActive = PTM.IsActive,
                                    CreatedBy = PTM.CreatedBy,
                                    CreatedOn = PTM.CreatedOn,
                                    UpdatedBy = PTM.UpdatedBy,
                                    UpdatedOn = PTM.UpdatedOn
                                }).ToList();

                    List<PlanningTypeMaster> returnList;
                    return returnList = data.ToList();
                }

            }


            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ConFilter> GetConFilter()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.ConFilter.ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ConPSCompareto> GetConPSCompareto()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.ConPSCompareto.ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ReadoutValue> GetReadoutValue()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.ReadoutValue.ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ActivitiesRegistrationDetails> GetActivitiesRegistrationRecive(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from AR in context.ActivitiesRegistration
                                where AR.LayoutRefId == Layout_ID && AR.PlanningRefId == "Receive" && AR.ActivitiesRegistrationName != "Start Trip"
                                select new ActivitiesRegistrationDetails()
                                {

                                    ActivitiesRegistrationId = AR.ActivitiesRegistrationId,
                                    ActivitiesRegistrationName = AR.ActivitiesRegistrationName
                                }).ToList();

                    List<ActivitiesRegistrationDetails> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /////  Flex Start  ///////////
        /// <summary>
        /// 
        /// </summary>
        /// <param name="questionRefId"></param>
        /// <returns></returns>
        public string GetFlexDetailTranslationText(FlexDetailTranslation flexDetailTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = flexDetailTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == flexDetailTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + flexDetailTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public string GetFlexDetailMaskTextTranslationText(FlexDetailMaskTextTranslation flexDetailMaskTextTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = flexDetailMaskTextTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == flexDetailMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + flexDetailMaskTextTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public string GetFlexDetailPalletsTranslationText(FlexDetailPalletsTranslation flexDetailPalletsTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = flexDetailPalletsTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == flexDetailPalletsTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailPalletsTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + flexDetailPalletsTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public string GetFlexDetailDocumentScannerTranslationText(FlexDetailDocumentScannerTranslation flexDetailDocumentScannerTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = flexDetailDocumentScannerTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == flexDetailDocumentScannerTranslation.LanguageName)
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + flexDetailDocumentScannerTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public List<Flex> GetFlexForActivities(int questionRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Flex.Where(x => x.QuestionRefID == questionRefId).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Flex InsertFlex(Flex Flex)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(Flex);
                    context.SaveChanges();
                    return Flex;
                    //return context.Flex.Where(x => x.LayoutRefID == layOutRefID && x.ActivityRefID == activitiyRefID && x.FlexId == flexID).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateFlex(Flex Flex)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Flex.SingleOrDefault(IC => IC.FlexId == Flex.FlexId);
                    if (result != null)
                    {
                        result.Title = Flex.Title;
                        result.AlternativetextonJoblevel = Flex.AlternativetextonJoblevel;
                        result.AlternativetextonPlacelevel = Flex.AlternativetextonPlacelevel;
                        result.Alternativetextonproductlevel = Flex.Alternativetextonproductlevel;
                        result.UpdatedBy = Flex.UpdatedBy;
                        result.UpdatedOn = Flex.UpdatedOn;
                        context.SaveChanges();
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<Pallets> GetFlexPalletsForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layOutRefID);

                    var palletsRefId = (Int32)0;
                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", QuesstionRefID);
                    SqlParameter extraiInfoParam = new SqlParameter("@PalletsRefId", palletsRefId);

                    var flexDetailPalletsTranslationDetails = context.FlexDetailPalletsTranslation.FromSql("GetPallets @QuestionRefId,@PalletsRefId", questionParam, extraiInfoParam).ToList();

                    List<Pallets> palletsList = new List<Pallets>();

                    foreach (var palletsDetail in flexDetailPalletsTranslationDetails)
                    {
                        Pallets pallets = new Pallets();

                        pallets.PalletsID = palletsDetail.Id;
                        pallets.ActivityRefID = palletsDetail.ActivityRefID ?? 0;
                        pallets.QuestionRefID = palletsDetail.QuestionRefID ?? 0;
                        pallets.LayoutRefID = palletsDetail.LayoutRefID ?? 0;
                        pallets.ScanLevel = palletsDetail.ScanLevel;
                        pallets.PlaceLevel = palletsDetail.PlaceLevel;
                        pallets.JobLevel = palletsDetail.JobLevel;
                        pallets.ProductLevel = palletsDetail.ProductLevel;
                        pallets.PlanningFeedbackcode = palletsDetail.PlanningFeedbackcode;
                        pallets.CreatedBy = palletsDetail.CreatedBy;
                        pallets.CreatedOn = palletsDetail.CreatedOn;
                        pallets.UpdatedBy = palletsDetail.Updatedby;
                        pallets.UpdatedOn = palletsDetail.UpdatedOn;
                        pallets.Title = GetFlexDetailPalletsTranslationText(palletsDetail, defualtLanguage);
                        pallets.PalletsRefId = palletsDetail.RefId;
                        pallets.PartnerCode = palletsDetail.PartnerCode;
                        palletsList.Add(pallets);
                    }

                    return palletsList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<Pallets>();
            }
        }
        public Pallets InsertFlexPallets(Pallets Pallets)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(Pallets);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(Pallets.LayoutRefID);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = Pallets.PalletsID ?? 0;
                    langrefdetail.QuestionRefId = Pallets.PalletsRefId;
                    langrefdetail.LayoutRefId = Pallets.LayoutRefID;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.Pallets_Title;
                    langrefdetail.LangText = Pallets.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = Pallets.CreatedBy;
                    langrefdetail.UpdatedBy = Pallets.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    /* Written By Selvam Thangaraj - Stop */

                    return Pallets;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateFlexPallets(Pallets Pallets)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var resultex = context.Questions.SingleOrDefault(IC => IC.QuestionId == Pallets.PalletsRefId);
                    if (resultex != null)
                    {

                        resultex.Name = Pallets.Title;
                        context.SaveChanges();

                        if (resultex.ISModification == 1)
                        {
                            resultex.ISModification = 2;
                            context.SaveChanges();
                        }
                    }

                    var result = context.Pallets.SingleOrDefault(IC => IC.PalletsID == Pallets.PalletsID);
                    if (result != null)
                    {
                        result.ScanLevel = Pallets.ScanLevel;
                        result.PartnerCode = Pallets.PartnerCode;
                        result.PlaceLevel = Pallets.PlaceLevel;
                        result.JobLevel = Pallets.JobLevel;
                        result.ProductLevel = Pallets.ProductLevel;
                        result.PlanningFeedbackcode = Pallets.PlanningFeedbackcode;
                        result.UpdatedBy = Pallets.UpdatedBy;
                        result.UpdatedOn = Pallets.UpdatedOn;
                        result.Title = Pallets.Title;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefID);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = Pallets.PalletsID ?? 0;
                        langrefdetail.QuestionRefId = Pallets.PalletsRefId;
                        langrefdetail.LayoutRefId = Pallets.LayoutRefID;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.Pallets_Title;
                        langrefdetail.LangText = Pallets.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = Pallets.UpdatedBy;
                        langrefdetail.UpdatedBy = Pallets.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteFlexPallets(int PalletsID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Pallets.Where(x => x.PalletsID == PalletsID).FirstOrDefault();
                    if (result != null)
                    {
                        var questions = context.Questions.Where(x => x.QuestionId == result.PalletsRefId).FirstOrDefault();
                        if (questions != null)
                        {
                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == questions.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                            if (IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green)
                            {
                                questions.ISModification = (int)EnumCopyColorCode.Red;
                                context.SaveChanges();
                            }
                            else
                            {
                                LangRefDetail langrefdetail = new LangRefDetail();
                                langrefdetail.RefId = questions.QuestionId;
                                langrefdetail.LayoutRefId = questions.LayoutRefId;
                                langrefdetail.CustomerRefId = null;
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                langrefdetail = new LangRefDetail();
                                langrefdetail.RefId = result.PalletsID ?? 0;
                                langrefdetail.LayoutRefId = result.LayoutRefID;
                                langrefdetail.CustomerRefId = null;
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.Pallets_Title;
                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                context.Pallets.Remove(result);
                                context.SaveChanges();

                                context.Questions.Remove(questions);
                                context.SaveChanges();
                            }
                        }
                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<ExtraInfo> GetFlexExtraInfoForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layOutRefID);
                    var extraInfoRefId = 0;
                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", QuesstionRefID);
                    SqlParameter extraiInfoParam = new SqlParameter("@ExtraInfoRefId", extraInfoRefId);

                    var flexDetailTranslationDetails = context.FlexDetailTranslation.FromSql("GetExtraInfo @QuestionRefId,@ExtraInfoRefId", questionParam, extraiInfoParam).ToList();
                    var flexDetailMaskTextTranslationDetails = context.FlexDetailMaskTextTranslation.FromSql("GetExtraInfoMaskText @QuestionRefId,@ExtraInfoRefId", questionParam, extraiInfoParam).ToList();

                    List<ExtraInfo> extraInfoList = new List<ExtraInfo>();

                    foreach (var extraInfoDetail in flexDetailTranslationDetails)
                    {
                        var maskText = flexDetailMaskTextTranslationDetails.Where(x => x.Id == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.ExtraInfo_MaskText).FirstOrDefault();

                        ExtraInfo extraInfo = new ExtraInfo();

                        extraInfo.ExtraInfoID = extraInfoDetail.Id;
                        extraInfo.ActivityRefID = extraInfoDetail.ActivityRefID ?? 0;
                        extraInfo.QuestionRefID = extraInfoDetail.QuestionRefID ?? 0;
                        extraInfo.LayoutRefID = extraInfoDetail.LayoutRefID ?? 0;
                        extraInfo.MaskText = (maskText == null ? "" : GetFlexDetailMaskTextTranslationText(maskText, defualtLanguage));
                        extraInfo.Format = extraInfoDetail.Format;
                        extraInfo.PlaceLevel = extraInfoDetail.PlaceLevel;
                        extraInfo.JobLevel = extraInfoDetail.JobLevel;
                        extraInfo.ProductLevel = extraInfoDetail.ProductLevel;
                        extraInfo.PlanningFeedbackcode = extraInfoDetail.PlanningFeedbackcode;
                        extraInfo.CreatedBy = extraInfoDetail.CreatedBy;
                        extraInfo.CreatedOn = extraInfoDetail.CreatedOn;
                        extraInfo.UpdatedBy = extraInfoDetail.Updatedby;
                        extraInfo.UpdatedOn = extraInfoDetail.UpdatedOn;
                        extraInfo.Title = GetFlexDetailTranslationText(extraInfoDetail, defualtLanguage);
                        extraInfo.ExtraInfoRefId = extraInfoDetail.RefId;
                        extraInfo.Size = extraInfoDetail.Size;
                        extraInfo.RegEx = extraInfoDetail.RegEx;
                        extraInfo.PartnerCode = extraInfoDetail.PartnerCode;
                        extraInfoList.Add(extraInfo);
                    }

                    return extraInfoList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ExtraInfo>();
            }
        }
        public ExtraInfo InsertFlexExtraInfo(ExtraInfo ExtraInfo)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.ExtraInfo.Add(ExtraInfo);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(ExtraInfo.LayoutRefID);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = ExtraInfo.ExtraInfoID ?? 0;
                    langrefdetail.QuestionRefId = ExtraInfo.ExtraInfoRefId;
                    langrefdetail.LayoutRefId = ExtraInfo.LayoutRefID;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfo_Title;
                    langrefdetail.LangText = ExtraInfo.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = ExtraInfo.CreatedBy;
                    langrefdetail.UpdatedBy = ExtraInfo.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    if (ExtraInfo.MaskText != null && ExtraInfo.MaskText.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfo_MaskText;
                        langrefdetail.LangText = ExtraInfo.MaskText ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return ExtraInfo;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateFlexExtraInfo(ExtraInfo ExtraInfo)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var resultex = context.Questions.SingleOrDefault(IC => IC.QuestionId == ExtraInfo.ExtraInfoRefId);
                    if (resultex != null)
                    {

                        resultex.Name = ExtraInfo.Title;
                        context.SaveChanges();

                        if (resultex.ISModification == 1)
                        {
                            resultex.ISModification = 2;
                            context.SaveChanges();
                        }
                    }
                    var result = context.ExtraInfo.SingleOrDefault(IC => IC.ExtraInfoID == ExtraInfo.ExtraInfoID);
                    if (result != null)
                    {
                        var MaskText = result.MaskText;

                        result.MaskText = ExtraInfo.MaskText;
                        result.Size = ExtraInfo.Size;
                        result.RegEx = ExtraInfo.RegEx;
                        result.PartnerCode = ExtraInfo.PartnerCode;
                        result.Format = ExtraInfo.Format;
                        result.PlaceLevel = ExtraInfo.PlaceLevel;
                        result.JobLevel = ExtraInfo.JobLevel;
                        result.ProductLevel = ExtraInfo.ProductLevel;
                        result.PlanningFeedbackcode = ExtraInfo.PlanningFeedbackcode;
                        result.UpdatedBy = ExtraInfo.UpdatedBy;
                        result.UpdatedOn = ExtraInfo.UpdatedOn;
                        result.Title = ExtraInfo.Title;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefID);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.ExtraInfoID ?? 0;
                        langrefdetail.QuestionRefId = result.ExtraInfoRefId;
                        langrefdetail.LayoutRefId = result.LayoutRefID;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfo_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if (MaskText != null && MaskText.Length > 0 && result.MaskText == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = ExtraInfo.ExtraInfoID ?? 0;
                            dlangrefdetail.LayoutRefId = ExtraInfo.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfo_MaskText;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.MaskText != null && result.MaskText.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfo_MaskText;
                            langrefdetail.LangText = ExtraInfo.MaskText ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteFlexExtraInfo(int ExtraInfoID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ExtraInfo.Where(x => x.ExtraInfoID == ExtraInfoID).FirstOrDefault();
                    if (result != null)
                    {
                        var questions = context.Questions.Where(x => x.QuestionId == result.ExtraInfoRefId).FirstOrDefault();
                        if (questions != null)
                        {
                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == questions.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                            if (IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green)
                            {
                                questions.ISModification = (int)EnumCopyColorCode.Red;
                                context.SaveChanges();
                            }
                            else
                            {

                                LangRefDetail langrefdetail = new LangRefDetail();
                                langrefdetail.RefId = questions.QuestionId;
                                langrefdetail.LayoutRefId = questions.LayoutRefId;
                                langrefdetail.CustomerRefId = null;
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                langrefdetail = new LangRefDetail();
                                langrefdetail.RefId = result.ExtraInfoID ?? 0;
                                langrefdetail.LayoutRefId = result.LayoutRefID;
                                langrefdetail.CustomerRefId = null;
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfo_Title;
                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfo_MaskText;
                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                context.ExtraInfo.Remove(result);
                                context.SaveChanges();

                                context.Questions.Remove(questions);
                                context.SaveChanges();

                            }
                        }
                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<Problems> GetFlexProblemsForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layOutRefID);

                    var ProblemsRefId = (Int32)0;

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", QuesstionRefID);
                    SqlParameter extraiInfoParam = new SqlParameter("@ProblemsRefId", ProblemsRefId);

                    var flexDetailTranslationDetails = context.FlexDetailTranslation.FromSql("GetProblems @QuestionRefId,@ProblemsRefId", questionParam, extraiInfoParam).ToList();
                    var flexDetailMaskTextTranslationDetails = context.FlexDetailMaskTextTranslation.FromSql("GetProblemsMaskText @QuestionRefId,@ProblemsRefId", questionParam, extraiInfoParam).ToList();

                    List<Problems> problemsList = new List<Problems>();

                    foreach (var problemsDetail in flexDetailTranslationDetails)
                    {
                        var maskText = flexDetailMaskTextTranslationDetails.Where(x => x.Id == problemsDetail.Id && x.LangRefColTableId == (int)LangRefColTable.Problems_MaskText).FirstOrDefault();

                        Problems problems = new Problems();

                        problems.ProblemsID = problemsDetail.Id;
                        problems.ActivityRefID = problemsDetail.ActivityRefID ?? 0;
                        problems.QuestionRefID = problemsDetail.QuestionRefID ?? 0;
                        problems.LayoutRefID = problemsDetail.LayoutRefID ?? 0;
                        problems.MaskText = (maskText == null ? "" : GetFlexDetailMaskTextTranslationText(maskText, defualtLanguage));
                        problems.Format = problemsDetail.Format;
                        problems.PlaceLevel = problemsDetail.PlaceLevel;
                        problems.JobLevel = problemsDetail.JobLevel;
                        problems.ProductLevel = problemsDetail.ProductLevel;
                        problems.PlanningFeedbackcode = problemsDetail.PlanningFeedbackcode;
                        problems.CreatedBy = problemsDetail.CreatedBy;
                        problems.CreatedOn = problemsDetail.CreatedOn;
                        problems.UpdatedBy = problemsDetail.Updatedby;
                        problems.UpdatedOn = problemsDetail.UpdatedOn;
                        problems.Title = GetFlexDetailTranslationText(problemsDetail, defualtLanguage);
                        problems.ProblemsRefId = problemsDetail.RefId;
                        problems.Size = problemsDetail.Size;
                        problems.RegEx = problemsDetail.RegEx;
                        problems.PartnerCode = problemsDetail.PartnerCode;
                        problemsList.Add(problems);
                    }

                    return problemsList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<Problems>();
            }
        }
        public Problems InsertFlexProblems(Problems Problems)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Problems.Add(Problems);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(Problems.LayoutRefID);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = Problems.ProblemsID ?? 0;
                    langrefdetail.QuestionRefId = Problems.ProblemsRefId;
                    langrefdetail.LayoutRefId = Problems.LayoutRefID;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.Problems_Title;
                    langrefdetail.LangText = Problems.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = Problems.CreatedBy;
                    langrefdetail.UpdatedBy = Problems.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    if (Problems.MaskText != null && Problems.MaskText.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.Problems_MaskText;
                        langrefdetail.LangText = Problems.MaskText ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return Problems;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateFlexProblems(Problems Problems)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var resultex = context.Questions.SingleOrDefault(IC => IC.QuestionId == Problems.ProblemsRefId);
                    if (resultex != null)
                    {

                        resultex.Name = Problems.Title;
                        context.SaveChanges();

                        //var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == AddChioceProperties.QuestionRefId);
                        if (resultex.ISModification == 1)
                        {
                            resultex.ISModification = 2;
                            context.SaveChanges();
                        }
                    }

                    var result = context.Problems.SingleOrDefault(IC => IC.ProblemsID == Problems.ProblemsID);
                    if (result != null)
                    {
                        var MaskText = result.MaskText;

                        result.MaskText = Problems.MaskText;
                        result.Format = Problems.Format;
                        result.PlaceLevel = Problems.PlaceLevel;
                        result.JobLevel = Problems.JobLevel;
                        result.ProductLevel = Problems.ProductLevel;
                        result.PlanningFeedbackcode = Problems.PlanningFeedbackcode;
                        result.Size = Problems.Size;
                        result.RegEx = Problems.RegEx;
                        result.PartnerCode = Problems.PartnerCode;
                        result.UpdatedBy = Problems.UpdatedBy;
                        result.UpdatedOn = Problems.UpdatedOn;
                        result.Title = Problems.Title;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefID);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.ProblemsID ?? 0;
                        langrefdetail.QuestionRefId = result.ProblemsRefId;
                        langrefdetail.LayoutRefId = result.LayoutRefID;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.Problems_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if (MaskText != null && MaskText.Length > 0 && result.MaskText == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = Problems.ProblemsID ?? 0;
                            dlangrefdetail.LayoutRefId = Problems.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.Problems_MaskText;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.MaskText != null && result.MaskText.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.Problems_MaskText;
                            langrefdetail.LangText = Problems.MaskText ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteFlexProblems(int ProblemsID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Problems.Where(x => x.ProblemsID == ProblemsID).FirstOrDefault();
                    if (result != null)
                    {
                        var questions = context.Questions.Where(x => x.QuestionId == result.ProblemsRefId).FirstOrDefault();
                        if (questions != null)
                        {
                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == questions.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                            if (IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green)
                            {
                                questions.ISModification = (int)EnumCopyColorCode.Red;
                                context.SaveChanges();
                            }
                            else
                            {
                                LangRefDetail langrefdetail = new LangRefDetail();
                                langrefdetail.RefId = questions.QuestionId;
                                langrefdetail.LayoutRefId = questions.LayoutRefId;
                                langrefdetail.CustomerRefId = null;
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                langrefdetail = new LangRefDetail();
                                langrefdetail.RefId = result.ProblemsID ?? 0;
                                langrefdetail.LayoutRefId = result.LayoutRefID;
                                langrefdetail.CustomerRefId = null;
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.Problems_Title;
                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                langrefdetail.LangRefColTableId = (int)LangRefColTable.Problems_MaskText;
                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                context.Problems.Remove(result);
                                context.SaveChanges();

                                context.Questions.Remove(questions);
                                context.SaveChanges();
                            }
                        }

                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<Signatures> GetFlexSignaturesForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Signatures.Where(x => x.LayoutRefID == layOutRefID && x.ActivityRefID == activitiyRefID && x.QuestionRefID == QuesstionRefID).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Signatures InsertFlexSignatures(Signatures Signatures)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Signatures.Add(Signatures);
                    context.SaveChanges();
                    return Signatures;
                    //return context.Flex.Where(x => x.LayoutRefID == layOutRefID && x.ActivityRefID == activitiyRefID && x.FlexId == flexID).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateFlexSignatures(Signatures Signatures)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var resultex = context.Questions.SingleOrDefault(IC => IC.QuestionId == Signatures.SignaturesRefId);
                    if (resultex != null)
                    {

                        resultex.Name = Signatures.Title;
                        context.SaveChanges();

                        //var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == AddChioceProperties.QuestionRefId);
                        if (resultex.ISModification == 1)
                        {
                            resultex.ISModification = 2;
                            context.SaveChanges();
                        }
                    }
                    var result = context.Signatures.SingleOrDefault(IC => IC.SignaturesID == Signatures.SignaturesID);
                    if (result != null)
                    {
                        result.PlaceLevel = Signatures.PlaceLevel;
                        result.JobLevel = Signatures.JobLevel;
                        result.UpdatedBy = Signatures.UpdatedBy;
                        result.UpdatedOn = Signatures.UpdatedOn;
                        result.Title = Signatures.Title;
                        context.SaveChanges();
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteFlexSignatures(int SignaturesID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Signatures.Where(x => x.SignaturesID == SignaturesID).FirstOrDefault();
                    if (result != null)
                    {
                        var questions = context.Questions.Where(x => x.QuestionId == result.QuestionRefID).FirstOrDefault();
                        if (questions != null)
                        {
                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == questions.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                            if (IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green)
                            {
                                questions.ISModification = (int)EnumCopyColorCode.Red;
                                context.SaveChanges();
                            }
                            else
                            {
                                context.Questions.Remove(questions);
                                context.SaveChanges();

                                context.Signatures.Remove(result);
                                context.SaveChanges();
                            }
                        }
                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<Picure> GetFlexPicureForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Picure.Where(x => x.LayoutRefID == layOutRefID && x.ActivityRefID == activitiyRefID && x.QuestionRefID == QuesstionRefID).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Picure InsertFlexPicure(Picure Picure)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Picure.Add(Picure);
                    context.SaveChanges();
                    return Picure;
                    //return context.Flex.Where(x => x.LayoutRefID == layOutRefID && x.ActivityRefID == activitiyRefID && x.FlexId == flexID).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateFlexPicure(Picure Picure)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Picure.SingleOrDefault(IC => IC.PicureID == Picure.PicureID);
                    if (result != null)
                    {
                        result.Title = Picure.Title;
                        result.AlternativetextonJoblevel = Picure.AlternativetextonJoblevel;
                        result.AlternativetextonPlacelevel = Picure.AlternativetextonPlacelevel;
                        result.Alternativetextonproductlevel = Picure.Alternativetextonproductlevel;
                        result.UpdatedBy = Picure.UpdatedBy;
                        result.UpdatedOn = Picure.UpdatedOn;
                        context.SaveChanges();
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteFlexPicure(int PicureID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Picure.Where(x => x.PicureID == PicureID).FirstOrDefault();
                    if (result != null)
                    {
                        var questions = context.Questions.Where(x => x.QuestionId == result.QuestionRefID).FirstOrDefault();
                        if (questions != null)
                        {
                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == questions.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                            if (IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green)
                            {
                                questions.ISModification = (int)EnumCopyColorCode.Red;
                                context.SaveChanges();
                            }
                            else
                            {
                                context.Questions.Remove(questions);
                                context.SaveChanges();

                                context.Picure.Remove(result);
                                context.SaveChanges();
                            }
                        }

                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<ActivitiesRegistration> GetCurrentActivitiesRegistration(int activityRegistrationID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == activityRegistrationID).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public int GetCountofInfoMessage(int activityRegistrationID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Questions.Where(x => x.ActivitiesRefId == activityRegistrationID && x.PropertyName == "Info Message").Count();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int GetCountofTextMessage(int activityRegistrationID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Questions.Where(x => x.ActivitiesRefId == activityRegistrationID && x.PropertyName == "Text Message").Count();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public List<string> GetPlanningFeedbackCode(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var chioces = context.ChioceProperties.Where(x => x.LayoutRefId == LayoutId && x.Planningfeedbackcode != null && x.Planningfeedbackcode.Trim().Length > 0).Select(x => x.Planningfeedbackcode).Distinct().ToList();
                    var chioceIdsList = context.ChioceProperties.Where(x => x.LayoutRefId == LayoutId).Select(x => x.ChioceId).Distinct().ToList();
                    var chioceDetails = context.AddChioceProperties.Where(x => chioceIdsList.Contains(x.ChioceRefId) && x.PlanningfeedbackCode != null && x.PlanningfeedbackCode.Trim().Length > 0).Select(x => x.PlanningfeedbackCode).Distinct().ToList();
                    var entry = context.EntryProperties.Where(x => x.LayoutRefId == LayoutId && x.PlanningfeedbackCode != null && x.PlanningfeedbackCode.Trim().Length > 0).Select(x => x.PlanningfeedbackCode).Distinct().ToList();
                    var varlist = context.VarListProperties.Where(x => x.LayoutRefId == LayoutId && x.PlanningFeedbackCode != null && x.PlanningFeedbackCode.Trim().Length > 0).Select(x => x.PlanningFeedbackCode).Distinct().ToList();
                    var transportType = context.TransportTypeProperties.Where(x => x.LayoutRefId == LayoutId && x.PlanningFeedbackCode != null && x.PlanningFeedbackCode.Trim().Length > 0).Select(x => x.PlanningFeedbackCode).Distinct().ToList();
                    var transportTypeIdsList = context.TransportTypeProperties.Where(x => x.LayoutRefId == LayoutId).Select(x => x.TransportTypeId).Distinct().ToList();
                    var transportTypeDetails = context.TransportTypePropertiesDetail.Where(x => transportTypeIdsList.Contains(x.TransportTypeRefId) && x.PlanningFeedbackCode != null && x.PlanningFeedbackCode.Trim().Length > 0).Select(x => x.PlanningFeedbackCode).Distinct().ToList();
                    var trailer = context.TrailerProperties.Where(x => x.LayoutRefId == LayoutId && x.PlanningFeedbackCode != null && x.PlanningFeedbackCode.Trim().Length > 0).Select(x => x.PlanningFeedbackCode).Distinct().ToList();
                    var Planningfeedbackcode = chioces.Union(entry).Union(transportType).Union(trailer).Union(chioceDetails).Union(transportTypeDetails).Union(varlist).Distinct().ToList();
                    return Planningfeedbackcode;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<string> GetChoicePlanningFeedbackCode(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var chioces = context.ChioceProperties.Where(x => x.LayoutRefId == LayoutId && x.Planningfeedbackcode != null && x.Planningfeedbackcode.Trim().Length > 0).Select(x => x.Planningfeedbackcode).Distinct().ToList();
                    var chioceIdsList = context.ChioceProperties.Where(x => x.LayoutRefId == LayoutId).Select(x => x.ChioceId).Distinct().ToList();
                    var chioceDetails = context.AddChioceProperties.Where(x => chioceIdsList.Contains(x.ChioceRefId) && x.PlanningfeedbackCode != null && x.PlanningfeedbackCode.Trim().Length > 0).Select(x => x.PlanningfeedbackCode).Distinct().ToList();
                    var entry = context.EntryProperties.Where(x => x.LayoutRefId == LayoutId && x.PlanningfeedbackCode != null && x.PlanningfeedbackCode.Trim().Length > 0).Select(x => x.PlanningfeedbackCode).Distinct().ToList();
                    var varlist = context.VarListProperties.Where(x => x.LayoutRefId == LayoutId && x.PlanningFeedbackCode != null && x.PlanningFeedbackCode.Trim().Length > 0).Select(x => x.PlanningFeedbackCode).Distinct().ToList();
                    var transportType = context.TransportTypeProperties.Where(x => x.LayoutRefId == LayoutId && x.PlanningFeedbackCode != null && x.PlanningFeedbackCode.Trim().Length > 0).Select(x => x.PlanningFeedbackCode).Distinct().ToList();
                    var transportTypeIdsList = context.TransportTypeProperties.Where(x => x.LayoutRefId == LayoutId).Select(x => x.TransportTypeId).Distinct().ToList();
                    var transportTypeDetails = context.TransportTypePropertiesDetail.Where(x => transportTypeIdsList.Contains(x.TransportTypeRefId) && x.PlanningFeedbackCode != null && x.PlanningFeedbackCode.Trim().Length > 0).Select(x => x.PlanningFeedbackCode).Distinct().ToList();
                    var trailer = context.TrailerProperties.Where(x => x.LayoutRefId == LayoutId && x.PlanningFeedbackCode != null && x.PlanningFeedbackCode.Trim().Length > 0).Select(x => x.PlanningFeedbackCode).Distinct().ToList();
                    var Planningfeedbackcode = chioces.Union(entry).Union(transportType).Union(trailer).Union(chioceDetails).Union(transportTypeDetails).Union(varlist).Distinct().ToList();
                    return Planningfeedbackcode;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<SetActionValueType> GetSetActionValueType(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var oBCType = context.OBCType.Where(x => x.LayoutRefId == layoutId).Select(x => x.OBCTypeRefId).FirstOrDefault();
                    if (oBCType == (int)EnumOBCType.TX_GO_FLEX)
                        return context.SetActionValueType.Where(x => x.IsActive && x.IsFlexSetActionValue).ToList();
                    else
                        return context.SetActionValueType.Where(x => x.IsActive).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<AliasNameDetails> GetAliasNameDetailsByLayout(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var alaisname = context.AliasNameDetails.Where(x => x.LayoutRefId == layoutId && x.IsTempData == false).ToList();
                    return alaisname;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool GetEmptyFullSoloByActivitiesRegistration(int ActivityId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var EmptyFullSolo = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == ActivityId).Select(x => x.EmptyFullSolo).FirstOrDefault();
                    return EmptyFullSolo;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool GetTransportTypeByActivitiesRegistration(int ActivityId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var TransportType = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == ActivityId).Select(x => x.TransportType).FirstOrDefault();
                    return TransportType;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        ////   Flex End ////////
        public string GetFlexTranslationText(FlexTranslation flexTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = flexTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == flexTranslation.LanguageName)
                    {
                        translationText = "@" + flexTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + flexTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public string GetFlexAlternativeTextTranslationText(FlexAlternativeTextTranslation flexAlternativeTextTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = flexAlternativeTextTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == flexAlternativeTextTranslation.LanguageName)
                    {
                        translationText = "@" + flexAlternativeTextTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + flexAlternativeTextTranslation.English;
                    }
                }
            }
            return translationText;
        }


        public ExtraInfoProperties InsertExtraInfoProperties(ExtraInfoProperties ExtraInfoProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.ExtraInfoProperties.Add(ExtraInfoProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(ExtraInfoProperties.LayoutRefID);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = ExtraInfoProperties.ExtraInfoPropertiesID ?? 0;
                    langrefdetail.QuestionRefId = ExtraInfoProperties.QuestionRefID;
                    langrefdetail.LayoutRefId = ExtraInfoProperties.LayoutRefID;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_Title;
                    langrefdetail.LangText = ExtraInfoProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = ExtraInfoProperties.CreatedBy;
                    langrefdetail.UpdatedBy = ExtraInfoProperties.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    if (ExtraInfoProperties.AlternativetextonPlacelevel != null && ExtraInfoProperties.AlternativetextonPlacelevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnPlaceLevel;
                        langrefdetail.LangText = ExtraInfoProperties.AlternativetextonPlacelevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (ExtraInfoProperties.AlternativetextonJoblevel != null && ExtraInfoProperties.AlternativetextonJoblevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnJobLevel;
                        langrefdetail.LangText = ExtraInfoProperties.AlternativetextonJoblevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (ExtraInfoProperties.Alternativetextonproductlevel != null && ExtraInfoProperties.Alternativetextonproductlevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnProductLevel;
                        langrefdetail.LangText = ExtraInfoProperties.Alternativetextonproductlevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return ExtraInfoProperties;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateExtraInfoProperties(ExtraInfoProperties ExtraInfoProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ExtraInfoProperties.SingleOrDefault(IC => IC.ExtraInfoPropertiesID == ExtraInfoProperties.ExtraInfoPropertiesID);
                    if (result != null)
                    {
                        var AlternativetextonJoblevel = result.AlternativetextonJoblevel;
                        var Alternativetextonproductlevel = result.Alternativetextonproductlevel;
                        var AlternativetextonPlacelevel = result.AlternativetextonPlacelevel;

                        result.Title = ExtraInfoProperties.Title;
                        result.AlternativetextonJoblevel = ExtraInfoProperties.AlternativetextonJoblevel;
                        result.AlternativetextonPlacelevel = ExtraInfoProperties.AlternativetextonPlacelevel;
                        result.Alternativetextonproductlevel = ExtraInfoProperties.Alternativetextonproductlevel;
                        result.Updatedby = ExtraInfoProperties.Updatedby;
                        result.UpdatedOn = ExtraInfoProperties.UpdatedOn;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefID);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.ExtraInfoPropertiesID ?? 0;
                        langrefdetail.QuestionRefId = result.QuestionRefID;
                        langrefdetail.LayoutRefId = result.LayoutRefID;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.Updatedby;
                        langrefdetail.UpdatedBy = result.Updatedby;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if (AlternativetextonPlacelevel != null && AlternativetextonPlacelevel.Length > 0 && result.AlternativetextonPlacelevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = ExtraInfoProperties.ExtraInfoPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnPlaceLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonPlacelevel != null && result.AlternativetextonPlacelevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnPlaceLevel;
                            langrefdetail.LangText = ExtraInfoProperties.AlternativetextonPlacelevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }


                        if (AlternativetextonJoblevel != null && AlternativetextonJoblevel.Length > 0 && result.AlternativetextonJoblevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = ExtraInfoProperties.ExtraInfoPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnJobLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonJoblevel != null && result.AlternativetextonJoblevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnJobLevel;
                            langrefdetail.LangText = ExtraInfoProperties.AlternativetextonJoblevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        if (Alternativetextonproductlevel != null && Alternativetextonproductlevel.Length > 0 && result.Alternativetextonproductlevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = ExtraInfoProperties.ExtraInfoPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnProductLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.Alternativetextonproductlevel != null && result.Alternativetextonproductlevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnProductLevel;
                            langrefdetail.LangText = ExtraInfoProperties.Alternativetextonproductlevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */

                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ExtraInfoProperties> GetExtraInfoProperties(int questionRefId, int layoutRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutRefId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var flexTranslationDetails = context.FlexTranslation.FromSql("GetExtraInfoProperties @QuestionRefId", questionParam).ToList();
                    var flexAlternativeTextTranslationDetails = context.FlexAlternativeTextTranslation.FromSql("GetExtraInfoPropertiesAlternativeText @QuestionRefId", questionParam).ToList();

                    List<ExtraInfoProperties> extraInfoPropertiesList = new List<ExtraInfoProperties>();

                    foreach (var extraInfoDetail in flexTranslationDetails)
                    {
                        var alternativetextonPlacelevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnPlaceLevel).FirstOrDefault();
                        var alternativetextonJoblevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnJobLevel).FirstOrDefault();
                        var alternativetextonProductlevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.ExtraInfoProperties_AlternativeTextOnProductLevel).FirstOrDefault();

                        ExtraInfoProperties extraInfo = new ExtraInfoProperties();

                        extraInfo.ExtraInfoPropertiesID = extraInfoDetail.Id;
                        extraInfo.ActivityRefID = extraInfoDetail.ActivityRefID ?? 0;
                        extraInfo.LayoutRefID = extraInfoDetail.LayoutRefID ?? 0;
                        extraInfo.CreatedBy = extraInfoDetail.CreatedBy;
                        extraInfo.CreatedOn = extraInfoDetail.CreatedOn;
                        extraInfo.Updatedby = extraInfoDetail.Updatedby;
                        extraInfo.UpdatedOn = extraInfoDetail.UpdatedOn;
                        extraInfo.QuestionRefID = extraInfoDetail.QuestionRefID ?? 0;
                        extraInfo.Title = GetFlexTranslationText(extraInfoDetail, defualtLanguage);
                        extraInfo.AlternativetextonPlacelevel = (alternativetextonPlacelevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonPlacelevel, defualtLanguage));
                        extraInfo.AlternativetextonJoblevel = (alternativetextonJoblevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonJoblevel, defualtLanguage));
                        extraInfo.Alternativetextonproductlevel = (alternativetextonProductlevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonProductlevel, defualtLanguage));
                        extraInfo.CopyLayoutRefId = extraInfoDetail.CopyLayoutRefId;
                        extraInfoPropertiesList.Add(extraInfo);
                    }

                    return extraInfoPropertiesList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ExtraInfoProperties>();
            }
        }
        public PalletsProperties InsertPalletsProperties(PalletsProperties PalletsProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(PalletsProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(PalletsProperties.LayoutRefID);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = PalletsProperties.PalletsPropertiesID ?? 0;
                    langrefdetail.QuestionRefId = PalletsProperties.QuestionRefID;
                    langrefdetail.LayoutRefId = PalletsProperties.LayoutRefID;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_Title;
                    langrefdetail.LangText = PalletsProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = PalletsProperties.CreatedBy;
                    langrefdetail.UpdatedBy = PalletsProperties.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    if (PalletsProperties.AlternativetextonPlacelevel != null && PalletsProperties.AlternativetextonPlacelevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnPlaceLevel;
                        langrefdetail.LangText = PalletsProperties.AlternativetextonPlacelevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (PalletsProperties.AlternativetextonJoblevel != null && PalletsProperties.AlternativetextonJoblevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnJobLevel;
                        langrefdetail.LangText = PalletsProperties.AlternativetextonJoblevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (PalletsProperties.Alternativetextonproductlevel != null && PalletsProperties.Alternativetextonproductlevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnProductLevel;
                        langrefdetail.LangText = PalletsProperties.Alternativetextonproductlevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return PalletsProperties;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdatePalletsProperties(PalletsProperties PalletsProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.PalletsProperties.SingleOrDefault(IC => IC.PalletsPropertiesID == PalletsProperties.PalletsPropertiesID);
                    if (result != null)
                    {
                        var AlternativetextonJoblevel = result.AlternativetextonJoblevel;
                        var Alternativetextonproductlevel = result.Alternativetextonproductlevel;
                        var AlternativetextonPlacelevel = result.AlternativetextonPlacelevel;

                        result.Title = PalletsProperties.Title;
                        result.AlternativetextonJoblevel = PalletsProperties.AlternativetextonJoblevel;
                        result.AlternativetextonPlacelevel = PalletsProperties.AlternativetextonPlacelevel;
                        result.Alternativetextonproductlevel = PalletsProperties.Alternativetextonproductlevel;
                        result.UpdatedBy = PalletsProperties.UpdatedBy;
                        result.UpdatedOn = PalletsProperties.UpdatedOn;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefID);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.PalletsPropertiesID ?? 0;
                        langrefdetail.QuestionRefId = result.QuestionRefID;
                        langrefdetail.LayoutRefId = result.LayoutRefID;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if (AlternativetextonPlacelevel != null && AlternativetextonPlacelevel.Length > 0 && result.AlternativetextonPlacelevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = PalletsProperties.PalletsPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnPlaceLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonPlacelevel != null && result.AlternativetextonPlacelevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnPlaceLevel;
                            langrefdetail.LangText = PalletsProperties.AlternativetextonPlacelevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }


                        if (AlternativetextonJoblevel != null && AlternativetextonJoblevel.Length > 0 && result.AlternativetextonJoblevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = PalletsProperties.PalletsPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnJobLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonJoblevel != null && result.AlternativetextonJoblevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnJobLevel;
                            langrefdetail.LangText = PalletsProperties.AlternativetextonJoblevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        if (Alternativetextonproductlevel != null && Alternativetextonproductlevel.Length > 0 && result.Alternativetextonproductlevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = PalletsProperties.PalletsPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnProductLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.Alternativetextonproductlevel != null && result.Alternativetextonproductlevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.PalletsProperties_AlternativeTextOnProductLevel;
                            langrefdetail.LangText = PalletsProperties.Alternativetextonproductlevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PalletsProperties> GetPalletsProperties(int questionRefId, int layoutRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutRefId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var flexTranslationDetails = context.FlexTranslation.FromSql("GetPalletsProperties @QuestionRefId", questionParam).ToList();
                    var flexAlternativeTextTranslationDetails = context.FlexAlternativeTextTranslation.FromSql("GetPalletsPropertiesAlternativeText @QuestionRefId", questionParam).ToList();

                    List<PalletsProperties> palletsPropertiesList = new List<PalletsProperties>();

                    foreach (var extraInfoDetail in flexTranslationDetails)
                    {
                        var alternativetextonPlacelevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.PalletsProperties_AlternativeTextOnPlaceLevel).FirstOrDefault();
                        var alternativetextonJoblevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.PalletsProperties_AlternativeTextOnJobLevel).FirstOrDefault();
                        var alternativetextonProductlevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.PalletsProperties_AlternativeTextOnProductLevel).FirstOrDefault();

                        PalletsProperties pallets = new PalletsProperties();

                        pallets.PalletsPropertiesID = extraInfoDetail.Id;
                        pallets.ActivityRefID = extraInfoDetail.ActivityRefID ?? 0;
                        pallets.LayoutRefID = extraInfoDetail.LayoutRefID ?? 0;
                        pallets.CreatedBy = extraInfoDetail.CreatedBy;
                        pallets.CreatedOn = extraInfoDetail.CreatedOn;
                        pallets.UpdatedBy = extraInfoDetail.Updatedby;
                        pallets.UpdatedOn = extraInfoDetail.UpdatedOn;
                        pallets.QuestionRefID = extraInfoDetail.QuestionRefID ?? 0;
                        pallets.Title = GetFlexTranslationText(extraInfoDetail, defualtLanguage);
                        pallets.AlternativetextonPlacelevel = (alternativetextonPlacelevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonPlacelevel, defualtLanguage));
                        pallets.AlternativetextonJoblevel = (alternativetextonJoblevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonJoblevel, defualtLanguage));
                        pallets.Alternativetextonproductlevel = (alternativetextonProductlevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonProductlevel, defualtLanguage));
                        pallets.CopyLayoutRefId = extraInfoDetail.CopyLayoutRefId;
                        palletsPropertiesList.Add(pallets);
                    }

                    return palletsPropertiesList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<PalletsProperties>();
            }
        }
        public ProblemsProperties InsertProblemsProperties(ProblemsProperties ProblemsProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(ProblemsProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(ProblemsProperties.LayoutRefID);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = ProblemsProperties.ProblemsPropertiesID ?? 0;
                    langrefdetail.QuestionRefId = ProblemsProperties.QuestionRefID;
                    langrefdetail.LayoutRefId = ProblemsProperties.LayoutRefID;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_Title;
                    langrefdetail.LangText = ProblemsProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = ProblemsProperties.CreatedBy;
                    langrefdetail.UpdatedBy = ProblemsProperties.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    if (ProblemsProperties.AlternativetextonPlacelevel != null && ProblemsProperties.AlternativetextonPlacelevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnPlaceLevel;
                        langrefdetail.LangText = ProblemsProperties.AlternativetextonPlacelevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (ProblemsProperties.AlternativetextonJoblevel != null && ProblemsProperties.AlternativetextonJoblevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnJobLevel;
                        langrefdetail.LangText = ProblemsProperties.AlternativetextonJoblevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (ProblemsProperties.Alternativetextonproductlevel != null && ProblemsProperties.Alternativetextonproductlevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnProductLevel;
                        langrefdetail.LangText = ProblemsProperties.Alternativetextonproductlevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return ProblemsProperties;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateProblemsProperties(ProblemsProperties ProblemsProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ProblemsProperties.SingleOrDefault(IC => IC.ProblemsPropertiesID == ProblemsProperties.ProblemsPropertiesID);
                    if (result != null)
                    {
                        var AlternativetextonJoblevel = result.AlternativetextonJoblevel;
                        var Alternativetextonproductlevel = result.Alternativetextonproductlevel;
                        var AlternativetextonPlacelevel = result.AlternativetextonPlacelevel;

                        result.Title = ProblemsProperties.Title;
                        result.AlternativetextonJoblevel = ProblemsProperties.AlternativetextonJoblevel;
                        result.AlternativetextonPlacelevel = ProblemsProperties.AlternativetextonPlacelevel;
                        result.Alternativetextonproductlevel = ProblemsProperties.Alternativetextonproductlevel;
                        result.UpdatedBy = ProblemsProperties.UpdatedBy;
                        result.UpdatedOn = ProblemsProperties.UpdatedOn;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefID);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.ProblemsPropertiesID ?? 0;
                        langrefdetail.QuestionRefId = result.QuestionRefID;
                        langrefdetail.LayoutRefId = result.LayoutRefID;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if (AlternativetextonPlacelevel != null && AlternativetextonPlacelevel.Length > 0 && result.AlternativetextonPlacelevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = ProblemsProperties.ProblemsPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnPlaceLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonPlacelevel != null && result.AlternativetextonPlacelevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnPlaceLevel;
                            langrefdetail.LangText = ProblemsProperties.AlternativetextonPlacelevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }


                        if (AlternativetextonJoblevel != null && AlternativetextonJoblevel.Length > 0 && result.AlternativetextonJoblevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = ProblemsProperties.ProblemsPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnJobLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonJoblevel != null && result.AlternativetextonJoblevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnJobLevel;
                            langrefdetail.LangText = ProblemsProperties.AlternativetextonJoblevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        if (Alternativetextonproductlevel != null && Alternativetextonproductlevel.Length > 0 && result.Alternativetextonproductlevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = ProblemsProperties.ProblemsPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnProductLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.Alternativetextonproductlevel != null && result.Alternativetextonproductlevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.ProblemsProperties_AlternativeTextOnProductLevel;
                            langrefdetail.LangText = ProblemsProperties.Alternativetextonproductlevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ProblemsProperties> GetProblemsProperties(int questionRefId, int layoutRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutRefId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var flexTranslationDetails = context.FlexTranslation.FromSql("GetProblemsProperties @QuestionRefId", questionParam).ToList();
                    var flexAlternativeTextTranslationDetails = context.FlexAlternativeTextTranslation.FromSql("GetProblemsPropertiesAlternativeText @QuestionRefId", questionParam).ToList();

                    List<ProblemsProperties> problemsPropertiesList = new List<ProblemsProperties>();

                    foreach (var extraInfoDetail in flexTranslationDetails)
                    {
                        var alternativetextonPlacelevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.ProblemsProperties_AlternativeTextOnPlaceLevel).FirstOrDefault();
                        var alternativetextonJoblevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.ProblemsProperties_AlternativeTextOnJobLevel).FirstOrDefault();
                        var alternativetextonProductlevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.ProblemsProperties_AlternativeTextOnProductLevel).FirstOrDefault();

                        ProblemsProperties problems = new ProblemsProperties();

                        problems.ProblemsPropertiesID = extraInfoDetail.Id;
                        problems.ActivityRefID = extraInfoDetail.ActivityRefID ?? 0;
                        problems.LayoutRefID = extraInfoDetail.LayoutRefID ?? 0;
                        problems.CreatedBy = extraInfoDetail.CreatedBy;
                        problems.CreatedOn = extraInfoDetail.CreatedOn;
                        problems.UpdatedBy = extraInfoDetail.Updatedby;
                        problems.UpdatedOn = extraInfoDetail.UpdatedOn;
                        problems.QuestionRefID = extraInfoDetail.QuestionRefID ?? 0;
                        problems.Title = GetFlexTranslationText(extraInfoDetail, defualtLanguage);
                        problems.AlternativetextonPlacelevel = (alternativetextonPlacelevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonPlacelevel, defualtLanguage));
                        problems.AlternativetextonJoblevel = (alternativetextonJoblevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonJoblevel, defualtLanguage));
                        problems.Alternativetextonproductlevel = (alternativetextonProductlevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonProductlevel, defualtLanguage));
                        problems.CopyLayoutRefId = extraInfoDetail.CopyLayoutRefId;
                        problemsPropertiesList.Add(problems);
                    }

                    return problemsPropertiesList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ProblemsProperties>();
            }
        }
        public SignaturesProperties InsertSignaturesProperties(SignaturesProperties SignaturesProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(SignaturesProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(SignaturesProperties.LayoutRefID);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = SignaturesProperties.SignaturesPropertiesID ?? 0;
                    langrefdetail.QuestionRefId = SignaturesProperties.QuestionRefID;
                    langrefdetail.LayoutRefId = SignaturesProperties.LayoutRefID;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_Title;
                    langrefdetail.LangText = SignaturesProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = SignaturesProperties.CreatedBy;
                    langrefdetail.UpdatedBy = SignaturesProperties.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    if (SignaturesProperties.AlternativetextonPlacelevel != null && SignaturesProperties.AlternativetextonPlacelevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnPlaceLevel;
                        langrefdetail.LangText = SignaturesProperties.AlternativetextonPlacelevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (SignaturesProperties.AlternativetextonJoblevel != null && SignaturesProperties.AlternativetextonJoblevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnJobLevel;
                        langrefdetail.LangText = SignaturesProperties.AlternativetextonJoblevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (SignaturesProperties.Alternativetextonproductlevel != null && SignaturesProperties.Alternativetextonproductlevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnProductLevel;
                        langrefdetail.LangText = SignaturesProperties.Alternativetextonproductlevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return SignaturesProperties;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateSignaturesProperties(SignaturesProperties SignaturesProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.SignaturesProperties.SingleOrDefault(IC => IC.SignaturesPropertiesID == SignaturesProperties.SignaturesPropertiesID);
                    if (result != null)
                    {

                        var AlternativetextonJoblevel = result.AlternativetextonJoblevel;
                        var Alternativetextonproductlevel = result.Alternativetextonproductlevel;
                        var AlternativetextonPlacelevel = result.AlternativetextonPlacelevel;

                        result.Title = SignaturesProperties.Title;
                        result.AlternativetextonJoblevel = SignaturesProperties.AlternativetextonJoblevel;
                        result.AlternativetextonPlacelevel = SignaturesProperties.AlternativetextonPlacelevel;
                        result.Alternativetextonproductlevel = SignaturesProperties.Alternativetextonproductlevel;
                        result.JobLevel = SignaturesProperties.JobLevel;
                        result.PlaceLevel = SignaturesProperties.PlaceLevel;
                        result.UpdatedBy = SignaturesProperties.UpdatedBy;
                        result.UpdatedOn = SignaturesProperties.UpdatedOn;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefID);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.SignaturesPropertiesID ?? 0;
                        langrefdetail.QuestionRefId = result.QuestionRefID;
                        langrefdetail.LayoutRefId = result.LayoutRefID;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if (AlternativetextonPlacelevel != null && AlternativetextonPlacelevel.Length > 0 && result.AlternativetextonPlacelevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = SignaturesProperties.SignaturesPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnPlaceLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonPlacelevel != null && result.AlternativetextonPlacelevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnPlaceLevel;
                            langrefdetail.LangText = SignaturesProperties.AlternativetextonPlacelevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }


                        if (AlternativetextonJoblevel != null && AlternativetextonJoblevel.Length > 0 && result.AlternativetextonJoblevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = SignaturesProperties.SignaturesPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnJobLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonJoblevel != null && result.AlternativetextonJoblevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnJobLevel;
                            langrefdetail.LangText = SignaturesProperties.AlternativetextonJoblevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        if (Alternativetextonproductlevel != null && Alternativetextonproductlevel.Length > 0 && result.Alternativetextonproductlevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = SignaturesProperties.SignaturesPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnProductLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.Alternativetextonproductlevel != null && result.Alternativetextonproductlevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.SignaturesProperties_AlternativeTextOnProductLevel;
                            langrefdetail.LangText = SignaturesProperties.Alternativetextonproductlevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<SignaturesProperties> GetSignaturesProperties(int questionRefId, int layoutRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutRefId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var flexTranslationDetails = context.FlexTranslation.FromSql("GetSignaturesProperties @QuestionRefId", questionParam).ToList();
                    var flexAlternativeTextTranslationDetails = context.FlexAlternativeTextTranslation.FromSql("GetSignaturesPropertiesAlternativeText @QuestionRefId", questionParam).ToList();

                    List<SignaturesProperties> signaturesPropertiesList = new List<SignaturesProperties>();

                    foreach (var extraInfoDetail in flexTranslationDetails)
                    {
                        var alternativetextonPlacelevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.SignaturesProperties_AlternativeTextOnPlaceLevel).FirstOrDefault();
                        var alternativetextonJoblevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.SignaturesProperties_AlternativeTextOnJobLevel).FirstOrDefault();
                        var alternativetextonProductlevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.SignaturesProperties_AlternativeTextOnProductLevel).FirstOrDefault();

                        SignaturesProperties signatures = new SignaturesProperties();

                        signatures.SignaturesPropertiesID = extraInfoDetail.Id;
                        signatures.ActivityRefID = extraInfoDetail.ActivityRefID ?? 0;
                        signatures.LayoutRefID = extraInfoDetail.LayoutRefID ?? 0;
                        signatures.CreatedBy = extraInfoDetail.CreatedBy;
                        signatures.CreatedOn = extraInfoDetail.CreatedOn;
                        signatures.UpdatedBy = extraInfoDetail.Updatedby;
                        signatures.UpdatedOn = extraInfoDetail.UpdatedOn;
                        signatures.QuestionRefID = extraInfoDetail.QuestionRefID ?? 0;
                        signatures.Title = GetFlexTranslationText(extraInfoDetail, defualtLanguage);
                        signatures.AlternativetextonPlacelevel = (alternativetextonPlacelevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonPlacelevel, defualtLanguage));
                        signatures.AlternativetextonJoblevel = (alternativetextonJoblevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonJoblevel, defualtLanguage));
                        signatures.Alternativetextonproductlevel = (alternativetextonProductlevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonProductlevel, defualtLanguage));
                        signatures.CopyLayoutRefId = extraInfoDetail.CopyLayoutRefId;
                        signatures.JobLevel = extraInfoDetail.JobLevel;
                        signatures.PlaceLevel = extraInfoDetail.PlaceLevel;

                        signaturesPropertiesList.Add(signatures);
                    }

                    return signaturesPropertiesList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<SignaturesProperties>();
            }
        }
        public List<Questions> GetPicure(int questionRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Questions.Where(x => x.QuestionId == questionRefId).ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public PictureProperties InsertPictureProperties(PictureProperties PictureProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(PictureProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(PictureProperties.LayoutRefID);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = PictureProperties.PicturePropertiesID ?? 0;
                    langrefdetail.QuestionRefId = PictureProperties.QuestionRefID;
                    langrefdetail.LayoutRefId = PictureProperties.LayoutRefID;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_Title;
                    langrefdetail.LangText = PictureProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = PictureProperties.CreatedBy;
                    langrefdetail.UpdatedBy = PictureProperties.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    if (PictureProperties.AlternativetextonPlacelevel != null && PictureProperties.AlternativetextonPlacelevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnPlaceLevel;
                        langrefdetail.LangText = PictureProperties.AlternativetextonPlacelevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (PictureProperties.AlternativetextonJoblevel != null && PictureProperties.AlternativetextonJoblevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnJobLevel;
                        langrefdetail.LangText = PictureProperties.AlternativetextonJoblevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (PictureProperties.Alternativetextonproductlevel != null && PictureProperties.Alternativetextonproductlevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnProductLevel;
                        langrefdetail.LangText = PictureProperties.Alternativetextonproductlevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return PictureProperties;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PictureProperties> GetPictureProperties(int questionRefId, int layoutRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutRefId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var flexTranslationDetails = context.FlexTranslation.FromSql("GetPictureProperties @QuestionRefId", questionParam).ToList();
                    var flexAlternativeTextTranslationDetails = context.FlexAlternativeTextTranslation.FromSql("GetPicturePropertiesAlternativeText @QuestionRefId", questionParam).ToList();

                    List<PictureProperties> picturePropertiesList = new List<PictureProperties>();

                    foreach (var extraInfoDetail in flexTranslationDetails)
                    {
                        var alternativetextonPlacelevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.PictureProperties_AlternativeTextOnPlaceLevel).FirstOrDefault();
                        var alternativetextonJoblevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.PictureProperties_AlternativeTextOnJobLevel).FirstOrDefault();
                        var alternativetextonProductlevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.PictureProperties_AlternativeTextOnProductLevel).FirstOrDefault();

                        PictureProperties picture = new PictureProperties();

                        picture.PicturePropertiesID = extraInfoDetail.Id;
                        picture.ActivityRefID = extraInfoDetail.ActivityRefID ?? 0;
                        picture.LayoutRefID = extraInfoDetail.LayoutRefID ?? 0;
                        picture.CreatedBy = extraInfoDetail.CreatedBy;
                        picture.CreatedOn = extraInfoDetail.CreatedOn;
                        picture.UpdatedBy = extraInfoDetail.Updatedby;
                        picture.UpdatedOn = extraInfoDetail.UpdatedOn;
                        picture.QuestionRefID = extraInfoDetail.QuestionRefID ?? 0;
                        picture.Title = GetFlexTranslationText(extraInfoDetail, defualtLanguage);
                        picture.AlternativetextonPlacelevel = (alternativetextonPlacelevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonPlacelevel, defualtLanguage));
                        picture.AlternativetextonJoblevel = (alternativetextonJoblevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonJoblevel, defualtLanguage));
                        picture.Alternativetextonproductlevel = (alternativetextonProductlevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonProductlevel, defualtLanguage));
                        picturePropertiesList.Add(picture);
                    }

                    return picturePropertiesList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<PictureProperties>();
            }
        }
        public string UpdatePictureProperties(PictureProperties PictureProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.PictureProperties.Where(IC => IC.PicturePropertiesID == PictureProperties.PicturePropertiesID).FirstOrDefault();
                    if (result != null)
                    {
                        var AlternativetextonJoblevel = result.AlternativetextonJoblevel;
                        var Alternativetextonproductlevel = result.Alternativetextonproductlevel;
                        var AlternativetextonPlacelevel = result.AlternativetextonPlacelevel;

                        result.Title = PictureProperties.Title;
                        result.AlternativetextonJoblevel = PictureProperties.AlternativetextonJoblevel;
                        result.AlternativetextonPlacelevel = PictureProperties.AlternativetextonPlacelevel;
                        result.Alternativetextonproductlevel = PictureProperties.Alternativetextonproductlevel;
                        result.UpdatedBy = PictureProperties.UpdatedBy;
                        result.UpdatedOn = PictureProperties.UpdatedOn;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefID);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.PicturePropertiesID ?? 0;
                        langrefdetail.QuestionRefId = result.QuestionRefID;
                        langrefdetail.LayoutRefId = result.LayoutRefID;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if (AlternativetextonPlacelevel != null && AlternativetextonPlacelevel.Length > 0 && result.AlternativetextonPlacelevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = PictureProperties.PicturePropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnPlaceLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonPlacelevel != null && result.AlternativetextonPlacelevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnPlaceLevel;
                            langrefdetail.LangText = PictureProperties.AlternativetextonPlacelevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }


                        if (AlternativetextonJoblevel != null && AlternativetextonJoblevel.Length > 0 && result.AlternativetextonJoblevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = PictureProperties.PicturePropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnJobLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonJoblevel != null && result.AlternativetextonJoblevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnJobLevel;
                            langrefdetail.LangText = PictureProperties.AlternativetextonJoblevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        if (Alternativetextonproductlevel != null && Alternativetextonproductlevel.Length > 0 && result.Alternativetextonproductlevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = PictureProperties.PicturePropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnProductLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.Alternativetextonproductlevel != null && result.Alternativetextonproductlevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.PictureProperties_AlternativeTextOnProductLevel;
                            langrefdetail.LangText = PictureProperties.Alternativetextonproductlevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return "Updated";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<SmartPallets> GetSmartPalletsForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.SmartPallets.Where(x => x.LayoutRefID == layOutRefID && x.QuestionRefID == QuesstionRefID).ToList(); // x.ActivityRefID == activitiyRefID &&
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public SmartPallets InsertSmartPallets(SmartPallets SmartPallets)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(SmartPallets);
                    context.SaveChanges();
                    return SmartPallets;
                    //return context.Flex.Where(x => x.LayoutRefID == layOutRefID && x.ActivityRefID == activitiyRefID && x.FlexId == flexID).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateSmartPallets(SmartPallets SmartPallets)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var resultex = context.Questions.SingleOrDefault(IC => IC.QuestionId == SmartPallets.SmartPalletsRefID);
                    if (resultex != null)
                    {

                        resultex.Name = SmartPallets.Title;
                        context.SaveChanges();

                        if (resultex.ISModification == 1)
                        {
                            resultex.ISModification = 2;
                            context.SaveChanges();
                        }
                    }

                    var result = context.SmartPallets.SingleOrDefault(IC => IC.SmartPalletsID == SmartPallets.SmartPalletsID);
                    if (result != null)
                    {
                        result.Partnercode = SmartPallets.Partnercode;
                        result.PlaceLevel = SmartPallets.PlaceLevel;
                        result.JobLevel = SmartPallets.JobLevel;
                        result.ProductLevel = SmartPallets.ProductLevel;
                        result.PlanningFeedbackcode = SmartPallets.PlanningFeedbackcode;
                        result.Updatedby = SmartPallets.Updatedby;
                        result.UpdatedOn = SmartPallets.UpdatedOn;
                        result.Title = SmartPallets.Title;
                        context.SaveChanges();
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteSmartPallets(int SmartPalletsID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.SmartPallets.Where(x => x.SmartPalletsID == SmartPalletsID).FirstOrDefault();
                    if (result != null)
                    {
                        var questions = context.Questions.Where(x => x.QuestionId == result.QuestionRefID).FirstOrDefault();
                        if (questions != null)
                        {
                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == questions.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                            if (IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green)
                            {
                                questions.ISModification = (int)EnumCopyColorCode.Red;
                                context.SaveChanges();
                            }
                            else
                            {
                                context.Questions.Remove(questions);
                                context.SaveChanges();

                                context.SmartPallets.Remove(result);
                                context.SaveChanges();
                            }
                        }

                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<SmartExtraInfo> GetSmartExtraInfoForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.SmartExtraInfo.Where(x => x.LayoutRefID == layOutRefID && x.QuestionRefID == QuesstionRefID).ToList(); // x.ActivityRefID == activitiyRefID && 
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public SmartExtraInfo InsertSmartExtraInfo(SmartExtraInfo SmartExtraInfo)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.SmartExtraInfo.Add(SmartExtraInfo);
                    context.SaveChanges();
                    return SmartExtraInfo;
                    //return context.Flex.Where(x => x.LayoutRefID == layOutRefID && x.ActivityRefID == activitiyRefID && x.FlexId == flexID).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateSmartExtraInfo(SmartExtraInfo SmartExtraInfo)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var resultex = context.Questions.SingleOrDefault(IC => IC.QuestionId == SmartExtraInfo.SmartExtraInfoRefID);
                    if (resultex != null)
                    {

                        resultex.Name = SmartExtraInfo.Title;
                        context.SaveChanges();

                        if (resultex.ISModification == 1)
                        {
                            resultex.ISModification = 2;
                            context.SaveChanges();
                        }
                    }
                    var result = context.SmartExtraInfo.SingleOrDefault(IC => IC.SmartExtraInfoID == SmartExtraInfo.SmartExtraInfoID);
                    if (result != null)
                    {
                        result.Partnercode = SmartExtraInfo.Partnercode;
                        result.RegEx = SmartExtraInfo.RegEx;
                        result.Size = SmartExtraInfo.Size;
                        result.Format = SmartExtraInfo.Format;
                        result.PlaceLevel = SmartExtraInfo.PlaceLevel;
                        result.JobLevel = SmartExtraInfo.JobLevel;
                        result.ProductLevel = SmartExtraInfo.ProductLevel;
                        result.PlanningFeedbackcode = SmartExtraInfo.PlanningFeedbackcode;
                        result.UpdatedBy = SmartExtraInfo.UpdatedBy;
                        result.UpdatedOn = SmartExtraInfo.UpdatedOn;
                        result.Title = SmartExtraInfo.Title;
                        context.SaveChanges();
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteSmartExtraInfo(int SmartExtraInfoID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.SmartExtraInfo.Where(x => x.SmartExtraInfoID == SmartExtraInfoID).FirstOrDefault();
                    if (result != null)
                    {
                        var questions = context.Questions.Where(x => x.QuestionId == result.QuestionRefID).FirstOrDefault();
                        if (questions != null)
                        {
                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == questions.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                            if (IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green)
                            {
                                questions.ISModification = (int)EnumCopyColorCode.Red;
                                context.SaveChanges();
                            }
                            else
                            {
                                context.Questions.Remove(questions);
                                context.SaveChanges();

                                context.SmartExtraInfo.Remove(result);
                                context.SaveChanges();
                            }
                        }

                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<SmartProblems> GetSmartProblemsForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.SmartProblems.Where(x => x.LayoutRefID == layOutRefID && x.QuestionRefID == QuesstionRefID).ToList(); // x.ActivityRefID == activitiyRefID &&
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public SmartProblems InsertSmartProblems(SmartProblems SmartProblems)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.SmartProblems.Add(SmartProblems);
                    context.SaveChanges();
                    return SmartProblems;
                    //return context.Flex.Where(x => x.LayoutRefID == layOutRefID && x.ActivityRefID == activitiyRefID && x.FlexId == flexID).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateSmartProblems(SmartProblems SmartProblems)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var resultex = context.Questions.SingleOrDefault(IC => IC.QuestionId == SmartProblems.SmartProblemsRefID);
                    if (resultex != null)
                    {

                        resultex.Name = SmartProblems.Title;
                        context.SaveChanges();

                        //var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == AddChioceProperties.QuestionRefId);
                        if (resultex.ISModification == 1)
                        {
                            resultex.ISModification = 2;
                            context.SaveChanges();
                        }
                    }

                    var result = context.SmartProblems.SingleOrDefault(IC => IC.SmartProblemsID == SmartProblems.SmartProblemsID);
                    if (result != null)
                    {
                        result.Partnercode = SmartProblems.Partnercode;
                        result.RegEx = SmartProblems.RegEx;
                        result.Size = SmartProblems.Size;
                        result.Format = SmartProblems.Format;
                        result.PlaceLevel = SmartProblems.PlaceLevel;
                        result.JobLevel = SmartProblems.JobLevel;
                        result.ProductLevel = SmartProblems.ProductLevel;
                        result.PlanningFeedbackcode = SmartProblems.PlanningFeedbackcode;
                        result.UpdatedBy = SmartProblems.UpdatedBy;
                        result.UpdatedOn = SmartProblems.UpdatedOn;
                        result.Title = SmartProblems.Title;
                        context.SaveChanges();
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteSmartProblems(int SmartProblemsID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.SmartProblems.Where(x => x.SmartProblemsID == SmartProblemsID).FirstOrDefault();
                    if (result != null)
                    {
                        var questions = context.Questions.Where(x => x.QuestionId == result.QuestionRefID).FirstOrDefault();
                        if (questions != null)
                        {
                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == questions.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                            if (IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green)
                            {
                                questions.ISModification = (int)EnumCopyColorCode.Red;
                                context.SaveChanges();
                            }
                            else
                            {
                                context.Questions.Remove(questions);
                                context.SaveChanges();

                                context.SmartProblems.Remove(result);
                                context.SaveChanges();
                            }
                        }

                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public CopyPaste InsertCopyPaste(CopyPaste CopyPaste)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.CopyPaste.Add(CopyPaste);
                    context.SaveChanges();
                    return CopyPaste;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetCopyPaste(string CreatedBy)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = context.CopyPaste.Where(x => x.CreatedBy == CreatedBy).FirstOrDefault();

                    return data.CopiedItem.ToString();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteCopyPaste(string CreatedBy)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.CopyPaste.Where(x => x.CreatedBy == CreatedBy).FirstOrDefault();
                    if (result != null)
                    {
                        context.CopyPaste.Remove(result);
                        context.SaveChanges();
                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public Layout GetLayoutDetails(int layouid)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Layout.Where(x => x.LayoutId == layouid).FirstOrDefault();
                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<boLayoutDetailsforCopy> GetLayoutforCopy(int CustomerRefId, int OBCTypeId, bool isCustomerLogin, string company)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<boLayoutDetailsforCopy> resultList = new List<boLayoutDetailsforCopy>();
                    var LayoutIds = context.OBCType.Where(x => x.OBCTypeRefId == OBCTypeId && x.CustomerRefId == CustomerRefId).Select(x => x.LayoutRefId).ToList();
                    var customerId = context.Customers.Where(x => x.MyTrxCompanyId == company).Select(x => x.CustomerId).FirstOrDefault();
                    if(isCustomerLogin && customerId != CustomerRefId)
                        resultList = context.Layout.Where(x => LayoutIds.Contains(x.LayoutId) && x.LayoutStatusRefId == (int)EnumLayoutStatus.Finished).Select(y => new boLayoutDetailsforCopy() { value = y.LayoutId, text = y.LayoutName }).ToList();
                    else
                        resultList = context.Layout.Where(x => LayoutIds.Contains(x.LayoutId)).Select(y => new boLayoutDetailsforCopy() { value = y.LayoutId, text = y.LayoutName }).ToList();

                    return resultList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<boLayoutDetailsforCopy> GetLayoutforCopywithoutCustomer()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Layout.Select(y => new boLayoutDetailsforCopy() { value = y.LayoutId, text = y.LayoutName }).ToList();
                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<boLayoutDetailsforCopy> GetActivitieslistCopy(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var activityId = 0;
                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", LayoutId);
                    SqlParameter activityParam = new SqlParameter("@ActivityRefId", activityId);

                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(LayoutId);
                    var activitiesRegistrationTranslationDetails = context.ActivitiesRegistrationTranslation.FromSql("GetActivitiesRegistration @LayoutRefId,@ActivityRefId", layoutParam, activityParam).ToList();
                    List<boLayoutDetailsforCopy> activitiesRegisrationList = new List<boLayoutDetailsforCopy>();

                    foreach (var actRegInfo in activitiesRegistrationTranslationDetails)
                    {
                        boLayoutDetailsforCopy actReg = new boLayoutDetailsforCopy();
                        actReg.value = actRegInfo.ActivitiesRegistrationId;
                        actReg.text = GetActivitiesRegistrationName(actRegInfo, defualtLanguage);
                        activitiesRegisrationList.Add(actReg);
                    }

                    return activitiesRegisrationList;
                }

            }
            catch (Exception ex)
            {
                return new List<boLayoutDetailsforCopy>();
            }
        }

        public List<ActivitiesRegistration> GetActivitiesList(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var activityId = 0;
                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", LayoutId);
                    SqlParameter activityParam = new SqlParameter("@ActivityRefId", activityId);

                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(LayoutId);
                    var activitiesRegistrationTranslationDetails = context.ActivitiesRegistrationTranslation.FromSql("GetActivitiesRegistration @LayoutRefId,@ActivityRefId", layoutParam, activityParam).ToList();
                    List<ActivitiesRegistration> activitiesRegisrationList = new List<ActivitiesRegistration>();

                    foreach (var actRegInfo in activitiesRegistrationTranslationDetails)
                    {
                        ActivitiesRegistration actReg = new ActivitiesRegistration();
                        actReg.ActivitiesRegistrationName = GetActivitiesRegistrationName(actRegInfo, defualtLanguage);
                        actReg.ActivitiesRegistrationId = actRegInfo.ActivitiesRegistrationId;
                        activitiesRegisrationList.Add(actReg);
                    }

                    return activitiesRegisrationList.OrderByDescending(x => x.ActivitiesRegistrationName).ToList();
                }

            }
            catch (Exception ex)
            {
                return new List<ActivitiesRegistration>();
            }
        }

public string GetActivitiesRegistrationName(ActivitiesRegistrationTranslation activitiesRegisrationTranslation, string defaultLanguage)
{
    var actRegName = string.Empty;

    if (LanguagesEnum.English.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.English;
    }
    else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Arabic;
    }
    else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Bulgarian;
    }
    else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Croatian;
    }
    else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Czech;
    }
    else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Danish;
    }
    else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Dutch;
    }
    else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Estonian;
    }
    else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Finnish;
    }
    else if (LanguagesEnum.French.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.French;
    }
    else if (LanguagesEnum.German.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.German;
    }
    else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Greek;
    }
    else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Hungarian;
    }
    else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Italian;
    }
    else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Latvian;
    }
    else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Lithuanian;
    }
    else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Macedonian;
    }
    else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Norwegian;
    }
    else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Polish;
    }
    else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Portuguese;
    }
    else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Romanian;
    }
    else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Russian;
    }
    else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Slovak;
    }
    else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Slovene;
    }
    else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Spanish;
    }
    else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Swedish;
    }
    else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Turkish;
    }
    else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Ukrainian;
    }
    else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
    {
        actRegName = activitiesRegisrationTranslation.Belarusian;
    }
    else
    {
        actRegName = "";
    }

    if (actRegName == null || actRegName == "")
    {
        using (var context = new Creatis_Context())
        {
            if (LanguagesEnum.English.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == activitiesRegisrationTranslation.LanguageName)
            {
                actRegName = "@" + activitiesRegisrationTranslation.Belarusian;
            }
            else
            {
                actRegName = "@" + activitiesRegisrationTranslation.English;
            }
        }
    }
    return actRegName;
}
public List<boLayoutDetailsforCopy> GetTransporttypelistCopy(int LayoutId, int activityid)
        {
            try

            {

                using (var context = new Creatis_Context())
                {
                    var istransportTypeEnable = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == activityid).Select(x => x.TransportType).FirstOrDefault();

                    if (Convert.ToBoolean(istransportTypeEnable))
                    {

                        var actQuestion = context.Questions.Where(x => x.ActivitiesRefId == activityid && x.LayoutRefId == LayoutId).Select(y => y.TransporttypeRefId).Distinct().ToList();

                        if (actQuestion != null)
                        {
                            var result = context.TransportTypes.Where(x => actQuestion.Contains(x.TransportTypeId) && x.IsActive).Select(y => new boLayoutDetailsforCopy { value = y.TransportTypeId, text = y.TransportTypeName }).ToList();
                            //var result=context.TransportTypes.Contains(actQuestion)
                            return result;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }


                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateTreeViewDraggingNodes(List<PositionedQuestions> positionedQuestions)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    foreach (var questions in positionedQuestions)
                    {
                        var quesionResult = context.Questions.SingleOrDefault(IC => IC.QuestionId == questions.QuestionId);
                        if (quesionResult != null)
                        {
                            quesionResult.ParentId = questions.ParentId ?? 0;
                            quesionResult.Position = questions.Position;
                            context.SaveChanges();
                        }

                    }
                    return "Updated";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PlanningType> GetCehckOffPlanningLevel(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.PlanningType.Where(x => x.LayoutRefId == LayoutId && x.PlanningTypeMaster.IsActive == 1).Select(y => new PlanningType { PlanningTypeName = y.PlanningTypeMaster.PlanningTypeMasterDescription }).Where(x => x.PlanningTypeName != "No Planning").ToList();
                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<DocumentTypeMaster> GetAllDocumentType()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.DocumentTypeMaster.Where(x => x.IsActive == 1).Select(y => new DocumentTypeMaster { DocumentTypeId = y.DocumentTypeId, DocumentTypeDescription = y.DocumentTypeDescription + " (" + y.DocumentTypeCode + ")" }).ToList();
                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DocumentScanTitle SaveDocumentScanTitle(DocumentScanTitle DocumentScanTitleInfo)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.DocumentScanTitle.Add(DocumentScanTitleInfo);
                    context.SaveChanges();
                    return DocumentScanTitleInfo;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<DocumentScanType> GetAddDocumentScanProperties(int DocumentScanTitleId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var returnresult = context.DocumentScanType.Where(x => x.DocumentScanTitleRefId == DocumentScanTitleId).Select(x => new DocumentScanType() { DocumentTypeTitle = x.DocumentTypeTitle, DocumentType = x.DocumentType, DocumentScanTypeId = x.DocumentScanTypeId, QuestionRefID = x.QuestionRefID }).ToList();
                    return returnresult;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DocumentScanType SaveAddDocumentProperties(DocumentScanType DocumentScanType)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.DocumentScanType.Add(DocumentScanType);
                    context.SaveChanges();

                    return DocumentScanType;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteAddDocumentScanProperties(int AddDocumentScanTypeId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.DocumentScanType.FirstOrDefault(x => x.DocumentScanTypeId == AddDocumentScanTypeId);
                    if (result != null)
                    {
                        var questions = context.Questions.Where(x => x.QuestionId == result.QuestionRefID).FirstOrDefault();
                        if (questions != null)
                        {
                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == questions.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                            if (IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green)
                            {
                                questions.ISModification = (int)EnumCopyColorCode.Red;
                                context.SaveChanges();
                            }
                            else
                            {
                                context.Questions.Remove(questions);
                                context.SaveChanges();

                                context.DocumentScanType.Remove(result);
                                context.SaveChanges();
                            }
                        }
                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<DocumentScanTitle> GetDocumetScanTitle(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.DocumentScanTitle.Where(x => x.QuestionRefId == questionRefId).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DocumentScanTitle UpdateDocumentScanTitle(int DocumentScanTitleId, int QuestionId, string Title, string updatedBy)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.DocumentScanTitle.Where(x => x.DocumetScanTitleId == DocumentScanTitleId).FirstOrDefault();
                    if (result != null)
                    {
                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = QuestionId;
                        objQuestions.Name = Title;
                        objQuestions.UpdatedBy = updatedBy;
                        objQuestions.UpdatedOn = DateTime.Now;
                        UpdateQuestions(objQuestions);

                        result.Title = Title;
                        result.UpdatedBy = updatedBy;
                        result.UpdatedOn = DateTime.Now;
                        context.SaveChanges();


                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DocumentScanType UpdateAddDocumentScanProperties(DocumentScanType AddDocumentScanTypeProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Questions.SingleOrDefault(IC => IC.QuestionId == AddDocumentScanTypeProperties.QuestionRefID);
                    if (result != null)
                    {
                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = AddDocumentScanTypeProperties.QuestionRefID;
                        objQuestions.Name = AddDocumentScanTypeProperties.DocumentTypeTitle;
                        objQuestions.UpdatedBy = AddDocumentScanTypeProperties.UpdatedBy;
                        objQuestions.UpdatedOn = DateTime.Now;
                        UpdateQuestions(objQuestions);

                        if (result.ISModification == 1)
                        {
                            result.ISModification = 2;
                            context.SaveChanges();
                        }

                        var documenttype = context.DocumentScanType.Where(x => x.DocumentScanTypeId == AddDocumentScanTypeProperties.DocumentScanTypeId).FirstOrDefault();
                        if (documenttype != null)
                        {
                            documenttype.DocumentTypeTitle = AddDocumentScanTypeProperties.DocumentTypeTitle;
                            documenttype.DocumentType = AddDocumentScanTypeProperties.DocumentType;
                            documenttype.UpdatedBy = AddDocumentScanTypeProperties.UpdatedBy;
                            documenttype.UpdatedOn = DateTime.Now;
                            context.SaveChanges();
                            AddDocumentScanTypeProperties = documenttype;
                        }
                    }
                    return AddDocumentScanTypeProperties;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        // Code added for Divakar - If value is 'Next Qp' and Start , End and New field will not be shown in select QP dropdownlist
        public List<ActivitiesRegistration> GetActivitiesRegistrationForSetActionValue(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ActivitiesRegistration.Where(x => x.LayoutRefId == LayoutId && x.ActivitiesRegistrationName != "End" && x.ActivitiesRegistrationName != "Start New" && x.ActivitiesRegistrationName != "Start Same").Select(y => new ActivitiesRegistration() { ActivitiesRegistrationId = y.ActivitiesRegistrationId, ActivitiesRegistrationName = y.ActivitiesRegistrationName }).ToList();
                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<SetActionNextViewValues> GetActivitiesRegistrationForSetActionNextViewValue()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.SetActionNextViewValues.Select(y => new SetActionNextViewValues() { Id = y.Id, Name = y.Name }).OrderBy(x => x.Name).ToList();
                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string Insertallowances(int LayoutID, string allowance, string createdby)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var customerID = context.Layout.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).FirstOrDefault();
                    var OBCTypeRefId = context.OBCType.Where(y => y.LayoutRefId == LayoutID).Select(x => x.OBCTypeRefId).FirstOrDefault();
                    var masterID = context.AllowancesMaster.Where(y => y.OBCTypeRefId == OBCTypeRefId && (y.AllowancesMasterDescription + " (" + y.AllowancesCode + ")") == allowance).Select(x => x.AllowancesMasterId).FirstOrDefault();
                    Allowances objallowances = new Allowances();
                    objallowances.LayoutRefId = LayoutID;
                    objallowances.CustomerRefId = customerID;
                    objallowances.AllowancesRefId = masterID;
                    objallowances.CreatedBy = createdby;
                    objallowances.CreatedOn = DateTime.Now;
                    context.Add(objallowances);
                    context.SaveChanges();
                    return "success";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string Updateallowances(int LayoutID, string allowance, int setactionId, string createdby)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var setactionallowance = context.SetActionValueProperties.Where(x => x.SetActionValueId == setactionId).FirstOrDefault();
                    if (setactionallowance.SelectAllowance != allowance)
                    {
                        var OBCTypeRefId = context.OBCType.Where(y => y.LayoutRefId == LayoutID).Select(x => x.OBCTypeRefId).FirstOrDefault();
                        var masterID = context.AllowancesMaster.Where(y => y.OBCTypeRefId == OBCTypeRefId && (y.AllowancesMasterDescription + " (" + y.AllowancesCode + ")") == allowance).Select(x => x.AllowancesMasterId).FirstOrDefault();
                        var customerID = context.Layout.Where(y => y.LayoutId == LayoutID).Select(x => x.LayoutId).FirstOrDefault();
                        Allowances objallowances = new Allowances();
                        objallowances.LayoutRefId = LayoutID;
                        objallowances.CustomerRefId = customerID;
                        objallowances.AllowancesRefId = masterID;
                        objallowances.CreatedBy = createdby;
                        objallowances.CreatedOn = DateTime.Now;
                        context.Add(objallowances);
                        context.SaveChanges();

                        var existmasterID = context.AllowancesMaster.Where(y => y.OBCTypeRefId == OBCTypeRefId && (y.AllowancesMasterDescription + " (" + y.AllowancesCode + ")") == setactionallowance.SelectAllowance).Select(x => x.AllowancesMasterId).FirstOrDefault();
                        var existlist = context.Allowances.Where(x => x.AllowancesRefId == existmasterID && x.LayoutRefId == LayoutID && x.CustomerRefId == customerID).FirstOrDefault();
                        if (existlist != null)
                        {
                            context.Allowances.Remove(existlist);
                            context.SaveChanges();
                        }
                    }

                    return "success";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public int GetOBCTypeForLayout(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = Convert.ToInt32(context.OBCType.Where(x => x.LayoutRefId == LayoutId).Select(y => y.OBCTypeRefId).FirstOrDefault());
                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public bool GetIsTripPalaningType(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.PlanningType.Where(x => x.LayoutRefId == LayoutId && x.PlanningTypeMaster.PlanningTypeMasterDescription == "Trip").FirstOrDefault() == null ? false : true;
                    return result;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string GetActivitiesRegistrationPlanning(int ActivityId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == ActivityId).Select(x => x.PlanningRefId).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // sridhar
        public List<DocumentScanner> GetFlexDocumentScannerForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layOutRefID);

                    var documentScannerRefId = 0;
                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", QuesstionRefID);
                    SqlParameter extraiInfoParam = new SqlParameter("@DocumentScannerRefId", documentScannerRefId);

                    var flexDetailDocumentScannerTranslationDetails = context.FlexDetailDocumentScannerTranslation.FromSql("GetDocumentScanner @QuestionRefId,@DocumentScannerRefId", questionParam, extraiInfoParam).ToList();

                    List<DocumentScanner> documentScannerList = new List<DocumentScanner>();

                    foreach (var documentScannerDetail in flexDetailDocumentScannerTranslationDetails)
                    {
                        DocumentScanner documentScanner = new DocumentScanner();

                        documentScanner.DocumentScannerID = documentScannerDetail.Id;
                        documentScanner.ActivityRefID = documentScannerDetail.ActivityRefID ?? 0;
                        documentScanner.QuestionRefID = documentScannerDetail.QuestionRefID ?? 0;
                        documentScanner.LayoutRefID = documentScannerDetail.LayoutRefID ?? 0;
                        documentScanner.PlaceLevel = documentScannerDetail.PlaceLevel;
                        documentScanner.JobLevel = documentScannerDetail.JobLevel;
                        documentScanner.ProductLevel = documentScannerDetail.ProductLevel;
                        documentScanner.DocumentType = documentScannerDetail.DocumentType;
                        documentScanner.CreatedBy = documentScannerDetail.CreatedBy;
                        documentScanner.CreatedOn = documentScannerDetail.CreatedOn;
                        documentScanner.UpdatedBy = documentScannerDetail.Updatedby;
                        documentScanner.UpdatedOn = documentScannerDetail.UpdatedOn ?? DateTime.Now;
                        documentScanner.Title = GetFlexDetailDocumentScannerTranslationText(documentScannerDetail, defualtLanguage);
                        documentScanner.DocumentScannerRefId = documentScannerDetail.RefId;
                        documentScannerList.Add(documentScanner);
                    }

                    return documentScannerList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<DocumentScanner>();
            }
        }
        public DocumentScanner InsertFlexDocumentScanner(DocumentScanner DocumentScanner)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.DocumentScanner.Add(DocumentScanner);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(DocumentScanner.LayoutRefID);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = DocumentScanner.DocumentScannerID ?? 0;
                    langrefdetail.QuestionRefId = DocumentScanner.DocumentScannerRefId;
                    langrefdetail.LayoutRefId = DocumentScanner.LayoutRefID;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScanner_Title;
                    langrefdetail.LangText = DocumentScanner.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = DocumentScanner.CreatedBy;
                    langrefdetail.UpdatedBy = DocumentScanner.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    /* Written By Selvam Thangaraj - Stop */

                    return DocumentScanner;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateFlexDocumentScanner(DocumentScanner DocumentScanner)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var resultex = context.Questions.SingleOrDefault(IC => IC.QuestionId == DocumentScanner.DocumentScannerRefId);
                    if (resultex != null)
                    {

                        resultex.Name = DocumentScanner.Title;
                        context.SaveChanges();

                        if (resultex.ISModification == 1)
                        {
                            resultex.ISModification = 2;
                            context.SaveChanges();
                        }
                    }
                    var result = context.DocumentScanner.SingleOrDefault(IC => IC.DocumentScannerID == DocumentScanner.DocumentScannerID);
                    if (result != null)
                    {
                        result.PlaceLevel = DocumentScanner.PlaceLevel;
                        result.JobLevel = DocumentScanner.JobLevel;
                        result.UpdatedBy = DocumentScanner.UpdatedBy;
                        result.UpdatedOn = DocumentScanner.UpdatedOn;
                        result.Title = DocumentScanner.Title;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefID);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.DocumentScannerID ?? 0;
                        langrefdetail.QuestionRefId = result.DocumentScannerRefId;
                        langrefdetail.LayoutRefId = result.LayoutRefID;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScanner_Title;
                        langrefdetail.LangText = DocumentScanner.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteFlexDocumentScanner(int DocumentScannerID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.DocumentScanner.Where(x => x.DocumentScannerID == DocumentScannerID).FirstOrDefault();
                    if (result != null)
                    {
                        var questions = context.Questions.Where(x => x.QuestionId == result.DocumentScannerRefId).FirstOrDefault();
                        if (questions != null)
                        {
                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == questions.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                            if (IsCopyLayout && questions.ISModification != (int)EnumCopyColorCode.Green)
                            {
                                questions.ISModification = (int)EnumCopyColorCode.Red;
                                context.SaveChanges();
                            }
                            else
                            {

                                LangRefDetail langrefdetail = new LangRefDetail();
                                langrefdetail.RefId = questions.QuestionId;
                                langrefdetail.LayoutRefId = questions.LayoutRefId;
                                langrefdetail.CustomerRefId = null;
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                langrefdetail = new LangRefDetail();
                                langrefdetail.RefId = result.DocumentScannerID ?? 0;
                                langrefdetail.LayoutRefId = result.LayoutRefID;
                                langrefdetail.CustomerRefId = null;
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScanner_Title;
                                _translatorRepository.DeleteLangDetail(langrefdetail);

                                context.DocumentScanner.Remove(result);
                                context.SaveChanges();

                                context.Questions.Remove(questions);
                                context.SaveChanges();
                            }
                        }
                        return "Deleted";
                    }
                    else
                    {
                        return "Not Deleted";
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public DocumentScannerProperties InsertDocumentScannerProperties(DocumentScannerProperties DocumentScannerProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(DocumentScannerProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */

                    var LanguageName = _translatorRepository.GetDefaultLanguage(DocumentScannerProperties.LayoutRefID);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = DocumentScannerProperties.DocumentScannerPropertiesID ?? 0;
                    langrefdetail.QuestionRefId = DocumentScannerProperties.QuestionRefID;
                    langrefdetail.LayoutRefId = DocumentScannerProperties.LayoutRefID;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_Title;
                    langrefdetail.LangText = DocumentScannerProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = DocumentScannerProperties.CreatedBy;
                    langrefdetail.UpdatedBy = DocumentScannerProperties.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    if (DocumentScannerProperties.AlternativetextonPlacelevel != null && DocumentScannerProperties.AlternativetextonPlacelevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnPlaceLevel;
                        langrefdetail.LangText = DocumentScannerProperties.AlternativetextonPlacelevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (DocumentScannerProperties.AlternativetextonJoblevel != null && DocumentScannerProperties.AlternativetextonJoblevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnJobLevel;
                        langrefdetail.LangText = DocumentScannerProperties.AlternativetextonJoblevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (DocumentScannerProperties.Alternativetextonproductlevel != null && DocumentScannerProperties.Alternativetextonproductlevel.Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnProductLevel;
                        langrefdetail.LangText = DocumentScannerProperties.Alternativetextonproductlevel ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return DocumentScannerProperties;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateDocumentScannerProperties(DocumentScannerProperties DocumentScannerProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.DocumentScannerProperties.SingleOrDefault(IC => IC.DocumentScannerPropertiesID == DocumentScannerProperties.DocumentScannerPropertiesID);
                    if (result != null)
                    {
                        var AlternativetextonJoblevel = result.AlternativetextonJoblevel;
                        var Alternativetextonproductlevel = result.Alternativetextonproductlevel;
                        var AlternativetextonPlacelevel = result.AlternativetextonPlacelevel;

                        result.Title = DocumentScannerProperties.Title;
                        result.AlternativetextonJoblevel = DocumentScannerProperties.AlternativetextonJoblevel;
                        result.AlternativetextonPlacelevel = DocumentScannerProperties.AlternativetextonPlacelevel;
                        result.Alternativetextonproductlevel = DocumentScannerProperties.Alternativetextonproductlevel;
                        result.UpdatedBy = DocumentScannerProperties.UpdatedBy;
                        result.UpdatedOn = DocumentScannerProperties.UpdatedOn;
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefID);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.DocumentScannerPropertiesID ?? 0;
                        langrefdetail.QuestionRefId = result.QuestionRefID;
                        langrefdetail.LayoutRefId = result.LayoutRefID;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if (AlternativetextonPlacelevel != null && AlternativetextonPlacelevel.Length > 0 && result.AlternativetextonPlacelevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = DocumentScannerProperties.DocumentScannerPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnPlaceLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonPlacelevel != null && result.AlternativetextonPlacelevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnPlaceLevel;
                            langrefdetail.LangText = DocumentScannerProperties.AlternativetextonPlacelevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }


                        if (AlternativetextonJoblevel != null && AlternativetextonJoblevel.Length > 0 && result.AlternativetextonJoblevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = DocumentScannerProperties.DocumentScannerPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnJobLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.AlternativetextonJoblevel != null && result.AlternativetextonJoblevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnJobLevel;
                            langrefdetail.LangText = DocumentScannerProperties.AlternativetextonJoblevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        if (Alternativetextonproductlevel != null && Alternativetextonproductlevel.Length > 0 && result.Alternativetextonproductlevel == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = DocumentScannerProperties.DocumentScannerPropertiesID ?? 0;
                            dlangrefdetail.LayoutRefId = result.LayoutRefID;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnProductLevel;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }
                        else if (result.Alternativetextonproductlevel != null && result.Alternativetextonproductlevel.Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnProductLevel;
                            langrefdetail.LangText = DocumentScannerProperties.Alternativetextonproductlevel ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return "Updated";

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<DocumentScannerProperties> GetDocumentScannerProperties(int questionRefId, int layoutRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutRefId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var flexTranslationDetails = context.FlexTranslation.FromSql("GetDocumentScannerProperties @QuestionRefId", questionParam).ToList();
                    var flexAlternativeTextTranslationDetails = context.FlexAlternativeTextTranslation.FromSql("GetDocumentScannerPropertiesAlternativeText @QuestionRefId", questionParam).ToList();

                    List<DocumentScannerProperties> documentScannerPropertiesList = new List<DocumentScannerProperties>();

                    foreach (var extraInfoDetail in flexTranslationDetails)
                    {
                        var alternativetextonPlacelevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnPlaceLevel).FirstOrDefault();
                        var alternativetextonJoblevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnJobLevel).FirstOrDefault();
                        var alternativetextonProductlevel = flexAlternativeTextTranslationDetails.Where(x => x.RefId == extraInfoDetail.Id && x.LangRefColTableId == (int)LangRefColTable.DocumentScannerProperties_AlternativeTextOnProductLevel).FirstOrDefault();

                        DocumentScannerProperties documentScanner = new DocumentScannerProperties();

                        documentScanner.DocumentScannerPropertiesID = extraInfoDetail.Id;
                        documentScanner.ActivityRefID = extraInfoDetail.ActivityRefID ?? 0;
                        documentScanner.LayoutRefID = extraInfoDetail.LayoutRefID ?? 0;
                        documentScanner.CreatedBy = extraInfoDetail.CreatedBy;
                        documentScanner.CreatedOn = extraInfoDetail.CreatedOn;
                        documentScanner.UpdatedBy = extraInfoDetail.Updatedby;
                        documentScanner.UpdatedOn = extraInfoDetail.UpdatedOn;
                        documentScanner.QuestionRefID = extraInfoDetail.QuestionRefID ?? 0;
                        documentScanner.Title = GetFlexTranslationText(extraInfoDetail, defualtLanguage);
                        documentScanner.AlternativetextonPlacelevel = (alternativetextonPlacelevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonPlacelevel, defualtLanguage));
                        documentScanner.AlternativetextonJoblevel = (alternativetextonJoblevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonJoblevel, defualtLanguage));
                        documentScanner.Alternativetextonproductlevel = (alternativetextonProductlevel == null ? "" : GetFlexAlternativeTextTranslationText(alternativetextonProductlevel, defualtLanguage));
                        documentScanner.CopyLayoutRefId = extraInfoDetail.CopyLayoutRefId ?? 0;
                        documentScannerPropertiesList.Add(documentScanner);
                    }

                    return documentScannerPropertiesList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<DocumentScannerProperties>();
            }
        }
        public int GetQuestionsStatus(int QuestionId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var questions = context.Questions.Where(x => x.QuestionId == QuestionId).Select(x => x.ISModification ?? 0).FirstOrDefault();
                    return questions;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public List<ReadoutDetail> GetReadoutDetail(string TockenId, int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var readoutValueDetails = context.ReadoutValue.Where(x => x.ReadoutValueStatus == 1).Select(x => new { x.ReadoutValueDesc, x.ReadoutValueId }).ToList();
                    var aliasNameDetails = context.AliasNameDetails.Where(x => x.LayoutRefId == LayoutId).Select(x => new { x.AliasName, x.AliasNameDetailsId }).ToList();
                    var readoutDetails = context.ReadoutDetail.Where(x => x.TokenId == TockenId).ToList();

                    var readoutDetailList = (from cp in readoutDetails
                                             join lt in readoutValueDetails on cp.ReadoutValueRefId equals lt.ReadoutValueId
                                                      into joinlt
                                             from dlt in joinlt.DefaultIfEmpty(new { ReadoutValueDesc = "", ReadoutValueId = cp.ReadoutValueRefId })
                                             join ad in aliasNameDetails on (cp.SaveTo ?? 0) equals ad.AliasNameDetailsId
                                             into joinad
                                             from dad in joinad.DefaultIfEmpty(new { AliasName = "", AliasNameDetailsId = cp.SaveTo ?? 0 })
                                             select new ReadoutDetail()
                                             {
                                                 ReadoutValueRefId = cp.ReadoutValueRefId,
                                                 ReadoutValue = (dlt.ReadoutValueDesc + " " + (cp.ConfigID == null ? (cp.CommentID == null ? "" : cp.CommentID.ToString()) : cp.ConfigID.ToString())),
                                                 ConfigID = cp.ConfigID ?? 0,
                                                 ConfigIDValue = (cp.ConfigID == null ? "" : cp.ConfigID.ToString()),
                                                 CommentID = cp.CommentID ?? 0,
                                                 CommentIDValue = (cp.CommentID == null ? "" : cp.CommentID.ToString()),
                                                 ReadoutRange = cp.ReadoutRange,
                                                 StartChar = cp.StartChar ?? 0,
                                                 StartCharValue = (cp.StartChar == null ? "" : cp.StartChar.ToString()),
                                                 CharLength = cp.CharLength ?? 0,
                                                 CharLengthValue = (cp.CharLength == null ? "" : cp.CharLength.ToString()),
                                                 ReadoutExample = cp.ReadoutExample,
                                                 TokenId = cp.TokenId,
                                                 CreatedBy = "",
                                                 CreatedOn = DateTime.Now,
                                                 SaveTo = cp.SaveTo,
                                                 SaveToValue = dad.AliasName,
                                                 ReadoutDetailId = cp.ReadoutDetailId

                                             }).ToList();

                    return readoutDetailList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ReadoutDetail SaveReadoutDetail(ReadoutDetail readoutDetail)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    if (readoutDetail.ReadoutDetailId == 0)
                    {
                        context.Add(readoutDetail);
                        context.SaveChanges();
                        return readoutDetail;
                    }
                    else
                    {
                        var objReadout = context.ReadoutDetail.Where(x => x.ReadoutDetailId == readoutDetail.ReadoutDetailId).FirstOrDefault();
                        if (objReadout != null)
                        {
                            objReadout.ReadoutValueRefId = readoutDetail.ReadoutValueRefId;
                            objReadout.CommentID = readoutDetail.CommentID;
                            objReadout.ConfigID = readoutDetail.ConfigID;
                            objReadout.ReadoutRange = readoutDetail.ReadoutRange;
                            objReadout.StartChar = readoutDetail.StartChar;
                            objReadout.CharLength = readoutDetail.CharLength;
                            objReadout.ReadoutExample = readoutDetail.ReadoutExample;
                            objReadout.TokenId = readoutDetail.TokenId;
                            objReadout.SaveTo = readoutDetail.SaveTo;
                            objReadout.IsDefaultAlaisName = readoutDetail.IsDefaultAlaisName;
                            context.SaveChanges();

                        }
                        return objReadout;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteReadoutDetail(int ReadoutDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var readoutDeatil = context.ReadoutDetail.Where(x => x.ReadoutDetailId == ReadoutDetailId).FirstOrDefault();
                    if (readoutDeatil != null)
                    {
                        context.ReadoutDetail.Remove(readoutDeatil);
                        context.SaveChanges();
                    }

                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<ReadoutDetail> EditReadoutDetail(int ReadoutDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var readoutDeatil = context.ReadoutDetail.Where(x => x.ReadoutDetailId == ReadoutDetailId).ToList();
                    if (readoutDeatil.Any())
                        return readoutDeatil;
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string SavePlanningSelectReadoutDetail(int PlanningSelectId, string TokenId, string UserId, DateTime CreatedOn)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var readoutList = context.ReadoutDetail.Where(x => x.TokenId == TokenId).ToList();
                    foreach (var readoutDetail in readoutList)
                    {
                        PlanningSelectReadoutDetail objReadout = new PlanningSelectReadoutDetail();
                        objReadout.PlanningSelectRefId = PlanningSelectId;
                        objReadout.ReadoutValueRefId = readoutDetail.ReadoutValueRefId;
                        objReadout.CommentID = readoutDetail.CommentID;
                        objReadout.ConfigID = readoutDetail.ConfigID;
                        objReadout.ReadoutRange = readoutDetail.ReadoutRange;
                        objReadout.StartChar = readoutDetail.StartChar;
                        objReadout.CharLength = readoutDetail.CharLength;
                        objReadout.ReadoutExample = readoutDetail.ReadoutExample;
                        objReadout.SaveTo = readoutDetail.SaveTo; // alias pK Id
                        objReadout.IsDefaultAlaisName = readoutDetail.IsDefaultAlaisName;
                        objReadout.CreatedBy = UserId;
                        objReadout.CreatedOn = CreatedOn;

                        var planningReadOutResult = context.PlanningSelectReadoutDetail.Add(objReadout);
                        context.SaveChanges();
                        if (objReadout.SaveTo > 0)
                        {
                            UpdateAliasPropertyTableIdForReadout(objReadout.SaveTo ?? 0, objReadout.PlanningSelectReadoutDetailId);
                        }

                    }
                    foreach (var readoutDetail in readoutList)
                    {
                        context.ReadoutDetail.Remove(readoutDetail);
                        context.SaveChanges();
                    }
                    return "";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public PlanningSelectReadoutDetail UpdatePlanningSelectReadoutDetail(PlanningSelectReadoutDetail readoutDetail)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (readoutDetail.PlanningSelectReadoutDetailId == 0)
                    {
                        context.Add(readoutDetail);
                        context.SaveChanges();
                        if (readoutDetail.SaveTo > 0)
                        {
                            UpdateAliasPropertyTableIdForReadout(readoutDetail.SaveTo ?? 0, readoutDetail.PlanningSelectReadoutDetailId);
                        }

                        return readoutDetail;
                    }
                    else
                    {
                        var objReadout = context.PlanningSelectReadoutDetail.Where(x => x.PlanningSelectReadoutDetailId == readoutDetail.PlanningSelectReadoutDetailId).FirstOrDefault();
                        if (objReadout != null)
                        {
                            objReadout.ReadoutValueRefId = readoutDetail.ReadoutValueRefId;
                            objReadout.CommentID = readoutDetail.CommentID;
                            objReadout.ConfigID = readoutDetail.ConfigID;
                            objReadout.ReadoutRange = readoutDetail.ReadoutRange;
                            objReadout.StartChar = readoutDetail.StartChar;
                            objReadout.CharLength = readoutDetail.CharLength;
                            objReadout.ReadoutExample = readoutDetail.ReadoutExample;
                            objReadout.PlanningSelectRefId = readoutDetail.PlanningSelectRefId;
                            objReadout.SaveTo = readoutDetail.SaveTo;
                            objReadout.IsDefaultAlaisName = readoutDetail.IsDefaultAlaisName;
                            objReadout.UpdatedBy = readoutDetail.UpdatedBy;
                            objReadout.UpdatedOn = readoutDetail.UpdatedOn;

                            context.SaveChanges();
                            if (objReadout.SaveTo > 0)
                            {
                                UpdateAliasPropertyTableIdForReadout(objReadout.SaveTo ?? 0, objReadout.PlanningSelectReadoutDetailId);
                            }

                            context.SaveChanges();
                        }
                        return objReadout;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PlanningSelectReadoutDetail> GetpReadoutDetail(int PlanningSelectRefId, int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var readoutValueDetails = context.ReadoutValue.Where(x => x.ReadoutValueStatus == 1).Select(x => new { x.ReadoutValueDesc, x.ReadoutValueId }).ToList();
                    var aliasNameDetails = context.AliasNameDetails.Where(x => x.LayoutRefId == LayoutId).Select(x => new { x.AliasName, x.AliasNameDetailsId }).ToList();
                    var readoutDetails = context.PlanningSelectReadoutDetail.Where(x => x.PlanningSelectRefId == PlanningSelectRefId).ToList();

                    var readoutDetailList = (from cp in readoutDetails
                                             join lt in readoutValueDetails on cp.ReadoutValueRefId equals lt.ReadoutValueId
                                                      into joinlt
                                             from dlt in joinlt.DefaultIfEmpty(new { ReadoutValueDesc = "", ReadoutValueId = cp.ReadoutValueRefId })
                                             join ad in aliasNameDetails on (cp.SaveTo ?? 0) equals ad.AliasNameDetailsId
                                             into joinad
                                             from dad in joinad.DefaultIfEmpty(new { AliasName = "", AliasNameDetailsId = cp.SaveTo ?? 0 })
                                             select new PlanningSelectReadoutDetail()
                                             {
                                                 PlanningSelectRefId = cp.PlanningSelectRefId,
                                                 ReadoutValueRefId = cp.ReadoutValueRefId,
                                                 ReadoutValue = (dlt.ReadoutValueDesc + " " + (cp.ConfigID == null ? (cp.CommentID == null ? "" : cp.CommentID.ToString()) : cp.ConfigID.ToString())),
                                                 ConfigID = cp.ConfigID ?? 0,
                                                 ConfigIDValue = (cp.ConfigID == null ? "" : cp.ConfigID.ToString()),
                                                 CommentID = cp.CommentID ?? 0,
                                                 CommentIDValue = (cp.CommentID == null ? "" : cp.CommentID.ToString()),
                                                 ReadoutRange = cp.ReadoutRange,
                                                 StartChar = cp.StartChar ?? 0,
                                                 StartCharValue = (cp.StartChar == null ? "" : cp.StartChar.ToString()),
                                                 CharLength = cp.CharLength ?? 0,
                                                 CharLengthValue = (cp.CharLength == null ? "" : cp.CharLength.ToString()),
                                                 ReadoutExample = cp.ReadoutExample,
                                                 CreatedBy = "",
                                                 CreatedOn = DateTime.Now,
                                                 SaveTo = cp.SaveTo,
                                                 SaveToValue = dad.AliasName,
                                                 PlanningSelectReadoutDetailId = cp.PlanningSelectReadoutDetailId

                                             }).ToList();

                    return readoutDetailList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeletePlanningSelectReadoutDetail(int PlanningSelectReadoutDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var readoutDeatil = context.PlanningSelectReadoutDetail.Where(x => x.PlanningSelectReadoutDetailId == PlanningSelectReadoutDetailId).FirstOrDefault();
                    if (readoutDeatil != null)
                    {
                        context.PlanningSelectReadoutDetail.Remove(readoutDeatil);
                        context.SaveChanges();
                    }

                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<PlanningSelectReadoutDetail> EditPlanningSelectReadoutDetail(int PlanningSelectReadoutDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var readoutDeatil = context.PlanningSelectReadoutDetail.Where(x => x.PlanningSelectReadoutDetailId == PlanningSelectReadoutDetailId).ToList();
                    if (readoutDeatil.Any())
                        return readoutDeatil;
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<InfoMessageIconType> GetInfoMessageIconType()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.InfoMessageIconType.Where(x => x.IsActive == true).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateAliasPropertyTableId(int aliasNameDetailsId, int propertyTableId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.AliasNameDetails.Where(x => x.AliasNameDetailsId == aliasNameDetailsId).FirstOrDefault();
                    if (result != null)
                    {
                        result.PropertyTableId = propertyTableId;
                        context.SaveChanges();
                    }
                    return "Updated";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateAliasPropertyTableIdForReadout(int aliasNameDetailsId, int propertyTableId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.AliasNameDetails.Where(x => x.AliasNameDetailsId == aliasNameDetailsId).FirstOrDefault();
                    if (result != null)
                    {
                        result.PropertyTableId = propertyTableId;
                        result.IsTempData = false;
                        context.SaveChanges();
                    }
                    return "Updated";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool DeleteUnAliasNames(AliasNameDetails aliasNames, string propertyName, int propertyId, int activityId)
        {
            bool isDeleted = true;
            using (var context = new Creatis_Context())
            {
                if (aliasNames != null)
                {
                    //var planningSelectReadOutRes = context.PlanningSelectReadoutDetail.Where(x => x.PlanningSelectReadoutDetailId == propertyId).FirstOrDefault();
                    //int planningSelectActivityId = 0;
                    //if (activityId != 0)
                    //{
                    //    planningSelectActivityId = context.PlanningselectProperties.Where(x => x.PlanningselectId == planningSelectReadOutRes.PlanningSelectRefId).Select(x => x.ActivitiesRefId).FirstOrDefault();
                    //}

                    if (context.ChioceProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.Saveto == aliasNames.AliasNameDetailsId && (propertyName == "Choice" ? (x.ChioceId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.ChioceProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.Propose == aliasNames.AliasNameDetailsId.ToString() && (propertyName == "Choice" ? (x.ChioceId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.ChioceProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConSavedValued == aliasNames.AliasNameDetailsId && (propertyName == "Choice" ? (x.ChioceId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.ChioceProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.comtoSavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Choice" ? (x.ChioceId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.EntryProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.SaveTo == aliasNames.AliasNameDetailsId && (propertyName == "Entry" ? (x.EntryId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.EntryProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.Propose == aliasNames.AliasNameDetailsId && (propertyName == "Entry" ? (x.EntryId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.EntryProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConItemSavedValued == aliasNames.AliasNameDetailsId && (propertyName == "Entry" ? (x.EntryId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.EntryProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConvalueSavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Entry" ? (x.EntryId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.PlanningselectProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.InstructionSetSavedValues == aliasNames.AliasNameDetailsId && (propertyName == "Planning Select" ? (x.PlanningselectId != propertyId) : (1 == 1))).Any())
                        return false;
                    //else if (context.PlanningSelectReadoutDetail.Where(x => (activityId > 0 ? planningSelectActivityId != activityId : (1 == 1)) && x.SaveTo == aliasNames.AliasNameDetailsId && (propertyName == "Planning Readout" ? (x.PlanningSelectReadoutDetailId != propertyId) : (1 == 1))).Any())
                    //    return false;
                    else if (context.ConditionalProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConSavedValued == aliasNames.AliasNameDetailsId && (propertyName == "Conditional Action" ? (x.ConditionalID != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.ConditionalProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.comtoSavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Conditional Action" ? (x.ConditionalID != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.SetActionValueProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConItemSavedValued == aliasNames.AliasNameDetailsId && (propertyName == "Set Action/value" ? (x.SetActionValueId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.SetActionValueProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConvalueSavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Set Action/value" ? (x.SetActionValueId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.SetActionValueProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.CopySavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Set Action/value" ? (x.SetActionValueId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.SetActionValueProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.Latitude == aliasNames.AliasNameDetailsId && (propertyName == "Set Action/value" ? (x.SetActionValueId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.SetActionValueProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.Longitude == aliasNames.AliasNameDetailsId && (propertyName == "Set Action/value" ? (x.SetActionValueId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.SetActionValueProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.PlanningIDOBC == aliasNames.AliasNameDetailsId && (propertyName == "Set Action/value" ? (x.SetActionValueId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.SetActionValueProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.SelectTargetSavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Set Action/value" ? (x.SetActionValueId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.SetActionValueProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.InfoColumnSavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Set Action/value" ? (x.SetActionValueId != propertyId) : (1 == 1))).Any())
                        return false;

                    else if (context.InfomessageProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConItemSavedValued == aliasNames.AliasNameDetailsId && (propertyName == "Info Messagee" ? (x.InfomessageId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.InfomessageProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConvalueSavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Info Message" ? (x.InfomessageId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.TextMessageProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConItemSavedValued == aliasNames.AliasNameDetailsId && (propertyName == "Text Message" ? (x.TextMessageId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.TextMessageProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConvalueSavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Text Message" ? (x.TextMessageId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.VariableListProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConditionSavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Set Action/value" ? (x.VariableListId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.VariableListProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConditionItemValue == aliasNames.AliasNameDetailsId && (propertyName == "Info Messagee" ? (x.VariableListId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.VariableListProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.CompareToSavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Info Message" ? (x.VariableListId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.DocumetScanProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConItemSavedValued == aliasNames.AliasNameDetailsId && (propertyName == "Text Message" ? (x.DocumetScanId != propertyId) : (1 == 1))).Any())
                        return false;
                    else if (context.DocumetScanProperties.Where(x => (activityId > 0 ? x.ActivitiesRefId != activityId : (1 == 1)) && x.ConvalueSavedValue == aliasNames.AliasNameDetailsId && (propertyName == "Text Message" ? (x.DocumetScanId != propertyId) : (1 == 1))).Any())
                        return false;

                }

                return isDeleted;
            }
        }
        public string GetPropertyNameByQuestionId(int QuestionID)
        {
            using (var context = new Creatis_Context())
            {
                var questions = context.Questions.Where(x => x.QuestionId == QuestionID).Select(x => x.PropertyName).FirstOrDefault();
                return questions;
            }
        }

        public Questions GetQuestionsByQuestionId(int QuestionID)
        {
            using (var context = new Creatis_Context())
            {
                var questions = context.Questions.Where(x => x.QuestionId == QuestionID).FirstOrDefault();
                return questions ?? new Questions();
            }
        }
        public TransportTypeProperties SaveTransportTypeProperties(TransportTypeProperties TransportTypeProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var transportTypeId = TransportTypeProperties.DashboardTransportTypeId;

                    context.Add(TransportTypeProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */
                    var LanguageName = _translatorRepository.GetDefaultLanguage(TransportTypeProperties.LayoutRefId);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = TransportTypeProperties.TransportTypeId;
                    langrefdetail.QuestionRefId = TransportTypeProperties.QuestionRefId;
                    langrefdetail.LayoutRefId = TransportTypeProperties.LayoutRefId;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TransportTypeProperties_Title;
                    langrefdetail.LangText = TransportTypeProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = TransportTypeProperties.CreatedBy;
                    langrefdetail.UpdatedBy = TransportTypeProperties.CreatedBy;
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    /* Written By Selvam Thangaraj - Stop */

                    var transportTypes = context.TransportTypes.Where(x => x.LayoutRefId == TransportTypeProperties.LayoutRefId && x.IsActive).Select(x => new { x.TransportTypeId, x.TransportTypeName }).OrderBy(x => x.TransportTypeId).ToList();

                    if (TransportTypeProperties.AddDefault == true)
                        transportTypes.Add(new { TransportTypeId = 999999, TransportTypeName = "Default" });

                    foreach (var tt in transportTypes.OrderBy(x => x.TransportTypeId))
                    {
                        SaveTransportTypePropertiesDetail(TransportTypeProperties, transportTypeId, tt.TransportTypeId, tt.TransportTypeName, LanguageName);
                    }

                    return TransportTypeProperties;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public TransportTypeProperties SaveDefaultTransportTypePropertiesDetail(TransportTypeProperties transportTypeProperties, int dashboardTransportTypeId, int transportTypeId, string transportTypeDescription, string languageName)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defaultTransportDetailCount = context.TransportTypePropertiesDetail.Where(x => x.TransportTypeRefId == transportTypeProperties.TransportTypeId && x.Title == "Default").Count();
                    if (defaultTransportDetailCount == 0 && transportTypeProperties.AddDefault == true)
                    {
                        SaveTransportTypePropertiesDetail(transportTypeProperties, dashboardTransportTypeId, transportTypeId, transportTypeDescription, languageName);
                    }
                    else if (defaultTransportDetailCount > 0 && transportTypeProperties.AddDefault == false)
                    {
                        var questionRefId = context.TransportTypePropertiesDetail.Where(x => x.TransportTypeRefId == transportTypeProperties.TransportTypeId && x.Title == "Default").Select(x => x.QuestionRefId).FirstOrDefault();
                        DeleteSpecificQuestionTree(questionRefId);
                    }

                    return transportTypeProperties;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public void SaveTransportTypePropertiesDetail(TransportTypeProperties transportTypeProperties, int dashboardTransportTypeId, int transportTypeId, string transportTypeDescription, string languageName)
        {
            using (var context = new Creatis_Context())
            {
                LangRefDetail langrefdetail = new LangRefDetail();

                if (dashboardTransportTypeId == 9999999)
                    dashboardTransportTypeId = context.Questions.Where(x => x.QuestionId == transportTypeProperties.QuestionRefId).Select(x => x.TransporttypeRefId).FirstOrDefault();

                if (languageName == "-")
                    languageName = _translatorRepository.GetDefaultLanguage(transportTypeProperties.LayoutRefId);

                var objQuestion = new Questions();
                objQuestion.ActivitiesRefId = transportTypeProperties.ActivitiesRefId;
                objQuestion.CreatedBy = transportTypeProperties.CreatedBy;
                objQuestion.CreatedOn = transportTypeProperties.CreatedOn;
                objQuestion.LayoutRefId = transportTypeProperties.LayoutRefId;
                objQuestion.Name = transportTypeDescription;
                objQuestion.ParentId = transportTypeProperties.QuestionRefId;
                objQuestion.TransporttypeRefId = dashboardTransportTypeId;
                objQuestion.PropertyName = "Transport Type Detail";
                objQuestion.ISModification = 0;
                objQuestion.IsQuestionPath = true;
                objQuestion.IsParent = false;
                objQuestion.IsDefault = true;
                objQuestion.IsChild = true;
                var questions = SaveQuestions(objQuestion);

                TransportTypePropertiesDetail objAddTT = new TransportTypePropertiesDetail();
                objAddTT.Title = transportTypeDescription;
                objAddTT.PlanningFeedbackCode = transportTypeProperties.PlanningFeedbackCode;
                objAddTT.TransportTypeRefId = transportTypeProperties.TransportTypeId;
                objAddTT.InfoColumn = null;
                objAddTT.InfoColumnAction = "Not set";
                objAddTT.CreatedBy = transportTypeProperties.CreatedBy;
                objAddTT.CreatedOn = transportTypeProperties.CreatedOn;
                objAddTT.PartnerCode = transportTypeProperties.PartnerCode;
                objAddTT.QuestionRefId = questions.QuestionId;
                objAddTT.DashboardTransportTypeId = (transportTypeId == 999999 ? 0 : transportTypeId);
                context.TransportTypePropertiesDetail.Add(objAddTT);
                context.SaveChanges();

                /* Written By Selvam Thangaraj - Go */

                langrefdetail = new LangRefDetail();
                langrefdetail.RefId = objAddTT.TransportTypeDetailId;
                langrefdetail.QuestionRefId = transportTypeProperties.QuestionRefId;
                langrefdetail.LayoutRefId = transportTypeProperties.LayoutRefId;
                langrefdetail.CustomerRefId = null;
                langrefdetail.LangRefColTableId = (int)LangRefColTable.TransportTypePropertiesDetail_Title;
                langrefdetail.LangText = transportTypeDescription;
                langrefdetail.Language = languageName ?? LanguagesEnum.English.ToString();
                langrefdetail.CreatedBy = transportTypeProperties.CreatedBy;
                langrefdetail.UpdatedBy = transportTypeProperties.CreatedBy;
                _translatorRepository.SaveLangDetail(langrefdetail);

                /* Written By Selvam Thangaraj - Stop */
            }
        }
        public List<TransportTypePropertiesDetail> GetTransportTypePropertiesDetail(int transportTypeRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter transporttypeparam = new SqlParameter("@TransportTypeRefId", transportTypeRefId);

                    var transportTypeTranslationDetails = context.TransportTypePropertiesDetailTranslation.FromSql("GetTransportTypePropertiesDetail @TransportTypeRefId", transporttypeparam).ToList();
                    List<TransportTypePropertiesDetail> transportTypePropertiesList = new List<TransportTypePropertiesDetail>();

                    foreach (var transportTypeinfo in transportTypeTranslationDetails)
                    {
                        TransportTypePropertiesDetail transportType = new TransportTypePropertiesDetail();

                        transportType.Title = GetTransportTypePropertiesDetailTitle(transportTypeinfo, defualtLanguage);
                        transportType.TransportTypeDetailId = transportTypeinfo.TransportTypeDetailId;
                        transportType.TransportTypeRefId = transportTypeinfo.TransportTypeRefId;
                        transportType.QuestionRefId = transportTypeinfo.QuestionRefId;
                        transportType.InfoColumn = transportTypeinfo.InfoColumn;
                        transportType.InfoColumnAction = transportTypeinfo.InfoColumnAction;
                        transportType.PlanningFeedbackCode = transportTypeinfo.PlanningFeedbackCode;
                        transportType.PartnerCode = transportTypeinfo.PartnerCode;
                        transportType.CreatedBy = transportTypeinfo.CreatedBy;
                        transportType.CreatedOn = transportTypeinfo.CreatedOn;
                        transportType.UpdatedBy = transportTypeinfo.UpdatedBy;
                        transportType.UpdatedOn = transportTypeinfo.UpdatedOn;
                        transportType.DashboardTransportTypeId = transportTypeinfo.DashboardTransportTypeId ?? 0;

                        transportTypePropertiesList.Add(transportType);
                    }

                    return transportTypePropertiesList;
                }
            }
            catch (Exception ex)
            {
                return new List<TransportTypePropertiesDetail>();
            }
        }
        public string GetTransportTypePropertiesDetailTitle(TransportTypePropertiesDetailTranslation transportTypePropertiesDetailTranslation, string defaultLanguage)
        {
            var transportTypeTitle = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesDetailTranslation.Belarusian;
            }
            else
            {
                transportTypeTitle = "";
            }

            if (transportTypeTitle == null || transportTypeTitle == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == transportTypePropertiesDetailTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.Belarusian;
                    }
                    else
                    {
                        transportTypeTitle = "@" + transportTypePropertiesDetailTranslation.English;
                    }
                }
            }
            return transportTypeTitle;
        }
        public TransportTypePropertiesDetail UpdateTransportTypePropertiesDetail(TransportTypePropertiesDetail TransportTypePropertiesDetails)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Questions.SingleOrDefault(IC => IC.QuestionId == TransportTypePropertiesDetails.QuestionRefId);
                    if (result != null)
                    {

                        result.Name = TransportTypePropertiesDetails.Title;
                        context.SaveChanges();

                        if (result.ISModification == 1)
                        {
                            result.ISModification = 2;
                            context.SaveChanges();
                        }
                        TransportTypePropertiesDetails.InfoColumnAction = null;
                        context.Update(TransportTypePropertiesDetails);
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = TransportTypePropertiesDetails.TransportTypeDetailId;
                        langrefdetail.QuestionRefId = TransportTypePropertiesDetails.QuestionRefId;
                        langrefdetail.LayoutRefId = result.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.TransportTypePropertiesDetail_Title;
                        langrefdetail.LangText = TransportTypePropertiesDetails.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = TransportTypePropertiesDetails.UpdatedBy;
                        langrefdetail.UpdatedBy = TransportTypePropertiesDetails.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return TransportTypePropertiesDetails;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<TransportTypeProperties> GetTransportTypeProperties(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);

                    var transportTypeTranslationDetails = context.TransportTypePropertiesTranslation.FromSql("GetTransportTypeProperties @QuestionRefId", questionParam).ToList();
                    List<TransportTypeProperties> transportTypePropertiesList = new List<TransportTypeProperties>();

                    foreach (var transportTypeinfo in transportTypeTranslationDetails)
                    {
                        TransportTypeProperties transportType = new TransportTypeProperties();

                        transportType.TransportTypeId = transportTypeinfo.TransportTypeId;
                        transportType.ActivitiesRefId = transportTypeinfo.ActivitiesRefId;
                        transportType.LayoutRefId = transportTypeinfo.LayoutRefId;
                        transportType.QuestionRefId = transportTypeinfo.QuestionRefId;
                        transportType.Title = GetTransportTypePropertiesTitle(transportTypeinfo, defualtLanguage);
                        transportType.Save = transportTypeinfo.Save;
                        transportType.SaveTo = transportTypeinfo.SaveTo;
                        transportType.Action = transportTypeinfo.Action;
                        transportType.Comment = transportTypeinfo.Comment;
                        transportType.InfoColumn = transportTypeinfo.InfoColumn;
                        transportType.InfoColumnAction = transportTypeinfo.InfoColumnAction;
                        transportType.PlanningFeedback = transportTypeinfo.PlanningFeedback;
                        transportType.FeedbackType = transportTypeinfo.FeedbackType;
                        transportType.PlanningFeedbackCode = transportTypeinfo.PlanningFeedbackCode;
                        transportType.PartnerCode = transportTypeinfo.PartnerCode;
                        transportType.ConditionValue = transportTypeinfo.ConditionValue;
                        transportType.ConditionSavedValue = transportTypeinfo.ConditionSavedValue;
                        transportType.CompareMethod = transportTypeinfo.CompareMethod;
                        transportType.CompareTo = transportTypeinfo.CompareTo;
                        transportType.CompareSavedValue = transportTypeinfo.CompareSavedValue;
                        transportType.CompareFixedText = transportTypeinfo.CompareFixedText;
                        transportType.CreatedBy = transportTypeinfo.CreatedBy;
                        transportType.CreatedOn = transportTypeinfo.CreatedOn;
                        transportType.UpdatedBy = transportTypeinfo.UpdatedBy;
                        transportType.UpdatedOn = transportTypeinfo.UpdatedOn;
                        transportType.CopyLayoutRefId = transportTypeinfo.CopyLayoutRefId;

                        transportTypePropertiesList.Add(transportType);
                    }

                    return transportTypePropertiesList;
                }
            }
            catch (Exception ex)
            {
                return new List<TransportTypeProperties>();
            }
        }
        public string GetTransportTypePropertiesTitle(TransportTypePropertiesTranslation transportTypePropertiesTranslation, string defaultLanguage)
        {
            var transportTypeTitle = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                transportTypeTitle = transportTypePropertiesTranslation.Belarusian;
            }
            else
            {
                transportTypeTitle = "";
            }

            if (transportTypeTitle == null || transportTypeTitle == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == transportTypePropertiesTranslation.LanguageName)
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.Belarusian;
                    }
                    else
                    {
                        transportTypeTitle = "@" + transportTypePropertiesTranslation.English;
                    }
                }
            }
            return transportTypeTitle;
        }
        public TransportTypeProperties UpdateTransportTypeProperties(TransportTypeProperties TransportTypeProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.TransportTypeProperties.SingleOrDefault(IC => IC.TransportTypeId == TransportTypeProperties.TransportTypeId);
                    if (result != null)
                    {
                        var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == result.QuestionRefId);
                        if (Question.ISModification == 1)
                        {
                            Question.ISModification = 2;
                            context.SaveChanges();
                        }
                        result.Title = TransportTypeProperties.Title;
                        result.Save = TransportTypeProperties.Save;
                        result.PlanningFeedback = TransportTypeProperties.PlanningFeedback;
                        result.PlanningFeedbackCode = TransportTypeProperties.PlanningFeedbackCode;
                        result.FeedbackType = TransportTypeProperties.FeedbackType;
                        result.ConditionValue = TransportTypeProperties.ConditionValue;
                        result.CompareMethod = TransportTypeProperties.CompareMethod;
                        result.CompareTo = TransportTypeProperties.CompareTo;
                        result.InfoColumn = TransportTypeProperties.InfoColumn;
                        result.InfoColumnAction = TransportTypeProperties.InfoColumnAction;
                        result.ConditionSavedValue = TransportTypeProperties.ConditionSavedValue;
                        result.CompareSavedValue = TransportTypeProperties.CompareSavedValue;
                        result.CompareFixedText = TransportTypeProperties.CompareFixedText;
                        result.Action = TransportTypeProperties.Action;
                        result.UpdatedBy = TransportTypeProperties.UpdatedBy;
                        result.UpdatedOn = TransportTypeProperties.UpdatedOn;
                        result.PartnerCode = TransportTypeProperties.PartnerCode;
                        result.SaveTo = TransportTypeProperties.SaveTo;
                        result.Comment = TransportTypeProperties.Comment;

                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.TransportTypeId;
                        langrefdetail.QuestionRefId = result.QuestionRefId;
                        langrefdetail.LayoutRefId = result.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.TransportTypeProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool CheckEndRecursiveCheckedOrNot(int choiceid)
        {
            using (var context = new Creatis_Context())
            {
                bool result = false;
                var choiceEndRecursive = context.ChioceProperties.Where(x => x.ChioceId == choiceid).Select(x => x.Recursiveloop).FirstOrDefault();
                if (choiceEndRecursive == 1)
                {
                    var choiceDetailsList = context.AddChioceProperties.Where(x => x.ChioceRefId == choiceid).ToList();
                    if (choiceDetailsList != null)
                    {
                        foreach (var choiceDetails in choiceDetailsList)
                        {
                            if (choiceDetails.Endrecursiveloop == true)
                            {
                                result = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }
        public bool CheckEndRecCheckedOrNotForChoiceid(int choiceid)
        {
            using (var context = new Creatis_Context())
            {
                bool result = false;
                var choiceEndRecursive = context.ChioceProperties.Where(x => x.ChioceId == choiceid).Select(x => x.Recursiveloop).FirstOrDefault();
                if (choiceEndRecursive == 1)
                {
                    result = true;
                }

                return result;
            }
        }
        public bool GetAddChoiceDetailsByChoiceId(int ChioceRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var addChoiceDetailsPropertiesList = context.AddChioceProperties.Where(x => x.ChioceRefId == ChioceRefId).ToList();
                    bool result = false;
                    if (addChoiceDetailsPropertiesList.Count > 0)
                        result = true;
                    else
                        result = false;
                    return result;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public TrailerProperties SaveTrailerProperties(TrailerProperties TrailerProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    TrailerProperties result = new TrailerProperties();

                    result.ActionType = TrailerProperties.ActionType;
                    result.QuestionRefId = TrailerProperties.QuestionRefId;
                    result.LayoutRefId = TrailerProperties.LayoutRefId;
                    result.ActivitiesRefId = TrailerProperties.ActivitiesRefId;
                    result.Name = TrailerProperties.Name;
                    if (result.ActionType == "Connect")
                    {
                        result.Title = TrailerProperties.Title;
                        result.MaskText = TrailerProperties.MaskText;
                        result.Size = TrailerProperties.Size;
                        result.NullNotAllowed = TrailerProperties.NullNotAllowed;
                        result.MinimumofCharacter = TrailerProperties.MinimumofCharacter;
                        result.MinimumofCharacterValue = TrailerProperties.MinimumofCharacterValue;
                        result.ExactOfCharacter = TrailerProperties.ExactOfCharacter;
                        result.ZeroNotAllowed = TrailerProperties.ZeroNotAllowed;
                        result.Regex = TrailerProperties.Regex;
                        result.Save = TrailerProperties.Save;
                        result.SaveTo = TrailerProperties.SaveTo;
                        result.Propose = TrailerProperties.Propose;
                        result.PlanningFeedback = TrailerProperties.PlanningFeedback;
                        result.FeedbackType = TrailerProperties.FeedbackType;
                        result.PlanningFeedbackCode = TrailerProperties.PlanningFeedbackCode;
                        result.TrailerFormat = TrailerProperties.TrailerFormat;
                        result.PartnerCode = TrailerProperties.PartnerCode;
                        result.AutoGenerate = TrailerProperties.AutoGenerate;

                        result.Other = null;
                    }
                    else
                    {
                        if (result.ActionType == "Disconnect")
                        {
                            result.Title = TrailerProperties.Name;
                            result.Save = null;
                            result.SaveTo = null;
                            result.Other = null;
                        }
                        else if (result.ActionType == "WTR")
                        {
                            result.Title = TrailerProperties.Title;
                            result.Other = TrailerProperties.Other;
                            result.Save = TrailerProperties.Save;
                            result.SaveTo = TrailerProperties.SaveTo;
                        }

                        result.MaskText = null;
                        result.Size = null;
                        result.NullNotAllowed = null;
                        result.MinimumofCharacter = null;
                        result.MinimumofCharacterValue = null;
                        result.ExactOfCharacter = null;
                        result.ZeroNotAllowed = null;
                        result.Regex = null;
                        result.Propose = null;
                        result.PlanningFeedback = null;
                        result.FeedbackType = null;
                        result.PlanningFeedbackCode = null;
                        result.TrailerFormat = null;
                        result.PartnerCode = null;
                        result.AutoGenerate = null;
                    }


                    result.CreatedBy = TrailerProperties.CreatedBy;
                    result.CreatedOn = TrailerProperties.CreatedOn;
                    result.Comment = TrailerProperties.Comment;

                    context.TrailerProperties.Add(result);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */
                    var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = result.TrailerId;
                    langrefdetail.QuestionRefId = result.QuestionRefId;
                    langrefdetail.LayoutRefId = result.LayoutRefId;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TrailerProperties_Title;
                    langrefdetail.LangText = result.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = result.CreatedBy;
                    langrefdetail.UpdatedBy = result.CreatedBy;

                    _translatorRepository.SaveLangDetail(langrefdetail);

                    langrefdetail.LangRefColTableId = (int)LangRefColTable.TrailerProperties_MaskText;
                    langrefdetail.LangText = result.MaskText ?? "";
                    _translatorRepository.SaveLangDetail(langrefdetail);

                    /* Written By Selvam Thangaraj - Stop */

                    return result;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<TrailerProperties> GetTrailerProperties(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var trailerTranslationDetails = context.TrailerPropertiesTranslation.FromSql("GetTrailerProperties @QuestionRefId", questionParam).ToList();
                    var trailerMaskTextTranslationDetails = context.TrailerPropertiesMaskTextTranslation.FromSql("GetTrailerPropertiesMaskText @QuestionRefId", questionParam).ToList();

                    List<TrailerProperties> trailerPropertiesList = new List<TrailerProperties>();

                    foreach (var trailerinfo in trailerTranslationDetails)
                    {
                        var maskText = trailerMaskTextTranslationDetails.FirstOrDefault(x => x.TrailerId == trailerinfo.TrailerId && x.LangRefColTableId == (int)LangRefColTable.TrailerProperties_MaskText);

                        TrailerProperties trailer = new TrailerProperties();
                        trailer.TrailerId = trailerinfo.TrailerId;
                        trailer.ActivitiesRefId = trailerinfo.ActivitiesRefId;
                        trailer.LayoutRefId = trailerinfo.LayoutRefId;
                        trailer.QuestionRefId = trailerinfo.QuestionRefId;
                        trailer.ActionType = trailerinfo.ActionType;
                        trailer.Name = trailerinfo.Name;
                        trailer.Title = GetTrailerTranslationText(trailerinfo, defualtLanguage);
                        trailer.TrailerFormat = trailerinfo.TrailerFormat;
                        trailer.MaskText = maskText == null ? "" : GetTrailerMaskTextTranslationText(maskText, defualtLanguage);
                        trailer.Size = trailerinfo.Size;
                        trailer.Regex = trailerinfo.Regex;
                        trailer.NullNotAllowed = trailerinfo.NullNotAllowed;
                        trailer.ZeroNotAllowed = trailerinfo.ZeroNotAllowed;
                        trailer.MinimumofCharacter = trailerinfo.MinimumofCharacter;
                        trailer.MinimumofCharacterValue = trailerinfo.MinimumofCharacterValue;
                        trailer.ExactOfCharacter = trailerinfo.ExactOfCharacter;
                        trailer.Save = trailerinfo.Save;
                        trailer.SaveTo = trailerinfo.SaveTo;
                        trailer.Propose = trailerinfo.Propose;
                        trailer.AutoGenerate = trailerinfo.AutoGenerate;
                        trailer.PlanningFeedback = trailerinfo.PlanningFeedback;
                        trailer.FeedbackType = trailerinfo.FeedbackType;
                        trailer.PlanningFeedbackCode = trailerinfo.PlanningFeedbackCode;
                        trailer.PartnerCode = trailerinfo.PartnerCode;
                        trailer.Other = trailerinfo.Other;
                        trailer.Comment = trailerinfo.Comment;
                        trailer.CreatedBy = trailerinfo.CreatedBy;
                        trailer.CreatedOn = trailerinfo.CreatedOn;
                        trailer.UpdatedBy = trailerinfo.UpdatedBy;
                        trailer.UpdatedOn = trailerinfo.UpdatedOn;
                        trailer.CopyLayoutRefId = trailerinfo.CopyLayoutRefId;

                        trailerPropertiesList.Add(trailer);
                    }

                    return trailerPropertiesList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<TrailerProperties>();
            }
        }
        public string GetTrailerTranslationText(TrailerPropertiesTranslation trailerTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = trailerTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == trailerTranslation.LanguageName)
                    {
                        translationText = "@" + trailerTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + trailerTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public string GetTrailerMaskTextTranslationText(TrailerPropertiesMaskTextTranslation trailerMaskTextTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = trailerMaskTextTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == trailerMaskTextTranslation.LanguageName)
                    {
                        translationText = "@" + trailerMaskTextTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + trailerMaskTextTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public TrailerProperties UpdateTrailerProperties(TrailerProperties TrailerProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.TrailerProperties.SingleOrDefault(IC => IC.TrailerId == TrailerProperties.TrailerId);
                    if (result != null)
                    {
                        var Title = result.Title;
                        var MaskText = result.MaskText;

                        var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == result.QuestionRefId);
                        if (Question.ISModification == 1)
                        {
                            Question.ISModification = 2;
                            context.SaveChanges();
                        }

                        if (result.ActionType != "WTR" && TrailerProperties.ActionType == "WTR")
                        {
                            Questions objQuestion = new Questions();
                            var questionid = result.QuestionRefId;
                            objQuestion.ActivitiesRefId = result.ActivitiesRefId;
                            objQuestion.CreatedBy = TrailerProperties.UpdatedBy;
                            objQuestion.CreatedOn = TrailerProperties.UpdatedOn ?? DateTime.Now;
                            objQuestion.LayoutRefId = result.LayoutRefId;
                            objQuestion.Name = "Select";
                            objQuestion.ParentId = questionid;
                            objQuestion.TransporttypeRefId = Question.TransporttypeRefId;
                            objQuestion.PropertyName = "Select";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = false;
                            objQuestion.IsDefault = true;
                            objQuestion.IsChild = true;
                            SaveQuestions(objQuestion);

                            objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = result.ActivitiesRefId;
                            objQuestion.CreatedBy = TrailerProperties.UpdatedBy;
                            objQuestion.CreatedOn = TrailerProperties.UpdatedOn ?? DateTime.Now;
                            objQuestion.LayoutRefId = result.LayoutRefId;
                            objQuestion.Name = "Other";
                            objQuestion.ParentId = questionid;
                            objQuestion.TransporttypeRefId = Question.TransporttypeRefId;
                            objQuestion.PropertyName = "Other";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = false;
                            objQuestion.IsDefault = true;
                            objQuestion.IsChild = true;
                            SaveQuestions(objQuestion);
                        }
                        else if (result.ActionType == "WTR" && TrailerProperties.ActionType != "WTR")
                        {
                            SqlParameter param1 = new SqlParameter("@pQuestionId", result.QuestionRefId);
                            var questionTree = context.Questions.FromSql("GetSpecificQuestionTree @pQuestionId", param1).ToList();
                            var excludeParentNode = questionTree.Where(x => x.QuestionId != result.QuestionRefId).ToList();
                            if (excludeParentNode.Any())
                            {
                                DeleteQuestions(excludeParentNode, false, 0);
                            }
                        }

                        result.ActionType = TrailerProperties.ActionType;

                        if (result.ActionType == "Connect")
                        {
                            result.Title = TrailerProperties.Title;
                            result.MaskText = TrailerProperties.MaskText;
                            result.Size = TrailerProperties.Size;
                            result.NullNotAllowed = TrailerProperties.NullNotAllowed;
                            result.MinimumofCharacter = TrailerProperties.MinimumofCharacter;
                            result.MinimumofCharacterValue = TrailerProperties.MinimumofCharacterValue;
                            result.ExactOfCharacter = TrailerProperties.ExactOfCharacter;
                            result.ZeroNotAllowed = TrailerProperties.ZeroNotAllowed;
                            result.Regex = TrailerProperties.Regex;
                            result.Save = TrailerProperties.Save;
                            result.SaveTo = TrailerProperties.SaveTo;
                            result.Propose = TrailerProperties.Propose;
                            result.PlanningFeedback = TrailerProperties.PlanningFeedback;
                            result.FeedbackType = TrailerProperties.FeedbackType;
                            result.PlanningFeedbackCode = TrailerProperties.PlanningFeedbackCode;
                            result.TrailerFormat = TrailerProperties.TrailerFormat;
                            result.PartnerCode = TrailerProperties.PartnerCode;
                            result.AutoGenerate = TrailerProperties.AutoGenerate;

                            result.Other = null;
                        }
                        else
                        {
                            if (result.ActionType == "Disonnect")
                            {
                                result.Title = TrailerProperties.Name;
                                result.Save = null;
                                result.SaveTo = null;
                                result.Other = null;
                            }
                            else if (result.ActionType == "WTR")
                            {
                                result.Title = TrailerProperties.Title;
                                result.Other = TrailerProperties.Other;
                                result.Save = TrailerProperties.Save;
                                result.SaveTo = TrailerProperties.SaveTo;
                            }

                            result.MaskText = null;
                            result.Size = null;
                            result.NullNotAllowed = null;
                            result.MinimumofCharacter = null;
                            result.MinimumofCharacterValue = null;
                            result.ExactOfCharacter = null;
                            result.ZeroNotAllowed = null;
                            result.Regex = null;
                            result.Propose = null;
                            result.PlanningFeedback = null;
                            result.FeedbackType = null;
                            result.PlanningFeedbackCode = null;
                            result.TrailerFormat = null;
                            result.PartnerCode = null;
                            result.AutoGenerate = null;
                        }

                        result.UpdatedBy = TrailerProperties.UpdatedBy;
                        result.UpdatedOn = TrailerProperties.UpdatedOn;
                        result.Comment = TrailerProperties.Comment;

                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(result.LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.TrailerId;
                        langrefdetail.QuestionRefId = result.QuestionRefId;
                        langrefdetail.LayoutRefId = result.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.TrailerProperties_Title;
                        langrefdetail.LangText = result.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = result.UpdatedBy;
                        langrefdetail.UpdatedBy = result.UpdatedBy;

                        _translatorRepository.SaveLangDetail(langrefdetail);

                        langrefdetail.LangRefColTableId = (int)LangRefColTable.TrailerProperties_MaskText;
                        langrefdetail.LangText = TrailerProperties.MaskText ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if ((TrailerProperties.Title != Title || TrailerProperties.MaskText != MaskText) && TrailerProperties.PropertyName == "Trailer From Variable List")
                        {
                            var trailerOtherTitleParentId = context.Questions.Where(x => x.QuestionId == TrailerProperties.QuestionRefId).Select(x => x.ParentId).FirstOrDefault();
                            if (trailerOtherTitleParentId > 0)
                            {
                                var otherParentId = context.Questions.Where(x => x.QuestionId == trailerOtherTitleParentId).Select(x => x.ParentId).FirstOrDefault();
                                if (otherParentId > 0)
                                {
                                    var varListObj = context.VarListProperties.Where(x => x.QuestionRefId == otherParentId).FirstOrDefault();
                                    if (varListObj != null)
                                    {
                                        var OtherTitle = varListObj.OtherTitle;
                                        var OtherMaskText = varListObj.OtherMaskText;

                                        varListObj.OtherMaskText = TrailerProperties.MaskText;
                                        varListObj.OtherTitle = TrailerProperties.Title;
                                        context.SaveChanges();

                                        if (OtherTitle != varListObj.OtherTitle && varListObj.OtherTitle != null && varListObj.OtherTitle.Trim().Length > 0)
                                        {
                                            langrefdetail.RefId = TrailerProperties.QuestionRefId;
                                            langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                                            langrefdetail.LangText = result.Title ?? "";
                                            _translatorRepository.SaveLangDetail(langrefdetail);

                                            langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherTitle;
                                            langrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                                            langrefdetail.LangText = varListObj.OtherTitle ?? "";
                                            _translatorRepository.SaveLangDetail(langrefdetail);
                                        }
                                        else if (OtherTitle != null && OtherTitle.Trim().Length > 0 && varListObj.OtherTitle == null)
                                        {
                                            LangRefDetail dlangrefdetail = new LangRefDetail();
                                            dlangrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                                            dlangrefdetail.LayoutRefId = varListObj.LayoutRefId;
                                            dlangrefdetail.CustomerRefId = null;
                                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherTitle;
                                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                                        }

                                        if (OtherMaskText != varListObj.OtherMaskText && varListObj.OtherMaskText != null && varListObj.OtherMaskText.Trim().Length > 0)
                                        {
                                            langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherMaskText;
                                            langrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                                            langrefdetail.LangText = varListObj.OtherMaskText ?? "";
                                            _translatorRepository.SaveLangDetail(langrefdetail);
                                        }
                                        else if (OtherMaskText != null && OtherMaskText.Trim().Length > 0 && varListObj.OtherMaskText == null)
                                        {
                                            LangRefDetail dlangrefdetail = new LangRefDetail();
                                            dlangrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                                            dlangrefdetail.LayoutRefId = varListObj.LayoutRefId;
                                            dlangrefdetail.CustomerRefId = null;
                                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherMaskText;
                                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                                        }
                                    }
                                }
                            }

                        }

                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public int GetChoiceIdByQuestionId(int quesionId)
        {
            using (var context = new Creatis_Context())
            {
                var choiceId = context.ChioceProperties.Where(x => x.QuestionRefId == quesionId).FirstOrDefault();
                if (choiceId != null)
                    return Convert.ToInt32(choiceId.ChioceId);
                else
                    return 0;
            }
        }
        public int GetChoiceIdByAddChoiceQuestionId(int quesionId)
        {
            using (var context = new Creatis_Context())
            {
                var choiceId = context.AddChioceProperties.Where(x => x.QuestionRefId == quesionId).FirstOrDefault();
                if (choiceId != null)
                    return Convert.ToInt32(choiceId.ChioceRefId);
                else
                    return 0;
            }
        }
        public int GetTransTypeIdByTTQuestionId(int quesionId)
        {
            using (var context = new Creatis_Context())
            {
                var choiceId = context.TransportTypePropertiesDetail.Where(x => x.QuestionRefId == quesionId).FirstOrDefault();
                if (choiceId != null)
                    return Convert.ToInt32(choiceId.TransportTypeRefId);
                else
                    return 0;
            }
        }
        public GetSystemValuesProperties SaveGetSystemValuesProperties(GetSystemValuesProperties getSystemValuesProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.GetSystemValuesProperties.Add(getSystemValuesProperties);
                    context.SaveChanges();

                    return getSystemValuesProperties;
                }
            }
            catch (Exception ex)
            {
                return new GetSystemValuesProperties();
            }
        }
        public GetSystemValuesProperties UpdateGetSystemValuesProperties(GetSystemValuesProperties getSystemValuesProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.GetSystemValuesProperties.SingleOrDefault(IC => IC.GetSystemValuesId == getSystemValuesProperties.GetSystemValuesId);
                    if (result != null)
                    {
                        var Question = context.Questions.SingleOrDefault(IC => IC.QuestionId == result.QuestionRefId);
                        if (Question.ISModification == 1)
                        {
                            Question.ISModification = 2;
                            context.SaveChanges();
                        }

                        result.Name = getSystemValuesProperties.Name;
                        result.GetSystemValuesRefId = getSystemValuesProperties.GetSystemValuesRefId;
                        result.GetSystemValuesFormatRefId = getSystemValuesProperties.GetSystemValuesFormatRefId;
                        result.SaveTo = getSystemValuesProperties.SaveTo;
                        result.Comment = getSystemValuesProperties.Comment;
                        result.UpdatedBy = getSystemValuesProperties.UpdatedBy;
                        result.UpdatedOn = getSystemValuesProperties.UpdatedOn;
                        result.Comment = getSystemValuesProperties.Comment;

                        context.SaveChanges();
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new GetSystemValuesProperties();
            }
        }
        public List<GetSystemValuesProperties> GetSystemValuesProperties(int questionRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.GetSystemValuesProperties.Where(x => x.QuestionRefId == questionRefId).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<GetSystemValuesProperties>();
            }
        }
        public List<GetSystemValues> GetSystemValuesDetails()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.GetSystemValues.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<GetSystemValues>();
            }
        }
        public List<GetSystemValuesFormat> GetSystemValuesFormatDetails(int getSystemValuesId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.GetSystemValuesFormat.Where(x => (getSystemValuesId == 0 ? (1 == 1) : (x.GetSystemValuesRefId == getSystemValuesId))).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<GetSystemValuesFormat>();
            }
        }
        public int DocumentScanValidation(int documentScanTitleRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.DocumentScanType.Where(x => x.DocumentScanTitleRefId == documentScanTitleRefId).Count();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int GetCustomerIdByLayoutId(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.CustomerRefId).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public List<CommentDetail> GetCommentReadoutDetail(string TockenId, int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var aliasNameDetails = context.AliasNameDetails.Where(x => x.LayoutRefId == LayoutId).Select(x => new { x.AliasName, x.AliasNameDetailsId }).ToList();
                    var CommentReadoutDetails = context.CommentDetail.Where(x => x.TokenId == TockenId).ToList();

                    var CommentReadoutDetailList = (from cp in CommentReadoutDetails
                                                    join ad in aliasNameDetails on (cp.SaveTo) equals ad.AliasNameDetailsId
                                                    into joinad
                                                    from dad in joinad.DefaultIfEmpty(new { AliasName = "", AliasNameDetailsId = cp.SaveTo })
                                                    select new CommentDetail()
                                                    {
                                                        CommentId = cp.CommentId,
                                                        ReadoutRange = cp.ReadoutRange,
                                                        StartChar = cp.StartChar,
                                                        StartCharValue = (cp.StartChar == null ? "" : cp.StartChar.ToString()),
                                                        CharLength = cp.CharLength,
                                                        CharLengthValue = (cp.CharLength == null ? "" : cp.CharLength.ToString()),
                                                        TokenId = cp.TokenId,
                                                        CreatedBy = "",
                                                        CreatedOn = DateTime.Now,
                                                        SaveTo = cp.SaveTo,
                                                        SaveToValue = dad.AliasName,
                                                        CommentDetailId = cp.CommentDetailId
                                                    }).ToList();

                    return CommentReadoutDetailList;
                }
            }
            catch (Exception ex)
            {
                return new List<CommentDetail>();
            }
        }
        public List<FilterDetail> GetFilterDetail(string TockenId, int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var aliasNameDetails = context.AliasNameDetails.Where(x => x.LayoutRefId == LayoutId).Select(x => new { x.AliasName, x.AliasNameDetailsId }).ToList();
                    var FilterDetails = context.FilterDetail.Where(x => x.TokenId == TockenId).ToList();

                    var FilterDetailList = (from cp in FilterDetails
                                            join ad in aliasNameDetails on (cp.SavedValue) equals ad.AliasNameDetailsId
                                            into joinad
                                            from dad in joinad.DefaultIfEmpty(new { AliasName = "", AliasNameDetailsId = cp.SavedValue })
                                            select new FilterDetail()
                                            {
                                                CommentId = cp.CommentId,
                                                FilterMethod = cp.FilterMethod,
                                                TokenId = cp.TokenId,
                                                CreatedBy = "",
                                                CreatedOn = DateTime.Now,
                                                SavedValue = cp.SavedValue,
                                                SavedValueName = dad.AliasName,
                                                FilterDetailId = cp.FilterDetailId
                                            }).ToList();

                    return FilterDetailList;
                }
            }
            catch (Exception ex)
            {
                return new List<FilterDetail>();
            }
        }
        public CommentDetail SaveCommentReadoutDetail(CommentDetail commentReadoutDetail)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    if (commentReadoutDetail.CommentDetailId == 0)
                    {
                        context.CommentDetail.Add(commentReadoutDetail);
                        context.SaveChanges();
                        return commentReadoutDetail;
                    }
                    else
                    {
                        var objReadout = context.CommentDetail.Where(x => x.CommentDetailId == commentReadoutDetail.CommentDetailId).FirstOrDefault();
                        if (objReadout != null)
                        {
                            objReadout.CommentId = commentReadoutDetail.CommentId;
                            objReadout.ReadoutRange = commentReadoutDetail.ReadoutRange;
                            objReadout.StartChar = commentReadoutDetail.StartChar;
                            objReadout.CharLength = commentReadoutDetail.CharLength;
                            objReadout.TokenId = commentReadoutDetail.TokenId;
                            objReadout.SaveTo = commentReadoutDetail.SaveTo;
                            objReadout.IsDefaultAlaisName = commentReadoutDetail.IsDefaultAlaisName;
                            context.SaveChanges();
                        }
                        return objReadout;
                    }
                }
            }
            catch (Exception ex)
            {
                return new CommentDetail();
            }
        }
        public string DeleteCommentReadoutDetail(int commentDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var commentDetail = context.CommentDetail.Where(x => x.CommentDetailId == commentDetailId).FirstOrDefault();
                    if (commentDetail != null)
                    {
                        context.CommentDetail.Remove(commentDetail);
                        context.SaveChanges();
                    }

                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<CommentDetail> EditCommentReadoutDetail(int commentDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var commentDetail = context.CommentDetail.Where(x => x.CommentDetailId == commentDetailId).ToList();
                    if (commentDetail.Any())
                        return commentDetail;
                    else
                        return new List<CommentDetail>();
                }
            }
            catch (Exception ex)
            {
                return new List<CommentDetail>();
            }
        }

        public FilterDetail SaveFilterDetail(FilterDetail filterDetail)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    if (filterDetail.FilterDetailId == 0)
                    {
                        context.FilterDetail.Add(filterDetail);
                        context.SaveChanges();
                        return filterDetail;
                    }
                    else
                    {
                        var objReadout = context.FilterDetail.Where(x => x.FilterDetailId == filterDetail.FilterDetailId).FirstOrDefault();
                        if (objReadout != null)
                        {
                            objReadout.CommentId = filterDetail.CommentId;
                            objReadout.FilterMethod = filterDetail.FilterMethod;
                            objReadout.SavedValue = filterDetail.SavedValue;
                            objReadout.TokenId = filterDetail.TokenId;
                            context.SaveChanges();
                        }
                        return objReadout;
                    }
                }
            }
            catch (Exception ex)
            {
                return new FilterDetail();
            }
        }
        public string DeleteFilterDetail(int filterDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var filterDetail = context.FilterDetail.Where(x => x.FilterDetailId == filterDetailId).FirstOrDefault();
                    if (filterDetail != null)
                    {
                        context.FilterDetail.Remove(filterDetail);
                        context.SaveChanges();
                    }

                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<FilterDetail> EditFilterDetail(int filterDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var filterDetail = context.FilterDetail.Where(x => x.FilterDetailId == filterDetailId).ToList();
                    if (filterDetail.Any())
                        return filterDetail;
                    else
                        return new List<FilterDetail>();
                }
            }
            catch (Exception ex)
            {
                return new List<FilterDetail>();
            }
        }

        public VarListProperties SaveVarListProperties(VarListProperties varListProperties)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(varListProperties);
                    context.SaveChanges();

                    /* Written By Selvam Thangaraj - Go */
                    var LanguageName = _translatorRepository.GetDefaultLanguage(varListProperties.LayoutRefId);

                    LangRefDetail langrefdetail = new LangRefDetail();
                    langrefdetail.RefId = varListProperties.VarListPropertiesId ?? 0;
                    langrefdetail.QuestionRefId = varListProperties.QuestionRefId;
                    langrefdetail.LayoutRefId = varListProperties.LayoutRefId;
                    langrefdetail.CustomerRefId = null;
                    langrefdetail.LangRefColTableId = (int)LangRefColTable.VariableListProperties_Title;
                    langrefdetail.LangText = varListProperties.Title ?? "";
                    langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                    langrefdetail.CreatedBy = varListProperties.CreatedBy;
                    langrefdetail.UpdatedBy = varListProperties.CreatedBy;

                    _translatorRepository.SaveLangDetail(langrefdetail);

                    if (varListProperties.OtherTitle != null && varListProperties.OtherTitle.Trim().Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherTitle;
                        langrefdetail.LangText = varListProperties.OtherTitle ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    if (varListProperties.OtherMaskText != null && varListProperties.OtherMaskText.Trim().Length > 0)
                    {
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherMaskText;
                        langrefdetail.LangText = varListProperties.OtherMaskText ?? "";
                        _translatorRepository.SaveLangDetail(langrefdetail);
                    }

                    /* Written By Selvam Thangaraj - Stop */

                    return varListProperties;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string SaveVarListCommentReadoutDetail(int VarListPropertiesId, string TokenId, string UserId, DateTime CreatedOn)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var readoutList = context.CommentDetail.Where(x => x.TokenId == TokenId).ToList();
                    foreach (var readoutDetail in readoutList)
                    {
                        VarListCommentReadoutDetail objReadout = new VarListCommentReadoutDetail();
                        objReadout.VarListPropertiesRefId = VarListPropertiesId;
                        objReadout.CommentId = readoutDetail.CommentId;
                        objReadout.ReadoutRange = readoutDetail.ReadoutRange;
                        objReadout.StartChar = readoutDetail.StartChar ?? 0;
                        objReadout.CharLength = readoutDetail.CharLength ?? 0;
                        objReadout.SaveTo = readoutDetail.SaveTo;
                        objReadout.IsDefaultAlaisName = readoutDetail.IsDefaultAlaisName;
                        objReadout.CreatedBy = UserId;
                        objReadout.CreatedOn = CreatedOn;

                        context.VarListCommentReadoutDetail.Add(objReadout);
                        context.SaveChanges();

                        if (objReadout.SaveTo > 0)
                            UpdateAliasPropertyTableIdForReadout(objReadout.SaveTo ?? 0, objReadout.VarListCommentReadoutDetailId);

                    }

                    context.CommentDetail.RemoveRange(readoutList);
                    context.SaveChanges();

                    return "";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string SaveVarListFilterDetail(int VarListPropertiesId, string TokenId, string UserId, DateTime CreatedOn)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var readoutList = context.FilterDetail.Where(x => x.TokenId == TokenId).ToList();
                    foreach (var readoutDetail in readoutList)
                    {
                        VarListFilterDetail objReadout = new VarListFilterDetail();
                        objReadout.VarListPropertiesRefId = VarListPropertiesId;
                        objReadout.CommentId = readoutDetail.CommentId;
                        objReadout.FilterMethod = readoutDetail.FilterMethod;
                        objReadout.SavedValue = readoutDetail.SavedValue;
                        objReadout.CreatedBy = UserId;
                        objReadout.CreatedOn = CreatedOn;

                        context.VarListFilterDetail.Add(objReadout);
                        context.SaveChanges();
                    }

                    context.FilterDetail.RemoveRange(readoutList);
                    context.SaveChanges();

                    return "";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<VarListProperties> GetVarListProperties(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var languageName = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var varListTranslationDetails = context.VarListPropertiesTranslation.FromSql("GetVarListProperties @QuestionRefId", questionParam).ToList();
                    var varListOtherTranslationDetails = context.VarListPropertiesOtherTranslation.FromSql("GetVarListPropertiesOtherTranslation @QuestionRefId", questionParam).ToList();

                    List<VarListProperties> varListPropertiesList = new List<VarListProperties>();

                    foreach (var varListDetail in varListTranslationDetails)
                    {
                        var otherTitle = varListOtherTranslationDetails.FirstOrDefault(x => x.VarListPropertiesId == varListDetail.VarListPropertiesId && x.LangRefColTableId == (int)LangRefColTable.VarListProperties_OtherTitle);
                        var otherMaskText = varListOtherTranslationDetails.FirstOrDefault(x => x.VarListPropertiesId == varListDetail.VarListPropertiesId && x.LangRefColTableId == (int)LangRefColTable.VarListProperties_OtherMaskText);

                        VarListProperties varList = new VarListProperties();
                        varList.VarListPropertiesId = varListDetail.VarListPropertiesId;
                        varList.ActivitiesRefId = varListDetail.ActivitiesRefId;
                        varList.QuestionRefId = varListDetail.QuestionRefId;
                        varList.LayoutRefId = varListDetail.LayoutRefId;
                        varList.Name = varListDetail.Name;
                        varList.Title = GetVarListTranslationText(varListDetail, languageName);
                        varList.VarListId = varListDetail.VarListId;
                        varList.Other = varListDetail.Other;
                        varList.OtherTitle = otherTitle == null ? "" : GetVarListOtherTranslationText(otherTitle, languageName);
                        varList.OtherMaskText = otherMaskText == null ? "" : GetVarListOtherTranslationText(otherMaskText, languageName);
                        varList.SaveDisplay = varListDetail.SaveDisplay;
                        varList.SaveDisplayTo = varListDetail.SaveDisplayTo;
                        varList.Comment = varListDetail.Comment;
                        varList.ConditionItemValue = varListDetail.ConditionItemValue;
                        varList.ConditionSavedValue = varListDetail.ConditionSavedValue;
                        varList.CompareMethod = varListDetail.CompareMethod;
                        varList.CompareTo = varListDetail.CompareTo;
                        varList.CompareToSavedValue = varListDetail.CompareToSavedValue;
                        varList.CompareToFixedText = varListDetail.CompareToFixedText;
                        varList.PlanningFeedback = varListDetail.PlanningFeedback;
                        varList.FeedbackType = varListDetail.FeedbackType;
                        varList.PlanningFeedbackCode = varListDetail.PlanningFeedbackCode;
                        varList.Infocolumn = varListDetail.Infocolumn;
                        varList.InfoColumnAction = varListDetail.InfoColumnAction;
                        varList.FileNumber = varListDetail.FileNumber;
                        varList.PartnerCode = varListDetail.PartnerCode;
                        varList.RegisterAsTrailer = varListDetail.RegisterAsTrailer;
                        varList.CreatedBy = varListDetail.CreatedBy;
                        varList.CreatedOn = varListDetail.CreatedOn;
                        varList.UpdatedBy = varListDetail.UpdatedBy;
                        varList.UpdatedOn = varListDetail.UpdatedOn;

                        varListPropertiesList.Add(varList);
                    }

                    return varListPropertiesList.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<VarListProperties>();
            }
        }
        public string GetVarListTranslationText(VarListPropertiesTranslation varListTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = varListTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == varListTranslation.LanguageName)
                    {
                        translationText = "@" + varListTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + varListTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public string GetVarListOtherTranslationText(VarListPropertiesOtherTranslation varListOtherTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = varListOtherTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == varListOtherTranslation.LanguageName)
                    {
                        translationText = "@" + varListOtherTranslation.Belarusian;
                    }
                    else
                    {
                        translationText = "@" + varListOtherTranslation.English;
                    }
                }
            }
            return translationText;
        }
        public List<VarListCommentReadoutDetail> GetVarListCommentReadoutDetail(int varListPropertiesId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var aliasNameDetails = context.AliasNameDetails.Where(x => x.LayoutRefId == layoutId).Select(x => new { x.AliasName, x.AliasNameDetailsId }).ToList();
                    var CommentReadoutDetails = context.VarListCommentReadoutDetail.Where(x => x.VarListPropertiesRefId == varListPropertiesId).ToList();

                    var CommentReadoutDetailList = (from cp in CommentReadoutDetails
                                                    join ad in aliasNameDetails on (cp.SaveTo ?? 0) equals ad.AliasNameDetailsId
                                                    into joinad
                                                    from dad in joinad.DefaultIfEmpty(new { AliasName = "", AliasNameDetailsId = (cp.SaveTo ?? 0) })
                                                    select new VarListCommentReadoutDetail()
                                                    {
                                                        CommentId = cp.CommentId,
                                                        ReadoutRange = cp.ReadoutRange,
                                                        StartChar = cp.StartChar,
                                                        StartCharValue = (cp.StartChar == 0 ? "" : cp.StartChar.ToString()),
                                                        CharLength = cp.CharLength,
                                                        CharLengthValue = (cp.CharLength == 0 ? "" : cp.CharLength.ToString()),
                                                        VarListCommentReadoutDetailId = cp.VarListCommentReadoutDetailId,
                                                        CreatedBy = "",
                                                        CreatedOn = DateTime.Now,
                                                        SaveTo = cp.SaveTo,
                                                        SaveToValue = dad.AliasName,
                                                        VarListPropertiesRefId = cp.VarListPropertiesRefId
                                                    }).ToList();

                    return CommentReadoutDetailList;
                }
            }
            catch (Exception ex)
            {
                return new List<VarListCommentReadoutDetail>();
            }
        }
        public List<VarListFilterDetail> GetVarListFilterDetail(int varListPropertiesId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var aliasNameDetails = context.AliasNameDetails.Where(x => x.LayoutRefId == layoutId).Select(x => new { x.AliasName, x.AliasNameDetailsId }).ToList();
                    var FilterDetails = context.VarListFilterDetail.Where(x => x.VarListPropertiesRefId == varListPropertiesId).ToList();

                    var FilterDetailList = (from cp in FilterDetails
                                            join ad in aliasNameDetails on (cp.SavedValue) equals ad.AliasNameDetailsId
                                            into joinad
                                            from dad in joinad.DefaultIfEmpty(new { AliasName = "", AliasNameDetailsId = cp.SavedValue })
                                            select new VarListFilterDetail()
                                            {
                                                CommentId = cp.CommentId,
                                                FilterMethod = cp.FilterMethod,
                                                VarListPropertiesRefId = cp.VarListPropertiesRefId,
                                                CreatedBy = "",
                                                CreatedOn = DateTime.Now,
                                                SavedValue = cp.SavedValue,
                                                SavedValueName = dad.AliasName,
                                                VarListFilterDetailId = cp.VarListFilterDetailId
                                            }).ToList();

                    return FilterDetailList;
                }
            }
            catch (Exception ex)
            {
                return new List<VarListFilterDetail>();
            }
        }
        public VarListCommentReadoutDetail UpdateVarListCommentReadoutDetail(VarListCommentReadoutDetail commentReadoutDetail)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    if (commentReadoutDetail.VarListCommentReadoutDetailId == 0)
                    {
                        context.VarListCommentReadoutDetail.Add(commentReadoutDetail);
                        context.SaveChanges();
                        return commentReadoutDetail;
                    }
                    else
                    {
                        var objReadout = context.VarListCommentReadoutDetail.Where(x => x.VarListCommentReadoutDetailId == commentReadoutDetail.VarListCommentReadoutDetailId).FirstOrDefault();
                        if (objReadout != null)
                        {
                            objReadout.VarListPropertiesRefId = commentReadoutDetail.VarListPropertiesRefId;
                            objReadout.CommentId = commentReadoutDetail.CommentId;
                            objReadout.ReadoutRange = commentReadoutDetail.ReadoutRange;
                            objReadout.StartChar = commentReadoutDetail.StartChar;
                            objReadout.CharLength = commentReadoutDetail.CharLength;
                            objReadout.SaveTo = commentReadoutDetail.SaveTo;
                            objReadout.IsDefaultAlaisName = commentReadoutDetail.IsDefaultAlaisName;
                            objReadout.UpdatedBy = commentReadoutDetail.CreatedBy;
                            objReadout.UpdatedOn = commentReadoutDetail.CreatedOn;
                            context.SaveChanges();
                        }
                        return objReadout;
                    }
                }
            }
            catch (Exception ex)
            {
                return new VarListCommentReadoutDetail();
            }
        }
        public string DeleteVarListCommentReadoutDetail(int commentDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var commentDetail = context.VarListCommentReadoutDetail.Where(x => x.VarListCommentReadoutDetailId == commentDetailId).FirstOrDefault();
                    if (commentDetail != null)
                    {
                        context.VarListCommentReadoutDetail.Remove(commentDetail);
                        context.SaveChanges();
                    }

                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<VarListCommentReadoutDetail> EditVarListCommentReadoutDetail(int commentDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var commentDetail = context.VarListCommentReadoutDetail.Where(x => x.VarListCommentReadoutDetailId == commentDetailId).ToList();
                    if (commentDetail.Any())
                        return commentDetail;
                    else
                        return new List<VarListCommentReadoutDetail>();
                }
            }
            catch (Exception ex)
            {
                return new List<VarListCommentReadoutDetail>();
            }
        }
        public VarListFilterDetail UpdateVarListFilterDetail(VarListFilterDetail filterDetail)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    if (filterDetail.VarListFilterDetailId == 0)
                    {
                        context.VarListFilterDetail.Add(filterDetail);
                        context.SaveChanges();
                        return filterDetail;
                    }
                    else
                    {
                        var objReadout = context.VarListFilterDetail.Where(x => x.VarListFilterDetailId == filterDetail.VarListFilterDetailId).FirstOrDefault();
                        if (objReadout != null)
                        {
                            objReadout.CommentId = filterDetail.CommentId;
                            objReadout.FilterMethod = filterDetail.FilterMethod;
                            objReadout.SavedValue = filterDetail.SavedValue;
                            objReadout.UpdatedBy = filterDetail.CreatedBy;
                            objReadout.UpdatedOn = filterDetail.CreatedOn;
                            context.SaveChanges();
                        }
                        return objReadout;
                    }
                }
            }
            catch (Exception ex)
            {
                return new VarListFilterDetail();
            }
        }
        public string DeleteVarListFilterDetail(int filterDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var filterDetail = context.VarListFilterDetail.Where(x => x.VarListFilterDetailId == filterDetailId).FirstOrDefault();
                    if (filterDetail != null)
                    {
                        context.VarListFilterDetail.Remove(filterDetail);
                        context.SaveChanges();
                    }

                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<VarListFilterDetail> EditVarListFilterDetail(int filterDetailId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var filterDetail = context.VarListFilterDetail.Where(x => x.VarListFilterDetailId == filterDetailId).ToList();
                    if (filterDetail.Any())
                        return filterDetail;
                    else
                        return new List<VarListFilterDetail>();
                }
            }
            catch (Exception ex)
            {
                return new List<VarListFilterDetail>();
            }
        }
        public int GetVarListOtherQuestionId(int questionRefId, string PropertyName)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var questionId = context.Questions.Where(x => x.ParentId == questionRefId && x.PropertyName == PropertyName).Select(x => x.QuestionId).FirstOrDefault();
                    return questionId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }


        }
        public VarListProperties UpdateVarListProperties(VarListProperties varListProperties, int transportTypeId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var varListObj = context.VarListProperties.SingleOrDefault(x => x.VarListPropertiesId == varListProperties.VarListPropertiesId);
                    if (varListObj != null)
                    {
                        var RegisterAsTrailer = varListObj.RegisterAsTrailer;
                        var Other = varListObj.Other;
                        var OtherTitle = varListObj.OtherTitle;
                        var OtherMaskText = varListObj.OtherMaskText;

                        int AliasNameDetailsId = 0;
                        int entryQuestionId = 0;
                        int trailerQuestionId = 0;
                        if (varListProperties.SaveDisplay)
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.AliasNameDetailsId = 0;
                            AND.LayoutRefId = varListProperties.LayoutRefId;
                            AND.ActivitiesRefId = varListProperties.ActivitiesRefId;
                            AND.AliasName = varListProperties.SaveDisplayAlaiasVal;
                            AND.PropertyName = "Variable List";
                            AND.CreatedBy = varListProperties.CreatedBy;
                            AND.CreatedOn = varListProperties.CreatedOn;
                            AND.IsTempData = false;
                            var aliasNameId = SaveAliasNameDetails(AND);
                            AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                        }

                        varListObj.Name = varListProperties.Title;
                        varListObj.Title = varListProperties.Title;
                        varListObj.VarListId = varListProperties.VarListId;
                        varListObj.Other = varListProperties.Other;
                        varListObj.OtherTitle = varListProperties.OtherTitle;
                        varListObj.OtherMaskText = varListProperties.OtherMaskText;
                        varListObj.SaveDisplay = varListProperties.SaveDisplay;

                        int SaveDisplayTo;
                        if (varListProperties.SavedDisplayValues == "0" || (!int.TryParse(varListProperties.SavedDisplayValues, out SaveDisplayTo)))
                            varListObj.SaveDisplayTo = AliasNameDetailsId;
                        else
                            varListObj.SaveDisplayTo = Convert.ToInt32(varListProperties.SavedDisplayValues);

                        varListObj.Comment = varListProperties.Comment;
                        varListObj.ConditionItemValue = varListProperties.ConditionItemValue;
                        varListObj.ConditionSavedValue = varListProperties.ConditionSavedValue;
                        varListObj.CompareMethod = varListProperties.CompareMethod;
                        varListObj.CompareTo = varListProperties.CompareTo;
                        varListObj.CompareToSavedValue = varListProperties.CompareToSavedValue;
                        varListObj.CompareToFixedText = varListProperties.CompareToFixedText;
                        varListObj.PlanningFeedback = varListProperties.PlanningFeedback;
                        varListObj.FeedbackType = varListProperties.FeedbackType;
                        varListObj.PlanningFeedbackCode = varListProperties.PlanningFeedbackCode;
                        varListObj.Infocolumn = varListProperties.Infocolumn;
                        varListObj.InfoColumnAction = varListProperties.InfoColumnAction;
                        varListObj.FileNumber = varListProperties.FileNumber;
                        varListObj.PartnerCode = varListProperties.PartnerCode;
                        varListObj.RegisterAsTrailer = varListProperties.RegisterAsTrailer;
                        varListObj.UpdatedBy = varListProperties.CreatedBy;
                        varListObj.UpdatedOn = varListProperties.CreatedOn;
                        context.SaveChanges();

                        if ((varListProperties.Other != "Driver entry" && Other == "Driver entry") || (varListProperties.RegisterAsTrailer == "Yes, Driver Selection" && Other == "Driver entry"))
                        {
                            var otherquestionid = GetVarListOtherQuestionId(varListProperties.QuestionRefId, "Other");
                            var removequestionid = GetVarListOtherQuestionId(otherquestionid, "Entry From Variable List");
                            DeleteSpecificQuestionTree(removequestionid);
                        }
                        if (varListProperties.RegisterAsTrailer != "Yes, Driver Selection" && RegisterAsTrailer == "Yes, Driver Selection")
                        {
                            var otherquestionid = GetVarListOtherQuestionId(varListProperties.QuestionRefId, "Other");
                            var removequestionid = GetVarListOtherQuestionId(otherquestionid, "Trailer From Variable List");
                            DeleteSpecificQuestionTree(removequestionid);
                        }

                        if (varListProperties.RegisterAsTrailer == "Yes, Driver Selection" && RegisterAsTrailer != "Yes, Driver Selection")
                        {
                            var otherquestionid = GetVarListOtherQuestionId(varListProperties.QuestionRefId, "Other");

                            Questions objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = varListProperties.ActivitiesRefId;
                            objQuestion.CreatedBy = varListProperties.CreatedBy;
                            objQuestion.CreatedOn = varListProperties.CreatedOn;
                            objQuestion.LayoutRefId = varListProperties.LayoutRefId;
                            objQuestion.Name = varListProperties.OtherTitle;
                            objQuestion.ParentId = otherquestionid;
                            objQuestion.TransporttypeRefId = transportTypeId;
                            objQuestion.PropertyName = "Trailer From Variable List";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = true;
                            objQuestion.IsDefault = true;
                            objQuestion.IsChild = false;
                            var entryplanningSelect = SaveQuestions(objQuestion);

                            TrailerProperties objTrailer = new TrailerProperties();
                            objTrailer.ActionType = "Connect";

                            objTrailer.QuestionRefId = entryplanningSelect.QuestionId;
                            objTrailer.LayoutRefId = varListProperties.LayoutRefId;
                            objTrailer.ActivitiesRefId = varListProperties.ActivitiesRefId;
                            objTrailer.TrailerFormat = "Alphanumeric";

                            objTrailer.Title = varListProperties.OtherTitle.Replace("ƒ", "+");
                            objTrailer.MaskText = varListProperties.OtherMaskText;
                            objTrailer.Size = 100;
                            objTrailer.NullNotAllowed = false;
                            objTrailer.MinimumofCharacter = false;
                            objTrailer.ExactOfCharacter = false;
                            objTrailer.ZeroNotAllowed = false;
                            objTrailer.Save = true;

                            objTrailer.PlanningFeedback = "Not Set";
                            objTrailer.FeedbackType = "";
                            objTrailer.PlanningFeedbackCode = "";
                            objTrailer.AutoGenerate = false;
                            objTrailer.PartnerCode = "";

                            int intSaveTo;
                            if (varListProperties.SavedDisplayValues == "0" || (!int.TryParse(varListProperties.SavedDisplayValues, out intSaveTo)))
                                objTrailer.SaveTo = AliasNameDetailsId;
                            else
                                objTrailer.SaveTo = Convert.ToInt32(varListProperties.SaveDisplayTo);

                            objTrailer.Name = "Trailer Connect";
                            objTrailer.Comment = "";
                            objTrailer.CreatedBy = varListProperties.CreatedBy;
                            objTrailer.CreatedOn = varListProperties.CreatedOn;

                            SaveTrailerProperties(objTrailer);
                        }
                        else if ((varListProperties.Other == "Driver entry" && Other != "Driver entry" && varListProperties.RegisterAsTrailer != "Yes, Driver Selection") || (varListProperties.RegisterAsTrailer != "Yes, Driver Selection" && RegisterAsTrailer == "Yes, Driver Selection" && Other == "Driver entry"))
                        {
                            var otherquestionid = GetVarListOtherQuestionId(varListProperties.QuestionRefId, "Other");

                            var listActivitiesRegistrations = GetActivitiesRegistration(varListProperties.ActivitiesRefId);

                            Questions objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = varListProperties.ActivitiesRefId;
                            objQuestion.CreatedBy = varListProperties.CreatedBy;
                            objQuestion.CreatedOn = varListProperties.CreatedOn;
                            objQuestion.LayoutRefId = varListProperties.LayoutRefId;
                            objQuestion.Name = varListProperties.OtherTitle;
                            objQuestion.ParentId = otherquestionid;
                            objQuestion.TransporttypeRefId = transportTypeId;
                            objQuestion.PropertyName = "Entry From Variable List";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = true;
                            objQuestion.IsDefault = true;
                            objQuestion.IsChild = false;
                            var entryplanningSelect = SaveQuestions(objQuestion);

                            int ANameDetailsId = 0;
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.AliasNameDetailsId = 0;
                            AND.LayoutRefId = varListProperties.LayoutRefId;
                            AND.ActivitiesRefId = varListProperties.ActivitiesRefId;
                            AND.PropertyName = "Entry From Variable List";
                            AND.CreatedBy = varListProperties.CreatedBy;
                            AND.CreatedOn = varListProperties.CreatedOn;
                            AND.IsTempData = false;
                            AND.AliasName = listActivitiesRegistrations + "." + varListProperties.Title + ".DriverCreated";
                            var aliasNameId = SaveAliasNameDetails(AND);
                            ANameDetailsId = aliasNameId.AliasNameDetailsId;

                            EntryProperties objEntries = new EntryProperties();
                            objEntries.QuestionRefId = entryplanningSelect.QuestionId;
                            objEntries.LayoutRefId = varListProperties.LayoutRefId;
                            objEntries.ActivitiesRefId = varListProperties.ActivitiesRefId;
                            objEntries.Title = varListProperties.OtherTitle;
                            objEntries.MaskText = varListProperties.OtherMaskText;
                            objEntries.Size = "100";
                            objEntries.NotNull = 1;
                            objEntries.Save = 1;
                            objEntries.Propose = 0;
                            objEntries.EntryType = "Alphanumeric";
                            objEntries.CreatedBy = varListProperties.CreatedBy;
                            objEntries.CreatedOn = varListProperties.CreatedOn;
                            objEntries.FileNumber = "Not Set";
                            objEntries.AutoGenerate = 0;
                            objEntries.Conditionitem = 1;
                            objEntries.SaveTo = ANameDetailsId;

                            var entries = SaveEntryProperties(objEntries);
                            UpdateAliasPropertyTableId(ANameDetailsId, entries.EntryId.GetValueOrDefault());

                            
                        }
                        else if (RegisterAsTrailer == "Yes, Driver Selection" && varListProperties.RegisterAsTrailer == "Yes, Driver Selection" && (OtherTitle != varListProperties.OtherTitle || OtherMaskText != varListProperties.OtherMaskText))
                        {
                            var otherQuestionId = GetVarListOtherQuestionId(varListProperties.QuestionRefId, "Other");
                            trailerQuestionId = GetVarListOtherQuestionId(otherQuestionId, "Trailer From Variable List");
                            TrailerProperties objTrailer = context.TrailerProperties.FirstOrDefault(x => x.QuestionRefId == trailerQuestionId);
                            objTrailer.Title = varListObj.OtherTitle;
                            objTrailer.MaskText = varListObj.OtherMaskText;
                            objTrailer.UpdatedBy = varListObj.UpdatedBy;
                            objTrailer.UpdatedOn = varListObj.UpdatedOn;
                            UpdateTrailerProperties(objTrailer);
                        }
                        else if (varListProperties.Other == "Driver entry" && Other == "Driver entry" && varListProperties.RegisterAsTrailer != "Yes, Driver Selection" && (OtherTitle != varListProperties.OtherTitle || OtherMaskText != varListProperties.OtherMaskText))
                        {
                            var otherQuestionId = GetVarListOtherQuestionId(varListProperties.QuestionRefId, "Other");
                            entryQuestionId = GetVarListOtherQuestionId(otherQuestionId, "Entry From Variable List");
                            EntryProperties objEntry = context.EntryProperties.FirstOrDefault(x => x.QuestionRefId == entryQuestionId);
                            objEntry.Title = varListObj.OtherTitle;
                            objEntry.MaskText = varListObj.OtherMaskText;
                            objEntry.UpdatedBy = varListObj.UpdatedBy;
                            objEntry.UpdatedOn = varListObj.UpdatedOn;
                            UpdateEntryProperties(objEntry);
                        }

                        /* Written By Selvam Thangaraj - Go */
                        var LanguageName = _translatorRepository.GetDefaultLanguage(varListObj.LayoutRefId);

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                        langrefdetail.QuestionRefId = varListObj.QuestionRefId;
                        langrefdetail.LayoutRefId = varListObj.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.VariableListProperties_Title;
                        langrefdetail.LangText = varListObj.Title ?? "";
                        langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                        langrefdetail.CreatedBy = varListObj.UpdatedBy;
                        langrefdetail.UpdatedBy = varListObj.UpdatedBy;
                        _translatorRepository.SaveLangDetail(langrefdetail);

                        if (OtherTitle != varListObj.OtherTitle && varListObj.OtherTitle != null && varListObj.OtherTitle.Trim().Length > 0)
                        {
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherTitle;
                            langrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                            langrefdetail.LangText = varListObj.OtherTitle ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);

                            if (entryQuestionId > 0 || trailerQuestionId > 0)
                            {
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.Questions_Name;
                                langrefdetail.RefId = entryQuestionId > 0 ? entryQuestionId : trailerQuestionId;
                                langrefdetail.LangText = varListObj.OtherTitle ?? "";
                                _translatorRepository.SaveLangDetail(langrefdetail);
                            }
                        }
                        else if (OtherTitle != null && OtherTitle.Trim().Length > 0 && varListObj.OtherTitle == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                            dlangrefdetail.LayoutRefId = varListObj.LayoutRefId;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherTitle;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }

                        if (OtherMaskText != varListObj.OtherMaskText && varListObj.OtherMaskText != null && varListObj.OtherMaskText.Trim().Length > 0)
                        {
                            langrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherMaskText;
                            langrefdetail.LangText = varListObj.OtherMaskText ?? "";
                            _translatorRepository.SaveLangDetail(langrefdetail);
                        }
                        else if (OtherMaskText != null && OtherMaskText.Trim().Length > 0 && varListObj.OtherMaskText == null)
                        {
                            LangRefDetail dlangrefdetail = new LangRefDetail();
                            dlangrefdetail.RefId = varListObj.VarListPropertiesId ?? 0;
                            dlangrefdetail.LayoutRefId = varListObj.LayoutRefId;
                            dlangrefdetail.CustomerRefId = null;
                            dlangrefdetail.LangRefColTableId = (int)LangRefColTable.VarListProperties_OtherMaskText;
                            _translatorRepository.DeleteLangDetail(dlangrefdetail);
                        }

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return varListObj;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetQuestionPathComment(int layoutId, int activityId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var comment = context.ActivitiesRegistration.Where(x => x.LayoutRefId == layoutId && x.ActivitiesRegistrationId == activityId).Select(x => x.Comment ?? "").FirstOrDefault();
                    return comment;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string SaveQuestionPathComment(int activityId, string comment)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var objActReg = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == activityId).FirstOrDefault();
                    if (objActReg != null)
                    {
                        objActReg.Comment = comment;
                        context.SaveChanges();
                    }

                    return comment;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public List<TransportTypes> GetTransportTypes(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var transportTypeList = context.TransportTypes.Where(x => x.LayoutRefId == layoutId && x.IsActive).Select(x => new TransportTypes()
                    {
                        TransportTypeId = x.TransportTypeId,
                        CustomerRefId = x.CustomerRefId,
                        LayoutRefId = x.LayoutRefId,
                        TransportTypeName = x.TransportTypeName,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        UpdatedBy = x.UpdatedBy,
                        UpdatedOn = x.UpdatedOn,
                        CopyColorCode = isCopyLayout ? GetCopyLayoutColor(x.IsModification) : "Grey",
                        IsModification = x.IsModification
                    }).ToList();

                    return transportTypeList;
                }

            }
            catch (Exception ex)
            {
                return new List<TransportTypes>();
            }
        }
        public string GetCopyLayoutColor(int IsModification)
        {
            var colorname = "";
            if (IsModification == 0)
                colorname = "Green";
            else if (IsModification == 2)
                colorname = "Yellow";
            else if (IsModification == 3)
                colorname = "Red";
            else
                colorname = "Grey";

            return colorname;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.Infrastructure.IRepository;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CREATIS.Infrastructure.Repository
{
    public class ScreenLayoutRepository : Utility, IScreenLayoutRepository
    {
        public readonly ITranslatorRepository _translatorRepository;
        public readonly IQuestionPathRepository _questionPathRepository;
        public readonly IDashboardRepository _dashboardRepository;
        public QuestionPathTemplate questionpathtemplateConfiguration { get; }
        public ScreenLayoutRepository(ITranslatorRepository translatorRepository, IQuestionPathRepository questionPathRepository, IDashboardRepository dashboardRepository, Microsoft.Extensions.Options.IOptions<QuestionPathTemplate> questionPathTemplate)
        {
            this._translatorRepository = translatorRepository;
            this._questionPathRepository = questionPathRepository;
            this._dashboardRepository = dashboardRepository;
            questionpathtemplateConfiguration = questionPathTemplate.Value;
        }


        public List<ActivitiesRegistrationDetails> GetRegistration(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var languageName = context.UsedLanguages.Where(x => x.LayoutRefId == Layout_ID && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();
                    var activityId = 0;
                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", Layout_ID);
                    SqlParameter activityParam = new SqlParameter("@ActivityRefId", activityId);

                    var activitiesRegistrationTranslationDetails = context.ActivitiesRegistrationTranslation.FromSql("GetActivitiesRegistration @LayoutRefId,@ActivityRefId", layoutParam, activityParam).ToList();
                    List<ActivitiesRegistrationDetails> activitiesRegisrationList = new List<ActivitiesRegistrationDetails>();
                    var activitiesRegisrationDetails = activitiesRegistrationTranslationDetails.Where(AR => AR.LayoutRefId == Layout_ID && AR.ActivitiesRegistrationName != "Start New"
                                && AR.ActivitiesRegistrationName != "Start Same" && AR.ActivitiesRegistrationName != "End"
                                && AR.WorkingCodeRefId == "Registration" && AR.Visible && AR.ActRegPosition == 0).ToList();

                    foreach (var actRegInfo in activitiesRegisrationDetails)
                    {
                        ActivitiesRegistrationDetails actReg = new ActivitiesRegistrationDetails();

                        actReg.ActivitiesRegistrationId = actRegInfo.ActivitiesRegistrationId;
                        actReg.CustomerRefId = actRegInfo.CustomerRefId;
                        actReg.LayoutRefId = actRegInfo.LayoutRefId;
                        actReg.ActivitiesRegistrationName = _dashboardRepository.GetActivitiesRegistrationName(actRegInfo, languageName);
                        actReg.WorkingCodeRefId = actRegInfo.WorkingCodeRefId;
                        actReg.PlanningRefId = actRegInfo.PlanningRefId;
                        actReg.PlanningOverviewRefId = actRegInfo.PlanningOverviewRefId;
                        actReg.SpecificRefId = actRegInfo.SpecificRefId;
                        actReg.Visible = Convert.ToBoolean(actRegInfo.Visible);
                        actReg.ConfirmButton = Convert.ToBoolean(actRegInfo.ConfirmButton);
                        actReg.PTORefId = actRegInfo.PTORefId;
                        actReg.InterruptibleByRefId = actRegInfo.InterruptibleByRefId;
                        actReg.TransportType = Convert.ToBoolean(actRegInfo.TransportType);
                        actReg.EmptyFullSolo = Convert.ToBoolean(actRegInfo.EmptyFullSolo);
                        actReg.Flexactivity = Convert.ToBoolean(actRegInfo.Flexactivity);
                        actReg.ISModification = actRegInfo.ISModification;
                        actReg.CreatedBy = actRegInfo.CreatedBy;
                        actReg.CreatedOn = actRegInfo.CreatedOn;
                        actReg.UpdatedBy = actRegInfo.UpdatedBy;
                        actReg.UpdatedOn = actRegInfo.UpdatedOn;
                        actReg.DefaultActivitiesRefId = actRegInfo.DefaultActivitiesRefId ?? 0;
                        actReg.CopyLayoutColor = (!actRegInfo.IsCopyLayout ? "Grey" : GetCopyLayoutColor(actRegInfo.ScreenLayoutIsModification));
                        activitiesRegisrationList.Add(actReg);
                    }

                    return activitiesRegisrationList.OrderBy(x => x.ActivitiesRegistrationName).ToList();

                }
            }
            catch (Exception ex)
            {
                return new List<ActivitiesRegistrationDetails>();
            }
        }
        public List<ActivitiesRegistrationDetails> GetActivities(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var languageName = context.UsedLanguages.Where(x => x.LayoutRefId == Layout_ID && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();
                    var activityId = 0;
                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", Layout_ID);
                    SqlParameter activityParam = new SqlParameter("@ActivityRefId", activityId);

                    var activitiesRegistrationTranslationDetails = context.ActivitiesRegistrationTranslation.FromSql("GetActivitiesRegistration @LayoutRefId,@ActivityRefId", layoutParam, activityParam).ToList();
                    List<ActivitiesRegistrationDetails> activitiesRegisrationList = new List<ActivitiesRegistrationDetails>();
                    var activitiesRegisrationDetails = activitiesRegistrationTranslationDetails.Where(AR => AR.LayoutRefId == Layout_ID && AR.ActivitiesRegistrationName != "Start New"
                                && AR.ActivitiesRegistrationName != "Start Same" && AR.ActivitiesRegistrationName != "End"
                                && AR.WorkingCodeRefId != "Registration" && AR.Visible == true && AR.ActRegPosition == 0).ToList();

                    foreach (var actRegInfo in activitiesRegisrationDetails)
                    {
                        ActivitiesRegistrationDetails actReg = new ActivitiesRegistrationDetails();

                        actReg.ActivitiesRegistrationId = actRegInfo.ActivitiesRegistrationId;
                        actReg.CustomerRefId = actRegInfo.CustomerRefId;
                        actReg.LayoutRefId = actRegInfo.LayoutRefId;
                        actReg.ActivitiesRegistrationName = _dashboardRepository.GetActivitiesRegistrationName(actRegInfo, languageName);
                        actReg.WorkingCodeRefId = actRegInfo.WorkingCodeRefId;
                        actReg.PlanningRefId = actRegInfo.PlanningRefId;
                        actReg.PlanningOverviewRefId = actRegInfo.PlanningOverviewRefId;
                        actReg.SpecificRefId = actRegInfo.SpecificRefId;
                        actReg.Visible = Convert.ToBoolean(actRegInfo.Visible);
                        actReg.ConfirmButton = Convert.ToBoolean(actRegInfo.ConfirmButton);
                        actReg.PTORefId = actRegInfo.PTORefId;
                        actReg.InterruptibleByRefId = actRegInfo.InterruptibleByRefId;
                        actReg.TransportType = Convert.ToBoolean(actRegInfo.TransportType);
                        actReg.EmptyFullSolo = Convert.ToBoolean(actRegInfo.EmptyFullSolo);
                        actReg.Flexactivity = Convert.ToBoolean(actRegInfo.Flexactivity);
                        actReg.ISModification = actRegInfo.ISModification;
                        actReg.CreatedBy = actRegInfo.CreatedBy;
                        actReg.CreatedOn = actRegInfo.CreatedOn;
                        actReg.UpdatedBy = actRegInfo.UpdatedBy;
                        actReg.UpdatedOn = actRegInfo.UpdatedOn;
                        actReg.DefaultActivitiesRefId = actRegInfo.DefaultActivitiesRefId ?? 0;
                        actReg.CopyLayoutColor = (!actRegInfo.IsCopyLayout ? "Grey" : GetCopyLayoutColor(actRegInfo.ScreenLayoutIsModification));
                        activitiesRegisrationList.Add(actReg);
                    }

                    return activitiesRegisrationList.OrderBy(x => x.ActivitiesRegistrationName).ToList();
                    
                }
            }
            catch (Exception ex)
            {
                return new List<ActivitiesRegistrationDetails>();
            }
        }

        public string UpdateActivitiesRegistrationPosition(int LayoutID, int ActivitiesRegistrationId, int position)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ActivitiesRegistration.FirstOrDefault(IC => IC.ActivitiesRegistrationId == ActivitiesRegistrationId && IC.LayoutRefId == LayoutID);
                    if (result != null)
                    {
                        var IsCopyLayout = context.Layout.Where(x => x.LayoutId == LayoutID).Select(x => x.IsCopyLayout).FirstOrDefault();
                        if (IsCopyLayout)
                        {
                            if (result.ActRegPosition != position && result.ScreenLayoutIsModification == (int)EnumCopyColorCode.Empty)
                                result.ScreenLayoutIsModification = (int)EnumCopyColorCode.Yellow;
                        }

                        result.ActRegPosition = position;
                        context.SaveChanges();
                    }
                    return "Updated";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string UpdateDeActivitiesRegistrationPosition(int LayoutID, int ActivitiesRegistrationId, int position)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ActivitiesRegistration.SingleOrDefault(IC => IC.ActivitiesRegistrationId == ActivitiesRegistrationId && IC.LayoutRefId == LayoutID);
                    if (result != null)
                    {
                        var IsCopyLayout = context.Layout.Where(x => x.LayoutId == LayoutID).Select(x => x.IsCopyLayout).FirstOrDefault();
                        if (IsCopyLayout)
                        {
                            if (result.ActRegPosition != position && result.ScreenLayoutIsModification == (int)EnumCopyColorCode.Empty)
                                result.ScreenLayoutIsModification = (int)EnumCopyColorCode.Yellow;
                        }

                        result.ActRegPosition = position;
                        context.SaveChanges();
                    }
                    return "Updated";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<ActivitiesRegistrationDetails> GetRegistrationPosition(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var languageName = context.UsedLanguages.Where(x => x.LayoutRefId == Layout_ID && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();
                    var activityId = 0;
                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", Layout_ID);
                    SqlParameter activityParam = new SqlParameter("@ActivityRefId", activityId);

                    var activitiesRegistrationTranslationDetails = context.ActivitiesRegistrationTranslation.FromSql("GetActivitiesRegistration @LayoutRefId,@ActivityRefId", layoutParam, activityParam).ToList();
                    List<ActivitiesRegistrationDetails> activitiesRegisrationList = new List<ActivitiesRegistrationDetails>();
                    var activitiesRegisrationDetails = activitiesRegistrationTranslationDetails.Where(AR => AR.LayoutRefId == Layout_ID && AR.ActivitiesRegistrationName != "Start New"
                                && AR.ActivitiesRegistrationName != "Start Same"
                                && AR.ActivitiesRegistrationName != "End"
                                && AR.WorkingCodeRefId == "Registration" && AR.Visible && AR.ActRegPosition != 0).ToList();

                    foreach (var actRegInfo in activitiesRegisrationDetails)
                    {
                        ActivitiesRegistrationDetails actReg = new ActivitiesRegistrationDetails();

                        actReg.ActivitiesRegistrationId = actRegInfo.ActivitiesRegistrationId;
                        actReg.CustomerRefId = actRegInfo.CustomerRefId;
                        actReg.LayoutRefId = actRegInfo.LayoutRefId;
                        actReg.ActivitiesRegistrationName = _dashboardRepository.GetActivitiesRegistrationName(actRegInfo, languageName);
                        actReg.WorkingCodeRefId = actRegInfo.WorkingCodeRefId;
                        actReg.PlanningRefId = actRegInfo.PlanningRefId;
                        actReg.PlanningOverviewRefId = actRegInfo.PlanningOverviewRefId;
                        actReg.SpecificRefId = actRegInfo.SpecificRefId;
                        actReg.Visible = Convert.ToBoolean(actRegInfo.Visible);
                        actReg.ConfirmButton = Convert.ToBoolean(actRegInfo.ConfirmButton);
                        actReg.PTORefId = actRegInfo.PTORefId;
                        actReg.InterruptibleByRefId = actRegInfo.InterruptibleByRefId;
                        actReg.TransportType = Convert.ToBoolean(actRegInfo.TransportType);
                        actReg.EmptyFullSolo = Convert.ToBoolean(actRegInfo.EmptyFullSolo);
                        actReg.Flexactivity = Convert.ToBoolean(actRegInfo.Flexactivity);
                        actReg.ISModification = actRegInfo.ISModification;
                        actReg.CreatedBy = actRegInfo.CreatedBy;
                        actReg.CreatedOn = actRegInfo.CreatedOn;
                        actReg.UpdatedBy = actRegInfo.UpdatedBy;
                        actReg.UpdatedOn = actRegInfo.UpdatedOn;
                        actReg.ActRegPosition = actRegInfo.ActRegPosition;
                        actReg.DefaultActivitiesRefId = actRegInfo.DefaultActivitiesRefId ?? 0;
                        actReg.CopyLayoutColor = (!actRegInfo.IsCopyLayout ? "Grey" : GetCopyLayoutColor(actRegInfo.ScreenLayoutIsModification));
                        activitiesRegisrationList.Add(actReg);
                    }

                    return activitiesRegisrationList.OrderBy(x => x.ActRegPosition).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ActivitiesRegistrationDetails>();
            }
        }
        public List<ActivitiesRegistrationDetails> GetActivitiesPosition(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var languageName = context.UsedLanguages.Where(x => x.LayoutRefId == Layout_ID && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();
                    var activityId = 0;
                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", Layout_ID);
                    SqlParameter activityParam = new SqlParameter("@ActivityRefId", activityId);

                    var activitiesRegistrationTranslationDetails = context.ActivitiesRegistrationTranslation.FromSql("GetActivitiesRegistration @LayoutRefId,@ActivityRefId", layoutParam, activityParam).ToList();
                    List<ActivitiesRegistrationDetails> activitiesRegisrationList = new List<ActivitiesRegistrationDetails>();
                    var activitiesRegisrationDetails = activitiesRegistrationTranslationDetails.Where(AR => AR.LayoutRefId == Layout_ID && AR.ActivitiesRegistrationName != "Start New"
                                && AR.ActivitiesRegistrationName != "Start Same"
                                && AR.ActivitiesRegistrationName != "End"
                                && AR.WorkingCodeRefId != "Registration" && AR.Visible && AR.ActRegPosition != 0).ToList();

                    foreach (var actRegInfo in activitiesRegisrationDetails)
                    {
                        ActivitiesRegistrationDetails actReg = new ActivitiesRegistrationDetails();

                        actReg.ActivitiesRegistrationId = actRegInfo.ActivitiesRegistrationId;
                        actReg.CustomerRefId = actRegInfo.CustomerRefId;
                        actReg.LayoutRefId = actRegInfo.LayoutRefId;
                        actReg.ActivitiesRegistrationName = _dashboardRepository.GetActivitiesRegistrationName(actRegInfo, languageName);
                        actReg.WorkingCodeRefId = actRegInfo.WorkingCodeRefId;
                        actReg.PlanningRefId = actRegInfo.PlanningRefId;
                        actReg.PlanningOverviewRefId = actRegInfo.PlanningOverviewRefId;
                        actReg.SpecificRefId = actRegInfo.SpecificRefId;
                        actReg.Visible = Convert.ToBoolean(actRegInfo.Visible);
                        actReg.ConfirmButton = Convert.ToBoolean(actRegInfo.ConfirmButton);
                        actReg.PTORefId = actRegInfo.PTORefId;
                        actReg.InterruptibleByRefId = actRegInfo.InterruptibleByRefId;
                        actReg.TransportType = Convert.ToBoolean(actRegInfo.TransportType);
                        actReg.EmptyFullSolo = Convert.ToBoolean(actRegInfo.EmptyFullSolo);
                        actReg.Flexactivity = Convert.ToBoolean(actRegInfo.Flexactivity);
                        actReg.ISModification = actRegInfo.ISModification;
                        actReg.CreatedBy = actRegInfo.CreatedBy;
                        actReg.CreatedOn = actRegInfo.CreatedOn;
                        actReg.UpdatedBy = actRegInfo.UpdatedBy;
                        actReg.UpdatedOn = actRegInfo.UpdatedOn;
                        actReg.ActRegPosition = actRegInfo.ActRegPosition;
                        actReg.DefaultActivitiesRefId = actRegInfo.DefaultActivitiesRefId ?? 0;
                        actReg.CopyLayoutColor = (!actRegInfo.IsCopyLayout ? "Grey" : GetCopyLayoutColor(actRegInfo.ScreenLayoutIsModification));
                        activitiesRegisrationList.Add(actReg);
                    }

                    return activitiesRegisrationList.OrderBy(x => x.ActRegPosition).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ActivitiesRegistrationDetails>();
            }
        }

        public string GetCopyLayoutColor(int IsModification)
        {
            var colorname = "";
            if (IsModification == (int)EnumCopyColorCode.Green)
                colorname = "Green";
            else if (IsModification == (int)EnumCopyColorCode.Yellow)
                colorname = "Yellow";
            else if (IsModification == (int)EnumCopyColorCode.Red)
                colorname = "Red";
            else
                colorname = "Grey";

            return colorname;
        }
    }
}

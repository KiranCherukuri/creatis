﻿using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.Infrastructure.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CREATIS.Infrastructure.Repository
{
    public class PlanningAndPartnerCodeOverviewRepository : IPlanningAndPartnerCodeOverviewRepository
    {
        public Creatis_Context dbContext;
        
        public List<PlanningAndPartnerCodeOverview> GetPlanningAndPartnerCodeOverview(int customerRefId, int layoutRefId)
        {
            List<PlanningAndPartnerCodeOverview> inList;
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (customerRefId == 999999 && layoutRefId == 999999)
                    {
                        inList = new List<PlanningAndPartnerCodeOverview>();
                    }
                    else
                    {
                        SqlParameter layoutParam = new SqlParameter("@LayoutRefId", layoutRefId);
                        SqlParameter customerParam = new SqlParameter("@CustomerRefId", customerRefId);
                        inList = context.PlanningAndPartnerCodeOverview.FromSql("GetPlanningAndPartnerCodeOverview @LayoutRefId,@CustomerRefId", customerParam, layoutParam).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<PlanningAndPartnerCodeOverview>();
            }
            return inList;
        }
    }
}

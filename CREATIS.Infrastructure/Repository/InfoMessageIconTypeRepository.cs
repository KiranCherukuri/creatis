﻿using CREATIS.ApplicationCore.Modals;
using CREATIS.Infrastructure.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CREATIS.Infrastructure.Repository
{
    public class InfoMessageIconTypeRepository : IInfoMessageIconTypeRepository
    {
        public Creatis_Context dbContext;
        public String SaveInfoMessageIconType(InfoMessageIconType InfoMessageIconTypeModel)
        {
            var result = "";
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (InfoMessageIconTypeModel.InfoMessageIconTypeId > 0)
                    {
                        var target = context.InfoMessageIconType.Where(x => x.InfoMessageIconTypeId == InfoMessageIconTypeModel.InfoMessageIconTypeId).FirstOrDefault();
                        if (target != null)
                        {
                            target.InfoMessageIconTypeName = InfoMessageIconTypeModel.InfoMessageIconTypeName;
                            target.IsActive = InfoMessageIconTypeModel.IsActive;
                            target.UpdatedBy = InfoMessageIconTypeModel.UpdatedBy;
                            target.UpdatedOn = InfoMessageIconTypeModel.UpdatedOn;
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        context.InfoMessageIconType.Add(InfoMessageIconTypeModel);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public List<InfoMessageIconType> GetInfoMessageIconType()
        {
            List<InfoMessageIconType> InfoMessageIconTypeList;
            try
            {
                using (var context = new Creatis_Context())
                {
                    InfoMessageIconTypeList = context.InfoMessageIconType.Select(x => new InfoMessageIconType()
                    {
                        InfoMessageIconTypeId = x.InfoMessageIconTypeId,
                        InfoMessageIconTypeName = x.InfoMessageIconTypeName,
                        Active = (x.IsActive == true ? "Active" : "InActive")
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return InfoMessageIconTypeList;
        }
    }
}

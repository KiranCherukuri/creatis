﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.Infrastructure.IRepository;
using CREATIS.Infrastructure;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CREATIS.Infrastructure.Repository
{
    public class DashboardRepository : Utility, IDashboardRepository
    {
        public readonly ITranslatorRepository _translatorRepository;
        public readonly IQuestionPathRepository _questionPathRepository;
        public readonly ICopyLayoutRepository _copyLayoutRepository;
        public QuestionPathTemplate questionpathtemplateConfiguration { get; }
        public DashboardRepository(ITranslatorRepository translatorRepository, IQuestionPathRepository questionPathRepository, ICopyLayoutRepository copylayoutRepository, Microsoft.Extensions.Options.IOptions<QuestionPathTemplate> questionPathTemplate)
        {
            this._translatorRepository = translatorRepository;
            this._questionPathRepository = questionPathRepository;
            this._copyLayoutRepository = copylayoutRepository;
            questionpathtemplateConfiguration = questionPathTemplate.Value;
        }

        public Creatis_Context dbconext;
        public string GetCopyLayoutName(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.LayoutName).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetLayoutorgin(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.LayoutOrigin).FirstOrDefault();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LayoutNotes> GetLayoutNotes(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var layoutNotesList = (from L in context.LayoutNotes.ToList()
                                           join LS in context.LayoutStatus.ToList()
                                           on L.LayoutStatusRefId equals LS.Id
                                           where L.LayoutRefId == LayoutId
                                           select new LayoutNotes()
                                           {
                                               Id = L.Id,
                                               CreatedByName = L.CreatedByName,
                                               Notes = L.Notes,
                                               LayoutStatusName = LS.Name,
                                               LayoutRefId = L.LayoutRefId,
                                               LayoutStatusRefId = L.LayoutStatusRefId,
                                               CreatedOn = L.CreatedOn
                                           }).OrderByDescending(x => x.CreatedOn).ToList();


                    return layoutNotesList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetCustomerandLayoutName(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from L in context.Layout
                                join C in context.Customers
                                on L.CustomerRefId equals C.CustomerId
                                join S in context.LayoutStatus
                                on L.LayoutStatusRefId equals S.Id
                                where L.LayoutId == layoutId

                                select C.Name + " - " + L.LayoutName + " (" + S.Name + ")[" + L.CustomerRefId

                                ).SingleOrDefault();
                    return data == null ? string.Empty : data.ToString();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<LayoutStatus> GetLayoutStatus(int Status)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<LayoutStatus> returnList = context.LayoutStatus.Where(x => (Status == 0 ? (x.IsRequestStatus == false) : (x.IsRequestStatus == true)) && x.IsActive == 1).ToList();
                    return returnList;
                }
            }
            catch (Exception ex)
            {
                return new List<LayoutStatus>();
            }
        }
        public string UpdateLayoutStatus(int LayoutID, int layoutStatusId, string userId, string emails)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Layout.SingleOrDefault(IC => IC.LayoutId == LayoutID);
                    if (result != null)
                    {
                        if (layoutStatusId == (int)EnumLayoutStatus.SupportRequested || layoutStatusId == (int)EnumLayoutStatus.ValidationRequested)
                            result.RequesterEmailId = emails;

                        result.InitiatorEmailId = emails;
                        result.LayoutStatusRefId = layoutStatusId;
                        result.UpdatedBy = userId;
                        result.UpdatedOn = DateTime.Now;
                        context.SaveChanges();
                    }
                    return "Updated";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string GetLayoutFromEMail(int layoutId, int layoutStatusId, string CustomizingEmail)
        {
            try
            {
                var resultEmails = "";
                using (var context = new Creatis_Context())
                {
                    var layoutEmails = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => new { x.EmailId, x.RequesterEmailId, x.InitiatorEmailId }).FirstOrDefault();
                    var layoutStatusInfo = context.LayoutStatus.FirstOrDefault(x => x.Id == layoutStatusId);

                    layoutStatusInfo = layoutStatusInfo ?? new LayoutStatus();

                    if (layoutStatusInfo.IsSentMailToCustomizing)
                        resultEmails = CustomizingEmail + "|";
                    if (layoutStatusInfo.IsSentMailToInitiator)
                        resultEmails = resultEmails + (layoutEmails.InitiatorEmailId ?? "");
                    if (layoutStatusInfo.IsSentMailToRequester)
                        resultEmails = resultEmails + (((resultEmails.Length > 0) && (layoutEmails.RequesterEmailId != null && layoutEmails.RequesterEmailId.Length > 0)) ? ("ƒ" + layoutEmails.RequesterEmailId) : "");
                    if (layoutStatusInfo.IsSentMailToOwner)
                        resultEmails = resultEmails + (((resultEmails.Length > 0) && (layoutEmails.EmailId != null && layoutEmails.EmailId.Length > 0)) ? ("ƒ" + layoutEmails.EmailId) : "");

                    return resultEmails;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string UpdateLayoutName(int layoutId, string LayoutType)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Layout.SingleOrDefault(IC => IC.LayoutId == layoutId);
                    if (result != null)
                    {
                        result.LayoutType = LayoutType;
                        context.SaveChanges();
                    }
                    return "Updated";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<OBCTypeMaster> OBCTypesMaster(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    List<OBCTypeMaster> returnList = context.OBCTypeMaster.Select(x => new OBCTypeMaster()
                    {
                        OBCTypeId = x.OBCTypeId,
                        OBCTypeDescription = x.OBCTypeDescription,
                        IsActive = x.IsActive,
                        CopyColorCode = (isCopyLayout ? GetCopyLayoutColor(OBCTypesIsModification(layoutId, x.OBCTypeId, isCopyLayout)) : "Grey")

                    }).ToList();
                    return returnList;
                }

            }
            catch (Exception ex)
            {
                return new List<OBCTypeMaster>();
            }
        }
        public int OBCTypesIsModification(int layoutId, int oBCTypeId, bool isCopyLayout)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isModification = (int)EnumCopyColorCode.Green;
                    if (isCopyLayout && context.OBCType.Where(x => x.LayoutRefId == layoutId && x.OBCTypeRefId == oBCTypeId).Any())
                        isModification = context.OBCType.Where(x => x.LayoutRefId == layoutId && x.OBCTypeRefId == oBCTypeId).Select(x => x.IsModification).FirstOrDefault();
                    else if (isCopyLayout)
                        isModification = (int)EnumCopyColorCode.Empty;

                    return isModification;
                }
            }
            catch (Exception ex)
            {
                return (int)EnumCopyColorCode.Green;
            }
        }

        public List<OBCType> OBCTypes(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var returnList = (from L in context.Layout
                                      join OBC in context.OBCType
                                      on L.LayoutId equals OBC.LayoutRefId
                                      where L.LayoutId == layoutId
                                      select new OBCType()
                                      {
                                          OBCTypeId = OBC.OBCTypeId,
                                          CustomerRefId = OBC.CustomerRefId,
                                          LayoutRefId = OBC.LayoutRefId,
                                          OBCTypeRefId = OBC.OBCTypeRefId,
                                          CreatedBy = OBC.CreatedBy,
                                          CreatedOn = OBC.CreatedOn,
                                          UpdatedBy = OBC.UpdatedBy,
                                          UpdatedOn = OBC.UpdatedOn
                                      }).ToList();

                    return returnList;
                }
            }
            catch (Exception ex)
            {
                return new List<OBCType>();
            }
        }
        public string SaveOBCTypes(OBCType OBCType)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from OM in context.OBCTypeMaster
                                where OM.OBCTypeId == OBCType.OBCTypeRefId
                                select OM.OBCTypeDescription
                                ).SingleOrDefault();

                    var result = context.Layout.SingleOrDefault(IC => IC.LayoutId == OBCType.LayoutRefId);
                    if (result != null)
                    {
                        result.LayoutType = data;
                        context.SaveChanges();

                        if (result.IsCopyLayout)
                            OBCType.IsModification = (int)EnumCopyColorCode.Green;
                    }

                    context.Add(OBCType);
                    context.SaveChanges();

                    return "";
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string UpdateOBCTypes(int OBCTypeId, int OBCTypeRefId, int LayoutRefId)
        {
            try
            {
                var isCopyLayout = false;
                using (var context = new Creatis_Context())
                {
                    var data = (from OM in context.OBCTypeMaster
                                where OM.OBCTypeId == OBCTypeRefId
                                select OM.OBCTypeDescription
                                ).FirstOrDefault();
                    var result = context.Layout.Where(IC => IC.LayoutId == LayoutRefId).FirstOrDefault();
                    if (result != null)
                    {
                        isCopyLayout = result.IsCopyLayout;
                        result.LayoutType = data;
                        context.SaveChanges();
                    }
                    var OBCUpdate = context.OBCType.Where(IC => IC.OBCTypeId == OBCTypeId).FirstOrDefault();
                    if (OBCUpdate != null)
                    {
                        if (isCopyLayout && OBCTypeRefId != OBCUpdate.OBCTypeRefId)
                            OBCUpdate.IsModification = (int)EnumCopyColorCode.Yellow;

                        OBCUpdate.OBCTypeRefId = OBCTypeRefId;
                        context.SaveChanges();
                    }
                    return "Updated";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public bool DeleteActiviesRegistrationByOBCType(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var actRegList = context.ActivitiesRegistration.Where(IC => IC.LayoutRefId == LayoutId).ToList();
                    if (actRegList.Count() > 0)
                    {
                        context.ActivitiesRegistration.RemoveRange(actRegList);
                        context.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<TransportTypes> GetTransportTypes(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var transportTypeList = context.TransportTypes.Where(x => x.LayoutRefId == layoutId).Select(x => new TransportTypes()
                    {
                        TransportTypeId = x.TransportTypeId,
                        CustomerRefId = x.CustomerRefId,
                        LayoutRefId = x.LayoutRefId,
                        TransportTypeName = x.TransportTypeName,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        UpdatedBy = x.UpdatedBy,
                        UpdatedOn = x.UpdatedOn,
                        CopyColorCode = isCopyLayout ? GetCopyLayoutColor(x.IsModification) : "Grey",
                        IsModification = x.IsModification
                    }).ToList();

                    return transportTypeList;
                }

            }
            catch (Exception ex)
            {
                return new List<TransportTypes>();
            }
        }
        public List<Layout> GetLayoutDetails(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = context.Layout.Where(L => L.LayoutId == layoutId).Select
                                (L => new Layout()
                                {
                                    LayoutId = L.LayoutId,
                                    CustomerRefId = L.CustomerRefId,
                                    LayoutName = L.LayoutName,
                                    LayoutType = L.LayoutType,
                                    LayoutOrigin = L.LayoutOrigin,
                                    LayoutStatus = L.LayoutStatus,
                                    IsLocked = L.IsLocked,
                                    CreatedBy = L.CreatedBy,
                                    CreatedOn = L.CreatedOn,
                                    UpdatedBy = L.UpdatedBy,
                                    UpdatedOn = L.UpdatedOn,
                                    IsCopyLayout = L.IsCopyLayout
                                }).ToList();

                    List<Layout> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public TransportTypes SaveTransportTypes(TransportTypes TransportTypes, string code)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var target = context.TransportTypes.Where(x => x.TransportTypeId == TransportTypes.TransportTypeId).FirstOrDefault();
                    int transportTypeId = 0;
                    if (target != null && code == "New")
                    {
                        target.TransportTypeName = TransportTypes.TransportTypeName;
                        target.UpdatedBy = TransportTypes.UpdatedBy;
                        target.UpdatedOn = TransportTypes.UpdatedOn;
                        target.IsModification = TransportTypes.IsModification;
                        target.IsActive = TransportTypes.IsActive;
                        context.SaveChanges();
                        transportTypeId = target.TransportTypeId;
                    }
                    else
                    {
                        if (code == "New")
                        {
                            context.Add(TransportTypes);
                            context.SaveChanges();
                        }
                        transportTypeId = TransportTypes.TransportTypeId;

                        var existsActRegList = context.ActivitiesRegistration.Where(x => x.LayoutRefId == TransportTypes.LayoutRefId && x.TransportType).ToList();
                        foreach (var actreg in existsActRegList)
                        {
                            Questions objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = actreg.ActivitiesRegistrationId;
                            objQuestion.CreatedBy = actreg.CreatedBy;
                            objQuestion.CreatedOn = DateTime.Now;
                            objQuestion.LayoutRefId = actreg.LayoutRefId;
                            objQuestion.Name = actreg.ActivitiesRegistrationName;
                            objQuestion.ParentId = 0;
                            objQuestion.TransporttypeRefId = TransportTypes.TransportTypeId;
                            objQuestion.PropertyName = actreg.ActivitiesRegistrationName;
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = true;
                            objQuestion.IsDefault = false;
                            objQuestion.IsChild = true;
                            _questionPathRepository.SaveQuestions(objQuestion);
                        }
                    }

                    var transportTypeCount = context.TransportTypes.Where(x => x.LayoutRefId == TransportTypes.LayoutRefId && x.IsActive).Distinct().Count();
                    if (transportTypeCount >= 2)
                    {
                        var alaisNameDetails = context.AliasNameDetails.Where(x => x.LayoutRefId == TransportTypes.LayoutRefId && x.AliasName == "TransportType" && x.PropertyName == "Transport Type").Count();
                        if (alaisNameDetails == 0)
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = TransportTypes.LayoutRefId;
                            AND.ActivitiesRefId = 0;
                            AND.AliasName = "TransportType";
                            AND.PropertyName = "Transport Type";
                            AND.CreatedBy = TransportTypes.CreatedBy;
                            AND.CreatedOn = TransportTypes.CreatedOn;
                            AND.IsTempData = false;
                            AND.PropertyTableId = transportTypeId;
                            _questionPathRepository.SaveAliasNameDetails(AND);
                        }
                    }


                    if (target != null)
                    {
                        var transportTypeList = context.TransportTypePropertiesDetail.Where(x => x.DashboardTransportTypeId == target.TransportTypeId).Select(x => new { x.QuestionRefId, x.TransportTypeDetailId }).ToList();
                        foreach (var ttp in transportTypeList)
                        {
                            var tragetttd = context.TransportTypePropertiesDetail.Where(x => x.TransportTypeDetailId == ttp.TransportTypeDetailId).FirstOrDefault();
                            if (tragetttd != null)
                            {
                                tragetttd.Title = target.TransportTypeName;
                                context.SaveChanges();

                                /* Written By Selvam Thangaraj - Go */
                                var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == target.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

                                LangRefDetail langrefdetail = new LangRefDetail();
                                langrefdetail.RefId = tragetttd.TransportTypeDetailId;
                                langrefdetail.QuestionRefId = ttp.QuestionRefId;
                                langrefdetail.LayoutRefId = target.LayoutRefId;
                                langrefdetail.CustomerRefId = null;
                                langrefdetail.LangRefColTableId = (int)LangRefColTable.TransportTypePropertiesDetail_Title;
                                langrefdetail.LangText = target.TransportTypeName ?? "";
                                langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                                langrefdetail.CreatedBy = target.UpdatedBy;
                                langrefdetail.UpdatedBy = target.UpdatedBy;
                                _translatorRepository.SaveLangDetail(langrefdetail);

                                /* Written By Selvam Thangaraj - Stop */
                            }
                            var tragetQuestion = context.Questions.Where(x => x.QuestionId == ttp.QuestionRefId).FirstOrDefault();
                            if (tragetQuestion != null)
                            {
                                tragetQuestion.Name = target.TransportTypeName;
                                tragetQuestion.PropertyName = target.TransportTypeName;
                                context.SaveChanges();
                            }

                        }
                    }
                    else
                    {
                        var transportTypeList = context.TransportTypeProperties.Where(x => x.LayoutRefId == TransportTypes.LayoutRefId).Select(x => new { x.ActivitiesRefId, x.QuestionRefId, x.TransportTypeId, x.PlanningFeedbackCode, x.PartnerCode }).ToList();
                        foreach (var ttp in transportTypeList)
                        {

                            var objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = ttp.ActivitiesRefId;
                            objQuestion.CreatedBy = TransportTypes.CreatedBy;
                            objQuestion.CreatedOn = TransportTypes.CreatedOn;
                            objQuestion.LayoutRefId = TransportTypes.LayoutRefId;
                            objQuestion.Name = TransportTypes.TransportTypeName;
                            objQuestion.ParentId = ttp.QuestionRefId;
                            objQuestion.TransporttypeRefId = (context.Questions.Where(x => x.QuestionId == ttp.QuestionRefId).Select(x => x.TransporttypeRefId).FirstOrDefault());
                            objQuestion.PropertyName = "Transport Type Detail";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = false;
                            objQuestion.IsDefault = true;
                            objQuestion.IsChild = true;
                            var questions = _questionPathRepository.SaveQuestions(objQuestion);

                            TransportTypePropertiesDetail objAddTT = new TransportTypePropertiesDetail();
                            objAddTT.Title = TransportTypes.TransportTypeName;
                            objAddTT.PlanningFeedbackCode = ttp.PlanningFeedbackCode;
                            objAddTT.TransportTypeRefId = ttp.TransportTypeId;
                            objAddTT.InfoColumn = null;
                            objAddTT.InfoColumnAction = "Not set";
                            objAddTT.CreatedBy = TransportTypes.CreatedBy;
                            objAddTT.CreatedOn = TransportTypes.CreatedOn;
                            objAddTT.PartnerCode = ttp.PartnerCode;
                            objAddTT.QuestionRefId = questions.QuestionId;
                            objAddTT.DashboardTransportTypeId = TransportTypes.TransportTypeId;
                            context.TransportTypePropertiesDetail.Add(objAddTT);
                            context.SaveChanges();

                            var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == TransportTypes.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

                            /* Written By Selvam Thangaraj - Go */

                            LangRefDetail langrefdetail = new LangRefDetail();
                            langrefdetail.RefId = objAddTT.TransportTypeDetailId;
                            langrefdetail.QuestionRefId = questions.QuestionId;
                            langrefdetail.LayoutRefId = TransportTypes.LayoutRefId;
                            langrefdetail.CustomerRefId = null;
                            langrefdetail.LangRefColTableId = (int)LangRefColTable.TransportTypePropertiesDetail_Title;
                            langrefdetail.LangText = TransportTypes.TransportTypeName;
                            langrefdetail.Language = LanguageName ?? LanguagesEnum.English.ToString();
                            langrefdetail.CreatedBy = TransportTypes.CreatedBy;
                            langrefdetail.UpdatedBy = TransportTypes.CreatedBy;
                            _translatorRepository.SaveLangDetail(langrefdetail);

                            /* Written By Selvam Thangaraj - Stop */
                        }

                    }
                    return TransportTypes;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool ValidateTransportType(int LayoutId, string Name, int TransportTypeId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var target = context.TransportTypes.Where(x => x.LayoutRefId == LayoutId && x.TransportTypeName.ToLower() == Name.ToLower() && x.TransportTypeId != TransportTypeId).FirstOrDefault();
                    if (target != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public TransportTypes RevertTransportTypes(TransportTypes TransportTypes)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var target = context.TransportTypes.Where(x => x.TransportTypeId == TransportTypes.TransportTypeId).FirstOrDefault();
                    if (target != null)
                    {
                        target.UpdatedBy = TransportTypes.UpdatedBy;
                        target.UpdatedOn = TransportTypes.UpdatedOn;
                        target.IsModification = TransportTypes.IsModification;
                        target.IsActive = TransportTypes.IsActive;
                        context.SaveChanges();
                    }
                    return target ?? new TransportTypes();
                }
            }
            catch (Exception ex)
            {
                return new TransportTypes();
            }
        }

        public UsedLanguages RevertUsedLanguage(UsedLanguages usedLanguages)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var target = context.UsedLanguages.Where(x => x.UsedLanguagesId == usedLanguages.UsedLanguagesId).FirstOrDefault();
                    if (target != null)
                    {
                        target.UpdatedBy = usedLanguages.UpdatedBy;
                        target.UpdatedOn = usedLanguages.UpdatedOn;
                        target.IsModification = usedLanguages.IsModification;
                        target.IsActive = usedLanguages.IsActive;
                        context.SaveChanges();
                    }
                    return target ?? new UsedLanguages();
                }
            }
            catch (Exception ex)
            {
                return new UsedLanguages();
            }
        }
        public List<DashboardLanguage> GetUsedLanguages(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from UL in context.UsedLanguages
                                join LA in context.LanguageMaster
                                on UL.LanguageRefId equals LA.LanguageId
                                where UL.LayoutRefId == layoutId
                                select new DashboardLanguage()
                                {
                                    IsDefault = UL.IsDefault,
                                    UsedLanguagesId = UL.UsedLanguagesId,
                                    CustomerRefId = UL.CustomerRefId,
                                    LayoutRefId = UL.LayoutRefId,
                                    LanguageRefId = UL.LanguageRefId,
                                    Language = (UL.IsDefault == 1 ? LA.LanguageDescription + " (Main)" : LA.LanguageDescription),
                                    CreatedBy = UL.CreatedBy,
                                    CreatedOn = UL.CreatedOn,
                                    UpdatedBy = UL.UpdatedBy,
                                    UpdatedOn = UL.UpdatedOn,
                                    CopyColorCode = (UL.Layout.IsCopyLayout ? GetCopyLayoutColor(UL.IsModification) : "Grey"),
                                    IsModification = UL.IsModification
                                }).ToList();

                    List<DashboardLanguage> returnList;
                    returnList = data.Where(x => x.LanguageRefId == 66).ToList();
                    returnList.AddRange(data.Where(x => x.LanguageRefId != 66).OrderByDescending(x => x.IsDefault).ThenBy(x => x.Language).ToList());

                    return returnList;
                }

            }
            catch (Exception ex)
            {
                return new List<DashboardLanguage>();
            }
        }
        public List<LanguageMaster> GetRemainingLanguages(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var usedLanguageIds = context.UsedLanguages.Where(x => x.LayoutRefId == layoutId).Select(x => x.LanguageRefId).ToList();
                    var remainingLanguages = context.LanguageMaster.Where(x => !usedLanguageIds.Contains(x.LanguageId)).ToList();
                    return remainingLanguages.OrderBy(x => x.LanguageDescription).ToList();
                }

            }
            catch (Exception ex)
            {
                return new List<LanguageMaster>();
            }
        }
        public List<PlanningTypeMaster> GetPlanningTypeMaster(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var returnList = context.PlanningTypeMaster.Where(x => x.IsActive == 1).Select(x => new PlanningTypeMaster()
                    {
                        PlanningTypeMasterId = x.PlanningTypeMasterId,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        IsActive = x.IsActive,
                        PlanningTypeMasterDescription = x.PlanningTypeMasterDescription,
                        UpdatedBy = x.UpdatedBy,
                        UpdatedOn = x.UpdatedOn,
                        CopyColorCode = (isCopyLayout ? GetCopyLayoutColor(PlanningTypeIsModification(layoutId, x.PlanningTypeMasterId, isCopyLayout)) : "Grey")

                    }).ToList();
                    return returnList;
                }

            }
            catch (Exception ex)
            {
                return new List<PlanningTypeMaster>();
            }
        }
        public int PlanningTypeIsModification(int layoutId, int planningTypeId, bool isCopyLayout)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isModification = (int)EnumCopyColorCode.Green;
                    if (isCopyLayout && context.PlanningType.Where(x => x.LayoutRefId == layoutId && x.PlanningTypeRefId == planningTypeId).Any())
                        isModification = context.PlanningType.Where(x => x.LayoutRefId == layoutId && x.PlanningTypeRefId == planningTypeId).Select(x => x.IsModification).FirstOrDefault();
                    else if (isCopyLayout)
                        isModification = (int)EnumCopyColorCode.Empty;

                    return isModification;
                }
            }
            catch (Exception ex)
            {
                return (int)EnumCopyColorCode.Green;
            }
        }
        public List<PlanningType> GetPlanningType(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", layoutId);
                    var boPlanningTypeList = context.BOPlanningType.FromSql("GetPlanningType @LayoutRefId", layoutParam).ToList();

                    var planningTypeList = boPlanningTypeList.Select(x => new PlanningType()
                    {
                        PlanningTypeId = x.PlanningTypeId,
                        CustomerRefId = x.CustomerRefId,
                        LayoutRefId = x.LayoutRefId,
                        PlanningTypeRefId = x.PlanningTypeRefId,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        UpdatedBy = x.UpdatedBy,
                        UpdatedOn = x.UpdatedOn,
                        PlanningTypeName = x.PlanningTypeName,
                        IsActive = x.IsActive
                    }).Where(x => x.IsActive).ToList();

                    return planningTypeList;
                }
            }
            catch (Exception ex)
            {
                return new List<PlanningType>();
            }
        }
        public List<PlanningType> ConsolidatedPlanningType(int layoutId, List<string> planningTypes)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<PlanningType> planningTypeList = new List<PlanningType>();
                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var newPlaningType = planningTypes.Select(Int32.Parse).ToList();
                    if (isCopyLayout)
                    {
                        var existPlaningTypeList = context.PlanningType.Where(x => x.LayoutRefId == layoutId).Select(x => new { x.PlanningTypeRefId, x.IsActive, x.IsModification }).ToList();
                        var existPlaningType = existPlaningTypeList.Select(x => x.PlanningTypeRefId).ToList();

                        var commonList = existPlaningType.Where(x => newPlaningType.Contains(x)).ToList();
                        var newList = newPlaningType.Where(x => !existPlaningType.Contains(x)).ToList();
                        var removeList = existPlaningType.Where(x => !newPlaningType.Contains(x)).ToList();

                        foreach (var common in commonList)
                        {
                            var isModification = existPlaningTypeList.Where(x => x.PlanningTypeRefId == common).Select(x => x.IsModification).FirstOrDefault();
                            PlanningType planningType = new PlanningType();
                            planningType.PlanningTypeId = common;
                            planningType.IsActive = true;
                            planningType.IsModification = isModification == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : isModification;
                            planningTypeList.Add(planningType);
                        }
                        foreach (var newplanning in newList)
                        {
                            PlanningType planningType = new PlanningType();
                            planningType.PlanningTypeId = newplanning;
                            planningType.IsActive = true;
                            planningType.IsModification = (int)EnumCopyColorCode.Green;
                            planningTypeList.Add(planningType);
                        }
                        foreach (var remove in removeList)
                        {
                            var isModification = existPlaningTypeList.Where(x => x.PlanningTypeRefId == remove).Select(x => x.IsModification).FirstOrDefault();
                            PlanningType planningType = new PlanningType();
                            planningType.PlanningTypeId = remove;
                            planningType.IsActive = false;
                            planningType.IsModification = isModification == (int)EnumCopyColorCode.Green ? (int)EnumCopyColorCode.Empty : (int)EnumCopyColorCode.Red;
                            planningTypeList.Add(planningType);
                        }
                    }
                    else
                    {
                        foreach (var newplanning in newPlaningType)
                        {
                            PlanningType planningType = new PlanningType();
                            planningType.PlanningTypeId = newplanning;
                            planningType.IsActive = true;
                            planningType.IsModification = (int)EnumCopyColorCode.Green;
                            planningTypeList.Add(planningType);
                        }
                    }

                    return planningTypeList;
                }
            }
            catch (Exception ex)
            {
                return new List<PlanningType>();
            }
        }

        public PlanningType SavePlanningType(PlanningType PlanningType)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var existingPlanningType = context.PlanningType.FirstOrDefault(a => a.LayoutRefId == PlanningType.LayoutRefId && a.PlanningTypeRefId == PlanningType.PlanningTypeRefId);
                    if (existingPlanningType == null)
                        context.Add(PlanningType);
                    else
                    {
                        existingPlanningType.IsModification = PlanningType.IsModification;
                        existingPlanningType.IsActive = PlanningType.IsActive;

                        if (!PlanningType.IsActive && PlanningType.IsModification == (int)EnumCopyColorCode.Empty)
                        {
                            context.PlanningType.Remove(existingPlanningType);
                            context.SaveChanges();
                        }
                    }
                    context.SaveChanges();

                    return existingPlanningType ?? PlanningType;
                }
            }
            catch (Exception ex)
            {
                return new PlanningType();
            }
        }
        public bool UpdateTripDefaultActivitiesRegisration(int layoutId, int DefaultActivitiesId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ActivitiesRegistration.SingleOrDefault(IC => IC.LayoutRefId == layoutId && IC.DefaultActivitiesRefId == DefaultActivitiesId);
                    if (result != null)
                    {
                        result.ISModification = (result.ISModification == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.ISModification);
                        result.IsModificationActivitiesRegistrationName = (result.IsModificationActivitiesRegistrationName == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationActivitiesRegistrationName);
                        result.IsModificationWorkingCodeRefId = (result.IsModificationWorkingCodeRefId == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationWorkingCodeRefId);
                        result.IsModificationPlanningRefId = (result.IsModificationPlanningRefId == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationPlanningRefId);
                        result.IsModificationPlanningOverviewRefId = (result.IsModificationPlanningOverviewRefId == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationPlanningOverviewRefId);
                        result.IsModificationSpecificRefId = (result.IsModificationSpecificRefId == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationSpecificRefId);
                        result.IsModificationVisible = (result.IsModificationVisible == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationVisible);
                        result.IsModificationConfirmButton = (result.IsModificationConfirmButton == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationConfirmButton);
                        result.IsModificationTransportType = (result.IsModificationTransportType == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationTransportType);
                        result.IsModificationEmptyFullSolo = (result.IsModificationEmptyFullSolo == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationEmptyFullSolo);
                        result.IsModificationFlexactivity = (result.IsModificationFlexactivity == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationFlexactivity);
                        result.IsModificationPTORefId = (result.IsModificationPTORefId == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationPTORefId);
                        result.IsModificationInterruptibleByRefId = (result.IsModificationInterruptibleByRefId == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.IsModificationInterruptibleByRefId);

                        context.SaveChanges();

                        var questionList = context.Questions.Where(x => x.LayoutRefId == layoutId && x.ActivitiesRefId == result.ActivitiesRegistrationId).ToList();
                        foreach (var questions in questionList)
                        {
                            questions.ISModification = (result.ISModification == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : result.ISModification);
                        }
                        context.SaveChanges();
                        return false;
                    }
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                return true;
            }
        }
        public string DeletePlanningType(int Layout_Id, int PlanningTypeRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isActive = false;
                    var result = context.PlanningType.SingleOrDefault(IC => IC.LayoutRefId == Layout_Id && IC.PlanningTypeRefId == PlanningTypeRefId);
                    if (result != null)
                    {
                        isActive = result.IsActive;
                        var isCopyLayout = context.Layout.Where(x => x.LayoutId == Layout_Id).Select(x => x.IsCopyLayout).FirstOrDefault();
                        if (!isCopyLayout || result.IsModification == (int)EnumCopyColorCode.Green)
                        {
                            context.PlanningType.Remove(result);
                            context.SaveChanges();
                        }
                        else
                        {
                            result.IsModification = result.IsModification == (int)EnumCopyColorCode.Empty ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty;
                            result.IsActive = false;
                            context.SaveChanges();
                        }


                        var OBCTypeId = context.OBCType.Where(x => x.LayoutRefId == Layout_Id).Select(x => x.OBCTypeRefId).FirstOrDefault();
                        List<int> defaultActivitiesIds = new List<int>();

                        if ((int)EnumOBCType.TX_SKY == OBCTypeId)
                        {
                            if ((int)EnumPlanningType.Trip == PlanningTypeRefId)
                            {
                                defaultActivitiesIds.Add((int)EnumDefaultActivities.TX_SKY_Start_Trip);
                                defaultActivitiesIds.Add((int)EnumDefaultActivities.TX_SKY_Stop_Trip);
                            }
                        }
                        else if ((int)EnumOBCType.TX_SKY_FLEX == OBCTypeId)
                        {
                            if ((int)EnumPlanningType.Trip == PlanningTypeRefId)
                            {
                                defaultActivitiesIds.Add((int)EnumDefaultActivities.TX_SKY_FLEX_Start_Trip);
                                defaultActivitiesIds.Add((int)EnumDefaultActivities.TX_SKY_FLEX_Stop_Trip);
                            }
                        }
                        else if ((int)EnumOBCType.TX_SMART == OBCTypeId)
                        {
                            if ((int)EnumPlanningType.Trip == PlanningTypeRefId)
                            {
                                defaultActivitiesIds.Add((int)EnumDefaultActivities.TX_SMART_Start_Trip);
                                defaultActivitiesIds.Add((int)EnumDefaultActivities.TX_SMART_Stop_Trip);
                            }
                        }
                        else if ((int)EnumOBCType.TX_GO_FLEX == OBCTypeId)
                        {
                            if ((int)EnumPlanningType.Trip == PlanningTypeRefId)
                            {
                                defaultActivitiesIds.Add((int)EnumDefaultActivities.TX_GO_FLEX_Start_Trip);
                                defaultActivitiesIds.Add((int)EnumDefaultActivities.TX_GO_FLEX_Stop_Trip);
                            }
                        }

                        if (isCopyLayout)
                        {
                            foreach (var defaultActivitiesId in defaultActivitiesIds)
                            {
                                var activiesmodal = context.ActivitiesRegistration.SingleOrDefault(IC => IC.LayoutRefId == Layout_Id && IC.DefaultActivitiesRefId == defaultActivitiesId);
                                if (activiesmodal != null && (activiesmodal.ISModification == (int)EnumCopyColorCode.Empty || activiesmodal.ISModification == (int)EnumCopyColorCode.Red))
                                {
                                    activiesmodal.ISModification = activiesmodal.ISModification == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationActivitiesRegistrationName = (activiesmodal.IsModificationActivitiesRegistrationName == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);
                                    activiesmodal.IsModificationWorkingCodeRefId = (activiesmodal.IsModificationWorkingCodeRefId == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);
                                    activiesmodal.IsModificationPlanningRefId = (activiesmodal.IsModificationPlanningRefId == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);
                                    activiesmodal.IsModificationPlanningOverviewRefId = (activiesmodal.IsModificationPlanningOverviewRefId == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);
                                    activiesmodal.IsModificationSpecificRefId = (activiesmodal.IsModificationSpecificRefId == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);
                                    activiesmodal.IsModificationVisible = (activiesmodal.IsModificationVisible == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);
                                    activiesmodal.IsModificationConfirmButton = (activiesmodal.IsModificationConfirmButton == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);
                                    activiesmodal.IsModificationTransportType = (activiesmodal.IsModificationTransportType == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);
                                    activiesmodal.IsModificationEmptyFullSolo = (activiesmodal.IsModificationEmptyFullSolo == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);
                                    activiesmodal.IsModificationFlexactivity = (activiesmodal.IsModificationFlexactivity == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);
                                    activiesmodal.IsModificationPTORefId = (activiesmodal.IsModificationPTORefId == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);
                                    activiesmodal.IsModificationInterruptibleByRefId = (activiesmodal.IsModificationInterruptibleByRefId == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty);

                                    context.SaveChanges();

                                    var questionList = context.Questions.Where(x => x.LayoutRefId == Layout_Id && x.ActivitiesRefId == activiesmodal.ActivitiesRegistrationId).ToList();
                                    foreach (var questions in questionList)
                                    {
                                        questions.ISModification = questions.ISModification == (int)EnumCopyColorCode.Empty && isActive ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty;
                                    }
                                    context.SaveChanges();
                                }
                                else
                                {
                                    if (activiesmodal != null)
                                    {
                                        var deleteresultQuestions = context.Questions.Where(IC => IC.LayoutRefId == Layout_Id && IC.ActivitiesRefId == activiesmodal.ActivitiesRegistrationId).ToList();
                                        if (deleteresultQuestions.Count() > 0)
                                        {
                                            var IsCopyLayout = context.Layout.Where(x => x.LayoutId == Layout_Id).Select(x => x.IsCopyLayout).FirstOrDefault();
                                            _questionPathRepository.DeleteQuestions(deleteresultQuestions, IsCopyLayout, activiesmodal.ActivitiesRegistrationId);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (var defaultActivitiesId in defaultActivitiesIds)
                            {
                                var activiesmodal = context.ActivitiesRegistration.SingleOrDefault(IC => IC.LayoutRefId == Layout_Id && IC.DefaultActivitiesRefId == defaultActivitiesId);
                                if (activiesmodal != null)
                                {
                                    var deleteresultQuestions = context.Questions.Where(IC => IC.LayoutRefId == Layout_Id && IC.ActivitiesRefId == activiesmodal.ActivitiesRegistrationId).ToList();
                                    if (deleteresultQuestions.Count() > 0)
                                    {
                                        var IsCopyLayout = context.Layout.Where(x => x.LayoutId == Layout_Id).Select(x => x.IsCopyLayout).FirstOrDefault();
                                        _questionPathRepository.DeleteQuestions(deleteresultQuestions, IsCopyLayout, activiesmodal.ActivitiesRegistrationId);
                                    }
                                }
                            }
                        }
                    }

                    return "";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<PlanningSpecificationMaster> GetPlanningSpecificationMaster(int[] PlanningTypeMasterRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var returnList = (from PSM in context.PlanningSpecificationMaster
                                      where PlanningTypeMasterRefId.Contains(PSM.PlanningTypeMasterRefId)
                                      select new PlanningSpecificationMaster()
                                      {
                                          PlanningSpecificationMasterId = PSM.PlanningSpecificationMasterId,
                                          PlanningSpecificationMasterDescription = PSM.PlanningSpecificationMasterDescription,
                                          PlanningTypeMasterRefId = PSM.PlanningTypeMasterRefId,
                                          IS_Active = PSM.IS_Active,
                                          CreatedBy = PSM.CreatedBy,
                                          CreatedOn = PSM.CreatedOn,
                                          UpdatedBy = PSM.UpdatedBy,
                                          UpdatedOn = PSM.UpdatedOn,
                                          CopyColorCode = (isCopyLayout ? GetCopyLayoutColor(PlanningSpecificationIsModification(layoutId, PSM.PlanningSpecificationMasterId, isCopyLayout)) : "Grey")
                                      }).ToList();

                    return returnList;
                }
            }
            catch (Exception ex)
            {
                return new List<PlanningSpecificationMaster>();
            }
        }

        public int PlanningSpecificationIsModification(int layoutId, int planningSpecificationMasterId, bool isCopyLayout)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isModification = (int)EnumCopyColorCode.Green;
                    if (isCopyLayout && context.PlanningSpecification.Where(x => x.LayoutRefId == layoutId && x.PlanningSpecificationRefId == planningSpecificationMasterId).Any())
                        isModification = context.PlanningSpecification.Where(x => x.LayoutRefId == layoutId && x.PlanningSpecificationRefId == planningSpecificationMasterId).Select(x => x.IsModification).FirstOrDefault();
                    else if (isCopyLayout)
                        isModification = (int)EnumCopyColorCode.Empty;

                    return isModification;
                }
            }
            catch (Exception ex)
            {
                return (int)EnumCopyColorCode.Green;
            }
        }

        public PlanningSpecification SavePlanningSpecification(PlanningSpecification PlanningSpecification)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var existingPlanningSpecification = context.PlanningSpecification.FirstOrDefault(a => a.LayoutRefId == PlanningSpecification.LayoutRefId && a.PlanningSpecificationRefId == PlanningSpecification.PlanningSpecificationRefId);
                    if (existingPlanningSpecification == null)
                        context.Add(PlanningSpecification);
                    else
                    {
                        existingPlanningSpecification.IsModification = PlanningSpecification.IsModification;
                        existingPlanningSpecification.IsActive = PlanningSpecification.IsActive;

                        if (!PlanningSpecification.IsActive && PlanningSpecification.IsModification == (int)EnumCopyColorCode.Empty)
                        {
                            context.PlanningSpecification.Remove(existingPlanningSpecification);
                            context.SaveChanges();
                        }

                        var isCopyLayout = context.Layout.Where(x => x.LayoutId == PlanningSpecification.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                        if (isCopyLayout)
                        {
                            var OBCTypeId = context.OBCType.Where(x => x.LayoutRefId == PlanningSpecification.LayoutRefId).Select(x => x.OBCTypeRefId).FirstOrDefault();
                            int defaultActivitiesId = 0;

                            if ((int)EnumOBCType.TX_SKY == OBCTypeId)
                            {
                                if ((int)EnumPlanningSpecification.PauseTrip == PlanningSpecification.PlanningSpecificationRefId)
                                    defaultActivitiesId = (int)EnumDefaultActivities.TX_SKY_Pause_Trip;
                                else if ((int)EnumPlanningSpecification.NextStop == PlanningSpecification.PlanningSpecificationRefId)
                                    defaultActivitiesId = (int)EnumDefaultActivities.TX_SKY_Next_Stop;
                            }
                            else if ((int)EnumOBCType.TX_SKY_FLEX == OBCTypeId)
                            {
                                if ((int)EnumPlanningSpecification.PauseTrip == PlanningSpecification.PlanningSpecificationRefId)
                                    defaultActivitiesId = (int)EnumDefaultActivities.TX_SKY_FLEX_Pause_Trip;
                                else if ((int)EnumPlanningSpecification.NextStop == PlanningSpecification.PlanningSpecificationRefId)
                                    defaultActivitiesId = (int)EnumDefaultActivities.TX_SKY_FLEX_Next_Stop;
                            }
                            else if ((int)EnumOBCType.TX_GO_FLEX == OBCTypeId)
                            {
                                if ((int)EnumPlanningSpecification.PauseTrip == PlanningSpecification.PlanningSpecificationRefId)
                                    defaultActivitiesId = (int)EnumDefaultActivities.TX_GO_FLEX_Pause_Trip;
                            }

                            var activiesmodal = context.ActivitiesRegistration.SingleOrDefault(IC => IC.LayoutRefId == PlanningSpecification.LayoutRefId && IC.DefaultActivitiesRefId == defaultActivitiesId);
                            if (activiesmodal != null && activiesmodal.ISModification == (int)EnumCopyColorCode.Red)
                            {
                                if (existingPlanningSpecification.IsActive && existingPlanningSpecification.IsModification == (int)EnumCopyColorCode.Empty)
                                {
                                    activiesmodal.ISModification = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationActivitiesRegistrationName = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationWorkingCodeRefId = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationPlanningRefId = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationPlanningOverviewRefId = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationSpecificRefId = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationVisible = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationConfirmButton = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationTransportType = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationEmptyFullSolo = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationFlexactivity = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationPTORefId = (int)EnumCopyColorCode.Empty;
                                    activiesmodal.IsModificationInterruptibleByRefId = (int)EnumCopyColorCode.Empty;

                                    context.SaveChanges();
                                }

                                var questionList = context.Questions.Where(x => x.LayoutRefId == PlanningSpecification.LayoutRefId && x.ActivitiesRefId == activiesmodal.ActivitiesRegistrationId).ToList();
                                foreach (var questions in questionList)
                                {
                                    if (existingPlanningSpecification.IsActive && existingPlanningSpecification.IsModification == (int)EnumCopyColorCode.Empty)
                                    {
                                        questions.ISModification = (int)EnumCopyColorCode.Empty;
                                        context.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    context.SaveChanges();

                    return existingPlanningSpecification ?? PlanningSpecification;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PlanningSpecification> ConsolidatedPlanningSpecification(int layoutId, List<string> planningSpecifications)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<PlanningSpecification> planningSpecificationList = new List<PlanningSpecification>();
                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var newPlaningSpecification = planningSpecifications.Select(Int32.Parse).ToList();
                    if (isCopyLayout)
                    {
                        var existPlaningSpecificationList = context.PlanningSpecification.Where(x => x.LayoutRefId == layoutId).Select(x => new { x.PlanningSpecificationRefId, x.IsActive, x.IsModification }).ToList();
                        var existPlaningSpecification = existPlaningSpecificationList.Select(x => x.PlanningSpecificationRefId).ToList();

                        var commonList = existPlaningSpecification.Where(x => newPlaningSpecification.Contains(x)).ToList();
                        var newList = newPlaningSpecification.Where(x => !existPlaningSpecification.Contains(x)).ToList();
                        var removeList = existPlaningSpecification.Where(x => !newPlaningSpecification.Contains(x)).ToList();

                        foreach (var common in commonList)
                        {
                            var isModification = existPlaningSpecificationList.Where(x => x.PlanningSpecificationRefId == common).Select(x => x.IsModification).FirstOrDefault();
                            PlanningSpecification planningSpecification = new PlanningSpecification();
                            planningSpecification.PlanningSpecificationId = common;
                            planningSpecification.IsActive = true;
                            planningSpecification.IsModification = isModification == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : isModification;
                            planningSpecificationList.Add(planningSpecification);
                        }
                        foreach (var newplanning in newList)
                        {
                            PlanningSpecification planningSpecification = new PlanningSpecification();
                            planningSpecification.PlanningSpecificationId = newplanning;
                            planningSpecification.IsActive = true;
                            planningSpecification.IsModification = (int)EnumCopyColorCode.Green;
                            planningSpecificationList.Add(planningSpecification);
                        }
                        foreach (var remove in removeList)
                        {
                            var isModification = existPlaningSpecificationList.Where(x => x.PlanningSpecificationRefId == remove).Select(x => x.IsModification).FirstOrDefault();
                            PlanningSpecification planningSpecification = new PlanningSpecification();
                            planningSpecification.PlanningSpecificationId = remove;
                            planningSpecification.IsActive = false;
                            planningSpecification.IsModification = isModification == (int)EnumCopyColorCode.Green ? (int)EnumCopyColorCode.Empty : (int)EnumCopyColorCode.Red;
                            planningSpecificationList.Add(planningSpecification);
                        }
                    }
                    else
                    {
                        foreach (var newplanning in newPlaningSpecification)
                        {
                            PlanningSpecification planningSpecification = new PlanningSpecification();
                            planningSpecification.PlanningSpecificationId = newplanning;
                            planningSpecification.IsActive = true;
                            planningSpecification.IsModification = (int)EnumCopyColorCode.Green;
                            planningSpecificationList.Add(planningSpecification);
                        }
                    }

                    return planningSpecificationList;
                }
            }
            catch (Exception ex)
            {
                return new List<PlanningSpecification>();
            }
        }
        public string DeletPlanningSpecification(int Layout_Id, int PlanningSpecificationRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isActive = false;

                    var result = context.PlanningSpecification.SingleOrDefault(IC => IC.LayoutRefId == Layout_Id && IC.PlanningSpecificationRefId == PlanningSpecificationRefId);
                    if (result != null)
                    {
                        isActive = result.IsActive;
                        var isCopyLayout = context.Layout.Where(x => x.LayoutId == Layout_Id).Select(x => x.IsCopyLayout).FirstOrDefault();
                        if (!isCopyLayout || (result.IsModification == (int)EnumCopyColorCode.Green))
                        {
                            context.PlanningSpecification.Remove(result);
                            context.SaveChanges();
                        }
                        else
                        {
                            result.IsModification = result.IsModification == (int)EnumCopyColorCode.Empty ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty;
                            result.IsActive = false;
                            context.SaveChanges();
                        }

                        var OBCTypeId = context.OBCType.Where(x => x.LayoutRefId == Layout_Id).Select(x => x.OBCTypeRefId).FirstOrDefault();
                        int defaultActivitiesId = 0;

                        if ((int)EnumOBCType.TX_SKY == OBCTypeId)
                        {
                            if ((int)EnumPlanningSpecification.PauseTrip == PlanningSpecificationRefId)
                                defaultActivitiesId = (int)EnumDefaultActivities.TX_SKY_Pause_Trip;
                            else if ((int)EnumPlanningSpecification.NextStop == PlanningSpecificationRefId)
                                defaultActivitiesId = (int)EnumDefaultActivities.TX_SKY_Next_Stop;
                        }
                        else if ((int)EnumOBCType.TX_SKY_FLEX == OBCTypeId)
                        {
                            if ((int)EnumPlanningSpecification.PauseTrip == PlanningSpecificationRefId)
                                defaultActivitiesId = (int)EnumDefaultActivities.TX_SKY_FLEX_Pause_Trip;
                            else if ((int)EnumPlanningSpecification.NextStop == PlanningSpecificationRefId)
                                defaultActivitiesId = (int)EnumDefaultActivities.TX_SKY_FLEX_Next_Stop;
                        }
                        else if ((int)EnumOBCType.TX_GO_FLEX == OBCTypeId)
                        {
                            if ((int)EnumPlanningSpecification.PauseTrip == PlanningSpecificationRefId)
                                defaultActivitiesId = (int)EnumDefaultActivities.TX_GO_FLEX_Pause_Trip;
                        }

                        if (isCopyLayout)
                        {
                            var activiesmodal = context.ActivitiesRegistration.SingleOrDefault(IC => IC.LayoutRefId == Layout_Id && IC.DefaultActivitiesRefId == defaultActivitiesId);
                            if (activiesmodal != null && (activiesmodal.ISModification == (int)EnumCopyColorCode.Empty || activiesmodal.ISModification == (int)EnumCopyColorCode.Red))
                            {
                                if ((isActive && activiesmodal.ISModification == (int)EnumCopyColorCode.Empty) || (!isActive && activiesmodal.ISModification == (int)EnumCopyColorCode.Red))
                                {
                                    activiesmodal.ISModification = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationActivitiesRegistrationName = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationWorkingCodeRefId = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationPlanningRefId = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationPlanningOverviewRefId = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationSpecificRefId = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationVisible = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationConfirmButton = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationTransportType = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationEmptyFullSolo = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationFlexactivity = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationPTORefId = (int)EnumCopyColorCode.Red;
                                    activiesmodal.IsModificationInterruptibleByRefId = (int)EnumCopyColorCode.Red;
                                }

                                context.SaveChanges();

                                var questionList = context.Questions.Where(x => x.LayoutRefId == Layout_Id && x.ActivitiesRefId == activiesmodal.ActivitiesRegistrationId).ToList();
                                foreach (var questions in questionList)
                                {
                                    if (isActive && questions.ISModification == (int)EnumCopyColorCode.Empty)
                                        questions.ISModification = (int)EnumCopyColorCode.Red;
                                    else if (!isActive && questions.ISModification == (int)EnumCopyColorCode.Red)
                                        questions.ISModification = (int)EnumCopyColorCode.Red;
                                }
                                context.SaveChanges();
                            }
                            else
                            {
                                if (activiesmodal != null)
                                {
                                    var deleteresultQuestions = context.Questions.Where(IC => IC.LayoutRefId == Layout_Id && IC.ActivitiesRefId == activiesmodal.ActivitiesRegistrationId).ToList();
                                    if (deleteresultQuestions.Count() > 0)
                                    {
                                        var IsCopyLayout = context.Layout.Where(x => x.LayoutId == Layout_Id).Select(x => x.IsCopyLayout).FirstOrDefault();
                                        _questionPathRepository.DeleteQuestions(deleteresultQuestions, IsCopyLayout, activiesmodal.ActivitiesRegistrationId);
                                    }
                                }
                            }
                        }
                        else
                        {
                            var activiesmodal = context.ActivitiesRegistration.SingleOrDefault(IC => IC.LayoutRefId == Layout_Id && IC.DefaultActivitiesRefId == defaultActivitiesId);
                            if (activiesmodal != null)
                            {
                                var deleteresultQuestions = context.Questions.Where(IC => IC.LayoutRefId == Layout_Id && IC.ActivitiesRefId == activiesmodal.ActivitiesRegistrationId).ToList();
                                if (deleteresultQuestions.Count() > 0)
                                {
                                    var IsCopyLayout = context.Layout.Where(x => x.LayoutId == Layout_Id).Select(x => x.IsCopyLayout).FirstOrDefault();
                                    _questionPathRepository.DeleteQuestions(deleteresultQuestions, IsCopyLayout, activiesmodal.ActivitiesRegistrationId);
                                }
                            }
                        }
                    }
                    return "Deleted";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<PlanningSpecification> GetPlanningSpecification(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var returnList = (from PS in context.PlanningSpecification
                                      where PS.LayoutRefId == layoutId
                                      select new PlanningSpecification()
                                      {
                                          PlanningSpecificationId = PS.PlanningSpecificationId,
                                          CustomerRefId = PS.CustomerRefId,
                                          LayoutRefId = PS.LayoutRefId,
                                          PlanningSpecificationRefId = PS.PlanningSpecificationRefId,
                                          CreatedBy = PS.CreatedBy,
                                          CreatedOn = PS.CreatedOn,
                                          UpdatedBy = PS.UpdatedBy,
                                          UpdatedOn = PS.UpdatedOn,
                                          IsActive = PS.IsActive
                                      }).Where(x => x.IsActive).ToList();

                    return returnList;
                }

            }
            catch (Exception ex)
            {
                return new List<PlanningSpecification>();
            }
        }
        public List<DocumentTypeMaster> GetDocumentType(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var documentTypeIds = context.DocumentTypeFlex.Where(x => x.LayoutRefId == layoutId).Select(x => x.DocumentTypeRefId).ToList();
                    var returnresult = context.DocumentTypeMaster.Where(x => x.IsActive == 1 && (documentTypeIds.Count() > 0 ? (!documentTypeIds.Contains(x.DocumentTypeId)) : (1 == 1)))
                        .Select(x => new DocumentTypeMaster()
                        {
                            DocumentTypeId = x.DocumentTypeId,
                            Name = x.DocumentTypeCode + " (" + x.DocumentTypeDescription + ")"
                        }).ToList();
                    return returnresult;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<DocumentTypeFlex> GetDocumentTypeFlex(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var returnresult = context.DocumentTypeFlex.Where(x => x.LayoutRefId == layoutId && (isCopyLayout ? (1 == 1) : x.IsActive))
                        .Select(x => new DocumentTypeFlex()
                        {
                            DocumentTypeFlexId = x.DocumentTypeFlexId,
                            Name = x.DocumentTypeMaster.DocumentTypeCode + " (" + x.DocumentTypeMaster.DocumentTypeDescription + ")",
                            CopyColorCode = isCopyLayout ? GetCopyLayoutColor(x.IsModification) : "Grey",
                            IsModification = x.IsModification
                        }).ToList();
                    return returnresult;
                }
            }
            catch (Exception ex)
            {
                return new List<DocumentTypeFlex>();
            }
        }
        public DocumentTypeFlex SaveDocumentTypeFlex(DocumentTypeFlex documenttypeflexmodal)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    context.Add(documenttypeflexmodal);
                    context.SaveChanges();
                    return documenttypeflexmodal;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteDocumentTypeFlex(int DocumentTypeFlexId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var target = context.DocumentTypeFlex.Where(x => x.DocumentTypeFlexId == DocumentTypeFlexId).FirstOrDefault();
                    if (target != null)
                    {
                        var isCopyLayout = context.Layout.Where(x => x.LayoutId == target.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                        if (isCopyLayout && target.IsModification == (int)EnumCopyColorCode.Empty)
                        {
                            target.IsModification = (int)EnumCopyColorCode.Red;
                            target.IsActive = false;
                        }
                        else if (isCopyLayout)
                        {
                            context.DocumentTypeFlex.Remove(target);
                        }
                        else
                        {
                            target.IsActive = false;
                        }
                        context.SaveChanges();
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string RevertDocumentTypeFlex(int DocumentTypeFlexId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var target = context.DocumentTypeFlex.Where(x => x.DocumentTypeFlexId == DocumentTypeFlexId).FirstOrDefault();
                    if (target != null)
                    {
                        var isCopyLayout = context.Layout.Where(x => x.LayoutId == target.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();
                        if (isCopyLayout && target.IsModification == (int)EnumCopyColorCode.Red)
                            target.IsModification = (int)EnumCopyColorCode.Empty;

                        target.IsActive = true;
                        context.SaveChanges();
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<AllowancesMaster> GetAllowancesMaster(int OBCTypeRefId, int CustomerId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var allowanceList = context.AllowancesMaster.Where(AM => AM.IS_Active == 1 && ((AM.OBCTypeRefId == OBCTypeRefId && AM.CustomerRefId == null) || (AM.OBCTypeRefId == OBCTypeRefId && AM.CustomerRefId == CustomerId))).Select(AM =>
                    new AllowancesMaster()
                    {
                        AllowancesMasterId = AM.AllowancesMasterId,
                        AllowancesMasterDescription = AM.AllowancesMasterDescription,
                        AllowancesMasterName = AM.AllowancesMasterDescription + " (" + AM.AllowancesCode + ")",
                        AllowancesCode = AM.AllowancesCode,
                        OBCTypeRefId = AM.OBCTypeRefId,
                        IS_Active = AM.IS_Active,
                        CreatedBy = AM.CreatedBy,
                        CreatedOn = AM.CreatedOn,
                        UpdatedBy = AM.UpdatedBy,
                        UpdatedOn = AM.UpdatedOn,
                        CustomerRefId = AM.CustomerRefId ?? 0,
                        CopyColorCode = (isCopyLayout ? GetCopyLayoutColor(AllowancesIsModification(layoutId, AM.AllowancesMasterId, isCopyLayout)) : "Grey")
                    }).OrderBy(x => x.AllowancesMasterName).ToList();

                    return allowanceList;
                }
            }
            catch (Exception ex)
            {
                return new List<AllowancesMaster>();
            }
        }
        public int AllowancesIsModification(int layoutId, int allowanceMasterId, bool isCopyLayout)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var isModification = (int)EnumCopyColorCode.Green;
                    if (isCopyLayout && context.Allowances.Where(x => x.LayoutRefId == layoutId && x.AllowancesRefId == allowanceMasterId).Any())
                        isModification = context.Allowances.Where(x => x.LayoutRefId == layoutId && x.AllowancesRefId == allowanceMasterId).Select(x => x.IsModification).FirstOrDefault();
                    else if (isCopyLayout)
                        isModification = (int)EnumCopyColorCode.Empty;

                    return isModification;
                }
            }
            catch (Exception ex)
            {
                return (int)EnumCopyColorCode.Green;
            }
        }
        public AllowancesMaster SaveAllowancesMaster(AllowancesMaster AllowancesMaster)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (AllowancesMaster.AllowancesMasterId > 0)
                    {
                        var target = context.AllowancesMaster.Where(x => x.AllowancesMasterId == AllowancesMaster.AllowancesMasterId).FirstOrDefault();
                        target.AllowancesMasterDescription = AllowancesMaster.AllowancesMasterDescription;
                        target.AllowancesCode = AllowancesMaster.AllowancesCode;
                        target.UpdatedBy = AllowancesMaster.UpdatedBy;
                        target.UpdatedOn = AllowancesMaster.UpdatedOn;

                        context.SaveChanges();
                    }
                    else
                    {
                        context.Add(AllowancesMaster);
                        context.SaveChanges();
                    }

                    return AllowancesMaster;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<Allowances> GetAllowances(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    int allowancesId = 0;
                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", layoutId);
                    SqlParameter allowancesParam = new SqlParameter("@AllowancesId", allowancesId);

                    var bOAllawancesList = context.BOAllowances.FromSql("GetAllowances @LayoutRefId,@AllowancesId", layoutParam, allowancesParam).ToList();

                    var allowanceList = bOAllawancesList.Select(x => new Allowances()
                    {
                        AllowancesId = x.AllowancesId,
                        CustomerRefId = x.CustomerRefId,
                        LayoutRefId = x.LayoutRefId,
                        AllowancesRefId = x.AllowancesRefId,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        UpdatedBy = x.UpdatedBy,
                        UpdatedOn = x.UpdatedOn,
                        IsQPUsed = x.IsQPUsed,
                        AllowancesMasterDescription = x.AllowancesMasterDescription,
                        AllowancesCode = x.AllowancesCode,
                        IsModification = x.IsModification,
                        IsActive = x.IsActive
                    }).Where(x => x.IsActive).ToList();

                    List<Allowances> returnList = allowanceList;
                    return returnList;
                }
            }
            catch (Exception ex)
            {
                return new List<Allowances>();
            }
        }
        public Allowances SaveAllowances(Allowances allowances)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var existingallowance = context.Allowances.FirstOrDefault(a => a.LayoutRefId == allowances.LayoutRefId && a.AllowancesRefId == allowances.AllowancesRefId);
                    if (existingallowance == null)
                    {
                        context.Add(allowances);
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */
                        var AllowancesMasterDescription = context.AllowancesMaster.Where(x => x.AllowancesMasterId == allowances.AllowancesRefId).Select(x => x.AllowancesMasterDescription).FirstOrDefault();
                        var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == allowances.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = allowances.AllowancesId;
                        langrefdetail.QuestionRefId = null;
                        langrefdetail.LayoutRefId = allowances.LayoutRefId;
                        langrefdetail.CustomerRefId = allowances.CustomerRefId;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.Allowances_AllowancesMasterDescription;
                        langrefdetail.LangText = AllowancesMasterDescription ?? "";
                        langrefdetail.Language = LanguageName;
                        langrefdetail.CreatedBy = allowances.CreatedBy;
                        langrefdetail.UpdatedBy = allowances.CreatedBy;

                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */

                        return allowances;
                    }
                    else
                    {
                        existingallowance.IsModification = allowances.IsModification;
                        existingallowance.IsActive = allowances.IsActive;

                        if (!allowances.IsActive && allowances.IsModification == (int)EnumCopyColorCode.Empty)
                        {
                            context.Allowances.Remove(existingallowance);
                        }

                        context.SaveChanges();
                        return existingallowance;
                    }
                }
            }
            catch (Exception ex)
            {
                return new Allowances();
            }
        }
        public string DeletAllowances(int Layout_Id, int AllowancesRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Allowances.SingleOrDefault(IC => IC.LayoutRefId == Layout_Id && IC.AllowancesRefId == AllowancesRefId);
                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == Layout_Id).Select(x => x.IsCopyLayout).FirstOrDefault();
                    if (!isCopyLayout || (result.IsModification == (int)EnumCopyColorCode.Green))
                    {
                        /* Written By Selvam Thangaraj - Go */

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.AllowancesId;
                        langrefdetail.LayoutRefId = result.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.Allowances_AllowancesMasterDescription;

                        _translatorRepository.DeleteLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */

                        context.Allowances.Remove(result);
                        context.SaveChanges();
                    }
                    else
                    {
                        result.IsModification = result.IsModification == (int)EnumCopyColorCode.Empty ? (int)EnumCopyColorCode.Red : (int)EnumCopyColorCode.Empty;
                        result.IsActive = false;
                        context.SaveChanges();
                    }

                    return "Deleted";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<Allowances> ConsolidatedAllowances(int layoutId, List<string> allowancess)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<Allowances> allowancesList = new List<Allowances>();
                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                    var newallowances = allowancess.Select(Int32.Parse).ToList();
                    if (isCopyLayout)
                    {
                        var existallowancesList = context.Allowances.Where(x => x.LayoutRefId == layoutId).Select(x => new { x.AllowancesRefId, x.IsActive, x.IsModification }).ToList();
                        var existallowances = existallowancesList.Select(x => x.AllowancesRefId).ToList();

                        var commonList = existallowances.Where(x => newallowances.Contains(x)).ToList();
                        var newList = newallowances.Where(x => !existallowances.Contains(x)).ToList();
                        var removeList = existallowances.Where(x => !newallowances.Contains(x)).ToList();

                        foreach (var common in commonList)
                        {
                            var isModification = existallowancesList.Where(x => x.AllowancesRefId == common).Select(x => x.IsModification).FirstOrDefault();
                            Allowances allowances = new Allowances();
                            allowances.AllowancesRefId = common;
                            allowances.IsActive = true;
                            allowances.IsModification = isModification == (int)EnumCopyColorCode.Red ? (int)EnumCopyColorCode.Empty : isModification;
                            allowancesList.Add(allowances);
                        }
                        foreach (var newplanning in newList)
                        {
                            Allowances allowances = new Allowances();
                            allowances.AllowancesRefId = newplanning;
                            allowances.IsActive = true;
                            allowances.IsModification = (int)EnumCopyColorCode.Green;
                            allowancesList.Add(allowances);
                        }
                        foreach (var remove in removeList)
                        {
                            var isModification = existallowancesList.Where(x => x.AllowancesRefId == remove).Select(x => x.IsModification).FirstOrDefault();
                            Allowances allowances = new Allowances();
                            allowances.AllowancesRefId = remove;
                            allowances.IsActive = false;
                            allowances.IsModification = isModification == (int)EnumCopyColorCode.Green ? (int)EnumCopyColorCode.Empty : (int)EnumCopyColorCode.Red;
                            allowancesList.Add(allowances);
                        }
                    }
                    else
                    {
                        foreach (var newplanning in newallowances)
                        {
                            Allowances allowances = new Allowances();
                            allowances.AllowancesRefId = newplanning;
                            allowances.IsActive = true;
                            allowances.IsModification = (int)EnumCopyColorCode.Green;
                            allowancesList.Add(allowances);
                        }
                    }

                    return allowancesList;
                }
            }
            catch (Exception ex)
            {
                return new List<Allowances>();
            }
        }
        public List<WorkingCode> GetWorkingCode()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<WorkingCode> returnList;
                    return returnList = context.WorkingCode.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<Planning> GetPlanning()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<Planning> returnList;
                    return returnList = context.Planning.OrderBy(x => x.DefaultActivitiesRegistrationDescription).ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ActivitiesRegistration> GetPlanningOverview(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<ActivitiesRegistration> returnList;
                    returnList = context.ActivitiesRegistration.Where(x => x.LayoutRefId == LayoutId && x.PlanningRefId == "Receive").ToList();

                    var defualtvalue = new ActivitiesRegistration() { ActivitiesRegistrationName = "None", ActivitiesRegistrationId = 0 };
                    returnList.Add(defualtvalue);

                    return returnList;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<Specific> GetSpecific(int DefaultActivitiesId, bool IsPauseTrip, int OBCTypeId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var specificList = context.Specific.Where(x => (DefaultActivitiesId == 0 ? (x.DefaultActivitiesRefId == null && x.IsPauseTrip == false) : (x.DefaultActivitiesRefId == DefaultActivitiesId)) || (IsPauseTrip == true ? (x.IsPauseTrip == IsPauseTrip && (DefaultActivitiesId == 6 || DefaultActivitiesId == 13 || DefaultActivitiesId == 15 || DefaultActivitiesId == 18)) : (1 != 1)))
                         .Select(x => new Specific() { SpecificDescription = x.SpecificDescription, SpecificId = x.SpecificId }).ToList();

                    if (DefaultActivitiesId == 0)
                    {
                        var TemplateLayoutId = _translatorRepository.GetTemplateLayoutId(questionpathtemplateConfiguration.CustomerId, OBCTypeId);
                        var templatesList = context.Questions.Where(x => x.LayoutRefId == TemplateLayoutId && x.ActivitiesRegistration.DefaultActivitiesRefId == null).Select(x => new Specific() { SpecificDescription = x.ActivitiesRegistration.ActivitiesRegistrationName, SpecificId = x.QuestionId }).Distinct().ToList();
                        if (templatesList.Count() > 0)
                            specificList.AddRange(templatesList);
                    }

                    return specificList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PTO> GetPTO()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<PTO> returnList;
                    return returnList = context.PTO.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<InterruptibleBy> GetInterruptibleBy()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<InterruptibleBy> returnList;
                    return returnList = context.InterruptibleBy.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ActivitiesRegistration> GetActivitiesRegistration(int layoutId, string LanguageName)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var activityId = 0;
                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", layoutId);
                    SqlParameter activityParam = new SqlParameter("@ActivityRefId", activityId);

                    var activitiesRegistrationTranslationDetails = context.ActivitiesRegistrationTranslation.FromSql("GetActivitiesRegistration @LayoutRefId,@ActivityRefId", layoutParam, activityParam).ToList();
                    List<ActivitiesRegistration> activitiesRegisrationList = new List<ActivitiesRegistration>();

                    foreach (var actRegInfo in activitiesRegistrationTranslationDetails)
                    {
                        ActivitiesRegistration actReg = new ActivitiesRegistration();

                        actReg.ActivitiesRegistrationId = actRegInfo.ActivitiesRegistrationId;
                        actReg.CustomerRefId = actRegInfo.CustomerRefId;
                        actReg.LayoutRefId = actRegInfo.LayoutRefId;
                        actReg.ActivitiesRegistrationName = GetActivitiesRegistrationName(actRegInfo, LanguageName);
                        actReg.WorkingCodeRefId = actRegInfo.WorkingCodeRefId;
                        actReg.PlanningRefId = actRegInfo.PlanningRefId;
                        actReg.PlanningOverviewRefId = actRegInfo.PlanningOverviewRefId;
                        actReg.SpecificRefId = actRegInfo.SpecificRefId;
                        actReg.Visible = Convert.ToBoolean(actRegInfo.Visible);
                        actReg.ConfirmButton = Convert.ToBoolean(actRegInfo.ConfirmButton);
                        actReg.PTORefId = actRegInfo.PTORefId;
                        actReg.InterruptibleByRefId = actRegInfo.InterruptibleByRefId;
                        actReg.TransportType = Convert.ToBoolean(actRegInfo.TransportType);
                        actReg.EmptyFullSolo = Convert.ToBoolean(actRegInfo.EmptyFullSolo);
                        actReg.Flexactivity = Convert.ToBoolean(actRegInfo.Flexactivity);
                        actReg.ISModification = actRegInfo.ISModification;
                        actReg.CreatedBy = actRegInfo.CreatedBy;
                        actReg.CreatedOn = actRegInfo.CreatedOn;
                        actReg.UpdatedBy = actRegInfo.UpdatedBy;
                        actReg.UpdatedOn = actRegInfo.UpdatedOn;
                        actReg.DefaultActivitiesRefId = actRegInfo.DefaultActivitiesRefId ?? 0;
                        actReg.IsCopyLayout = actRegInfo.IsCopyLayout;
                        actReg.IsModificationActivitiesRegistrationName = actRegInfo.IsModificationActivitiesRegistrationName;
                        actReg.IsModificationWorkingCodeRefId = actRegInfo.IsModificationWorkingCodeRefId;
                        actReg.IsModificationPlanningRefId = actRegInfo.IsModificationPlanningRefId;
                        actReg.IsModificationPlanningOverviewRefId = actRegInfo.IsModificationPlanningOverviewRefId;
                        actReg.IsModificationSpecificRefId = actRegInfo.IsModificationSpecificRefId;
                        actReg.IsModificationVisible = actRegInfo.IsModificationVisible;
                        actReg.IsModificationConfirmButton = actRegInfo.IsModificationConfirmButton;
                        actReg.IsModificationTransportType = actRegInfo.IsModificationTransportType;
                        actReg.IsModificationEmptyFullSolo = actRegInfo.IsModificationEmptyFullSolo;
                        actReg.IsModificationFlexactivity = actRegInfo.IsModificationFlexactivity;
                        actReg.IsModificationPTORefId = actRegInfo.IsModificationPTORefId;
                        actReg.IsModificationInterruptibleByRefId = actRegInfo.IsModificationInterruptibleByRefId;

                        activitiesRegisrationList.Add(actReg);
                    }

                    return activitiesRegisrationList.OrderByDescending(x => x.CreatedOn).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ActivitiesRegistration>();
            }
        }
        public string GetActivitiesRegistrationName(ActivitiesRegistrationTranslation activitiesRegisrationTranslation, string defaultLanguage)
        {
            var actRegName = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                actRegName = activitiesRegisrationTranslation.Belarusian;
            }
            else
            {
                actRegName = "";
            }

            if (actRegName == null || actRegName == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == activitiesRegisrationTranslation.LanguageName)
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.Belarusian;
                    }
                    else
                    {
                        actRegName = "@" + activitiesRegisrationTranslation.English;
                    }
                }
            }
            return actRegName;
        }
        public ActivitiesRegistration SaveActivitiesRegistration(ActivitiesRegistration ActivitiesRegistration)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var LanguageInfo = context.UsedLanguages.Where(x => x.LayoutRefId == ActivitiesRegistration.LayoutRefId && x.IsDefault == 1).Select(x => new { x.LanguageMaster.LanguageDescription, x.LanguageRefId }).FirstOrDefault();
                    var ActAdded = false;


                    var result = context.ActivitiesRegistration.SingleOrDefault(IC => IC.LayoutRefId == ActivitiesRegistration.LayoutRefId && IC.ActivitiesRegistrationName == ActivitiesRegistration.ActivitiesRegistrationName);
                    if (result == null)
                    {
                        ActivitiesRegistration.DefaultLanguageId = LanguageInfo.LanguageRefId;

                        ActivitiesRegistration.IsModificationActivitiesRegistrationName = (int)EnumCopyColorCode.Green;
                        ActivitiesRegistration.IsModificationWorkingCodeRefId = (int)EnumCopyColorCode.Green;
                        ActivitiesRegistration.IsModificationPlanningRefId = (int)EnumCopyColorCode.Green;
                        ActivitiesRegistration.IsModificationPlanningOverviewRefId = (int)EnumCopyColorCode.Green;
                        ActivitiesRegistration.IsModificationSpecificRefId = (int)EnumCopyColorCode.Green;
                        ActivitiesRegistration.IsModificationVisible = (int)EnumCopyColorCode.Green;
                        ActivitiesRegistration.IsModificationConfirmButton = (int)EnumCopyColorCode.Green;
                        ActivitiesRegistration.IsModificationTransportType = (int)EnumCopyColorCode.Green;
                        ActivitiesRegistration.IsModificationEmptyFullSolo = (int)EnumCopyColorCode.Green;
                        ActivitiesRegistration.IsModificationFlexactivity = (int)EnumCopyColorCode.Green;
                        ActivitiesRegistration.IsModificationPTORefId = (int)EnumCopyColorCode.Green;
                        ActivitiesRegistration.IsModificationInterruptibleByRefId = (int)EnumCopyColorCode.Green;

                        context.ActivitiesRegistration.Add(ActivitiesRegistration);
                        context.SaveChanges();
                        ActAdded = true;
                    }

                    var OBCTypeId = context.OBCType.Where(x => x.LayoutRefId == ActivitiesRegistration.LayoutRefId).Select(x => x.OBCTypeRefId).FirstOrDefault();
                    var TemplateLayoutId = _translatorRepository.GetTemplateLayoutId(questionpathtemplateConfiguration.CustomerId, OBCTypeId);

                    var tamplateActRegId = context.Questions.Where(x => x.LayoutRefId == TemplateLayoutId && x.ActivitiesRegistration.ActivitiesRegistrationName == ActivitiesRegistration.SpecificRefId).Select(x => x.ActivitiesRefId).FirstOrDefault();
                    var transportTypeList = context.TransportTypes.Where(x => x.LayoutRefId == ActivitiesRegistration.LayoutRefId && x.IsActive).Select(x => x.TransportTypeId).OrderBy(x => x).ToList();
                    if (tamplateActRegId > 0)
                    {
                        if (ActivitiesRegistration.TransportType)
                            _copyLayoutRepository.SaveQuestionPath(TemplateLayoutId, tamplateActRegId, 0, new List<int>(), ActivitiesRegistration.LayoutRefId, ActivitiesRegistration.ActivitiesRegistrationId, 0, 0, ActivitiesRegistration.CreatedBy, "TC");
                        else
                        {
                            foreach (var transporttype in transportTypeList)
                                _copyLayoutRepository.SaveQuestionPath(TemplateLayoutId, tamplateActRegId, 0, new List<int>(), ActivitiesRegistration.LayoutRefId, ActivitiesRegistration.ActivitiesRegistrationId, transporttype, 0, ActivitiesRegistration.CreatedBy, "TC");
                        }
                    }
                    else
                    {
                        if (!ActivitiesRegistration.TransportType)
                        {
                            Questions objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = ActivitiesRegistration.ActivitiesRegistrationId;
                            objQuestion.CreatedBy = ActivitiesRegistration.CreatedBy;
                            objQuestion.CreatedOn = DateTime.Now;
                            objQuestion.LayoutRefId = ActivitiesRegistration.LayoutRefId;
                            objQuestion.Name = ActivitiesRegistration.ActivitiesRegistrationName;
                            objQuestion.ParentId = 0;
                            objQuestion.TransporttypeRefId = 0;
                            objQuestion.PropertyName = ActivitiesRegistration.ActivitiesRegistrationName;
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = true;
                            objQuestion.IsDefault = false;
                            objQuestion.IsChild = true;
                            _questionPathRepository.SaveQuestions(objQuestion);
                        }
                        else
                        {
                            foreach (var transporttype in transportTypeList)
                            {
                                Questions objQuestion = new Questions();
                                objQuestion.ActivitiesRefId = ActivitiesRegistration.ActivitiesRegistrationId;
                                objQuestion.CreatedBy = ActivitiesRegistration.CreatedBy;
                                objQuestion.CreatedOn = DateTime.Now;
                                objQuestion.LayoutRefId = ActivitiesRegistration.LayoutRefId;
                                objQuestion.Name = ActivitiesRegistration.ActivitiesRegistrationName;
                                objQuestion.ParentId = 0;
                                objQuestion.TransporttypeRefId = transporttype;
                                objQuestion.PropertyName = ActivitiesRegistration.ActivitiesRegistrationName;
                                objQuestion.ISModification = 0;
                                objQuestion.IsQuestionPath = true;
                                _questionPathRepository.SaveQuestions(objQuestion);
                            }
                        }
                    }

                    if (ActAdded)
                    {
                        /* Written By Selvam Thangaraj - Go */

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = ActivitiesRegistration.ActivitiesRegistrationId;
                        langrefdetail.QuestionRefId = null;
                        langrefdetail.LayoutRefId = ActivitiesRegistration.LayoutRefId;
                        langrefdetail.CustomerRefId = ActivitiesRegistration.CustomerRefId;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ActivitiesRegistration_ActivitiesRegistrationName;
                        langrefdetail.LangText = ActivitiesRegistration.ActivitiesRegistrationName;
                        langrefdetail.Language = LanguageInfo.LanguageDescription;
                        langrefdetail.CreatedBy = ActivitiesRegistration.CreatedBy;
                        langrefdetail.UpdatedBy = ActivitiesRegistration.CreatedBy;

                        _translatorRepository.SaveLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */
                    }

                    return ActivitiesRegistration;
                }
            }
            catch (Exception ex)
            {
                return new ActivitiesRegistration();
            }
        }
        public ActivitiesRegistration UpdateActivitiesRegistration(ActivitiesRegistration ActivitiesRegistration)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var existActReg = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == ActivitiesRegistration.ActivitiesRegistrationId).FirstOrDefault() ?? new ActivitiesRegistration();
                    var activitiesRegistrationName = existActReg.ActivitiesRegistrationName;
                    var transportType = existActReg.TransportType;
                    if (existActReg != null)
                    {
                        var isCopyLayout = context.Layout.Where(x => x.LayoutId == ActivitiesRegistration.LayoutRefId).Select(x => x.IsCopyLayout).FirstOrDefault();

                        if(isCopyLayout)
                        {
                            if (existActReg.ActivitiesRegistrationName != ActivitiesRegistration.ActivitiesRegistrationName && existActReg.IsModificationActivitiesRegistrationName != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationActivitiesRegistrationName = (int)EnumCopyColorCode.Yellow;
                            
                            if (existActReg.WorkingCodeRefId != ActivitiesRegistration.WorkingCodeRefId && existActReg.IsModificationWorkingCodeRefId != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationWorkingCodeRefId = (int)EnumCopyColorCode.Yellow;
                            
                            if (existActReg.PlanningRefId != ActivitiesRegistration.PlanningRefId && existActReg.IsModificationPlanningRefId != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationPlanningRefId = (int)EnumCopyColorCode.Yellow;
                            
                            if (existActReg.PlanningOverviewRefId != ActivitiesRegistration.PlanningOverviewRefId && existActReg.IsModificationPlanningOverviewRefId != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationPlanningOverviewRefId = (int)EnumCopyColorCode.Yellow;
                            
                            if (existActReg.SpecificRefId != ActivitiesRegistration.SpecificRefId && existActReg.IsModificationSpecificRefId != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationSpecificRefId = (int)EnumCopyColorCode.Yellow;
                            
                            if (existActReg.Visible != ActivitiesRegistration.Visible && existActReg.IsModificationVisible != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationVisible = (int)EnumCopyColorCode.Yellow;
                            
                            if (existActReg.ConfirmButton != ActivitiesRegistration.ConfirmButton && existActReg.IsModificationConfirmButton != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationConfirmButton = (int)EnumCopyColorCode.Yellow;
                            
                            if (existActReg.TransportType != ActivitiesRegistration.TransportType && existActReg.IsModificationTransportType != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationTransportType = (int)EnumCopyColorCode.Yellow;
                            
                            if (existActReg.EmptyFullSolo != ActivitiesRegistration.EmptyFullSolo && existActReg.IsModificationEmptyFullSolo != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationEmptyFullSolo = (int)EnumCopyColorCode.Yellow;
                            
                            if (existActReg.Flexactivity != ActivitiesRegistration.Flexactivity && existActReg.IsModificationFlexactivity != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationFlexactivity = (int)EnumCopyColorCode.Yellow;
                            
                            if (existActReg.PTORefId != ActivitiesRegistration.PTORefId && existActReg.IsModificationPTORefId != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationPTORefId = (int)EnumCopyColorCode.Yellow;
                            
                            if (existActReg.InterruptibleByRefId != ActivitiesRegistration.InterruptibleByRefId && existActReg.IsModificationInterruptibleByRefId != (int)EnumCopyColorCode.Green)
                                existActReg.IsModificationInterruptibleByRefId = (int)EnumCopyColorCode.Yellow;
                        }
                        else
                        {
                            existActReg.IsModificationActivitiesRegistrationName = (int)EnumCopyColorCode.Green;
                            existActReg.IsModificationWorkingCodeRefId = (int)EnumCopyColorCode.Green;
                            existActReg.IsModificationPlanningRefId = (int)EnumCopyColorCode.Green;
                            existActReg.IsModificationPlanningOverviewRefId = (int)EnumCopyColorCode.Green;
                            existActReg.IsModificationSpecificRefId = (int)EnumCopyColorCode.Green;
                            existActReg.IsModificationVisible = (int)EnumCopyColorCode.Green;
                            existActReg.IsModificationConfirmButton = (int)EnumCopyColorCode.Green;
                            existActReg.IsModificationTransportType = (int)EnumCopyColorCode.Green;
                            existActReg.IsModificationEmptyFullSolo = (int)EnumCopyColorCode.Green;
                            existActReg.IsModificationFlexactivity = (int)EnumCopyColorCode.Green;
                            existActReg.IsModificationPTORefId = (int)EnumCopyColorCode.Green;
                            existActReg.IsModificationInterruptibleByRefId = (int)EnumCopyColorCode.Green;
                        }

                        existActReg.CustomerRefId = ActivitiesRegistration.CustomerRefId;
                        existActReg.LayoutRefId = ActivitiesRegistration.LayoutRefId;
                        existActReg.ActivitiesRegistrationName = ActivitiesRegistration.ActivitiesRegistrationName;
                        existActReg.WorkingCodeRefId = ActivitiesRegistration.WorkingCodeRefId;
                        existActReg.PlanningRefId = ActivitiesRegistration.PlanningRefId;
                        existActReg.PlanningOverviewRefId = ActivitiesRegistration.PlanningOverviewRefId;
                        existActReg.SpecificRefId = ActivitiesRegistration.SpecificRefId;
                        existActReg.Visible = ActivitiesRegistration.Visible;
                        existActReg.ConfirmButton = ActivitiesRegistration.ConfirmButton;
                        existActReg.PTORefId = ActivitiesRegistration.PTORefId;
                        existActReg.InterruptibleByRefId = ActivitiesRegistration.InterruptibleByRefId;
                        existActReg.TransportType = ActivitiesRegistration.TransportType;
                        existActReg.EmptyFullSolo = ActivitiesRegistration.EmptyFullSolo;
                        existActReg.Flexactivity = ActivitiesRegistration.Flexactivity;
                        existActReg.ISModification = ActivitiesRegistration.ISModification;
                        existActReg.UpdatedBy = ActivitiesRegistration.UpdatedBy;
                        existActReg.UpdatedOn = ActivitiesRegistration.UpdatedOn;


                        context.Update(existActReg);
                        context.SaveChanges();
                        if(activitiesRegistrationName.ToLower() != ActivitiesRegistration.ActivitiesRegistrationName.ToLower())
                        {
                            var planningOverviewList = context.ActivitiesRegistration.Where(x => x.LayoutRefId == ActivitiesRegistration.LayoutRefId && x.PlanningOverviewRefId.ToLower() == activitiesRegistrationName.ToLower()).ToList();
                            foreach (var planningOverview in planningOverviewList)
                            {
                                planningOverview.PlanningOverviewRefId = ActivitiesRegistration.ActivitiesRegistrationName;
                                context.SaveChanges();
                            }
                        }

                        var transportTypeList = context.TransportTypes.Where(x => x.LayoutRefId == ActivitiesRegistration.LayoutRefId && x.IsActive).Select(x => x.TransportTypeId).OrderBy(x => x).ToList();
                        if (!transportType && ActivitiesRegistration.TransportType)
                        {
                            int loopcount = 0;
                            foreach (var transporttype in transportTypeList)
                            {
                                if (loopcount == 0)
                                {
                                    var questionsPathList = context.Questions.Where(x => x.ActivitiesRefId == ActivitiesRegistration.ActivitiesRegistrationId && x.LayoutRefId == ActivitiesRegistration.LayoutRefId).ToList();

                                    foreach (var updatetransporttype in questionsPathList)
                                    {
                                        updatetransporttype.TransporttypeRefId = transporttype;
                                        context.SaveChanges();
                                    }
                                    loopcount++;
                                }
                                else
                                {
                                    Questions objQuestion = new Questions();
                                    objQuestion.ActivitiesRefId = ActivitiesRegistration.ActivitiesRegistrationId;
                                    objQuestion.CreatedBy = ActivitiesRegistration.CreatedBy;
                                    objQuestion.CreatedOn = DateTime.Now;
                                    objQuestion.LayoutRefId = ActivitiesRegistration.LayoutRefId;
                                    objQuestion.Name = ActivitiesRegistration.ActivitiesRegistrationName;
                                    objQuestion.ParentId = 0;
                                    objQuestion.TransporttypeRefId = transporttype;
                                    objQuestion.PropertyName = ActivitiesRegistration.ActivitiesRegistrationName;
                                    objQuestion.ISModification = 0;
                                    objQuestion.IsQuestionPath = true;
                                    _questionPathRepository.SaveQuestions(objQuestion);
                                }
                            }
                        }
                        else if (transportType  && !ActivitiesRegistration.TransportType)
                        {
                            int loopcount = 0;
                            foreach (var transporttype in transportTypeList)
                            {
                                if (loopcount == 0)
                                {
                                    var questionsPathList = context.Questions.Where(x => x.ActivitiesRefId == ActivitiesRegistration.ActivitiesRegistrationId && x.LayoutRefId == ActivitiesRegistration.LayoutRefId && x.TransporttypeRefId == transporttype).ToList();

                                    foreach (var updatetransporttype in questionsPathList)
                                    {
                                        updatetransporttype.TransporttypeRefId = 0;
                                        context.SaveChanges();
                                    }
                                    loopcount++;
                                }
                                else
                                {
                                    var targetQuestionsPath = context.Questions.Where(x => x.ActivitiesRefId == ActivitiesRegistration.ActivitiesRegistrationId && x.LayoutRefId == ActivitiesRegistration.LayoutRefId && x.TransporttypeRefId == transporttype).Select(x => x.QuestionId).OrderBy(x => x).ToList();
                                    if (targetQuestionsPath.Count() > 0)
                                    {
                                        foreach (var QuestionID in targetQuestionsPath)
                                        {
                                            _questionPathRepository.DeleteSpecificQuestionTree(QuestionID);
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        /* Written By Selvam Thangaraj - Go */

                        var LanguageName = context.UsedLanguages.Where(x => x.LayoutRefId == ActivitiesRegistration.LayoutRefId && x.IsDefault == 1).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = ActivitiesRegistration.ActivitiesRegistrationId;
                        langrefdetail.QuestionRefId = null;
                        langrefdetail.LayoutRefId = ActivitiesRegistration.LayoutRefId;
                        langrefdetail.CustomerRefId = ActivitiesRegistration.CustomerRefId;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ActivitiesRegistration_ActivitiesRegistrationName;
                        langrefdetail.LangText = ActivitiesRegistration.ActivitiesRegistrationName;
                        langrefdetail.Language = LanguageName;
                        langrefdetail.CreatedBy = ActivitiesRegistration.UpdatedBy;
                        langrefdetail.UpdatedBy = ActivitiesRegistration.UpdatedBy;

                        _translatorRepository.SaveLangDetail(langrefdetail);

                        var questionRefId = context.Questions.Where(x => x.ActivitiesRefId == ActivitiesRegistration.ActivitiesRegistrationId && x.ParentId == 0).Select(x => x.QuestionId).FirstOrDefault();
                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = questionRefId;
                        objQuestions.Name = ActivitiesRegistration.ActivitiesRegistrationName;
                        objQuestions.UpdatedBy = ActivitiesRegistration.UpdatedBy;
                        objQuestions.UpdatedOn = DateTime.Now;
                        _questionPathRepository.UpdateQuestions(objQuestions);

                        /* Written By Selvam Thangaraj - Stop */
                    }
                    return existActReg;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<DefaultActivities> GetDefaultActivitiesOBC(int OBCTypeId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from DA in context.DefaultActivities
                                where DA.OBCTypeMasterRefId == OBCTypeId && (OBCTypeId == 3 ? (1 == 1) : (DA.OBCDefault == 1 && DA.ISPlanningSpec == 0 && DA.PauseTrip == 0))
                                select new DefaultActivities()
                                {
                                    ActivitiesRegistrationId = DA.ActivitiesRegistrationId,
                                    ActivitiesRegistrationName = DA.ActivitiesRegistrationName,
                                    WorkingCodeRefId = DA.WorkingCodeRefId,
                                    PlanningRefId = DA.PlanningRefId,
                                    PlanningOverviewRefId = DA.PlanningOverviewRefId,
                                    SpecificRefId = DA.SpecificRefId,
                                    Visible = DA.Visible,
                                    ConfirmButton = DA.ConfirmButton,
                                    PTORefId = DA.PTORefId,
                                    InterruptibleByRefId = DA.InterruptibleByRefId,
                                    TransportType = DA.TransportType,
                                    EmptyFullSolo = DA.EmptyFullSolo,
                                    Flexactivity = DA.Flexactivity,
                                    ISModification = DA.ISModification,
                                    CreatedBy = DA.CreatedBy,
                                    CreatedOn = DA.CreatedOn,
                                    OBCDefault = DA.OBCDefault,
                                    ISPlanningSpec = DA.ISPlanningSpec
                                }).ToList();

                    List<DefaultActivities> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<DefaultActivities> GetDefaultActivitiesTrip(int LayoutID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var OBCypeId = context.OBCType.Where(x => x.LayoutRefId == LayoutID).Select(x => x.OBCTypeRefId).FirstOrDefault();

                    var data = (from DA in context.DefaultActivities
                                where DA.OBCTypeMasterRefId == OBCypeId && DA.OBCDefault == 0 && DA.ISPlanningSpec == 0 && DA.PauseTrip == 0
                                select new DefaultActivities()
                                {
                                    ActivitiesRegistrationId = DA.ActivitiesRegistrationId,
                                    ActivitiesRegistrationName = DA.ActivitiesRegistrationName,
                                    WorkingCodeRefId = DA.WorkingCodeRefId,
                                    PlanningRefId = DA.PlanningRefId,
                                    PlanningOverviewRefId = DA.PlanningOverviewRefId,
                                    SpecificRefId = DA.SpecificRefId,
                                    Visible = DA.Visible,
                                    ConfirmButton = DA.ConfirmButton,
                                    PTORefId = DA.PTORefId,
                                    InterruptibleByRefId = DA.InterruptibleByRefId,
                                    TransportType = DA.TransportType,
                                    EmptyFullSolo = DA.EmptyFullSolo,
                                    Flexactivity = DA.Flexactivity,
                                    ISModification = DA.ISModification,
                                    CreatedBy = DA.CreatedBy,
                                    CreatedOn = DA.CreatedOn,
                                    OBCDefault = DA.OBCDefault,
                                    ISPlanningSpec = DA.ISPlanningSpec
                                }).ToList();

                    List<DefaultActivities> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<DefaultActivities> GetDefaultActivitiesOBCPlanningspec(int LayoutID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var OBCypeId = context.OBCType.Where(x => x.LayoutRefId == LayoutID).Select(x => x.OBCTypeRefId).FirstOrDefault();

                    var data = (from DA in context.DefaultActivities
                                where DA.OBCTypeMasterRefId == OBCypeId && DA.OBCDefault == 1 && DA.ISPlanningSpec == 1
                                select new DefaultActivities()
                                {
                                    ActivitiesRegistrationId = DA.ActivitiesRegistrationId,
                                    ActivitiesRegistrationName = DA.ActivitiesRegistrationName,
                                    WorkingCodeRefId = DA.WorkingCodeRefId,
                                    PlanningRefId = DA.PlanningRefId,
                                    PlanningOverviewRefId = DA.PlanningOverviewRefId,
                                    SpecificRefId = DA.SpecificRefId,
                                    Visible = DA.Visible,
                                    ConfirmButton = DA.ConfirmButton,
                                    PTORefId = DA.PTORefId,
                                    InterruptibleByRefId = DA.InterruptibleByRefId,
                                    TransportType = DA.TransportType,
                                    EmptyFullSolo = DA.EmptyFullSolo,
                                    Flexactivity = DA.Flexactivity,
                                    ISModification = DA.ISModification,
                                    CreatedBy = DA.CreatedBy,
                                    CreatedOn = DA.CreatedOn,
                                    OBCDefault = DA.OBCDefault,
                                    ISPlanningSpec = DA.ISPlanningSpec
                                }).ToList();

                    List<DefaultActivities> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<DefaultActivities> GetDefaultActivitiesOBCPauseTrip(int LayoutID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var OBCypeId = context.OBCType.Where(x => x.LayoutRefId == LayoutID).Select(x => x.OBCTypeRefId).FirstOrDefault();

                    var data = (from DA in context.DefaultActivities
                                where DA.OBCTypeMasterRefId == OBCypeId && DA.OBCDefault == 1 && DA.PauseTrip == 1
                                select new DefaultActivities()
                                {
                                    ActivitiesRegistrationId = DA.ActivitiesRegistrationId,
                                    ActivitiesRegistrationName = DA.ActivitiesRegistrationName,
                                    WorkingCodeRefId = DA.WorkingCodeRefId,
                                    PlanningRefId = DA.PlanningRefId,
                                    PlanningOverviewRefId = DA.PlanningOverviewRefId,
                                    SpecificRefId = DA.SpecificRefId,
                                    Visible = DA.Visible,
                                    ConfirmButton = DA.ConfirmButton,
                                    PTORefId = DA.PTORefId,
                                    InterruptibleByRefId = DA.InterruptibleByRefId,
                                    TransportType = DA.TransportType,
                                    EmptyFullSolo = DA.EmptyFullSolo,
                                    Flexactivity = DA.Flexactivity,
                                    ISModification = DA.ISModification,
                                    CreatedBy = DA.CreatedBy,
                                    CreatedOn = DA.CreatedOn,
                                    OBCDefault = DA.OBCDefault,
                                    ISPlanningSpec = DA.ISPlanningSpec
                                }).ToList();

                    List<DefaultActivities> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<DefaultActivities> GetDefaultActivitiesPlanning()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from DA in context.DefaultActivities
                                where DA.OBCDefault == 1 && DA.ISPlanningSpec == 1
                                select new DefaultActivities()
                                {
                                    ActivitiesRegistrationId = DA.ActivitiesRegistrationId,
                                    ActivitiesRegistrationName = DA.ActivitiesRegistrationName,
                                    WorkingCodeRefId = DA.WorkingCodeRefId,
                                    PlanningRefId = DA.PlanningRefId,
                                    PlanningOverviewRefId = DA.PlanningOverviewRefId,
                                    SpecificRefId = DA.SpecificRefId,
                                    Visible = DA.Visible,
                                    ConfirmButton = DA.ConfirmButton,
                                    PTORefId = DA.PTORefId,
                                    InterruptibleByRefId = DA.InterruptibleByRefId,
                                    TransportType = DA.TransportType,
                                    EmptyFullSolo = DA.EmptyFullSolo,
                                    Flexactivity = DA.Flexactivity,
                                    ISModification = DA.ISModification,
                                    CreatedBy = DA.CreatedBy,
                                    CreatedOn = DA.CreatedOn,
                                    OBCDefault = DA.OBCDefault,
                                    ISPlanningSpec = DA.ISPlanningSpec
                                }).ToList();

                    List<DefaultActivities> returnList;
                    return returnList = data.ToList();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeletActivities(int Layout_Id, string Activitiesname)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ActivitiesRegistration.SingleOrDefault(IC => IC.LayoutRefId == Layout_Id && IC.ActivitiesRegistrationName == Activitiesname);
                    if (result != null)
                    {
                        context.ActivitiesRegistration.Remove(result);
                        context.SaveChanges();

                        /* Written By Selvam Thangaraj - Go */

                        LangRefDetail langrefdetail = new LangRefDetail();
                        langrefdetail.RefId = result.ActivitiesRegistrationId;
                        langrefdetail.LayoutRefId = result.LayoutRefId;
                        langrefdetail.CustomerRefId = null;
                        langrefdetail.LangRefColTableId = (int)LangRefColTable.ActivitiesRegistration_ActivitiesRegistrationName;

                        _translatorRepository.DeleteLangDetail(langrefdetail);

                        /* Written By Selvam Thangaraj - Stop */
                    }

                    return "Deleted";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public List<Layout> GetLayout()
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<Layout> returnList;
                    return returnList = context.Layout.Where(x => x.LayoutStatusRefId != (int)EnumLayoutStatus.ValidationRequested).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string DeleteActivity(int layoutId, int activityId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.ActivitiesRegistration.SingleOrDefault(IC => IC.LayoutRefId == layoutId && IC.ActivitiesRegistrationId == activityId);
                    if (result != null)
                    {
                        result.ISModification = (int)EnumCopyColorCode.Red;

                        result.IsModificationActivitiesRegistrationName = (int)EnumCopyColorCode.Red;
                        result.IsModificationWorkingCodeRefId = (int)EnumCopyColorCode.Red;
                        result.IsModificationPlanningRefId = (int)EnumCopyColorCode.Red;
                        result.IsModificationPlanningOverviewRefId = (int)EnumCopyColorCode.Red;
                        result.IsModificationSpecificRefId = (int)EnumCopyColorCode.Red;
                        result.IsModificationVisible = (int)EnumCopyColorCode.Red;
                        result.IsModificationConfirmButton = (int)EnumCopyColorCode.Red;
                        result.IsModificationTransportType = (int)EnumCopyColorCode.Red;
                        result.IsModificationEmptyFullSolo = (int)EnumCopyColorCode.Red;
                        result.IsModificationFlexactivity = (int)EnumCopyColorCode.Red;
                        result.IsModificationPTORefId = (int)EnumCopyColorCode.Red;
                        result.IsModificationInterruptibleByRefId = (int)EnumCopyColorCode.Red;

                        context.SaveChanges();

                        var questionList = context.Questions.Where(x => x.LayoutRefId == layoutId && x.ActivitiesRefId == activityId).ToList();
                        foreach (var questions in questionList)
                        {
                            questions.ISModification = (int)EnumCopyColorCode.Red;
                        }
                        context.SaveChanges();
                    }
                    return "Deleted";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string DeleteActivityNew(int layoutId, int activityId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var deleteresultQuestions = context.Questions.Where(IC => IC.LayoutRefId == layoutId && IC.ActivitiesRefId == activityId).ToList();
                    if (deleteresultQuestions.Any())
                    {
                        var IsCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();
                        _questionPathRepository.DeleteQuestions(deleteresultQuestions, IsCopyLayout, activityId);
                    }

                    return "deleted";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string SetMainLanguage(int layoutID, int languageId, string updatedBy, DateTime updatedOn)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.UsedLanguages.Where(IC => IC.LayoutRefId == layoutID).ToList();

                    foreach (var item in result)
                        item.IsDefault = 0;

                    context.SaveChanges();

                    var Updatedresult = context.UsedLanguages.SingleOrDefault(IC => IC.LayoutRefId == layoutID && IC.LanguageRefId == languageId);
                    if (Updatedresult != null)
                    {
                        Updatedresult.IsDefault = 1;
                        Updatedresult.UpdatedBy = updatedBy;
                        Updatedresult.UpdatedOn = updatedOn;

                        context.SaveChanges();
                    }

                    return "Updated";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string DeleteUsedLanguage(int layoutID, int languageId, string updatedBy, DateTime updatedOn)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var deleteresult = context.UsedLanguages.Where(IC => IC.LayoutRefId == layoutID && IC.LanguageRefId == languageId).FirstOrDefault();
                    if (deleteresult != null)
                    {

                        var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutID).Select(x => x.IsCopyLayout).FirstOrDefault();
                        if (isCopyLayout && deleteresult.IsModification == (int)EnumCopyColorCode.Empty)
                        {
                            deleteresult.IsModification = (int)EnumCopyColorCode.Red;
                            deleteresult.IsActive = false;
                            context.SaveChanges();
                        }
                        else
                        {
                            context.UsedLanguages.Remove(deleteresult);
                            context.SaveChanges();
                        }
                    }

                    return "deleted";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string DeleteTransportType(int layoutID, int transporttypeId, string updatedBy, DateTime updatedOn)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var existsActRegList = context.ActivitiesRegistration.Where(x => x.LayoutRefId == layoutID && x.TransportType).Select(x => x.ActivitiesRegistrationId).ToList();
                    if (existsActRegList.Count() == 0)
                    {
                        if (context.Questions.Where(IC => IC.LayoutRefId == layoutID && IC.TransporttypeRefId == transporttypeId).Count() == 0)
                        {
                            var deleteresult = context.TransportTypes.Where(IC => IC.LayoutRefId == layoutID && IC.TransportTypeId == transporttypeId).FirstOrDefault();
                            if (deleteresult != null)
                            {
                                var aliasNameDetailsId = context.AliasNameDetails.Where(x => x.LayoutRefId == layoutID && x.PropertyName == "Transport Type" && x.AliasName == "TransportType").Select(x => x.AliasNameDetailsId).FirstOrDefault();
                                var aliasNameDetailsFromQuestions = _questionPathRepository.GetAliasNameDetails(0, deleteresult.LayoutRefId, "TT").Select(x => x.AliasNameDetailsId).Distinct().ToList();
                                if (!aliasNameDetailsFromQuestions.Contains(aliasNameDetailsId))
                                {
                                    var transportTypeList = context.TransportTypePropertiesDetail.Where(x => x.DashboardTransportTypeId == deleteresult.TransportTypeId).Select(x => new { x.QuestionRefId, x.TransportTypeDetailId }).ToList();
                                    foreach (var ttp in transportTypeList)
                                    {
                                        var tragetttd = context.TransportTypePropertiesDetail.Where(x => x.TransportTypeDetailId == ttp.TransportTypeDetailId).FirstOrDefault();
                                        if (tragetttd != null)
                                        {
                                            /* Written By Selvam Thangaraj - Go */

                                            LangRefDetail langrefdetail = new LangRefDetail();
                                            langrefdetail.RefId = tragetttd.TransportTypeDetailId;
                                            langrefdetail.LayoutRefId = deleteresult.LayoutRefId;
                                            langrefdetail.CustomerRefId = null;
                                            langrefdetail.LangRefColTableId = (int)LangRefColTable.TransportTypePropertiesDetail_Title;

                                            _translatorRepository.DeleteLangDetail(langrefdetail);

                                            /* Written By Selvam Thangaraj - Stop */

                                            context.TransportTypePropertiesDetail.Remove(tragetttd);
                                            context.SaveChanges();
                                        }

                                        var tragetQuestion = context.Questions.Where(x => x.QuestionId == ttp.QuestionRefId).FirstOrDefault();
                                        if (tragetQuestion != null)
                                        {
                                            context.Questions.Remove(tragetQuestion);
                                            context.SaveChanges();
                                        }

                                    }

                                    var isCopyLayout = context.Layout.Where(x => x.LayoutId == layoutID).Select(x => x.IsCopyLayout).FirstOrDefault();
                                    if (isCopyLayout && deleteresult.IsModification == (int)EnumCopyColorCode.Empty)
                                    {
                                        deleteresult.IsModification = (int)EnumCopyColorCode.Red;
                                        deleteresult.IsActive = false;
                                    }
                                    else
                                        context.TransportTypes.Remove(deleteresult);

                                    context.SaveChanges();
                                    return "";
                                }
                                else
                                    return "Transport Type Alias Used For That Layout, You Can't Remove This Transport Type";
                            }
                            else
                                return "Transport Type Already Removed";

                        }
                        else
                            return "Question Path is Available For This Transport Type, You Can't Remove This Transport Type";
                    }
                    else
                        return "Activities-Regisration is Available For This Transport Type, You Can't Remove This Transport Type";

                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public string DeleteAllowance(int LayoutId, int AllowanceMasterId, string updatedBy, DateTime updatedOn)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (context.Allowances.Where(IC => IC.AllowancesRefId == AllowanceMasterId).Count() == 0)
                    {
                        var deleteresult = context.AllowancesMaster.Where(IC => IC.AllowancesMasterId == AllowanceMasterId).FirstOrDefault();
                        if (deleteresult != null)
                        {
                            context.AllowancesMaster.Remove(deleteresult);
                            context.SaveChanges();
                        }
                        return "";
                    }
                    else
                        return "Allowance Already Used,So Can't Remove This Allowance.";
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public string UpdateOBCTypesFlex(int OBCTypeRefId, int LayoutRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var OBCUpdateexists = context.OBCType.SingleOrDefault(IC => IC.LayoutRefId == LayoutRefId && IC.OBCTypeRefId == OBCTypeRefId);
                    if (OBCUpdateexists == null)
                    {
                        var isCopyLayout = false;
                        var data = (from OM in context.OBCTypeMaster
                                    where OM.OBCTypeId == OBCTypeRefId
                                    select OM.OBCTypeDescription
                                    ).SingleOrDefault();
                        var result = context.Layout.SingleOrDefault(IC => IC.LayoutId == LayoutRefId);
                        if (result != null)
                        {
                            isCopyLayout = result.IsCopyLayout;
                            result.LayoutType = data;
                            context.SaveChanges();
                        }
                        var OBCUpdate = context.OBCType.SingleOrDefault(IC => IC.LayoutRefId == LayoutRefId);
                        if (OBCUpdate != null)
                        {
                            if (isCopyLayout && OBCTypeRefId != OBCUpdate.OBCTypeRefId)
                                OBCUpdate.IsModification = (int)EnumCopyColorCode.Yellow;

                            OBCUpdate.OBCTypeRefId = OBCTypeRefId;
                            context.SaveChanges();
                        }
                    }
                    return "Updated";
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public int GetOBCTypeByLayout(int LayoutRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var OBCTypeRefId = context.OBCType.Where(IC => IC.LayoutRefId == LayoutRefId).Select(x => x.OBCTypeRefId).FirstOrDefault();
                    return OBCTypeRefId;
                }

            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public string UpdateLayoutNotes(LayoutNotes layoutNotes)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (layoutNotes.LayoutStatusRefId == 0)
                    {
                        var layoutStatusRefId = context.Layout.Where(x => x.LayoutId == layoutNotes.LayoutRefId).Select(x => x.LayoutStatusRefId).FirstOrDefault();
                        layoutNotes.LayoutStatusRefId = layoutStatusRefId;
                    }

                    context.Add(layoutNotes);
                    context.SaveChanges();
                    return layoutNotes.Notes;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string GetUserEmailId(string DomainId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var address = context.Users.Where(x => x.DomainId == DomainId).FirstOrDefault();
                    if (address != null)
                        return address.EmailId;
                    else
                        return null;
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public Customers GetCustomer(int customerRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Customers.Where(x => x.CustomerId == customerRefId).FirstOrDefault();
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool GetExsistingLayoutName(int CustomerId, string LayoutName)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.Layout.Where(x => x.CustomerRefId == CustomerId && x.LayoutName == LayoutName.Trim()).FirstOrDefault();
                    if (result == null)
                        return true;
                    else
                        return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public bool GetInfoColumnDetails(int ToCustomerId, int FromCustomerId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var fromresult = context.InfoColumn.Where(x => x.CustomerRefId == FromCustomerId).ToList();
                    var toresult = context.InfoColumn.Where(x => x.CustomerRefId == ToCustomerId).ToList();
                    if (ToCustomerId == FromCustomerId)
                        return false;
                    if (toresult.Count() == 0 && fromresult.Count() > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public string GetCopyLayoutColor(int IsModification)
        {
            var colorname = "";
            if (IsModification == 0)
                colorname = "Green";
            else if (IsModification == 2)
                colorname = "Yellow";
            else if (IsModification == 3)
                colorname = "Red";
            else
                colorname = "Grey";

            return colorname;
        }
        public string DeleteLayoutNotes(int layoutNotesId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = context.LayoutNotes.Where(x => x.Id == layoutNotesId).FirstOrDefault();
                    if (result != null)
                    {
                        context.LayoutNotes.Remove(result);
                        context.SaveChanges();
                    }
                    return "";
                }

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public int GetCustomerSourceId(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var customerSourceId = 0;
                    var customerId = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.CustomerRefId).FirstOrDefault();
                    if (customerId > 0)
                        customerSourceId = context.Customers.Where(x => x.CustomerId == customerId).Select(x => x.SourceId).FirstOrDefault();

                    return customerSourceId;
                }

            }
            catch (Exception ex)
            {
                return 999999;
            }
        }

        public List<LayoutNotes> EditLayoutNotes(int layoutId, int layoutNoteId, string layoutNotes)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var target = context.LayoutNotes.FirstOrDefault(x => x.Id == layoutNoteId);
                    if (target != null)
                    {
                        target.Notes = layoutNotes;
                        context.SaveChanges();
                    }

                    var layoutNotesList = (from L in context.LayoutNotes.ToList()
                                           join LS in context.LayoutStatus.ToList()
                                           on L.LayoutStatusRefId equals LS.Id
                                           where L.LayoutRefId == layoutId
                                           select new LayoutNotes()
                                           {
                                               Id = L.Id,
                                               CreatedByName = L.CreatedByName,
                                               Notes = L.Notes,
                                               LayoutStatusName = LS.Name,
                                               LayoutRefId = L.LayoutRefId,
                                               LayoutStatusRefId = L.LayoutStatusRefId,
                                               CreatedOn = L.CreatedOn
                                           }).OrderByDescending(x => x.CreatedOn).ToList();


                    return layoutNotesList;
                }
            }
            catch (Exception ex)
            {
                return new List<LayoutNotes>();
            }
        }
        public string GetAlertCopiedLayout(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    SqlParameter layoutParam = new SqlParameter("@LayoutRefId", layoutId);

                    var layoutList = context.AlertCopiedLayout.FromSql("GetAlertCopiedLayout @LayoutRefId", layoutParam).ToList();

                    if (layoutList.Any(x => x.Code == "SL") && layoutList.Any(x => x.Code == "TR"))
                        return "SLƒTR";
                    else if (layoutList.Any(x => x.Code == "SL") && (!layoutList.Any(x => x.Code == "TR")))
                        return "SLƒN";
                    else if ((!layoutList.Any(x => x.Code == "SL")) && layoutList.Any(x => x.Code == "TR"))
                        return "NƒTR";
                    else
                        return "NƒN";
                }
            }
            catch (Exception ex)
            {
                return "NƒN";
            }
        }
        public List<Layout> GetLayout(int customerId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    return context.Layout.Where(x => x.CustomerRefId == customerId).ToList();
                }

            }
            catch (Exception ex)
            {
                return new List<Layout>();
            }
        }
    }
}

//context.Update
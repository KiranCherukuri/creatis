﻿using CREATIS.ApplicationCore.Modals;
using CREATIS.Infrastructure.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CREATIS.Infrastructure.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        public readonly IHomeRepository _homeRepository;
        public CustomerRepository(IHomeRepository homeRepository)
        {
            this._homeRepository = homeRepository;
        }

        public Creatis_Context dbContext;
        public String SaveCustomer(Customers cusModel)
        {
            var result = "";
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (cusModel.CustomerId > 0)
                    {
                        var target = context.Customers.Where(x => x.CustomerId == cusModel.CustomerId).FirstOrDefault();
                        if (target != null)
                        {
                            target.Environment = cusModel.Environment;
                            target.Source = cusModel.Source;
                            target.Name = cusModel.Name;
                            target.Active = cusModel.Active;
                            target.ModifiedDateTime = cusModel.ModifiedDateTime;
                            target.CustomerSystemTypeRefId = context.CustomerSystemType.Where(x => x.Name == cusModel.CustomerSystemTypeName).Select(x => x.Id).FirstOrDefault();
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        cusModel.CustomerSystemTypeRefId = context.CustomerSystemType.Where(x => x.Name == cusModel.CustomerSystemTypeName).Select(x => x.Id).FirstOrDefault();
                        cusModel.CreatedDateTime = DateTime.Now;
                        context.Customers.Add(cusModel);
                        context.SaveChanges();

                        var infoColumnMasters = context.InfoColumnMaster.Select(x => x.ICM_Column_No).ToList();
                        foreach (var icno in infoColumnMasters)
                        {
                            InfoColumns infomodal = new InfoColumns();

                            infomodal.CreatedBy = cusModel.CreatedBy;
                            infomodal.CreatedOn = DateTime.Now;
                            infomodal.ICM_Column_Ref_No = icno;
                            infomodal.IC_Column_Name = "Info " + icno;
                            infomodal.CustomerRefId = cusModel.CustomerId;
                            infomodal.IS_Active = 1;

                            context.InfoColumn.Add(infomodal);
                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public List<Customers> GetCustomer()
        {
            List<Customers> cusList;
            try
            {
                using (var context = new Creatis_Context())
                {
                    cusList = (from c in context.Customers
                                join cs in context.CustomerSystemType
                                on  c.CustomerSystemTypeRefId equals cs.Id
                                select new Customers()
                                {
                                    CustomerId = c.CustomerId,
                                    Name = c.Name,
                                    IsActive = (c.Active == 1 ? "Active" : "InActive"),
                                    Environment = c.Environment,
                                    Source = c.Source,
                                    CustomCustomer = c.CustomCustomer,
                                    CustomerSystemTypeName =cs.Name
                                }).OrderByDescending(x=> x.CustomerId).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<Customers>();
            }
            return cusList;
        }
        public List<CustomerSystemType> GetCustomerSystemType()
        {
            List<CustomerSystemType> customerSystemTypeList;
            try
            {
                using (var context = new Creatis_Context())
                {
                    customerSystemTypeList = context.CustomerSystemType.Where(x=> x.IsActive).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<CustomerSystemType>();
            }
            return customerSystemTypeList;
        }
        public string DeleteCustomer(int customerId)
        {
            var result = "";
            try
            {
                using (var context = new Creatis_Context())
                {
                    var layoutIds = context.Layout.Where(x => x.CustomerRefId == customerId).Select(x => x.LayoutId).ToList();
                    var icInfo = context.InfoColumn.Where(x => x.CustomerRefId == customerId).ToList();
                    if (icInfo.Any())
                    {
                        context.InfoColumn.RemoveRange(icInfo);
                        context.SaveChanges();
                    }
                    var cnInfo = context.CustomerNote.Where(x => x.CustomerRefId == customerId).ToList();
                    if (cnInfo.Any())
                    {
                        context.CustomerNote.RemoveRange(cnInfo);
                        context.SaveChanges();
                    }

                    var iInfo = context.Integrator.Where(x => x.CustomerRefId == customerId).ToList();
                    if (iInfo.Any())
                    {
                        context.Integrator.RemoveRange(iInfo);
                        context.SaveChanges();
                    }

                    foreach (var layoutId in layoutIds)
                    {
                        _homeRepository.DeleteDashboard(layoutId);
                    }

                    var target = context.Customers.FirstOrDefault(x => x.CustomerId == customerId);
                    context.Customers.Remove(target);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
    }
}
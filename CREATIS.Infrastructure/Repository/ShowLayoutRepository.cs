﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using Microsoft.EntityFrameworkCore.Internal;
using CREATIS.Infrastructure.IRepository;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CREATIS.Infrastructure.Repository
{
    public class ShowLayoutRepository : Utility, IShowLayoutRepository
    {

        public Creatis_Context dbconext;

        public readonly ITranslatorRepository _translatorRepository;

        public readonly IQuestionPathRepository _questionPathRepository;
        public ShowLayoutRepository(ITranslatorRepository translatorRepository, IQuestionPathRepository questionPathRepository)
        {
            this._translatorRepository = translatorRepository;
            this._questionPathRepository = questionPathRepository;
        }
        public List<ActivitiesRegistration> GetActiveRegistrationByLayoutId(int LayoutId)

        {
            using (var context = new Creatis_Context())
            {

                return context.ActivitiesRegistration.Where(x => x.LayoutRefId == LayoutId).ToList();
            }


        }
        public List<Questions> GetAllQusByActivityId(int LayoutId)
        {
            using (var context = new Creatis_Context())
            {

                return (from actlist in context.ActivitiesRegistration.Where(predicate: x => x.LayoutRefId == LayoutId).ToList()
                        join quslist in context.Questions.ToList() on actlist.ActivitiesRegistrationId equals
                         quslist.ActivitiesRefId
                        select quslist).ToList();
            }

        }
        public List<Questions> GetQuestionsByActivityId(int activityId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var questionList = context.Questions.Where(x => x.ActivitiesRefId == activityId).ToList();

                    return questionList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public ShowLayoutViewModel GetAllShowLayoutModelByActivitesID(int ActivitiesId)
        {
            using (var context = new Creatis_Context())
            {
                ShowLayoutViewModel model = new ShowLayoutViewModel();

                model.QuestionPathShowLayoutLists = context.Questions.Where(y => y.ActivitiesRefId == ActivitiesId).Select
                                                     (x => new QuestionPathShowLayoutList
                                                     {
                                                         ActivityId = x.ActivitiesRefId,
                                                         WorkingCode = x.ActivitiesRegistration.WorkingCodeRefId,
                                                         ActivityName = x.ActivitiesRegistration.ActivitiesRegistrationName,
                                                         Confirmbutton = x.ActivitiesRegistration.ConfirmButton,
                                                         QuestionId = x.QuestionId,
                                                         QuestionName = x.Name,
                                                         ParentId = x.ParentId
                                                     }).ToList();

                if (model.QuestionPathShowLayoutLists.Count() <= 0)
                {
                    model.ActivitiesRegistrations = context.ActivitiesRegistration.Where(y => y.ActivitiesRegistrationId == ActivitiesId).ToList();

                }


                return model;
            }
        }
        public ShowLayoutViewModel GetAllShowLayoutModelByLayoutID(int LayoutId)
        {


            try
            {
                using (var context = new Creatis_Context())
                {
                    ShowLayoutViewModel model = new ShowLayoutViewModel();

                    model.ActivitiesRegistrations = context.ActivitiesRegistration.Where(x => x.LayoutRefId == LayoutId).ToList();

                    model.QuestionPathShowLayoutLists = (from actlist in context.ActivitiesRegistration.Where(x => x.LayoutRefId == LayoutId).ToList()
                                                         join quslist in context.Questions.ToList() on actlist.ActivitiesRegistrationId equals
                                                         quslist.ActivitiesRefId
                                                         select new QuestionPathShowLayoutList
                                                         {
                                                             ActivityId = quslist.ActivitiesRefId,
                                                             WorkingCode = quslist.ActivitiesRegistration.WorkingCodeRefId,
                                                             ActivityName = quslist.ActivitiesRegistration.ActivitiesRegistrationName,
                                                             Confirmbutton = quslist.ActivitiesRegistration.ConfirmButton,
                                                             QuestionId = quslist.QuestionId,
                                                             QuestionName = quslist.Name,
                                                             ParentId = quslist.ParentId

                                                         }).ToList();

                    return model;

                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ShowLayoutResponse> GetQuestionsByLayoutId(int LayoutId, int activitiesRegistrationId)
        {
            List<BoShowLayout> objBoShowLayout = new List<BoShowLayout>();
            List<ShowLayoutResponse> objShowLayoutResponse = new List<ShowLayoutResponse>();

            using (var context = new Creatis_Context())
            {
                var parentIDDD = string.Empty;
                SqlParameter layoutId = new SqlParameter("@pLayoutRefId", LayoutId);
                SqlParameter ActivitiesRegistrationId = new SqlParameter("@pActivityId", activitiesRegistrationId);
                objBoShowLayout = context.ShowLayout.FromSql("GetShowLayoutQuestionTraslations @pLayoutRefId,@pActivityId ", layoutId, ActivitiesRegistrationId).Select(x => new BoShowLayout
                {
                    ActivitiesRefId = x.ActivitiesRefId,
                    ConfirmButton = x.ConfirmButton ?? false,
                    CopyLayoutRefId = x.CopyLayoutRefId,
                    LayoutRefId = x.LayoutRefId,
                    LanguageName = x.LanguageName,
                    Name = x.Name,
                    CustomerRefId = x.CustomerRefId,
                    Nodetype = x.Nodetype,
                    ParentId = x.ParentId,
                    PropertyName = x.PropertyName,
                    QuestionId = x.QuestionId,
                    TransporttypeRefId = x.TransporttypeRefId,
                    WorkingCodeRefId = x.WorkingCodeRefId,
                    QuestionPathCount = x.QuestionPathCount ?? 0,
                    Info = x.Info == null ? "" : x.Info.TrimStart(',').TrimEnd(' ').TrimEnd(','),
                    FeedBackType = x.FeedBackType,
                    PartnerCode = x.PartnerCode,
                    TransportType = x.TransportType,
                    ISModification = x.ISModification,
                    ISActivityModification = x.ISActivityModification,
                    IsFlex = x.IsFlex,
                    IsSmart = x.IsSmart,
                    Visible = x.Visible,
                    PTO = x.PTO,
                    InterruptedBy = x.InterruptedBy,
                    Arabic = x.Arabic,
                    Bulgarian = x.Bulgarian,
                    Croatian = x.Croatian,
                    Czech = x.Czech,
                    Danish = x.Danish,
                    Dutch = x.Dutch,
                    English = x.English,
                    Estonian = x.Estonian,
                    Finnish = x.Finnish,
                    French = x.French,
                    German = x.German,
                    Greek = x.Greek,
                    Hungarian = x.Hungarian,
                    Italian = x.Italian,
                    Latvian = x.Latvian,
                    Lithuanian = x.Lithuanian,
                    Macedonian = x.Macedonian,
                    Norwegian = x.Norwegian,
                    Polish = x.Polish,
                    Portuguese = x.Portuguese,
                    Romanian = x.Romanian,
                    Russian = x.Russian,
                    Slovak = x.Slovak,
                    Slovene = x.Slovene,
                    Spanish = x.Spanish,
                    Swedish = x.Swedish,
                    Turkish = x.Turkish,
                    Ukrainian = x.Ukrainian,
                    Belarusian = x.Belarusian,
                    LayoutName = x.LayoutName,
                    PlanningOverviewRefId = x.PlanningOverviewRefId,
                    SpecificRefId = x.SpecificRefId,
                    EmptyFullSolo = x.EmptyFullSolo,
                    IsTransportType = x.IsTransportType,
                    PlanningRefId = x.PlanningRefId,
                    Comment = x.Comment,
                    Flexactivity = x.Flexactivity,
                    IsModificationActivitiesRegistrationName=x.IsModificationActivitiesRegistrationName,
                    IsModificationWorkingCodeRefId=x.IsModificationWorkingCodeRefId,
                    IsModificationPlanningRefId= x.IsModificationPlanningRefId,
                    IsModificationPlanningOverviewRefId = x.IsModificationPlanningOverviewRefId,
                    IsModificationSpecificRefId = x.IsModificationSpecificRefId,
                    IsModificationVisible = x.IsModificationVisible,
                    IsModificationConfirmButton = x.IsModificationConfirmButton,
                    IsModificationTransportType = x.IsModificationTransportType,
                    IsModificationEmptyFullSolo = x.IsModificationEmptyFullSolo,
                    IsModificationFlexactivity = x.IsModificationFlexactivity,
                    IsModificationPTORefId = x.IsModificationPTORefId,
                    IsModificationInterruptibleByRefId = x.IsModificationInterruptibleByRefId
                }).ToList();

                foreach (var activity in objBoShowLayout.GroupBy(z => new { z.ActivitiesRefId, z.TransporttypeRefId }))
                {
                    ShowLayoutResponse objShowLayRes = new ShowLayoutResponse();
                    objShowLayRes.ActivitiesRegistrationName = activity.Select(s => s.ActivitiesRegistrationName).FirstOrDefault();
                    objShowLayRes.ActivitiesRegId = activity.Select(s => s.ActivitiesRefId).FirstOrDefault();
                    objShowLayRes.ConfirmButton = Convert.ToBoolean(activity.Select(s => s.ConfirmButton).FirstOrDefault());
                    objShowLayRes.WorkingCodeRefId = activity.Select(s => s.WorkingCodeRefId).FirstOrDefault();
                    objShowLayRes.QuestionPathCount = Convert.ToInt32(activity.Select(s => s.QuestionPathCount).FirstOrDefault()) + 1;
                    objShowLayRes.TransportType = activity.Select(s => s.TransportType).FirstOrDefault();
                    objShowLayRes.ISActivityModification = activity.Select(s => s.ISActivityModification).FirstOrDefault();
                    objShowLayRes.IsSmart = activity.Where(x => x.IsSmart == true).Count() > 0 ? true : false;
                    objShowLayRes.IsFlex = activity.Where(x => x.IsFlex == true).Count() > 0 ? true : false;
                    objShowLayRes.Visible = activity.Where(x => x.Visible == true).Count() > 0 ? true : false;
                    objShowLayRes.PTO = activity.Select(s => s.PTO).FirstOrDefault();
                    objShowLayRes.InterruptedBy = activity.Select(s => s.InterruptedBy).FirstOrDefault();
                    objShowLayRes.LayoutName = activity.Select(x => x.LayoutName).FirstOrDefault();
                    objShowLayRes.PlanningOverviewRefId = activity.Select(x => x.PlanningOverviewRefId).FirstOrDefault();
                    objShowLayRes.PlanningRefId = activity.Select(x => x.PlanningRefId).FirstOrDefault();
                    objShowLayRes.SpecificRefId = activity.Select(x => x.SpecificRefId).FirstOrDefault();
                    objShowLayRes.EmptyFullSolo = activity.Where(x => x.EmptyFullSolo == true).Count() > 0 ? true : false;
                    objShowLayRes.IsTransportType = activity.Where(x => x.IsTransportType == true).Count() > 0 ? true : false;
                    objShowLayRes.Flexactivity = activity.Where(x => x.Flexactivity == true).Count() > 0 ? true : false;
                    objShowLayRes.IsModificationActivitiesRegistrationName = activity.Select(s => s.IsModificationActivitiesRegistrationName).FirstOrDefault();
                    objShowLayRes.IsModificationWorkingCodeRefId = activity.Select(s => s.IsModificationWorkingCodeRefId).FirstOrDefault();
                    objShowLayRes.IsModificationPlanningRefId = activity.Select(s => s.IsModificationPlanningRefId).FirstOrDefault();
                    objShowLayRes.IsModificationPlanningOverviewRefId = activity.Select(s => s.IsModificationPlanningOverviewRefId).FirstOrDefault();
                    objShowLayRes.IsModificationSpecificRefId = activity.Select(s => s.IsModificationSpecificRefId).FirstOrDefault();
                    objShowLayRes.IsModificationVisible = activity.Select(s => s.IsModificationVisible).FirstOrDefault();
                    objShowLayRes.IsModificationConfirmButton = activity.Select(s => s.IsModificationConfirmButton).FirstOrDefault();
                    objShowLayRes.IsModificationTransportType = activity.Select(s => s.IsModificationTransportType).FirstOrDefault();
                    objShowLayRes.IsModificationEmptyFullSolo = activity.Select(s => s.IsModificationEmptyFullSolo).FirstOrDefault();
                    objShowLayRes.IsModificationFlexactivity = activity.Select(s => s.IsModificationFlexactivity).FirstOrDefault();
                    objShowLayRes.IsModificationPTORefId = activity.Select(s => s.IsModificationPTORefId).FirstOrDefault();
                    objShowLayRes.IsModificationInterruptibleByRefId = activity.Select(s => s.IsModificationInterruptibleByRefId).FirstOrDefault();

                    objShowLayRes.Comment = activity.Select(x => x.Comment).FirstOrDefault();
                    if (!activity.Select(x => x.Flexactivity).FirstOrDefault())
                        objShowLayRes.LayQuesions = activity.Where(x => !x.IsFlex).ToList();
                    else
                        objShowLayRes.LayQuesions = activity.ToList();

                    objShowLayoutResponse.Add(objShowLayRes);
                }
                return objShowLayoutResponse;
            }
        }
        public string GetDefaultShowlayoutLanguage(int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = (from UL in context.UsedLanguages
                                join LA in context.LanguageMaster
                                on UL.LanguageRefId equals LA.LanguageId
                                where UL.LayoutRefId == layoutId && UL.IsDefault == 1
                                select (UL.IsDefault == 1 ? LA.LanguageDescription : LA.LanguageDescription)).FirstOrDefault();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetLanguageName(int launguageId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var data = context.LanguageMaster.Where(x => x.LanguageId == launguageId).Select(x => x.LanguageDescription).SingleOrDefault();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public LangDetail GetLaunaguageDetailsByLatoutId(int langRefColTableId, int refId, int layoutId)
        {
            try
            {
                LangDetail langresult = new LangDetail();
                using (var context = new Creatis_Context())
                {
                    var IsCopyLayout = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.IsCopyLayout).FirstOrDefault();

                    var langdetail = (from lrd in context.LangRefDetail.Where(x => x.LangRefColTableId == langRefColTableId && x.RefId == refId && x.LayoutRefId == layoutId).ToList()
                                      join ld in context.LangDetail.ToList() on lrd.LangDetailId equals ld.Id
                                      select new LangDetail()
                                      {
                                          Id = ld.Id,
                                          LangRefDetailId = ld.LangRefDetailId,
                                          Arabic = ld.Arabic,
                                          Bulgarian = ld.Bulgarian,
                                          Croatian = ld.Croatian,
                                          Czech = ld.Czech,
                                          Danish = ld.Danish,
                                          Dutch = ld.Dutch,
                                          English = ld.English,
                                          Estonian = ld.Estonian,
                                          Finnish = ld.Finnish,
                                          French = ld.French,
                                          German = ld.German,
                                          Greek = ld.Greek,
                                          Hungarian = ld.Hungarian,
                                          Italian = ld.Italian,
                                          Latvian = ld.Latvian,
                                          Lithuanian = ld.Lithuanian,
                                          Macedonian = ld.Macedonian,
                                          Norwegian = ld.Norwegian,
                                          Polish = ld.Polish,
                                          Portuguese = ld.Portuguese,
                                          Romanian = ld.Romanian,
                                          Russian = ld.Russian,
                                          Slovak = ld.Slovak,
                                          Slovene = ld.Slovene,
                                          Spanish = ld.Spanish,
                                          Swedish = ld.Swedish,
                                          Turkish = ld.Turkish,
                                          Ukrainian = ld.Ukrainian,
                                          Belarusian = ld.Belarusian,
                                          CreatedBy = ld.CreatedBy,
                                          CreatedOn = ld.CreatedOn,
                                          UpdatedBy = ld.UpdatedBy,
                                          UpdatedOn = ld.UpdatedOn,
                                          IsModificationColorArabic = (IsCopyLayout == true && ld.Arabic != null) ? GetCopyLayoutColor(ld.IsModificationArabic) : "",
                                          IsModificationColorBulgarian = (IsCopyLayout == true && ld.Bulgarian != null) ? GetCopyLayoutColor(ld.IsModificationBulgarian) : "",
                                          IsModificationColorCroatian = (IsCopyLayout == true && ld.Croatian != null) ? GetCopyLayoutColor(ld.IsModificationCroatian) : "",
                                          IsModificationColorCzech = (IsCopyLayout == true && ld.Czech != null) ? GetCopyLayoutColor(ld.IsModificationCzech) : "",
                                          IsModificationColorDanish = (IsCopyLayout == true && ld.Danish != null) ? GetCopyLayoutColor(ld.IsModificationDanish) : "",
                                          IsModificationColorDutch = (IsCopyLayout == true && ld.Dutch != null) ? GetCopyLayoutColor(ld.IsModificationDutch) : "",
                                          IsModificationColorEnglish = (IsCopyLayout == true && ld.English != null) ? GetCopyLayoutColor(ld.IsModificationEnglish) : "",
                                          IsModificationColorEstonian = (IsCopyLayout == true && ld.Estonian != null) ? GetCopyLayoutColor(ld.IsModificationEstonian) : "",
                                          IsModificationColorFinnish = (IsCopyLayout == true && ld.Finnish != null) ? GetCopyLayoutColor(ld.IsModificationFinnish) : "",
                                          IsModificationColorFrench = (IsCopyLayout == true && ld.French != null) ? GetCopyLayoutColor(ld.IsModificationFrench) : "",
                                          IsModificationColorGerman = (IsCopyLayout == true && ld.German != null) ? GetCopyLayoutColor(ld.IsModificationGerman) : "",
                                          IsModificationColorGreek = (IsCopyLayout == true && ld.Greek != null) ? GetCopyLayoutColor(ld.IsModificationGreek) : "",
                                          IsModificationColorHungarian = (IsCopyLayout == true && ld.Hungarian != null) ? GetCopyLayoutColor(ld.IsModificationHungarian) : "",
                                          IsModificationColorItalian = (IsCopyLayout == true && ld.Italian != null) ? GetCopyLayoutColor(ld.IsModificationItalian) : "",
                                          IsModificationColorLatvian = (IsCopyLayout == true && ld.Latvian != null) ? GetCopyLayoutColor(ld.IsModificationLatvian) : "",
                                          IsModificationColorLithuanian = (IsCopyLayout == true && ld.Lithuanian != null) ? GetCopyLayoutColor(ld.IsModificationLithuanian) : "",
                                          IsModificationColorMacedonian = (IsCopyLayout == true && ld.Macedonian != null) ? GetCopyLayoutColor(ld.IsModificationMacedonian) : "",
                                          IsModificationColorNorwegian = (IsCopyLayout == true && ld.Norwegian != null) ? GetCopyLayoutColor(ld.IsModificationNorwegian) : "",
                                          IsModificationColorPolish = (IsCopyLayout == true && ld.Polish != null) ? GetCopyLayoutColor(ld.IsModificationPolish) : "",
                                          IsModificationColorPortuguese = (IsCopyLayout == true && ld.Portuguese != null) ? GetCopyLayoutColor(ld.IsModificationPortuguese) : "",
                                          IsModificationColorRomanian = (IsCopyLayout == true && ld.Romanian != null) ? GetCopyLayoutColor(ld.IsModificationRomanian) : "",
                                          IsModificationColorRussian = (IsCopyLayout == true && ld.Russian != null) ? GetCopyLayoutColor(ld.IsModificationRussian) : "",
                                          IsModificationColorSlovak = (IsCopyLayout == true && ld.Slovak != null) ? GetCopyLayoutColor(ld.IsModificationSlovak) : "",
                                          IsModificationColorSlovene = (IsCopyLayout == true && ld.Slovene != null) ? GetCopyLayoutColor(ld.IsModificationSlovene) : "",
                                          IsModificationColorSpanish = (IsCopyLayout == true && ld.Spanish != null) ? GetCopyLayoutColor(ld.IsModificationSpanish) : "",
                                          IsModificationColorSwedish = (IsCopyLayout == true && ld.Swedish != null) ? GetCopyLayoutColor(ld.IsModificationSwedish) : "",
                                          IsModificationColorTurkish = (IsCopyLayout == true && ld.Turkish != null) ? GetCopyLayoutColor(ld.IsModificationTurkish) : "",
                                          IsModificationColorUkrainian = (IsCopyLayout == true && ld.Ukrainian != null) ? GetCopyLayoutColor(ld.IsModificationUkrainian) : "",
                                          IsModificationColorBelarusian = (IsCopyLayout == true && ld.Belarusian != null) ? GetCopyLayoutColor(ld.IsModificationBelarusian) : "",
                                      }).FirstOrDefault();

                    return langdetail;
                }
            }
            catch (Exception ex)
            { throw ex; }
        }
        public List<LanguageMaster> GetLanguages(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<LanguageMaster> returnList;
                    return returnList = (from LM in context.LanguageMaster.ToList()
                                         join US in context.UsedLanguages.ToList().Where(x => x.LayoutRefId == LayoutId).ToList() on LM.LanguageId equals US.LanguageRefId
                                         select new LanguageMaster()
                                         {
                                             LanguageId = LM.LanguageId,
                                             LanguageDescription = LM.LanguageDescription,
                                         }).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool GetLayoutColor(int layoutId)
        {
            using (var context = new Creatis_Context())
            {
                bool layoutColor = context.Layout.Where(x => x.LayoutId == layoutId).Select(y => y.IsCopyLayout).FirstOrDefault();
                return layoutColor;
            }
        }
        public string GetCopyLayoutColor(int IsModification)
        {
            var colorname = "";
            if (IsModification == (int)EnumCopyColorCode.Green)
                colorname = "Green";
            else if (IsModification == (int)EnumCopyColorCode.Yellow)
                colorname = "Yellow";
            else if (IsModification == (int)EnumCopyColorCode.Red)
                colorname = "Red";
            else
                colorname = "Grey";

            return colorname;
        }
        public string GetLangtransProperties(LangDetail langdetail, int launguageId, int QuestionId, string targetCode)
        {

            var LanguageContent = string.Empty;
            string languagename = GetLanguageName(launguageId);

            if (langdetail != null)
            {
                if (LanguagesEnum.English.ToString() == languagename)
                    LanguageContent = langdetail.English;
                else if (LanguagesEnum.Arabic.ToString() == languagename)
                {
                    LanguageContent = langdetail.Arabic;
                }
                else if (LanguagesEnum.Bulgarian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Bulgarian;
                }
                else if (LanguagesEnum.Croatian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Croatian;
                }
                else if (LanguagesEnum.Czech.ToString() == languagename)
                {
                    LanguageContent = langdetail.Czech;
                }
                else if (LanguagesEnum.Danish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Danish;
                }
                else if (LanguagesEnum.Dutch.ToString() == languagename)
                {
                    LanguageContent = langdetail.Dutch;
                }
                else if (LanguagesEnum.Estonian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Estonian;
                }
                else if (LanguagesEnum.Finnish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Finnish;
                }
                else if (LanguagesEnum.French.ToString() == languagename)
                {
                    LanguageContent = langdetail.French;
                }
                else if (LanguagesEnum.German.ToString() == languagename)
                {
                    LanguageContent = langdetail.German;
                }
                else if (LanguagesEnum.Greek.ToString() == languagename)
                {
                    LanguageContent = langdetail.Greek;
                }
                else if (LanguagesEnum.Hungarian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Hungarian;
                }
                else if (LanguagesEnum.Italian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Italian;
                }
                else if (LanguagesEnum.Latvian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Latvian;
                }
                else if (LanguagesEnum.Lithuanian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Lithuanian;
                }
                else if (LanguagesEnum.Macedonian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Macedonian;
                }
                else if (LanguagesEnum.Norwegian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Norwegian;
                }
                else if (LanguagesEnum.Polish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Polish;
                }
                else if (LanguagesEnum.Portuguese.ToString() == languagename)
                {
                    LanguageContent = langdetail.Portuguese;
                }
                else if (LanguagesEnum.Romanian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Romanian;
                }
                else if (LanguagesEnum.Russian.ToString() == languagename)
                {
                    LanguageContent = langdetail.Russian;
                }
                else if (LanguagesEnum.Slovak.ToString() == languagename)
                {
                    LanguageContent = langdetail.Slovak;
                }
                else if (LanguagesEnum.Slovene.ToString() == languagename)
                {
                    LanguageContent = langdetail.Slovene;
                }
                else if (LanguagesEnum.Spanish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Spanish;
                }
                else if (LanguagesEnum.Swedish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Swedish;
                }
                else if (LanguagesEnum.Turkish.ToString() == languagename)
                {
                    LanguageContent = langdetail.Turkish;
                }
                else
                {
                    LanguageContent = "";
                }
                if (LanguageContent == null || LanguageContent == "")
                {
                    using (var context = new Creatis_Context())
                    {

                        //languagename = context.Questions.Where(x => x.QuestionId == QuestionId).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();
                        if (targetCode == "AR")
                            languagename = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == QuestionId).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();
                        else if (targetCode == "Q")
                            languagename = context.Questions.Where(x => x.QuestionId == QuestionId).Select(x => x.LanguageMaster.LanguageDescription).FirstOrDefault();

                        if (LanguagesEnum.English.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.English;
                        }
                        else if (LanguagesEnum.Arabic.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Arabic;
                        }
                        else if (LanguagesEnum.Bulgarian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Bulgarian;
                        }
                        else if (LanguagesEnum.Croatian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Croatian;
                        }
                        else if (LanguagesEnum.Czech.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Czech;
                        }
                        else if (LanguagesEnum.Danish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Danish;
                        }
                        else if (LanguagesEnum.Dutch.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Dutch;
                        }
                        else if (LanguagesEnum.Estonian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Estonian;
                        }
                        else if (LanguagesEnum.Finnish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Finnish;
                        }
                        else if (LanguagesEnum.French.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.French;
                        }
                        else if (LanguagesEnum.German.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.German;
                        }
                        else if (LanguagesEnum.Greek.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Greek;
                        }
                        else if (LanguagesEnum.Hungarian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Hungarian;
                        }
                        else if (LanguagesEnum.Italian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Italian;
                        }
                        else if (LanguagesEnum.Latvian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Latvian;
                        }
                        else if (LanguagesEnum.Lithuanian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Lithuanian;
                        }
                        else if (LanguagesEnum.Macedonian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Macedonian;
                        }
                        else if (LanguagesEnum.Norwegian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Norwegian;
                        }
                        else if (LanguagesEnum.Polish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Polish;
                        }
                        else if (LanguagesEnum.Portuguese.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Portuguese;
                        }
                        else if (LanguagesEnum.Romanian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Romanian;
                        }
                        else if (LanguagesEnum.Russian.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Russian;
                        }
                        else if (LanguagesEnum.Slovak.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Slovak;
                        }
                        else if (LanguagesEnum.Slovene.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Slovene;
                        }
                        else if (LanguagesEnum.Spanish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Spanish;
                        }
                        else if (LanguagesEnum.Swedish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Swedish;
                        }
                        else if (LanguagesEnum.Turkish.ToString() == languagename)
                        {
                            LanguageContent = "@" + langdetail.Turkish;
                        }
                        else
                        {
                            LanguageContent = "@" + langdetail.English;
                        }
                    }
                }
            }
            else
            {
                LanguageContent = "";
            }

            return LanguageContent;
        }
        public string GetPlanChoosingActivites(int planningSelectId)
        {
            string planChoosingActivites = string.Empty;
            StringBuilder activitiesNames = new StringBuilder();
            using (var context = new Creatis_Context())
            {
                var plannigSpecific = context.PlanningselectProperties.Where(x => x.PlanningselectId == planningSelectId).Select(a => a.Planningspecific).FirstOrDefault();
                if (plannigSpecific == "Places of other activity")
                {
                    var activites = context.PlanningselectProperties.Where(x => x.PlanningselectId == planningSelectId).Select(x => x.Chooseactivity).FirstOrDefault().ToString();
                    if (activites != null && activites != "")
                    {
                        string[] activiteIds = activites.Split('-');
                        for (int i = 0; i < activiteIds.Length; i++)
                        {
                            activitiesNames.Append(context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == Convert.ToInt32(activiteIds[i])).Select(x => x.ActivitiesRegistrationName).FirstOrDefault().ToString());
                        }
                    }
                }
            }
            return activitiesNames.ToString();
        }
        public PlanningselectProperties GetPlanningSelectLevel(int planningSelectId)
        {
            PlanningselectProperties objPlanningselectProperties = new PlanningselectProperties();
            using (var context = new Creatis_Context())
            {
                objPlanningselectProperties.Planninglevel = context.PlanningselectProperties.Where(x => x.PlanningselectId == planningSelectId).Select(x => x.Planninglevel).FirstOrDefault();
                objPlanningselectProperties.Other = context.PlanningselectProperties.Where(x => x.PlanningselectId == planningSelectId).Select(x => x.Other).FirstOrDefault();
                objPlanningselectProperties.OtherMaskText = context.PlanningselectProperties.Where(x => x.PlanningselectId == planningSelectId).Select(x => x.OtherMaskText).FirstOrDefault();
                return objPlanningselectProperties;
            }
        }
        public bool GetActivityFluxStaus(int activityRegId)
        {

            using (var context = new Creatis_Context())
            {
                return context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == activityRegId).Select(x => x.Flexactivity).FirstOrDefault();
            }
        }
        public string GetInfoMessageContent(int questionRefId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var languageName = _translatorRepository.GetDefaultLanguage(layoutId);

                    SqlParameter questionParam = new SqlParameter("@QuestionRefId", questionRefId);
                    var infoMessageTranslationDetails = context.InfoMessagePropertiesTranslation.FromSql("GetInfoMessageProperties @QuestionRefId", questionParam).ToList();
                    var infoMessageContentTranslationDetails = context.InfoMessageContentTranslation.FromSql("GetInfoMessagePropertiesMessageContent @QuestionRefId", questionParam).ToList();

                    List<InfomessageProperties> infomessagePropertiesList = new List<InfomessageProperties>();

                    foreach (var infoMessageDetail in infoMessageTranslationDetails)
                    {
                        var messageContent = infoMessageContentTranslationDetails.Where(x => x.InfomessageId == infoMessageDetail.InfomessageId && x.LangRefColTableId == (int)LangRefColTable.InfoMessageContentDetail_MessageContent).FirstOrDefault();

                        InfomessageProperties infoMessage = new InfomessageProperties();
                        infoMessage.Messagecontent = messageContent == null ? "" : _questionPathRepository.GetInfoMessageContentTranslationText(messageContent, languageName).Replace("@<p>", "<p>@");
                        infomessagePropertiesList.Add(infoMessage);
                    }

                    if (infomessagePropertiesList.Count > 0)
                        return (String.IsNullOrEmpty(infomessagePropertiesList.Select(x => x.Messagecontent.Replace("@", "").Trim()).FirstOrDefault()) ? "" : "<br/><b>Message Content :</b><br/>" + infomessagePropertiesList.Select(x => x.Messagecontent).FirstOrDefault().ToString().Replace("<p>", "").Replace("</p>", "<br/>"));
                    else
                        return ("");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetParentPropertyName(int questionRefId)
        {
            string parentPropertyName = string.Empty;
            using (var context = new Creatis_Context())
            {
                parentPropertyName = context.Questions.Where(x => x.QuestionId == questionRefId).Select(a => a.PropertyName).FirstOrDefault();
            }
            return parentPropertyName;
        }

        public PlanningselectProperties GetPlanningSelectOtherContent(int questionRefId)
        {
            //bool isPlanningSelectOtherContent = false;
            using (var context = new Creatis_Context())
            {
                var planningSelectOtherContent = context.PlanningselectProperties.Where(x => x.QuestionRefId == questionRefId).FirstOrDefault();
                //if (planningSelectOtherContent == "No other" || planningSelectOtherContent == "No other Driver entry (A50)" || planningSelectOtherContent == "No other Fixed value" || planningSelectOtherContent == "No other QP (save to DriverCreated)")
                //    isPlanningSelectOtherContent = true;
                //else
                //    isPlanningSelectOtherContent = false;
                return planningSelectOtherContent;
            }

        }
        public List<int> GetActivitiesRegistrationByLayoutId(int LayoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    List<int> returnList = context.ActivitiesRegistration.Where(x => x.LayoutRefId == LayoutId).Select(x => x.ActivitiesRegistrationId).OrderBy(x => x).ToList();
                    return returnList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool GetTrailerOtherContent(int questionRefId)
        {
            bool isTrailerContent = false;
            using (var context = new Creatis_Context())
            {
                var trailerOtherContent = context.TrailerProperties.Where(x => x.QuestionRefId == questionRefId).Select(a => a.Other ?? false).FirstOrDefault();
                if (trailerOtherContent)
                    isTrailerContent = true;
            }
            return isTrailerContent;
        }
        public bool GetVariableListOtherContent(int questionRefId)
        {
            bool isTrailerContent = false;
            using (var context = new Creatis_Context())
            {
                var trailerOtherContent = context.VarListProperties.Where(x => x.QuestionRefId == questionRefId).Select(a => a.Other).FirstOrDefault();
                if (trailerOtherContent == "No other")
                {
                    isTrailerContent = true;
                }
            }
            return isTrailerContent;
        }
        public ActivitiesRegistrationTranslations GetActivityRegistrationTranslations(int activityRegId, int layoutId)
        {
            ActivitiesRegistrationTranslations objActRegTranslation = new ActivitiesRegistrationTranslations();

            using (var context = new Creatis_Context())
            {
                var parentIDDD = string.Empty;
                SqlParameter sqlLayoutId = new SqlParameter("@LayoutRefId", layoutId);
                SqlParameter ActivitiesRegistrationId = new SqlParameter("@ActivityRefId", activityRegId);
                objActRegTranslation = context.ActivitiesRegistrationTranslations.FromSql("GetShowLayoutActivitiesRegistrationTranslations @LayoutRefId,@ActivityRefId ", sqlLayoutId, ActivitiesRegistrationId).FirstOrDefault();
                return objActRegTranslation;
            }
        }
        public List<ShowlayoutInfoTranslations> GetInfoTranslations(int layoutId, int customerId)
        {
            List<ShowlayoutInfoTranslations> objInfoTranslations = new List<ShowlayoutInfoTranslations>();

            using (var context = new Creatis_Context())
            {
                int intIsLoadHtml = 1;
                SqlParameter sqlLayoutId = new SqlParameter("@LayoutRefId", layoutId);
                SqlParameter sqlCustomerId = new SqlParameter("@CustomerRefId", customerId);
                SqlParameter isLoadHtml = new SqlParameter("@IsLoadHtml", intIsLoadHtml);
                objInfoTranslations = context.ShowlayoutInfoTranslations.FromSql("GetShowLayoutInfoTranslations @LayoutRefId,@CustomerRefId,@IsLoadHtml", sqlLayoutId, sqlCustomerId, isLoadHtml).ToList();
                return objInfoTranslations;
            }
        }
        public string GetActivityRegistationName(ActivitiesRegistrationTranslations activitieRegTranslation, string defaultLanguage)
        {
            var questionName = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                questionName = activitieRegTranslation.Belarusian;
            }
            else
            {
                questionName = "";
            }

            if (questionName == null || questionName == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == activitieRegTranslation.LanguageName)
                    {
                        questionName = "@" + activitieRegTranslation.Belarusian;
                    }
                    else
                    {
                        questionName = "@" + activitieRegTranslation.English;
                    }
                }
            }
            return questionName;
        }
        public string GetQuestoinsTranslation(BoShowLayout questionsTranslation, string defaultLanguage)
        {
            var questionName = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Belarusian;
            }
            else
            {
                questionName = "";
            }

            if (questionName == null || questionName == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == questionsTranslation.LanguageName)
                    {
                        questionName = "@" + questionsTranslation.Belarusian;
                    }
                    else
                    {
                        questionName = "@" + questionsTranslation.English;
                    }
                }
            }
            return questionName;
        }
        public string GetInfoTranslationLanguage(ShowlayoutInfoTranslations questionsTranslation, string defaultLanguage, string translationDefaultlanguage)
        {
            var questionName = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                questionName = questionsTranslation.Belarusian;
            }
            else
            {
                questionName = "";
            }

            if (questionName == null || questionName == "")
            {
                using (var context = new Creatis_Context())
                {
                    if (LanguagesEnum.English.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.English;
                    }
                    else if (LanguagesEnum.Arabic.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Arabic;
                    }
                    else if (LanguagesEnum.Bulgarian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Bulgarian;
                    }
                    else if (LanguagesEnum.Croatian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Croatian;
                    }
                    else if (LanguagesEnum.Czech.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Czech;
                    }
                    else if (LanguagesEnum.Danish.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Danish;
                    }
                    else if (LanguagesEnum.Dutch.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Dutch;
                    }
                    else if (LanguagesEnum.Estonian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Estonian;
                    }
                    else if (LanguagesEnum.Finnish.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Finnish;
                    }
                    else if (LanguagesEnum.French.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.French;
                    }
                    else if (LanguagesEnum.German.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.German;
                    }
                    else if (LanguagesEnum.Greek.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Greek;
                    }
                    else if (LanguagesEnum.Hungarian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Hungarian;
                    }
                    else if (LanguagesEnum.Italian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Italian;
                    }
                    else if (LanguagesEnum.Latvian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Latvian;
                    }
                    else if (LanguagesEnum.Lithuanian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Lithuanian;
                    }
                    else if (LanguagesEnum.Macedonian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Macedonian;
                    }
                    else if (LanguagesEnum.Norwegian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Norwegian;
                    }
                    else if (LanguagesEnum.Polish.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Polish;
                    }
                    else if (LanguagesEnum.Portuguese.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Portuguese;
                    }
                    else if (LanguagesEnum.Romanian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Romanian;
                    }
                    else if (LanguagesEnum.Russian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Russian;
                    }
                    else if (LanguagesEnum.Slovak.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Slovak;
                    }
                    else if (LanguagesEnum.Slovene.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Slovene;
                    }
                    else if (LanguagesEnum.Spanish.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Spanish;
                    }
                    else if (LanguagesEnum.Swedish.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Swedish;
                    }
                    else if (LanguagesEnum.Turkish.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Turkish;
                    }
                    else if (LanguagesEnum.Ukrainian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Ukrainian;
                    }
                    else if (LanguagesEnum.Belarusian.ToString() == translationDefaultlanguage)
                    {
                        questionName = "@" + questionsTranslation.Belarusian;
                    }
                    else
                    {
                        questionName = "@" + questionsTranslation.English;
                    }
                }
            }
            return questionName;
        }
        public string GetProposeFixedValue(int entryId, string translatedFIxedValue)
        {
            using (var context = new Creatis_Context())
            {
                string proposeFixedValue = string.Empty;
                var entryResult = context.EntryProperties.Where(x => x.EntryId == entryId).FirstOrDefault();
                if (entryResult.Propose != 0 && entryResult.Propose != null && entryResult.AutoGenerate != 1)
                {
                    proposeFixedValue = ", Propose '" + translatedFIxedValue + "'";
                }
                if (entryResult.Propose != 0 && entryResult.AutoGenerate == 1)
                {
                    proposeFixedValue = ", AutoGenerate  '" + translatedFIxedValue + "'";
                }
                return proposeFixedValue;
            }

        }
        public string GetSmartActivityStaus(int activityRegId, int layoutId)
        {

            using (var context = new Creatis_Context())
            {
                string smartEnabled = string.Empty;
                string layoutType = context.Layout.Where(x => x.LayoutId == layoutId).Select(x => x.LayoutType).FirstOrDefault();
                if (layoutType == "TX-SMART")
                {
                    string planningReferenceType = context.ActivitiesRegistration.Where(x => x.ActivitiesRegistrationId == activityRegId).Select(x => x.PlanningRefId).FirstOrDefault();
                    if (planningReferenceType == "Receive")
                    {
                        smartEnabled = "Plannable";
                    }
                    else if (planningReferenceType == "Use")
                    {
                        smartEnabled = "Show all planning";
                    }
                }
                return smartEnabled;
            }
        }

        public List<boPlanningLevelDrop> PlanningFeedbackLevel(int ActivityRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    string[] PlanningselectPropertiestypes = new string[] { "Trip", "Place", "Job", "Product" };
                    var entryProperties = context.EntryProperties.Where(x => PlanningselectPropertiestypes.Contains(x.Planningfeedback) && x.ActivitiesRefId == ActivityRefId).Select(Y => new boPlanningLevelDrop() { Planningfeedback = Y.Planningfeedback }).Distinct().ToList();
                    var choiceProperties = context.ChioceProperties.Where(x => PlanningselectPropertiestypes.Contains(x.Planningfeedback) && x.ActivitiesRefId == ActivityRefId).Select(Y => new boPlanningLevelDrop() { Planningfeedback = Y.Planningfeedback }).Distinct().ToList();
                    var trailerProperties = context.TrailerProperties.Where(x => PlanningselectPropertiestypes.Contains(x.PlanningFeedback) && x.ActivitiesRefId == ActivityRefId).Select(Y => new boPlanningLevelDrop() { Planningfeedback = Y.PlanningFeedback }).Distinct().ToList();
                    var varListProperties = context.VarListProperties.Where(x => PlanningselectPropertiestypes.Contains(x.PlanningFeedback) && x.ActivitiesRefId == ActivityRefId).Select(Y => new boPlanningLevelDrop() { Planningfeedback = Y.PlanningFeedback }).Distinct().ToList();
                    var planningSelect = context.PlanningselectProperties.Where(x => PlanningselectPropertiestypes.Contains(x.Planninglevel) && x.ActivitiesRefId == ActivityRefId).Select(Y => new boPlanningLevelDrop() { Planningfeedback = Y.Planninglevel }).Distinct().ToList();
                    var transportType = context.TransportTypeProperties.Where(x => PlanningselectPropertiestypes.Contains(x.PlanningFeedback) && x.ActivitiesRefId == ActivityRefId).Select(Y => new boPlanningLevelDrop() { Planningfeedback = Y.PlanningFeedback }).Distinct().ToList();
                    var data = entryProperties.Union(planningSelect).Union(choiceProperties).Union(trailerProperties).Union(trailerProperties);
                    List<boPlanningLevelDrop> returnList;
                    return returnList = data.OrderByDescending(x => x.Planningfeedback == "Trip").OrderByDescending(x => x.Planningfeedback == "Place").OrderByDescending(x => x.Planningfeedback == "Job").OrderByDescending(x => x.Planningfeedback == "Product").ToList();
                    //return returnList = data.ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PlanningType> GetPlanningTypeSL(int Layout_ID)
        {
            try
            {
                using (var context = new Creatis_Context())
                {

                    var data = (from PT in context.PlanningType
                                join PTM in context.PlanningTypeMaster on PT.PlanningTypeRefId equals PTM.PlanningTypeMasterId
                                where PT.LayoutRefId == Layout_ID
                                select new PlanningType()
                                {
                                    PlanningTypeId = PT.PlanningTypeId,
                                    CustomerRefId = PT.CustomerRefId,
                                    LayoutRefId = PT.LayoutRefId,
                                    PlanningTypeRefId = PT.PlanningTypeRefId,
                                    CreatedBy = PT.CreatedBy,
                                    CreatedOn = PT.CreatedOn,
                                    UpdatedBy = PT.UpdatedBy,
                                    UpdatedOn = PT.UpdatedOn,
                                    IsModification=PT.IsModification,
                                    PlanningTypeName = PTM.PlanningTypeMasterDescription
                                }).ToList();

                    List<PlanningType> returnList;
                    return returnList = data.OrderByDescending(x => x.PlanningTypeName == "Product").OrderByDescending(x => x.PlanningTypeName == "Job").OrderByDescending(x => x.PlanningTypeName == "Place").OrderByDescending(x => x.PlanningTypeName == "Trip").ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}

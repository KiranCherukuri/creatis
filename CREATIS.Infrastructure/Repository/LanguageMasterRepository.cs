﻿using CREATIS.ApplicationCore.Modals;
using CREATIS.Infrastructure.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CREATIS.Infrastructure.Repository
{
    public class LanguageMasterRepository : ILanguageMasterRepository
    {
        public Creatis_Context dbContext;
        public String SaveLanguageMaster(LanguageMaster langModel)
        {
            var result = "";
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (langModel.LanguageId > 0)
                    {
                        var target = context.LanguageMaster.Where(x => x.LanguageId == langModel.LanguageId).FirstOrDefault();
                        if (target != null)
                        {
                            target.LanguageDescription = langModel.LanguageDescription;
                            target.LanguageCode = langModel.LanguageCode;
                            target.IS_Active = langModel.IS_Active;
                            target.UpdatedBy = langModel.UpdatedBy;
                            target.UpdatedOn = langModel.UpdatedOn;
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        langModel.LanguageDescription = langModel.LanguageDescription;
                        langModel.LanguageCode = langModel.LanguageCode;
                        langModel.IS_Active = langModel.IS_Active;
                        langModel.CreatedBy = langModel.CreatedBy;
                        langModel.CreatedOn = DateTime.Now;
                        context.LanguageMaster.Add(langModel);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public List<LanguageMaster> GetLanguageMaster()
        {
            List<LanguageMaster> langList;
            try
            {
                using (var context = new Creatis_Context())
                {
                    langList = context.LanguageMaster.Select(x => new LanguageMaster()
                    {
                        LanguageId = x.LanguageId,
                        LanguageDescription = x.LanguageDescription,
                        Active = (x.IS_Active == 1 ? "Active" : "InActive"),
                        LanguageCode = x.LanguageCode
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return langList;
        }
    }
}

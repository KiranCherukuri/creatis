﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using CREATIS.Infrastructure.IRepository;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using System.Linq;
using static CREATIS.Infrastructure.Utility;

namespace CREATIS.Infrastructure.Repository
{
    public class TooltipRepository : ITooltipRepository
    {
        public readonly ITranslatorRepository _translatorRepository;
        public TooltipRepository(ITranslatorRepository translatorRepository)
        {
            this._translatorRepository = translatorRepository;
        }
        public List<Tooltip> GetTooltip()
        {
            List<Tooltip> tooltipList = new List<Tooltip>();
            try
            {
                using (var context = new Creatis_Context())
                {
                    var botooltipList = context.BOTooltip.FromSql("GetTooltip").ToList();
                    foreach (var tooltipinfo in botooltipList)
                    {
                        Tooltip tooltip = new Tooltip();

                        tooltip.TooltipId = tooltipinfo.TooltipId;
                        tooltip.UITranslationRefId = tooltipinfo.UITranslationRefId;
                        tooltip.WizardTooltips = tooltipinfo.WizardTooltips;
                        tooltip.ExtendedHelp = tooltipinfo.ExtendedHelp;
                        tooltip.PropertyName = tooltipinfo.PropertyName;
                        tooltip.Title = tooltipinfo.Title;
                        tooltip.UpdatedOn = tooltipinfo.UpdatedOn;

                        tooltipList.Add(tooltip);
                    }

                    return tooltipList.OrderByDescending(x => x.UpdatedOn).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<Tooltip>();
            }

        }
        public List<WizardTooltipTransation> GetWizardTooltip()
        {
            List<WizardTooltipTransation> tooltipList;
            try
            {
                using (var context = new Creatis_Context())
                {
                    tooltipList = context.WizardTooltipTransation.FromSql("GetWizardTooltipTransation").ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<WizardTooltipTransation>();
            }

            return tooltipList;
        }
        public List<ExtendedTooltipTransation> GetExtendedTooltip()
        {
            List<ExtendedTooltipTransation> tooltipList;
            try
            {
                using (var context = new Creatis_Context())
                {
                    tooltipList = context.ExtendedTooltipTransation.FromSql("GetExtendedTooltipTransation").ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ExtendedTooltipTransation>();
            }

            return tooltipList;
        }
        public int SaveTooltipImage(TooltipImage tooltipImageInfo)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (tooltipImageInfo.TooltipImageId > 0)
                    {
                        var tooltipImageTemp = context.TooltipImage.FirstOrDefault(x => x.TooltipImageId == tooltipImageInfo.TooltipImageId);
                        tooltipImageTemp.Title = tooltipImageInfo.Title;
                        tooltipImageTemp.TooltipRefId = tooltipImageInfo.TooltipRefId;
                        tooltipImageTemp.UpdatedBy = tooltipImageInfo.UpdatedBy;
                        tooltipImageTemp.UpdatedOn = tooltipImageInfo.UpdatedOn;
                        context.SaveChanges();
                    }
                    else
                    {
                        tooltipImageInfo.Title = "";
                        tooltipImageInfo.TooltipRefId = 0;
                        context.TooltipImage.Add(tooltipImageInfo);
                        context.SaveChanges();
                    }

                    return tooltipImageInfo.TooltipImageId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public List<TooltipImage> GetTooltipImage(int tooltipRefId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var tooltipImageList = context.TooltipImage.Where(x => x.TooltipRefId == tooltipRefId).ToList();
                    return tooltipImageList.OrderByDescending(x => x.CreatedOn).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<TooltipImage>();
            }

        }
        public List<TooltipImage> DeleteTooltipImage(int tooltipImageId, int tooltipId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var tooltipImage = context.TooltipImage.FirstOrDefault(x => x.TooltipImageId == tooltipImageId);
                    if (tooltipImage != null)
                    {
                        context.TooltipImage.Remove(tooltipImage);
                        context.SaveChanges();
                    }

                    var tooltipImageList = context.TooltipImage.Where(x => x.TooltipRefId == tooltipId).ToList();
                    return tooltipImageList.OrderByDescending(x => x.CreatedOn).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<TooltipImage>();
            }

        }
        public int UpdateTooltip(Tooltip tooltip)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (tooltip.TooltipId > 0)
                    {
                        var tooltipmodal = context.Tooltip.FirstOrDefault(x => x.TooltipId == tooltip.TooltipId);
                        if (tooltipmodal != null)
                        {
                            tooltipmodal.ExtendedHelp = tooltip.ExtendedHelp;
                            tooltipmodal.WizardTooltips = tooltip.WizardTooltips;
                            tooltipmodal.UpdatedBy = tooltip.UpdatedBy;
                            tooltipmodal.UpdatedOn = tooltip.UpdatedOn;
                            context.SaveChanges();

                            var wizardTooltipTransaltion = context.TooltipTranslation.FirstOrDefault(x => x.TooltipRefId == tooltip.TooltipId && x.TooltipTypeId == 1);
                            if (wizardTooltipTransaltion != null)
                            {
                                wizardTooltipTransaltion.English = tooltip.WizardTooltips;
                                wizardTooltipTransaltion.UpdatedBy = tooltip.UpdatedBy;
                                wizardTooltipTransaltion.UpdatedOn = tooltip.UpdatedOn;
                                context.SaveChanges();
                            }

                            if(tooltip.ExtendedHelp.Length > 0)
                            {
                                var extendedHelpTransaltion = context.TooltipTranslation.FirstOrDefault(x => x.TooltipRefId == tooltip.TooltipId && x.TooltipTypeId == 2);
                                if (extendedHelpTransaltion != null)
                                {
                                    extendedHelpTransaltion.English = tooltip.ExtendedHelp;
                                    extendedHelpTransaltion.UpdatedBy = tooltip.UpdatedBy;
                                    extendedHelpTransaltion.UpdatedOn = tooltip.UpdatedOn;
                                    context.SaveChanges();
                                }
                                else
                                {
                                    TooltipTranslation extendedHelp = new TooltipTranslation();
                                    extendedHelp.TooltipTypeId = 2;
                                    extendedHelp.TooltipRefId = tooltip.TooltipId;
                                    extendedHelp.English = tooltip.ExtendedHelp;
                                    extendedHelp.CreatedBy = tooltip.UpdatedBy;
                                    extendedHelp.CreatedOn = DateTime.Now;
                                    extendedHelp.UpdatedBy = tooltip.UpdatedBy;
                                    extendedHelp.UpdatedOn = DateTime.Now;
                                    context.TooltipTranslation.Add(extendedHelp);
                                    context.SaveChanges();
                                }
                            }
                        }
                    }
                    return tooltip.TooltipId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public string SaveTooltipTranslation(TooltipTranslation tooltipTranslation)
        {
            var result = "success";
            try
            {
                using (var context = new Creatis_Context())
                {
                    var target = context.TooltipTranslation.FirstOrDefault(x => x.TooltipTranslationId == tooltipTranslation.TooltipTranslationId);
                    if (target != null)
                    {
                        target.Arabic = tooltipTranslation.Arabic;
                        target.Bulgarian = tooltipTranslation.Bulgarian;
                        target.Croatian = tooltipTranslation.Croatian;
                        target.Czech = tooltipTranslation.Czech;
                        target.Danish = tooltipTranslation.Danish;
                        target.Dutch = tooltipTranslation.Dutch;
                        target.Estonian = tooltipTranslation.Estonian;
                        target.Finnish = tooltipTranslation.Finnish;
                        target.French = tooltipTranslation.French;
                        target.German = tooltipTranslation.German;
                        target.Greek = tooltipTranslation.Greek;
                        target.Hungarian = tooltipTranslation.Hungarian;
                        target.Italian = tooltipTranslation.Italian;
                        target.Latvian = tooltipTranslation.Latvian;
                        target.Lithuanian = tooltipTranslation.Lithuanian;
                        target.Macedonian = tooltipTranslation.Macedonian;
                        target.Norwegian = tooltipTranslation.Norwegian;
                        target.Polish = tooltipTranslation.Polish;
                        target.Portuguese = tooltipTranslation.Portuguese;
                        target.Romanian = tooltipTranslation.Romanian;
                        target.Russian = tooltipTranslation.Russian;
                        target.Slovak = tooltipTranslation.Slovak;
                        target.Slovene = tooltipTranslation.Slovene;
                        target.Spanish = tooltipTranslation.Spanish;
                        target.Swedish = tooltipTranslation.Swedish;
                        target.Turkish = tooltipTranslation.Turkish;
                        target.Ukrainian = tooltipTranslation.Ukrainian;
                        target.Belarusian = tooltipTranslation.Belarusian;
                        target.UpdatedBy = tooltipTranslation.UpdatedBy;
                        target.UpdatedOn = tooltipTranslation.UpdatedOn;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public string GetExtendedHelpText(int uiId, int layoutId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);
                    var tooltipId = context.Tooltip.Where(x => x.UITranslationRefId == uiId).Select(x => x.TooltipId).FirstOrDefault();
                    var tooltipTranslation = context.TooltipTranslation.FirstOrDefault(x => x.TooltipRefId == tooltipId && x.TooltipTypeId == 2);
                    return ExtendedHelpText(tooltipTranslation, defualtLanguage);
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string ExtendedHelpText(TooltipTranslation tooltipTranslation, string defaultLanguage)
        {
            var translationText = string.Empty;

            if (LanguagesEnum.English.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.English;
            }
            else if (LanguagesEnum.Arabic.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Arabic;
            }
            else if (LanguagesEnum.Bulgarian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Bulgarian;
            }
            else if (LanguagesEnum.Croatian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Croatian;
            }
            else if (LanguagesEnum.Czech.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Czech;
            }
            else if (LanguagesEnum.Danish.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Danish;
            }
            else if (LanguagesEnum.Dutch.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Dutch;
            }
            else if (LanguagesEnum.Estonian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Estonian;
            }
            else if (LanguagesEnum.Finnish.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Finnish;
            }
            else if (LanguagesEnum.French.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.French;
            }
            else if (LanguagesEnum.German.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.German;
            }
            else if (LanguagesEnum.Greek.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Greek;
            }
            else if (LanguagesEnum.Hungarian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Hungarian;
            }
            else if (LanguagesEnum.Italian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Italian;
            }
            else if (LanguagesEnum.Latvian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Latvian;
            }
            else if (LanguagesEnum.Lithuanian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Lithuanian;
            }
            else if (LanguagesEnum.Macedonian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Macedonian;
            }
            else if (LanguagesEnum.Norwegian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Norwegian;
            }
            else if (LanguagesEnum.Polish.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Polish;
            }
            else if (LanguagesEnum.Portuguese.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Portuguese;
            }
            else if (LanguagesEnum.Romanian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Romanian;
            }
            else if (LanguagesEnum.Russian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Russian;
            }
            else if (LanguagesEnum.Slovak.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Slovak;
            }
            else if (LanguagesEnum.Slovene.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Slovene;
            }
            else if (LanguagesEnum.Spanish.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Spanish;
            }
            else if (LanguagesEnum.Swedish.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Swedish;
            }
            else if (LanguagesEnum.Turkish.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Turkish;
            }
            else if (LanguagesEnum.Ukrainian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Ukrainian;
            }
            else if (LanguagesEnum.Belarusian.ToString() == defaultLanguage)
            {
                translationText = tooltipTranslation.Belarusian;
            }
            else
            {
                translationText = "";
            }

            if (translationText == null || translationText == "")
            {
                translationText = tooltipTranslation.English;
            }

            return translationText;
        }
        public List<TooltipImage> GetExtendedHelpTooltipImage(int uiId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var tooltipRefId = context.Tooltip.Where(x => x.UITranslationRefId == uiId).Select(x => x.TooltipId).FirstOrDefault();
                    var tooltipImageList = context.TooltipImage.Where(x => x.TooltipRefId == tooltipRefId).ToList();
                    return tooltipImageList.OrderByDescending(x => x.CreatedOn).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<TooltipImage>();
            }
        }

        public List<Tooltip> GetTooltipList(int layoutId)
        {
            List<Tooltip> tooltipList = new List<Tooltip>();
            try
            {
                using (var context = new Creatis_Context())
                {
                    var defualtLanguage = _translatorRepository.GetDefaultLanguage(layoutId);
                    var botooltipList = context.BOTooltip.FromSql("GetTooltip").ToList();
                    var tooltipTranslationList = context.TooltipTranslation.Where(x => x.TooltipTypeId == 1).ToList();
                    foreach (var tooltipinfo in botooltipList)
                    {
                        var tooltipTranslation = tooltipTranslationList.FirstOrDefault(x => x.TooltipRefId == tooltipinfo.TooltipId);
                        if(tooltipTranslation != null)
                        {
                            Tooltip tooltip = new Tooltip();
                            tooltip.UITranslationRefId = tooltipinfo.UITranslationRefId;
                            tooltip.PropertyName = tooltipinfo.PropertyName;
                            tooltip.WizardTooltips = ExtendedHelpText(tooltipTranslation, defualtLanguage);
                            tooltip.IsExtendedHelp = (tooltipinfo.ExtendedHelp ?? "").Length > 0;
                            tooltipList.Add(tooltip);
                        }
                    }

                    return tooltipList.OrderBy(x => x.UITranslationRefId).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<Tooltip>();
            }
        }

        public string GetTooltipTitle(int uiId)
        {
            try
            {
                using (var context = new Creatis_Context())
                {
                    var result = string.Empty;
                    var uiTranslationInfo = context.UITranslation.Where(x => x.UITranslationId == uiId).Select(x => new { x.ParentId, x.Name }).FirstOrDefault();
                    if (uiTranslationInfo != null)
                        result = context.UITranslation.Where(x => x.UITranslationId == uiTranslationInfo.ParentId).Select(x => x.Name).FirstOrDefault() + ", " + uiTranslationInfo.Name;
                    return result;
                }
            }
            catch (Exception ex)
            {
                return "-";
            }
        }
    }
}

﻿using CREATIS.ApplicationCore.Modals;
using CREATIS.Infrastructure.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CREATIS.Infrastructure.Repository
{
    public class InputMasterRepository : IInputMasterRepository
    {
        public Creatis_Context dbContext;
        public String SaveInputMaster(InputMaster inputModel)
        {
            var result = "";
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (inputModel.Id > 0)
                    {
                        var target = context.InputMaster.Where(x => x.Id == inputModel.Id).FirstOrDefault();
                        if (target != null)
                        {
                            target.MaxLength = inputModel.MaxLength;
                            target.UpdatedBy = inputModel.UpdatedBy;
                            target.UpdatedOn = DateTime.Now;
                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public List<InputMaster> GetInputMaster()
        {
            List<InputMaster> inList;
            try
            {
                using (var context = new Creatis_Context())
                {
                    inList = context.InputMaster.Select(x => new InputMaster()
                    {
                        Id = x.Id,
                        ModuleName = x.ModuleName,
                        StatusName = (x.Status == 1 ? "Active" : "InActive"),
                        ControlName = x.ControlName,
                        MaxLength = x.MaxLength
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return inList;
        }
    }
}

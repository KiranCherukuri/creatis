﻿using CREATIS.ApplicationCore.Modals;
using CREATIS.Infrastructure.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CREATIS.Infrastructure.Repository
{
    public class DocumentTypeMasterRepository : IDocumentTypeMasterRepository
    {
        public Creatis_Context dbContext;
        public String SaveDocumentTypeMaster(DocumentTypeMaster DocumentTypeModel)
        {
            var result = "";
            try
            {
                using (var context = new Creatis_Context())
                {
                    if (DocumentTypeModel.DocumentTypeId > 0)
                    {
                        var target = context.DocumentTypeMaster.Where(x => x.DocumentTypeId == DocumentTypeModel.DocumentTypeId).FirstOrDefault();
                        if (target != null)
                        {
                            target.DocumentTypeCode = DocumentTypeModel.DocumentTypeCode;
                            target.DocumentTypeDescription = DocumentTypeModel.DocumentTypeDescription;
                            target.IsActive = DocumentTypeModel.IsActive;
                            target.UpdatedBy = DocumentTypeModel.UpdatedBy;
                            target.UpdatedOn = DocumentTypeModel.UpdatedOn;
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        context.DocumentTypeMaster.Add(DocumentTypeModel);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public List<DocumentTypeMaster> GetDocumentTypeMaster()
        {
            List<DocumentTypeMaster> DocumentTypeList;
            try
            {
                using (var context = new Creatis_Context())
                {
                    DocumentTypeList = context.DocumentTypeMaster.Select(x => new DocumentTypeMaster()
                    {
                        DocumentTypeId = x.DocumentTypeId,
                        DocumentTypeCode = x.DocumentTypeCode,
                        DocumentTypeDescription = x.DocumentTypeDescription,
                        Active = (x.IsActive == 1 ? "Active" : "InActive")
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DocumentTypeList;
        }
    }
}

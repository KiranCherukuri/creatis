﻿using System;
using System.Collections.Generic;
using System.Text;
using CREATIS.ApplicationCore.Modals;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Extensions.Configuration;
using CREATIS.ApplicationCore.BusinessObjects;

namespace CREATIS.Infrastructure
{
    public class Creatis_Context : DbContext
    {
        public Creatis_Context()
        {
        }
        public Creatis_Context(DbContextOptions options) : base(options)
        { }
        public DbSet<Users> Users { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<RoleMapping> RoleMapping { get; set; }
        public DbSet<Modules> Modules { get; set; }
        public DbSet<ModulesDetail> ModulesDetail { get; set; }
        public DbSet<RoleAccessDetail> RoleAccessDetail { get; set; }
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Layout> Layout { get; set; }
        public DbSet<LayoutNotes> LayoutNotes { get; set; }
        public DbSet<RecentActivity> RecentActivity { get; set; }
        public DbSet<CustomerNotes> CustomerNote { get; set; }
        public DbSet<InfoColumns> InfoColumn { get; set; }
        public DbSet<InfoColumnsMaster> InfoColumnMaster { get; set; }
        public DbSet<Integrator> Integrator { get; set; }
        public DbSet<StatusMaster> StatusMaster { get; set; }
        public DbSet<LanguageMaster> LanguageMaster { get; set; }
        public DbSet<UsedLanguages> UsedLanguages { get; set; }
        public DbSet<OBCTypeMaster> OBCTypeMaster { get; set; }
        public DbSet<OBCType> OBCType { get; set; }
        public DbSet<TransportTypes> TransportTypes { get; set; }
        public DbSet<PlanningTypeMaster> PlanningTypeMaster { get; set; }
        public DbSet<PlanningType> PlanningType { get; set; }
        public DbSet<PlanningSpecificationMaster> PlanningSpecificationMaster { get; set; }
        public DbSet<PlanningSpecification> PlanningSpecification { get; set; }
        public DbSet<AllowancesMaster> AllowancesMaster { get; set; }
        public DbSet<Allowances> Allowances { get; set; }
        public DbSet<WorkingCode> WorkingCode { get; set; }
        public DbSet<Planning> Planning { get; set; }
        public DbSet<PlanningOverview> PlanningOverview { get; set; }
        public DbSet<Specific> Specific { get; set; }
        public DbSet<PTO> PTO { get; set; }
        public DbSet<InterruptibleBy> InterruptibleBy { get; set; }
        public DbSet<ActivitiesRegistration> ActivitiesRegistration { get; set; }
        public DbSet<SetActionNextViewValues> SetActionNextViewValues { get; set; }
        public DbSet<DefaultActivities> DefaultActivities { get; set; }
        public DbSet<ScreenLayout> ScreenLayout { get; set; }
        public DbSet<MainToolBox> MainToolBox { get; set; }
        public DbSet<ToolBoxSubItem> ToolBoxSubItem { get; set; }
        public DbSet<ToolBoxIcons> ToolBoxIcons { get; set; }
        public DbSet<PropertyMaster> PropertyMaster { get; set; }
        public DbSet<PropertyDetails> PropertyDetails { get; set; }
        public DbSet<Questions> Questions { get; set; }
        public DbSet<EntryProperties> EntryProperties { get; set; }
        public DbSet<UIContentLanguage> UIContentLanguage { get; set; }
        public DbSet<UserLanguage> UserLanguage { get; set; }
        public DbSet<PageMaster> PageMaster { get; set; }
        public DbSet<UIContentMaster> UIContentMaster { get; set; }
        public DbSet<AddChioceProperties> AddChioceProperties { get; set; }
        public DbSet<CheckoffProperties> CheckoffProperties { get; set; }
        public DbSet<ChioceProperties> ChioceProperties { get; set; }
        public DbSet<DocumetScanProperties> DocumetScanProperties { get; set; }
        public DbSet<InfomessageProperties> InfomessageProperties { get; set; }
        public DbSet<PlanningselectProperties> PlanningselectProperties { get; set; }
        public DbSet<TextMessageProperties> TextMessageProperties { get; set; }
        public DbSet<VariableListProperties> VariableListProperties { get; set; }
        public DbSet<SetActionValueProperties> SetActionValueProperties { get; set; }
        public DbSet<LangDetail> LangDetail { get; set; }
        public DbSet<LangRefDetail> LangRefDetail { get; set; }
        public DbSet<LangRefColTable> LangRefColTable { get; set; }
        public DbSet<LangRefTable> LangRefTable { get; set; }
        public DbSet<conditionCompareTo> ConditionCompareTo { get; set; }
        public DbSet<conditionCompareMethod> ConditionCompareMethod { get; set; }
        public DbSet<conditionvalue> Conditionvalue { get; set; }
        public DbSet<AliasNameDetails> AliasNameDetails { get; set; }
        public DbSet<InfoColumnAction> InfoColumnAction { get; set; }
        public DbSet<ConditionItem> ConditionItem { get; set; }
        public DbSet<ConditionalProperties> ConditionalProperties { get; set; }
        public DbSet<LayoutStatus> LayoutStatus { get; set; }
        public DbSet<PSLoadedItem> PSLoadedItem { get; set; }
        public DbSet<ReadoutValue> ReadoutValue { get; set; }
        public DbSet<ConPSCompareto> ConPSCompareto { get; set; }
        public DbSet<ConFilter> ConFilter { get; set; }
        public DbSet<Signatures> Signatures { get; set; }
        public DbSet<Problems> Problems { get; set; }
        public DbSet<Pallets> Pallets { get; set; }
        public DbSet<ExtraInfo> ExtraInfo { get; set; }
        public DbSet<Picure> Picure { get; set; }
        public DbSet<Flex> Flex { get; set; }
        public DbSet<InputMaster> InputMaster { get; set; }
        public DbSet<SetActionValueType> SetActionValueType { get; set; }
        public DbSet<ServerEnvironment> ServerEnvironment { get; set; }
        public DbSet<UserEnvironment> UserEnvironment { get; set; }
        public DbSet<SignaturesProperties> SignaturesProperties { get; set; }
        public DbSet<ProblemsProperties> ProblemsProperties { get; set; }
        public DbSet<PalletsProperties> PalletsProperties { get; set; }
        public DbSet<ExtraInfoProperties> ExtraInfoProperties { get; set; }
        public DbSet<DocumentScannerProperties> DocumentScannerProperties { get; set; }
        public DbSet<DocumentScanner> DocumentScanner { get; set; }
        public DbSet<SmartProblems> SmartProblems { get; set; }
        public DbSet<SmartPallets> SmartPallets { get; set; }
        public DbSet<SmartExtraInfo> SmartExtraInfo { get; set; }
        public DbSet<CopyPaste> CopyPaste { get; set; }
        public DbSet<DocumentTypeMaster> DocumentTypeMaster { get; set; }
        public DbSet<DocumentScanTitle> DocumentScanTitle { get; set; }
        public DbSet<DocumentScanType> DocumentScanType { get; set; }
        public DbSet<DocumentTypeFlex> DocumentTypeFlex { get; set; }
        public DbSet<ReadoutDetail> ReadoutDetail { get; set; }
        public DbSet<PlanningSelectReadoutDetail> PlanningSelectReadoutDetail { get; set; }
        public DbSet<PictureProperties> PictureProperties { get; set; }
        public DbSet<InfoMessageIconType> InfoMessageIconType { get; set; }
        public DbSet<InfoMessageContentDetail> InfoMessageContentDetail { get; set; }
        public DbSet<LayoutStatusAccessDetail> LayoutStatusAccessDetail { get; set; }
        public DbSet<TransportTypeProperties> TransportTypeProperties { get; set; }
        public DbSet<TransportTypePropertiesDetail> TransportTypePropertiesDetail { get; set; }
        public DbSet<TrailerProperties> TrailerProperties { get; set; }
        public DbSet<RoleWithLayoutStatusAccessDetail> RoleWithLayoutStatusAccessDetail { get; set; }
        public DbSet<QuestionsTranslation> QuestionsTranslation { get; set; }
        public DbSet<Translations> Translations { get; set; }
        public DbSet<UITranslationDetail> UITranslationDetail { get; set; }
        public DbSet<UITranslation> UITranslation { get; set; }
        public DbSet<UILangDetail> UILangDetail { get; set; }
        public DbSet<ShowLayout> ShowLayout { get; set; }
        public DbSet<ActivitiesRegistrationTranslations> ActivitiesRegistrationTranslations { get; set; }
        public DbSet<ShowlayoutInfoTranslations> ShowlayoutInfoTranslations { get; set; }
        public DbSet<ToolBoxItems> ToolBoxItems { get; set; }
        public DbSet<AddChiocePropertyTranslations> AddChiocePropertyTranslations { get; set; }
        public DbSet<UserRecentActivities> UserRecentActivities { get; set; }
        public DbSet<GetSystemValues> GetSystemValues { get; set; }
        public DbSet<GetSystemValuesFormat> GetSystemValuesFormat { get; set; }
        public DbSet<GetSystemValuesProperties> GetSystemValuesProperties { get; set; }
        public DbSet<ActivitiesRegistrationTranslation> ActivitiesRegistrationTranslation { get; set; }
        public DbSet<FlexTranslation> FlexTranslation { get; set; }
        public DbSet<FlexAlternativeTextTranslation> FlexAlternativeTextTranslation { get; set; }
        public DbSet<FlexDetailTranslation> FlexDetailTranslation { get; set; }
        public DbSet<FlexDetailMaskTextTranslation> FlexDetailMaskTextTranslation { get; set; }
        public DbSet<FlexDetailPalletsTranslation> FlexDetailPalletsTranslation { get; set; }
        public DbSet<FlexDetailDocumentScannerTranslation> FlexDetailDocumentScannerTranslation { get; set; }
        public DbSet<BOAllowances> BOAllowances { get; set; }
        public DbSet<CustomerOverviewDetail> CustomerOverviewDetail { get; set; }
        public DbSet<InfocolumnList> InfocolumnList { get; set; }
        public DbSet<InfoMessagePropertiesTranslation> InfoMessagePropertiesTranslation { get; set; }
        public DbSet<InfoMessageContentTranslation> InfoMessageContentTranslation { get; set; }
        public DbSet<GeneralTranslation> GeneralTranslation { get; set; }
        public DbSet<VarListProperties> VarListProperties { get; set; }
        public DbSet<CommentDetail> CommentDetail { get; set; }
        public DbSet<VarListCommentReadoutDetail> VarListCommentReadoutDetail { get; set; }
        public DbSet<FilterDetail> FilterDetail { get; set; }
        public DbSet<VarListFilterDetail> VarListFilterDetail { get; set; }
        public DbSet<VarListPropertiesOtherTranslation> VarListPropertiesOtherTranslation { get; set; }
        public DbSet<VarListPropertiesTranslation> VarListPropertiesTranslation { get; set; }
        public DbSet<AlertCopiedLayout> AlertCopiedLayout { get; set; }
        public DbSet<PlanningAndPartnerCodeOverview> PlanningAndPartnerCodeOverview { get; set; }
        public DbSet<SavedValues> SavedValues { get; set; }
        public DbSet<ChoicePropertiesTranslation> ChoicePropertiesTranslation { get; set; }
        public DbSet<EntryPropertiesTranslation> EntryPropertiesTranslation { get; set; }
        public DbSet<EntryPropertiesMaskTextTranslation> EntryPropertiesMaskTextTranslation { get; set; }
        public DbSet<TrailerPropertiesTranslation> TrailerPropertiesTranslation { get; set; }
        public DbSet<TrailerPropertiesMaskTextTranslation> TrailerPropertiesMaskTextTranslation { get; set; }
        public DbSet<PlanningSelectPropertiesTranslation> PlanningSelectPropertiesTranslation { get; set; }
        public DbSet<PlanningSelectPropertiesOtherTranslation> PlanningSelectPropertiesOtherTranslation { get; set; }
        public DbSet<TextMessagePropertiesTranslation> TextMessagePropertiesTranslation { get; set; }
        public DbSet<SetActionValuePropertiesTranslation> SetActionValuePropertiesTranslation { get; set; }
        public DbSet<SetActionValuePropertiesInfoCoumnFixedTextTranslation> SetActionValuePropertiesInfoCoumnFixedTextTranslation { get; set; }
        public DbSet<DocumetScanPropertiesTranslation> DocumetScanPropertiesTranslation { get; set; }
        public DbSet<TransportTypePropertiesTranslation> TransportTypePropertiesTranslation { get; set; }
        public DbSet<TransportTypePropertiesDetailTranslation> TransportTypePropertiesDetailTranslation { get; set; }
        public DbSet<CustomerSystemType> CustomerSystemType { get; set; }
        public DbSet<Tooltip> Tooltip { get; set; }
        public DbSet<TooltipTranslation> TooltipTranslation { get; set; }
        public DbSet<TooltipImage> TooltipImage { get; set; }
        public DbSet<ExtendedTooltipTransation> ExtendedTooltipTransation { get; set; }
        public DbSet<WizardTooltipTransation> WizardTooltipTransation { get; set; }
        public DbSet<BOTooltip> BOTooltip { get; set; }
        public DbSet<BOPlanningType> BOPlanningType { get; set; }
        public DbSet<LayoutOverviewValues> OverallLayoutValues { get; set; }
        public virtual void Commit()
        {
            base.SaveChanges();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DB_Context"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.Infrastructure
{
    public abstract class Utility
    {
        public enum LanguagesEnum
        {
            English = 1,
            Arabic = 2,
            Bulgarian = 3,
            Croatian = 4,
            Czech = 5,
            Danish = 6,
            Dutch = 7,
            Estonian = 8,
            Finnish = 9,
            French = 10,
            German = 11,
            Greek = 12,
            Hungarian = 13,
            Italian = 14,
            Latvian = 15,
            Lithuanian = 16,
            Macedonian = 17,
            Norwegian = 18,
            Polish = 19,
            Portuguese = 20,
            Romanian = 21,
            Russian = 22,
            Slovak = 23,
            Slovene = 24,
            Spanish = 25,
            Swedish = 26,
            Turkish = 27,
            Ukrainian = 28,
            Belarusian = 29
        }
        public enum LangRefTable
        {
            ActivitiesRegistration = 1,
            InfoColumns = 2,
            Allowances = 3,
            ChioceProperties = 4,
            AddChioceProperties = 5,
            EntryProperties = 6,
            InfomessageProperties = 7,
            TextMessageProperties = 8,
            VariableListProperties = 9,
            PlanningselectProperties = 10,
            DocumetScanProperties = 11,
            InfoMessageContentDetail = 12,
            TransportTypeProperties = 13,
            TransportTypePropertiesDetail = 14,
            TrailerProperties = 15,
            Questions = 16,
            SetActionValueProperties = 17,
            ExtraInfoProperties = 18,
            ExtraInfo = 19,
            PalletsProperties = 20,
            Pallets = 21,
            ProblemsProperties = 22,
            Problems = 23,
            SignaturesProperties = 24,
            PictureProperties = 25,
            DocumentScannerProperties = 26,
            DocumentScanner = 27
        }
        public enum LangRefColTable
        {
            ActivitiesRegistration_ActivitiesRegistrationName = 1,
            InfoColumns_IC_Column_Name = 2,
            Allowances_AllowancesMasterDescription = 3,
            ChioceProperties_Title = 4,
            AddChioceProperties_Title = 5,
            EntryProperties_Title = 6,
            EntryProperties_MaskText = 7,
            InfomessageProperties_Title = 8,
            InfoMessageContentDetail_MessageContent = 9,
            TextMessageProperties_Messagecontent = 10,
            VariableListProperties_Title = 11,
            PlanningselectProperties_Title = 12,
            PlanningselectProperties_OtherTitle = 13,
            PlanningselectProperties_Fixedvalue = 14,
            DocumetScanProperties_Title = 15,
            TransportTypeProperties_Title = 16,
            TransportTypePropertiesDetail_Title = 17,
            TrailerProperties_Title = 18,
            TrailerProperties_MaskText = 19,
            Questions_Name = 20,
            EntryProperties_FixedValue = 21,
            SetActionValueProperties_Value = 22,
            SetActionValueProperties_InfoColumnFixedText = 23,
            ExtraInfoProperties_Title = 24,
            ExtraInfoProperties_AlternativeTextOnPlaceLevel = 25,
            ExtraInfoProperties_AlternativeTextOnJobLevel = 26,
            ExtraInfoProperties_AlternativeTextOnProductLevel = 27,
            ExtraInfo_Title = 28,
            ExtraInfo_MaskText = 29,
            PalletsProperties_Title = 30,
            PalletsProperties_AlternativeTextOnPlaceLevel = 31,
            PalletsProperties_AlternativeTextOnJobLevel = 32,
            PalletsProperties_AlternativeTextOnProductLevel = 33,
            Pallets_Title = 34,
            ProblemsProperties_Title = 35,
            ProblemsProperties_AlternativeTextOnPlaceLevel = 36,
            ProblemsProperties_AlternativeTextOnJobLevel = 37,
            ProblemsProperties_AlternativeTextOnProductLevel = 38,
            Problems_Title = 39,
            Problems_MaskText = 40,
            SignaturesProperties_Title = 41,
            SignaturesProperties_AlternativeTextOnPlaceLevel = 42,
            SignaturesProperties_AlternativeTextOnJobLevel = 43,
            SignaturesProperties_AlternativeTextOnProductLevel = 44,
            PictureProperties_Title = 45,
            PictureProperties_AlternativeTextOnPlaceLevel = 46,
            PictureProperties_AlternativeTextOnJobLevel = 47,
            PictureProperties_AlternativeTextOnProductLevel = 48,
            DocumentScannerProperties_Title = 49,
            DocumentScannerProperties_AlternativeTextOnPlaceLevel = 50,
            DocumentScannerProperties_AlternativeTextOnJobLevel = 51,
            DocumentScannerProperties_AlternativeTextOnProductLevel = 52,
            DocumentScanner_Title = 53,
            VarListProperties_OtherTitle = 54,
            VarListProperties_OtherMaskText = 55
        }

        public enum EnumToolBoxItems
        {
            Choice = 1,
            Entry = 2,
            Planning_Select = 3,
            Check_Off = 4,
            Conditional_Action = 5,
            Info_Message = 6,
            Text_Message = 7,
            Set_Action___Value = 8,
            Variable_List = 9,
            Document_Scan = 10,
            Trailer = 11,
            Transport_Type = 12,
            Add_Chioce = 13,


            Flex = 14,
            Extra_Info = 15,
            Extra_Infop = 16,
            Pallets = 17,
            Palletsp = 18,
            Signature = 19,
            Signaturep = 20,
            Problem = 21,
            Problemp = 22,
            Picture = 23,

            Smart = 24,
            Smart_Extra_Info = 25,
            Smart_Extra_Infop = 26,
            Smart_Pallets = 27,
            Smart_Palletsp = 28,
            Smart_Problem = 29,
            Smart_Problemp = 30,
            Smart_Signature = 31,
            Smart_Picture = 32,

            Type_Of_Document = 33,
            Add_Document_Scan = 34,
            DocumentScanner = 35,
            DocumentScannerp = 36,
            Transport_Type_Detail = 37,
            Planning_Select_Other_Title = 38,
            Get_System_Values = 39,
            Entry_From_Variable_List = 40,
            Trailer_From_Variable_List = 41

        }
        public enum EnumLayoutStatus
        {
            InProgress = 1,
            InDevelopment = 2,
            ValidationRequested = 3,
            CheckNeeded = 4,
            ValidatedandPlanned = 5,
            Finished = 6,
            SupportRequested = 7
        }
        public enum EnumCopyColorCode
        {
            Green = 0,
            Empty = 1,
            Yellow = 2,
            Red = 3,
        }

        public enum EnumDefaultActivities
        {
            TX_SKY_Start_New = 1,
            TX_SKY_Start_Same = 2,
            TX_SKY_End = 3,
            TX_SKY_Next_Stop = 4,
            TX_SKY_Pause_Trip = 5,
            TX_SKY_Start_Trip = 6,
            TX_SKY_Stop_Trip = 7,
            TX_SKY_FLEX_Start_New = 8,
            TX_SKY_FLEX_Start_Same = 9,
            TX_SKY_FLEX_End = 10,
            TX_SKY_FLEX_Next_Stop = 11,
            TX_SKY_FLEX_Pause_Trip = 12,
            TX_SKY_FLEX_Start_Trip = 13,
            TX_SKY_FLEX_Stop_Trip = 14,
            TX_SMART_Start_Trip = 15,
            TX_SMART_Stop_Trip = 16,
            TX_GO_FLEX_Pause_Trip = 17,
            TX_GO_FLEX_Start_Trip = 18,
            TX_GO_FLEX_Stop_Trip = 19
        }
        public enum EnumOBCType
        {
            TX_SKY = 1,
            TX_SKY_FLEX = 2,
            TX_SMART = 3,
            TX_GO_FLEX = 4
        }

        public enum EnumPlanningSpecification
        {
            PauseTrip = 1,
            NextStop = 2
        }
        public enum EnumPlanningType
        {
            NoPlanning = 1,
            Trip = 3,
            Place = 4,
            Job = 6,
            Product = 7
        }

        public enum EnumSetActionValueType
        {
            NextQP = 1,
            NextView = 2,
            Allowance = 3,
            InfoColumn = 4,
            SavedValue = 5,
            EmptyFullSoloState = 6
        }
        public enum EnumCustomerSystemType
        {
            Customer = 1,
            Standard = 2,
            Testing = 3
        }
    }
}

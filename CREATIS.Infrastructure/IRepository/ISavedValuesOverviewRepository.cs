﻿using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.Infrastructure.IRepository
{
    public interface ISavedValuesOverviewRepository
    {
        List<SavedValues> GetSavedValues(int customerRefId, int layoutRefId);
        string SaveAlias(AliasNameDetails AliasNameDetailsModel);
        List<AliasNameDetails> GetAliasList(int layoutId);
    }
}

﻿using CREATIS.ApplicationCore.Modals;
using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.Infrastructure.IRepository
{
    public interface ICustomerRepository
    {
        String SaveCustomer(Customers cusModel);
        List<Customers> GetCustomer();
        String DeleteCustomer(int customerId);
        List<CustomerSystemType> GetCustomerSystemType();
    }
}

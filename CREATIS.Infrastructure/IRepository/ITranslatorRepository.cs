﻿using System;
using System.Collections.Generic;
using System.Text;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;

namespace CREATIS.Infrastructure.IRepository
{
    public interface ITranslatorRepository
    {
        List<UsedLanguages> GetUsedLanguage(int LayoutId);
        LanguageMaster GetLanguageById(int Id);
        List<LanguageMaster> GetAllLanguage();
        UsedLanguages SaveUsedLanguage(UsedLanguages UsedLanguages);
        List<AllowancesMaster> GetAllowancesMaster(int Layout_ID);
        List<UsedLanguages> GetUsedLanguageByLayoutIdandLangId(int layoutId, int langId);
        List<LangDetail> GetLangDetail(int TableId, int LayoutId, int Id, bool IsCopyLayout, bool isLoadHtml);
        List<LangDetail> GetLangActivities(int TableId, int LayoutId);
        List<LangDetail> GetLangRegistration(int TableId, int LayoutId);
        List<LangDetail> GetAllLangDetail(int TableId, int LayoutId);
        LangDetail GetLangInfo(LangDetail LangInfo, int TableId, string ColumnName);
        LangDetail UpdateLangDetail(LangDetail LangInfo);
        string SaveLangDetail(LangRefDetail LangRefInfo);
        string SaveCopyLayoutLangDetail(LangRefDetail LangRefInfo);
        string DeleteLangDetail(LangRefDetail LangRefInfo);
        List<LangDetail> GetLaunaguageDetailsInDashboard(int langRefColTableId, int layoutId);
        string GetLanguageContent(LangDetail langdetail, string languagename, int Id, string TableCode);
        string GetDefaultLanguage(int layoutId);
        string SaveTranslations(LangDetail LangInfo);
        bool GetIsCopyLayout(int LayoutId);
        List<LanguageMaster> GetSelectedLanguages(string LanguageIds, int LayoutId);
        LangRefDetail GetLangRefDetail(int Id);
        LangDetail GetLangDetail(int Id);
        string GetExportFileName(int LayoutId);
        string GetExportFileNameByCustomer(int customerId);
        LangDetail GetIsDefaultLayoutLanguage(int LayoutId);
        bool GetExportExcelValidation(int LayoutId);
        int GetLayoutDefaultLanguageId(int LayoutId);
        int GetTemplateLayoutId(int CustomerId, int OBCTypeId);
        bool ValidateTemplateLayoutId(int CustomerId, int LayoutId);
        string StripHTML(string source);
        List<LangDetail> GetGeneralTranslation(int Id);
        string SaveCentralTranslation(LangDetail LangDetailModel);
        List<Customers> GetCustomerSpecificList();
        List<LangDetail> GetCustomerSpecificTranslation(int CustomerId);
        string CopyTranslationFromCustomerSpecificToGeneral(int langDetailId, string userName, DateTime createdDate);
        LangDetail GetPropertiesLangDetail(LangDetail LangInfo);
        LangDetail GetQuestionsLangDetail(LangDetail LangInfo, int QuestionRefId);
        LangDetail GetPlanningSelectLangDetail(LangDetail LangInfo, int QuestionRefId);
        List<UITranslationDetail> GetCommonUITranslation();
        string SaveCommonUITranslation(UITranslationDetail LangDetailModel);
        List<UILangDetail> GetSpecificUITranslation();
        string SaveSpecificUITranslation(UILangDetail UILangDetailModel);

        string ImportCentralTranslations(LangDetail langdetail);
        UITranslationDetail GetUITranslationDetail(int id);
        UILangDetail GetUILangDetail(int id);
        string GetInfoMessageType(int id);
        string DeleteGeneralTranslation(int id);
        int GetPlanningSelectEntryQuestionId(int questionId);
    }
}

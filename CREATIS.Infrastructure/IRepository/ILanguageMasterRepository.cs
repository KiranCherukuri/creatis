﻿using CREATIS.ApplicationCore.Modals;
using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.Infrastructure.IRepository
{
    public interface  ILanguageMasterRepository
    {
        String SaveLanguageMaster(LanguageMaster langModel);
        List<LanguageMaster> GetLanguageMaster();
    }
}

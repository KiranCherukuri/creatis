﻿using System;
using System.Collections.Generic;
using System.Text;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;

namespace CREATIS.Infrastructure.IRepository
{
    public interface ICustomerOverviewRepository
    {
        List<CustomerOverviewDetail> GetLayoutList(int CustomerId);
        List<InfocolumnDetails> GetInfoColumn(int customerId, string createdBy);
        InfoColumns SaveInfoColumn(InfoColumns InfoColumn);
        string UpdateInfoColumn(int ID, string Columnname, string Updatedby, DateTime Updatedon);
        string DeleteInfoColumn(int ID);
        List<Integrator> GetIntegrator(int CustomerId);
        Integrator SaveIntegrator(Integrator Integrator);
        string UpdateIntegrator(int I_ID, string I_Details, string Updatedby, DateTime Updatedon);
        string DeleteIntegrator(int I_ID);
        List<CustomerNotes> GetCustomerNotes(int CustomerId);
        CustomerNotes SaveCustomerNotes(CustomerNotes CustomerNotes);
        string UpdateCustomerNotes(int CN_ID, string CN_Details, string Updatedby, DateTime Updatedon);
        string DeleteCustomerNotes(int CN_ID);
        List<LanguageMaster> GetLanguages();
        Layout SaveLayout(Layout LayoutDetails);
        UsedLanguages SaveUsedLanguage(UsedLanguages UsedLanguage);
        string GetCustomerName(int Customerid);
        Layout updateLayout(Layout LayoutDetails);
        List<InfoColumns> GetAssignedInfoColumn(int CustomerId);
        List<InfocolumnList> GetInfoColumnQuestionpath(int CustomerId);
        bool ValidateLayoutName(string LayoutName, int CustomerId, int LayoutId);
        string SaveDefaultInfoColumn(int customerRefId, string createdBy);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;

namespace CREATIS.Infrastructure.IRepository
{
    public interface IScreenLayoutRepository
    {
        List<ActivitiesRegistrationDetails> GetRegistration(int Layout_ID);
        List<ActivitiesRegistrationDetails> GetActivities(int Layout_ID);
        string UpdateActivitiesRegistrationPosition(int LayoutID, int ActivitiesRegistrationId, int position);
        string UpdateDeActivitiesRegistrationPosition(int LayoutID, int ActivitiesRegistrationId, int position);
        List<ActivitiesRegistrationDetails> GetRegistrationPosition(int Layout_ID);
        List<ActivitiesRegistrationDetails> GetActivitiesPosition(int Layout_ID);
        string GetCopyLayoutColor(int IsModification);
    }
}

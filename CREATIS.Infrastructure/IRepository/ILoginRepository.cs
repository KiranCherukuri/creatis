﻿using System;
using System.Collections.Generic;
using System.Text;
using CREATIS.ApplicationCore.Modals;

namespace CREATIS.Infrastructure.IRepository
{
    public interface ILoginRepository
    {
        Users GetUserList(string domainname);
        List<int> GetRolesList(int userId);
        List<string> GetRolesAccessList(List<string> roleList);
        List<string> GetLayoutStatusAccessList(int layoutId);
        List<string> GetRoleWithLayoutStatusAccessList(int layoutId, List<string> roleList);
        string SaveRecentActivity(RecentActivity recentModel);
        int GetLoginCustomer(string company, int customerId);
        List<int> GetCustomerLayoutList(string company, int customerId);
        bool IsCustomerLogin(List<string> roleList);
        int GetLayoutCustomerId(int layoutId);
    }
}

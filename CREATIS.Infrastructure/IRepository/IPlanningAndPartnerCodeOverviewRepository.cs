﻿using CREATIS.ApplicationCore.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.Infrastructure.IRepository
{
    public interface IPlanningAndPartnerCodeOverviewRepository
    {
        List<PlanningAndPartnerCodeOverview> GetPlanningAndPartnerCodeOverview(int customerRefId, int layoutRefId);
    }
}

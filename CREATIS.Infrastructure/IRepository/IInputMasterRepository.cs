﻿using CREATIS.ApplicationCore.Modals;
using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.Infrastructure.IRepository
{
    public interface IInputMasterRepository
    {
        String SaveInputMaster(InputMaster inputModel);
        List<InputMaster> GetInputMaster();
    }
}

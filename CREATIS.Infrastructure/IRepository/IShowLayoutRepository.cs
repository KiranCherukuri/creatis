﻿using System;
using System.Collections.Generic;
using System.Text;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;

namespace CREATIS.Infrastructure.IRepository
{
    public interface IShowLayoutRepository
    {
        ShowLayoutViewModel GetAllShowLayoutModelByLayoutID(int LayoutId);
        List<ActivitiesRegistration> GetActiveRegistrationByLayoutId(int LayoutId);
        List<Questions> GetAllQusByActivityId(int LayoutId);
        List<Questions> GetQuestionsByActivityId(int activityId);
        ShowLayoutViewModel GetAllShowLayoutModelByActivitesID(int ActivitiesId);
        List<ShowLayoutResponse> GetQuestionsByLayoutId(int LayoutId, int activitiesRegistrationId);
        string GetDefaultShowlayoutLanguage(int layoutId);
        LangDetail GetLaunaguageDetailsByLatoutId(int langRefColTableId, int refId, int layoutId);
        string GetLanguageName(int launguageId);
        List<LanguageMaster> GetLanguages(int LayoutId);
        bool GetLayoutColor(int layoutId);
        string GetLangtransProperties(LangDetail langdetail, int launguageId, int QuestionId, string targetCode);
        string GetPlanChoosingActivites(int planningSelectId);
        PlanningselectProperties GetPlanningSelectLevel(int planningSelectId);
        bool GetActivityFluxStaus(int activityRegId);
        string GetInfoMessageContent(int questionRefId, int layoutId);
        string GetParentPropertyName(int questionRefId);
        PlanningselectProperties GetPlanningSelectOtherContent(int questionRefId);
        List<int> GetActivitiesRegistrationByLayoutId(int LayoutId);
        bool GetTrailerOtherContent(int questionRefId);
        bool GetVariableListOtherContent(int questionRefId);
        ActivitiesRegistrationTranslations GetActivityRegistrationTranslations(int activityRegId, int layoutId);
        string GetActivityRegistationName(ActivitiesRegistrationTranslations activitieRegTranslation, string defaultLanguage);
        string GetQuestoinsTranslation(BoShowLayout questionsTranslation, string defaultLanguage);
        List<ShowlayoutInfoTranslations> GetInfoTranslations(int layoutId, int customerId);
        string GetInfoTranslationLanguage(ShowlayoutInfoTranslations questionsTranslation, string defaultLanguage, string translationDefaultlanguage);
        string GetProposeFixedValue(int entryId, string translatedFIxedValue);
        string GetSmartActivityStaus(int activityRegId, int layoutId);
        List<boPlanningLevelDrop> PlanningFeedbackLevel(int ActivityRefId);
        List<PlanningType> GetPlanningTypeSL(int Layout_ID);
    }
}

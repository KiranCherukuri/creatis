﻿using System;
using System.Collections.Generic;
using System.Text;
using CREATIS.ApplicationCore.Modals;

namespace CREATIS.Infrastructure.IRepository
{
    public interface ICopyLayoutRepository
    {
        string SaveCopyLayout(int LayoutId, string NewLayoutName, int CustomerId, string LanguageIds, string UserName, bool InfoColumn, string EmailId, string displayName);
        string SaveQuestionPath(int LayoutId, int ActivitiesRegistrationId, int TransportTypeId, List<int> QuestionIds, int NewLayoutId, int NewActivitiesRegistrationId, int NewTransportTypeId, int NewQuestionId, string CreatedBy, string Type);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using CREATIS.ApplicationCore;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;

namespace CREATIS.Infrastructure.IRepository
{
    public interface IDocumentTypeMasterRepository
    {
        String SaveDocumentTypeMaster(DocumentTypeMaster DocumentTypeModel);
        List<DocumentTypeMaster> GetDocumentTypeMaster();
    }
}

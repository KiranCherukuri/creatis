﻿using System;
using System.Collections.Generic;
using System.Text;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;

namespace CREATIS.Infrastructure.IRepository
{
    public interface IHomeRepository
    {
        List<Customers> Customer();
        List<UserRecentActivities> GetRecentActivities(string userId);
        string DeleteDashboard(int layoutId);
        string UpdateLayout(int LayoutId, string LayoutName);
        string RemoveRecentActivities(int layoutId, string userId);
    }
}

﻿using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.Infrastructure.IRepository
{
    public interface ITooltipRepository
    {
        List<Tooltip> GetTooltip();
        List<WizardTooltipTransation> GetWizardTooltip();
        List<ExtendedTooltipTransation> GetExtendedTooltip();
        int SaveTooltipImage(TooltipImage tooltipImageInfo);
        List<TooltipImage> GetTooltipImage(int tooltipRefId);
        List<TooltipImage> DeleteTooltipImage(int tooltipImageId, int tooltipId);
        int UpdateTooltip(Tooltip tooltip);
        string SaveTooltipTranslation(TooltipTranslation tooltipTranslation);
        List<TooltipImage> GetExtendedHelpTooltipImage(int uiId);
        string GetExtendedHelpText(int uiId, int layoutId);
        List<Tooltip> GetTooltipList(int layoutId);
        string GetTooltipTitle(int uiId);
    }
}

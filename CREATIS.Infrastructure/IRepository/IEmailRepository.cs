﻿using System;
using System.Collections.Generic;
using System.Text;
using CREATIS.ApplicationCore.Modals;

namespace CREATIS.Infrastructure.IRepository
{
    public interface IEmailRepository
    {
        void SendLayoutUpdateStatusMail(LayoutNotes layoutNotes, string fromAddress,string toAddress,string customername, string layoutname,string SelectedStatus,string urlString, int LayoutId);
        void SendRequestMail(LayoutNotes layoutNotes, string fromAddress, string toAddress, string customername, string layoutname, string SelectedStatus, string urlString, string username);


    }
}

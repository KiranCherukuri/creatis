﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("TextMessageProperties")]
    public class TextMessageProperties
    {
        [Key]
        [Column("TextMessageId", Order = 0)]
        public int? TextMessageId { get; set; }
        [Column("QuestionRefId", Order = 1)]
        public int QuestionRefId { get; set; }
        [Column("LayoutRefId", Order = 2)]
        public int LayoutRefId { get; set; }
        [Column("ActivitiesRefId", Order = 3)]
        public int ActivitiesRefId { get; set; }
        [Column("Name", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string Name { get; set; }
        [Column("SendMethod", Order = 5)]
        [MaxLength(50)]
        public string SendMethod { get; set; }
        [Column("Messagecontent", Order = 6)]
        [MaxLength(50)]
        public string Messagecontent { get; set; }
        [Column("CreatedBy", Order = 7)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 8)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 9)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 10)]
        public DateTime? UpdatedOn { get; set; }
        [Column("Conditionitem", Order = 23)]
        public int? Conditionitem { get; set; }
        [Column("ConCompareMethod", Order = 24)]
        public int? ConCompareMethod { get; set; }
        [Column("Convalue", Order = 25)]
        public int? Convalue { get; set; }
        [Column("ConItemSavedValued", Order = 29)]
        public int? ConItemSavedValued { get; set; }
        [Column("ConvalueSavedValue", Order = 30)]
        public int? ConvalueSavedValue { get; set; }
        [Column("ComFixedText", Order = 22)]
        [MaxLength(100)]
        public string ComFixedText { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        public string Comment { get; set; }
    }
}

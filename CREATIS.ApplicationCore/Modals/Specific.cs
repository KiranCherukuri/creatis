﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CREATIS.ApplicationCore.Modals
{
    [Table("Specific")]
    public class Specific
    {
        [Key]
        [Column("SpecificId", Order = 0)]
        public int SpecificId { get; set; }
        [Column("SpecificDescription", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        public string SpecificDescription { get; set; }
        [Column("CreatedBy", Order = 2)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 3)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 4)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 5)]
        public DateTime? UpdatedOn { get; set; }
        [Column("DefaultActivitiesRefId", Order = 6)]
        public int? DefaultActivitiesRefId { get; set; }
        [Column("IsPauseTrip", Order = 7)]
        public bool IsPauseTrip { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class TransportTypes
    {
        [Key]
        public int TransportTypeId { get; set; }
        public int CustomerRefId { get; set; }
        public int LayoutRefId { get; set; }
        public string TransportTypeName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int IsModification { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("CustomerRefId")]
        public Customers Customers { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        public int? CopyLayoutRefId { get; set; }
        [NotMapped]
        public bool IsActivityTransportType { get; set; }
        [NotMapped]
        public string CopyColorCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("SmartExtraInfo")]
    public class SmartExtraInfo
    {
        [Key]
        [Column("SmartExtraInfoID", Order = 0)]
        public int? SmartExtraInfoID { get; set; }
        [Column("ActivityRefID", Order = 1)]
        public int ActivityRefID { get; set; }
        [Column("LayoutRefID", Order = 2)]
        public int LayoutRefID { get; set; }
        [Column("Title", Order = 14)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(20)]
        public string Title { get; set; }
        [Column("Format", Order = 14)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(10)]
        public string Format { get; set; }
        [Column("Size", Order = 4)]
        [MaxLength(15)]
        public string Size { get; set; }
        [Column("RegEx", Order = 4)]
        [MaxLength(15)]
        public string RegEx { get; set; }
        [Column("PlaceLevel", Order = 5)]
        public bool PlaceLevel { get; set; }
        [Column("JobLevel", Order = 6)]
        public bool JobLevel { get; set; }
        [Column("ProductLevel", Order = 7)]
        public bool ProductLevel { get; set; }
        [Column("PlanningFeedbackcode", Order = 8)]
        [MaxLength(20)]
        public string PlanningFeedbackcode { get; set; }
        [Column("Partnercode", Order = 8)]
        [MaxLength(20)]
        public string Partnercode { get; set; }
        [Column("CreatedBy", Order = 9)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 10)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 11)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 12)]
        public DateTime? UpdatedOn { get; set; }
        [Column("QuestionRefID", Order = 13)]
        public int QuestionRefID { get; set; }
        [Column("SmartExtraInfoRefID", Order = 15)]
        public int? SmartExtraInfoRefID { get; set; }
        [Column("MaskText", Order = 15)]
        public string MaskText { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class Modules
    {
        [Key]
        [Column("ModuleId", Order = 0)]
        public int ModuleId { get; set; }
        [Column("ModuleName", Order = 0)]
        public string ModuleName { get; set; }
        [Column("IsActive", Order = 0)]
        public bool IsActive { get; set; }
        [Column("CreatedBy", Order = 0)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 0)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 0)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 0)]
        public DateTime UpdatedOn { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("AddChioceProperties")]
    public class AddChioceProperties
    {
        [Key]
        [Column("AddChioceId", Order = 0)]
        public int? AddChioceId { get; set; }
        [Column("Title", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string Title { get; set; }
        [Column("PlanningfeedbackCode", Order = 2)]
        [MaxLength(100)]
        public string PlanningfeedbackCode { get; set; }
        [Column("ChioceRefId", Order = 3)]
        public int ChioceRefId { get; set; }
        [Column("CreatedBy", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 5)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 6)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 7)]
        public DateTime? UpdatedOn { get; set; }
        [Column("Endrecursiveloop", Order = 8)]
        public bool Endrecursiveloop { get; set; }
        [Column("QuestionRefId", Order = 9)]
        public int QuestionRefId { get; set; }
        [Column("Infocolumn", Order = 10)]
        [MaxLength(50)]
        public string Infocolumn { get; set; }
        [Column("InfoColumnAction", Order = 11)]
        [MaxLength(50)]
        public string InfoColumnAction { get; set; }
        [Column("FileNumber", Order = 12)]
        [MaxLength(50)]
        public string FileNumber { get; set; }
        [ForeignKey("ChioceRefId")]
        public ChioceProperties ChioceProperties { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        [Column("PartCode", Order = 2)]
        [MaxLength(100)]
        public string PartCode { get; set; }
        [Column("Dynamicp", Order = 2)]
        [MaxLength(100)]
        public string Dynamicp { get; set; }
        [NotMapped]
        public bool IsFeedbackType { get; set; }
        public string Comment { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("TransportTypeProperties")]
    public class TransportTypeProperties
    {
        [Key]
        public int TransportTypeId { get; set; }
        public int ActivitiesRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int QuestionRefId { get; set; }
        public string Title { get; set; }
        public bool Save { get; set; }
        public int SaveTo { get; set; }
        public string Action { get; set; }
        public string Comment { get; set; }
        public int? InfoColumn { get; set; }
        public int? InfoColumnAction { get; set; }
        public string PlanningFeedback { get; set; }
        public string FeedbackType { get; set; }
        public string PlanningFeedbackCode { get; set; }
        public string PartnerCode { get; set; }
        public int? ConditionValue { get; set; }
        public int? ConditionSavedValue { get; set; }
        public int? CompareMethod { get; set; }
        public int? CompareTo { get; set; }
        public int? CompareSavedValue { get; set; }
        public string CompareFixedText { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public bool? AddDefault { get; set; }

        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        [NotMapped]
        public int DashboardTransportTypeId { get; set; }
        public int? CopyLayoutRefId { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class PlanningSelectOther
    {
        [Key]
        public int PlanningSelectOtherId { get; set; }
        public int QuestionRefId { get; set; }
        public string OtherTitle { get; set; }
        public string OtherMaskText { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateOn { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
    }
}

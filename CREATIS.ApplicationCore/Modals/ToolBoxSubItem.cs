﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ToolBoxSubItem")]
    public class ToolBoxSubItem
    {
        [Key]
        public int SubToolboxId { get; set; }
        public int ToolboxRefId { get; set; }
        public string SubToolboxName { get; set; }
        public string SubToolboxDescription { get; set; }
        public int SubToolboxStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? Position { get; set; }
        [ForeignKey("ToolboxRefId")]
        public MainToolBox MainToolBox { get; set; }
        public bool? IsSky { get; set; }
        public bool? IsSkyFlex { get; set; }
        public bool? IsSmart { get; set; }
        public bool? IsFlex { get; set; }
    }
}

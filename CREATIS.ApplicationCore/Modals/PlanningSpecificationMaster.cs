﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("PlanningSpecificationMaster")]
    public class PlanningSpecificationMaster
    {
        [Key]
        public int PlanningSpecificationMasterId { get; set; }
        public string PlanningSpecificationMasterDescription { get; set; }
        public int PlanningTypeMasterRefId { get; set; }
        public int IS_Active { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        [ForeignKey("PlanningTypeMasterRefId")]
        public PlanningTypeMaster PlanningTypeMaster { get; set; }
        public ICollection<PlanningSpecification> PlanningSpecification { get; set; }
        [NotMapped]
        public string Active { get; set; }
        [NotMapped]
        public string CopyColorCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("InputMaster")]
    public class InputMaster
    {
        [Key]
        [Column("Id", Order = 0)]
        public int Id { get; set; }
        [Column("ModuleName", Order = 1)]
        [Required(AllowEmptyStrings = false)]
        public string ModuleName { get; set; }
        [Column("ControlName", Order = 2)]
        [Required(AllowEmptyStrings = false)]
        public string ControlName { get; set; }
        public int MaxLength { get; set; }
        public int CreatedBy { get; set; }
        public int Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        [NotMapped]
        public string StatusName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ServerEnvironment")]
    public class ServerEnvironment
    {
        [Key]
        [Column("Id", Order = 0)]
        public int Id { get; set; }
        [Column("Name", Order = 1)]
        public string Name { get; set; }
        [Column("IsActive", Order = 2)]
        public int IsActive { get; set; }
        [Column("CreatedBy", Order = 3)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 4)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 5)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 6)]
        public DateTime? UpdatedOn { get; set; }
        [NotMapped]
        public string Active { get; set; }

    }
}

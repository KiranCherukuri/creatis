﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("OBCTypeMaster")]
    public class OBCTypeMaster
    {
        [Key]
        public int OBCTypeId { get; set; }
        public string OBCTypeDescription { get; set; }
        public int IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public ICollection<OBCType> OBCTypes { get; set; }
        public ICollection<AllowancesMaster> AllowancesMaster { get; set; }
        [NotMapped]
        public string Active { get; set; }
        [NotMapped]
        public string CopyColorCode { get; set; }
    }
}

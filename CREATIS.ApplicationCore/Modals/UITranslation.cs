﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace CREATIS.ApplicationCore.Modals
{
    [Table("UITranslation")]
    public class UITranslation
    {
        [Key]
        public int UITranslationId { get; set; }
        public int UITranslationDetailRefId { get; set; }
        public int ModulesRefId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public int ParentId { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        [ForeignKey("UITranslationDetailRefId")]
        public UITranslationDetail UITranslationDetail { get; set; }

        [ForeignKey("ModulesRefId")]
        public Modules Modules { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("UIContentLanguage")]
    public class UIContentLanguage
    {
        [Key]
        [Column("LabelID", Order = 1)]
        public int LabelID { get; set; }
        [Column("AllLangauges", Order = 2)]
        public string AllLangauges { get; set; }
        [Column("CreatedBy", Order = 3)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 4)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 5)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 6)]
        public DateTime UpdatedOn { get; set; }
    }
}

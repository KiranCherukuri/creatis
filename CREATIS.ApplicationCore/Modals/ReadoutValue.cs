﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ReadoutValue")]
    public class ReadoutValue
    {
        [Key]
        [Column("ReadoutValueId", Order = 0)]
        public int ReadoutValueId { get; set; }
        [Column("ReadoutValueDesc", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string ReadoutValueDesc { get; set; }
        [Column("ReadoutValueStatus", Order = 2)]
        public int ReadoutValueStatus { get; set; }
    }
}

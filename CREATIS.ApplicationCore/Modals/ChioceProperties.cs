﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ChioceProperties")]
    public class ChioceProperties
    {
        [Key]
        [Column("ChioceId", Order = 0)]
        public int? ChioceId { get; set; }
        [Column("QuestionRefId", Order = 1)]
        public int QuestionRefId { get; set; }
        [Column("LayoutRefId", Order = 2)]
        public int LayoutRefId { get; set; }
        [Column("ActivitiesRefId", Order = 3)]
        public int ActivitiesRefId { get; set; }
        [Column("Title", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string Title { get; set; }
        [Column("Recursiveloop", Order = 5)]
        public int Recursiveloop { get; set; }
        [Column("Save", Order = 6)]
        public int Save { get; set; }
        [Column("Propose", Order = 7)]
        [MaxLength(100)]
        public String Propose { get; set; }
        [Column("Planningfeedback", Order = 8)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string Planningfeedback { get; set; }
        [Column("FeedbackType", Order = 9)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string FeedbackType { get; set; }
        [Column("CreatedBy", Order = 10)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 11)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 12)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 13)]
        public DateTime? UpdatedOn { get; set; }
        [Column("ConditionValue", Order = 14)]
        public int? ConditionValue { get; set; }
        [Column("ConCompareMethod", Order = 15)]
        public int? ConCompareMethod { get; set; }
        [Column("ConCompareTo", Order = 16)]
        public int? ConCompareTo { get; set; }
        [Column("Infocolumn", Order = 17)]
        [MaxLength(50)]
        public string Infocolumn { get; set; }
        [Column("InfoColumnAction", Order = 18)]
        public int? InfoColumnAction { get; set; }
        [Column("FileNumber", Order = 19)]
        [MaxLength(50)]
        public string FileNumber { get; set; }
        [Column("ConSavedValued", Order = 20)]
        public int? ConSavedValued { get; set; }
        [Column("comtoSavedValue", Order = 21)]
        public int? comtoSavedValue { get; set; }
        [Column("ComFixedText", Order = 22)]
        [MaxLength(100)]
        public string ComFixedText { get; set; }
        //[Column("InfoActionQP", Order = 23)]
        //public int? InfoActionQP { get; set; }
        [Column("AutoGenerate", Order = 23)]
        public int? AutoGenerate { get; set; }
        [Column("SaveValue", Order = 24)]
        public int? SaveValue { get; set; }
        [Column("CopyLayoutRefId", Order = 25)]
        public int? CopyLayoutRefId { get; set; }
        [Column("Comment", Order = 26)]
        public string Comment { get; set; }
        [Column("Planningfeedbackcode", Order = 27)]
        public string Planningfeedbackcode { get; set; }
        [Column("InfoActionQP", Order = 28)]
        public int? InfoActionQP { get; set; }
        
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        [Column("Saveto", Order = 28)]
        public int? Saveto { get; set; }
        [Column("PartnerCode", Order = 27)]
        public string PartnerCode { get; set; }
        public bool? IsDefaultAlaisName { get; set; }
    }
}

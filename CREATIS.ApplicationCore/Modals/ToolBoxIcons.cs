﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ToolBoxIcons")]
    public class ToolBoxIcons
    {
        [Key]
        [Column("ToolboxIconId", Order = 0)]
        public int ToolboxIconId { get; set; }
        [Column("ToolboxRefId", Order = 1)]
        public int ToolboxRefId { get; set; }
        [Column("ToolboxType", Order = 2)]
        public int ToolboxType { get; set; }
        [Column("ToolboxIconPath", Order = 3)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(500)]
        public string ToolboxIconPath { get; set; }
        [Column("ToolboxIconStatus", Order = 4)]
        public int ToolboxIconStatus { get; set; }
        [Column("CreatedBy", Order = 5)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 6)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 7)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 8)]
        public DateTime? UpdatedOn { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ConditionalProperties")]
    public class ConditionalProperties
    {
        [Key]
        [Column("ConditionalID", Order = 0)]
        public int? ConditionalID { get; set; }
        [Column("QuestionRefId", Order = 1)]
        public int QuestionRefId { get; set; }
        [Column("LayoutRefId", Order = 2)]
        public int LayoutRefId { get; set; }
        [Column("ActivitiesRefId", Order = 3)]
        public int ActivitiesRefId { get; set; }
        [Column("CreatedBy", Order = 10)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 11)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 12)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 13)]
        public DateTime? UpdatedOn { get; set; }
        [Column("ConditionValue", Order = 14)]
        public int ConditionValue { get; set; }
        [Column("ConCompareMethod", Order = 15)]
        public int ConCompareMethod { get; set; }
        [Column("ConCompareTo", Order = 16)]
        public int ConCompareTo { get; set; }
        [Column("ConSavedValued", Order = 20)]
        public int? ConSavedValued { get; set; }
        [Column("comtoSavedValue", Order = 21)]
        public int? comtoSavedValue { get; set; }
        [Column("ComFixedText", Order = 22)]
        [MaxLength(100)]
        public string ComFixedText { get; set; }
        [Column("Name", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string Name { get; set; }
        public string Comment { get; set; }
        public string Repeat { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("RecentActivity")]
    public class RecentActivity
    {
        [Key]
        public int RecentActivityId { get; set; }
        public int LayoutRefId { get; set; }
        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
    }
}

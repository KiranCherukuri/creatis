﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("DefaultActivities")]
    public class DefaultActivities
    {
        [Key]
        [Column("ActivitiesRegistrationId", Order = 0)]
        public int ActivitiesRegistrationId { get; set; }
        [Column("ActivitiesRegistrationName", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        public string ActivitiesRegistrationName { get; set; }
        [Column("WorkingCodeRefId", Order = 2)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string WorkingCodeRefId { get; set; }
        [Column("PlanningRefId", Order = 3)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string PlanningRefId { get; set; }
        [Column("PlanningOverviewRefId", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string PlanningOverviewRefId { get; set; }
        [Column("SpecificRefId", Order = 5)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string SpecificRefId { get; set; }
        [Column("Visible", Order = 6)]
        public bool Visible { get; set; }
        [Column("ConfirmButton", Order = 7)]
        public bool ConfirmButton { get; set; }
        [Column("PTORefId", Order = 8)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string PTORefId { get; set; }
        [Column("InterruptibleByRefId", Order = 9)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string InterruptibleByRefId { get; set; }
        [Column("TransportType", Order = 10)]
        public bool TransportType { get; set; }
        [Column("EmptyFullSolo", Order = 11)]
        public bool EmptyFullSolo { get; set; }
        [Column("Flexactivity", Order = 12)]
        public bool Flexactivity { get; set; }
        [Column("ISModification", Order = 13)]
        public int ISModification { get; set; }
        [Column("CreatedBy", Order = 14)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 15)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 16)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 17)]
        public DateTime? UpdatedOn { get; set; }
        [Column("OBCDefault", Order = 18)]
        public int OBCDefault { get; set; }
        [Column("ISPlanningSpec", Order = 19)]
        public int ISPlanningSpec { get; set; }
        [Column("PauseTrip", Order = 20)]
        public int PauseTrip { get; set; }
        [Column("OBCTypeMasterRefId", Order = 21)]
        public int OBCTypeMasterRefId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class VarListCommentReadoutDetail
    {
        [Key]
        public int VarListCommentReadoutDetailId { get; set; }
        public int VarListPropertiesRefId { get; set; }
        public int CommentId { get; set; }
        public string ReadoutRange { get; set; }
        public int? StartChar { get; set; }
        public int? CharLength { get; set; }
        public int? SaveTo { get; set; }
        public bool IsDefaultAlaisName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        [NotMapped]
        public string StartCharValue { get; set; }
        [NotMapped]
        public string CharLengthValue { get; set; }
        [NotMapped]
        public string SaveToValue { get; set; }
        [NotMapped]
        public string SaveToText { get; set; }
        [NotMapped]
        public string DefaultAliasName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ConditionCompareMethod")]
    public class conditionCompareMethod
    {
        [Key]
        [Column("CompareMethodId", Order = 0)]
        public int CompareMethodId { get; set; }
        [Column("CompareMethodDesc", Order = 2)]
        [Required(AllowEmptyStrings = true)]
        public string CompareMethodDesc { get; set; }
        [Column("CompareMethodStatus", Order = 3)]
        public bool CompareMethodStatus { get; set; }
        public bool IsSavedValue { get; set; }
        public bool IsConditionalSavedValue { get; set; }
    }
}

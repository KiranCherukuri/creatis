﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("InfoColumnMaster")]
    public class InfoColumnsMaster
    {
        [Key]
        public int ICM_Column_No { get; set; }
        [MaxLength(50)]
        public string ICM_Name { get; set; }
        public int ISM_Active { get; set; }
        public ICollection<InfoColumns> InfoColum { get; set; }

    }
}

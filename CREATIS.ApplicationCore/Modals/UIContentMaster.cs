﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("UIContentMaster")]
    public class UIContentMaster
    {
        [Key]
        [Column("LabelId", Order = 0)]
        public int LabelId { get; set; }
        [Column("PageRefId", Order = 1)]
        public int PageRefId { get; set; }
        [Column("LabelName", Order = 2)]
        [MaxLength(150)]
        public string LabelName { get; set; }
        [Column("CreatedBy", Order = 3)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 4)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 5)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 6)]
        public DateTime UpdatedOn { get; set; }
        [ForeignKey("PageRefId")]
        public PageMaster PageMaster { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ActivitiesRegistration")]
    public class ActivitiesRegistration
    {
        [Key]
        public int ActivitiesRegistrationId { get; set; }
        public int CustomerRefId { get; set; }
        public int LayoutRefId { get; set; }
        public string ActivitiesRegistrationName { get; set; }
        public string WorkingCodeRefId { get; set; }
        public string PlanningRefId { get; set; }
        public string PlanningOverviewRefId { get; set; }
        public string SpecificRefId { get; set; }
        public bool Visible { get; set; }
        public bool ConfirmButton { get; set; }
        public string PTORefId { get; set; }
        public string InterruptibleByRefId { get; set; }
        public bool TransportType { get; set; }
        public bool EmptyFullSolo { get; set; }
        public bool Flexactivity { get; set; }
        public int ISModification { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? OBCDefault { get; set; }
        public int ActRegPosition { get; set; }
        public int? DefaultActivitiesRefId { get; set; }
        public int ScreenLayoutIsModification { get; set; }
        public int? CopyLayoutRefId { get; set; }
        public int? DefaultLanguageId { get; set; }
        public string Comment { get; set; }
        public int IsModificationActivitiesRegistrationName { get; set; }
        public int IsModificationWorkingCodeRefId { get; set; }
        public int IsModificationPlanningRefId { get; set; }
        public int IsModificationPlanningOverviewRefId { get; set; }
        public int IsModificationSpecificRefId { get; set; }
        public int IsModificationVisible { get; set; }
        public int IsModificationConfirmButton { get; set; }
        public int IsModificationTransportType { get; set; }
        public int IsModificationEmptyFullSolo { get; set; }
        public int IsModificationFlexactivity { get; set; }
        public int IsModificationPTORefId { get; set; }
        public int IsModificationInterruptibleByRefId { get; set; }

        [ForeignKey("CustomerRefId")]
        public Customers Customers { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }

        [ForeignKey("DefaultLanguageId")]
        public LanguageMaster LanguageMaster { get; set; }

        public ICollection<ScreenLayout> ScreenLayout
        {
            get; set;
        }
        public ICollection<Questions> Questions
        {
            get; set;
        }
        public ICollection<EntryProperties> EntryProperties
        {
            get; set;
        }
        [NotMapped]
        public bool IsCopyLayout { get; set; }
        
    }

}

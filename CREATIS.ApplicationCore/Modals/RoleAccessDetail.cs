﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class RoleAccessDetail
    {
        [Key]
        public int RoleAccessDetailId { get; set; }
        public int ModulesDetailRefId { get; set; }
        public int RolesRefId { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

        [ForeignKey("ModulesDetailRefId")]
        public ModulesDetail ModulesDetail { get; set; }

        [ForeignKey("RolesRefId")]
        public Roles Roles { get; set; }
    }
}

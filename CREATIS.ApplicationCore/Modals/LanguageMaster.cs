﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("LanguageMaster")]
    public class LanguageMaster
    {
        [Key]
        [Column("LanguageId", Order = 0)]
        public int LanguageId { get; set; }
        [Column("LanguageCode", Order = 1)]
        [MaxLength(30)]
        public string LanguageCode { get; set; }
        [Column("LanguageDescription", Order = 2)]
        [MaxLength(50)]
        public string LanguageDescription { get; set; }
        [Column("IS_Active", Order = 3)]
        public int IS_Active { get; set; }
        [Column("CreatedBy", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 5)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 6)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 7)]
        public DateTime? UpdatedOn { get; set; }
        public ICollection<UsedLanguages> UsedLanguages { get; set; }
        [NotMapped]
        public string Active { get; set; }

        [NotMapped]
        public int IsDefault { get; set; }
    }
}

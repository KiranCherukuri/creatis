﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class VarListFilterDetail
    {
        [Key]
        public int VarListFilterDetailId { get; set; }
        public int VarListPropertiesRefId { get; set; }
        public int CommentId { get; set; }
        public string FilterMethod { get; set; }
        public int SavedValue { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        [NotMapped]
        public string SavedValueName { get; set; }
    }
}

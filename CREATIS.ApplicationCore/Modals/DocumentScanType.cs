﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CREATIS.ApplicationCore.Modals
{
    public class DocumentScanType
    {
        [Key]
        public int? DocumentScanTypeId { get; set; }
        public int DocumentScanTitleRefId { get; set; }
        public int QuestionRefID { get; set; }
        public string DocumentTypeTitle { get; set; }
        public string DocumentType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        [ForeignKey("DocumentScanTitleRefId")]
        public DocumentScanTitle DocumentScanTitle { get; set; }
    }
}

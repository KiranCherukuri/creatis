﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("Users")]
    public class Users
    {
        [Key]
        public int UserId { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(50)]
        public string DomainId { get; set; }
        [MaxLength(100)]
        public string EmailId { get; set; }
        [MaxLength(100)]
        public string RoleId { get; set; }
        public int UserStatus { get; set; }
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        [Column("ApprovedBy")]
        public int? ApprovedBy { get; set; }
        [Column("ApprovedOn")]
        public DateTime? ApprovedOn { get; set; }
        [Column("ApprovedStatus")]
        public int? ApprovedStatus { get; set; }
        [NotMapped]
        public string ApproverName { get; set; }
        [NotMapped]
        public string ApprovalStatus { get; set; }

    }
}

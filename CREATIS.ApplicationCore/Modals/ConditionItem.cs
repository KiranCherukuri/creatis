﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ConditionItem")]
    public class ConditionItem
    {
        [Key]
        [Column("ItemId", Order = 0)]
        public int? ItemId { get; set; }
        [Column("ItemDesc", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        public string ItemDesc { get; set; }
        [Column("LayoutRefId", Order = 2)]
        public int LayoutRefId { get; set; }
        [Column("RefTable", Order = 3)]
        [Required(AllowEmptyStrings = true)]
        public string RefTable { get; set; }
        [Column("RefTablePrimaryId", Order = 4)]
        public int RefTablePrimaryId { get; set; }
        [Column("ItemStatus", Order = 5)]
        public bool ItemStatus { get; set; }
        [Column("CreatedBy", Order = 6)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 7)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 8)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 9)]
        public DateTime? UpdatedOn { get; set; }
    }
}

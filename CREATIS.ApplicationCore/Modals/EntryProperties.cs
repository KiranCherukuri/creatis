﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("EntryProperties")]
    public class EntryProperties
    {
        [Key]
        [Column("EntryId", Order = 0)]
        public int? EntryId { get; set; }
        [Column("QuestionRefId", Order = 1)]
        public int QuestionRefId { get; set; }
        [Column("LayoutRefId", Order = 2)]
        public int LayoutRefId { get; set; }
        [Column("ActivitiesRefId", Order = 3)]
        public int ActivitiesRefId { get; set; }
        [Column("Title", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string Title { get; set; }
        [Column("MaskText", Order = 5)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string MaskText { get; set; }
        [Column("Size", Order = 6)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string Size { get; set; }
        [Column("NotNull", Order = 7)]
        public int NotNull { get; set; }
        [Column("MinimumofCharacter", Order = 8)]
        public int MinimumofCharacter { get; set; }
        [Column("ExactCharacter", Order = 9)]
        public int ExactCharacter { get; set; }
        [Column("Zeronotallowed", Order = 10)]
        public int Zeronotallowed { get; set; }
        [Column("Regex", Order = 11)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string Regex { get; set; }
        [Column("Predefined", Order = 12)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string Predefined { get; set; }
        [Column("Save", Order = 13)]
        public int Save { get; set; }
        [Column("Propose", Order = 14)]
        public int? Propose { get; set; }
        [Column("Planningfeedback", Order = 15)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string Planningfeedback { get; set; }
        [Column("FeedbackType", Order = 16)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string FeedbackType { get; set; }
        [Column("PlanningfeedbackCode", Order = 17)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string PlanningfeedbackCode { get; set; }
        [Column("CreatedBy", Order = 18)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 19)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 20)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 21)]
        public DateTime? UpdatedOn { get; set; }
        [Column("EntryType", Order = 22)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string EntryType { get; set; }
        [Column("Conditionitem", Order = 23)]
        public int? Conditionitem { get; set; }
        [Column("ConCompareMethod", Order = 24)]
        public int? ConCompareMethod { get; set; }
        [Column("Convalue", Order = 25)]
        public int? Convalue { get; set; }
        [Column("Infocolumn", Order = 26)]
        [MaxLength(50)]
        public string Infocolumn { get; set; }
        [Column("InfoColumnAction", Order = 27)]
        public int? InfoColumnAction { get; set; }
        [Column("FileNumber", Order = 28)]
        [MaxLength(50)]
        public string FileNumber { get; set; }
        [Column("ConItemSavedValued", Order = 29)]
        public int? ConItemSavedValued { get; set; }
        [Column("ConvalueSavedValue", Order = 30)]
        public int? ConvalueSavedValue { get; set; }
        [Column("PartnerCode", Order = 31)]
        [MaxLength(100)]
        public string PartnerCode { get; set; }
        [Column("SaveValue", Order = 32)]
        public int? SaveValue { get; set; }
        [Column("DatetimeFormat", Order = 33)]
        [MaxLength(100)]
        public string DatetimeFormat { get; set; }
        [Column("AutoGenerate", Order = 34)]
        public int? AutoGenerate { get; set; }
        [Column("ComFixedText", Order = 35)]
        [MaxLength(100)]
        public string ComFixedText { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        public string Comment { get; set; }
        public int? CopyLayoutRefId { get; set; }
        public int? SaveTo { get; set; }

        [Column("MinimumOfCharterValue")]
        public int? MinimumOfCharterValue { get; set; }
        public bool? IsDefaultAlaisName { get; set; }
        public string ProposeFixedValue { get; set; }
        [NotMapped]
        public string PropertyName { get; set; }

    }
}

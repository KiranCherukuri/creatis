﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class VarListProperties
    {
        [Key]
        public int? VarListPropertiesId { get; set; }
        public int QuestionRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int ActivitiesRefId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public int VarListId { get; set; }
        public string Other { get; set; }
        public string OtherTitle { get; set; }
        public string OtherMaskText { get; set; }
        public bool SaveDisplay { get; set; }
        public int? SaveDisplayTo { get; set; }
        public string Comment { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? ConditionItemValue { get; set; }
        public int? ConditionSavedValue { get; set; }
        public int? CompareMethod { get; set; }
        public int? CompareTo { get; set; }
        public int? CompareToSavedValue { get; set; }
        public string CompareToFixedText { get; set; }
        public string PlanningFeedback { get; set; }
        public string FeedbackType { get; set; }
        public string PlanningFeedbackCode { get; set; }
        public string Infocolumn { get; set; }
        public int? InfoColumnAction { get; set; }
        public string FileNumber { get; set; }
        public string PartnerCode { get; set; }
        public string RegisterAsTrailer { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        [NotMapped]
        public string CommentTokenId { get; set; }
        [NotMapped]
        public string FilterTokenId { get; set; }
        [NotMapped]
        public string SavedDisplayValues { get; set; }
        [NotMapped]
        public string SaveDisplayAlaiasVal { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("VariableListProperties")]
    public class VariableListProperties
    {
        [Key]
        [Column("VariableListId", Order = 0)]
        public int? VariableListId { get; set; }
        [Column("QuestionRefId", Order = 1)]
        public int QuestionRefId { get; set; }
        [Column("LayoutRefId", Order = 2)]
        public int LayoutRefId { get; set; }
        [Column("ActivitiesRefId", Order = 3)]
        public int ActivitiesRefId { get; set; }
        [Column("Title", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string Title { get; set; }
        [Column("VarListID", Order = 5)]
        public int VarListID { get; set; }
        [Column("Other", Order = 6)]
        [MaxLength(50)]
        public string Other { get; set; }
        [Column("CreatedBy", Order = 7)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 8)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 9)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 10)]
        public DateTime? UpdatedOn { get; set; }

        [NotMapped]
        [Column("Name", Order = 11)]
        [MaxLength(50)]
        public string Name { get; set; }
        [Column("ConditionItemValue", Order = 23)]
        public int? ConditionItemValue { get; set; }
        [Column("ConditionSavedValue", Order = 24)]
        public int? ConditionSavedValue { get; set; }
        [Column("CompareMethod", Order = 25)]
        public int? CompareMethod { get; set; }
        [Column("CompareTo", Order = 29)]
        public int? CompareTo { get; set; }
        [Column("CompareToSavedValue", Order = 30)]
        public int? CompareToSavedValue { get; set; }
        [Column("CompareToFixedText", Order = 22)]
        public string CompareToFixedText { get; set; }

        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        public string Comment { get; set; }


        [Column("SaveDisplay", Order = 30)]
        public int? SaveDisplay { get; set; }
        [Column("SaveComment", Order = 30)]
        public int? SaveComment { get; set; }
        [Column("FilterComment", Order = 30)]
        public int? FilterComment { get; set; }

        [Column("FilterMethods", Order = 30)]
        public string FilterMethods { get; set; }
        [Column("SavedValues", Order = 30)]
        public int? SavedValues { get; set; }
        [Column("Filter", Order = 30)]
        public string Filter { get; set; }


    }
}

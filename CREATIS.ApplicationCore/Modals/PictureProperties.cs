﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("PictureProperties")]
    public class PictureProperties
    {
        [Key]
        [Column("PicturePropertiesID", Order = 0)]
        public int? PicturePropertiesID { get; set; }
        [Column("ActivityRefID", Order = 1)]
        public int ActivityRefID { get; set; }
        [Column("LayoutRefID", Order = 2)]
        public int LayoutRefID { get; set; }
        [Column("CreatedBy", Order = 3)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 4)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 5)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 6)]
        public DateTime? UpdatedOn { get; set; }
        [Column("QuestionRefID", Order = 7)]
        public int QuestionRefID { get; set; }
        [Column("Title", Order = 8)]
        public string Title { get; set; }
        [Column("AlternativetextonPlacelevel", Order = 9)]
        public string AlternativetextonPlacelevel { get; set; }
        [Column("AlternativetextonJoblevel", Order = 10)]
        public string AlternativetextonJoblevel { get; set; }
        [Column("Alternativetextonproductlevel", Order = 11)]
        public string Alternativetextonproductlevel { get; set; }
    }
}

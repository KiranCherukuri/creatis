﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ScreenLayout")]
    public class ScreenLayout
    {
        [Key]
        [Column("ScreenLayoutId", Order = 0)]
        public int ScreenLayoutId { get; set; }
        [Column("CustomerRefId", Order = 1)]
        public int CustomerRefId { get; set; }
        [Column("LayoutRefId", Order = 2)]
        public int LayoutRefId { get; set; }
        [Column("ActivitiesRegistrationRefId", Order = 3)]
        public int ActivitiesRegistrationRefId { get; set; }
        [Column("ScreenLayoutPosition", Order = 4)]
        public int ScreenLayoutPosition { get; set; }
        [Column("CreatedBy", Order = 5)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 6)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 7)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 8)]
        public DateTime? UpdatedOn { get; set; }
        [ForeignKey("CustomerRefId")]
        public Customers Customers { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRegistrationRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
    }


    public class ShowLayoutViewModel
    {
       public List<ActivitiesRegistration> ActivitiesRegistrations { get; set; }
       public List<Questions> Questions { get; set; }
  
       public List<QuestionPathShowLayoutList> QuestionPathShowLayoutLists { get; set; }

       public string name { get; set; }


    }

    public class QuestionPathShowLayoutList
    {

        public int QuestionId { get; set; }

        public int ActivityId { get; set; }

        public string ActivityName { get; set; }

        public string WorkingCode { get; set; }

        public bool Confirmbutton { get; set; }

        public string QuestionName { get; set; }

         public int ParentId { get; set; }
 


    }

}

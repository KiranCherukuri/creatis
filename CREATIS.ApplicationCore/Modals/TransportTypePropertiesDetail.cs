﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("TransportTypePropertiesDetail")]
    public class TransportTypePropertiesDetail
    {
        [Key]
        public int TransportTypeDetailId { get; set; }
        public int TransportTypeRefId { get; set; }
        public int QuestionRefId { get; set; }
        public string Title { get; set; }
        public string InfoColumn { get; set; }
        public string InfoColumnAction { get; set; }
        public string PlanningFeedbackCode { get; set; }
        public string PartnerCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        public int DashboardTransportTypeId { get; set; }
    }
}

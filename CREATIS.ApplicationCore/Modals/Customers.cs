﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("Customers")]
    public class Customers
    {
        [Key]
        public int CustomerId { get; set; }
        public string Source { get; set; }
        public int SourceId { get; set; }
        public string Environment { get; set; }
        public string Name { get; set; }
        public int Active { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public ICollection<Layout> LayoutDetails { get; set; }
        public ICollection<InfoColumns> InfoCol { get; set; }
        public ICollection<Integrator> IntegratorDetails { get; set; }
        public ICollection<CustomerNotes> CustomersNote { get; set; }
        public ICollection<OBCType> OBCTypes { get; set; }
        public ICollection<UsedLanguages> UsedLanguages { get; set; }
        public ICollection<TransportTypes> TransportTypes { get; set; }
        public ICollection<PlanningType> PlanningType { get; set; }
        public ICollection<PlanningSpecification> PlanningSpecification { get; set; }
        public ICollection<Allowances> Allowances { get; set; }
        public ICollection<ActivitiesRegistration> ActivitiesRegistration { get; set; }
        public ICollection<ScreenLayout> ScreenLayout { get; set; }
        [NotMapped]
        public string IsActive { get; set; }
        public int CustomCustomer { get; set; }
        public string MyTrxCompanyId { get; set; }
        public string MyTrxActive { get; set; }
        public DateTime? MyTrxModifiedDateTime { get; set; }
        [NotMapped]
        public string CreatedBy { get; set; }
        public int? CustomerSystemTypeRefId { get; set; }
        [NotMapped]
        public string   CustomerSystemTypeName { get; set; }
    }
}

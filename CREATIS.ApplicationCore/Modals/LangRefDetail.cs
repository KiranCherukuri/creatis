﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("LangRefDetail")]
    public class LangRefDetail
    {
        [Key]
        [Column("Id", Order = 0)]
        public int Id { get; set; }
        [Column("LangRefColTableId", Order = 1)]
        public int LangRefColTableId { get; set; }
        [Column("RefId", Order = 2)]
        public int RefId { get; set; }
        [Column("LayoutRefId", Order = 3)]
        public int? LayoutRefId { get; set; }
        [Column("CustomerRefId", Order = 4)]
        public int? CustomerRefId { get; set; }
        [Column("CreatedBy", Order = 5)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 6)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 7)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 8)]
        public DateTime UpdatedOn { get; set; }
        [Column("LangDetailId", Order = 9)]
        public int? LangDetailId { get; set; }
        public int? QuestionRefId { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("CustomerRefId")]
        public Customers Customers { get; set; }
        [ForeignKey("LangRefColTableId")]
        public LangRefColTable LangRefColTable { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        [NotMapped]
        public string Language { get; set; }
        [NotMapped]
        public string LangText { get; set; }
        [NotMapped]
        public int IsModification { get; set; }
        [NotMapped]
        public int? NewLatoutRefId { get; set; }
        [NotMapped]
        public int? NewRefId { get; set; }
    }
}

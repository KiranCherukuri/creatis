﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("InfoColumns")]
    public class InfoColumns
    {
        [Key]
        public int IC_ID { get; set; }
        public int? CustomerRefId { get; set; }
        public int? ICM_Column_Ref_No { get; set; }
        [Required(AllowEmptyStrings = true)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string IC_Column_Name { get; set; }
        public int? IS_Active { get; set; }
        [MaxLength(50)]
        [Required(AllowEmptyStrings = true)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        [MaxLength(50)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        [ForeignKey("CustomerRefId")]
        public Customers Customers { get; set; }
        [ForeignKey("ICM_Column_Ref_No")]
        public InfoColumnsMaster InfoColumnsMaster { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace CREATIS.ApplicationCore.Modals
{
    public class Tooltip
    {
        [Key]
        public int TooltipId { get; set; }
        public int UITranslationRefId { get; set; }
        public string WizardTooltips { get; set; }
        public string ExtendedHelp { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        [NotMapped]
        public string Title { get; set; }
        [NotMapped]
        public string PropertyName { get; set; }
        [NotMapped]
        public bool IsExtendedHelp { get; set; }

    }
}

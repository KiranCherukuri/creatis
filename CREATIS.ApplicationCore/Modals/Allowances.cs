﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("Allowances")]
    public class Allowances
    {
        [Key]
        public int AllowancesId { get; set; }
        public int CustomerRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int AllowancesRefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int IsModification { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("CustomerRefId")]
        public Customers Customers { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("AllowancesRefId")]
        public AllowancesMaster AllowancesMaster { get; set; }

        [NotMapped]
        public int IsQPUsed { get; set; }

        [NotMapped]
        public string AllowancesMasterDescription { get; set; }
        [NotMapped]
        public string AllowancesCode { get; set; }
      
    }
}

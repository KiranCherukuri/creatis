﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("Picure")]
    public class Picure
    {
        [Key]
        [Column("PicureID", Order = 0)]
        public int? PicureID { get; set; }
        [Column("ActivityRefID", Order = 1)]
        public int ActivityRefID { get; set; }
        [Column("LayoutRefID", Order = 2)]
        public int LayoutRefID { get; set; }
        [Column("CreatedBy", Order = 3)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 4)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 5)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 6)]
        public DateTime? UpdatedOn { get; set; }
        [Column("QuestionRefID", Order = 7)]
        public int QuestionRefID { get; set; }
        [Column("Title", Order = 8)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(20)]
        public string Title { get; set; }
        [Column("AlternativetextonPlacelevel", Order = 9)]
        [MaxLength(15)]
        public string AlternativetextonPlacelevel { get; set; }
        [Column("AlternativetextonJoblevel", Order = 10)]
        [MaxLength(15)]
        public string AlternativetextonJoblevel { get; set; }
        [Column("Alternativetextonproductlevel", Order = 11)]
        [MaxLength(15)]
        public string Alternativetextonproductlevel { get; set; }
    }
}

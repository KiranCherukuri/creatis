﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("WorkingCode")]
    public class WorkingCode
    {
        [Key]
        [Column("WorkingCodeId", Order = 0)]
        public int WorkingCodeId { get; set; }
        [Column("WorkingCodeDescription", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        public string WorkingCodeDescription { get; set; }
        [Column("CreatedBy", Order = 2)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 3)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 4)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 5)]
        public DateTime? UpdatedOn { get; set; }
    }
}

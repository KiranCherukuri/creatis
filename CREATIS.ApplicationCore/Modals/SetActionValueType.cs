﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class SetActionValueType
    {
        [Key]
        [Column("Id", Order = 0)]
        public int Id { get; set; }
        [Required(AllowEmptyStrings = true)]
        [Column("Name", Order = 1)]
        [MaxLength(50)]
        public string Name { get; set; }
        [Column("IsActive", Order = 2)]
        public bool IsActive { get; set; }
        [Required(AllowEmptyStrings = true)]
        [Column("CreatedBy", Order = 3)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 4)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 5)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 6)]
        public DateTime? UpdatedOn { get; set; }
        public bool IsFlexSetActionValue { get; set; }
    }
}

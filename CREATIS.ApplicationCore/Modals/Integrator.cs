﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("Integrator")]
    public class Integrator
    {
        [Key]
        public int I_ID { get; set; }
        public int CustomerRefId { get; set; }
        public string I_Details { get; set; }
        public int IS_Active { get; set; }
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        [ForeignKey("CustomerRefId")]
        public Customers Customers { get; set; }
    }
}

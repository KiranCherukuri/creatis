﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class FilterDetail
    {
        [Key]
        public int FilterDetailId { get; set; }
        public string TokenId { get; set; }
        public int CommentId { get; set; }
        public string FilterMethod { get; set; }
        public int SavedValue { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        [NotMapped]
        public string SavedValueName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("SignaturesProperties")]
    public class SignaturesProperties
    {
        [Key]
        [Column("SignaturesPropertiesID", Order = 0)]
        public int? SignaturesPropertiesID { get; set; }
        [Column("ActivityRefID", Order = 1)]
        public int ActivityRefID { get; set; }
        [Column("LayoutRefID", Order = 2)]
        public int LayoutRefID { get; set; }
        [Column("CreatedBy", Order = 5)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 6)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 7)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 8)]
        public DateTime? UpdatedOn { get; set; }
        [Column("QuestionRefID", Order = 9)]
        public int QuestionRefID { get; set; }
        [Column("Title", Order = 10)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(20)]
        public string Title { get; set; }
        [Column("AlternativetextonPlacelevel", Order = 11)]
        [MaxLength(15)]
        public string AlternativetextonPlacelevel { get; set; }
        [Column("AlternativetextonJoblevel", Order = 12)]
        [MaxLength(15)]
        public string AlternativetextonJoblevel { get; set; }
        [Column("Alternativetextonproductlevel", Order = 13)]
        [MaxLength(15)]
        public string Alternativetextonproductlevel { get; set; }
        public int? CopyLayoutRefId { get; set; }
        public bool PlaceLevel { get; set; }
        public bool JobLevel { get; set; }
    }
}

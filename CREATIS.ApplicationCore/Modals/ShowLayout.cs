﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CREATIS.ApplicationCore.Modals
{
    public class ShowLayout
    {
        [Key]
        public int QuestionId { get; set; }
        public int LayoutRefId { get; set; }
        public int ActivitiesRefId { get; set; }
        public int TransporttypeRefId { get; set; }
        public int? CustomerRefId { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public string LanguageName { get; set; }
        public string PropertyName { get; set; }
        public int? ISModification { get; set; }
        public string Nodetype { get; set; }
        public bool? ConfirmButton { get; set; }
        public string WorkingCodeRefId { get; set; }
        public int? QuestionPathCount { get; set; }
        public string Info { get; set; }
        public string FeedBackType { get; set; }
        public string PartnerCode { get; set; }
        public string TransportType { get; set; }
        public int? CopyLayoutRefId { get; set; }
        public int? DefaultLanguageId { get; set; } 
        public bool IsQuestionPath { get; set; } 
        public bool IsFlex { get; set; } 
        public bool IsSmart { get; set; } 
        public bool? IsParent { get; set; } //KC
        public bool? IsDefault { get; set; } //KC
        public bool? IsChild { get; set; } //KC
        public int? ISActivityModification { get; set; }
        public int? Position { get; set; }
        public bool? Visible { get; set; }
        public string PTO { get; set; }
        public string InterruptedBy { get; set; }
        public string Arabic { get; set; }
        public string Bulgarian { get; set; }
        public string Croatian { get; set; }
        public string Czech { get; set; }
        public string Danish { get; set; }
        public string Dutch { get; set; }
        public string English { get; set; }
        public string Estonian { get; set; }
        public string Finnish { get; set; }
        public string French { get; set; }
        public string German { get; set; }
        public string Greek { get; set; }
        public string Hungarian { get; set; }
        public string Italian { get; set; }
        public string Latvian { get; set; }
        public string Lithuanian { get; set; }
        public string Macedonian { get; set; }
        public string Norwegian { get; set; }
        public string Polish { get; set; }
        public string Portuguese { get; set; }
        public string Romanian { get; set; }
        public string Russian { get; set; }
        public string Slovak { get; set; }
        public string Slovene { get; set; }
        public string Spanish { get; set; }
        public string Swedish { get; set; }
        public string Turkish { get; set; }
        public string Ukrainian { get; set; }
        public string Belarusian { get; set; }
        public string LayoutName { get; set; }
        public string PlanningOverviewRefId { get; set; }
        public string SpecificRefId { get; set; }
        public bool EmptyFullSolo { get; set; }
        public bool IsTransportType { get; set; }
        public string PlanningRefId { get; set; }
        public string Comment { get; set; }
        public bool Flexactivity { get; set; }
        public int IsModificationActivitiesRegistrationName { get; set; }
        public int IsModificationWorkingCodeRefId { get; set; }
        public int IsModificationPlanningRefId { get; set; }
        public int IsModificationPlanningOverviewRefId { get; set; }
        public int IsModificationSpecificRefId { get; set; }
        public int IsModificationVisible { get; set; }
        public int IsModificationConfirmButton { get; set; }
        public int IsModificationTransportType { get; set; }
        public int IsModificationEmptyFullSolo { get; set; }
        public int IsModificationFlexactivity { get; set; }
        public int IsModificationPTORefId { get; set; }
        public int IsModificationInterruptibleByRefId { get; set; }

    }
}

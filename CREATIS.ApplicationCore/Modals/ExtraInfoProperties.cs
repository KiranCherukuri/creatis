﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ExtraInfoProperties")]
    public class ExtraInfoProperties
    {
        [Key]
        [Column("ExtraInfoPropertiesID", Order = 0)]
        public int? ExtraInfoPropertiesID { get; set; }
        [Column("ActivityRefID", Order = 1)]
        public int ActivityRefID { get; set; }
        [Column("LayoutRefID", Order = 2)]
        public int LayoutRefID { get; set; }
        [Column("CreatedBy", Order = 9)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 10)]
        public DateTime CreatedOn { get; set; }
        [Column("Updatedby", Order = 11)]
        [MaxLength(50)]
        public string Updatedby { get; set; }
        [Column("UpdatedOn", Order = 12)]
        public DateTime? UpdatedOn { get; set; }
        [Column("QuestionRefID", Order = 13)]
        public int QuestionRefID { get; set; }
        [Column("Title", Order = 14)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(20)]
        public string Title { get; set; }
        [Column("AlternativetextonPlacelevel", Order = 15)]
        [MaxLength(15)]
        public string AlternativetextonPlacelevel { get; set; }
        [Column("AlternativetextonJoblevel", Order = 16)]
        [MaxLength(15)]
        public string AlternativetextonJoblevel { get; set; }
        [Column("Alternativetextonproductlevel", Order = 17)]
        [MaxLength(15)]
        public string Alternativetextonproductlevel { get; set; }

        public int? CopyLayoutRefId { get; set; }
    }
}

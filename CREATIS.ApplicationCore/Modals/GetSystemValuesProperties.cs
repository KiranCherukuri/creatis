﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{

    [Table("GetSystemValuesProperties")]
    public class GetSystemValuesProperties
    {
        [Key]
        public int GetSystemValuesId { get; set; }
        public int ActivitiesRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int QuestionRefId { get; set; }
        public string Name { get; set; }
        public int GetSystemValuesRefId { get; set; }
        public int? GetSystemValuesFormatRefId { get; set; }
        public int SaveTo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string Comment { get; set; }
        public bool IsDefaultAlaisName { get; set; }

        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }

        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }

        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }

        [ForeignKey("GetSystemValuesRefId")]
        public GetSystemValues GetSystemValues { get; set; }

        [ForeignKey("GetSystemValuesFormatRefId")]
        public GetSystemValuesFormat GetSystemValuesFormat { get; set; }
        [NotMapped]
        public string SaveToText { get; set; }
        [NotMapped]
        public string SaveToValue { get; set; }
    }
}

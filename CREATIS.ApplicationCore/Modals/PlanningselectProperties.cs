﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("PlanningselectProperties")]
    public class PlanningselectProperties
    {
        [Key]
        [Column("PlanningselectId", Order = 0)]
        public int? PlanningselectId { get; set; }
        [Column("QuestionRefId", Order = 1)]
        public int QuestionRefId { get; set; }
        [Column("LayoutRefId", Order = 2)]
        public int LayoutRefId { get; set; }
        [Column("ActivitiesRefId", Order = 3)]
        public int ActivitiesRefId { get; set; }
        [Column("Title", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string Title { get; set; }
        [Column("Planninglevel", Order = 5)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string Planninglevel { get; set; }
        [Column("Planningspecific", Order = 6)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string Planningspecific { get; set; }
        [Column("Chooseactivity", Order = 7)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string Chooseactivity { get; set; }
        [Column("Startselecteditem", Order = 8)]
        public int Startselecteditem { get; set; }
        [Column("Other", Order = 7)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(100)]
        public string Other { get; set; }
        [Column("OtherTitle", Order = 8)]
        [MaxLength(100)]
        public string OtherTitle { get; set; }
        [Column("Fixedvalue", Order = 9)]
        [MaxLength(100)]
        public string Fixedvalue { get; set; }
        [Column("QP", Order = 10)]
        [MaxLength(100)]
        public string QP { get; set; }
        [Column("Planningreadout", Order = 11)]
        [MaxLength(100)]
        public string Planningreadout { get; set; }
        [Column("CreatedBy", Order = 12)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 13)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 14)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 15)]
        public DateTime? UpdatedOn { get; set; }
        [Column("StartSelection", Order = 16)]
        public string StartSelection { get; set; }
        [Column("FilterRefId", Order = 17)]
        [MaxLength(100)]
        public string FilterRefId { get; set; }
        [Column("CompareMethodRefId", Order = 18)]
        public int? CompareMethodRefId { get; set; }
        [Column("InstructionSetSavedValues", Order = 19)]
        public int? InstructionSetSavedValues { get; set; }
        [Column("PSComparetoRefId", Order = 20)]
        public int? PSComparetoRefId { get; set; }
        [Column("CheckOffPlanning", Order = 21)]
        [MaxLength(100)]
        public string CheckOffPlanning { get; set; }

        [Column("OtherMaskText", Order = 23)]
        [MaxLength(100)]
        public string OtherMaskText { get; set; }

        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        public string Comment { get; set; }
        [NotMapped]
        public string TokenId { get; set; }
        public string PlanningSelectionMethod { get; set; }


    }
}

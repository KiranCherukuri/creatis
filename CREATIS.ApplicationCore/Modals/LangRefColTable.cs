﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("LangRefColTable")]
    public class LangRefColTable
    {
        [Key]
        [Column("Id", Order = 0)]
        public int Id { get; set; }
        [Column("LangRefTableId", Order = 1)]
        public int LangRefTableId { get; set; }
        [Column("Name", Order = 2)]
        public string Name { get; set; }
        [Column("CreatedBy", Order = 3)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 4)]
        public DateTime CreatedOn { get; set; }
        [Column("ItemName", Order = 5)]
        public string ItemName { get; set; }
        [ForeignKey("LangRefTableId")]
        public LangRefTable LangRefTable { get; set; }
    }
}

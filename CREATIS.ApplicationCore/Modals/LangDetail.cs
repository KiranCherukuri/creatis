﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("LangDetail")]
    public class LangDetail
    {
        [Key]
        public int Id { get; set; }
        public string Arabic { get; set; }
        public string Bulgarian { get; set; }
        public string Croatian { get; set; }
        public string Czech { get; set; }
        public string Danish { get; set; }
        public string Dutch { get; set; }
        public string English { get; set; }
        public string Estonian { get; set; }
        public string Finnish { get; set; }
        public string French { get; set; }
        public string German { get; set; }
        public string Greek { get; set; }
        public string Hungarian { get; set; }
        public string Italian { get; set; }
        public string Latvian { get; set; }
        public string Lithuanian { get; set; }
        public string Macedonian { get; set; }
        public string Norwegian { get; set; }
        public string Polish { get; set; }
        public string Portuguese { get; set; }
        public string Romanian { get; set; }
        public string Russian { get; set; }
        public string Slovak { get; set; }
        public string Slovene { get; set; }
        public string Spanish { get; set; }
        public string Swedish { get; set; }
        public string Turkish { get; set; }
        public string Ukrainian { get; set; }
        public string Belarusian { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int IsModificationArabic { get; set; }
        public int IsModificationBulgarian { get; set; }
        public int IsModificationCroatian { get; set; }
        public int IsModificationCzech { get; set; }
        public int IsModificationDanish { get; set; }
        public int IsModificationDutch { get; set; }
        public int IsModificationEnglish { get; set; }
        public int IsModificationEstonian { get; set; }
        public int IsModificationFinnish { get; set; }
        public int IsModificationFrench { get; set; }
        public int IsModificationGerman { get; set; }
        public int IsModificationGreek { get; set; }
        public int IsModificationHungarian { get; set; }
        public int IsModificationItalian { get; set; }
        public int IsModificationLatvian { get; set; }
        public int IsModificationLithuanian { get; set; }
        public int IsModificationMacedonian { get; set; }
        public int IsModificationNorwegian { get; set; }
        public int IsModificationPolish { get; set; }
        public int IsModificationPortuguese { get; set; }
        public int IsModificationRomanian { get; set; }
        public int IsModificationRussian { get; set; }
        public int IsModificationSlovak { get; set; }
        public int IsModificationSlovene { get; set; }
        public int IsModificationSpanish { get; set; }
        public int IsModificationSwedish { get; set; }
        public int IsModificationTurkish { get; set; }
        public int IsModificationUkrainian { get; set; }
        public int IsModificationBelarusian { get; set; }
        public int CustomerRefId { get; set; }
        public int IsDefaultTranslation { get; set; }
        public int? LangRefColTableId { get; set; }
        [NotMapped]
        public int? LayoutRefId { get; set; }
        [NotMapped]
        public int RefId { get; set; }
        [NotMapped]
        public bool IsShowEnglish { get; set; }
        [NotMapped]
        public bool IsShowArabic { get; set; }
        [NotMapped]
        public bool IsShowBulgarian { get; set; }
        [NotMapped]
        public bool IsShowCroatian { get; set; }
        [NotMapped]
        public bool IsShowCzech { get; set; }
        [NotMapped]
        public bool IsShowDanish { get; set; }
        [NotMapped]
        public bool IsShowDutch { get; set; }
        [NotMapped]
        public bool IsShowEstonian { get; set; }
        [NotMapped]
        public bool IsShowFinnish { get; set; }
        [NotMapped]
        public bool IsShowFrench { get; set; }
        [NotMapped]
        public bool IsShowGerman { get; set; }
        [NotMapped]
        public bool IsShowGreek { get; set; }
        [NotMapped]
        public bool IsShowHungarian { get; set; }
        [NotMapped]
        public bool IsShowItalian { get; set; }
        [NotMapped]
        public bool IsShowLatvian { get; set; }
        [NotMapped]
        public bool IsShowLithuanian { get; set; }
        [NotMapped]
        public bool IsShowMacedonian { get; set; }
        [NotMapped]
        public bool IsShowNorwegian { get; set; }
        [NotMapped]
        public bool IsShowPolish { get; set; }
        [NotMapped]
        public bool IsShowPortuguese { get; set; }
        [NotMapped]
        public bool IsShowRomanian { get; set; }
        [NotMapped]
        public bool IsShowRussian { get; set; }
        [NotMapped]
        public bool IsShowSlovak { get; set; }
        [NotMapped]
        public bool IsShowSlovene { get; set; }
        [NotMapped]
        public bool IsShowSpanish { get; set; }
        [NotMapped]
        public bool IsShowSwedish { get; set; }
        [NotMapped]
        public bool IsShowTurkish { get; set; }
        [NotMapped]
        public bool IsShowUkrainian { get; set; }
        [NotMapped]
        public bool IsShowBelarusian { get; set; }
        [NotMapped]
        public int IsDefaultEnglish { get; set; }
        [NotMapped]
        public int IsDefaultArabic { get; set; }
        [NotMapped]
        public int IsDefaultBulgarian { get; set; }
        [NotMapped]
        public int IsDefaultCroatian { get; set; }
        [NotMapped]
        public int IsDefaultCzech { get; set; }
        [NotMapped]
        public int IsDefaultDanish { get; set; }
        [NotMapped]
        public int IsDefaultDutch { get; set; }
        [NotMapped]
        public int IsDefaultEstonian { get; set; }
        [NotMapped]
        public int IsDefaultFinnish { get; set; }
        [NotMapped]
        public int IsDefaultFrench { get; set; }
        [NotMapped]
        public int IsDefaultGerman { get; set; }
        [NotMapped]
        public int IsDefaultGreek { get; set; }
        [NotMapped]
        public int IsDefaultHungarian { get; set; }
        [NotMapped]
        public int IsDefaultItalian { get; set; }
        [NotMapped]
        public int IsDefaultLatvian { get; set; }
        [NotMapped]
        public int IsDefaultLithuanian { get; set; }
        [NotMapped]
        public int IsDefaultMacedonian { get; set; }
        [NotMapped]
        public int IsDefaultNorwegian { get; set; }
        [NotMapped]
        public int IsDefaultPolish { get; set; }
        [NotMapped]
        public int IsDefaultPortuguese { get; set; }
        [NotMapped]
        public int IsDefaultRomanian { get; set; }
        [NotMapped]
        public int IsDefaultRussian { get; set; }
        [NotMapped]
        public int IsDefaultSlovak { get; set; }
        [NotMapped]
        public int IsDefaultSlovene { get; set; }
        [NotMapped]
        public int IsDefaultSpanish { get; set; }
        [NotMapped]
        public int IsDefaultSwedish { get; set; }
        [NotMapped]
        public int IsDefaultTurkish { get; set; }
        [NotMapped]
        public int IsDefaultUkrainian { get; set; }
        [NotMapped]
        public int IsDefaultBelarusian { get; set; }
        [NotMapped]
        public int LangRefTableId { get; set; }
        [NotMapped]
        public string TableName { get; set; }
        [NotMapped]
        public string ColumnName { get; set; }
        [NotMapped]
        public List<UsedLanguageList> UsedLanguageList { get; set; }
        [NotMapped]
        public List<LangDetail> langDetailList { get; set; }
        [NotMapped]
        public string Language { get; set; }
        [NotMapped]
        public string SelectedLanguage { get; set; }
        [NotMapped]
        public string LangText { get; set; }
        [NotMapped]
        public string WorkingCode { get; set; }
        [NotMapped]
        public int LangRefDetailId { get; set; }
        [NotMapped]
        public string PropertiesPath { get; set; }
        [NotMapped]
        public string PropertiesName { get; set; }
        [NotMapped]
        public string CopyColorCode { get; set; }

        [NotMapped] public string IsModificationColorArabic { get; set; }
        [NotMapped] public string IsModificationColorBulgarian { get; set; }
        [NotMapped] public string IsModificationColorCroatian { get; set; }
        [NotMapped] public string IsModificationColorCzech { get; set; }
        [NotMapped] public string IsModificationColorDanish { get; set; }
        [NotMapped] public string IsModificationColorDutch { get; set; }
        [NotMapped] public string IsModificationColorEnglish { get; set; }
        [NotMapped] public string IsModificationColorEstonian { get; set; }
        [NotMapped] public string IsModificationColorFinnish { get; set; }
        [NotMapped] public string IsModificationColorFrench { get; set; }
        [NotMapped] public string IsModificationColorGerman { get; set; }
        [NotMapped] public string IsModificationColorGreek { get; set; }
        [NotMapped] public string IsModificationColorHungarian { get; set; }
        [NotMapped] public string IsModificationColorItalian { get; set; }
        [NotMapped] public string IsModificationColorLatvian { get; set; }
        [NotMapped] public string IsModificationColorLithuanian { get; set; }
        [NotMapped] public string IsModificationColorMacedonian { get; set; }
        [NotMapped] public string IsModificationColorNorwegian { get; set; }
        [NotMapped] public string IsModificationColorPolish { get; set; }
        [NotMapped] public string IsModificationColorPortuguese { get; set; }
        [NotMapped] public string IsModificationColorRomanian { get; set; }
        [NotMapped] public string IsModificationColorRussian { get; set; }
        [NotMapped] public string IsModificationColorSlovak { get; set; }
        [NotMapped] public string IsModificationColorSlovene { get; set; }
        [NotMapped] public string IsModificationColorSpanish { get; set; }
        [NotMapped] public string IsModificationColorSwedish { get; set; }
        [NotMapped] public string IsModificationColorTurkish { get; set; }
        [NotMapped] public string IsModificationColorUkrainian { get; set; }
        [NotMapped] public string IsModificationColorBelarusian { get; set; }
        [NotMapped] public string CustomerName { get; set; }
        [NotMapped] public int? QuestionRefId { get; set; }
        [NotMapped]
        public string Code { get; set; }

    }
    public class UsedLanguageInfo
    {
        public int LanguageId { get; set; }
        public string LanguageName { get; set; }
        public int LanguageRefId { get; set; }
        public int LayoutId { get; set; }
        public int IdDefault { get; set; }
    }

    public class UsedLanguageList
    {
        public int LanguageId { get; set; }
        public string LanguageName { get; set; }
        public int LanguageRefId { get; set; }
        public int LayoutId { get; set; }
        public int IdDefault { get; set; }


    }
    public class UsedLangDetail
    {
        public int? RefId { get; set; }
        public string LangText { get; set; }
    }
}

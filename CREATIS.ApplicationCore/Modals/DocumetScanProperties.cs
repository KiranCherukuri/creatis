﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("DocumetScanProperties")]
    public class DocumetScanProperties
    {
        [Key]
        [Column("DocumetScanId", Order = 0)]
        public int? DocumetScanId { get; set; }
        [Column("QuestionRefId", Order = 1)]
        public int QuestionRefId { get; set; }
        [Column("LayoutRefId", Order = 2)]
        public int LayoutRefId { get; set; }
        [Column("ActivitiesRefId", Order = 3)]
        public int ActivitiesRefId { get; set; }
        [Column("Title", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string Title { get; set; }
        [Column("PlanningFeedbackLevel", Order = 5)]
        [MaxLength(50)]
        public string PlanningFeedbackLevel { get; set; }
        [Column("CreatedBy", Order = 6)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 7)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 8)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 9)]
        public DateTime? UpdatedOn { get; set; }
        [Column("DocumentName", Order = 10)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string DocumentName { get; set; }
        [Column("Conditionitem", Order = 23)]
        public int? Conditionitem { get; set; }
        [Column("ConCompareMethod", Order = 24)]
        public int? ConCompareMethod { get; set; }
        [Column("Convalue", Order = 25)]
        public int? Convalue { get; set; }
        [Column("ConItemSavedValued", Order = 29)]
        public int? ConItemSavedValued { get; set; }
        [Column("ConvalueSavedValue", Order = 30)]
        public int? ConvalueSavedValue { get; set; }
        [Column("ComFixedText", Order = 22)]
        [MaxLength(100)]
        public string ComFixedText { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        public string Comment { get; set; }
    }
}

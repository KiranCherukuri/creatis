﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("TrailerProperties")]
    public class TrailerProperties
    {
        [Key]
        public int TrailerId { get; set; }
        public int ActivitiesRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int QuestionRefId { get; set; }
        public string ActionType { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string TrailerFormat { get; set; }
        public string MaskText { get; set; }
        public int? Size { get; set; }
        public string Regex { get; set; }
        public bool? NullNotAllowed { get; set; }
        public bool? ZeroNotAllowed { get; set; }
        public bool? MinimumofCharacter { get; set; }
        public int? MinimumofCharacterValue { get; set; }
        public bool? ExactOfCharacter { get; set; }
        public bool? Save { get; set; }
        public int? SaveTo { get; set; }
        public int? Propose { get; set; }
        public bool? AutoGenerate { get; set; }
        public string PlanningFeedback { get; set; }
        public string FeedbackType { get; set; }
        public string PlanningFeedbackCode { get; set; }
        public string PartnerCode { get; set; }
        public bool? Other { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string Comment { get; set; }

        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        public int? CopyLayoutRefId { get; set; }
        [NotMapped]
        public string   PropertyName { get; set; }
    }
}

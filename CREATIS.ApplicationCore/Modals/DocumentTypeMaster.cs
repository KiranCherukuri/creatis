﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("DocumentTypeMaster")]
    public class DocumentTypeMaster
    {
        [Key]
        [Column("DocumentTypeId", Order = 0)]
        public int DocumentTypeId { get; set; }
        [Column("DocumentTypeCode", Order = 1)]
        public string DocumentTypeCode { get; set; }
        [Column("DocumentTypeDescription", Order = 2)]
        public string DocumentTypeDescription { get; set; }
        [Column("IsActive", Order = 3)]
        public int IsActive { get; set; }
        [Required(AllowEmptyStrings = true)]
        [Column("CreatedBy", Order = 4)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 5)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 6)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 7)]
        public DateTime? UpdatedOn { get; set; }
        [NotMapped]
        public string Active { get; set; }
        [NotMapped]
        public string Name { get; set; }

    }
}

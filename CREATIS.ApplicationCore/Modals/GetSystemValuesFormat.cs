﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("GetSystemValuesFormat")]
    public class GetSystemValuesFormat
    {
        [Key]
        public int GetSystemValuesFormatId { get; set; }
        public string Name { get; set; }
        public int GetSystemValuesRefId { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        [ForeignKey("GetSystemValuesRefId")]
        public GetSystemValues GetSystemValues { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("LangRefTable")]
    public class LangRefTable
    {
        [Key]
        [Column("Id", Order = 0)]
        public int Id { get; set; }
        [Column("Name", Order = 1)]
        public string Name { get; set; }
        [Column("CreatedBy", Order = 2)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 3)]
        public DateTime CreatedOn { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace CREATIS.ApplicationCore.Modals
{
    public class TooltipImage
    {
        [Key]
        public int TooltipImageId { get; set; }
        public int TooltipRefId { get; set; }
        public string Title { get; set; }
        public string Path { get; set; }
        public string RootPath { get; set; }
        public string OriginalFileName { get; set; }
        public string SystemFileName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("Signatures")]
    public class Signatures
    {
        [Key]
        [Column("SignaturesID", Order = 0)]
        public int? SignaturesID { get; set; }
        [Column("ActivityRefID", Order = 1)]
        public int ActivityRefID { get; set; }
        [Column("LayoutRefID", Order = 2)]
        public int LayoutRefID { get; set; }
        [Column("SignaturesRefId", Order = 15)]
        public int? SignaturesRefId { get; set; }
        [Column("PlaceLevel", Order = 3)]
        public bool PlaceLevel { get; set; }
        [Column("JobLevel", Order = 4)]
        public bool JobLevel { get; set; }
        [Column("CreatedBy", Order = 5)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 6)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 7)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 8)]
        public DateTime? UpdatedOn { get; set; }
        [Column("QuestionRefID", Order = 9)]
        public int QuestionRefID { get; set; }
        [Column("Title", Order = 10)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(20)]
        public string Title { get; set; }
       
    }
}

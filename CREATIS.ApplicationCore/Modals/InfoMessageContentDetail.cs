﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("InfoMessageContentDetail")]
    public class InfoMessageContentDetail
    {
        [Key]
        public int InfoMessageContentDetailId { get; set; }
        public int InfomessagePropertiesRefId { get; set; }
        public string MessageContent { get; set; }
        public int MessageContentLineNo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}

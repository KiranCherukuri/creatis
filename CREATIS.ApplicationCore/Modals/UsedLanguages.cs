﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class UsedLanguages
    {
        [Key]
        public int UsedLanguagesId { get; set; }
        public int CustomerRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int LanguageRefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int IsDefault { get; set; }
        public int IsModification { get; set; }
        public bool IsActive { get; set; }

        [ForeignKey("CustomerRefId")]
        public Customers Customers { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("LanguageRefId")]
        public LanguageMaster LanguageMaster { get; set; }
        [NotMapped]
        public List<LanguageMaster> LanguageMasters { get; set; }
        [NotMapped]
        public string LanguageSelectedId { get; set; }
        [NotMapped]
        public string ActiveList { get; set; }
        [NotMapped]
        public int TableId { get; set; }
    }
}

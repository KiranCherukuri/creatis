﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
   
     public class Layout
    {
        [Key]
        public int LayoutId { get; set; }
        public int CustomerRefId { get; set; }
        public string LayoutName { get; set; }
        public string LayoutType { get; set; }
        public string LayoutOrigin { get; set; }
        public int LayoutStatusRefId { get; set; }
        public string EmailId { get; set; }

        [ForeignKey("LayoutStatusRefId")]
        public LayoutStatus LayoutStatus { get; set; }

        public int IsLocked { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool IsCopyLayout { get; set; }
        public string RequesterEmailId { get; set; }
        public string InitiatorEmailId { get; set; }

        [ForeignKey("CustomerRefId")]
        public Customers Customers { get; set; }
        //LayoutType, CustomerId, Origin
    }
}

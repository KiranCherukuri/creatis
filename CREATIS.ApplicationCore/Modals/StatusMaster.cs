﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("StatusMaster")]
    public  class StatusMaster
    {
        [Key]
        public int Status_ID { get; set; }
        public string Status_Description { get; set; }
        public int Status_IsActive { get; set; }
    }
}

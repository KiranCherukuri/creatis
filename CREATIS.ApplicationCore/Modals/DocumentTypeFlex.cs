﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("DocumentTypeFlex")]
    public class DocumentTypeFlex
    {
        [Key]
        public int DocumentTypeFlexId { get; set; }
        public int DocumentTypeRefId { get; set; }
        public int LayoutRefId { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int IsModification { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("DocumentTypeRefId")]
        public DocumentTypeMaster DocumentTypeMaster { get; set; }
        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string CopyColorCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ConPSCompareto")]
    public class ConPSCompareto
    {
        [Key]
        [Column("PSComparetoId", Order = 0)]
        public int PSComparetoId { get; set; }
        [Column("PSComparetoDesc", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string PSComparetoDesc { get; set; }
        [Column("PSComparetoStatus", Order = 2)]
        public int PSComparetoStatus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ConFilter")]
    public class ConFilter
    {
        [Key]
        [Column("FilterId", Order = 0)]
        public int FilterId { get; set; }
        [Column("FilterDesc", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string FilterDesc { get; set; }
        [Column("FilterStatus", Order = 2)]
        public int FilterStatus { get; set; }
    }
}

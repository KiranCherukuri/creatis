﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ModulesDetail")]
    public class ModulesDetail
    {
        [Key]
        [Column("ModulesDetailId", Order = 0)]
        public int ModulesDetailId { get; set; }
        [Column("ModulesDetailCode", Order = 1)]
        public string ModulesDetailCode { get; set; }
        [Column("ModulesDetailName", Order = 2)]
        public string ModulesDetailName { get; set; }
        [Column("ModulesRefId", Order = 3)]
        public int ModulesRefId { get; set; }

        [Column("IsActive", Order = 4)]
        public bool IsActive { get; set; }
        [Column("CreatedBy", Order = 5)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 6)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 7)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 8)]
        public DateTime UpdatedOn { get; set; }

        [ForeignKey("ModulesRefId")]
        public Modules Modules { get; set; }

    }
}

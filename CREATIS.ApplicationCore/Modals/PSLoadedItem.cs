﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("PSLoadedItem")]
    public class PSLoadedItem
    {
        [Key]
        [Column("PSLoadedItemId", Order = 0)]
        public int PSLoadedItemId { get; set; }
        [Column("PSLoadedItemDesc", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string PSLoadedItemDesc { get; set; }
        [Column("ActivitiesRefID", Order = 2)]
        public int ActivitiesRefID { get; set; }
        [Column("QuestionRefId", Order = 3)]
        public int QuestionRefId { get; set; }
        [Column("PlanningSelectRefID", Order = 4)]
        public int PlanningSelectRefID { get; set; }
        [Column("ReadoutValueRefId", Order = 5)]
        public int ReadoutValueRefId { get; set; }
        [Column("Createdby", Order = 6)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string Createdby { get; set; }
        [Column("Createdon", Order = 7)]
        public DateTime Createdon { get; set; }
        [Column("Updatedtedby", Order = 8)]
        [MaxLength(50)]
        public string Updatedtedby { get; set; }
        [Column("Updatededon", Order = 9)]
        public DateTime? Updatededon { get; set; }
    }
}

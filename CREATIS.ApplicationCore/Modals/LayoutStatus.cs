﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("LayoutStatus")]
    public class LayoutStatus
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsRequestStatus { get; set; }
        public int IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsSentMailToInitiator { get; set; }
        public bool IsSentMailToRequester { get; set; }
        public bool IsSentMailToOwner { get; set; }
        public bool IsSentMailToCustomizing { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace CREATIS.ApplicationCore.Modals
{
    public class CommentDetail
    {
        [Key]
        public int CommentDetailId { get; set; }
        public string TokenId { get; set; }
        public int CommentId { get; set; }
        public string ReadoutRange { get; set; }
        public int? StartChar { get; set; }
        public int? CharLength { get; set; }
        public int SaveTo { get; set; }
        public bool IsDefaultAlaisName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        [NotMapped]
        public string StartCharValue { get; set; }
        [NotMapped]
        public string CharLengthValue { get; set; }
        [NotMapped]
        public string SaveToValue { get; set; }
        [NotMapped]
        public string SaveToText { get; set; }
        [NotMapped]
        public string DefaultAliasName { get; set; }

    }
}

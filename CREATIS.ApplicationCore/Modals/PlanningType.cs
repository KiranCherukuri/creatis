﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("PlanningType")]
    public class PlanningType
    {
        [Key]
        public int PlanningTypeId { get; set; }
        public int CustomerRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int PlanningTypeRefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int IsModification { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("CustomerRefId")]
        public Customers Customers { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("PlanningTypeRefId")]
        public PlanningTypeMaster PlanningTypeMaster { get; set; }
        [NotMapped]
        public string PlanningTypeName { get; set; }
    }
}

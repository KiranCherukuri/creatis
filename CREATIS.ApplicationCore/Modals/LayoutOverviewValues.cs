﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CREATIS.ApplicationCore.Modals
{
    public class LayoutOverviewValues
    {
        [Key]
        public int LayoutRefId { get; set; }
        public string OBCType { get; set; }
        public string LayoutName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string Name { get; set; }
        public string LayoutOrigin { get; set; }
        public DateTime? DateLastStatus { get; set; }
        public string StatusChangedBy { get; set; }
        public string UsedLanguages { get; set; }
        public string PlanningTypes { get; set; }
    }
}

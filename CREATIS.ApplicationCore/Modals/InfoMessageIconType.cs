﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("InfoMessageIconType")]
    public class InfoMessageIconType
    {
        [Key]
        public int InfoMessageIconTypeId { get; set; }
        public string InfoMessageIconTypeName { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        [NotMapped]
        public string Active { get; set; }

    }
}

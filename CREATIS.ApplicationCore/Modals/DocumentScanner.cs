﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CREATIS.ApplicationCore.Modals
{
   public class DocumentScanner
    {
        [Key]
        [Column("DocumentScannerID", Order = 0)]
        public int? DocumentScannerID { get; set; }
        [Column("ActivityRefID", Order = 1)]
        public int ActivityRefID { get; set; }
        [Column("QuestionRefID", Order = 2)]
        public int? QuestionRefID { get; set; }
        [Column("LayoutRefID", Order = 3)]
        public int LayoutRefID { get; set; }
        [Column("PlaceLevel", Order = 4)]
        public bool PlaceLevel { get; set; }
        [Column("JobLevel", Order = 5)]
        public bool JobLevel { get; set; }
        [Column("ProductLevel", Order = 6)]
        public bool ProductLevel { get; set; }
        [Column("CreatedBy", Order = 7)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 8)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 9)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 10)]
        public DateTime UpdatedOn { get; set; }
        [Column("Title", Order = 11)]
        public string Title { get; set; }
        [Column("DocumentScannerRefId", Order = 12)]
        public int? DocumentScannerRefId { get; set; }
        [Column("DocumentType", Order = 13)]
        public string DocumentType { get; set; }





    }
}

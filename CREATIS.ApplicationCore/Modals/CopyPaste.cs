﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("CopyPaste")]
    public class CopyPaste
    {
        [Key]
        [Column("CopyPasteId", Order = 0)]
        public int CopyPasteId { get; set; }
        [Column("CopiedItem", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        public string CopiedItem { get; set; }
        [Column("CreatedBy", Order = 2)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 3)]
        public DateTime CreatedOn { get; set; }
    }
}

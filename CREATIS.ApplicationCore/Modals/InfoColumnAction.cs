﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("InfoColumnAction")]
    public class InfoColumnAction
    {
        [Key]
        [Column("InfoColumnActionId", Order = 0)]
        public int InfoColumnActionId { get; set; }
        [Column("InfoColumnActionDesc", Order = 2)]
        [Required(AllowEmptyStrings = true)]
        public string InfoColumnActionDesc { get; set; }
        [Column("InfoColumnActionStatus", Order = 3)]
        public bool InfoColumnActionStatus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("AddDocumentScanProperties")]
    public class AddDocumentScanProperties
    {
        [Key]
        public int AddDocumentScanId { get; set; }
        public int DocumentScanRefId { get; set; }
        public string Title { get; set; }
        public int DocumentTypeMasterRefId { get; set; }
        public int QuestionRefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

        [ForeignKey("DocumentScanRefId")]
        public  DocumetScanProperties DocumetScanProperties { get; set; }
        [ForeignKey("DocumentTypeMasterRefId")]
        public DocumentTypeMaster DocumentTypeMaster { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }

    }
}

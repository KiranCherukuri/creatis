﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class PlanningTypeMaster
    {
        [Key]
        public int PlanningTypeMasterId { get; set; }
        public string PlanningTypeMasterDescription { get; set; }
        public int IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public ICollection<PlanningType> PlanningType { get; set; }
        public ICollection<PlanningSpecificationMaster> PlanningSpecificationMaster { get; set; }
        [NotMapped]
        public string Active { get; set; }
        [NotMapped]
        public string CopyColorCode { get; set; }
    }
}

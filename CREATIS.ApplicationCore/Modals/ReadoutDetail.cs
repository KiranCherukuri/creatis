﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("ReadoutDetail")]
    public class ReadoutDetail
    {
        [Key]
        public int ReadoutDetailId { get; set; }
        public string TokenId { get; set; }
        public int ReadoutValueRefId { get; set; }
        public int? ConfigID { get; set; }
        public int? CommentID { get; set; }
        public string ReadoutRange { get; set; }
        public int? StartChar { get; set; }
        public int? CharLength { get; set; }
        public string ReadoutExample { get; set; }
        public int? SaveTo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        [NotMapped]
        public string ReadoutValue { get; set; }
        [NotMapped]
        public string SaveToValue { get; set; }
        [NotMapped]
        public string Save { get; set; }
        [NotMapped]
        public string ConfigIDValue { get; set; }
        [NotMapped]
        public string CommentIDValue { get; set; }
        [NotMapped]
        public string StartCharValue { get; set; }
        [NotMapped]
        public string CharLengthValue { get; set; }
        public bool? IsDefaultAlaisName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("conditionvalue")]
    public class conditionvalue
    {
        [Key]
        [Column("ValueId", Order = 0)]
        public int ValueId { get; set; }
        [Column("ValueDesc", Order = 2)]
        [Required(AllowEmptyStrings = true)]
        public string ValueDesc { get; set; }
        [Column("ValueStatus", Order = 3)]
        public bool ValueStatus { get; set; }
        public bool IsFlexValue { get; set; }
    }
}

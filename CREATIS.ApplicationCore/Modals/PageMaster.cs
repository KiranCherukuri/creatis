﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("PageMaster")]
    public class PageMaster
    {
        [Key]
        [Column("PageId", Order = 0)]
        public int PageId { get; set; }
        [Column("PageName", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string PageName { get; set; }
        [Column("CreatedBy", Order = 2)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 3)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 4)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 5)]
        public DateTime UpdatedOn { get; set; }
    }
}

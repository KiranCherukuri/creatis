﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class AllowancesMaster
    {
        [Key]
        public int AllowancesMasterId { get; set; }
        public string AllowancesMasterDescription { get; set; }
        public string AllowancesCode { get; set; }
        public int OBCTypeRefId { get; set; }
        public int IS_Active { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        [ForeignKey("OBCTypeRefId")]
        public OBCTypeMaster OBCTypeMaster { get; set; }
        public int? CustomerRefId { get; set; }
        public int? CopyLayoutRefId { get; set; }
        [NotMapped]
        public string AllowancesMasterName { get; set; }
        [NotMapped]
        public string Active { get; set; }
        [NotMapped]
        public string OBCType { get; set; }
        [NotMapped]
        public string CopyColorCode { get; set; }
    }
}

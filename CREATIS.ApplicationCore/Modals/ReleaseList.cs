﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CREATIS.ApplicationCore.Modals
{
    public class ReleaseList
    {
        public int ReleaseListid { get; set; }
        public int Versiongroupid { get; set; }
        public string Product { get; set; }
        public string ProductVersion { get; set; }
        public string MobileTOSTypes { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string Comments { get; set; }
        public string RDStatusTypes { get; set; }
        public string InitiatorTypes { get; set; }
        public DateTime Releasedate { get; set; }
        public string StatusTypes { get; set; }
        public string EnvironmentTypes { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string AttachmentPath { get; set; }
        public int Status { get; set; }
        public string Createdby { get; set; }
        public System.DateTime Createdon { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string Updateby { get; set; }
        public System.DateTime? Updateon { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string Link { get; set; }
        public int Versionuniqueid { get; set; }
    }
}

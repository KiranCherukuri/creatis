﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("conditionCompareTo")]
    public class conditionCompareTo
    {
        [Key]
        [Column("CompareToId", Order = 0)]
        public int CompareToId { get; set; }
        [Column("CompareToDesc", Order = 2)]
        [Required(AllowEmptyStrings = true)]
        public string CompareToDesc { get; set; }
        [Column("CompareToStatus", Order = 3)]
        public bool CompareToStatus { get; set; }
        public bool IsSavedValue { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("UserEnvironment")]
    public class UserEnvironment
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        [Column("EnvironmentId")]
        public int EnvironmentId { get; set; }
        [Column("UserId")]
        public int UserId { get; set; }
        [Column("TxUserId")]
        public string TxUserId { get; set; }
        [Column("IsActive")]
        public int IsActive { get; set; }
        [Column("CreatedBy")]
        public string CreatedBy { get; set; }
        [Column("CreatedOn")]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy")]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn")]
        public DateTime? UpdatedOn { get; set; }
        [NotMapped]
        public string EnvironmentName { get; set; }
        [ForeignKey("UserId")]
        public Users Users { get; set; }
        [ForeignKey("EnvironmentId")]
        public ServerEnvironment ServerEnvironment { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CREATIS.ApplicationCore.Modals
{
    [Table("MainToolBox")]
    public class MainToolBox
    {
        [Key]
        public int ToolboxId { get; set; }
        public string ToolboxName { get; set; }
        public string ToolboxDescription { get; set; }
        public int ToolboxStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? Position { get; set; }
        public bool? IsSky { get; set; }
        public bool? IsSkyFlex { get; set; }
        public bool? IsSmart { get; set; }
        public bool? IsFlex { get; set; }
        public ICollection<ToolBoxSubItem> ToolBoxSubItem
        {
            get; set;
        }
    }
}

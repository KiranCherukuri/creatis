﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("PropertyDetails")]
    public class PropertyDetails
    {
        [Key]
        [Column("PropertyDetailsId", Order = 0)]
        public int PropertyDetailsId { get; set; }
        [Column("PropertyRefId", Order = 1)]
        public int PropertyRefId { get; set; }
        [Column("PropertyValue", Order = 2)]
        public string PropertyValue { get; set; }
        [Column("IsDefault", Order = 3)]
        public int IsDefault { get; set; }
        [Column("PropertyDetailsStatus", Order = 4)]
        public int PropertyDetailsStatus { get; set; }
        [Column("CreatedBy", Order = 5)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 6)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 7)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 8)]
        public DateTime? UpdatedOn { get; set; }
        [ForeignKey("PropertyRefId")]
        public PropertyMaster PropertyMaster { get; set; }
    }
}

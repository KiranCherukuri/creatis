﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("PTO")]
    public class PTO
    {
        [Key]
        [Column("PTOId", Order = 0)]
        public int PTOId { get; set; }
        [Column("PTODescription", Order = 1)]
        [Required(AllowEmptyStrings = true)]
        public string PTODescription { get; set; }
        [Column("CreatedBy", Order = 2)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 3)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 4)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 5)]
        public DateTime? UpdatedOn { get; set; }
    }
}

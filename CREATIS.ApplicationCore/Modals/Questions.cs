﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("Questions")]
    public class Questions
    {
        [Key]
        public int QuestionId { get; set; }
        public int LayoutRefId { get; set; }
        public int ActivitiesRefId { get; set; }
        public int TransporttypeRefId { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string PropertyName { get; set; }
        public int? ISModification { get; set; }
        public string Nodetype { get; set; }
        public string ActivitiesRegistrationName { get; set; }
        public bool? ConfirmButton { get; set; }
        public string WorkingCodeRefId { get; set; }
        public int? QuestionPathCount { get; set; }
        public string Info { get; set; }
        public string FeedBackType { get; set; }
        public string PartnerCode { get; set; }
        public string TransportType { get; set; }
        public int? CopyLayoutRefId { get; set; }
        public int? DefaultLanguageId { get; set; }
        public bool IsQuestionPath { get; set; }
        public bool IsFlex { get; set; }
        public bool IsSmart { get; set; }
        public bool? IsParent { get; set; } //KC
        public bool? IsDefault { get; set; } //KC
        public bool? IsChild { get; set; } //KC
        public int? ISActivityModification { get; set; }
        public int? Position { get; set; }
        public bool? Visible { get; set; }
        public string PTO { get; set; }
        public string InterruptedBy { get; set; }
        [NotMapped]
        public bool Expanded { get; set; }

        [ForeignKey("DefaultLanguageId")]
        public LanguageMaster LanguageMaster { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        public ICollection<EntryProperties> EntryProperties
        {
            get; set;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("Pallets")]
    public class Pallets
    {
        [Key]
        [Column("PalletsID", Order = 0)]
        public int? PalletsID { get; set; }
        [Column("ActivityRefID", Order = 1)]
        public int ActivityRefID { get; set; }
        [Column("LayoutRefID", Order = 2)]
        public int LayoutRefID { get; set; }
        [Column("PalletsRefId", Order = 15)]
        public int? PalletsRefId { get; set; }
        [Column("PlaceLevel", Order = 5)]
        public bool PlaceLevel { get; set; }
        [Column("JobLevel", Order = 6)]
        public bool JobLevel { get; set; }
        [Column("ProductLevel", Order = 7)]
        public bool ProductLevel { get; set; }
        [Column("PlanningFeedbackcode", Order = 8)]
        public string PlanningFeedbackcode { get; set; }
        [Column("CreatedBy", Order = 9)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 10)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 11)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 12)]
        public DateTime? UpdatedOn { get; set; }
        [Column("QuestionRefID", Order = 13)]
        public int QuestionRefID { get; set; }
        [Column("Title", Order = 14)]
        [Required(AllowEmptyStrings = true)]
        public string Title { get; set; }
        public bool ScanLevel { get; set; }
        public string PartnerCode { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CREATIS.ApplicationCore.Modals
{
    public class RoleMapping
    {
        [Key]
        [Column("RoleMapId")]
        public int? RoleMapId { get; set; }
        [Column("UserId")]
        public int? UserId { get; set; }
        [Column("RoleId")]
        public int? RoleId { get; set; }
        [Column("Status")]
        public int? Status { get; set; }
        [Column("CreatedBy")]
        public int? CreatedBy { get; set; }
        [Column("CreatedOn")]
        public DateTime? CreatedOn { get; set; }
    }
}

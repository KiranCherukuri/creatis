﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("PropertyMaster")]
    public class PropertyMaster
    {
        [Key]
        [Column("PropertyId", Order = 0)]
        public int PropertyId { get; set; }
        [Column("SubToolboxRefId", Order = 1)]
        public int SubToolboxRefId { get; set; }
        [Column("PropertyName", Order = 2)]
        public string PropertyName { get; set; }
        [Column("IsDefault", Order = 3)]
        public int IsDefault { get; set; }
        [Column("PropertyStatus", Order = 4)]
        public int PropertyStatus { get; set; }
        [Column("CreatedBy", Order = 5)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 6)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 7)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 8)]
        public DateTime? UpdatedOn { get; set; }
        [ForeignKey("SubToolboxRefId")]
        public ToolBoxSubItem ToolBoxSubItem { get; set; }
    }
}

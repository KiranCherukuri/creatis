﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("UserLanguage")]
    public class UserLanguage
    {
        [Key]
        [Column("Id", Order = 0)]
        public int Id { get; set; }
        [Column("UserRefId", Order = 1)]
        public int UserRefId { get; set; }
        [Column("Language", Order = 2)]
        public string Language { get; set; }
        [Column("IsDefault", Order = 3)]
        public bool IsDefault { get; set; }
        [Column("CreatedBy", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 5)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 6)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 7)]
        public DateTime UpdatedOn { get; set; }
        [ForeignKey("UserRefId")]
        public Users Users { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class OBCType
    {
        [Key]
        public int OBCTypeId { get; set; }
        [Column("CustomerRefId", Order = 1)]
        public int CustomerRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int OBCTypeRefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int IsModification { get; set; }
        [ForeignKey("CustomerRefId")]
        public Customers Customers { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("OBCTypeRefId")]
        public OBCTypeMaster OBCTypeMaster { get; set; }
    }
}

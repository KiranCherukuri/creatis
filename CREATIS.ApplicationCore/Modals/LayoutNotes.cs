﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("LayoutNotes")]
    public class LayoutNotes
    {
        [Key]
        public int Id { get; set; }
        public int LayoutRefId { get; set; }
        public string CreatedBy { get; set; }
        public string Notes { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedByName { get; set; }
        public int LayoutStatusRefId { get; set; }
        [NotMapped]
        public string LayoutStatusName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("AliasNameDetails")]
    public class AliasNameDetails
    {
        [Key]
        [Column("AliasNameDetailsId", Order = 0)]
        public int AliasNameDetailsId { get; set; }
        [Column("LayoutRefId", Order = 1)]
        public int LayoutRefId { get; set; }
        [Column("ActivitiesRefId", Order = 3)]
        public int ActivitiesRefId { get; set; }
        [Column("AliasName", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string AliasName { get; set; }
        [Column("PropertyName", Order = 5)]
        public string PropertyName { get; set; }
        [Column("PropertyTableId", Order = 6)]
        public int PropertyTableId { get; set; }
        [Column("CreatedBy", Order = 7)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 8)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 9)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 10)]
        public DateTime? UpdatedOn { get; set; }
        public bool? IsTempData { get; set; }
        public string Comment { get; set; }
    }
}

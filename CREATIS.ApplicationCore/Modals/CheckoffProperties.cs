﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    [Table("CheckoffProperties")]
    public class CheckoffProperties
    {
        [Key]
        [Column("CheckoffId", Order = 0)]
        public int? CheckoffId { get; set; }
        [Column("QuestionRefId", Order = 1)]
        public int QuestionRefId { get; set; }
        [Column("LayoutRefId", Order = 2)]
        public int LayoutRefId { get; set; }
        [Column("ActivitiesRefId", Order = 3)]
        public int ActivitiesRefId { get; set; }
        [Column("Planninglevel", Order = 4)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string Planninglevel { get; set; }
        [Column("Name", Order = 5)]
        [MaxLength(50)]
        public string Name { get; set; }
        [Column("CreatedBy", Order = 6)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [Column("CreatedOn", Order = 7)]
        public DateTime CreatedOn { get; set; }
        [Column("UpdatedBy", Order = 8)]
        [MaxLength(50)]
        public string UpdatedBy { get; set; }
        [Column("UpdatedOn", Order = 9)]
        public DateTime? UpdatedOn { get; set; }
        [ForeignKey("LayoutRefId")]
        public Layout Layout { get; set; }
        [ForeignKey("ActivitiesRefId")]
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        [ForeignKey("QuestionRefId")]
        public Questions Questions { get; set; }
        public string Comment { get; set; }
       
    }
}

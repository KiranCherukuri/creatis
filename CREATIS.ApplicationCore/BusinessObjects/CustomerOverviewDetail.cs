﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
     public class CustomerOverviewDetail
    {
        [Key]
        public int LayoutId { get; set; }
        public int CustomerRefId { get; set; }
        public string LayoutName { get; set; }
        public string LayoutType { get; set; }
        public string LayoutOrigin { get; set; }
        public string LayoutStatus { get; set; }
        public int IsLocked { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string CustomerName { get; set; }
        public int LayoutStatusRefId { get; set; }
    }
}

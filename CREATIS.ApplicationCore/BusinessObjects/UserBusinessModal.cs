﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class UserBusinessModal
    {
        public string id { get; set; }
        public string displayName { get; set; }
        public string company { get; set; }
        public string companyName { get; set; }
        public int customerId { get; set; }
        public string customerName { get; set; }
        public string timezone { get; set; }
        public string language { get; set; }
        public string locale { get; set; }
        public List<string> emails { get; set; }
        public string isIntegrator { get; set; }
        public string impersonatorId { get; set; }
        public List<string> permissions { get; set; }
        public List<string> locations { get; set; }
        public List<string> accessableCompanies { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class SavedValues
    {
        [Key]
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Item { get; set; }
        public string Location { get; set; }
    }
}

﻿using CREATIS.ApplicationCore.Modals;
using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class BOVariableListProperties
    {
        public int? VariableListId { get; set; }
        public int QuestionRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int ActivitiesRefId { get; set; }
        public string Title { get; set; }
        public int VarListID { get; set; }
        public string Other { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string Name { get; set; }
        public int? ConditionItemValue { get; set; }
        public int? ConditionSavedValue { get; set; }
        public int? CompareMethod { get; set; }
        public int? CompareTo { get; set; }
        public int? CompareToSavedValue { get; set; }
        public string CompareToFixedText { get; set; }
        public Layout Layout { get; set; }
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        public Questions Questions { get; set; }
        public string Comment { get; set; }
        public int? SaveDisplay { get; set; }
        public int? SaveComment { get; set; }
        public int? FilterComment { get; set; }
        public string FilterMethods { get; set; }
        public string SavedValues { get; set; }
        public string SavetoAlaiasVal { get; set; }
        public string Filter { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class InfoMessagePropertiesTranslation
    {
        [Key]
        public int? InfomessageId { get; set; }
        public int QuestionRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int ActivitiesRefId { get; set; }
        public string Name { get; set; }
        public string Messagetype { get; set; }
        public string Title { get; set; }
        public string Titlestyle { get; set; }
        public int InfoMessageIconTypeRefId { get; set; }
        public string Messagecontent { get; set; }
        public string Fontstyle { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? Conditionitem { get; set; }
        public int? ConCompareMethod { get; set; }
        public int? Convalue { get; set; }
        public int? ConItemSavedValued { get; set; }
        public int? ConvalueSavedValue { get; set; }
        public string ComFixedText { get; set; }
        public string LanguageName { get; set; }
        public int? CustomerRefId { get; set; }
        public string Arabic { get; set; }
        public string Bulgarian { get; set; }
        public string Croatian { get; set; }
        public string Czech { get; set; }
        public string Danish { get; set; }
        public string Dutch { get; set; }
        public string English { get; set; }
        public string Estonian { get; set; }
        public string Finnish { get; set; }
        public string French { get; set; }
        public string German { get; set; }
        public string Greek { get; set; }
        public string Hungarian { get; set; }
        public string Italian { get; set; }
        public string Latvian { get; set; }
        public string Lithuanian { get; set; }
        public string Macedonian { get; set; }
        public string Norwegian { get; set; }
        public string Polish { get; set; }
        public string Portuguese { get; set; }
        public string Romanian { get; set; }
        public string Russian { get; set; }
        public string Slovak { get; set; }
        public string Slovene { get; set; }
        public string Spanish { get; set; }
        public string Swedish { get; set; }
        public string Turkish { get; set; }
        public string Ukrainian { get; set; }
        public string Belarusian { get; set; }
        public int? LangRefColTableId { get; set; }
        public string Comment { get; set; }
    }
}

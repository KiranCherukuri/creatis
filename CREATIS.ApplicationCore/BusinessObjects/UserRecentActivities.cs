﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;


namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class UserRecentActivities
    {
        [Key]
        public int RecentActivityId { get; set; }
        public int LayoutRefId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public DateTime RecentActivityOn { get; set; }
        public string UpdatedBy { get; set; }
        public string Source { get; set; }
        public string Environment { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class AlertCopiedLayout
    {
        [Key]
        public int Id { get; set; }
        public int LayoutId { get; set; }
        public string Code { get; set; }
    }
}

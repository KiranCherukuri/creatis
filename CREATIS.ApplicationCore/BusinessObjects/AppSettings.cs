﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class EmailConfiguration
    {
        public string EmailServer { get; set; }
        public string EmailAddress { get; set; }
        public string DisplayName { get; set; }
        public int EmailPort { get; set; }
        public string ToEmailId { get; set; }
    }
    public class QuestionPathTemplate
    {
        public int CustomerId { get; set; }
    }
    public class UserAuthentication
    {
        public string apiKey { get; set; }
        public string secretKey { get; set; }
        public string myTransicsURI { get; set; }
        public string myTransicsBaseURI { get; set; }
        public int isLive { get; set; }
    }
    public class ConnectionStrings
    {
        public string DB_Context { get; set; }
    }
    public class ServiceSettings
    {
        public string TxDesignurl { get; set; }
        public string SOAPAction { get; set; }
    }
    public class OthersSettings
    {
        public string CustomerEnvironment { get; set; }
        public int CustomCustomer { get; set; }
        public string SessionLink { get; set; }
    }
}

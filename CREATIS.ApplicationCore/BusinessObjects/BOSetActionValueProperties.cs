﻿using CREATIS.ApplicationCore.Modals;
using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class BOSetActionValueProperties
    {
        public int? SetActionValueId { get; set; }
        public int QuestionRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int ActivitiesRefId { get; set; }
        public string Title { get; set; }
        public int? SelectQP { get; set; }
        public string CancelcurrentQP { get; set; }
        public string SelectView { get; set; }
        public string SelectAllowance { get; set; }
        public int? SelectInfoColumn { get; set; }
        public string SetReset { get; set; }
        public string Value { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? Conditionitem { get; set; }
        public int? ConCompareMethod { get; set; }
        public int? Convalue { get; set; }
        public int? ConItemSavedValued { get; set; }
        public int? ConvalueSavedValue { get; set; }
        public string ComFixedText { get; set; }
        public string Comment { get; set; }
        public int SetActionValueTypeId { get; set; }
        public string InfoCoumnFixedText { get; set; }
        public int? InfoColumnSavedValue { get; set; }
        public string SelectTargetSavedValue { get; set; }
        public string TargetAlaiasVal { get; set; }
        public string SetState { get; set; }
        public int? PlanningIDOBC { get; set; }
        public int? Longitude { get; set; }
        public int? Latitude { get; set; }
        public int? CopySavedValue { get; set; }
        public Layout Layout { get; set; }
        public Questions Questions { get; set; }
        public string NextQpName { get; set; }
        public string NextViewName { get; set; }
        public string AllowanceName { get; set; }
        public string InfoColumnName { get; set; }
        public string SavedValue { get; set; }
        public string CopyRange { get; set; }
        public int? StartChar { get; set; }
        public int? CharLength { get; set; }
    }
}

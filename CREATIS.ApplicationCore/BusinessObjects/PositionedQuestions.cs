﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class PositionedQuestions
    {
        public int QuestionId { get; set; }
        public int? ParentId { get; set; }
        public int Position { get; set; }
    }

    //public class RootObject
    //{

    //public List<PositionedQuestions> PositionedQuestions { get; set; }
    //}
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class PlanningAndPartnerCodeOverview
    {
        [Key]
        public int QuestionId { get; set; }
        public string ActivitiesRegistrationName { get; set; }
        public string Item { get; set; }
        public string Location { get; set; }
        public string PlanningLevel { get; set; }
        public string ExtraInfo { get; set; }
        public string Pallets { get; set; }
        public string Anomaly { get; set; }
        public string PartnerCode { get; set; }
    }
}

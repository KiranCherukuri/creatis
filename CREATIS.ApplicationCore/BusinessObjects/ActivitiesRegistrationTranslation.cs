﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class ActivitiesRegistrationTranslation
    {
        [Key]
        public int ActivitiesRegistrationId { get; set; }
        public string ActivitiesRegistrationName { get; set; }
        public bool ConfirmButton { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CustomerRefId { get; set; }
        public bool EmptyFullSolo { get; set; }
        public bool Flexactivity { get; set; }
        public int ISModification { get; set; }
        public string InterruptibleByRefId { get; set; }
        public int LayoutRefId { get; set; }
        public string PTORefId { get; set; }
        public string PlanningOverviewRefId { get; set; }
        public string PlanningRefId { get; set; }
        public string SpecificRefId { get; set; }
        public bool TransportType { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool Visible { get; set; }
        public string WorkingCodeRefId { get; set; }
        public int? OBCDefault { get; set; }
        public int ActRegPosition { get; set; }
        public int? DefaultLanguageId { get; set; }
        public int ScreenLayoutIsModification { get; set; }
        public int? DefaultActivitiesRefId { get; set; }
        public string LanguageName { get; set; }
        public string Arabic { get; set; }
        public string Bulgarian { get; set; }
        public string Croatian { get; set; }
        public string Czech { get; set; }
        public string Danish { get; set; }
        public string Dutch { get; set; }
        public string English { get; set; }
        public string Estonian { get; set; }
        public string Finnish { get; set; }
        public string French { get; set; }
        public string German { get; set; }
        public string Greek { get; set; }
        public string Hungarian { get; set; }
        public string Italian { get; set; }
        public string Latvian { get; set; }
        public string Lithuanian { get; set; }
        public string Macedonian { get; set; }
        public string Norwegian { get; set; }
        public string Polish { get; set; }
        public string Portuguese { get; set; }
        public string Romanian { get; set; }
        public string Russian { get; set; }
        public string Slovak { get; set; }
        public string Slovene { get; set; }
        public string Spanish { get; set; }
        public string Swedish { get; set; }
        public string Turkish { get; set; }
        public string Ukrainian { get; set; }
        public string Belarusian { get; set; }
        public int? LangRefColTableId { get; set; }
        public bool IsCopyLayout { get; set; }
        public int IsModificationActivitiesRegistrationName { get; set; }
        public int IsModificationWorkingCodeRefId { get; set; }
        public int IsModificationPlanningRefId { get; set; }
        public int IsModificationPlanningOverviewRefId { get; set; }
        public int IsModificationSpecificRefId { get; set; }
        public int IsModificationVisible { get; set; }
        public int IsModificationConfirmButton { get; set; }
        public int IsModificationTransportType { get; set; }
        public int IsModificationEmptyFullSolo { get; set; }
        public int IsModificationFlexactivity { get; set; }
        public int IsModificationPTORefId { get; set; }
        public int IsModificationInterruptibleByRefId { get; set; }

    }
}

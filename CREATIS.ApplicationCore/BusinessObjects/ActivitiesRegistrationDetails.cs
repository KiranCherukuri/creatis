﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class ActivitiesRegistrationDetails
    {
        public int? ActivitiesRegistrationId { get; set; }
        public int CustomerRefId { get; set; }
        public int LayoutRefId { get; set; }
        public string ActivitiesRegistrationName { get; set; }
        public string WorkingCodeRefId { get; set; }
        public string PlanningRefId { get; set; }
        public string PlanningOverviewRefId { get; set; }
        public string SpecificRefId { get; set; }
        public bool Visible { get; set; }
        public bool ConfirmButton { get; set; }
        public string PTORefId { get; set; }
        public string InterruptibleByRefId { get; set; }
        public bool TransportType { get; set; }
        public bool EmptyFullSolo { get; set; }
        public bool Flexactivity { get; set; }
        public int? ISModification { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public int DefaultActivitiesRefId { get; set; }

        public int ActRegPosition { get; set; }

        public string CopyLayoutColor { get; set; }

    }
}

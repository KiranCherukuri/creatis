﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class BoShowLayout
    {
        public int QuestionId { get; set; }
        public int LayoutRefId { get; set; }
        public int ActivitiesRefId { get; set; }
        public int TransporttypeRefId { get; set; }
        public int ParentId { get; set; }
        public int? CustomerRefId { get; set; }
        public string Name { get; set; }
        public string LanguageName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string PropertyName { get; set; }
        public string Nodetype { get; set; }
        public string ActivitiesRegistrationName { get; set; }
        public bool ConfirmButton { get; set; }
        public string WorkingCodeRefId { get; set; }
        public int QuestionPathCount { get; set; }
        public int? CopyLayoutRefId { get; set; }
        public string Info { get; set; }
        public string FeedBackType { get; set; }
        public string PartnerCode { get; set; }
        public string TransportType { get; set; }
        public int? ISModification { get; set; }
        public int? ISActivityModification { get; set; }
        public bool IsFlex { get; set; }
        public bool IsSmart { get; set; }
        public bool? Visible { get; set; }
        public string PTO { get; set; }
        public string InterruptedBy { get; set; }

        public string Arabic { get; set; }
        public string Bulgarian { get; set; }
        public string Croatian { get; set; }
        public string Czech { get; set; }
        public string Danish { get; set; }
        public string Dutch { get; set; }
        public string English { get; set; }
        public string Estonian { get; set; }
        public string Finnish { get; set; }
        public string French { get; set; }
        public string German { get; set; }
        public string Greek { get; set; }
        public string Hungarian { get; set; }
        public string Italian { get; set; }
        public string Latvian { get; set; }
        public string Lithuanian { get; set; }
        public string Macedonian { get; set; }
        public string Norwegian { get; set; }
        public string Polish { get; set; }
        public string Portuguese { get; set; }
        public string Romanian { get; set; }
        public string Russian { get; set; }
        public string Slovak { get; set; }
        public string Slovene { get; set; }
        public string Spanish { get; set; }
        public string Swedish { get; set; }
        public string Turkish { get; set; }
        public string Ukrainian { get; set; }
        public string Belarusian { get; set; }
        public int IsDefaultTranslation { get; set; }
        public int IsModificationArabic { get; set; }
        public int IsModificationBulgarian { get; set; }
        public int IsModificationCroatian { get; set; }
        public int IsModificationCzech { get; set; }
        public int IsModificationDanish { get; set; }
        public int IsModificationDutch { get; set; }
        public int IsModificationEnglish { get; set; }
        public int IsModificationEstonian { get; set; }
        public int IsModificationFinnish { get; set; }
        public int IsModificationFrench { get; set; }
        public int IsModificationGerman { get; set; }
        public int IsModificationGreek { get; set; }
        public int IsModificationHungarian { get; set; }
        public int IsModificationItalian { get; set; }
        public int IsModificationLatvian { get; set; }
        public int IsModificationLithuanian { get; set; }
        public int IsModificationMacedonian { get; set; }
        public int IsModificationNorwegian { get; set; }
        public int IsModificationPolish { get; set; }
        public int IsModificationPortuguese { get; set; }
        public int IsModificationRomanian { get; set; }
        public int IsModificationRussian { get; set; }
        public int IsModificationSlovak { get; set; }
        public int IsModificationSlovene { get; set; }
        public int IsModificationSpanish { get; set; }
        public int IsModificationSwedish { get; set; }
        public int IsModificationTurkish { get; set; }
        public int IsModificationUkrainian { get; set; }
        public int IsModificationBelarusian { get; set; }
        public string LayoutName { get; set; }
        public string PlanningOverviewRefId { get; set; }
        public string PlanningRefId { get; set; }
        public string SpecificRefId { get; set; }
        public bool EmptyFullSolo { get; set; }
        public bool IsTransportType { get; set; }
        public string Comment { get; set; }
        public bool Flexactivity { get; set; }
        public int IsModificationActivitiesRegistrationName { get; set; }
        public int IsModificationWorkingCodeRefId { get; set; }
        public int IsModificationPlanningRefId { get; set; }
        public int IsModificationPlanningOverviewRefId { get; set; }
        public int IsModificationSpecificRefId { get; set; }
        public int IsModificationVisible { get; set; }
        public int IsModificationConfirmButton { get; set; }
        public int IsModificationTransportType { get; set; }
        public int IsModificationEmptyFullSolo { get; set; }
        public int IsModificationFlexactivity { get; set; }
        public int IsModificationPTORefId { get; set; }
        public int IsModificationInterruptibleByRefId { get; set; }

    }

    public class ShowLayoutResponse
    {
        public string ActivitiesRegistrationName { get; set; }
        public bool ConfirmButton { get; set; }
        public string WorkingCodeRefId { get; set; }
        public int QuestionPathCount { get; set; }
        public string TransportType { get; set; }
        public int ActivitiesRegId { get; set; }
        public bool IsQuestionPath { get; set; }
        public List<BoShowLayout> LayQuesions { get; set; }
        public int? ISActivityModification { get; set; }
        public bool IsFlex { get; set; }
        public bool IsSmart { get; set; }
        public bool? Visible { get; set; }
        public string PTO { get; set; }
        public string InterruptedBy { get; set; }
        public string LayoutName { get; set; }
        public string PlanningOverviewRefId { get; set; }
        public string PlanningRefId { get; set; }
        public string SpecificRefId { get; set; }
        public bool EmptyFullSolo { get; set; }
        public bool IsTransportType { get; set; }
        public string Comment { get; set; }
        public bool Flexactivity { get; set; }
        public int IsModificationActivitiesRegistrationName { get; set; }
        public int IsModificationWorkingCodeRefId { get; set; }
        public int IsModificationPlanningRefId { get; set; }
        public int IsModificationPlanningOverviewRefId { get; set; }
        public int IsModificationSpecificRefId { get; set; }
        public int IsModificationVisible { get; set; }
        public int IsModificationConfirmButton { get; set; }
        public int IsModificationTransportType { get; set; }
        public int IsModificationEmptyFullSolo { get; set; }
        public int IsModificationFlexactivity { get; set; }
        public int IsModificationPTORefId { get; set; }
        public int IsModificationInterruptibleByRefId { get; set; }

    }
}

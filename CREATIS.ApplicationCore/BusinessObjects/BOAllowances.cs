﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class BOAllowances
    {
        [Key]
        public int AllowancesId { get; set; }
        public int CustomerRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int AllowancesRefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int IsQPUsed { get; set; }
        public string AllowancesMasterDescription { get; set; }
        public string AllowancesCode { get; set; }
        public bool IsActive { get; set; }
        public int IsModification { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class ToolBoxItems
    {
        [Key]
        public int ToolboxId { get; set; }
        public int SubToolboxId { get; set; }
        public string ToolboxName { get; set; }
        public int Position { get; set; }
        public int SubPosition { get; set; }
    }
}

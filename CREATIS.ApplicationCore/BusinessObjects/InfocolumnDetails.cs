﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class InfocolumnDetails
    {
        public int IC_ID { get; set; }
        public string ICM_Column_Name { get; set; }
        public int CustomerRefId { get; set; }
    }

    public class InfocolumnList
    {
        [Key]
        public int IC_ID { get; set; }
        public string ICM_Column_Name { get; set; }
        public int CustomerRefId { get; set; }
    }

}

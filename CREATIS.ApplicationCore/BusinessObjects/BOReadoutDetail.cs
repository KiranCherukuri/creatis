﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class BOReadoutDetail
    {
        public int PlanningSelectReadoutDetailId { get; set; }
        public int PlanningSelectRefId { get; set; }
        public int ReadoutDetailId { get; set; }
        public string TokenId { get; set; }
        public int ReadoutValueRefId { get; set; }
        public string ReadOutValueRefText { get; set; }
        public string ReadOutValue { get; set; }
        public int? ConfigID { get; set; }
        public int? CommentID { get; set; }
        public string ReadoutRange { get; set; }
        public int? StartChar { get; set; }
        public int? CharLength { get; set; }
        public string ReadoutExample { get; set; }
        public string Saveto { get; set; }
        public string DefaultAliasName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ReadoutValue { get; set; }
        public string SaveToValue { get; set; }
        public string Save { get; set; }
        public string ConfigIDValue { get; set; }
        public string CommentIDValue { get; set; }
        public string StartCharValue { get; set; }
        public string CharLengthValue { get; set; }
    }
}

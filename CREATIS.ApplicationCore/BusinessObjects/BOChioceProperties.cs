﻿using CREATIS.ApplicationCore.Modals;
using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class BOChioceProperties
    {
        public int? ChioceId { get; set; }
        public int QuestionRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int ActivitiesRefId { get; set; }
        public string Title { get; set; }
        public int Recursiveloop { get; set; }
        public int Save { get; set; }
        public String Propose { get; set; }
        public string Planningfeedback { get; set; }
        public string FeedbackType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? ConditionValue { get; set; }
        public int? ConCompareMethod { get; set; }
        public int? ConCompareTo { get; set; }
        public string Infocolumn { get; set; }
        public int? InfoColumnAction { get; set; }
        public string FileNumber { get; set; }
        public int? ConSavedValued { get; set; }
        public int? comtoSavedValue { get; set; }
        public string ComFixedText { get; set; }
        public int? AutoGenerate { get; set; }
        public int? SaveValue { get; set; }
        public int? CopyLayoutRefId { get; set; }
        public string Comment { get; set; }
        public string Planningfeedbackcode { get; set; }
        public int? InfoActionQP { get; set; }
        public Layout Layout { get; set; }
        public ActivitiesRegistration ActivitiesRegistration { get; set; }
        public Questions Questions { get; set; }
        public string Saveto { get; set; }
        public string SavetoAlaiasVal { get; set; }
        public string PartnerCode { get; set; }
        public bool? IsDefaultAlaisName { get; set; }
    }
}

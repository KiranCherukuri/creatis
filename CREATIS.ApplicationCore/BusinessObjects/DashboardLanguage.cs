﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class DashboardLanguage
    {
        public int UsedLanguagesId { get; set; }
        public int CustomerRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int LanguageRefId { get; set; }
        public string Language { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int IsDefault { get; set; }
        public string CopyColorCode { get; set; }
        public int IsModification { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class Toolbox
    {
        public int ToolboxId { get; set; }
        public string ToolboxName { get; set; }
        public string ToolboxDescription { get; set; }
        public int SubToolboxId { get; set; }
        public int ToolboxRefId { get; set; }
        public string SubToolboxName { get; set; }
        public string SubToolboxDescription { get; set; }

    }
}

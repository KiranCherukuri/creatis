﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CREATIS.ApplicationCore.BusinessObjects
{
    public class boEntryProperties
    {
        public int? EntryId { get; set; }
        public int QuestionRefId { get; set; }
        public int LayoutRefId { get; set; }
        public int ActivitiesRefId { get; set; }
        public string Title { get; set; }
        public string MaskText { get; set; }
        public string Size { get; set; }
        public int NotNull { get; set; }
        public int MinimumofCharacter { get; set; }
        public int ExactCharacter { get; set; }
        public int Zeronotallowed { get; set; }
        public string Regex { get; set; }
        public string Predefined { get; set; }
        public int Save { get; set; }
        public int? Propose { get; set; }
        public string ProposeFixedValue { get; set; }
        public string Planningfeedback { get; set; }
        public string FeedbackType { get; set; }
        public string PlanningfeedbackCode { get; set; }
        public string EntryType { get; set; }
        public string Comment { get; set; }
        public string DatetimeFormat { get; set; }
        public int? Conditionitem { get; set; }
        public int? ConCompareMethod { get; set; }
        public int? Convalue { get; set; }
        public string Infocolumn { get; set; }
        public int? InfoColumnAction { get; set; }
        public string FileNumber { get; set; }
        public int? ConItemSavedValued { get; set; }
        public int? ConvalueSavedValue { get; set; }
        public string PartnerCode { get; set; }
        public int? SaveValue { get; set; }
        public int? AutoGenerate { get; set; }
        public string ComFixedText { get; set; }
        public string Saveto { get; set; }
        public string SavetoAlaiasVal { get; set; }
        public int? MinimumOfCharterValue { get; set; }
        public bool? IsDefaultAlaisName { get; set; }
        public string PropertyName { get; set; }

    }
}

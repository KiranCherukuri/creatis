﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CREATIS.ApplicationCore.Modals
{
    public class Translations
    {
        [Key]
        public int LangRefDetailId { get; set; }
        public int? LangDetailId { get; set; }
        public int? LangRefColTableId { get; set; }
        public int? LangRefTableId { get; set; }
        public int? LayoutRefId { get; set; }
        public int? CustomerRefId { get; set; }
        public int? RefId { get; set; }
        public int? QuestionRefId { get; set; }
        public string PropertiesPath { get; set; }
        public string Arabic { get; set; }
        public string Bulgarian { get; set; }
        public string Croatian { get; set; }
        public string Czech { get; set; }
        public string Danish { get; set; }
        public string Dutch { get; set; }
        public string English { get; set; }
        public string Estonian { get; set; }
        public string Finnish { get; set; }
        public string French { get; set; }
        public string German { get; set; }
        public string Greek { get; set; }
        public string Hungarian { get; set; }
        public string Italian { get; set; }
        public string Latvian { get; set; }
        public string Lithuanian { get; set; }
        public string Macedonian { get; set; }
        public string Norwegian { get; set; }
        public string Polish { get; set; }
        public string Portuguese { get; set; }
        public string Romanian { get; set; }
        public string Russian { get; set; }
        public string Slovak { get; set; }
        public string Slovene { get; set; }
        public string Spanish { get; set; }
        public string Swedish { get; set; }
        public string Turkish { get; set; }
        public string Ukrainian { get; set; }
        public string Belarusian { get; set; }
        public int IsDefaultTranslation { get; set; }
        public int IsModificationArabic { get; set; }
        public int IsModificationBulgarian { get; set; }
        public int IsModificationCroatian { get; set; }
        public int IsModificationCzech { get; set; }
        public int IsModificationDanish { get; set; }
        public int IsModificationDutch { get; set; }
        public int IsModificationEnglish { get; set; }
        public int IsModificationEstonian { get; set; }
        public int IsModificationFinnish { get; set; }
        public int IsModificationFrench { get; set; }
        public int IsModificationGerman { get; set; }
        public int IsModificationGreek { get; set; }
        public int IsModificationHungarian { get; set; }
        public int IsModificationItalian { get; set; }
        public int IsModificationLatvian { get; set; }
        public int IsModificationLithuanian { get; set; }
        public int IsModificationMacedonian { get; set; }
        public int IsModificationNorwegian { get; set; }
        public int IsModificationPolish { get; set; }
        public int IsModificationPortuguese { get; set; }
        public int IsModificationRomanian { get; set; }
        public int IsModificationRussian { get; set; }
        public int IsModificationSlovak { get; set; }
        public int IsModificationSlovene { get; set; }
        public int IsModificationSpanish { get; set; }
        public int IsModificationSwedish { get; set; }
        public int IsModificationTurkish { get; set; }
        public int IsModificationUkrainian { get; set; }
        public int IsModificationBelarusian { get; set; }
        public string ColumnName { get; set; }
        public string PropertiesName { get; set; }
        public string TableName { get; set; }
    }
    
}

﻿CREATIS.factory('dataFactory', function ($http) {
    var dataFactory = {};

    dataFactory.DeleteLayout = function (Layoutid)
    {
        var url = "DeleteLayout?layoutId=" +Layoutid
        return $http.post(url)
    };

    dataFactory.GetRecentActivitiestoDashboard = function (Layoutid)
    {
        var url = "Dashboard?LayoutID=";
        $http.post(url).then(function ()
        {
            return window.location.href = url + Layoutid;
        });
    };

    dataFactory.GetCustomer = function () {
        var url = "HomeCustomer"
        return $http.post(url);
    };

    dataFactory.GetRecentActivities = function () {
        var url = "HomeRecentActivities"
        return $http.post(url);
    };

    dataFactory.RedirectToCustomerOverview = function (DropCust) {
        var url = "CustomerOverview?customerid=" + DropCust
        return window.location.href = url;
    };


    dataFactory.RedirecttoScreenLayout  = function (Layoutid)
    {
        var url = "ScreenLayout?LayoutID=";
        $http.post(url).then(function ()
        {
            return window.location.href = url + Layoutid;
        });
    };

    dataFactory.RedirecttoShowLayout = function (Layoutid)
    {
        var url = "ShowLayout?LayoutID=";
        $http.post(url).then(function ()
        {
            return window.location.href = url + Layoutid;
        });
    };

    dataFactory.RedirecttoTranslation = function (Layoutid)
    {
        var url = "Translation?LayoutID=";
        $http.post(url).then(function ()
        {
            return window.location.href = url + Layoutid;
        });
    };

    dataFactory.GetCustomerName = function (customerid)
    {
        var url = "GetCustomerName?customerid=" + customerid
        return $http.post(url);
    };

    dataFactory.GetLayout = function (DropCust) {
        var url = "CustomerOverviewAvailableLayout?customerid=" + DropCust
       
        return $http.post(url);
    };

    dataFactory.GoHomePage = function () {
        var url = "Index"
        return window.location.href = url;
    };

    dataFactory.GetInfocolumn = function (DropCust) {
        var url = "InfoColumnDetails?customerid=" + DropCust
        return $http.post(url);
    };

    dataFactory.SaveInfocolumn = function (iCID, icMColumnName, customerRefId) {
        var url = "SaveInfoColumnDetails?ID=" + iCID + "&ColumnName=" + icMColumnName + "&customerRefId=" + customerRefId
        return $http.post(url);
    };

    dataFactory.UpdateInfocolumn = function (iCID, icMColumnName, customerRefId) {
        var url = "UpdateInfoColumnDetails?ID=" + iCID + "&ColumnName=" + icMColumnName + "&customerRefId=" + customerRefId
        return $http.post(url);
    };

    dataFactory.DeleteInfocolumn = function (iCID) {
        var url = "DeleteInfoColumnDetails?ID=" + iCID 
        return $http.post(url);
    };

dataFactory.GetIntegrator = function (customerid) {
        var url = "GetIntegrator?customerid=" + customerid
        return $http.post(url);
    };

    dataFactory.SaveIntegrator = function (I_Details, customerRefId) {
        var url = "SaveIntegrator?I_Details=" + I_Details + "&customerRefId=" + customerRefId
        return $http.post(url);
    };

    dataFactory.UpdateIntegrator = function (I_ID, I_Details) {
        var url = "UpdateIntegrator?I_ID=" + I_ID + "&I_Details=" + I_Details
        return $http.post(url);
    };

    dataFactory.DeleteIntegrator = function (I_ID) {
        var url = "DeleteIntegrator?I_ID=" + I_ID 
        return $http.post(url);
    };

dataFactory.GetCustomerNotes = function (customerid) {
        var url = "GetCustomerNotes?customerid=" + customerid
        return $http.post(url);
    };

    dataFactory.SaveCustomerNotes = function (CN_Details, customerRefId) {
        var url = "SaveCustomerNotes?CN_Details=" + CN_Details + "&customerRefId=" + customerRefId
        return $http.post(url);
    };

    dataFactory.UpdateCustomerNotes = function (CN_ID, CN_Details) {
        var url = "UpdateCustomerNotes?CN_ID=" + CN_ID + "&CN_Details=" + CN_Details
        return $http.post(url);
    };

    dataFactory.DeleteCustomerNotes = function (CN_ID) {
        var url = "DeleteCustomerNotes?CN_ID=" + CN_ID 
        return $http.post(url);
    };


    dataFactory.GetLanguages = function () {
        var url = "GetLanguages"
        return $http.post(url);
    };

    dataFactory.SaveLayoutDetails = function (LanguageID, LayoutName, customerRefId, defaultlan) {
        var url = "SaveLayoutDetails?LanguageId=" + LanguageID + "&LayoutName=" + LayoutName + "&customerRefId=" + customerRefId + "&defaultlan=" + defaultlan
        return $http.post(url);
    };

    dataFactory.RedirectToDashboard = function (LayoutID) {
        var url = "Dashboard?LayoutID=" + LayoutID
        return window.location.href = url;
    };

    dataFactory.PanelTitle = function (LayoutID) {
        var url = "GetLayoutName?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetOBCTypeMaster = function () {
        var url = "GetOBCTypeMaster"
        return $http.post(url);
    };

    dataFactory.GetAssignedOBCTypes = function (LayoutID) {
        var url = "GetOBCType?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.SaveOBCType = function (LayoutID, OBCTypeId)
    {
        var url = "SaveOBCType?LayoutID=" + LayoutID + "&OBCTypeId=" + OBCTypeId
        return $http.post(url);
    };

    dataFactory.UpdateOBCTypes = function (OBCTypeId, OBCTypeRefId, LayoutID)
    {
        var url = "UpdateOBCTypes?OBCTypeId=" + OBCTypeId + "&OBCTypeRefId=" + OBCTypeRefId + "&LayoutID=" + LayoutID 
        return $http.post(url);
    };

    dataFactory.GetAssignedTransportTypes = function (LayoutID) {
        var url = "GetTransportType?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.SaveTransportTypes = function (LayoutID, TransportDescription) {
        var url = "SaveTransportTypes?LayoutID=" + LayoutID + "&TransportDescription=" + TransportDescription
        return $http.post(url);
    };

    dataFactory.GetUsedLanguage = function (LayoutID) {
        var url = "GetUsedLanguage?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.DashboardSaveUsedLanguage = function (LayoutID, LanguageID) {
        var url = "DashboardSaveUsedLanguage?LayoutID=" + LayoutID + "&LanguageID=" + LanguageID
        return $http.post(url);
    };

    dataFactory.GetGetPlanningTypeMaster = function () {
        var url = "GetGetPlanningTypeMaster"
        return $http.post(url);
    };

    dataFactory.GetPlanningType = function (LayoutID) {
        var url = "GetPlanningType?LayoutID="+LayoutID
        return $http.post(url);
    };

    dataFactory.SavePlanningType = function (LayoutID,PlanningTypeRefId) {
        var url = "SavePlanningType?LayoutID="+LayoutID+"&PlanningTypeRefId="+PlanningTypeRefId
        return $http.post(url);
    };
    
    dataFactory.DeletPlanningType = function (LayoutID,PlanningTypeRefId) {
        var url = "DeletPlanningType?LayoutID="+LayoutID+"&PlanningTypeRefId="+PlanningTypeRefId
        return $http.post(url);
    };
    dataFactory.GetAllowancesMaster = function (OBCTypeRefId) {
        var url = "GetAllowancesMaster?OBCTypeRefId=" + OBCTypeRefId
        return $http.post(url);
    };

    dataFactory.GetPlanningSpecificationMaster = function (PlanningTypeMasterRefId) {
        var url = "GetPlanningSpecificationMaster?PlanningTypeMasterRefId=" + PlanningTypeMasterRefId
        return $http.post(url);
    };

dataFactory.GetPlanningSpecification = function (LayoutID) {
        var url = "GetPlanningSpecification?LayoutID="+LayoutID
        return $http.post(url);
    };

dataFactory.SavePlanningSpecification = function (LayoutID,PlanningSpecificationRefId) {
        var url = "SavePlanningSpecification?LayoutID="+LayoutID+"&PlanningSpecificationRefId="+PlanningSpecificationRefId
        return $http.post(url);
    };
    
    dataFactory.DeletPlanningSpecification = function (LayoutID,PlanningSpecificationRefId) {
        var url = "DeletPlanningSpecification?LayoutID="+LayoutID+"&PlanningSpecificationRefId="+PlanningSpecificationRefId
        return $http.post(url);
    };

dataFactory.GetAllowances = function (LayoutID) {
        var url = "GetAllowances?LayoutID="+LayoutID
        return $http.post(url);
    };

dataFactory.SaveAllowances = function (LayoutID,AllowancesRefId) {
        var url = "SaveAllowances?LayoutID="+LayoutID+"&AllowancesRefId="+AllowancesRefId
        return $http.post(url);
    };
    
    dataFactory.DeletAllowances = function (LayoutID,AllowancesRefId) {
        var url = "DeletAllowances?LayoutID="+LayoutID+"&AllowancesRefId="+AllowancesRefId
        return $http.post(url);
    };


    dataFactory.SaveAllowancesMaster = function (OBCTypeRefId, AllowancesMasterDescription, AllowanceCode) {
        var url = "SaveAllowancesMaster?OBCTypeRefId=" + OBCTypeRefId + "&AllowancesMasterDescription=" + AllowancesMasterDescription + "&AllowanceCode=" + AllowanceCode
        return $http.post(url);
    };
    
dataFactory.GetWorkingCode = function () {
        var url = "GetWorkingCode"
        return $http.post(url);
    };

dataFactory.GetPlanning = function () {
        var url = "GetPlanning"
        return $http.post(url);
    };

dataFactory.GetPlanningOverview = function () {
        var url = "GetPlanningOverview"
        return $http.post(url);
    };

dataFactory.GetSpecific = function () {
        var url = "GetSpecific"
        return $http.post(url);
    };

dataFactory.GetPTO = function () {
        var url = "GetPTO"
        return $http.post(url);
    };

dataFactory.GetInterruptibleBy = function () {
        var url = "GetInterruptibleBy"
        return $http.post(url);
    };

dataFactory.GetActivitiesRegistration = function (LayoutID) {
        var url = "GetActivitiesRegistration?LayoutID=" + LayoutID
        return $http.post(url);
    };

dataFactory.SaveActivitiesRegistration = function (LayoutID,ACTReg){
//ActivitiesRegistrationName,WorkingCodeRefId,PlanningRefId,PlanningOverviewRefId,SpecificRefId,Visible,ConfirmButton,PTORefId,InterruptibleByRefId,TransportType,EmptyFullSolo,Flexactivity,ISModification) {
        //var url = "SaveActivitiesRegistration?LayoutID=" + LayoutID+"&ActivitiesRegistrationName="+ActivitiesRegistrationName+"&WorkingCodeRefId="+WorkingCodeRefId+"&PlanningRefId="+PlanningRefId+"&PlanningOverviewRefId="+"&SpecificRefId="+SpecificRefId+"&Visible="+Visible+"&ConfirmButton="+ConfirmButton+"&PTORefId="+PTORefId+"&InterruptibleByRefId="+InterruptibleByRefId+"&TransportType="+TransportType+"&EmptyFullSolo="+EmptyFullSolo+"&Flexactivity="+Flexactivity+"&ISModification="+ISModification
var url = "SaveActivitiesRegistration?LayoutID=" +LayoutID+"&ACTReg="+ACTReg    
 return $http.post(url);
};

dataFactory.RedirectToQuestionPath = function (LayoutID,ActivitiyID)
{
    //alert(LayoutID);
    var url = "QuestionPath?LayoutID=" + LayoutID + "&ActivityID=" + ActivitiyID
    return window.location.href = url;
};

dataFactory.GetRegistration = function (LayoutID)
{
    var url = "GetRegistration?LayoutID=" + LayoutID
    return $http.post(url);
};

dataFactory.GetActivities = function (LayoutID)
{
    var url = "GetActivities?LayoutID=" + LayoutID
    return $http.post(url);
};

    dataFactory.GetRegistrationPosition = function (LayoutID)
{
        var url = "GetRegistrationPosition?LayoutID=" + LayoutID
    return $http.post(url);
};

    dataFactory.GetActivitiesPosition = function (LayoutID)
{
        var url = "GetActivitiesPosition?LayoutID=" + LayoutID
    return $http.post(url);
};


dataFactory.GetQuestionPathToolBox = function ()
{
    var url = "GetToolBox"
    return $http.post(url)
};

    dataFactory.GetToolBoxParentNodes = function ()
{
        var url = "GetToolBoxParentNodes"
    return $http.post(url)
};

    dataFactory.GetToolBoxChildNodes = function ()
{
        var url = "GetToolBoxChildNodes"
    return $http.post(url)
    };
    dataFactory.GetQuestions = function (LayoutID, activityId, transportId)
    {
        var url = "GetQuestions?layoutId=" + LayoutID + "&activityId=" + activityId + "&transportId=" + transportId
        return $http.post(url);
    };

    dataFactory.SaveQuestions = function (LayoutID, transportTypeId, Question, propertyName)
    {
        var url = "SaveQuestions?LayoutID=" + LayoutID + "&transportTypeId=" + transportTypeId + "&Question=" + Question + "&propertyName=" + propertyName
        return $http.post(url);
    };

    dataFactory.SaveQuestionsDefault = function (LayoutID, transportTypeId, Question,activityId)
    {
        var url = "SaveQuestionsDefault?LayoutID=" + LayoutID + "&transportTypeId=" + transportTypeId + "&Question=" + Question + "&activityId=" + activityId
        return $http.post(url);
    };

    dataFactory.GetEntryProperties = function (questionRefId)
    {
        var url = "GetEntryProperties?questionRefId=" + questionRefId 
        return $http.post(url);
    };

    dataFactory.GetParticularActivitiesRegistration = function (activitiesId)
    {
        var url = "GetParticularActivitiesRegistration?activitiesId=" + activitiesId
        return $http.post(url);
    };

    dataFactory.GetDocumetScanProperties = function (questionRefId)
    {
        var url = "GetDocumetScanProperties?questionRefId=" + questionRefId
        return $http.post(url);
    };
    dataFactory.GetVariableListProperties = function (questionRefId)
    {
        var url = "GetVariableListProperties?questionRefId=" + questionRefId
        return $http.post(url);
    };
    dataFactory.GetTextMessageProperties = function (questionRefId)
    {
        var url = "GetTextMessageProperties?questionRefId=" + questionRefId
        return $http.post(url);
    };
    dataFactory.GetInfomessageProperties = function (questionRefId)
    {
        var url = "GetInfomessageProperties?questionRefId=" + questionRefId
        return $http.post(url);
    };

    dataFactory.GetCheckoffProperties = function (questionRefId)
    {
        var url = "GetCheckoffProperties?questionRefId=" + questionRefId
        return $http.post(url);
    };
    dataFactory.GetPlanningselectProperties = function (questionRefId)
    {
        var url = "GetPlanningselectProperties?questionRefId=" + questionRefId
        return $http.post(url);
    };

    dataFactory.GetSetActionValueProperties = function (questionRefId)
    {
        var url = "GetSetActionValueProperties?questionRefId=" + questionRefId
        return $http.post(url);
    };

    dataFactory.GetChioceProperties = function (questionRefId)
    {
        var url = "GetChioceProperties?questionRefId=" + questionRefId
        return $http.post(url);
    };

    dataFactory.SaveAddChioceProperties = function (AddChioce)
    {
        var url = "SaveAddChioceProperties?AddChioce=" + AddChioce
        return $http.post(url);
    };

    dataFactory.UpdateDocumetScanProperties = function (DocumetScanProperties)
    {
        var url = "UpdateDocumetScanProperties?DocumetScanProperties=" + DocumetScanProperties
        return $http.post(url);
    };

    dataFactory.UpdateVariableListProperties = function (VariableListProperties, pVariableListnodeid)
    {
        var url = "UpdateVariableListProperties?VariableListProperties=" + VariableListProperties + "&pVariableListnodeid=" + pVariableListnodeid
        return $http.post(url);
    };

    dataFactory.UpdateTextMessageProperties = function (TextMessageProperties)
    {
        var url = "UpdateTextMessageProperties?TextMessageProperties=" + TextMessageProperties
        return $http.post(url);
    };

    dataFactory.UpdateInfomessageProperties = function (InfomessageProperties, pInfomessagenodeid)
    {
        var url = "UpdateInfomessageProperties?InfomessageProperties=" + InfomessageProperties + "&pInfomessagenodeid=" + pInfomessagenodeid
        return $http.post(url);
    };

    dataFactory.UpdateCheckoffProperties = function (CheckoffProperties, pCheckoffnodeid)
    {
        var url = "UpdateCheckoffProperties?CheckoffProperties=" + CheckoffProperties + "&pCheckoffnodeid=" + pCheckoffnodeid
        return $http.post(url);
    };

    dataFactory.UpdatePlanningselectProperties = function (PlanningselectProperties)
    {
        var url = "UpdatePlanningselectProperties?PlanningselectProperties=" + PlanningselectProperties
        return $http.post(url);
    };

    dataFactory.UpdateSetActionValueProperties = function (SetActionValueProperties)
    {
        var url = "UpdateSetActionValueProperties?SetActionValueProperties=" + SetActionValueProperties
        return $http.post(url);
    };

    dataFactory.UpdateChioceProperties = function (ChioceProperties)
    {
        var url = "UpdateChioceProperties?ChioceProperties=" + ChioceProperties
        return $http.post(url);
    };

    dataFactory.UpdateAddChioceProperties = function (AddChioceProperties)
    {
        var url = "UpdateAddChioceProperties?AddChioceProperties=" + AddChioceProperties
        return $http.post(url);
    };



    dataFactory.GetLayoutfordrop = function ()
    {
        var url = "GetLayout"
        return $http.post(url);
    };

    dataFactory.UpdateActivitiesRegistrationPosition = function (layoutId, actRegId)
    {
        //alert('entering');
        var url = "UpdateActivitiesRegistrationPosition?layoutId=" + layoutId + "&actRegId=" + actRegId
        return $http.post(url);
    };

    dataFactory.UpdateDeActivitiesRegistrationPosition = function (layoutId, actRegId)
    {
        //alert('entering');
        var url = "UpdateDeActivitiesRegistrationPosition?layoutId=" + layoutId + "&actRegId=" + actRegId
        return $http.post(url);
    };

    dataFactory.GetTranslation = function (LayoutID)
    {

        return $http.post('GetAllActRegByLayoutId?LayoutID=' + LayoutID);
        return $http.post(url);
    };

    dataFactory.GetTranslatorInfoColumn = function (LayoutID)
    {

        return $http.post('GetAllInfoByLayoutId?LayoutID=' + LayoutID);
        return $http.post(url);

    };

    dataFactory.GetRegistrationTranslation = function (LayoutID)
    {

        return $http.post('GetAllRegByLayoutId?LayoutID=' + LayoutID);
        return $http.post(url);
    };

    dataFactory.GetTranslatorAllowanceColumn = function (LayoutID)
    {

        return $http.post('GetAllAllowanceByLayoutId?LayoutID=' + LayoutID);
        return $http.post(url);
    }; 

    dataFactory.addtranslator = function (ActivityId, header)
    {

        return $http.post('AddActRegByActivityId?ActivityId=' + ActivityId + '&header=' + header);
        return $http.post(url);

    }

    dataFactory.addInfoPopup = function (InfoId, header)
    {

        return $http.post('AddInfoyActInfoIdId?InfoId=' + InfoId + '&header=' + header);
        return $http.post(url);

    }
    dataFactory.addAllowancePopup = function (AllowanceId, header)
    {

        return $http.post('AddAllowanceByAllowanceId?AllowanceId=' + AllowanceId + '&header=' + header);
        return $http.post(url);

    }

    dataFactory.addlanguage = function (LayoutID)
    {

        return $http.post('AddLanguage?LayoutID=' + LayoutID);
        return $http.post(url);

    }

    dataFactory.saveTranslator = function (paramdata)
    {

        return $http.post('SaveTranslator?paramdata=' + paramdata);
        return $http.post(url);

    }

    dataFactory.saveLanguageInfo = function (paramdata)
    {

        return $http.post('SaveLanguageInfo?paramdata=' + paramdata);
        return $http.post(url);

    }
    dataFactory.saveLanguageAllowance = function (paramdata)
    {

        return $http.post('SaveLanguageAllowance?paramdata=' + paramdata);
        return $http.post(url);

    }

    dataFactory.saveUsedLanguages = function (paramdata)
    {

        return $http.post('SaveUsedLanguage?paramdata=' + paramdata);
        return $http.post(url);

    }

    dataFactory.GetQuestionOBCTypes = function (LayoutID)
    {
        var url = "OBCTypes?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetQuestionAllowancesDescription = function (LayoutID)
    {
        var url = "GetAllowancesDescription?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetQuestionInfoColumns = function (LayoutID)
    {
        var url = "GetInfoColumns?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.SaveCopyLayout = function (param1, param2, param3, param4)
    {
        return $http.post("SaveCopyLayout?LayoutId=" + param1 + "&NewLayoutName=" + param2 + "&CustomerId=" + param3 + "&LanguageIds=" + param4);
    }

    dataFactory.ISPlanningType = function (LayoutID)
    {
        var url = "ISPlanningType?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.UpdateDescription = function (Description)
    {
        var url = "UpdateDescription?Description=" + Description
        return $http.post(url);
    };

    return dataFactory;
});
﻿var transportids = '';
var ActivitiesName = '';
var ActivitiesPlanning = '';
var OBCTypeDescription = '';
var availablePlanningType = '';
CREATIS.controller('QuestionpathController', ['$scope', '$http', 'dataFactory', '$uibModal', '$location', function ($scope, $http, dataFactory, $uibModal, $location)
{
    var absUrl = $location.absUrl();
    urldata = absUrl.toString();
    $scope.LayoutID = urldata.split('=')[1].split('&')[0];
    $scope.ActivityID = urldata.split('=')[2];

    dataFactory.GetQuestionPathToolBox().then(function (resObj)
    {
        $scope.toolBox = resObj.data;
        toolBox(resObj.data, $scope);
    }, function ()
        {
            $scope.error = "Some Error.";
        });

    dataFactory.GetActivitiesRegistration($scope.LayoutID).then(function (resObj)
    {
        $scope.Activitiesregistration = resObj.data;
        //alert(JSON.stringify($scope.Activitiesregistration));
    }, function ()
    {
        $scope.error = "Some Error.";
        });

    dataFactory.ISPlanningType($scope.LayoutID).then(function (resObj)
    {
        availablePlanningType = resObj.data;
    }, function ()
        {
            $scope.error = "Some Error.";
        });

    dataFactory.GetQuestionAllowancesDescription($scope.LayoutID).then(function (resObj)
    {
        $scope.AllowancesDescription = resObj.data;
        //alert(JSON.stringify($scope.AllowancesDescription));
    }, function ()
        {
            $scope.error = "Some Error.";
        });

    dataFactory.GetQuestionInfoColumns($scope.LayoutID).then(function (resObj)
    {
        $scope.InfoColumns = resObj.data;
        //alert(JSON.stringify($scope.InfoColumns));
    }, function ()
        {
            $scope.error = "Some Error.";
        });

    //$('#sltentry').on('change', function ()
    //{
    //    var entrytype = $("#sltentry option:selected").val();
    //    if (entrytype == 'Alphanumeric')
    //    {

    //        $('#chk0not').attr("disabled", true);

    //        //$('#lblpredefined').hide();
    //        //$('#sltpredefined').hide();
    //        //$('#lblregex').hide();

    //        //$('#txtRegex').show();
    //        //$('#lblnull').show();
    //        //$('#chkNotnull').show();
    //        //$('#exactchar').show();
    //        //$('#minchar').show();

    //    }
    //    else if (entrytype == 'Numeric')
    //    {
    //        $('#txtRegex').show();
    //    }
    //    else if (entrytype == 'Custom')
    //    { }
    //})

    $scope.Dashboard = function ()
    {
        dataFactory.GetRecentActivitiestoDashboard($scope.LayoutID);
    }

    dataFactory.GetAssignedTransportTypes($scope.LayoutID).then(function (resObj)
    {
        $scope.Transporttypecount = resObj.data.length;
        $scope.transporttypedata = resObj.data;
        if ($scope.Transporttypecount > 0)
        {
            angular.forEach(resObj.data, function (value, key)
            {
                dataFactory.GetQuestions($scope.LayoutID, $scope.ActivityID, value.transportTypeId).then(function (resObj)
                {
                    if (resObj.data.length == 0)
                    {
                        dataFactory.SaveQuestionsDefault($scope.LayoutID, value.transportTypeId, 'Description', $scope.ActivityID).then(function (resObj) { }, function ()
                        {
                            $scope.error = "Some Error.";
                        });
                    }

                }, function ()
                    {
                        $scope.error = "Some Error.";
                    });
            });
        } else
        {
            dataFactory.GetQuestions($scope.LayoutID, $scope.ActivityID, 0).then(function (resObj)
            {
                if (resObj.data.length == 0)
                {
                    dataFactory.SaveQuestionsDefault($scope.LayoutID, 0, 'Description', $scope.ActivityID).then(function (resObj)
                    { }, function ()
                        {
                            $scope.error = "Some Error.";
                        });
                }

            }, function ()
                {
                    $scope.error = "Some Error.";
                });
        }
        $scope.tabs = resObj.data;
        setTimeout(function ()
        {
            transportids = $("#transportid").val();
            questionPath($scope, dataFactory);
        }, 1500);
    }, function ()
        {
            $scope.error = "Some Error.";
        });

    $scope.model = {
        name: 'Tabs'
    };

    $scope.Questionstab = function (param)
    {
        $scope.tabtransportid = param;
        //alert(param);
        setTimeout(function ()
        {
            //alert('entering');
            questionPath($scope, dataFactory);
        }, 500);
        if ($scope.tabtransportid != '' && $scope.tabtransportid != undefined)
        {
            transportids = $scope.tabtransportid;
            // alert(transportids);
        } else
        {
            transportids = $("#transportid").val();
        }
    }

    dataFactory.GetParticularActivitiesRegistration($scope.ActivityID).then(function (resObj)
    {
        ActivitiesName = resObj.data[0].activitiesRegistrationName;
        ActivitiesPlanning = resObj.data[0].planningRefId;
    }, function ()
        {
            $scope.error = "Some Error.";
        });

    dataFactory.GetQuestionOBCTypes($scope.LayoutID).then(function (resObj)
    {
        //alert(JSON.stringify(resObj.data));
        if (resObj.data != '')
        {
            OBCTypeDescription = resObj.data[0].obcTypeDescription;
        }
    }, function ()
        {
            $scope.error = "Some Error.";
        });

    $scope.saveDescription = function ()
    {
        var Name = $("#txtpName").val();
        //var txtMask = $("#pnodeid").val();
        var QuestionId = $("#pnodeid").val();

        var data = { QuestionId: QuestionId, Name: Name};
        dataFactory.UpdateDescription(encodeURIComponent(JSON.stringify(data))).then(function (resObj)
        {
            questionPath($scope, dataFactory);
            toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.saveentry = function ()
    {
        var entrytype = $("#sltentry option:selected").val();
        var title = $("#txtTitle").val();
        var txtMask = $("#txtMask").val();
        var nodeid = $("#nodeid").val();
        var transportidpopup = transportids;
        //$("#transportidpopup").val();
        var txtSize = $("#txtSize").val();
        var chkNotnull = 0;
        if ($('#chkNotnull').is(':checked'))
        {
            chkNotnull = 1;
        }
        var chkminimChar = 0;
        if ($('#chkminimChar').is(':checked'))
        {
            chkminimChar = 1;
        }
        var chkExactchar = 0;
        if ($('#chkExactchar').is(':checked'))
        {
            chkExactchar = 1;
        }
        var chk0not = 0;
        if ($('#chk0not').is(':checked'))
        {
            chk0not = 1;
        }
        var txtRegex = $("#txtRegex").val();
        var sltpredefined = $("#sltpredefined option:selected").val();
        var chksave = 0;
        if ($('#chksave').is(':checked'))
        {
            chksave = 1;
        }
        var chkPropose = 0;
        if ($('#chkPropose').is(':checked'))
        {
            chkPropose = 1;
        }
        var sltPlanningFeed = $("#sltPlanningFeed option:selected").val();
        var sltFeedbackType = $("#sltFeedbackType option:selected").val();
        var txtfeedbackcode = $("#txtfeedbackcode").val();
        var QuestionRefId = "";
        var LayoutRefId = "";
        var ActivitiesRefId = "";
        var Title = "";
        var MaskText = "";
        var Size = "";
        var NotNull = "";
        var MinimumofCharacter = "";
        var ExactCharacter = "";
        var Zeronotallowed = "";
        var Regex = "";
        var Predefined = "";
        var Save = "";
        var Propose = "";
        var Planningfeedback = "";
        var FeedbackType = "";
        var PlanningfeedbackCode = "";
        var EntryType = "";
        var data = { QuestionRefId: nodeid, ActivitiesRefId: $scope.ActivityID, LayoutRefId: $scope.LayoutID, Title: title, MaskText: txtMask, Size: txtSize, NotNull: chkNotnull, MinimumofCharacter: chkminimChar, ExactCharacter: chkExactchar, Zeronotallowed: chk0not, Regex: txtRegex, Predefined: sltpredefined, Save: chksave, Propose: chkPropose, Planningfeedback: sltPlanningFeed, FeedbackType: sltFeedbackType, PlanningfeedbackCode: txtfeedbackcode, EntryType: entrytype };
        dataFactory.SaveQuestions($scope.LayoutID, transportidpopup, encodeURIComponent(JSON.stringify(data)), 'Entry').then(function (resObj)
        {
            questionPath($scope, dataFactory);
            toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }


    $scope.saveDocumentScan = function ()
    {
        var transportidpopup = transportids;
        var nodeid = $("#nodeid").val();
        var DocPlanningFeed = $("#sltDocumentPlanningFeedback option:selected").val();
        var title = $("#txtDocumentTitle").val();
        var DocumentName = $("#txtDocumentName").val();
        var data = { QuestionRefId: nodeid, ActivitiesRefId: $scope.ActivityID, LayoutRefId: $scope.LayoutID, Title: title, PlanningFeedbackLevel: DocPlanningFeed, DocumentName: DocumentName };
        dataFactory.SaveQuestions($scope.LayoutID, transportidpopup, encodeURIComponent(JSON.stringify(data)), 'Document scan').then(function (resObj)
        {
            questionPath($scope, dataFactory);
            toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.PropertysaveDocumentScan = function ()
    {
        var transportidpopup = transportids;
        var DocumetScanId = $("#documentscanid").val();
        var PlanningFeedbackLevel = $("#sltpDocumentScanPlanningFeedback option:selected").val();
        var Title = $("#txtpDocumentScanTitle").val();
        var DocumentName = $("#txtpDocumentScanName").val();
        var data = {DocumetScanId: DocumetScanId, Title: Title,PlanningFeedbackLevel: PlanningFeedbackLevel, DocumentName: DocumentName};
        //alert(JSON.stringify(data));
        dataFactory.UpdateDocumetScanProperties(encodeURIComponent(JSON.stringify(data))).then(function (resObj)
        {
            validationTrue("Documet Scan updated successfully.");
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.saveVariableList = function ()
    {
        var transportidpopup = transportids;
        var nodeid = $("#nodeid").val();
        var VarlistOthers = 0;
        if ($('#chkVariableListVarlistOthers').is(':checked'))
        {
            VarlistOthers = 1;
        }
        var title = $("#txtVariableListTitle").val();
        var Varlistid = $("#txtVariableListVarlistid").val();
        var data = { QuestionRefId: nodeid, ActivitiesRefId: $scope.ActivityID, LayoutRefId: $scope.LayoutID, Title: title, Other: VarlistOthers, VarListID: Varlistid, Name: ActivitiesName};
        dataFactory.SaveQuestions($scope.LayoutID, transportidpopup, encodeURIComponent(JSON.stringify(data)), 'Variable list').then(function (resObj)
        {
            questionPath($scope, dataFactory);
            toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.savePropertyVariableList = function ()
    {
        var transportidpopup = transportids;
        var VariableListId = $("#VariableListId").val();
        var VarlistOthers = 0;
        if ($('#chkpVariableListVarlistOthers').is(':checked'))
        {
            VarlistOthers = 1;
        }
        var title = $("#txtpVariableListTitle").val();
        var Varlistid = $("#txtpVariableListVarlistid").val();
        var pVariableListnodeid = $("#pVariableListnodeid").val();
        var data = { VariableListId: VariableListId, Title: title, Other: VarlistOthers, VarListID: Varlistid, Name: ActivitiesName };
        dataFactory.UpdateVariableListProperties(encodeURIComponent(JSON.stringify(data)), pVariableListnodeid).then(function (resObj)
        {
            questionPath($scope, dataFactory);
            toolBox($scope.toolBox, $scope);
            validationTrue("Variable list updated successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.saveTextMessage = function ()
    {
        var transportidpopup = transportids;
        var nodeid = $("#nodeid").val();
        var TextMessageSendMethod = $("#sltTextMessageSendMethod option:selected").val();
        
        var TextMessagecontent = $("#txtTextMessagecontent").val();
        var TextMessageName = "SMS " + ActivitiesName;
        var data = { QuestionRefId: nodeid, ActivitiesRefId: $scope.ActivityID, LayoutRefId: $scope.LayoutID, Name: 'SMS ' + ActivitiesName , SendMethod: TextMessageSendMethod, Messagecontent: TextMessagecontent };
        dataFactory.SaveQuestions($scope.LayoutID, transportidpopup, encodeURIComponent(JSON.stringify(data)), 'Text message').then(function (resObj)
        {
            questionPath($scope, dataFactory);
            toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.PropertysaveTextMessage = function ()
    {
        var transportidpopup = transportids;
        var TextMessageId = $("#textMessageId").val();
        var TextMessageSendMethod = $("#sltpTextMessageSendMethod option:selected").val();
        //var TextMessageName = $("#txtpTextMessageName").val();
        var TextMessagecontent = $("#txtpTextMessagecontent").val();
        var TextMessageName = "SMS " + ActivitiesName;
        var data = { TextMessageId: TextMessageId, Name: 'SMS ' + ActivitiesName, SendMethod: TextMessageSendMethod, Messagecontent: TextMessagecontent };
        dataFactory.UpdateTextMessageProperties(encodeURIComponent(JSON.stringify(data))).then(function (resObj)
        {
            questionPath($scope, dataFactory);
            //toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.saveInfomessage = function ()
    {
        var transportidpopup = transportids;
        var nodeid = $("#nodeid").val();
        var Messagetype = $("#sltInfomessageMessagetype option:selected").val();
        var Titlestyle = $("#sltInfomessageTitlestyle option:selected").val();
        var Icontype = $("#sltInfomessageIcontype option:selected").val();
        var Fontstyle = $("#sltInfomessageFontstyle option:selected").val();
        var InfomessageName = $("#txtInfomessageName").val();
        var InfomessageTitle = $("#txtInfomessageTitle").val();
        var InfomessageMessagecontent = $("#txtInfomessageMessagecontent").val();
        var data = {
            QuestionRefId: nodeid, ActivitiesRefId: $scope.ActivityID, LayoutRefId: $scope.LayoutID,
            Messagetype: Messagetype, Titlestyle: Titlestyle, Icontype: Icontype,
            Fontstyle: Fontstyle, Messagecontent: InfomessageMessagecontent, Name: InfomessageName, Title: InfomessageTitle
        };
        dataFactory.SaveQuestions($scope.LayoutID, transportidpopup, encodeURIComponent(JSON.stringify(data)), 'Info message').then(function (resObj)
        {
            questionPath($scope, dataFactory);
            toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.PropertysaveInfomessage = function ()
    {
        var transportidpopup = transportids;
        var InfomessageId = $("#InfomessageId").val();
        var Messagetype = $("#sltpInfomessageMessagetype option:selected").val();
        var Titlestyle = $("#sltpInfomessageTitlestyle option:selected").val();
        var Icontype = $("#sltpInfomessageIcontype option:selected").val();
        var Fontstyle = $("#sltpInfomessageFontstyle option:selected").val();
        var InfomessageName = $("#txtpInfomessageName").val();
        var InfomessageTitle = $("#txtpInfomessageTitle").val();
        var InfomessageMessagecontent = $("#txtpInfomessageMessagecontent").val();
        var pInfomessagenodeid = $("#pInfomessagenodeid").val();
        //alert(pInfomessagenodeid);
        var data = {
            InfomessageId: InfomessageId,Messagetype: Messagetype, Titlestyle: Titlestyle, Icontype: Icontype,
            Fontstyle: Fontstyle, Messagecontent: InfomessageMessagecontent, Name: InfomessageName, Title: InfomessageTitle
        };
        dataFactory.UpdateInfomessageProperties(encodeURIComponent(JSON.stringify(data)), pInfomessagenodeid).then(function (resObj)
        {
            questionPath($scope, dataFactory);
            //toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.saveCheckoff = function ()
    {
        var transportidpopup = transportids;
        var nodeid = $("#nodeid").val();
        var CheckoffPlanninglevel = $("#sltCheckoffPlanninglevel option:selected").val();
        var Name = $("#txtCheckoffName").val();
        var data = { QuestionRefId: nodeid, ActivitiesRefId: $scope.ActivityID, LayoutRefId: $scope.LayoutID, Name: Name, Planninglevel: CheckoffPlanninglevel };
        dataFactory.SaveQuestions($scope.LayoutID, transportidpopup, encodeURIComponent(JSON.stringify(data)), 'Check off').then(function (resObj)
        {
            questionPath($scope, dataFactory);
            toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.PropertysaveCheckoff = function ()
    {
        var transportidpopup = transportids;
        var CheckoffId = $("#CheckoffId").val();
        var CheckoffPlanninglevel = $("#sltpCheckoffPlanninglevel option:selected").val();
        var Name = $("#txtpCheckoffName").val();
        var pCheckoffnodeid = $("#pCheckoffnodeid").val();
        var data = { CheckoffId: CheckoffId, Name: Name, Planninglevel: CheckoffPlanninglevel };
        dataFactory.UpdateCheckoffProperties(encodeURIComponent(JSON.stringify(data)), pCheckoffnodeid).then(function (resObj)
        {
            questionPath($scope, dataFactory);
            //toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.savePlanningselect = function ()
    {
        var transportidpopup = transportids;
        var nodeid = $("#nodeid").val();

        var Title = $("#txtPlanningselectTitle").val();
        var Planninglevel = $("#sltPlanningselectPlanninglevel option:selected").val();
        var Planningspecific = $("#sltPlanningselectPlanningspecific option:selected").val();
        var Chooseactivity = $("#sltPlanningselectChooseactivity option:selected").val();


        var Startselecteditem = 0;
        if ($('#chkPlanningselectStartselecteditem').is(':checked'))
        {
            Startselecteditem = 1;
        }
        var Other = $("#sltPlanningselectOther option:selected").val();
        var OtherTitle = $("#txtPlanningselectOtherTitle").val();
        var Fixedvalue = $("#txtPlanningselectFixedvalue").val();
        var QP = $("#txtPlanningselectQP").val();
        var Planningreadout = $("#txtPlanningselectPlanningreadout").val();


        var data = {
            QuestionRefId: nodeid, ActivitiesRefId: $scope.ActivityID, LayoutRefId: $scope.LayoutID,
            Title: Title, Planninglevel: Planninglevel, Planningspecific: Planningspecific, Chooseactivity: Chooseactivity, Startselecteditem: Startselecteditem,
            Other: Other, OtherTitle: OtherTitle, Fixedvalue: Fixedvalue, QP: QP, Planningreadout: Planningreadout
        };
        dataFactory.SaveQuestions($scope.LayoutID, transportidpopup, encodeURIComponent(JSON.stringify(data)), 'Planning Select').then(function (resObj)
        {
            questionPath($scope, dataFactory);
            toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.PropertysavePlanningselect = function ()
    {
        var transportidpopup = transportids;
        var PlanningselectId = $("#PlanningselectId").val();

        var Title = $("#txtpPlanningselectTitle").val();
        var Planninglevel = $("#sltPlanningselectpPlanninglevel option:selected").val();
        var Planningspecific = $("#sltPlanningselectpPlanningspecific option:selected").val();
        var Chooseactivity = $("#sltPlanningselectpChooseactivity option:selected").val();


        var Startselecteditem = 0;
        if ($('#chkPlanningselectpStartselecteditem').is(':checked'))
        {
            Startselecteditem = 1;
        }
        var Other = $("#sltPlanningselectpOther option:selected").val();
        var OtherTitle = $("#txtPlanningselectpOtherTitle").val();
        var Fixedvalue = $("#txtPlanningselectpFixedvalue").val();
        var QP = $("#txtPlanningselectpQP").val();
        var Planningreadout = $("#txtPlanningselectpPlanningreadout").val();


        var data = {
            PlanningselectId: PlanningselectId, Title: Title, Planninglevel: Planninglevel, Planningspecific: Planningspecific, Chooseactivity: Chooseactivity, Startselecteditem: Startselecteditem,
            Other: Other, OtherTitle: OtherTitle, Fixedvalue: Fixedvalue, QP: QP, Planningreadout: Planningreadout
        };
        dataFactory.UpdatePlanningselectProperties(encodeURIComponent(JSON.stringify(data))).then(function (resObj)
        {
            //questionPath($scope, dataFactory);
            //toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.saveSetaction = function ()
    {
        var transportidpopup = transportids;
        var nodeid = $("#nodeid").val();
        var Title = $("#txtSetactionTitle").val();
        var NextQpName = $("#txtSetactionName").val();
        var SelectQP = $("#sltSetactionQP option:selected").val();
        var CancelcurrentQP = $("#sltSetactionCancelcurrentQP option:selected").val();
        var NextViewName = $("#txtSetactionViewName").val();
        var SelectView = $("#sltSetactionSelectView option:selected").val();
        var AllowanceName = $("#txtSetactionAllowanceName").val();
        var SelectAllowance = $("#sltSetactionSelectAllowance option:selected").val();
        var InfoColumnName = $("#txtSetactionName").val();
        var SelectInfoColumn = $("#sltSetactionSelectInfoColumn option:selected").val();
        var SetReset = $("#sltSetactionSetReset option:selected").val();
        var Value = $("#sltSetactionValue option:selected").val();

        var data = {
            QuestionRefId: nodeid, ActivitiesRefId: $scope.ActivityID, LayoutRefId: $scope.LayoutID,
            Title: Title, NextQpName: NextQpName, SelectQP: SelectQP, CancelcurrentQP: CancelcurrentQP, NextViewName: NextViewName,
            SelectView: SelectView, AllowanceName: AllowanceName, SelectAllowance: SelectAllowance, InfoColumnName: InfoColumnName,
            SelectInfoColumn: SelectInfoColumn, SetReset: SetReset, Value: Value
        };
        dataFactory.SaveQuestions($scope.LayoutID, transportidpopup, encodeURIComponent(JSON.stringify(data)), 'Set action / value').then(function (resObj)
        {
            questionPath($scope, dataFactory);
            toolBox($scope.toolBox, $scope);
            validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
            $('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
    }

    $scope.savechioce = function ()
    {
        var transportidpopup = transportids;
        var nodeid = $("#nodeid").val();
        var Title = $("#txtChiocetitle").val();
        //alert($("#txtChiocetitle").val());
        var Recursiveloop = 0;
        if ($('#chkChioceRecursiveloop').is(':checked'))
        {
            Recursiveloop = 1;
        }
        var Save = 0;
        if ($('#chkChiocesave').is(':checked'))
        {
            Save = 1;
        }
        var Propose = $('txtChiocepropose').val();
        var Planningfeedback = $("#sltChiocePlanningFeed option:selected").val();
        var FeedbackType = $("#sltChioceFeedbackType option:selected").val();
        var data = {
            QuestionRefId: nodeid, ActivitiesRefId: $scope.ActivityID, LayoutRefId: $scope.LayoutID,
            Title: Title, Recursiveloop: Recursiveloop, Save: Save, Propose: Propose, Planningfeedback: Planningfeedback,FeedbackType: FeedbackType
        };
        //alert(JSON.stringify(data));
        dataFactory.SaveQuestions($scope.LayoutID, transportidpopup, encodeURIComponent(JSON.stringify(data)), 'Choice').then(function (resObj)
        {
            $('#cancelmain').attr("disabled", true);
            $('#submit').attr("disabled", true);
            $('#choiceidfor').val(resObj.data);
            if (availablePlanningType == 'false')
            {
                $('#txtaddChiocefeedbackcode').attr("disabled", true);
                $('#sltChioceFeedbackType').attr("disabled", true);
            }
            $('#txtaddchioceTitle').attr("disabled", false);
            $('#chkaddChioceendloop').attr("disabled", false);
            $('#txtaddChiocefeedbackcode').attr("disabled", false);
            $('#cancel').attr("disabled", false);
            $('#Previous').attr("disabled", false);
            $('#Next').attr("disabled", false);
            $('#Finish').attr("disabled", false);
            //alert(JSON.stringify(resObj.data));
            //questionPath($scope, dataFactory);
           // toolBox($scope.toolBox, $scope);
            //validationTrue("Questions saved successfully.");
            //$uibModalInstance.dismiss('cancel');
           //$('#toolboxModal').modal('hide');

        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
        
        
    }

    $scope.Saveaddchoice = function ()
    {
        var ChioceRefId = $('#choiceidfor').val();
        var Title = $('#txtaddchioceTitle').val();
        var Endrecursiveloop = 0;
        if ($('#chkaddChioceendloop').is(':checked'))
        {
            Endrecursiveloop = 1;
        }
        var PlanningfeedbackCode= $('#txtaddChiocefeedbackcode').val();
        var data = {
            ChioceRefId: ChioceRefId,
            Title: Title, PlanningfeedbackCode: PlanningfeedbackCode, Endrecursiveloop: Endrecursiveloop
        };
        dataFactory.SaveAddChioceProperties(encodeURIComponent(JSON.stringify(data))).then(function (resObj)

        {
            $('#txtaddchioceTitle').val('');
            $('#txtaddChiocefeedbackcode').val('');
            $('#chkaddChioceendloop').prop('checked', false);
            
        }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });


        
    }

    $scope.finishaddchoice = function ()
    {
        questionPath($scope, dataFactory);
        toolBox($scope.toolBox, $scope);
        validationTrue("Questions saved successfully.");
        $('#toolboxModal').modal('hide');
    }

    $scope.toolboxclear = function ()
    {
        toolBox($scope.toolBox, $scope);
    }
}]);

function toolBox(toolboxitems, $scope)
{
    $("#divtoolboxtree").ejTreeView({
        enableMultipleExpand: true,
        allowDragAndDrop: true,
        fields: {
            dataSource: toolboxitems, id: "toolboxId",
            parentId: "subToolboxId", text: "toolboxName", hasChild: true, expanded: true
        }, enableMultipleExpand: true,
        allowDragAndDropAcrossControl: false,
        nodeSelect: function (args)
        {
            if ($scope.Transporttypecount > 0)
            {

                var treeObj = $("#" + transportids).ejTreeView('instance');
                var nodes = treeObj.getSelectedNode();

                var toolObj = $("#divtoolboxtree").ejTreeView('instance');
                var tools = toolObj.getSelectedNode();
                Properties(tools[0].name, nodes[0].name);
                //alert(JSON.stringify(nodes.questionId));

                var treeObjid = $("#" + transportids).data("ejTreeView");
                var nodeId = treeObj.getSelectedNode().attr("id");
                $("#nodeid").val(nodeId);
                $("#transportidpopup").val($("#transportid").val());
            }
            else
            {
                var treeObj = $("#divQuestiontreewithouttransport").ejTreeView('instance');
                var nodes = treeObj.getSelectedNode();

                var toolObj = $("#divtoolboxtree").ejTreeView('instance');
                var tools = toolObj.getSelectedNode();
                Properties(tools[0].name, nodes[0].name);
                //alert(JSON.stringify(nodes.questionId));

                var treeObjid = $("#divQuestiontreewithouttransport").data("ejTreeView");
                var nodeId = treeObj.getSelectedNode().attr("id");
                //alert(nodeId);
                $("#nodeid").val(nodeId);
                $("#transportidpopup").val($("#transportid").val());
            }

        }
    });
    var treeObj = $("#divtoolboxtree").data("ejTreeView");
    treeObj.expandAll();

}


function Properties(header, name)
{
    $(".popupheader").empty();
    $('.diventrypopup').removeClass('show').addClass('hide');
    $('.divchiocepopup').removeClass('show').addClass('hide');
    $('.divDocumentScanpopup').removeClass('show').addClass('hide');
    $('.divVariableListpopup').removeClass('show').addClass('hide');
    $('.divTextMessagepopup').removeClass('show').addClass('hide');
    $('.divInfomessagepopup').removeClass('show').addClass('hide');
    $('.divCheckoffpopup').removeClass('show').addClass('hide');
    $('.divPlanningSelectpopup').removeClass('show').addClass('hide');
    $('.divSetactionpopup').removeClass('show').addClass('hide');

    
    if (header == 'Choice')
    {
        $('#toolboxModal').modal();
        $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
        $('#frmchioce').removeClass('hide').addClass('show');
       
        if (OBCTypeDescription == 'TX-SMART')
        {
            $('#chkChiocesave').attr("disabled", true);
            $('#txtChiocepropose').attr("disabled", true);
            $('#chkChioceRecursiveloop').attr("disabled", true);
        }

        if (availablePlanningType=='false')
        {
            $('#txtaddChiocefeedbackcode').attr("disabled", true);
            $('#sltChioceFeedbackType').attr("disabled", true);
        }
        if (ActivitiesPlanning == 'Receive' || ActivitiesPlanning == 'Use')
        {
            $('#sltChiocePlanningFeed').attr("disabled", true);
        }
        $('#txtaddchioceTitle').attr("disabled", true);
        $('#chkaddChioceendloop').attr("disabled", true);
        $('#txtaddChiocefeedbackcode').attr("disabled", true);
        $('#cancel').attr("disabled", true);
        $('#Previous').attr("disabled", true);
        $('#Next').attr("disabled", true);
        $('#Finish').attr("disabled", true);
    }
    if (header == 'Entry')
    {
        $('#toolboxModal').modal();
        $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
        $('#frmentry').removeClass('hide').addClass('show');
    }
    if (header == 'Document scan')
    {
        $('#toolboxModal').modal();
        $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
        $('#frmDocumentScan').removeClass('hide').addClass('show');
        $('#txtDocumentTitle').val(ActivitiesName + '  DOCSCAN');
    }
    if (header == 'Variable list')
    {
        $('#toolboxModal').modal();
        $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
        $('#frmVariableList').removeClass('hide').addClass('show');
    }
    if (header == 'Text message')
    {
        $('#toolboxModal').modal();
        $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
        $('#frmTextMessage').removeClass('hide').addClass('show');
        $('#txtTextMessageName').val('SMS' + ' ' + ActivitiesName);
        $('#sltTextMessageSendMethod').val('Upon finishing QP');
    }
    if (header == 'Info message')
    {
        $('#toolboxModal').modal();
        $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
        $('#frmInfomessage').removeClass('hide').addClass('show');
    }
    if (header == 'Check off')
    {
        $('#toolboxModal').modal();
        $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
        $('#frmCheckoff').removeClass('hide').addClass('show');
    }
    if (header == 'Planning Select')
    {
        $('#toolboxModal').modal();
        $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
        $('#frmPlanningselect').removeClass('hide').addClass('show');
        $('#txtPlanningselectTitle').val(LTC + ' ' + ActivitiesName);
    }
    if (header == 'Set action / value')
    {
        $('#toolboxModal').modal();
        $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
        $('#frmSetaction').removeClass('hide').addClass('show');
    }


}

function questionPath($scope, dataFactory)
{
    if ($scope.Transporttypecount > 0)
    {

        var question = [];

        dataFactory.GetQuestions($scope.LayoutID, $scope.ActivityID, transportids).then(function (resObj)
        {
            question = resObj.data;

        }, function ()
            {
                $scope.error = "Some Error.";
            });
        setTimeout(function ()
        {
            //alert(JSON.stringify(question));

            $("#" + transportids).ejTreeView({
                allowDragAndDrop: true,
                fields: {
                    dataSource: question, id: "questionId",
                    parentId: "parentId", text: "name", hasChild: true, expanded: true
                },
                allowDragAndDropAcrossControl: false,
                nodeSelect: function (args)
                {
                    var treeObjid = $("#" + transportids).data("ejTreeView");
                    var nodeId = treeObj.getSelectedNode().attr("id");

                    angular.forEach(question, function (value, key)
                    {
                        if (value.questionId == nodeId)
                        {
                            $scope.propertyType = value.propertyName;
                            $scope.transportidpopupproperties = value.transporttypeRefId;
                            $scope.description = value.name;
                            //alert($scope.propertyType);
                        }
                    });
                    //alert($scope.propertyType);
                    $('.divpropertiesDescriptionpopup').removeClass('show').addClass('hide');
                    $('.divpropertieschiocepopup').removeClass('show').addClass('hide');
                    $('.divpropertiesentrypopup').removeClass('show').addClass('hide');
                    $('.divpropertiesDocumentScanpopup').removeClass('show').addClass('hide');
                    $('.divpropertiesVariableListpopup').removeClass('show').addClass('hide');
                    $('.divpropertiesTextMessagepopup').removeClass('show').addClass('hide');
                    $('.divpropertiesInfomessagepopup').removeClass('show').addClass('hide');
                    $('.divpropertiesCheckoffpopup').removeClass('show').addClass('hide');
                    $('.divpropertiesPlanningSelectpopup').removeClass('show').addClass('hide');
                    $('.divpropertiesSetactionpopup').removeClass('show').addClass('hide');
                    if ($scope.propertyType == 'Description')
                    {
                        
                        $('#propertiesfrmDescription').removeClass('hide').addClass('show');
                        $("#pnodeid").val(nodeId);
                        $("#txtpName").val($scope.description);
                        $("#ptransportidpopup").val($scope.transportidpopupproperties);
                    }
                    if ($scope.propertyType == 'Choice')
                    {
                        
                        $('#propertiesfrmchioce').removeClass('hide').addClass('show');
                        dataFactory.GetChioceProperties(nodeId).then(function (resObj)
                        {
                            //$scope.EntryProperties = resObj.data;

                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    $("#ChioceId").val(value.chioceId);
                                    $("#txtpChiocetitle").val(value.title);
                                    //alert($("#txtChiocetitle").val());
                                    if (value.recursiveloop == 1)
                                    {
                                        $('#chkpChioceRecursiveloop').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpChioceRecursiveloop').prop('checked', false);
                                    }
                                    if (value.save == 1)
                                    {
                                        $('#chkpChiocesave').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpChiocesave').prop('checked', false);
                                    }


                                    $('#txtpChiocepropose').val(value.propose);
                                    $("#sltpChiocePlanningFeed").val(value.planningfeedback);
                                    $("#sltpChioceFeedbackType").val(value.feedbackType);

                                }
                            });

                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });

                        if (OBCTypeDescription == 'TX-SMART')
                        {
                            $('#chkpChiocesave').attr("disabled", true);
                            $('#txtpChiocepropose').attr("disabled", true);
                            $('#chkpChioceRecursiveloop').attr("disabled", true);
                        }

                        if (availablePlanningType == 'false')
                        {
                            //$('#txtaddChiocefeedbackcode').attr("disabled", true);
                            $('#sltpChioceFeedbackType').attr("disabled", true);
                        }
                        if (ActivitiesPlanning == 'Receive' || ActivitiesPlanning == 'Use')
                        {
                            $('#sltpChiocePlanningFeed').attr("disabled", true);
                        }
                    }
                    if ($scope.propertyType == 'Entry')
                    {

                        dataFactory.GetEntryProperties(nodeId).then(function (resObj)
                        {
                            $scope.EntryProperties = resObj.data;
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    $("#entryid").val(value.entryId);
                                    $("#sltpentry").val(value.entryType);
                                    $("#txtpTitle").val(value.title);
                                    $("#txtpMask").val(value.maskText);
                                    $("#nodeid").val(value.questionRefId);
                                    $("#transportidpopup").val($scope.transportidpopupproperties);
                                    $("#txtpSize").val(value.size);
                                    if (value.notNull == 1)
                                    {
                                        $('#chkpNotnull').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpNotnull').prop('checked', false);
                                    }
                                    if (value.minimumofCharacter == 1)
                                    {
                                        $('#chkpminimChar').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpminimChar').prop('checked', false);
                                    }
                                    if (value.exactCharacter == 1)
                                    {
                                        $('#chkpExactchar').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpExactchar').prop('checked', false);
                                    }
                                    if (value.zeronotallowed == 1)
                                    {
                                        $('#chkp0not').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkp0not').prop('checked', false);
                                    }
                                    $("#txtpRegex").val(value.regex);
                                    $("#sltppredefined").val(value.predefined);
                                    if (value.save == 1)
                                    {
                                        $('#chkpsave').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpsave').prop('checked', false);
                                    }
                                    if (value.propose == 1)
                                    {
                                        $('#chkpPropose').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpPropose').prop('checked', false);
                                    }
                                    $("#sltpPlanningFeed").val(value.planningfeedback);
                                    $("#sltpFeedbackType").val(value.feedbackType);
                                    $("#txtpfeedbackcode").val(value.planningfeedbackCode);

                                }
                            });

                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $('#propertiesfrmentry').removeClass('hide').addClass('show');

                    }
                    if ($scope.propertyType == 'Document scan')
                    {
                        dataFactory.GetDocumetScanProperties(nodeId).then(function (resObj)
                        {
                            //$scope.EntryProperties = resObj.data;
                            $scope.DocumetScan = resObj.data;
                            //alert(JSON.stringify(resObj.data));
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    // alert(nodeId);
                                    $("#txtpDocumentScanTitle").val(value.title);
                                    $("#txtpDocumentScanName").val(value.documentName);
                                    $("#sltpDocumentScanPlanningFeedback").val(value.planningFeedbackLevel);
                                    $("#documentscanid").val(value.documetScanId);
                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $("#pDocumentScannodeid").val(nodeId);
                        $("#pDocumentScantransportidpopup").val($scope.transportidpopupproperties);
                        $('#propertiesfrmDocumentScan').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Planning Select')
                    {
                        dataFactory.GetPlanningselectProperties(nodeId).then(function (resObj)
                        {
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    $("#txtpPlanningselectTitle").val(value.title);
                                    $("#sltPlanningselectpPlanninglevel").val(value.planninglevel);
                                    $("#sltPlanningselectpPlanningspecific").val(value.planningspecific);
                                    $("#sltPlanningselectpChooseactivity").val(value.chooseactivity);
                                    $("#PlanningselectId").val(value.planningselectId);
                                    $("#sltPlanningselectpOther").val(value.other);
                                    $("#txtPlanningselectpOtherTitle").val(value.otherTitle);
                                    $("#txtPlanningselectpFixedvalue").val(value.fixedvalue);
                                    $("#txtPlanningselectpQP").val(value.qP);
                                    $("#txtPlanningselectpPlanningreadout").val(value.planningreadout);
                                    if (value.startselecteditem == 1)
                                    {
                                        $('#chkPlanningselectpStartselecteditem').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkPlanningselectpStartselecteditem').prop('checked', false);
                                    }
                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });

                        $("#pPlanningselecttransportidpopup").val($scope.transportidpopupproperties);
                        $('.divpropertiesPlanningSelectpopup').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Check off')
                    {
                        dataFactory.GetCheckoffProperties(nodeId).then(function (resObj)
                        {
                            //alert(resObj.data);
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    $("#sltpCheckoffPlanninglevel").val(value.planninglevel);
                                    $("#txtpCheckoffName").val(value.name);
                                    $("#CheckoffId").val(value.checkoffId);
                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $("#pCheckoffnodeid").val(nodeId);
                        $("#pCheckofftransportidpopup").val($scope.transportidpopupproperties);
                        $('#divpropertiesCheckoffpopup').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Conditionalaction')
                    {

                    }
                    if ($scope.propertyType == 'Info message')
                    {
                        dataFactory.GetInfomessageProperties(nodeId).then(function (resObj)
                        {
                            //$scope.DocumetScan = resObj.data;
                            //alert(JSON.stringify(resObj.data));
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    // alert(nodeId);
                                    $("#txtpInfomessageName").val(value.name);
                                    $("#txtpInfomessageTitle").val(value.title);
                                    $("#sltpInfomessageMessagetype").val(value.messagetype);
                                    $("#InfomessageId").val(value.infomessageId);
                                    $("#sltpInfomessageTitlestyle").val(value.titlestyle);
                                    $("#sltpInfomessageIcontype").val(value.icontype);
                                    $("#txtpInfomessageMessagecontent").val(value.messagecontent);
                                    $("#sltpInfomessageFontstyle").val(value.fontstyle);
                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $("#pInfomessagenodeid").val(nodeId);
                        $("#pInfomessagetransportidpopup").val($scope.transportidpopupproperties);
                        $('#propertiesfrmInfomessage').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Text message')
                    {
                        dataFactory.GetTextMessageProperties(nodeId).then(function (resObj)
                        {
                            $scope.DocumetScan = resObj.data;
                            //alert(JSON.stringify(resObj.data));
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    // alert(nodeId);
                                    $("#txtpTextMessageName").val(value.name);
                                    $("#sltpTextMessageSendMethod").val(value.sendMethod);
                                    $("#txtpTextMessagecontent").val(value.messagecontent);
                                    $("#textMessageId").val(value.textMessageId);
                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });

                        $("#pTextMessagetransportidpopup").val($scope.transportidpopupproperties);
                        $('#propertiesfrmTextMessage').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Set action / value')
                    {
                        dataFactory.GetSetActionValueProperties(nodeId).then(function (resObj)
                        {
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    // alert(nodeId);
                                    $("#txtpSetactionTitle").val(value.title);
                                    $("#txtpSetactionName").val(value.nextQpName);
                                    $("#sltpSetactionQP").val(value.selectQP);
                                    $("#sltpSetactionCancelcurrentQP").val(value.cancelcurrentQP);
                                    $("#txtpSetactionViewName").val(value.nextViewName);
                                    $("#sltpSetactionSelectView").val(value.selectView);
                                    $("#txtpSetactionAllowanceName").val(value.allowanceName);
                                    $("#sltpSetactionSelectAllowance").val(value.selectAllowance);
                                    $("#txtpSetactionName").val(value.infoColumnName);
                                    $("#sltpSetactionSelectInfoColumn").val(value.selectInfoColumn);
                                    $("#sltpSetactionSetReset").val(value.setReset);
                                    $("#sltpSetactionValue").val(value.value);
                                    $("#SetActionValueId").val(value.setActionValueId);

                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $("#nodeid").val(nodeId);
                        $("#pSetactiontransportidpopup").val($scope.transportidpopupproperties);
                        $('#propertiesfrmSetaction').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Variable list')
                    {
                        dataFactory.GetVariableListProperties(nodeId).then(function (resObj)
                        {
                            //$scope.DocumetScan = resObj.data;
                            //alert(JSON.stringify(resObj.data));
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    // alert(nodeId);
                                    $("#txtpVariableListTitle").val(value.title);
                                    $("#txtpVariableListVarlistid").val(value.varListID);
                                    if (value.other == 1)
                                    {
                                        $('#chkpVariableListVarlistOthers').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpVariableListVarlistOthers').prop('checked', false);
                                    }
                                    $("#VariableListId").val(value.variableListId);

                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $("#pVariableListnodeid").val(nodeId);
                        $("#pVariableListtransportidpopup").val($scope.transportidpopupproperties);
                        $('#propertiesfrmVariableList').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Trailer')
                    {

                    }
                    if ($scope.propertyType == 'Transporttype')
                    {

                    }

                }

            });
            var treeObj = $("#" + transportids).data("ejTreeView");
            treeObj.expandAll();
        }, 500);
    }
    else
    {
        var question = [];
        dataFactory.GetQuestions($scope.LayoutID, $scope.ActivityID, 0).then(function (resObj)
        {
            question = resObj.data;

        }, function ()
            {
                $scope.error = "Some Error.";
            });
        setTimeout(function ()
        {
            $("#divQuestiontreewithouttransport").ejTreeView({
                allowDragAndDrop: true,
                fields: {
                    dataSource: question, id: "questionId",
                    parentId: "parentId", text: "name", hasChild: true, expanded: true
                },
                allowDragAndDropAcrossControl: false,
                nodeSelect: function (args)
                {
                    var treeObjid = $("#divQuestiontreewithouttransport").data("ejTreeView");
                    var nodeId = treeObjid.getSelectedNode().attr("id");
                    // alert(JSON.stringify(question));
                    angular.forEach(question, function (value, key)
                    {
                        if (value.questionId == nodeId)
                        {
                            $scope.propertyType = value.propertyName;
                            $scope.transportidpopupproperties = value.transporttypeRefId;
                            $scope.description = value.name;
                        }
                    });
                    //alert($scope.propertyType);
                    $('.divpropertiesDescriptionpopup').removeClass('show').addClass('hide');
                    $('.divpropertieschiocepopup').removeClass('show').addClass('hide');
                    $('.divpropertiesentrypopup').removeClass('show').addClass('hide');
                    $('.divpropertiesDocumentScanpopup').removeClass('show').addClass('hide');
                    $('.divpropertiesVariableListpopup').removeClass('show').addClass('hide');
                    $('.divpropertiesTextMessagepopup').removeClass('show').addClass('hide');
                    $('.divpropertiesInfomessagepopup').removeClass('show').addClass('hide');
                    $('.divpropertiesCheckoffpopup').removeClass('show').addClass('hide');
                    $('.divpropertiesPlanningSelectpopup').removeClass('show').addClass('hide');
                    $('.divpropertiesSetactionpopup').removeClass('show').addClass('hide');
                    if ($scope.propertyType == 'Description')
                    {
                        $('#propertiesfrmDescription').removeClass('hide').addClass('show');
                        $("#pnodeid").val(nodeId);
                        $("#txtpName").val($scope.description);
                        $("#ptransportidpopup").val($scope.transportidpopupproperties);
                    }
                    if ($scope.propertyType == 'Choice')
                    {
                       
                        $('#propertiesfrmchioce').removeClass('hide').addClass('show');
                        dataFactory.GetChioceProperties(nodeId).then(function (resObj)
                        {
                            //$scope.EntryProperties = resObj.data;
                            //alert(JSON.stringify(resObj.data));
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    $("#ChioceId").val(value.chioceId);
                                    $("#txtpChiocetitle").val(value.title);
                                    $("#txtpChiocepropose").val(value.propose);
                                    $("#sltpChioceFeedbackType").val(value.feedbackType);
                                    $("#sltpChiocePlanningFeed").val(value.planningfeedback);
                                    if (value.recursiveloop == 1)
                                    {
                                        $("#chkpChioceRecursiveloop").prop('checked', true);
                                    }
                                    else
                                    {
                                        $("#chkpChioceRecursiveloop").prop('checked', false);
                                    }
                                    if (value.save == 1)
                                    {
                                        $("#chkpChiocesave").prop('checked', true);
                                    }
                                    else
                                    {
                                        $("#chkpChiocesave").prop('checked', false);
                                    }


                                   
                                    

                                }
                            });

                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });

                        if (OBCTypeDescription == 'TX-SMART')
                        {
                            $('#chkpChiocesave').attr("disabled", true);
                            $('#txtpChiocepropose').attr("disabled", true);
                            $('#chkpChioceRecursiveloop').attr("disabled", true);
                        }

                        if (availablePlanningType == 'false')
                        {
                            //$('#txtaddChiocefeedbackcode').attr("disabled", true);
                            $('#sltpChioceFeedbackType').attr("disabled", true);
                        }
                        if (ActivitiesPlanning == 'Receive' || ActivitiesPlanning == 'Use')
                        {
                            $('#sltpChiocePlanningFeed').attr("disabled", true);
                        }
                    }
                    if ($scope.propertyType == 'Entry')
                    {
                        dataFactory.GetEntryProperties(nodeId).then(function (resObj)
                        {
                            $scope.EntryProperties = resObj.data;
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    $("#entryid").val(value.entryId);
                                    $("#sltpentry").val(value.entryType);
                                    $("#txtpTitle").val(value.title);
                                    $("#txtpMask").val(value.maskText);
                                    $("#nodeid").val(value.questionRefId);
                                    $("#transportidpopup").val($scope.transportidpopupproperties);
                                    $("#txtpSize").val(value.size);
                                    if (value.notNull == 1)
                                    {
                                        $('#chkpNotnull').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpNotnull').prop('checked', false);
                                    }
                                    if (value.minimumofCharacter == 1)
                                    {
                                        $('#chkpminimChar').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpminimChar').prop('checked', false);
                                    }
                                    if (value.exactCharacter == 1)
                                    {
                                        $('#chkpExactchar').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpExactchar').prop('checked', false);
                                    }
                                    if (value.zeronotallowed == 1)
                                    {
                                        $('#chkp0not').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkp0not').prop('checked', false);
                                    }
                                    $("#txtpRegex").val(value.regex);
                                    $("#sltppredefined").val(value.predefined);
                                    if (value.save == 1)
                                    {
                                        $('#chkpsave').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpsave').prop('checked', false);
                                    }
                                    if (value.propose == 1)
                                    {
                                        $('#chkpPropose').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpPropose').prop('checked', false);
                                    }
                                    $("#sltpPlanningFeed").val(value.planningfeedback);
                                    $("#sltpFeedbackType").val(value.feedbackType);
                                    $("#txtpfeedbackcode").val(value.planningfeedbackCode);

                                }
                            });

                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $('#propertiesfrmentry').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Planning Select')
                    {
                        dataFactory.GetPlanningselectProperties(nodeId).then(function (resObj)
                        {
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    $("#txtpPlanningselectTitle").val(value.title);
                                    $("#sltPlanningselectpPlanninglevel").val(value.planninglevel);
                                    $("#sltPlanningselectpPlanningspecific").val(value.planningspecific);
                                    $("#sltPlanningselectpChooseactivity").val(value.chooseactivity);
                                    $("#PlanningselectId").val(value.planningselectId);
                                    $("#sltPlanningselectpOther").val(value.other);
                                    $("#txtPlanningselectpOtherTitle").val(value.otherTitle);
                                    $("#txtPlanningselectpFixedvalue").val(value.fixedvalue);
                                    $("#txtPlanningselectpQP").val(value.qP);
                                    $("#txtPlanningselectpPlanningreadout").val(value.planningreadout);
                                    if (value.startselecteditem == 1)
                                    {
                                        $('#chkPlanningselectpStartselecteditem').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkPlanningselectpStartselecteditem').prop('checked', false);
                                    }
                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });

                        $("#pPlanningselecttransportidpopup").val($scope.transportidpopupproperties);
                        $('.divpropertiesPlanningSelectpopup').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Check off')
                    {
                        dataFactory.GetCheckoffProperties(nodeId).then(function (resObj)
                        {
                            //$scope.DocumetScan = resObj.data;
                            //alert(JSON.stringify(resObj.data));
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    $("#sltpCheckoffPlanninglevel").val(value.planninglevel);
                                    $("#txtpCheckoffName").val(value.name);
                                    $("#CheckoffId").val(value.checkoffId);
                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $("#pCheckoffnodeid").val(nodeId);
                        $("#pCheckofftransportidpopup").val($scope.transportidpopupproperties);
                        $('.divpropertiesCheckoffpopup').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Conditionalaction')
                    {

                    }
                    if ($scope.propertyType == 'Info message')
                    {
                        dataFactory.GetInfomessageProperties(nodeId).then(function (resObj)
                        {
                            //$scope.DocumetScan = resObj.data;
                            //alert(JSON.stringify(resObj.data));
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    // alert(nodeId);
                                    $("#txtpInfomessageName").val(value.name);
                                    $("#txtpInfomessageTitle").val(value.title);
                                    $("#sltpInfomessageMessagetype").val(value.messagetype);
                                    $("#InfomessageId").val(value.infomessageId);
                                    $("#sltpInfomessageTitlestyle").val(value.titlestyle);
                                    $("#sltpInfomessageIcontype").val(value.icontype);
                                    $("#txtpInfomessageMessagecontent").val(value.messagecontent);
                                    $("#sltpInfomessageFontstyle").val(value.fontstyle);
                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $("#pInfomessagenodeid").val(nodeId);
                        $("#pInfomessagetransportidpopup").val($scope.transportidpopupproperties);
                        $('#propertiesfrmInfomessage').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Text message')
                    {
                        dataFactory.GetTextMessageProperties(nodeId).then(function (resObj)
                        {
                            //$scope.DocumetScan = resObj.data;
                            //alert(JSON.stringify(resObj.data));
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    // alert(nodeId);
                                    $("#txtpTextMessageName").val(value.name);
                                    $("#sltpTextMessageSendMethod").val(value.sendMethod);
                                    $("#txtpTextMessagecontent").val(value.messagecontent);
                                    $("#textMessageId").val(value.textMessageId);
                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $("#pTextMessagenodeid").val(nodeId);
                        $("#pTextMessagetransportidpopup").val($scope.transportidpopupproperties);
                        $('#propertiesfrmTextMessage').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Set action / value')
                    {
                        dataFactory.GetSetActionValueProperties(nodeId).then(function (resObj)
                        {
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    // alert(nodeId);
                                    $("#txtpSetactionTitle").val(value.title);
                                    $("#txtpSetactionName").val(value.nextQpName);
                                    $("#sltpSetactionQP").val(value.selectQP);
                                    $("#sltpSetactionCancelcurrentQP").val(value.cancelcurrentQP);
                                    $("#txtpSetactionViewName").val(value.nextViewName);
                                    $("#sltpSetactionSelectView").val(value.selectView);
                                    $("#txtpSetactionAllowanceName").val(value.allowanceName);
                                    $("#sltpSetactionSelectAllowance").val(value.selectAllowance);
                                    $("#txtpSetactionName").val(value.infoColumnName);
                                    $("#sltpSetactionSelectInfoColumn").val(value.selectInfoColumn);
                                    $("#sltpSetactionSetReset").val(value.setReset);
                                    $("#sltpSetactionValue").val(value.value);
                                    $("#SetActionValueId").val(value.setActionValueId);

                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $("#nodeid").val(nodeId);
                        $("#pSetactiontransportidpopup").val($scope.transportidpopupproperties);
                        $('#propertiesfrmSetaction').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Variable list')
                    {
                        dataFactory.GetVariableListProperties(nodeId).then(function (resObj)
                        {
                            //$scope.DocumetScan = resObj.data;
                            //alert(nodeId);
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    // alert(nodeId);
                                    $("#txtpVariableListTitle").val(value.title);
                                    $("#txtpVariableListVarlistid").val(value.varListID);
                                    if (value.other == 1)
                                    {
                                        $('#chkpVariableListVarlistOthers').prop('checked', true);
                                    }
                                    else
                                    {
                                        $('#chkpVariableListVarlistOthers').prop('checked', false);
                                    }
                                    $("#VariableListId").val(value.variableListId);
                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $("#pVariableListnodeid").val(nodeId);
                        $("#pVariableListtransportidpopup").val($scope.transportidpopupproperties);
                        $('#propertiesfrmVariableList').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Document scan')
                    {
                        //alert(nodeId);   
                        dataFactory.GetDocumetScanProperties(nodeId).then(function (resObj)
                        {
                            $scope.DocumetScan = resObj.data;
                            //alert(JSON.stringify(resObj.data));
                            angular.forEach(resObj.data, function (value, key)
                            {

                                if (value.questionRefId == nodeId)
                                {
                                    // alert(nodeId);
                                    $("#txtpDocumentScanTitle").val(value.title);
                                    $("#txtpDocumentScanName").val(value.documentName);
                                    $("#sltpDocumentScanPlanningFeedback").val(value.planningFeedbackLevel);
                                    $("#documentscanid").val(value.documetScanId);
                                }
                            });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                        $("#pDocumentScannodeid").val(nodeId);
                        $("#pDocumentScantransportidpopup").val($scope.transportidpopupproperties);
                        $('#propertiesfrmDocumentScan').removeClass('hide').addClass('show');
                    }
                    if ($scope.propertyType == 'Trailer')
                    {

                    }
                    if ($scope.propertyType == 'Transporttype')
                    {

                    }

                }

            });
            var treeObj = $("#divQuestiontreewithouttransport").data("ejTreeView");
            treeObj.expandAll();
        }, 500);
    }
}




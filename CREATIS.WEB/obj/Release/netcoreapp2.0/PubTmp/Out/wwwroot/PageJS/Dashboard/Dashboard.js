﻿var workingcode = "";
var Planning = "";
var PlanningOverview = "";
var Specific = "";
var Pto = "";
var Interby = "";
var initialworkingcode = [];
var initialPlanning = [];
var initialPlanningOverview = [];
var initialSpecific = [];
var initialPto = [];
var initialInterby = [];
var LayoutID = "";
var oldpTMData = "";
var InsertPTMData = "";
var DeletePTMData = "";
var InsertPTSData = "";
var DeletePTSData = "";
var updateobc = 0;
var countoftransporttype = [];
CREATIS.controller('DashboardController', ['$scope', '$http', 'dataFactory', '$uibModal', '$location', '$rootScope', function ($scope, $http, dataFactory, $uibModal, $location, $rootScope) {
			var absUrl = $location.absUrl();
			urldata = absUrl.toString();
			$scope.LayoutID = urldata.split('=')[1];
			LayoutID = $scope.LayoutID;
			$scope.LayoutTitle = "";
			$scope.OBCTypeMaster = "";
			$scope.OBCTypeAssignedValues = "";
            //$scope.UsedLanguages = "";
            $rootScope.UsedLanguages = [];
            $rootScope.TransportTypes = "";
			$scope.Language = "";
			$scope.PlanningTypeMaster = "";
			$scope.Allowancestype = "";
			$scope.PlanningSpecifics = "";
			$scope.obcvalues = 0;
			$scope.obctypeids = 0;
            $scope.AssignedPlanningType = "";
            $scope.Customerid;
            dataFactory.PanelTitle($scope.LayoutID).then(function (resObj)
            {
                var layoutdetails   = resObj.data.split('[');
                $scope.LayoutTitle = layoutdetails[0];
                $scope.Customerid = layoutdetails[1];
			}, function () {
				$scope.error = "Some Error.";
			});
            $scope.Redirecttocustomer = function ()
            {
                dataFactory.RedirectToCustomerOverview($scope.Customerid);
            }
            $scope.RedirecttoScreenLayout = function ()
            {
                dataFactory.RedirecttoScreenLayout($scope.LayoutID);
            }

            $scope.RedirecttoShowLayout = function ()
            {
                dataFactory.RedirecttoShowLayout($scope.LayoutID);
            }

            $scope.RedirecttoTranslation = function ()
            {
                dataFactory.RedirecttoTranslation($scope.LayoutID);
            }
			GetGetOBCTypeMaster($scope, dataFactory);
            //GetAssignedOBCTypes($scope, dataFactory);
            GetAssignedTransportTypes($scope, dataFactory, $scope.LayoutID, $rootScope);
            
			

			dataFactory.GetLanguages().then(function (resObj) {
                $scope.Language = resObj.data;
                GetUsedLanguage($scope, dataFactory, $scope.LayoutID, $rootScope);
			}, function () {
				$scope.error = "Some Error.";
			});

			dataFactory.GetPlanningType($scope.LayoutID).then(function (resObj) {
				$scope.AssignedPlanningType = resObj.data;
				InsertPTMData = [];
				if (resObj.data.length !== 0) {
                    setTimeout(function ()
                    {
                        angular.forEach(resObj.data, function (value, key)
                        {
						InsertPTMData.push(value.planningTypeRefId);
					});
            
					$(':radio:not(:checked)').attr('disabled', true);
                    GetPlanningSpecificationMaster($scope, dataFactory, InsertPTMData, $scope.LayoutID);

                }, 500);

				} else {
					$(':radio:not(:checked)').attr('disabled', false);
				}
			}, function () {
				$scope.error = "Some Error.";
			});

			dataFactory.GetGetPlanningTypeMaster().then(function (resObj) {
				$scope.PlanningTypeMaster = resObj.data;
				if (resObj.data.length !== 0) {}

			}, function () {
				$scope.error = "Some Error.";
			});

            $scope.SaveOBCTypes = function ()
            {
               // alert("first");
               // alert(updateobc);
                //GetAssignedOBCTypes($scope, dataFactory);
                if (updateobc !== 0) {
                    dataFactory.UpdateOBCTypes($scope.obctypeids, $scope.OBCMvalue, $scope.LayoutID).then(function (resObj)
                    {
                            //GetAssignedOBCTypes($scope, dataFactory);
					}, function () {
						$scope.error = "Some Error.";
					});
				} else {
                    dataFactory.SaveOBCType($scope.LayoutID, $scope.OBCMvalue).then(function (resObj)
                    {
                            //GetAssignedOBCTypes($scope, dataFactory);
					}, function () {
						$scope.error = "Some Error.";
					});
				}
				//GetAllowancesMaster($scope, dataFactory,$scope.LayoutID);
                setTimeout(function ()
                {
                    //alert("second");
                    gridLoadFunction();
                }, 500);
            };
            var gridLoadFunction = function ()
            {
                
                GetAssignedOBCTypes($scope, dataFactory);

            }
            $scope.GetPlanningSpec = function (event)
            {
                
                if ($("input[name='PlanningType_1']").is(":checked"))
                {
                    
                    $("input[name='PlanningType_3']").prop("disabled", true);
                    $("input[name='PlanningType_4']").prop("disabled", true);
                    $("input[name='PlanningType_6']").prop("disabled", true);
                    $("input[name='PlanningType_7']").prop("disabled", true);
                } else
                {
                    $("input[name='PlanningType_3']").prop("disabled", false);
                    $("input[name='PlanningType_4']").prop("disabled", false);
                    $("input[name='PlanningType_6']").prop("disabled", true);
                    $("input[name='PlanningType_7']").prop("disabled", true);
                }

                if ($("input[name='PlanningType_3']").is(":checked"))
                {
                    $("input[name='PlanningType_4']:checkbox").prop('checked', true);
                    $("input[name='PlanningType_1']").prop("disabled", true);
                } 

                if ($("input[name='PlanningType_4']").is(":checked"))
                {
                    $("input[name='PlanningType_1']").prop("disabled", true);
                    $("input[name='PlanningType_6']").prop("disabled", false);
                    $("input[name='PlanningType_7']").prop("disabled", false);
                } else
                {
                    $("input[name='PlanningType_6']").prop("disabled", true);
                    $("input[name='PlanningType_7']").prop("disabled", true);
                }

                if (!$("input[name='PlanningType_3']").is(":checked") && !$("input[name='PlanningType_4']").is(":checked") && !$("input[name='PlanningType_6']").is(":checked") && !$("input[name='PlanningType_7']").is(":checked"))
                {
                    $("input[name='PlanningType_1']").prop("disabled", false);
                } else
                {
                    $("input[name='PlanningType_1']").prop("disabled", true);
                }

				InsertPTMData = [];
				DeletePTMData = [];
				$('#divPlan input:checked').each(function () {
					InsertPTMData.push(this.value);
				});
				$('#divPlan input:unchecked').each(function () {
					DeletePTMData.push(this.value);
				});

				
                if (DeletePTMData.length > 0)
                {
                    dataFactory.DeletPlanningType($scope.LayoutID, DeletePTMData).then(function (resObj) { }, function ()
                    {
                        $scope.error = "Some Error.";
                    });
                }
                
                //$scope.AssignedPlanningType
                if (InsertPTMData.length>0)
                {
                    dataFactory.SavePlanningType($scope.LayoutID, InsertPTMData).then(function (resObj) { }, function ()
                    {
                        $scope.error = "Some Error.";
                    });
					GetPlanningSpecificationMaster($scope, dataFactory, InsertPTMData);
					$(':radio:not(:checked)').attr('disabled', true);

				} else {

					$(':radio:not(:checked)').attr('disabled', false);
				}
				$scope.PlanningSpecifics = "";

			};

			$scope.GetAllowancetype = function (event) {
				var AllowanceData = "";
                var DeleteAllowanceData="";
				AllowanceData = [];
				$('#divAllow input:checked').each(function () {
					AllowanceData.push(this.value);
				});
				if (AllowanceData !== "") {
					$(':radio:not(:checked)').attr('disabled', true);
				} else {
					
					$(':radio:not(:checked)').attr('disabled', false);
				}
				DeleteAllowanceData = [];
				$('#divAllow input:unchecked').each(function () {
					DeleteAllowanceData.push(this.value);
				});

				dataFactory.SaveAllowances($scope.LayoutID, AllowanceData).then(function (resObj) {}, function () {
					$scope.error = "Some Error.";
				});

				dataFactory.DeletAllowances($scope.LayoutID, DeleteAllowanceData).then(function (resObj) {}, function () {
					$scope.error = "Some Error.";
				});
			};

            $scope.GetPlanningSpecsub = function (event)
            {
                if ($("input[name='" + event.target.name + "']").is(":checked"))
                {
                    $("input[name='PlanningType_" + event.target.id + "']").prop("disabled", true);
                } else
                {
                    $("input[name='PlanningType_" + event.target.id + "']").prop("disabled", false);

                }
                setTimeout(function ()
                {
                    //if ($("#" + event.target.id).is(":checked"))
                    

                    InsertPTSData = [];
                    DeletePTSData = [];
                    $('#divPlanSpec input:checked').each(function ()
                    {
                        InsertPTSData.push(this.value);
                    });
                    $('#divPlanSpec input:unchecked').each(function ()
                    {
                        DeletePTSData.push(this.value);
                    });
                    if (InsertPTSData.length > 0)
                    {
                        dataFactory.SavePlanningSpecification($scope.LayoutID, InsertPTSData).then(function (resObj)
                        {
                            setTimeout(function ()
                            {
                                GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
                            }, 100);
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
                    }
                    if (DeletePTSData.length > 0)
                    {
                        dataFactory.DeletPlanningSpecification($scope.LayoutID, DeletePTSData).then(function (resObj)
                        {
                            setTimeout(function ()
                            {
                                GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
                            }, 100);
                        }, function ()
                        {
                            $scope.error = "Some Error.";
                        });
                    }
                }, 200);
			};

			$scope.AddNewLanguage = function () {
				var modalInstance = $uibModal.open({
						templateUrl: 'AddLanguage.html',
						controller: 'LanguageCtl',
                        scope: $scope,
                        rootscope: $rootScope,
						backdrop: false
					});
				modalInstance.result.then(function (obj) {}, function (obj) {});
			};

			$scope.AddTransportTypes = function () {
				var modalInstance = $uibModal.open({
						templateUrl: 'AddTransPortType.html',
						controller: 'TransPortTypeCtl',
						scope: $scope,
						backdrop: false
					});
				modalInstance.result.then(function (obj) {
                
                }, function (obj) {});
			};

    //////////

            $scope.CopyLayoutPopup = function ()
            {
                var modalInstance = $uibModal.open({
                    templateUrl: 'CopyLayout.html',
                    controller: 'CopyLayoutCtl',
                    scope: $scope,
                    rootscope: $rootScope,
                    backdrop: false
                });

                modalInstance.result.then(function (obj) { }, function (obj) { });

                dataFactory.GetCustomer().then(function (resObj)
                {
                    CopyCustomerDropDown(resObj.data);
                }, function ()
                {
                    $scope.error = "Some Error.";
                });

                dataFactory.GetUsedLanguage($scope.LayoutID).then(function (resObj)
                {
                    //alert(JSON.stringify(resObj.data));
                    LanguagesDropDown(resObj.data);
                }, function ()
                {
                    $scope.error = "Some Error.";
                    });

                dataFactory.GetLayoutfordrop().then(function (resObj)
                {
                    existingLayout(resObj.data);
                }, function ()
                    {
                        $scope.error = "Some Error.";
                    });
            };

            function existingLayout(layouts)
            {

                $('#sltexistingLayout').ejDropDownList({
                    dataSource: layouts,
                    watermarkText: "Select Layout",
                    fields: { text: "layoutName", value: 'layoutId' },
                    width: 260,
                    showCheckbox: false,
                    enableFilterSearch: true,
                    enablePopupResize: true
                });

                //alert('entering');
                dropDownObj = $("#sltexistingLayout").data("ejDropDownList");
                dropDownObj.setSelectedValue($scope.LayoutID); // Set value in drodownlist based on 'value' field.
                dropDownObj.disable();

               

                $('#dpCopyLanguages_popup_list_wrapper').addClass("syncdropdown");
            }



            function LanguagesDropDown(language)
            {

                $('#dpCopyLanguages').ejDropDownList({
                    dataSource: language,
                    watermarkText: "Select Language",
                    fields: { text: "language", value: 'languageRefId' },
                    width: 260,
                    showCheckbox: true,
                    enableFilterSearch: true,
                    enablePopupResize: true
                });

                $('#dpCopyLanguages_popup_list_wrapper').addClass("syncdropdown");
            }

            function CopyCustomerDropDown(customers)
            {

                $('#CopyCustomer').ejDropDownList({
                    dataSource: customers,
                    watermarkText: "Select Customer",
                    fields: { text: "cust", value: 'customerId' },
                    width: 260,
                    showCheckbox: false,
                    select: function (sender)
                    {
                        customerId = sender.value;
                    },
                    enableFilterSearch: true,
                    enablePopupResize: true
                });

                $('#CopyCustomer_popup_list_wrapper').addClass("syncdropdown");
            }



    ///////////////
			$scope.AddNewAllowance = function () {
				var modalInstance = $uibModal.open({
						templateUrl: 'Allowance.html',
						controller: 'AllowanceCtl',
						scope: $scope,
						backdrop: false
					});
				modalInstance.result.then(function (obj) {}, function (obj) {});
			};

			dataFactory.GetWorkingCode().then(function (resObj) {

				workingcode = resObj.data;
				for (i = 0; i < workingcode.length; i++) {
					var Workingco = {
						"text": workingcode[i].workingCodeDescription,
						"value": workingcode[i].workingCodeDescription,
						"id": workingcode[i].workingCodeId
					};
					initialworkingcode.push(Workingco);
				}
			}, function () {
				$scope.error = "Some Error.";
			});
			dataFactory.GetPlanning().then(function (resObj) {

				Planning = resObj.data;
				for (i = 0; i < Planning.length; i++) {
					var Plannin = {
						"text": Planning[i].defaultActivitiesRegistrationDescription,
						"value": Planning[i].defaultActivitiesRegistrationDescription,
						"id": Planning[i].planningId
					};
					initialPlanning.push(Plannin);
				}
			}, function () {
				$scope.error = "Some Error.";
			});
			dataFactory.GetPlanningOverview().then(function (resObj) {

				PlanningOverview = resObj.data;
				for (i = 0; i < PlanningOverview.length; i++) {
					var PlanningOver = {
						"text": PlanningOverview[i].planningOverviewDescription,
						"value": PlanningOverview[i].planningOverviewDescription,
						"id": PlanningOverview[i].planningOverviewId
					};
					initialPlanningOverview.push(PlanningOver);
				}
			}, function () {
				$scope.error = "Some Error.";
			});

			dataFactory.GetSpecific().then(function (resObj) {

				Specific = resObj.data;
				for (i = 0; i < Specific.length; i++) {
					var Specifi = {
						"text": Specific[i].specificDescription,
						"value": Specific[i].specificDescription,
						"id": Specific[i].specificId
					};
					initialSpecific.push(Specifi);
				}
			}, function () {
				$scope.error = "Some Error.";
			});
			dataFactory.GetPTO().then(function (resObj) {

				Pto = resObj.data;
				for (i = 0; i < Pto.length; i++) {
					var Pt = {
						"text": Pto[i].ptoDescription,
						"value": Pto[i].ptoDescription,
						"id": Pto[i].ptoId
					};
					initialPto.push(Pt);
				}
			}, function () {
				$scope.error = "Some Error.";
			});

			dataFactory.GetInterruptibleBy().then(function (resObj) {

				Interby = resObj.data;
				for (i = 0; i < Interby.length; i++) {
					var Interb = {
						"text": Interby[i].interruptibleByDescription,
						"value": Interby[i].interruptibleByDescription,
						"id": Interby[i].interruptibleById
					};
					initialInterby.push(Interb);
				}
			}, function () {
				$scope.error = "Some Error.";
			});

			//GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
            $scope.GoHome = function ()
            {
                dataFactory.GoHomePage();
            };

            function recallgrid()
            {
                GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
            }

}
    
	]);




CREATIS.controller('CopyLayoutCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory', '$rootScope',
    function ($scope, $http, $uibModalInstance, dataFactory, $rootScope)
    {
        // Copy Layout By Selvam

        $scope.cancel = function ()
        {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.submitCopyLayout = function ()
        {

            var LayoutId = $("#sltexistingLayout").val();
            var NewLayoutName = $("#txtNewLayout").val();
            var CustomerId = $("#CopyCustomer").val();
            var LanguageIds = $("#dpCopyLanguages").val()

            dataFactory.SaveCopyLayout(LayoutId, NewLayoutName, CustomerId, LanguageIds).then(function (resObj)
            {
                //if (response.data.error === undefined)
               // {
                    validationTrue("Copy Layout Saved Successfully.");
                //}
                //else
                  //  validationFalse(response.data.error);

                $uibModalInstance.dismiss('cancel');
            }, function ()
            {
                validationFalse("Request error.");
                $scope.error = "Some Error.";
            });
        };

         // Copy Layout By Selvam
    }
]);

CREATIS.controller('LanguageCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory','$rootScope',
    function ($scope, $http, $uibModalInstance, dataFactory, $rootScope) {

			$scope.submitLanguage = function () {
				var LanguageId = $scope.Languagevalu;
				var LayoutId = $scope.LayoutID;
                dataFactory.DashboardSaveUsedLanguage(LayoutId, LanguageId).then(function (resObj) {
					//$scope.UsedLanguages = resObj.data;
                    //dataFactory.GetUsedLanguage(LayoutId).then(function (resObj)
                    //{
                    //    //$rootscope.usedLanguages = resObj.data;
                    //    debugger;
                    //    $scope.UsedLanguages = resObj.data;
                    //});
                    GetUsedLanguage($scope, dataFactory, LayoutId, $rootScope);
					$uibModalInstance.dismiss('cancel');

				}, function () {
					validationFalse("Request error.");
					$scope.error = "Some Error.";
				});

			};

			$scope.cancel = function () {

				$uibModalInstance.dismiss('cancel');

			};
        
		}

	]);

CREATIS.controller('TransPortTypeCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory','$rootScope',
    function ($scope, $http, $uibModalInstance, dataFactory, $rootScope) {
			$scope.submitTransPortType = function () {
                var TransportType = $scope.TransportType;

				var LayoutId = $scope.LayoutID;
				dataFactory.SaveTransportTypes(LayoutId, TransportType).then(function (resObj) {
if(resObj.data.length!==0){
setTimeout(function() {

    GetAssignedTransportTypes($scope, dataFactory, $scope.LayoutID, $rootScope);
$("#divtransp").html();
},300);
}
					//$scope.TransportTypes = resObj.data;
					//$scope.TransportTypes.push($scope.TransportType);

				}, function () {
					validationFalse("Request error.");
					$scope.error = "Some Error.";
				});
                
                $uibModalInstance.dismiss('cancel');
			};
			$scope.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};

		}

	]);

CREATIS.controller('AllowanceCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory',
		function ($scope, $http, $uibModalInstance, dataFactory) {
			$scope.submitAllowance = function () {
                var Allowances = $scope.AllowancesMast;
                var AllowanceCode = $scope.AllowanceCodes
				dataFactory.SaveAllowancesMaster($scope.OBCMvalue, Allowances,AllowanceCode).then(function (resObj) {
					dataFactory.GetAllowancesMaster($scope.OBCMvalue).then(function (resObj) {
						$scope.Allowancestype = resObj.data;
					}, function () {
						$scope.error = "Some Error.";
					});
					$uibModalInstance.dismiss('cancel');

				}, function () {
					validationFalse("Request error.");
					$scope.error = "Some Error.";
				});
			};
			$scope.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};
		}
	]);

function GetGetOBCTypeMaster($scope, dataFactory) {
	dataFactory.GetOBCTypeMaster().then(function (resObj) {
        $scope.OBCTypeMaster = resObj.data;
        setTimeout(function ()
        {
            GetAssignedOBCTypes($scope, dataFactory);
        }
            , 100);
	}, function () {
		$scope.error = "Some Error.";
	});

}

function GetAssignedOBCTypes($scope, dataFactory)
{

	dataFactory.GetAssignedOBCTypes($scope.LayoutID).then(function (resObj) {
		$scope.OBCTypeAssignedValues = resObj.data;
		if (resObj.data.length !== 0) {
			angular.forEach($scope.OBCTypeAssignedValues, function (value, key) {
                $scope.OBCMvalue = value.obcTypeRefId;
                updateobc = value.obcTypeRefId;
				$scope.obcvalues = value.obcTypeRefId;
                $scope.obctypeids = value.obcTypeId;
			});
            setTimeout(function() {
                $("#"+$scope.obcvalues).prop('checked', true);
                GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
            }
            ,300);
			GetAllowancesMaster($scope, dataFactory,$scope.LayoutID);
            
        } else
        {
            updateobc = 0;
			$scope.obcvalues = 0;
			$scope.obctypeids = 0;
		}
	}, function () {
		$scope.error = "Some Error.";
	});
        
}

function GetAllowancesMaster($scope, dataFactory, LayoutID) {
	dataFactory.GetAllowancesMaster($scope.OBCMvalue).then(function (resObj) {
		$scope.Allowancestype = resObj.data;
        GetAllowances($scope, dataFactory, LayoutID);
	}, function () {
		$scope.error = "Some Error.";
	});
}

function GetPlanningSpecificationMaster($scope, dataFactory, InsertPTMData, LayoutID) {
	dataFactory.GetPlanningSpecificationMaster(InsertPTMData).then(function (resObj) {
		$scope.PlanningSpecifics = resObj.data;
        GetPlanningSpecification($scope, dataFactory, $scope.LayoutID);
        angular.forEach(InsertPTMData, function (value, key)
        {
            $("input[name='PlanningType_" + value + "']:checkbox").prop('checked', true);
        });



	}, function () {
		$scope.error = "Some Error.";
	});
	if (InsertPTMData === "") {
		$(':radio:not(:checked)').attr('disabled', false);
	}
}

function GetPlanningSpecification($scope, dataFactory, Layoutid) {
    var readonly = "";
    $scope.PlanningSpecification = [];
    dataFactory.GetPlanningSpecification($scope.LayoutID).then(function (resObj) {
		$scope.PlanningSpecification = resObj.data;
        //if (resObj.data.length !== 0) {}
        //alert(JSON.stringify(resObj.data));
		    angular.forEach(resObj.data, function (value, key) {
                $("input[name='PlanningSpec_" + value.planningSpecificationRefId + "']:checkbox").prop('checked', true);

        });
            
            if ($("input[name='PlanningType_1']").is(":checked"))
            {

                $("input[name='PlanningType_3']").prop("disabled", true);
                $("input[name='PlanningType_4']").prop("disabled", true);
                $("input[name='PlanningType_6']").prop("disabled", true);
                $("input[name='PlanningType_7']").prop("disabled", true);
            } else
            {
                $("input[name='PlanningType_3']").prop("disabled", false);
                $("input[name='PlanningType_4']").prop("disabled", false);
                $("input[name='PlanningType_6']").prop("disabled", true);
                $("input[name='PlanningType_7']").prop("disabled", true);
            }

            if ($("input[name='PlanningType_3']").is(":checked"))
            {
                $("input[name='PlanningType_4']:checkbox").prop('checked', true);
                $("input[name='PlanningType_1']").prop("disabled", true);
            }

            if ($("input[name='PlanningType_4']").is(":checked"))
            {
                $("input[name='PlanningType_1']").prop("disabled", true);
                $("input[name='PlanningType_6']").prop("disabled", false);
                $("input[name='PlanningType_7']").prop("disabled", false);
            } else
            {
                $("input[name='PlanningType_6']").prop("disabled", true);
                $("input[name='PlanningType_7']").prop("disabled", true);
            }

            if (!$("input[name='PlanningType_3']").is(":checked") && !$("input[name='PlanningType_4']").is(":checked") && !$("input[name='PlanningType_6']").is(":checked") && !$("input[name='PlanningType_7']").is(":checked"))
            {
                $("input[name='PlanningType_1']").prop("disabled", false);
            } else
            {
                $("input[name='PlanningType_1']").prop("disabled", true);
            }

	}, function () {
		$scope.error = "Some Error.";
	});
}

function GetAllowances($scope, dataFactory, Layoutid) {
	dataFactory.GetAllowances(Layoutid).then(function (resObj) {
		$scope.Allow = resObj.data;
		angular.forEach(resObj.data, function (value, key) {
			$("input[name='Allowancestype_" + value.allowancesRefId + "']:checkbox").prop('checked', true);
		});
	}, function () {
		$scope.error = "Some Error.";
	});
}

function GetAssignedTransportTypes($scope, dataFactory, Layoutid, $rootScope)
{
    //alert(JSON.stringify($scope.TransportTypes));
    //$scope.TransportTypes =[];
    dataFactory.GetAssignedTransportTypes(Layoutid).then(function (resObj)
    {
        counttransporttype = resObj.data.length;
        $rootScope.TransportTypes = resObj.data;
      //  var transport = resObj.data;
      //  setTimeout(function ()
      //  {
      //      if (resObj.data.length !== 0)
      //      {
      //          //$scope.TransportTypes = transport;
      //          angular.forEach(resObj.data, function (value, key)
      //          {
      //          $scope.TransportTypes.push({ transportTypesDescription: value.transportTypesDescription });
      //          $scope.$apply();
      //          //    alert(value.transportTypesDescription);
      //          //    $scope.TransportTypes.push(value.transportTypesDescription);
      //          //    //= transport;
      //          });
                
      ////          console.log($scope.TransportTypes);
      //      }
            
            
      //  }, 200);
			}, function () {
				$scope.error = "Some Error.";
			});
}

function fnGetIndex(arr, key, value)
{
    for (var i = 0; i < arr.length; i++)
    {
        if (arr[i][key] == value) { return i; }
    }
    return -1;
} 

function GetUsedLanguage($scope, dataFactory, Layoutid, $rootScope) {
    dataFactory.GetUsedLanguage($scope.LayoutID).then(function (resObj)
    {
        //$rootScope.UsedLanguages = [];

        $scope.UsedLangu = resObj.data;
        angular.forEach($scope.Language, function (value1, key1)
        {
            
            angular.forEach(resObj.data, function (value2, key2)
            {
                //console.($scope.Language);
                if (value1.languageId === value2.languageRefId)
                {
                    var i = fnGetIndex($scope.Language, "languageId", value1.languageId);
                    $scope.Language.splice(i, 1);
                }
            });
        });
       
        $rootScope.UsedLanguages = $scope.UsedLangu;
        
        //console.log($rootScope.UsedLanguages);

            
        
        
			}, function () {
				$scope.error = "Some Error.";
       });
    
}

function GetActivitiesRegistrationgrid($scope, dataFactory, Layoutid)
{
    
dataFactory.GetActivitiesRegistration($scope.LayoutID).then(function (resObj) {
				setTimeout(function() {

                    LoadRegistrationActivities($scope, dataFactory,resObj.data);
                    //GetAssignedTransportTypes($scope, dataFactory, $scope.LayoutID);
 
},200);

			}, function () {
				$scope.error = "Some Error.";
			});
}


//divRegistrationActivitytransportType
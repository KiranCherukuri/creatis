﻿var customerId = "";
CREATIS.controller('HomeController', ['$scope', '$http', 'dataFactory', '$uibModal', '$window', function ($scope, $http, dataFactory, $uibModal, $window)
{
    $scope.customer = "";
    $scope.recentActivities = "";
    var target;
    
    $scope.GoHome = function () {
				dataFactory.GoHomePage();
			};

    dataFactory.GetCustomer().then(function (resObj) {
        $scope.customer = resObj.data;
        CustomerDropDown(resObj.data);
    }, function () {
        $scope.error = "Some Error.";
        });

   

    dataFactory.GetRecentActivities().then(function (resObj) {
        //$scope.RecentActivities = resObj.data;
        LoadRecentActivities(dataFactory,resObj.data)
    }, function () {
        $scope.error = "Some Error.";
    });

    



    $scope.CustomerOverview = function ()
    {
        if (customerId == undefined || customerId == "")
        {
            validationFalse("Please select Customer.");
        } else
        {
            //alert($scope.DropCust);
            dataFactory.RedirectToCustomerOverview(customerId);
        }
    };

    
    $(document).on("click", ".fa-trash-o", function ()
    {
        var layoutid = $(this).attr("id");
        var objj = $("#RecentActivities").ejGrid("instance");
        var gridData = objj.model.dataSource;
        var modalInstance = $uibModal.open({
            templateUrl: 'Delete.html',
            controller: 'DeleteCtl',
            scope: $scope,
            backdrop: false
        });
        angular.forEach(gridData, function (value, key)
        {
           if (value.layoutRefId == layoutid)
           {
               $scope.layoutidforDelete = layoutid;
               $scope.customerforDelete = value.source;
               $scope.layoutNameforDelete = value.name;
               
        //        if ($window.confirm("Are you sure want to delete?"))
        //        {
        //               
        //            };
        //        //dataFactory.GetRecentActivitiestoDashboard(layoutid);
            }
        });
    });
    function GotoDashboard(dataFactory,layoutid) { dataFactory.GetRecentActivitiestoDashboard(layoutid); }
}]);

CREATIS.controller('DeleteCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $http, $uibModalInstance, dataFactory)
    {
        $scope.deletelayout = function ()
        {
            dataFactory.DeleteLayout($scope.layoutidforDelete).then(function (resObj)
            {
                $uibModalInstance.dismiss('cancel');
                validationTrue("Layout Deleted Successfully!");
                dataFactory.GetRecentActivities().then(function (resObj)
                {
                    //$scope.RecentActivities = resObj.data;
                    LoadRecentActivities(dataFactory, resObj.data)
                }, function ()
                {
                    $scope.error = "Some Error.";
                });
                        }, function ()
                            {
                                $scope.error = "Some Error.";
                            });
        }

        $scope.cancel = function ()
        {

            $uibModalInstance.dismiss('cancel');

        };

    }

]);

function LoadRecentActivities(dataFactory, RecentActivitie)
{
    var gridheight;
    var x = screen.height;
    //alert(x);
    if (x == 1024)
    {
        gridheight = 480;
    } else

        if (x == 768)
        {
            gridheight = 260;
        }
        else
        { gridheight = 520; }
    $("#RecentActivities").ejGrid({
        dataSource: RecentActivitie,
        allowScrolling: true,
        isResponsive: true,
        allowResizeToFit: true,
        //querycellinfo: "queryCellInfo",
        allowPaging: true,
        pageSettings: { pageSize: 15 },
        scrollSettings: {
            height: gridheight
        }, toolbarSettings: {
            showToolbar: false,
            toolbarItems: [ej.Grid.ToolBarItems.Delete]
        }, editSettings: { allowDeleting: true, showDeleteConfirmDialog: true },
        allowTextWrap: true,
        columns: [
            { field: "source", headerText: "Customer" },
            { field: "environment", headerText: "Type" },
            { field: "name", headerText: "Name" },
            { field: "createdOn", headerText: "Created Date", format: "{0:dd/MM/yyyy}", type: "date" },
            { field: "status", headerText: "Status" },
            { field: "updatedOn", headerText: "Date Last Status", format: "{0:dd/MM/yyyy}", type: "date" },
            { headerText: "Delete", template: "#Delete", textAlign: "center" },
            {
                field: "layoutRefId",
                headerText: "PK",
                visible: false
            }

        ], recordDoubleClick: function (args)
        {

            dataFactory.GetRecentActivitiestoDashboard(args.data.layoutRefId);

        },
        querycellinfo: function (args)
        {
            if (args.data.status == "In Progress")
            {

            }
        },
        rowSelected: function (args)
        {
            if (args.data.status == "In Progress")
            {
                $("#RecentActivities_delete").removeClass("e-disable");
            }
            else
            {
                $("#RecentActivities_delete").addClass("e-disable");
            }
            var selectedrowindex = this.selectedRowsIndexes;  // get selected row indexes             
            var selectedrecords = this.getSelectedRecords();  // get selected records   
        },
        endDelete: function (args) 
        { 
            if (args.data.status == "In Progress")
            {
                dataFactory.DeleteLayout(args.data.layoutRefId).then(function (resObj)
                {
                    validationTrue("Layout Deleted Successfully!");
                    $("#RecentActivities_delete").addClass("e-disable");
                }, function ()
                {
                    $scope.error = "Some Error.";
                });

            }
            
        }


    });
    //$(".e-gridcontent").css('border-radius', '5px');
    $(".e-pagercontainer").css('display', 'none');
    $("#RecentActivities_delete").css('padding', '5px');
    $("#RecentActivities_delete").addClass("pull-right");
    $("#RecentActivities_delete").addClass("e-disable");
    $('.e-pagermsg').text('Last 15 Activities');
    $('.e-parentmsgbar').addClass("pull-left");
    //$('.e-content').css('width', 'auto');e-gridheader e-textover e-scrollcss
    $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
    $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');
}


function CustomerDropDown(customers)
{
    
        $('#Customer').ejDropDownList({
            dataSource: customers,
            watermarkText: "Select Customer",
            fields: { text: "cust", value: 'customerId' },
            width: 260,
            showCheckbox: false,
            select: function (sender)
            {
                customerId = sender.value;
                
            },
            enableFilterSearch: true,
            enablePopupResize: true
    });

        $('#Customer_popup_list_wrapper').addClass("syncdropdown");
        //$('#Customer_popup_list_wrapper').css('width', '260px !important');
        //$('.e-ddl-popup .e-box .e-widget .e-popup .e-resizable').css('height', '397px !important');
        //$('.e-ddl-popup .e-box .e-widget .e-popup .e-resizable').css('width', '260px !important');
}





﻿CREATIS.controller('ScreenLayoutController', ['$scope', '$http', 'dataFactory', '$uibModal', '$location', function ($scope, $http, dataFactory, $uibModal, $location)
{
    var absUrl = $location.absUrl();
    urldata = absUrl.toString();
    $scope.LayoutID = urldata.split('=')[1];

    
    //if ($("#Act2").has("li").length == 0)
    //{
    //    $("#Act2").html("Please drag your activities to this side and arrange them in the desired order.");
    //} 

    dataFactory.GetRegistration($scope.LayoutID).then(function (resObj)
    {
        $scope.Registration = resObj.data;
    }, function ()
    {
        $scope.error = "Some Error.";
    });

    dataFactory.GetActivities($scope.LayoutID).then(function (resObj)
    {
        //alert(JSON.stringify(resObj.data));
        $scope.Activities = resObj.data;
        
    }, function ()
        {
            $scope.error = "Some Error.";
        });

    dataFactory.GetRegistrationPosition($scope.LayoutID).then(function (resObj)
    {
        $scope.GetRegistrationPosition = resObj.data;
    }, function ()
        {
            $scope.error = "Some Error.";
        });

    dataFactory.GetActivitiesPosition($scope.LayoutID).then(function (resObj)
    {
        $scope.GetActivitiesPosition = resObj.data;
    }, function ()
        {
            $scope.error = "Some Error.";
        });

    $scope.Dashboard = function ()
    {
        dataFactory.GetRecentActivitiestoDashboard($scope.LayoutID);
    }
    $scope.AddActPosition = function ()
    {
        var id = [];
        $('#Act2 li').each(function ()
        {
            id.push($(this).attr('id'));
        });
        var Did = [];
        $('#Act1 li').each(function ()
        {
            Did.push($(this).attr('id'));
        });
        //alert(id);
        //alert(Did);
        dataFactory.UpdateActivitiesRegistrationPosition($scope.LayoutID, id).then(function (resObj)
        {
        }, function ()
            {
                $scope.error = "Some Error.";
            });

        dataFactory.UpdateDeActivitiesRegistrationPosition($scope.LayoutID, Did).then(function (resObj)
        {
        }, function ()
            {
                $scope.error = "Some Error.";
            });
        //setTimeout(function ()
        //{
        //    dataFactory.GetActivitiesPosition($scope.LayoutID).then(function (resObj)
        //    {
        //        $scope.GetActivitiesPosition = resObj.data;
        //    }, function ()
        //        {
        //            $scope.error = "Some Error.";
        //        });

        //    dataFactory.GetActivities($scope.LayoutID).then(function (resObj)
        //    {
        //        $scope.Activities = resObj.data;
        //    }, function ()
        //        {
        //            $scope.error = "Some Error.";
        //        });

           validationTrue("Position Assigned");
        //}, 1000);
        //alert('ok');
        //location.reload(); 
        ////alert(id);
    }

    $scope.AddRegPosition = function ()
    {
        var id = [];
        $('#Reg2 li').each(function ()
        {
            id.push($(this).attr('id'));
        });
        var Did = [];
        $('#Reg1 li').each(function ()
        {
            Did.push($(this).attr('id'));
        });
        dataFactory.UpdateActivitiesRegistrationPosition($scope.LayoutID, id).then(function (resObj)
        {

            //dataFactory.GetRegistrationPosition($scope.LayoutID).then(function (resObj)
            //{
            //    $scope.GetRegistrationPosition = resObj.data;
            //}, function ()
            //    {
            //        $scope.error = "Some Error.";
            //    });

           
            //$scope.Activities = resObj.data;
        }, function ()
            {
                $scope.error = "Some Error.";
            });

        dataFactory.UpdateDeActivitiesRegistrationPosition($scope.LayoutID, Did).then(function (resObj)
        {

            //dataFactory.GetRegistration($scope.LayoutID).then(function (resObj)
            //{
            //    $scope.Registration = resObj.data;
            //}, function ()
            //    {
            //        $scope.error = "Some Error.";
            //    });

            // validationTrue("Position Assigned");

            //$scope.Activities = resObj.data;
        }, function ()
            {
                $scope.error = "Some Error.";
            });
        //alert(id);
        validationTrue("Position Assigned");
    }

}]);
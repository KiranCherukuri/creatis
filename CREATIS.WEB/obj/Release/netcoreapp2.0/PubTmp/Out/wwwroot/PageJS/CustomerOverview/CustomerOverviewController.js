﻿CREATIS.controller('CustomerOverviewController', ['$scope', '$http', 'dataFactory', '$uibModal', '$location', '$window', function ($scope, $http, dataFactory, $uibModal, $location, $window)
{
    $scope.Language = "";
    var absUrl = $location.absUrl();
    urldata = absUrl.toString();
    $scope.customerId = urldata.split('=')[1];
    $scope.integrator = "";
    $scope.iId == "0";
    $scope.itegratordata = "";
    $scope.customerNote = "";
    $scope.cnId = "0";
    $scope.customerNotes = "";
    $scope.layoutDatas = "";
    $scope.duplicate = "";
    $scope.customername = "";
    $(".modal-content").on("show", function ()
    {
        $("body").addClass("modal-open");
    }).on("hidden", function ()
    {
        $("body").removeClass("modal-open")
    });
    $scope.GoCustomer = function ()
    {
        dataFactory.RedirectToCustomerOverview(urldata.split('=')[1]);
    }
    dataFactory.GetLayout(urldata.split('=')[1]).then(function (resObj)
    {
        $scope.layoutDatas = resObj.data;
        LoadCustomerlayout(dataFactory, resObj.data);
    }, function ()
    {
        $scope.error = "Some Error.";
    });
    dataFactory.GetCustomerName(urldata.split('=')[1]).then(function (resObj)
    {
        $scope.customername = resObj.data;
    }, function ()
    {
        $scope.error = "Some Error.";
    });

    $scope.GoHome = function ()
    {
        dataFactory.GoHomePage();
    };

    dataFactory.GetInfocolumn(urldata.split('=')[1]).then(function (resObj)
    {

        $scope.Info = resObj.data;
    }, function ()
    {
        $scope.error = "Some Error.";
    });

    $scope.InfoColumn = {
        data: 'Info',
        enableCellSelection: false,
        enableRowSelection: false,
        enableCellEditOnFocus: true,
        columnDefs: [{
            field: 'iC_ID',
            displayName: 'S.No',
            enableCellEdit: false,
            visible: false
        }, {
            field: 'icM_Column_Name',
            displayName: 'Column Name',
            enableCellEdit: true
        }, {
            field: 'customerRefId',
            displayName: 'Customer ID',
            enableCellEdit: false,
            visible: false
        }, {
            displayName: 'Actions',
            cellTemplate:
            '<div class="grid-action-cell" >' +
            '<a ng-click="deleteThisRow(row.entity.iC_ID,row.entity.customerRefId);"  ng-disabled="row.entity.customerRefId" >Clear</a></div>',
            enableCellEdit: false
        }

        ]
    };

    $scope.$on('ngGridEventEndCellEdit', function (evt)
    {
        //console.log(evt.targetScope.row.entity);  // the underlying data bound to the row
        // Detect changes and send entity to server
        //alert(evt.targetScope.row.entity.iC_ID);
        //alert(evt.targetScope.row.entity.customerRefId);
        if (evt.targetScope.row.entity.customerRefId == null)
        {
            dataFactory.SaveInfocolumn(evt.targetScope.row.entity.iC_ID, evt.targetScope.row.entity.icM_Column_Name, urldata.split('=')[1]).then(function (resObj)
            {
                validationTrue("Info Column Saved Successfully.");
                $scope.CallInfoColumn();
            }, function ()
            {
                $scope.error = "Some Error.";
            });

        } else
        {
            dataFactory.UpdateInfocolumn(evt.targetScope.row.entity.iC_ID, evt.targetScope.row.entity.icM_Column_Name, evt.targetScope.row.entity.customerRefId).then(function (resObj)
            {
                validationTrue("Info Column Saved Successfully.");
                $scope.CallInfoColumn();
            }, function ()
            {
                $scope.error = "Some Error.";
            });

            //, evt.targetScope.row.entity.icM_Column_Ref_No, evt.targetScope.row.entity.createdBy, evt.targetScope.row.entity.createdOn);
        }

    });

    $scope.deleteThisRow = function (iC_ID, customerRefId)
    {
        if (customerRefId != '')
        {
            dataFactory.DeleteInfocolumn(iC_ID).then(function (resObj)
            {
                validationTrue("Info Column cleared.");
                $scope.CallInfoColumn();
            }, function ()
            {
                $scope.error = "Some Error.";
            });

        }
        $scope.CallInfoColumn();
    }

    $scope.CallInfoColumn = function ()
    {
        dataFactory.GetInfocolumn(urldata.split('=')[1]).then(function (resObj)
        {

            $scope.Info = resObj.data;
        }, function ()
        {
            $scope.error = "Some Error.";
        });
    };

    $scope.Infocolumn = function ()
    {

        var modalInstance = $uibModal.open({
            templateUrl: 'Infocolumn.html',
            controller: 'InfocolumnCtl',
            scope: $scope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) { }, function (obj) { });
    };

    dataFactory.GetIntegrator(urldata.split('=')[1]).then(function (resObj)
    {
        $scope.integrator = resObj.data;
        if (resObj.data.length != 0)
        {
            var gridData = $scope.integrator;
            angular.forEach(gridData, function (value, key)
            {
                $scope.iId = value.i_ID;
                $scope.itegratordata = value.i_Details;
                //value.i_Details;

            });
        } else
        {
            $scope.iId = "0";
            $scope.itegratordata = "";
        }
    }, function ()
    {
        $scope.error = "Some Error.";
    });

    $scope.SaveIntegrator = function ()
    {
        if ($scope.itegratordata != "")
        {

            if (($scope.iId == "undefined") || ($scope.iId == "") || ($scope.iId == "0"))
            {
                dataFactory.SaveIntegrator($scope.itegratordata, urldata.split('=')[1]).then(function (resObj)
                {
                    $scope.integrator = resObj.data;
                    validationTrue("Integrator Saved Successfully.");
                    dataFactory.GetIntegrator(urldata.split('=')[1]).then(function (resObj)
                    {
                        $scope.integrator = resObj.data;
                        if (resObj.data.length != 0)
                        {
                            var gridData = $scope.integrator;
                            angular.forEach(gridData, function (value, key)
                            {
                                $scope.iId = value.i_ID;
                                $scope.itegratordata = value.i_Details;
                                //value.i_Details;

                            });
                        } else
                        {
                            $scope.iId = "0";
                            $scope.itegratordata = "";
                        }
                    }, function ()
                    {
                        $scope.error = "Some Error.";
                    });

                }, function ()
                {
                    $scope.error = "Some Error.";
                });
            } else
            {

                dataFactory.UpdateIntegrator($scope.iId, $scope.itegratordata).then(function (resObj)
                {
                    validationTrue("Integrator Saved Successfully.");
                    $scope.integrator = resObj.data;
                    dataFactory.GetIntegrator(urldata.split('=')[1]).then(function (resObj)
                    {
                        $scope.integrator = resObj.data;
                        if (resObj.data.length != 0)
                        {
                            var gridData = $scope.integrator;
                            angular.forEach(gridData, function (value, key)
                            {
                                $scope.iId = value.i_ID;
                                $scope.itegratordata = value.i_Details;
                                //value.i_Details;

                            });
                        } else
                        {
                            $scope.iId = "0";
                            $scope.itegratordata = "";
                        }
                    }, function ()
                    {
                        $scope.error = "Some Error.";
                    });
                }, function ()
                {
                    $scope.error = "Some Error.";
                });
            }
        } else
        {
            if ($scope.iId != "0")
            {
                dataFactory.DeleteIntegrator($scope.iId).then(function (resObj)
                {

                    $scope.integrator = resObj.data;
                    dataFactory.GetIntegrator(urldata.split('=')[1]).then(function (resObj)
                    {
                        $scope.integrator = resObj.data;
                        if (resObj.data.length != 0)
                        {
                            var gridData = $scope.integrator;
                            angular.forEach(gridData, function (value, key)
                            {
                                $scope.iId = value.i_ID;
                                $scope.itegratordata = value.i_Details;
                                //value.i_Details;

                            });
                        } else
                        {
                            $scope.iId = "0";
                            $scope.itegratordata = "";
                        }
                    }, function ()
                    {
                        $scope.error = "Some Error.";
                    });
                }, function ()
                {
                    $scope.error = "Some Error.";
                });
            }
        }
    };

    $scope.Itegrator = function ()
    {
        var modalInstance = $uibModal.open({
            templateUrl: 'Integrator.html',
            controller: 'IntegratorCtl',
            scope: $scope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) { }, function (obj) { });
    };

    $scope.CustomerNotespopup = function ()
    {
        var modalInstance = $uibModal.open({
            templateUrl: 'CustomerNotes.html',
            controller: 'CustomerNotesCtl',
            scope: $scope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) { }, function (obj) { });
    };
    dataFactory.GetCustomerNotes(urldata.split('=')[1]).then(function (resObj)
    {
        $scope.customerNote = resObj.data;
        if (resObj.data.length != 0)
        {
            var gridData = $scope.customerNote;
            angular.forEach(gridData, function (value, key)
            {
                $scope.cnId = value.cN_ID;
                $scope.customerNotes = value.cN_Details;

            });
        } else
        {
            $scope.cnId = "0";
            $scope.customerNotes = "";
        }
    }, function ()
    {
        $scope.error = "Some Error.";
    });

    $scope.SaveCustomerNotes = function ()
    {
        if ($scope.customerNotes != "")
        {

            if (($scope.cnId == "undefined") || ($scope.cnId == "") || ($scope.cnId == "0"))
            {
                dataFactory.SaveCustomerNotes($scope.customerNotes, urldata.split('=')[1]).then(function (resObj)
                {
                    validationTrue("Customer Notes Saved Successfully.");
                    $scope.customerNote = resObj.data;
                    dataFactory.GetCustomerNotes(urldata.split('=')[1]).then(function (resObj)
                    {
                        $scope.customerNote = resObj.data;
                        if (resObj.data.length != 0)
                        {
                            var gridData = $scope.customerNote;
                            angular.forEach(gridData, function (value, key)
                            {
                                $scope.cnId = value.cN_ID;
                                $scope.customerNotes = value.cN_Details;

                            });
                        } else
                        {
                            $scope.cnId = "0";
                            $scope.customerNotes = "";
                        }
                    }, function ()
                    {
                        $scope.error = "Some Error.";
                    });

                }, function ()
                {
                    $scope.error = "Some Error.";
                });
            } else
            {

                dataFactory.UpdateCustomerNotes($scope.cnId, $scope.customerNotes).then(function (resObj)
                {
                    validationTrue("Customer Notes Saved Successfully.");
                    $scope.customerNote = resObj.data;
                    dataFactory.GetCustomerNotes(urldata.split('=')[1]).then(function (resObj)
                    {
                        $scope.customerNote = resObj.data;
                        if (resObj.data.length != 0)
                        {
                            var gridData = $scope.customerNote;
                            angular.forEach(gridData, function (value, key)
                            {
                                $scope.cnId = value.cN_ID;
                                $scope.customerNotes = value.cN_Details;

                            });
                        } else
                        {
                            $scope.cnId = "0";
                            $scope.customerNotes = "";
                        }
                    }, function ()
                    {
                        $scope.error = "Some Error.";
                    });
                }, function ()
                {
                    $scope.error = "Some Error.";
                });
            }
        } else
        {
            if ($scope.cnId != "0")
            {
                dataFactory.DeleteCustomerNotes($scope.cnId).then(function (resObj)
                {
                    validationTrue("Info Column Saved Successfully.");
                    $scope.customerNote = resObj.data;
                    dataFactory.GetCustomerNotes(urldata.split('=')[1]).then(function (resObj)
                    {
                        $scope.customerNote = resObj.data;
                        if (resObj.data.length != 0)
                        {
                            var gridData = $scope.customerNote;
                            angular.forEach(gridData, function (value, key)
                            {
                                $scope.cnId = value.cN_ID;
                                $scope.customerNotes = value.cN_Details;

                            });
                        } else
                        {
                            $scope.cnId = "0";
                            $scope.customerNotes = "";
                        }
                    }, function ()
                    {
                        $scope.error = "Some Error.";
                    });
                }, function ()
                {
                    $scope.error = "Some Error.";
                });
            }
        }
    };
    $scope.Validateduplicatelayout = function ()
    {
        var gridData = $scope.layoutDatas;
        var i = 0;
        angular.forEach(gridData, function (value, key)
        {
            if (value.customerRefId == $scope.customerId && value.layoutName == $scope.LayoutName)
            {
                i = 1;

            }

        });
        if (i == 1)
        {
            $scope.duplicate = 'The entering Layout Name is duplicate';
        } else
        {
            $scope.duplicate = '';
        }
    }

    $scope.NewInstruction = function ()
    {
        var modalInstance = $uibModal.open({
            templateUrl: 'GoToInstructionset.html',
            controller: 'InstructionsetCtl',
            scope: $scope,
            backdrop: false
        });
        $("#txyLayoutName").focus();
        modalInstance.result.then(function (obj) { }, function (obj) { });
        
    };

    dataFactory.GetLanguages().then(function (resObj)
    {
        $scope.Language = resObj.data;
    }, function ()
    {
        $scope.error = "Some Error.";
    });

    $(document).on("click", ".fa-trash-o", function ()
    {
        var Layoutid = $(this).attr("id");
        var objj = $("#CustomerOverview").ejGrid("instance");
        var gridData = objj.model.dataSource;
        var modalInstance = $uibModal.open({
            templateUrl: 'Delete.html',
            controller: 'DeleteCtl',
            scope: $scope,
            backdrop: false
        });

        angular.forEach(gridData, function (value, key)
        {
            if (value.layoutId == Layoutid)
            {
                $scope.layoutidforDelete = value.layoutId;
                $scope.layoutNameforDelete = value.layoutName;
                //if ($window.confirm("Are you sure want to delete?"))
                //{
                //    dataFactory.DeleteLayout(Layoutid).then(function (resObj)
                //    {
                //        validationTrue("Layout Deleted Successfully!");
                //    }, function ()
                //    {
                //        $scope.error = "Some Error.";
                //    });
                //};
            }
        });
    });
}
]);


CREATIS.controller('DeleteCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $http, $uibModalInstance, dataFactory)
    {
        $scope.deletelayout = function ()
        {
            dataFactory.DeleteLayout($scope.layoutidforDelete).then(function (resObj)
            {
                $uibModalInstance.dismiss('cancel');
                validationTrue("Layout Deleted Successfully!");
                dataFactory.GetLayout(urldata.split('=')[1]).then(function (resObj)
                {
                    $scope.layoutDatas = resObj.data;
                    LoadCustomerlayout(dataFactory, resObj.data);
                }, function ()
                    {
                        $scope.error = "Some Error.";
                    });
            }, function ()
                {
                    $scope.error = "Some Error.";
                });
        }

        $scope.cancel = function ()
        {

            $uibModalInstance.dismiss('cancel');

        };

    }

]);

CREATIS.controller('InfocolumnCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $http, $uibModalInstance, dataFactory)
    {

        $scope.cancel = function ()
        {

            $uibModalInstance.dismiss('cancel');

        };

    }

]);

CREATIS.controller('IntegratorCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $http, $uibModalInstance, dataFactory)
    {

        $scope.cancel = function ()
        {

            $uibModalInstance.dismiss('cancel');

        };

    }

]);

CREATIS.controller('CustomerNotesCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $http, $uibModalInstance, dataFactory)
    {

        $scope.cancel = function ()
        {

            $uibModalInstance.dismiss('cancel');

        };

    }

]);

CREATIS.controller('InstructionsetCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $http, $uibModalInstance, dataFactory)
    {

        $scope.submitinstructionset = function ()
        {
            if ($scope.LayoutName.length > 2)
            {
                var LanguageId = $scope.DefaultLanguage;
                var LayoutName = $scope.LayoutName;
                var CustomerId = $scope.customerId;

                if (LanguageId == undefined)
                {
                    LanguageId = 66;
                }
                //alert(LanguageId);
                dataFactory.SaveLayoutDetails(LanguageId, LayoutName, CustomerId, 1).then(function (resObj)
                {
                    dataFactory.RedirectToDashboard(resObj.data);
                    //validationTrue("Release details saved successfully.");
                    $uibModalInstance.dismiss('cancel');

                }, function ()
                    {
                        validationFalse("Request error.");
                        $scope.error = "Some Error.";
                    });
            }
            else
            {
                validationFalse("Please give Layout name minimum 3 characters");
                $("#txyLayoutName").focus();
            }
        }

        $scope.cancel = function ()
        {

            $uibModalInstance.dismiss('cancel');

        };

    }

]);

function LoadCustomerlayout(dataFactory, layout)
{
    $("#CustomerOverview").ejGrid({
        dataSource: layout,
        allowScrolling: true,
        isResponsive: true,
        allowResizeToFit: true,
        allowPaging: true,
        pageSettings: {
            pageSize: 15
        },
        scrollSettings: {
            height: 235
        },
        toolbarSettings: {
            showToolbar: false,
            toolbarItems: [ej.Grid.ToolBarItems.Delete]
        },
        editSettings: {
            allowDeleting: true,
            showDeleteConfirmDialog: true
        },
        allowTextWrap: true,
        columns: [{
            field: "layoutType",
            headerText: "Type",
            width: 40
        }, {
            field: "layoutName",
            headerText: "Name",
            width: 40
        }, {
            field: "createdOn",
            headerText: "Created Date",
            format: "{0:MM/dd/yyyy}",
            type: "date",
            width: 40
        }, {
            field: "layoutStatus",
            headerText: "Status",
            width: 40
        }, {
            field: "updatedOn",
            headerText: "Date Last Status",
            format: "{0:MM/dd/yyyy}",
            type: "date",
            width: 40
        }, {
            field: "layoutOrigin",
            headerText: "Origin",
            width: 40
        }, {
            field: "layoutId",
            headerText: "PK",
            visible: false
        }, {
            template: true,
            templateID: "#Delete",
            headerText: "Action",
            width: 40,
            textAlign: "center"
        }

        ],
        recordDoubleClick: function (args)
        {

            dataFactory.GetRecentActivitiestoDashboard(args.data.layoutId);

        },
        rowSelected: function (args)
        {
            if (args.data.layoutStatus == "In Progress")
            {
                $("#CustomerOverview_delete").removeClass("e-disable");
            } else
            {
                $("#CustomerOverview_delete").addClass("e-disable");
            }
            var selectedrowindex = this.selectedRowsIndexes; // get selected row indexes
            var selectedrecords = this.getSelectedRecords(); // get selected records
        },
        endDelete: function (args)
        {
            if (args.data.layoutStatus == "In Progress")
            {
                dataFactory.DeleteLayout(args.data.layoutId).then(function (resObj)
                {
                    validationTrue("Layout Deleted Successfully!");
                    $("#CustomerOverview_delete").addClass("e-disable");
                }, function ()
                {
                    $scope.error = "Some Error.";
                });

            }

        }
    });
    $(".e-gridcontent").css('border-radius', '5px');
    $("#CustomerOverview_delete").css('padding', '5px');
    $("#CustomerOverview_delete").addClass("pull-right");
    $("#CustomerOverview_delete").addClass("e-disable");
}
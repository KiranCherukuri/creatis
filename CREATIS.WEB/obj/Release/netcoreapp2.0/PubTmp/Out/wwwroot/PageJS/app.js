﻿var CreatisService = angular.module('CreatisService', []);

var CREATIS = angular.module('CREATIS', ['ui.bootstrap', 'ngGrid','CreatisService']);

CREATIS.filter("mydate", function () {
    var re = /\/Date\(([0-9]*)\)\//;
    return function (x) {
        var m = x.match(re);
        if (m) return new Date(parseInt(m[1]));
        else return null;
    };
});

var validationTrue = function (toastrMsg) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": 5000,
        "extendedTimeOut": 4000,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    Command: toastr["success"]("<p>" + toastrMsg + "</p>")
};

var validationFalse = function (toastrMsg) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": 5000,
        "extendedTimeOut": 4000,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    Command: toastr["error"]("<p>" + toastrMsg + "</p>")
};

function showLoader() {
    var offset = $('body').offset();
    var height = $('body').height();
    var width = $('body').width();
    $("#loading").addClass("loading-visible").removeClass("loading-invisible");
    $('#fountainG').find("div").addClass('fountainG');
    $("#loading").css({ "height": "" + height + "px", "width": "" + width + "px" })
    $("#loading").css({ "position": "absolute", "top": "" + offset.top + "px", "left": "" + offset.left + "px", "z-index": "1060" });
}
function hideLoader() {
    $("#loading").addClass("loading-invisible").removeClass("loading-visible");
    $('#fountainG').find("div").removeClass('fountainG');
}



$(document).ready(function () {

    var myDate = new Date();
    var hrs = myDate.getHours();
    var greet;
    if (hrs < 12)
        greet = 'Good Morning';
    else if (hrs >= 12 && hrs <= 17)
        greet = 'Good Afternoon';
    else if (hrs >= 17 && hrs <= 24)
        greet = 'Good Evening';
    $("#lblGreetings").text(greet);
});
﻿CREATIS.controller('TranslationController', ['$scope', '$http', 'dataFactory', '$uibModal', '$location', '$rootScope', function ($scope, $http, dataFactory, $uibModal, $location, $rootScope) {
    var absUrl = $location.absUrl();
    urldata = absUrl.toString();
    $scope.LayoutID = urldata.split('=')[1];

    $scope.GetRecentActivitiestoDashboard = function ()
    {
        dataFactory.GetRecentActivitiestoDashboard($scope.LayoutID);
    }
    
    dataFactory.GetTranslation($scope.LayoutID).then(function (response) {
        $scope.translators = response.data;
       }, function () {
        $scope.error = "Some Error";
    });
    $scope.translatoractivities = function () {
        dataFactory.GetTranslation($scope.LayoutID).then(function (response) {
            $scope.translators = response.data;
        }, function () {
            $scope.error = "Some Error";
        });
    }
    $scope.translatorReg = function () {

        dataFactory.GetRegistrationTranslation($scope.LayoutID).then(function (response) {
            $scope.translators = response.data;
        }, function () {
            $scope.error = "Some Error";
        });
    }
    //For Info Column
    
    $scope.translatorInfoColum = function () {

        dataFactory.GetTranslatorInfoColumn($scope.LayoutID).then(function (response) {
            $scope.translators = response.data;
        }, function () {
            $scope.error = "Some Error";
        });
    }

    //Allowance

    $scope.translatorAllowanceColumn = function () {

        dataFactory.GetTranslatorAllowanceColumn($scope.LayoutID).then(function (response) {
            $scope.translators = response.data;
        }, function () {
            $scope.error = "Some Error";
        });
    }
    
    $scope.openpopup = function (ActivityId, header) {
        dataFactory.addtranslator(ActivityId, header).then(function success(response) {
            $scope.translator = response.data;
            $scope.language = header;
            $('.divtranslator').removeClass('show').addClass('hide');
            $('#TranslatorModal').modal();
            $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
            $('#frmtranslator').removeClass('hide').addClass('show');

        }, function () {
            $scope.error = "Some Error.";
        });

    }
    $scope.openInfopopup = function (InfoId, header) {
        dataFactory.addInfoPopup(InfoId, header).then(function success(response) {
            $scope.translator = response.data;
            $scope.language = header;
            $('.divtranslator').removeClass('show').addClass('hide');
            $('#TranslatorModal').modal();
            $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
            $('#frmInfoColumns').removeClass('hide').addClass('show');

        }, function () {
            $scope.error = "Some Error.";
        });

    }

    $scope.openAllowancepopup = function (AllowanceId, header) {
        dataFactory.addAllowancePopup(AllowanceId, header).then(function success(response) {
            $scope.translator = response.data;
            $scope.language = header;
            $('.divtranslator').removeClass('show').addClass('hide');
            $('#TranslatorModal').modal();
            $(".popupheader").html('<h4 class="pull- left"><b>' + header + '</b></h4>');
            $('#frmAllowance').removeClass('hide').addClass('show');

        }, function () {
            $scope.error = "Some Error.";
        });

    }
    

    $scope.saveTranslator = function (translator) {
        var obj = {
            "activityId": translator.activityId, "language": translator.language, "selectedLanguage": translator.selectedLanguage
        }
        var paramdata = JSON.stringify(obj);

        dataFactory.saveTranslator(paramdata).then(function success(response) {
            validationTrue("Saved Successfully...")
            $scope.translators = response.data;
              closepopup();

        }, function () {
            $scope.error = "Some Error.";
        });

    }
    $scope.savelanguageInfo = function (translator) {
        var obj = {
            "infoColumnId": translator.infoColumnId, "language": translator.language, "selectedLanguage": translator.selectedLanguage
        }
        var paramdata = JSON.stringify(obj);
        dataFactory.saveLanguageInfo(paramdata).then(function success(response) {
            validationTrue("Saved Successfully...")
            $scope.translators = response.data;
            closepopup();

        }, function () {
            $scope.error = "Some Error.";
        });

    }
    $scope.savelanguageAllowance = function (translator) {

        var obj = {
            "allowanceId": translator.allowanceId, "language": translator.language, "selectedLanguage": translator.selectedLanguage
        }
        var paramdata = JSON.stringify(obj);

        dataFactory.saveLanguageAllowance(paramdata).then(function success(response) {
            validationTrue("Saved Successfully...")
            $scope.translators = response.data;
            closepopup();

        }, function () {
            $scope.error = "Some Error.";
        });

    }
    $scope.saveUsedLanguage = function (languageSelect) {

        var obj = { "layoutRefId": languageSelect.layoutRefId, "languageRefId": languageSelect.languageSelectedId }
      
        var paramdata = JSON.stringify(obj);

        dataFactory.saveUsedLanguages(paramdata).then(function success(response)
        {
            //alert("sdfcsc");
            validationTrue("Saved Successfully...")
            $scope.usedLanuages = response.data;
            $(".popupheader").empty();
            $('.divlanguage').removeClass('show').addClass('hide');
            $('#LanguageSelectModal').modal('hide');
            

        }, function (response)
        {
            //alert(response.data);
            $scope.error = "Some Error.";
        });


    }
    

    var closepopup = function () {
        $(".popupheader").empty();
        $('.divtranslator').removeClass('show').addClass('hide');
        $('#TranslatorModal').modal('hide');
    }


    $scope.closePopUp = function () {
        closepopup();
    }


    $scope.AddNewLanguage = function () {

        dataFactory.addlanguage($scope.LayoutID).then(function success(response) {
            $scope.languageSelect = response.data;
     
            $('.divlanguage').removeClass('show').addClass('hide');
            $('#LanguageSelectModal').modal();
            $(".popupheader").html('<h4 class="pull- left"><b> Add Language </b></h4>');
            $('#frmLanguage').removeClass('hide').addClass('show');

        }, function () {
            $scope.error = "Some Error.";
        });
    };
}]);




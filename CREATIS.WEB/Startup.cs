using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Text;
using Microsoft.SqlServer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using CREATIS.Infrastructure.Repository;
using CREATIS.Infrastructure;
using CREATIS.ApplicationCore;
using CREATIS.Infrastructure.IRepository;

namespace CREATIS.WEB
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSession();
            services.AddMvc();
            services.AddDbContext<Creatis_Context>(options => options.UseSqlServer(Configuration.GetConnectionString("DB_Context")));
            services.Configure<ApplicationCore.BusinessObjects.EmailConfiguration>(Configuration.GetSection("EmailSettings"));
            services.Configure<ApplicationCore.BusinessObjects.QuestionPathTemplate>(Configuration.GetSection("QuestionPathTemplate"));
            services.Configure<ApplicationCore.BusinessObjects.UserAuthentication>(Configuration.GetSection("UserAuthentication"));
            services.Configure<ApplicationCore.BusinessObjects.ConnectionStrings>(Configuration.GetSection("ConnectionStrings"));
            services.Configure<ApplicationCore.BusinessObjects.ServiceSettings>(Configuration.GetSection("ServiceSettings"));
            services.Configure<ApplicationCore.BusinessObjects.OthersSettings>(Configuration.GetSection("OthersSettings"));


            services.AddScoped(typeof(ILoginRepository), typeof(LoginRepository));
            services.AddScoped(typeof(IHomeRepository), typeof(HomeRepository));
            services.AddScoped(typeof(ICustomerOverviewRepository), typeof(CustomerOverviewRepository));
            services.AddScoped(typeof(IDashboardRepository), typeof(DashboardRepository));
            services.AddScoped(typeof(IQuestionPathRepository), typeof(QuestionPathRepository));
            services.AddScoped(typeof(IScreenLayoutRepository), typeof(ScreenLayoutRepository));
            services.AddScoped(typeof(ITranslatorRepository), typeof(TranslatorRepository));
            services.AddScoped(typeof(ICopyLayoutRepository), typeof(CopyLayoutRepository));
            services.AddScoped(typeof(IShowLayoutRepository), typeof(ShowLayoutRepository));
            services.AddScoped(typeof(IEmailRepository), typeof(EmailRepository));
            services.AddScoped(typeof(ILanguageMasterRepository), typeof(LanguageMasterRepository));
            services.AddScoped(typeof(ICustomerRepository), typeof(CustomerRepository));
            services.AddScoped(typeof(IInputMasterRepository), typeof(InputMasterRepository));
            services.AddScoped(typeof(IDocumentTypeMasterRepository), typeof(DocumentTypeMasterRepository));
            services.AddScoped(typeof(IInfoMessageIconTypeRepository), typeof(InfoMessageIconTypeRepository));
            services.AddScoped(typeof(IPlanningAndPartnerCodeOverviewRepository), typeof(PlanningAndPartnerCodeOverviewRepository));
            services.AddScoped(typeof(ISavedValuesOverviewRepository), typeof(SavedValuesOverviewRepository));
            services.AddScoped(typeof(ITooltipRepository), typeof(TooltipRepository));
            services.AddScoped(typeof(ILayoutOverviewRepository), typeof(LayoutOverviewRepository));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Login}/{action=Index}/{id?}");
            });
        }
    }
}

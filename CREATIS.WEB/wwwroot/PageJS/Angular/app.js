﻿var CreatisService = angular.module('CreatisService', []);

var CREATIS = angular.module('CREATIS', ['ui.bootstrap', 'ngGrid', 'CreatisService', 'Spinner']);

CREATIS.filter("mydate", function () {
    var re = /\/Date\(([0-9]*)\)\//;
    return function (x) {
        var m = x.match(re);
        if (m) return new Date(parseInt(m[1]));
        else return null;
    };
});

var validationTrue = function (toastrMsg) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": 5000,
        "extendedTimeOut": 4000,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "stack": 1
    };
    Command: toastr["success"]("<p>" + toastrMsg + "</p>")
};

var validationFalse = function (toastrMsg) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": 5000,
        "extendedTimeOut": 4000,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "stack": 1
    };
    Command: toastr["error"]("<p>" + toastrMsg + "</p>")
};

function showLoader() {
    var offset = $('body').offset();
    var height = $('body').height();
    var width = $('body').width();
    $("#loading").addClass("loading-visible").removeClass("loading-invisible");
    $('#fountainG').find("div").addClass('fountainG');
    $("#loading").css({ "height": "" + height + "px", "width": "" + width + "px" })
    $("#loading").css({ "position": "absolute", "top": "" + offset.top + "px", "left": "" + offset.left + "px", "z-index": "1060" });
}

function hideLoader() {
    $("#loading").addClass("loading-invisible").removeClass("loading-visible");
    $('#fountainG').find("div").removeClass('fountainG');
}



$(document).ready(function () {

    var myDate = new Date();
    var hrs = myDate.getHours();
    var greet;
    if (hrs < 12)
        greet = 'Good Morning';
    else if (hrs >= 12 && hrs <= 17)
        greet = 'Good Afternoon';
    else if (hrs >= 17 && hrs <= 24)
        greet = 'Good Evening';
    $("#lblGreetings").text(greet);
});



//$(document).on('keypress', 'input[type=text]', function (e) {
//    var keycodes = [];
//    var exceptthis = $(this).data("expectvalidation");
//    if (exceptthis == "!" || exceptthis == "&") {
//        keycodes = [64, 36, 37, 94, 42, 60, 62, 34];
//    }
//    else {
//        keycodes = [33, 64, 36, 37, 94, 38, 42, 60, 62, 34];
//    }

//    if (keycodes.indexOf(e.keyCode) > -1)
//        return false;
//});

//$(document).on('blur', 'input[type=text]', function (e) {
//    var keyvalues = ['!', '@', '$', '%', '^', '&', '*', '<', '>', '"'];
//    var str = $(this).val().toString();
//    var e = 0;
//    var splchar = "";
//    for (var i = 0; i < str.length; i++) {
//        var exceptthis = $(this).data("expectvalidation");
//        if (exceptthis != "!" && exceptthis != "&") {
//            if (keyvalues.indexOf(str.charAt(i)) > -1) {
//                e = e + 1;
//                splchar = splchar + "," + str.charAt(i);
//                $(this).val($(this).val().toString().replace(str.charAt(i), ''));
//            }
//        }
//    }
//    if (e > 0)
//        validationFalse("Special Characters (" + splchar.toString().slice(1) + ") Not Allowed to Paste");
//});

$(document).on('click', '.collapsebutton', function () {
    var isOpen = $(this).find('.fa-minus');
    if (isOpen.length > 0) {
        $(this).find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
    } else {
        $(this).find('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
    }
});

$(document).on('click', '.collapseheader', function () {
    var isOpen = $(this).find('.fa-chevron-down');
    if (isOpen.length > 0) {
        $(this).find('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-left');
    } else {
        $(this).find('.fa-chevron-left').removeClass('fa-chevron-left').addClass('fa-chevron-down');
    }
}); 
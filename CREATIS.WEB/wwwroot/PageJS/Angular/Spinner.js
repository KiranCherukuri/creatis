﻿'use strict';
angular.module('Spinner', [])
    .directive('loading', function () {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="loading modal-backdrop" ><img class="loadingimg" src="/tx-creatis/images/loader.gif" alt="LoadingIcon"  align="middle" /></div>',
            link: function (scope, element, attr) {
                scope.$watch('loading', function (val) {
                    if (val) {
                        $(element).addClass("modal-backdrop");
                        $(element).show();
                    }
                    else {
                        $(element).removeClass("modal-backdrop");
                        $(element).hide();
                    }
                });
            }
        }
    });

﻿var jsdatafactory;
CREATIS.controller('LayoutOverviewController', ['$scope', 'dataFactory', '$uibModal', '$location', '$http', function ($scope, dataFactory, $uibModal, $location, $http, modalService) {
    jsdatafactory = dataFactory;
    $scope.loading = true;

 

    $(".loading").show();

    $scope.Dashboard = function () {
        dataFactory.GetRecentActivitiestoDashboard($scope.layoutId);
    }

    $scope.GoHome = function () {
        dataFactory.GoHomePage();
    };

    $scope.RedirecttoScreenLayout = function () {
        dataFactory.RedirecttoScreenLayout($scope.layoutId);
    }

    $scope.RedirecttoShowLayout = function () {
        dataFactory.RedirecttoShowLayout($scope.layoutId);
    }

    $scope.RedirecttoTranslation = function () {
        dataFactory.RedirecttoTranslation($scope.layoutId);
    }

    dataFactory.GetAlertCopiedLayout($scope.layoutId).then(function (resObj) {

        var result = resObj.data.split("ƒ");

        if (result[0] == "SL")
            $('#GOScreenLayout').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GOScreenLayout').removeClass('btn-warning').addClass('btn-primary');

        if (result[1] == "TR")
            $('#GoTranslation').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GoTranslation').removeClass('btn-warning').addClass('btn-primary');

    });

    fillLayoutOverviewgrid();

    function fillLayoutOverviewgrid() {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetOverallLayoutValues',
            success: function (data) {
                $(".loading").hide();
                fillGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }
    function fillGrid(data) {
        debugger;
        $("#gridLayoutOverViewValues").ejGrid({
            dataSource: ej.DataManager(ej.parseJSON(data)),
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Search] },
            allowPaging: true,
            allowScrolling: true,
            allowResizeToFit: true,
            allowFiltering: true,
            //allowTextWrap: true,
            allowSorting: true,
            queryCellInfo: "queryCellInfo",
            pageSettings: {
                pageSize: 25
            },
            columns: [
                { field: "layoutRefId", isPrimaryKey: true, visible: false },
                { field: "obcType", headerText: 'OBC Type' },
                { field: "layoutName", headerText: 'Layout Name' },
                { field: "createdBy", headerText: 'Created By' },
                { field: "createdOn", headerText: 'Created On', format: '{0:dd/MM/yyyy}' },
                { field: "name", headerText: 'Layout Name' },
                { field: "layoutOrigin", headerText: 'Layout Origin' },
                { field: "dateLastStatus", headerText: 'Date Last Status', format: '{0:dd/MM/yyyy}' },
                { field: "statusChangedBy", headerText: 'Status ChangedBy' },
                { field: "usedLanguages", headerText: 'Used Languages' },
                { field: "planningTypes", headerText: 'Planning Types' },
            ], actionComplete: function (args) {

                $("#gridLayoutOverViewValues_searchbar").css("height", "20px").css("width", "500px");
                $scope.loading = false;
            },
        });

        $('.e-pagercontainer').addClass("pull-left");
    }

}]);

function queryCellInfo(args) {

    $(args.cell).attr({
        "data-toggle": "tooltip",
        "data-container": "body",
        "title": args.data[args.column.field]
    });
}

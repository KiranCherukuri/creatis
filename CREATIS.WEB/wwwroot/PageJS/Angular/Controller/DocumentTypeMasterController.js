﻿CREATIS.controller('DocumentTypeMasterController', ['$scope', 'dataFactory', '$uibModal', '$http', function ($scope, dataFactory, $uibModal, $http, modalService) {
    //DocumentTypeMaster
    $scope.loading = true;
    $scope.active = [{ text: "Active", value: "Active" }, { text: "InActive", value: "InActive" }];
    fillDocumentTypeList();
    function fillDocumentTypeList() {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetDocumentTypeMaster',
            success: function (data) {
                if (data.Error === "") {
                    var dataManager = ej.DataManager({
                        json: data.data,
                        batchUrl: "/DocumentTypeMaster/SaveDocumentTypeMaster?data=" + data + "",
                        removeUrl: "/Grid/UrlDelete",
                        adaptor: "remoteSaveAdaptor",
                    });
                }
                fillGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }
    function fillGrid(data) {
        $("#grdDocumentType").ejGrid({
            dataSource: data,

            editSettings: { allowEditing: true, allowAdding: true, allowDeleting: false, allowCancel: true, editMode: ej.Grid.EditMode.Batch },
            allowPaging: true,
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Add, ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            columns: [
                { field: "documentTypeId", visible: false, isPrimaryKey: true },
                { field: "documentTypeCode", headerText: 'Document Type Code', width: "20%", validationRules: { required: true, maxlength: 3 } },
                { field: "documentTypeDescription", headerText: 'Document Type', width: "60%", validationRules: { required: true, maxlength: 25 } },
                { field: "active", headerText: 'Status', editType: ej.Grid.EditingType.Dropdown, dataSource: $scope.active, width: "80%", validationRules: { required: true } },
            ], actionComplete: function (args) {
                $scope.loading = false;
                $("#grdDocumentType_searchbar").css("height", "20px").css("width", "500px");

                var obj = $("#grdDocumentType").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');

            },
            beforeBatchSave: function (args, e) {
                var gridObj = $("#grdDocumentType").data("ejGrid");
                var updated = args.batchChanges.changed;
                var added = args.batchChanges.added;
                $.each(updated, function (index, value) {
                    debugger;
                    if (value.active == "Active")
                        value.isActive = 1;
                    else if (value.active == "InActive")
                        value.isActive = 0;
                    else {
                        validationFalse("Choose the status");
                        args.cancel = true;
                    }
                    var d = new Date();
                    value.modifiedOn = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                });
                $.each(added, function (index, value) {
                    debugger;
                    if (value.active == "Active")
                        value.isActive = 1;
                    else if (value.active == "InActive")
                        value.isActive = 0;
                    var d = new Date();
                    value.createdOn = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                });
                data = gridObj.getBatchChanges();
                BatchSaveDocumentTypeMaster(data)
            }
        });
        $scope.GoDocumentType = function () {
            dataFactory.GoDocumentType();
        };

        $('.e-pagercontainer').addClass("pull-left");
        $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
        $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');
    }
    function BatchSaveDocumentTypeMaster(data) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "SaveDocumentTypeMaster",
            data: data,
            success: function (data) {
                fillDocumentTypeList();
                if (data == "success")
                    validationTrue("Document Saved Successfully.");
                if (data == "error")
                    validationFalse("Name Already Exist");
            }
        })
    }
}]);
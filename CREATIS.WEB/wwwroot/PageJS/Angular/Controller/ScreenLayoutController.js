﻿CREATIS.controller('ScreenLayoutController', ['$scope', '$http', 'dataFactory', '$uibModal', '$location', function ($scope, $http, dataFactory, $uibModal, $location) {

    $scope.loading = true;
    $scope.rolesAccessList = [];

    var absUrl = $location.absUrl();
    urldata = absUrl.toString();

    var layoutid = urldata.split('=')[1];
    if (layoutid.toString().indexOf("#") >= 0)
        layoutid = layoutid.split('#')[0];


    $scope.LayoutID = layoutid;

    $scope.GoHome = function () {
        dataFactory.GoHomePage();
    };

    dataFactory.GetRolesAccessList().then(function (resObj) {
        $scope.rolesAccessList = resObj.data;
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetRegistration($scope.LayoutID).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.Registration = resObj.data;

            setTimeout(function () {
                updateregcolor("L");
            }, 100);
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });



    dataFactory.GetActivities($scope.LayoutID).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.Activities = resObj.data;
            setTimeout(function () {
                updateactcolor("L");
            }, 100);

        }
        else
            validationFalse(resObj.data.error);

    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetRegistrationPosition($scope.LayoutID).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.GetRegistrationPosition = resObj.data;
            setTimeout(function () {
                updateregcolor("R");
            }, 100);
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetActivitiesPosition($scope.LayoutID).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.GetActivitiesPosition = resObj.data;
            $scope.loading = false;
            setTimeout(function () {
                updateactcolor("R");
            }, 100);
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    $scope.Dashboard = function () {
        dataFactory.GetRecentActivitiestoDashboard($scope.LayoutID);
    }

    $scope.RedirecttoShowLayout = function () {
        dataFactory.RedirecttoShowLayout($scope.LayoutID);
    }

    $scope.RedirecttoTranslation = function () {
        dataFactory.RedirecttoTranslation($scope.LayoutID);
    }

    $scope.AddActPosition = function () {


        if ($scope.rolesAccessList.indexOf("SLASB") !== -1) {
            $scope.loading = true;
            var id = [];
            $('#Act2 li').each(function () {
                if ($(this).attr('id') != undefined) {
                    id.push($(this).attr('id'));
                }
            });
            var Did = [];
            $('#Act1 li').each(function () {
                if ($(this).attr('id') != undefined) {
                    Did.push($(this).attr('id'));
                }

            });

            if (id.length > 0) {
                dataFactory.UpdateActivitiesRegistrationPosition($scope.LayoutID, id).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        validationTrue("Position Assigned Successfully");
                        if (Did.length == 0) {
                            $scope.loading = false;
                            setTimeout(function () {
                                dataFactory.RedirecttoScreenLayout($scope.LayoutID);
                            }, 500);
                        }
                    }
                    else
                        validationFalse(resObj.data.error);
                }, function () {
                    $scope.error = "Some Error.";
                });
            }
            if (Did.length > 0) {
                dataFactory.UpdateDeActivitiesRegistrationPosition($scope.LayoutID, Did).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        validationTrue("Position Re-Assigned Successfully");
                        setTimeout(function () {
                            $scope.loading = false;
                            dataFactory.RedirecttoScreenLayout($scope.LayoutID);
                        }, 500);
                    }
                    else
                        validationFalse(resObj.data.error);

                }, function () {
                    $scope.error = "Some Error.";
                });
            }
        }
        else
            validationFalse("Access Denied");
    }

    $scope.AddRegPosition = function () {

        if ($scope.rolesAccessList.indexOf("SLRSB") !== -1) {
            $scope.loading = true;
            var id = [];
            $('#Reg2 li').each(function () {
                if ($(this).attr('id') != undefined) {
                    id.push($(this).attr('id'));
                }
            });
            var Did = [];
            $('#Reg1 li').each(function () {
                if ($(this).attr('id') != undefined) {
                    Did.push($(this).attr('id'));
                }
            });
            if (id.length > 0) {
                dataFactory.UpdateActivitiesRegistrationPosition($scope.LayoutID, id).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        validationTrue("Position Assigned Successfully");
                        if (Did.length == 0) {
                            setTimeout(function () {
                                $scope.loading = false;
                                dataFactory.RedirecttoScreenLayout($scope.LayoutID);
                            }, 500);
                        }
                    }
                    else
                        validationFalse(resObj.data.error);

                }, function () {
                    $scope.error = "Some Error.";
                });
            }

            if (Did.length > 0) {
                dataFactory.UpdateDeActivitiesRegistrationPosition($scope.LayoutID, Did).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        validationTrue("Position Re-Assigned Successfully");
                        setTimeout(function () {
                            $scope.loading = false;
                            dataFactory.RedirecttoScreenLayout($scope.LayoutID);
                        }, 500);
                    }
                    else
                        validationFalse(resObj.data.error);
                }, function () {
                    $scope.error = "Some Error.";
                });
            }
        }
        else
            validationFalse("Access Denied");
    }

    dataFactory.GetAlertCopiedLayout($scope.LayoutID).then(function (resObj) {

        var result = resObj.data.split("ƒ");

        if (result[1] == "TR")
            $('#GoTranslation').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GoTranslation').removeClass('btn-warning').addClass('btn-primary');
    });

}]);

function updateactcolor(Code) {
    if (Code == "L") {
        $("#Act1").find(".Grey").each(function () {
            $(this).closest("li").css("backgroundColor", "#AEAEAE");
        });
        $("#Act1").find(".Green").each(function () {
            $(this).closest("li").css("backgroundColor", "#004d00").css("color", "white");
        });
        $("#Act1").find(".Yellow").each(function () {
            $(this).closest("li").css("backgroundColor", "#cccc00").css("color", "white");
        });
        $("#Act1").find(".Red").each(function () {
            $(this).closest("li").css("backgroundColor", "#cc0000").css("color", "white");
        });
    }
    else if (Code == "R") {
        $("#Act2").find(".Grey").each(function () {
            $(this).closest("li").css("backgroundColor", "#AEAEAE");
        });
        $("#Act2").find(".Green").each(function () {
            $(this).closest("li").css("backgroundColor", "#004d00").css("color", "white");
        });
        $("#Act2").find(".Yellow").each(function () {
            $(this).closest("li").css("backgroundColor", "#cccc00").css("color", "white");
        });
        $("#Act2").find(".Red").each(function () {
            $(this).closest("li").css("backgroundColor", "#cc0000").css("color", "white");
        });
    }
}

function updateregcolor(Code) {
    if (Code == "L") {
        $("#Reg1").find(".Grey").each(function () {
            $(this).closest("li").css("backgroundColor", "#AEAEAE");
        });
        $("#Reg1").find(".Green").each(function () {
            $(this).closest("li").css("backgroundColor", "#004d00").css("color", "white");
        });
        $("#Reg1").find(".Yellow").each(function () {
            $(this).closest("li").css("backgroundColor", "#cccc00").css("color", "white");
        });
        $("#Reg1").find(".Red").each(function () {
            $(this).closest("li").css("backgroundColor", "#cc0000").css("color", "white");
        });

    }
    else if (Code == "R") {
        $("#Reg2").find(".Grey").each(function () {
            $(this).closest("li").css("backgroundColor", "#AEAEAE");
        });
        $("#Reg2").find(".Green").each(function () {
            $(this).closest("li").css("backgroundColor", "#004d00").css("color", "white");
        });
        $("#Reg2").find(".Yellow").each(function () {
            $(this).closest("li").css("backgroundColor", "#cccc00").css("color", "white");
        });
        $("#Reg2").find(".Red").each(function () {
            $(this).closest("li").css("backgroundColor", "#cc0000").css("color", "white");
        });
    }
}

function handleDrop() {
    alert('working');
}


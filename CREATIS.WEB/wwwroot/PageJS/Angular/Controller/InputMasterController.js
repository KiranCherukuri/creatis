﻿CREATIS.controller('InputMasterController', ['$scope', 'dataFactory', '$uibModal', '$http', function ($scope, dataFactory, $uibModal, $http, modalService) {
    //InputMaster
    $scope.loading = true;
    $scope.statusName = [{ text: "Active", value: "Active" }, { text: "InActive", value: "InActive" }];
    fillinputList();
    function fillinputList() {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetInputMaster',
            success: function (data) {
                if (data.Error === "") {
                    var dataManager = ej.DataManager({
                        json: data.data,
                        batchUrl: "/InputMaster/SaveInputMaster?data=" + data + "",
                        removeUrl: "/Grid/UrlDelete",
                        adaptor: "remoteSaveAdaptor",
                    });
                }
                fillGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }
    function fillGrid(data) {
        $.validator.addMethod("MaxLengthVal", function (value, element, params) {
            var val = ($("#grdinmaxLength").val());
            return (val) > 0;
        }, "Maximum length must be greater than 0.");
        $("#grdin").ejGrid({
            dataSource: data,
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Edit, ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            editSettings: { allowEditing: true, allowCancel: true },
            allowPaging: true,
            columns: [
                { field: "id", isPrimaryKey: true },
                { field: "moduleName", headerText: 'Module', width: "80%", allowEditing: false },
                { field: "controlName", headerText: 'Control', width: "80%", allowEditing: false },
                { field: "maxLength", headerText: 'MaxLength', width: "80%", validationRules: { required: true, MaxLengthVal: "value" } },
                { field: "statusName", headerText: 'Status', editType: ej.Grid.EditingType.Dropdown, dataSource: $scope.statusName, width: "80%", validationRules: { required: true } },
            ], actionComplete: function (args) {
                $("#grdin_searchbar").css("height", "20px").css("width", "500px");
                $scope.loading = false;


                var obj = $("#grdin").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');

                if (args.requestType == "save") {
                    var saveData = {
                        Id: args.data.id,
                        ModuleName: args.data.moduleName,
                        ControlName: args.data.controlName,
                        MaxLength: args.data.maxLength,
                        StatusName: args.data.statusName
                    }
                    dataFactory.SaveInputMaster(JSON.stringify(saveData)).then(function success(response) {
                        validationTrue('Updated successfully.');
                    }, function error(response) {
                        ValidationFalse("Error Occured.")
                        console.error(response.headers, response.status, response.statusText);
                    });
                    //UpdateInput Master
                }
            },
        });
        $scope.GoInput = function () {
            dataFactory.GoInput();
        };
        $('.e-pagercontainer').addClass("pull-left");
    }

}]);
﻿
var flag = true, targetPage = 1;
CREATIS.controller('TranslationController', ['$scope', '$http', 'dataFactory', '$uibModal', '$location', '$rootScope', function ($scope, $http, dataFactory, $uibModal, $location, $rootScope) {

    $scope.rolesAccessList = [];
    $scope.layoutStatusAccessList = [];
    $scope.roleWithLayoutStatusAccessList = [];
    $scope.layoutDefaultLanguageId = 0;

    var absUrl = $location.absUrl();
    urldata = absUrl.toString();
    $scope.LayoutID = urldata.split('=')[1];

    contenthideshow("G");

    $scope.Dashboard = function () {
        dataFactory.GetRecentActivitiestoDashboard($scope.LayoutID);
    }

    dataFactory.GetRolesAccessList().then(function (resObj) {
        $scope.rolesAccessList = resObj.data;
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetLayoutStatusAccessList($scope.LayoutID).then(function (resObj) {
        $scope.layoutStatusAccessList = resObj.data;
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetRoleWithLayoutStatusAccessList($scope.LayoutID).then(function (resObj) {
        $scope.roleWithLayoutStatusAccessList = resObj.data;
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetLayoutDefaultLanguageId($scope.LayoutID).then(function (resObj) {
        $scope.layoutDefaultLanguageId = resObj.data;
    }, function () {
        $scope.error = "Some Error.";
    });

    $scope.GetRecentActivitiestoDashboard = function () {
        dataFactory.GetRecentActivitiestoDashboard($scope.LayoutID);
    }

    dataFactory.GetAlertCopiedLayout($scope.LayoutID).then(function (resObj) {

        var result = resObj.data.split("ƒ");

        if (result[0] == "SL")
            $('#GOScreenLayout').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GOScreenLayout').removeClass('btn-warning').addClass('btn-primary');
    });

    ////dataFactory.GetTranslation($scope.LayoutID, 1).then(function (response) {
    //if (response.data.error === undefined) {
    //    $scope.translators = response.data;

    //    $scope.loading = false;
    //}
    //else
    //    validationFalse(response.data.error);
    //}, function () {
    //$scope.error = "Some Error";
    //});

    $scope.RedirecttoScreenLayout = function () {
        dataFactory.RedirecttoScreenLayout($scope.LayoutID);
    }

    $scope.RedirecttoShowLayout = function () {
        dataFactory.RedirecttoShowLayout($scope.LayoutID);
    }

    $scope.translatorAll = function (tableid) {
        contenthideshow("G");
        fillTranslations($scope, dataFactory, $scope.LayoutID);
    }

    $scope.translateActivities = function (tableid) {
        contenthideshow("T");
        $scope.loading = true;
        dataFactory.GetTranslation($scope.LayoutID, tableid).then(function (response) {
            if (response.data.error === undefined) {
                $scope.translators = response.data;

                $scope.loading = false;
            }
            else
                validationFalse(response.data.error);
        }, function () {
            $scope.error = "Some Error";
        });
    }

    $scope.translateRegistration = function (tableid) {
        contenthideshow("T");
        $scope.loading = true;
        dataFactory.GetRegistrationTranslation($scope.LayoutID, tableid).then(function (response) {
            if (response.data.error === undefined) {
                $scope.translators = response.data;

                $scope.loading = false;
            }
            else
                validationFalse(response.data.error);
        }, function () {
            $scope.error = "Some Error";
        });
    }

    $scope.translateInfoColum = function (tableid) {
        contenthideshow("T");
        $scope.loading = true;
        dataFactory.GetAllLangDetail($scope.LayoutID, tableid).then(function (response) {
            if (response.data.error === undefined) {
                $scope.translators = response.data;

                $scope.loading = false;
            }
            else
                validationFalse(response.data.error);
        }, function () {
            $scope.error = "Some Error";
        });
    }

    $scope.translateAllowances = function (tableid) {
        contenthideshow("T");
        $scope.loading = true;
        dataFactory.GetAllLangDetail($scope.LayoutID, tableid).then(function (response) {
            if (response.data.error === undefined) {
                $scope.translators = response.data;

                $scope.loading = false;
            }
            else
                validationFalse(response.data.error);
        }, function () {
            $scope.error = "Some Error";
        });
    }

    $scope.translateQuestionPath = function (tableid) {
        contenthideshow("T");
        $scope.loading = true;
        dataFactory.GetAllLangDetail($scope.LayoutID, tableid).then(function (response) {
            
            if (response.data.error === undefined) {
                $scope.translators = response.data;

                $scope.loading = false;
            }
            else
                validationFalse(response.data.error);
        }, function () {
            $scope.error = "Some Error";
        });
    }

    $scope.translateFlex = function (tableid) {
        contenthideshow("T");
        $scope.loading = true;
        dataFactory.GetAllLangDetail($scope.LayoutID, tableid).then(function (response) {
            if (response.data.error === undefined) {
                $scope.translators = response.data;

                $scope.loading = false;
            }
            else
                validationFalse(response.data.error);
        }, function () {
            $scope.error = "Some Error";
        });
    }


    $scope.openpopup = function (TableId, ColumnName, Id, Header) {

        if (($scope.rolesAccessList.indexOf("TU") !== -1 && $scope.layoutStatusAccessList.indexOf("TU") !== -1) || ($scope.roleWithLayoutStatusAccessList.indexOf("TU") !== -1)) {

            $scope.loading = true;
            dataFactory.updatelangdetail(TableId, ColumnName, Id, Header, $scope.LayoutID).then(function success(response) {
                $scope.loading = false;
                if (response.data.error === undefined) {
                    $scope.translator = response.data;
                    $scope.language = Header;
                    $('.divtranslator').removeClass('show').addClass('hide');
                    $('#TranslatorModal').modal();
                    $(".popupheader").html('<h4 class="pull-left"><b>' + Header + '</b></h4>');

                    //****** LangRefTable Id******
                    //ActivitiesRegistration = 1,
                    //InfoColumns = 2,
                    //Allowances = 3,
                    //ChioceProperties = 4,
                    //AddChioceProperties = 5,
                    //EntryProperties = 6,
                    //InfomessageProperties = 7,
                    //TextMessageProperties = 8,
                    //VariableListProperties = 9,
                    //PlanningselectProperties = 10,
                    //DocumetScanProperties = 11
                    //InfoMessageContentDetail = 12
                    //TransportTypeProperties = 13
                    //TransportTypePropertiesDetail = 14
                    //TrailerProperties = 15
                    //Questions = 16
                    //SetActionValue = 17
                    //ExtraInfoProperties = 18,
                    //ExtraInfo = 19,
                    //PalletsProperties = 20,
                    //Pallets = 21,
                    //ProblemsProperties = 22,
                    //Problems = 23,
                    //SignaturesProperties = 24,
                    //PictureProperties = 25,
                    //DocumentScannerProperties = 26,
                    //DocumentScanner = 27,

                    if (TableId == 1) {
                        $('#frmtranslator').removeClass('hide').addClass('show');
                        setTimeout(function () { $('#txttranslatorlanguage').focus(); }, 500);
                    }
                    else if (TableId == 2) {
                        $('#frmInfoColumns').removeClass('hide').addClass('show');
                        setTimeout(function () { $('#txtinfocoulmn').focus(); }, 500);
                    }
                    else if (TableId == 3) {
                        $('#frmAllowance').removeClass('hide').addClass('show');
                        setTimeout(function () { $('#txtallowance').focus(); }, 500);
                    }
                    else if (TableId == 7) {
                        $('#frmInfoMessage').removeClass('hide').addClass('show');
                        $("#txtInfoMessage").ejRTE({
                            width: "100%",
                            maxHeight: "120px",
                            minWidth: "150px",
                            toolsList: ['style'],
                            tools: {
                                style: ["bold", "underline"],
                            }, isResponsive: false
                        });
                        var selectedLanguage = "";
                        if (response.data.selectedLanguage == null) {
                            selectedLanguage = "";
                        }
                        else if (response.data.selectedLanguage.indexOf("<p>") != -1) {
                            selectedLanguage = response.data.selectedLanguage;
                        }
                        else {
                            selectedLanguage = "<p>" + response.data.selectedLanguage + "</p>";
                        }
                        setTimeout(function () {
                            $("#divtitleLangText").html(response.data.langText);
                            $("#txtInfoMessage").ejRTE({ value: selectedLanguage }); $('#txtInfoMessage').focus();
                        }, 500);
                    }
                    else if (TableId == 12) {
                        $('#frmInfoMessageContent').removeClass('hide').addClass('show');
                        $("#txtInfoMessageContent").ejRTE({
                            width: "100%",
                            maxHeight: "250px",
                            minWidth: "150px",
                            toolsList: ['style'],
                            tools: {
                                style: ["bold", "underline"],
                            }, isResponsive: true,
                            keydown: "onpMessageTranskeydown",
                            create: "oncreateTrans"
                        });
                        var selectedLanguage = "";
                        if (response.data.selectedLanguage == null) {
                            selectedLanguage = "";
                        }
                        else if (response.data.selectedLanguage.indexOf("<p>") != -1) {
                            selectedLanguage = response.data.selectedLanguage;
                        }
                        else {
                            selectedLanguage = "<p>" + response.data.selectedLanguage + "</p>";
                        }
                        
                        setTimeout(function () { $("#divlangText").html(response.data.langText); $("#txtInfoMessageContent").ejRTE({ value: selectedLanguage }); $('#txtInfoMessageContent').focus(); }, 500);
                    }
                    else {
                        $('#frmQuestionPath').removeClass('hide').addClass('show');
                        setTimeout(function () { $('#txtQuestionPath').focus(); }, 500);
                    }
                }
                else
                    validationFalse(response.data.error);
            }, function () {
                $scope.error = "Some Error.";
                $scope.loading = false;
            });
        }
        else
            validationFalse("Access Denied");
    }


    $scope.resetlang = function () {

        $scope.translator.selectedLanguage = "";
        setTimeout(function () {
            $('#txttranslatorlanguage').focus();
            $('#txtinfocoulmn').focus();
            $('#txtallowance').focus();
            $('#txtQuestionPath').focus();
            $('#txtInfoMessage').focus();
            $("#txtInfoMessageContent").focus();
        }, 100);
    }

    $scope.resetusedlang = function () {
        $scope.languageSelect.languageSelectedId = "";
        setTimeout(function () {
            $('#sltlanguage').focus();
        }, 100);
    }

    $scope.saveLangActivitiesRegistration = function (translator) {

        var obj = {
            "id": translator.id, "language": translator.language, "selectedLanguage": translator.selectedLanguage, "langRefTableId": translator.langRefTableId, "layoutRefId": (translator.layoutRefId == null ? $scope.LayoutID : translator.layoutRefId), "customerRefId": translator.customerRefId
        }

        var paramdata = JSON.stringify(obj);
        if (translator.selectedLanguage != '' && translator.selectedLanguage != null) {
            $scope.loading = true;
            if (translator.workingCode == "Registration") {
                dataFactory.saveregistrationlangdetail(paramdata).then(function success(response) {
                    $scope.loading = false;
                    if (response.data.error === undefined) {
                        validationTrue("Language Saved Successfully")
                        $scope.translators = response.data;
                        closepopup();
                    }
                    else
                        validationFalse(response.data.error);
                }, function () {
                    $scope.loading = false;
                    $scope.error = "Some Error.";
                });
            }
            else {
                dataFactory.saveactivitieslangdetail(paramdata).then(function success(response) {
                    $scope.loading = false;
                    if (response.data.error === undefined) {
                        validationTrue("Language Updated Successfully")
                        $scope.translators = response.data;
                        closepopup();
                    }
                    else
                        validationFalse(response.data.error);
                }, function () {
                    $scope.loading = false;
                    $scope.error = "Some Error.";
                });
            }
        }
        else {
            setTimeout(function () {
                $('#txttranslatorlanguage').focus();
                $('#txtinfocoulmn').focus();
                $('#txtallowance').focus();
                $('#txtQuestionPath').focus();
                $('#txtInfoMessage').focus();
                $("#txtInfoMessageContent").focus();
            }, 100);
            validationFalse("Please Enter Valid Input");
        }
    }

    $scope.saveAllLangDetail = function (translator) {

        if (parseInt(translator.langRefTableId, 10) == 12) {
            var InfomessageMessagecontentobj = $("#txtInfoMessageContent").data("ejRTE");
            translator.selectedLanguage = InfomessageMessagecontentobj.getHtml().toString().replace("&", "ƒ");
        }
        else if (parseInt(translator.langRefTableId, 10) == 7) {
            var InfomessageMessageobj = $("#txtInfoMessage").data("ejRTE");
            translator.selectedLanguage = InfomessageMessageobj.getHtml();
        }

        var obj = {
            "id": translator.id, "language": translator.language, "selectedLanguage": translator.selectedLanguage, "langRefTableId": translator.langRefTableId, "layoutRefId": (translator.layoutRefId == null ? $scope.LayoutID : translator.layoutRefId), "customerRefId": translator.customerRefId, "questionRefId": translator.questionRefId
        }
        var paramdata = JSON.stringify(obj);
        if (translator.selectedLanguage != '' && translator.selectedLanguage != null) {
            $scope.loading = true;
            dataFactory.savealllangdetail(paramdata.toString().replace("&nbsp;", " ")).then(function success(response) {
                $scope.loading = false;
                if (response.data.error === undefined) {
                    validationTrue("Language Saved Successfully")
                    $scope.translators = response.data;
                    closepopup();
                }
                else
                    validationFalse(response.data.error);
            }, function () {
                $scope.loading = false;
                $scope.error = "Some Error.";
            });
        }
        else {
            setTimeout(function () {
                $('#txttranslatorlanguage').focus();
                $('#txtinfocoulmn').focus();
                $('#txtallowance').focus();
                $('#txtQuestionPath').focus();
                $('#txtInfoMessage').focus();
                $("#txtInfoMessageContent").focus();
            }, 100);
            validationFalse("Please Enter Valid Input");
        }
    }

    $scope.saveInfoMessageContentLangDetail = function (translator) {

        if (parseInt(translator.langRefTableId, 10) == 12) {
            var InfomessageMessagecontentobj = $("#txtInfoMessageContent").data("ejRTE");
            translator.selectedLanguage = InfomessageMessagecontentobj.getHtml().toString().replace("&", "ƒ");
        }
        else if (parseInt(translator.langRefTableId, 10) == 7) {
            var InfomessageMessageobj = $("#txtInfoMessage").data("ejRTE");
            translator.selectedLanguage = InfomessageMessageobj.getHtml();
        }

        var obj = {
            "id": translator.id, "langRefDetailId": translator.langRefDetailId, "language": translator.language, "selectedLanguage": translator.selectedLanguage, "langRefTableId": translator.langRefTableId, "layoutRefId": (translator.layoutRefId == null ? $scope.LayoutID : translator.layoutRefId), "customerRefId": translator.customerRefId, "questionRefId": translator.questionRefId
        }
        var paramdata = JSON.stringify(obj);
        if (translator.selectedLanguage != '' && translator.selectedLanguage != null) {
            $scope.loading = true;
            dataFactory.saveInfoMessageContentLangDetail(paramdata.toString().replace("&nbsp;", " ")).then(function success(response) {
                $scope.loading = false;
                if (response.data.error === undefined) {
                    validationTrue("Language Saved Successfully")
                    $scope.translators = response.data;
                    closepopup();
                }
                else
                    validationFalse(response.data.error);
            }, function () {
                $scope.loading = false;
                $scope.error = "Some Error.";
            });
        }
        else {
            setTimeout(function () {
                $('#txttranslatorlanguage').focus();
                $('#txtinfocoulmn').focus();
                $('#txtallowance').focus();
                $('#txtQuestionPath').focus();
                $('#txtInfoMessage').focus();
                $("#txtInfoMessageContent").focus();
            }, 100);
            validationFalse("Please Enter Valid Input");
        }
    }

    $scope.saveInfoMessageTitleLangDetail = function (translator) {

        if (parseInt(translator.langRefTableId, 10) == 12) {
            var InfomessageMessagecontentobj = $("#txtInfoMessageContent").data("ejRTE");
            translator.selectedLanguage = InfomessageMessagecontentobj.getHtml().toString().replace("&", "ƒ");
        }
        else if (parseInt(translator.langRefTableId, 10) == 7) {
            var InfomessageMessageobj = $("#txtInfoMessage").data("ejRTE");
            translator.selectedLanguage = InfomessageMessageobj.getHtml();
        }

        var obj = {
            "id": translator.id, "langRefDetailId": translator.langRefDetailId, "language": translator.language, "selectedLanguage": translator.selectedLanguage, "langRefTableId": translator.langRefTableId, "layoutRefId": (translator.layoutRefId == null ? $scope.LayoutID : translator.layoutRefId), "customerRefId": translator.customerRefId, "questionRefId": translator.questionRefId
        }
        var paramdata = JSON.stringify(obj);
        if (translator.selectedLanguage != '' && translator.selectedLanguage != null) {
            $scope.loading = true;
            dataFactory.saveInfoMessageTitleLangDetail(paramdata.toString().replace("&nbsp;", " ")).then(function success(response) {
                $scope.loading = false;
                if (response.data.error === undefined) {
                    validationTrue("Language Saved Successfully")
                    $scope.translators = response.data;
                    closepopup();
                }
                else
                    validationFalse(response.data.error);
            }, function () {
                $scope.loading = false;
                $scope.error = "Some Error.";
            });
        }
        else {
            setTimeout(function () {
                $('#txttranslatorlanguage').focus();
                $('#txtinfocoulmn').focus();
                $('#txtallowance').focus();
                $('#txtQuestionPath').focus();
                $('#txtInfoMessage').focus();
                $("#txtInfoMessageContent").focus();
            }, 100);
            validationFalse("Please Enter Valid Input");
        }
    }

    $scope.saveUsedLanguage = function (languageSelect) {

        var obj = { "layoutRefId": languageSelect.layoutRefId, "activeList": languageSelect.activeList, "languageRefId": languageSelect.languageSelectedId, 'tableId': languageSelect.tableId }

        if (languageSelect.languageSelectedId != null && languageSelect.languageSelectedId != '') {
            var paramdata = JSON.stringify(obj);
            $scope.loading = true;
            dataFactory.saveUsedLanguages(paramdata).then(function success(response) {
                $scope.loading = false;
                if (response.data.error === undefined) {
                    validationTrue("Language Added Successfully")
                    $scope.translators = response.data;
                    if ($("#hidType").val() == "Table View") {
                        $(".popupheader").empty();
                        $('.divlanguage').removeClass('show').addClass('hide');
                        $('#LanguageSelectModal').modal('hide');
                    }
                    else {
                        setTimeout(function () {
                            dataFactory.RedirecttoTranslation($scope.LayoutID);
                        }, 1000);
                    }
                }
                else
                    validationFalse(response.data.error);

            }, function (response) {
                $scope.loading = false;
                $scope.error = "Some Error.";
            });

        }
        else {
            setTimeout(function () {
                $('#sltlanguage').focus();
            }, 100);
            validationFalse("Please Select Language");
        }
    }

    $scope.GoHome = function () {
        dataFactory.GoHomePage();
    };

    var closepopup = function () {
        $(".popupheader").empty();
        $('.divtranslator').removeClass('show').addClass('hide');
        $('#TranslatorModal').modal('hide');
    }

    $scope.closePopUp = function () {
        closepopup();
    }

    $scope.AddNewLanguage = function () {

        var activeLanguage = $('ul.activechildul').find('li.active').data('interest');
        if (activeLanguage == undefined)
            activeLanguage = $('ul#activeul').find('li.active').data('interest');

        var tableid = $('ul.activechildul').find('li.active').data('tableid');
        if (tableid == undefined)
            tableid = $('ul#activeul').find('li.active').data('tableid');

        dataFactory.addlanguage($scope.LayoutID, activeLanguage, tableid).then(function success(response) {
            if (response.data.error === undefined) {
                $scope.languageSelect = response.data;

                $('.divlanguage').removeClass('show').addClass('hide');
                $('#LanguageSelectModal').modal();
                $(".popupheader").html('<h4 class="pull- left"><b> Add Language </b></h4>');
                $('#frmLanguage').removeClass('hide').addClass('show');
                $("#hidType").val("Table View");

                setTimeout(function () {
                    $('#sltlanguage').focus();
                }, 500);
            }
            else
                validationFalse(response.data.error);

        }, function () {
            $scope.error = "Some Error.";
        });
    };

    $scope.ListViewAddNewLanguage = function () {

        var activeLanguage = $('ul.activechildul').find('li.active').data('interest');
        if (activeLanguage == undefined)
            activeLanguage = $('ul#activeul').find('li.active').data('interest');

        var tableid = $('ul.activechildul').find('li.active').data('tableid');
        if (tableid == undefined)
            tableid = $('ul#activeul').find('li.active').data('tableid');

        dataFactory.addlanguage($scope.LayoutID, activeLanguage, tableid).then(function success(response) {
            if (response.data.error === undefined) {
                $scope.languageSelect = response.data;

                $('.divlanguage').removeClass('show').addClass('hide');
                $('#LanguageSelectModal').modal();
                $(".popupheader").html('<h4 class="pull- left"><b> Add Language </b></h4>');
                $('#frmLanguage').removeClass('hide').addClass('show');
                $("#hidType").val("List View");

                setTimeout(function () {
                    $('#sltlanguage').focus();
                }, 500);
            }
            else
                validationFalse(response.data.error);

        }, function () {
            $scope.error = "Some Error.";
        });
    };

    $scope.closelanguage = function () {
        $(".popupheader").empty();
        $('.divlanguage').removeClass('show').addClass('hide');
        $('#LanguageSelectModal').modal('hide');

    }

    $("#UploadTranslation").ejUploadbox({
        saveUrl: "UploadTranslations",
        extensionsAllow: ".xlsx",
        multipleFilesSelection: false,
        dialogText: { title: "Import Translation" },
        dialogPosition: { X: 200, Y: 5 },
        dialogAction: { drag: false },
        dialogAction: { closeOnComplete: true },
        buttonText: { browse: "Import Excel", upload: "Upload", cancel: "Cancel" },
        complete: function (args) {
            if (args.responseText == "")
                validationTrue("Translation Imported Successfully.");
            else
                validationFalse(args.responseText);

            $("#loading").hide();
            fillTranslations($scope, dataFactory, $scope.LayoutID);
        },
        begin: function (args) {
            $("#loading").show();
        }
    });

    function contenthideshow(type) {
        if (type == "T") {
            $("#translationstable").show();
            $("#divtransactiongrid").hide();
        }
        else {
            $("#divtransactiongrid").show();
            $("#translationstable").hide();
        }
    }

    fillTranslations($scope, dataFactory, $scope.LayoutID);

    function fillGrid($scope, dataFactory, data) {
        var docHeight = $(document).height();
        if (docHeight <= 750)
            docHeight = parseInt(((docHeight * 45) / 100), 10);
        else if (docHeight <= 1000)
            docHeight = parseInt(((docHeight * 60) / 100), 10);
        else
            docHeight = parseInt(((docHeight * 65) / 100), 10);
        
        $("#translationsgrid").ejGrid({
            dataSource: ej.DataManager(data.langDetailList),
            minWidth: 600,
            isResponsive: true,
            allowTextWrap: true,
            allowSorting: true,
            allowFiltering: false,
            allowSrolling: true,
            enableHeaderHover: true,
            allowResizeToFit: true,
            allowPaging: true,
            //actionBegin: "begin",
            scrollSettings: { height: parseInt(docHeight), frozenRows: 0 },

            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            editSettings: { allowEditing: true, allowAdding: false, allowDeleting: false, allowCancel: true, editMode: ej.Grid.EditMode.Batch, showConfirmDialog: false },
            pageSettings: {
                pageSize: 25, currentPage: targetPage, printMode: ej.Grid.PrintMode.CurrentPage
            },
            queryCellInfo: "queryCellInfo",
            columns: [
                { field: "langRefDetailId", visible: false, isPrimaryKey: true },
                { field: "questionRefId", visible: false },
                { field: "customerRefId", visible: false },
                { field: "refId", visible: false },
                { field: "propertiesName", headerText: 'Item', allowEditing: false },
                { field: "propertiesPath", headerText: 'Detail', allowEditing: false },
                { field: "arabic", headerText: 'Arabic', visible: ((data.isShowArabic) ? true : false) },
                { field: "belarusian", headerText: 'Belarusian', visible: ((data.isShowBelarusian) ? true : false) },
                { field: "bulgarian", headerText: 'Bulgarian', visible: ((data.isShowBulgarian) ? true : false) },
                { field: "croatian", headerText: 'Croatian', visible: ((data.isShowCroatian) ? true : false) },
                { field: "czech", headerText: 'Czech', visible: ((data.isShowCzech) ? true : false) },
                { field: "danish", headerText: 'Danish', visible: ((data.isShowDanish) ? true : false) },
                { field: "dutch", headerText: 'Dutch', visible: ((data.isShowDutch) ? true : false) },
                { field: "english", headerText: 'English', visible: ((data.isShowEnglish) ? true : false) },
                { field: "estonian", headerText: 'Estonian', visible: ((data.isShowEstonian) ? true : false) },
                { field: "finnish", headerText: 'Finnish', visible: ((data.isShowFinnish) ? true : false) },
                { field: "french", headerText: 'French', visible: ((data.isShowFrench) ? true : false) },
                { field: "german", headerText: 'German', visible: ((data.isShowGerman) ? true : false) },
                { field: "greek", headerText: 'Greek', visible: ((data.isShowGreek) ? true : false) },
                { field: "hungarian", headerText: 'Hungarian', visible: ((data.isShowHungarian) ? true : false) },
                { field: "italian", headerText: 'Italian', visible: ((data.isShowItalian) ? true : false) },
                { field: "latvian", headerText: 'Latvian', visible: ((data.isShowLatvian) ? true : false) },
                { field: "lithuanian", headerText: 'Lithuanian', visible: ((data.isShowLithuanian) ? true : false) },
                { field: "macedonian", headerText: 'Macedonian', visible: ((data.isShowMacedonian) ? true : false) },
                { field: "norwegian", headerText: 'Norwegian', visible: ((data.isShowNorwegian) ? true : false) },
                { field: "polish", headerText: 'Polish', visible: ((data.isShowPolish) ? true : false) },
                { field: "portuguese", headerText: 'Portuguese', visible: ((data.isShowPortuguese) ? true : false) },
                { field: "romanian", headerText: 'Romanian', visible: ((data.isShowRomanian) ? true : false) },
                { field: "russian", headerText: 'Russian', visible: ((data.isShowRussian) ? true : false) },
                { field: "slovak", headerText: 'Slovak', visible: ((data.isShowSlovak) ? true : false) },
                { field: "slovene", headerText: 'Slovene', visible: ((data.isShowSlovene) ? true : false) },
                { field: "spanish", headerText: 'Spanish', visible: ((data.isShowSpanish) ? true : false) },
                { field: "swedish", headerText: 'Swedish', visible: ((data.isShowSwedish) ? true : false) },
                { field: "turkish", headerText: 'Turkish', visible: ((data.isShowTurkish) ? true : false) },
                { field: "ukrainian", headerText: 'Ukrainian', visible: ((data.isShowUkrainian) ? true : false) }

            ],
            actionBegin: function (args) {
                
                $("#loading").hide();
                $("#loading").hide();
                if (args.requestType == "paging") {
                    var batch = this.getBatchChanges();
                    targetPage = args.currentPage;
                    if (flag) {
                        if (batch.changed.length || batch.deleted.length || batch.added.length) {
                            args.cancel = true;
                            $("#ConfirmDialog").ejDialog("open");
                        }
                    }
                    else
                        flag = true;
                }
            },
            actionComplete: function (args) {
                $("#translationsgrid_searchbar").css("height", "20px").css("width", "500px");


                var obj = $("#translationsgrid").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');

                if (($scope.rolesAccessList.indexOf("TULV") !== -1 && $scope.layoutStatusAccessList.indexOf("TULV") !== -1) || ($scope.roleWithLayoutStatusAccessList.indexOf("TULV") !== -1)) {
                    $("#translationsgrid_update").removeClass("e-disable");
                }
                else {
                    $("#translationsgrid_update").addClass("e-disable");
                }
            },
            beforeBatchSave: function (args, e) {

                if (($scope.rolesAccessList.indexOf("TULV") !== -1 && $scope.layoutStatusAccessList.indexOf("TULV") !== -1) || ($scope.roleWithLayoutStatusAccessList.indexOf("TULV") !== -1)) {
                    var gridObj = $("#translationsgrid").data("ejGrid");
                    var changed = args.batchChanges.changed;
                    $("#loading").show();
                    BatchSaveUserEnvironment(changed);
                }
                else {
                    validationFalse("Updating Translations Is Not Allowed For The Current Layout Status");
                }


            },
            dataBound: function (args) {
                $("[data-toggle=tooltip]").tooltip();
            }
        });
        function BatchSaveUserEnvironment(changed) {
            $("#loading").show();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "SaveTranslations",
                data: { changedList: JSON.stringify(changed) },
                success: function (data) {
                    $("#loading").hide();
                    if (data == "success") {
                        validationTrue("Translation Updated Successfully.");
                        fillTranslations($scope, dataFactory, $scope.LayoutID);
                        //var obj = $(".e-grid").ejGrid("instance");
                        //obj.gotoPage(targetPage);
                    }
                    else
                        validationFalse(data);
                }
            })
        }
        $('.e-pagercontainer').addClass("pull-left");

        var obj = $(".e-grid").ejGrid("instance");
        obj.gotoPage(targetPage);
    }


    function fillTranslations($scope, dataFactory, LayoutID) {
        $("#loading").show();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetAllLangDetail',
            data: { LayoutID: LayoutID, TableId: 999999 },
            success: function (data) {
                fillGrid($scope, dataFactory, data);
            }, error: function (data) {
                alert(data);
            }
        });
    }

    $scope.ExportTranslations = function () {
        $scope.loading = true;

        dataFactory.GetExportExcelValidation($scope.LayoutID).then(function (resObj) {
            if (resObj.data.error === undefined) {
                $scope.loading = false;
                $('.divexporttranslations').removeClass('show').addClass('hide');
                $('#ExportTranslationsModal').modal();
                $(".popupheader").html('<h4 class="pull- left"><b> Export Translations </b></h4>');
                $('#frmExportTranslations').removeClass('hide').addClass('show');

                dataFactory.GetUsedLanguage($scope.LayoutID).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        LanguagesDropDown(resObj.data, $scope.layoutDefaultLanguageId);
                    }
                    else
                        validationFalse(resObj.data.error);
                }, function () {
                    $scope.error = "Some Error.";
                });

                setTimeout(function () {
                    $('#dpExportTranslations').focus();
                }, 500);
            }
            else {
                $scope.loading = false;
                validationFalse(resObj.data.error);
            }

        }, function () {
            $scope.error = "Some Error.";
        });

    };

    $scope.cancelExportTranslations = function () {
        $(".popupheader").empty();
        $('.divexporttranslations').removeClass('show').addClass('hide');
        $('#ExportTranslationsModal').modal('hide');
    }

    $scope.submitExportTranslations = function () {

        var LanguageIds = $("#dpExportTranslations").val();
        if (LanguageIds.trim().length > 0) {
            window.open('/tx-creatis/Translator/ExportTranslations?LayoutId=' + $scope.LayoutID + "&LanguageIds=" + LanguageIds, '_blank');
            $(".popupheader").empty();
            $('.divexporttranslations').removeClass('show').addClass('hide');
            $('#ExportTranslationsModal').modal('hide');
        }
        else {
            $("#dpExportTranslations").focus();
            validationFalse("Please Select Languages");
        }
    }

    function LanguagesDropDown(language, defualtlanguageId) {

        $('#dpExportTranslations').ejDropDownList({
            dataSource: language,
            watermarkText: "Select Language",
            fields: { text: "language", value: 'languageRefId' },
            width: "100%",
            showCheckbox: true,
            enableFilterSearch: true,
            enablePopupResize: true,
            delimiterChar: "-",
        });

        dropDownObj = $("#dpExportTranslations").data("ejDropDownList");
        dropDownObj.setSelectedValue(defualtlanguageId);
        for (var i = 0; i < language.length; i++) {
            if (language[i].languageRefId.toString() == defualtlanguageId.toString())
                dropDownObj.disableItemsByIndices(i);
        }

        $('#dpExportTranslations_popup_list_wrapper').addClass("syncdropdown");
        $scope.loading = false;

    }

}]);


$(function () {
    $("#ConfirmDialog").ejDialog({
        showOnInit: false,
        showFooter: true,
        footerTemplateId: "sample",
        beforeClose: "ondialogclose", 
        enableModal: true
    });
    $("#ok").ejButton({ size: "mini", height: 30, click: "OK", width: 70 });
    $("#cancel").ejButton({ size: "mini", height: 30, click: "Cancel", width: 70 });

});

function OK() {
    
    flag = false;
    var obj = $(".e-grid").ejGrid("instance");
    $("#ConfirmDialog").ejDialog("close");
    obj.batchSave();
    obj.gotoPage(targetPage);

}


function ondialogclose() {
    var obj = $(".e-grid").ejGrid("instance");
    var pager = obj.getPager().ejPager("instance");
    var pagermodel = obj.getPager().ejPager("model");
    pagermodel.currentPage = obj.model.pageSettings.currentPage;
    pager.refreshPager();
} 

function Cancel() {
    $("#ConfirmDialog").ejDialog("close");
} 



ej.RTE.prototype._emptyContent = function () {
    var setNode, tempSpanNode = document.createElement("span");
    tempSpanNode.innerHTML = "&#65279;"
    var range = this._getDocument().createRange(), Selection = this._getDocument().getSelection();
    this._getDocument().body.firstChild && this._getDocument().body.firstChild.remove();
    this._getDocument().body.appendChild(setNode = this._getDocument().createElement("p"));
    setNode.appendChild(this._getDocument().createElement("br"));
    setNode.appendChild(tempSpanNode);
    Selection.removeAllRanges();
    range.setStart(tempSpanNode.firstChild, 0);
    range.setEnd(tempSpanNode.firstChild, 0);
    Selection.addRange(range);
}

var event, rteObj, first = true, arr, isNode = false; var t, Node, lastIndex, lasttext;
var parts = [];


function oncreateTrans() {

    //this.getDocument().body.style.width = "240px";

    rteObj = this;
    focusNode = this.getDocument().defaultView.getSelection().focusNode;

    textLen = $(focusNode).closest('p');

    index = $(this.getDocument().body).find('p').index(textLen);

    range = this.createRange();
    $(this.getDocument().body).keydown(function (e) {

        event = e;
    });
    $(this.getDocument().body).bind('paste', function (e) {
        isNode = false;
        if ($(rteObj.getDocument().body).find('p').length > 13) {
            e.preventDefault();
        }
        if ((e.originalEvent.clipboardData.getData("Text").length > 35 || rteObj._getSelectedNode().textContent.length + e.originalEvent.clipboardData.getData("Text").length > 35) && ((rteObj._currentSelNode.textContent.length + e.originalEvent.clipboardData.getData("Text").length) > 35) && ($(rteObj.getDocument().body).find('p').length <= 13)) {
            e.preventDefault();
            var data = e.originalEvent.clipboardData.getData("Text");
            if (rteObj._getSelectedNode() && rteObj._getSelectedNode().textContent.length >= 0 && rteObj._getSelectedNode().textContent != "﻿") {
                isNode = true;
                var rang = rteObj._getRange().startOffset;
                if (rteObj._getSelectedNode().tagName == "BODY" && $(rteObj._getSelectedNode()).find("p:last")[0] != null) {
                    t = $(rteObj._getSelectedNode()).find("p:last")[0].textContent
                    data = t.substr() + data
                }
                else {
                    t = rteObj._getSelectedNode().textContent;
                    data = t.substr(0, rang) + data + t.substr(rang);
                }

                data = data.replace(/\n/g, '')
            }
            var count;
            var splitArr = data.match(/.{1,35}/g); //split the data into 35 
            for (var i = 0; i < splitArr.length; i++) {
                splitArr[i] = "<p>" + splitArr[i] + "</p>"; // wrap these words into <p> tag
            }
            count = 12 - $(rteObj.getDocument().body).find('p').length
            if (splitArr.length >= 12) {
                splitArr = splitArr.splice(0, 12);
            }
            if ($(rteObj.getDocument().body).find('p').length > 12) {
                splitArr = splitArr.splice(0, count);
            }
            if (!first && splitArr.length > count) {
                if (count == 0) {
                    arr = splitArr.splice(0, 1);
                }
                else {
                    arr = splitArr.splice(0, count);
                }

                parts = arr.join("");
            }
            else {
                parts = splitArr.join(""); //join these <p> tags
                first = false;
            }
            if (isNode && parts != "" && rteObj._getSelectedNode().tagName != "BODY") {
                var fragment = document.createRange().createContextualFragment(parts);
                var ele = document.createElement('p');
                ele.appendChild(fragment);
                if ((rteObj._getSelectedNode().previousSibling == null)) {
                    if (rteObj._getSelectedNode().tagName == "SPAN" && rteObj._getSelectedNode().parentNode.previousSibling != null) {
                        rteObj._getSelectedNode().parentNode.previousSibling.insertAdjacentHTML("afterend", parts);
                        rteObj._getSelectedNode().parentNode.remove();
                    } else {
                        rteObj._getSelectedNode().insertAdjacentHTML("afterend", parts)
                        rteObj._getSelectedNode().remove();
                    }

                } else {
                    rteObj._getSelectedNode().previousSibling.insertAdjacentHTML("afterend", parts);
                    if ($(rteObj.getDocument().body).find('p').length != 12)
                        rteObj._getSelectedNode().remove();
                }

            }
            else if (parts != "" && rteObj._getSelectedNode().tagName == "BODY" && $(rteObj._getSelectedNode()).find("p:last")[0] != null) {
                $(rteObj._getSelectedNode()).find("p:last").remove();
                $(rteObj._getSelectedNode()).find("p:last")[0].insertAdjacentHTML("afterend", parts);

            }
            //paste the splitted content into RTE.				
            else if (parts != "")
                rteObj.executeCommand("insertHtml", parts);
        }
    });
}

function onpMessageTranskeydown(args) {


    if (event.ctrlKey && (args.keyCode === 67 || args.keyCode == 88 || args.keyCode == 86 || args.keyCode === 65 || args.keyCode === 17)) {
        return
    }

    var focusNode = this.getDocument().defaultView.getSelection().focusNode;

    var textLen = $(focusNode).closest('p');

    var index = $(this.getDocument().body).find('p').index(textLen);

    var range = this.createRange();

    if (((textLen.text().length) >= 35) && (args.keyCode != 17) && args.keyCode !== 13 && args.keyCode !== 8 && args.keyCode !== 46 && args.keyCode !== 40 && args.keyCode !== 39 && args.keyCode !== 38 && args.keyCode !== 37) {
        if ((/\s+$/.test(textLen.text())) == false) {
            lastIndex = textLen.text().lastIndexOf(" ");
            if (lastIndex != -1 && lastIndex < 36) {
                lasttext = textLen.text().substring(lastIndex);
                rteObj._getSelectedNode().innerText = focusNode.textContent.replace(lasttext, "");
            }
            else {
                lasttext = "";
            }
        }
        if ($(this.getDocument().body).find('p').length > 11) {
            event.preventDefault();
            return
        }
        var range = this.createRange();
        pNode = document.createElement('span');
        if (lasttext) {
            pNode.innerHTML = lasttext;
            lasttext = "";
        }
        else
            pNode.innerHTML = '&#65279;&#65279;';
        var child = $("<p></p>").append(pNode);
        $(textLen).after(child);
        var tag = $(child).find("span")[0].firstChild;
        range.setStart(tag, tag.length);
        range.setEnd(tag, tag.length);
        rteObj.selectRange(range);

    }

    if (args.keyCode == 13) {
        if ($(this.getDocument().body).find('p').length > 11) {
            event.preventDefault();
        }
    }
    else if (((index > 12) && ($(this.getDocument().body).find('p').length > 12)) && (args.keyCode != 8) && (args.keyCode != 46) && (args.ctrlKey != true) && (args.keyCode !== 40 && args.keyCode !== 39 && args.keyCode !== 38 && args.keyCode !== 37)) {

        event.preventDefault();

    }

}

function queryCellInfo(args) {

    $(args.cell).attr({
        "data-toggle": "tooltip",
        "data-container": "body",
        "title": args.data[args.column.field]
    });

    

    if (args.column.field == "english" && args.data.isModificationColorEnglish != "Grey") {
        if (args.data.isModificationColorEnglish == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorEnglish == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "arabic" && args.data.isModificationColorArabic != "Grey") {
        if (args.data.isModificationColorArabic == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorArabic == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "Bulgarian" && args.data.isModificationColorBulgarian != "Grey") {
        if (args.data.isModificationColorBulgarian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorBulgarian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "croatian" && args.data.isModificationColorCroatian != "Grey") {
        if (args.data.isModificationColorCroatian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorCroatian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "czech" && args.data.isModificationColorCzech != "Grey") {
        if (args.data.isModificationColorCzech == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorCzech == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "danish" && args.data.isModificationColorDanish != "Grey") {
        if (args.data.isModificationColorDanish == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorDanish == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "dutch" && args.data.isModificationColorDutch != "Grey") {
        if (args.data.isModificationColorDutch == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorDutch == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "estonian" && args.data.isModificationColorEstonian != "Grey") {
        if (args.data.isModificationColorEstonian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorEstonian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "finnish" && args.data.isModificationColorFinnish != "Grey") {
        if (args.data.isModificationColorFinnish == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorFinnish == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "french" && args.data.isModificationColorFrench != "Grey") {
        if (args.data.isModificationColorFrench == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorFrench == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "german" && args.data.isModificationColorGerman != "Grey") {
        if (args.data.isModificationColorGerman == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorGerman == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "greek" && args.data.isModificationColorGreek != "Grey") {
        if (args.data.isModificationColorGreek == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorGreek == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "hungarian" && args.data.isModificationColorHungarian != "Grey") {
        if (args.data.isModificationColorHungarian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorHungarian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "italian" && args.data.isModificationColorItalian != "Grey") {
        if (args.data.isModificationColorItalian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorItalian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "latvian" && args.data.isModificationColorLatvian != "Grey") {
        if (args.data.isModificationColorLatvian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorLatvian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "lithuanian" && args.data.isModificationColorLithuanian != "Grey") {
        if (args.data.isModificationColorLithuanian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorLithuanian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "macedonian" && args.data.isModificationColorMacedonian != "Grey") {
        if (args.data.isModificationColorMacedonian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorMacedonian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "norwegian" && args.data.isModificationColorNorwegian != "Grey") {
        if (args.data.isModificationColorNorwegian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorNorwegian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "polish" && args.data.isModificationColorPolish != "Grey") {
        if (args.data.isModificationColorPolish == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorPolish == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "portuguese" && args.data.isModificationColorPortuguese != "Grey") {
        if (args.data.isModificationColorPortuguese == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorPortuguese == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "romanian" && args.data.isModificationColorRomanian != "Grey") {
        if (args.data.isModificationColorRomanian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorRomanian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "russian" && args.data.isModificationColorRussian != "Grey") {
        if (args.data.isModificationColorRussian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorRussian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "slovak" && args.data.isModificationColorSlovak != "Grey") {
        if (args.data.isModificationColorSlovak == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorSlovak == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "slovene" && args.data.isModificationColorSlovene != "Grey") {
        if (args.data.isModificationColorSlovene == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorSlovene == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "spanish" && args.data.isModificationColorSpanish != "Grey") {
        if (args.data.isModificationColorSpanish == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorSpanish == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "swedish" && args.data.isModificationColorSwedish != "Grey") {
        if (args.data.isModificationColorSwedish == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorSwedish == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "turkish" && args.data.isModificationColorTurkish != "Grey") {
        if (args.data.isModificationColorTurkish == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorTurkish == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "ukrainian" && args.data.isModificationColorUkrainian != "Grey") {
        if (args.data.isModificationColorUkrainian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorUkrainian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }
    if (args.column.field == "belarusian" && args.data.isModificationColorBelarusian != "Grey") {
        if (args.data.isModificationColorBelarusian == "Green") {
            args.cell.style.backgroundColor = "#004d00";
            args.cell.style.color = "white";
        }
        else if (args.data.isModificationColorBelarusian == "Yellow") {
            args.cell.style.backgroundColor = "#cccc00";
            args.cell.style.color = "white";
        }
    }

    if (args.column.field != "propertiesName" && args.column.field != "propertiesPath") {
        if (args.data.isDefaultTranslation)
            args.cell.style.fontWeight = "bold";
        else
            args.cell.style.fontWeight = "normal";
    }
   
}

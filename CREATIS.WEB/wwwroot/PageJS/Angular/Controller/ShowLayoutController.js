﻿CREATIS.controller('ShowLayoutController', ['$scope', '$http', 'dataFactory', '$uibModal', '$location', '$rootScope', function ($scope, $http, dataFactory, $uibModal, $location, $rootScope) {

    var absUrl = $location.absUrl();
    urldata = absUrl.toString();
    $scope.LayoutID = urldata.split('=')[1];

    $scope.GoHome = function () {
        dataFactory.GoHomePage();
    };

    $scope.Dashboard = function () {
        dataFactory.GetRecentActivitiestoDashboard($scope.LayoutID);
    }

    dataFactory.GetAllActivityRegistrationByLayoutId($scope.LayoutID).then(function success(response) {

        $scope.showlayoutist = response.data;
        ActivitiesDropDown($scope, dataFactory, response.data);
    }, function (response) {

        $scope.error = "Some Error";
    });

    $scope.RedirecttoScreenLayout = function () {
        dataFactory.RedirecttoScreenLayout($scope.LayoutID);
    }

    $scope.RedirecttoTranslation = function () {
        dataFactory.RedirecttoTranslation($scope.LayoutID);
    }

    dataFactory.GetLanguagesByLayoutId($scope.LayoutID).then(function (resObj) {

        $scope.Language = resObj.data;
        if (resObj.data.error === undefined) {
            LanguagesDropDown(resObj.data, $scope.LayoutID);
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    function LanguagesDropDown(language, layoutId, loading) {
        debugger
        //  
        var DefaultLanguageId = $("#hdDefaultLanguageId").val();
        if (DefaultLanguageId != "" && DefaultLanguageId != null)
            $('#dpLanguage').ejDropDownList({
                dataSource: language,
                watermarkText: "Select Language",
                fields: { text: "languageDescription", value: 'languageId' },
                width: 260,
                change: function (sender) {
                    //
                    var obj = $('#dpLanguage').data("ejDropDownList");
                    $("#hdDefaultLanguageId").val(obj.option("text"));
                    var langauage = obj.option("text");
                    var layoutId = $("#hdLayoutId").val();

                    $("#divShowLayoutData").html('');
                    $(".loading").css("display", "block").css("background-color", "transparent !important;");
                    dataFactory.GetActivitiesRegistrationByLayoutId(layoutId).then(function success(response) {
                        var activitiesRegisrationList = response.data;
                        var endLoopId = 0;
                        var altercolorid = 0;
                        debugger
                        var htmlElements = "";
                        for (var i = 0; i < activitiesRegisrationList.length; i++) {

                            if (i == (parseInt(activitiesRegisrationList.length, 10) - 1))
                                endLoopId = 1;
                            $.ajax({
                                type: "POST",
                                url: "GetShowLayotDataByLanguageId?langauage=" + langauage + "&layoutId=" + layoutId + "&activitiesRegistrationId=" + activitiesRegisrationList[i] + "&loopId=" + i + "&endLoopId=" + endLoopId + "&altercolorid=" + altercolorid,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                async: false,
                                success: function (data) {
                                    if (altercolorid == 0)
                                        altercolorid = 1;
                                    else
                                        altercolorid = 0;
                                    debugger
                                    $(".loading").css("display", "none");
                                    htmlElements = htmlElements + data.strbuilderData.m_StringValue;
                                    $("#divShowLayoutData").html(htmlElements);
                                    LoadShowLayoutDatatablesData(i);
                                }
                            });
                        }
                    }, function (response) {
                        $scope.error = "Some Error";
                    });
                }
            }).data("ejDropDownList").setSelectedText($("#hdDefaultLanguageId").val());
    }

    var stoploading = function () {
        $scope.loading = false;
        return false;

    }

    function LoadShowLayoutDatatablesData(repeatCount) {
        // fixTable(document.getElementById('fixed-table-container-' + repeatCount));
        //fixTable($());
        //var table = $('#show_Layout' + repeatCount).DataTable({
        //var table = $("table.showLayoutTables").DataTable({
        //    columns: [
        //        {
        //            name: 'first', width: '15%'
        //        },
        //        {
        //            name: 'second', width: '35%'
        //        },
        //        {
        //            name: 'three', width: '10%'
        //        },
        //        {
        //            name: 'four', width: '10%'
        //        },
        //        {
        //            name: 'five', width: '10%'
        //        },
        //        {
        //            name: 'six', width: '10%'
        //        }
        //    ],
        //    scrollY: "500px",
        //    scrollX: true,
        //    scrollCollapse: true,
        //    paging: false,
        //    fixedColumns: {
        //        leftColumns: 1
        //    },
        //    //ordering: false,
        //    //fnDrawCallback: function (settings) {
        //    //    $("table.showLayoutTables").parent().toggle(settings.fnRecordsDisplay() > 0);
        //    //}
        //});
        // $('table.showLayoutTables').dataTable();
        // $('table.showLayoutTables').destroy();
    }

    dataFactory.GetAlertCopiedLayout($scope.LayoutID).then(function (resObj) {

        var result = resObj.data.split("ƒ");

        if (result[0] == "SL")
            $('#GOScreenLayout').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GOScreenLayout').removeClass('btn-warning').addClass('btn-primary');

        if (result[1] == "TR")
            $('#GoTranslation').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GoTranslation').removeClass('btn-warning').addClass('btn-primary');

    });

}]);

function ActivitiesDropDown($scope, dataFactory, activities) {
    //alert(JSON.stringify(activities));
    $('#sltActivities').ejDropDownList({
        dataSource: activities.activitiesRegistrations,
        watermarkText: "Search By Activities",
        fields: { text: "activitiesRegistrationName", value: 'activitiesRegistrationId' },
        width: 260,
        //showCheckbox: true,
        // enableFilterSearch: true,
        enablePopupResize: true,
        //select: function (sender) {

        //    GetAllShowLayoutView($scope, dataFactory, sender.value);
        //}
        change: function (sender) {
            //  
            var element = document.getElementById($(".e-active").attr('data-value'));
            element.scrollIntoView({});
        }
    });

    //  $('#sltActivities_popup_list_wrapper').addClass("syncdropdown");
}

function GetAllShowLayoutView($scope, dataFactory, activityId) {
    //alert(activityId);
    dataFactory.GetAllShowLayoutView(activityId).then(function success(response) {
        //
        //alert('sucess');
        //  alert(JSON.stringify(response.data));

        $scope.showlayoutviewlist = response.data;
        setTimeout(function () {
            $("#divQuestiontreewithouttransport").ejTreeView({
                allowDragAndDrop: false,
                fields: {
                    dataSource: response.data.questionPathShowLayoutLists, id: "questionId",
                    parentId: "parentId", text: "questionName", hasChild: true, expanded: true
                },
                allowDragAndDropAcrossControl: false,


            });
            var treeObj = $("#divQuestiontreewithouttransport").data("ejTreeView");
            treeObj.expandAll();
        }, 500);

    }, function (response) {

        $scope.error = "Some Error";
    });
}

function ClickActivities(activityRegid) {
    window.location = "/tx-creatis/QuestionPath/QuestionPath?LayoutID=" + $("#hdLayoutId").val() + "&ActivityID=" + activityRegid;
}
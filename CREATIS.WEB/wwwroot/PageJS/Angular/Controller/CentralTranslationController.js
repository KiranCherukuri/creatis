﻿// comments added ok 11
CREATIS.controller('CentralTranslationController', ['$scope', '$http', 'dataFactory', '$uibModal', '$location', '$rootScope', function ($scope, $http, dataFactory, $uibModal, $location, $rootScope) {

    $scope.rolesAccessList = [];


    dataFactory.GetRolesAccessList().then(function (resObj) {
        $scope.rolesAccessList = resObj.data;
    }, function () {
        $scope.error = "Some Error.";
    });

    $('.generaltranslations').removeClass('hide').addClass('show');
    $('.customerspecifictranslations').removeClass('show').addClass('hide');
    $('.commonuitranslations').removeClass('show').addClass('hide');
    $('.specificuitranslations').removeClass('show').addClass('hide');

    $("#loading").show();

    fillGeneralTranslationList();

    $scope.generalTranslation = function () {
        $("#loading").show();
        fillGeneralTranslationList();
        $('.generaltranslations').removeClass('hide').addClass('show');
        $('.customerspecifictranslations').removeClass('show').addClass('hide');
        $('.commonuitranslations').removeClass('show').addClass('hide');
        $('.specificuitranslations').removeClass('show').addClass('hide');
    }

    function fillGeneralTranslationList() {

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetGeneralTranslation',
            success: function (data) {
                if (data.Error === "") {
                    var dataManager = ej.DataManager({
                        json: data.data,
                        batchUrl: "/Translator/SaveGeneralTranslation?data=" + data + "",
                        removeUrl: "/Grid/UrlDelete",
                        adaptor: "remoteSaveAdaptor",
                    });
                }
                fillGeneralTranslationGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }

    function fillGeneralTranslationGrid(data) {

        $("#loading").show();

        var docHeight = $(document).height();
        if (docHeight <= 750)
            docHeight = parseInt(((docHeight * 45) / 100), 10);
        else if (docHeight <= 1000)
            docHeight = parseInt(((docHeight * 60) / 100), 10);
        else
            docHeight = parseInt(((docHeight * 65) / 100), 10);

        $("#generaltranslationsgrid").ejGrid({
            dataSource: ej.DataManager(data),
            minWidth: 600,
            isResponsive: true,
            enablelAltRow: true,
            allowTextWrap: true,
            allowSorting: true,
            allowFiltering: false,
            allowScrolling: true,
            enableHeaderHover: true,
            allowResizeToFit: true,
            editSettings: { allowEditing: true, allowAdding: true, allowDeleting: true, allowCancel: true, editMode: ej.Grid.EditMode.Batch },
            allowPaging: true,
            scrollSettings: { height: parseInt(docHeight), frozenRows: 0 },
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Add, ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Delete, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            pageSettings: {
                pageSize: 25
            },
            queryCellInfo: "queryCellInfo",
            columns: [
                { field: "id", visible: false, isPrimaryKey: true },
                { field: "code", allowEditing: false, headerText: 'Code', width: 100 },
                { field: "english", headerText: 'English', width: 150 },
                { field: "arabic", headerText: 'Arabic', width: 150 },
                { field: "belarusian", headerText: 'Belarusian', width: 150 },
                { field: "bulgarian", headerText: 'Bulgarian', width: 150 },
                { field: "croatian", headerText: 'Croatian', width: 150 },
                { field: "czech", headerText: 'Czech', width: 150 },
                { field: "danish", headerText: 'Danish', width: 150 },
                { field: "dutch", headerText: 'Dutch', width: 150 },
                { field: "estonian", headerText: 'Estonian', width: 150 },
                { field: "finnish", headerText: 'Finnish', width: 150 },
                { field: "french", headerText: 'French', width: 150 },
                { field: "german", headerText: 'German', width: 150 },
                { field: "greek", headerText: 'Greek', width: 150 },
                { field: "hungarian", headerText: 'Hungarian', width: 150 },
                { field: "italian", headerText: 'Italian', width: 150 },
                { field: "latvian", headerText: 'Latvian', width: 150 },
                { field: "lithuanian", headerText: 'Lithuanian', width: 150 },
                { field: "macedonian", headerText: 'Macedonian', width: 150 },
                { field: "norwegian", headerText: 'Norwegian', width: 150 },
                { field: "polish", headerText: 'Polish', width: 150 },
                { field: "portuguese", headerText: 'Portuguese', width: 150 },
                { field: "romanian", headerText: 'Romanian', width: 150 },
                { field: "russian", headerText: 'Russian', width: 150 },
                { field: "slovak", headerText: 'Slovak', width: 150 },
                { field: "slovene", headerText: 'Slovene', width: 150 },
                { field: "spanish", headerText: 'Spanish', width: 150 },
                { field: "swedish", headerText: 'Swedish', width: 150 },
                { field: "turkish", headerText: 'Turkish', width: 150 },
                { field: "ukrainian", headerText: 'Ukrainian', width: 150 }
            ], actionComplete: function (args) {
                $("#loading").hide();

                $("#generaltranslationsgrid_searchbar").css("height", "20px").css("width", "500px");
                var obj = $("#generaltranslationsgrid").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');
               

            },
            beforeBatchSave: function (args, e) {
                var gridObj = $("#generaltranslationsgrid").data("ejGrid");
                var changed = args.batchChanges.changed;
                var added = args.batchChanges.added;
                var deleted = args.batchChanges.deleted;
                debugger
                BatchSaveGeneralTranslation(changed, added, deleted)
            }

        });

        $('.e-pagercontainer').addClass("pull-left");
        $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
        $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');
    }

    function BatchSaveGeneralTranslation(changed, added, deleted) {
        $("#loading").show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "SaveGeneralTranslation",
            data: { changedList: JSON.stringify(changed), addedList: JSON.stringify(added), deletedList: JSON.stringify(deleted) },
            success: function (data) {
                $("#loading").hide();
                fillGeneralTranslationList();
                if (data == "success")
                    validationTrue("General Translation Saved Successfully.");
                else if (data == "error")
                    validationFalse("General Translation Already Exist");
                else
                    validationFalse(data);
            }
        });
    }

    $scope.customerSpecificTranslation = function () {


        $('.customerspecifictranslations').removeClass('hide').addClass('show');
        $('.generaltranslations').removeClass('show').addClass('hide');
        $('.commonuitranslations').removeClass('show').addClass('hide');
        $('.specificuitranslations').removeClass('show').addClass('hide');

        if (parseInt($('#CustomerSpecific').ejDropDownList("getSelectedValue"), 10) > 0)
            $('#customerspecificgrid').show();
        else
            $('#customerspecificgrid').hide();
    }

    $scope.getCustomerSpecificTranslation = function () {
        $("#loading").show();

        $('.customerspecifictranslations').removeClass('hide').addClass('show');
        $('.generaltranslations').removeClass('show').addClass('hide');
        $('.commonuitranslations').removeClass('show').addClass('hide');
        $('.specificuitranslations').removeClass('show').addClass('hide');

        $('#customerspecificgrid').show();
        fillCustomerSpecificTranslationList($('#CustomerSpecific').ejDropDownList("getSelectedValue"));
    }

    dataFactory.GetCustomerSpecificList().then(function (resObj) {
        if (resObj.data.error === undefined) {
            CustomerSpecificDropDown(resObj.data);
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    function fillCustomerSpecificTranslationList(data) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetCustomerSpecificTranslation',
            data: { CustomerId: data },
            success: function (data) {
                if (data.Error === "") {
                    var dataManager = ej.DataManager({
                        json: data.data,
                        batchUrl: "/Translator/SaveCustomerSpecificTranslation?data=" + data + "",
                        removeUrl: "/Grid/UrlDelete",
                        adaptor: "remoteSaveAdaptor",
                    });
                }
                fillCustomerSpecificTranslationGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }

    function fillCustomerSpecificTranslationGrid(data) {
        $("#loading").show();
        var docHeight = $(document).height();
        if (docHeight <= 750)
            docHeight = parseInt(((docHeight * 40) / 100), 10);
        else if (docHeight <= 1000)
            docHeight = parseInt(((docHeight * 55) / 100), 10);
        else
            docHeight = parseInt(((docHeight * 60) / 100), 10);

        $("#customerspecifictranslationsgrid").ejGrid({
            dataSource: ej.DataManager(data),
            minWidth: 600,
            isResponsive: true,
            enablelAltRow: true,
            allowTextWrap: true,
            allowSorting: true,
            allowFiltering: false,
            allowSrolling: true,
            enableHeaderHover: true,
            allowResizeToFit: true,
            editSettings: { allowEditing: true, allowCancel: true, editMode: ej.Grid.EditMode.Batch },
            allowPaging: true,
            scrollSettings: { height: parseInt(docHeight), frozenRows: 0 },
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            pageSettings: {
                pageSize: 25
            },
            queryCellInfo: "queryCellInfo",
            columns: [
                { field: "id", visible: false, isPrimaryKey: true },
                { field: "customerRefId", visible: false },
                //{ field: "customerName", headerText: 'Customer Name', width: 250 },
                { headerText: "Action", template: "#CustomerSpecificToGeneral", textAlign: "center", width: 100, allowEditing: false },
                { field: "english", headerText: 'English', width: 150 },
                { field: "arabic", headerText: 'Arabic', width: 150 },
                { field: "belarusian", headerText: 'Belarusian', width: 150 },
                { field: "bulgarian", headerText: 'Bulgarian', width: 150 },
                { field: "croatian", headerText: 'Croatian', width: 150 },
                { field: "czech", headerText: 'Czech', width: 150 },
                { field: "danish", headerText: 'Danish', width: 150 },
                { field: "dutch", headerText: 'Dutch', width: 150 },
                { field: "estonian", headerText: 'Estonian', width: 150 },
                { field: "finnish", headerText: 'Finnish', width: 150 },
                { field: "french", headerText: 'French', width: 150 },
                { field: "german", headerText: 'German', width: 150 },
                { field: "greek", headerText: 'Greek', width: 150 },
                { field: "hungarian", headerText: 'Hungarian', width: 150 },
                { field: "italian", headerText: 'Italian', width: 150 },
                { field: "latvian", headerText: 'Latvian', width: 150 },
                { field: "lithuanian", headerText: 'Lithuanian', width: 150 },
                { field: "macedonian", headerText: 'Macedonian', width: 150 },
                { field: "norwegian", headerText: 'Norwegian', width: 150 },
                { field: "polish", headerText: 'Polish', width: 150 },
                { field: "portuguese", headerText: 'Portuguese', width: 150 },
                
                { field: "romanian", headerText: 'Romanian', width: 150 },
                { field: "russian", headerText: 'Russian', width: 150 },
                { field: "slovak", headerText: 'Slovak', width: 150 },
                { field: "slovene", headerText: 'Slovene', width: 150 },
                { field: "spanish", headerText: 'Spanish', width: 150 },
                { field: "swedish", headerText: 'Swedish', width: 150 },
                { field: "turkish", headerText: 'Turkish', width: 150 },
                { field: "ukrainian", headerText: 'Ukrainian', width: 150 }

            ], actionComplete: function (args) {
                $("#loading").hide();
                $("#customerspecifictranslationsgrid_searchbar").css("height", "20px").css("width", "500px");
                var obj = $("#customerspecifictranslationsgrid").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');
            },
            beforeBatchSave: function (args, e) {
                var gridObj = $("#customerspecifictranslationsgrid").data("ejGrid");
                var changed = args.batchChanges.changed;
                BatchSaveCustomerSpecificTranslation(changed)
            }
        });

        $('.e-pagercontainer').addClass("pull-left");
        $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
        $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');
    }

    function BatchSaveCustomerSpecificTranslation(changed) {
        $("#loading").show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "SaveCustomerSpecificTranslation",
            data: { changedList: JSON.stringify(changed) },
            success: function (data) {
                $("#loading").hide();
                fillCustomerSpecificTranslationList($('#CustomerSpecific').ejDropDownList("getSelectedValue"));
                if (data == "success")
                    validationTrue("Customer Specific Translation Saved Successfully");
                if (data == "error")
                    validationFalse("Customer Specific Translation Already Exist");
            }
        })
    }

    $(document).on("click", ".fa-clone", function () {
        $("#loading").show();
        var langdetailid = $(this).attr("id");
        var languagedata = $(this).data("english");
        if (languagedata.trim().length > 0) {
            dataFactory.copyTranslationFromCustomerSpecificToGeneral(langdetailid).then(function (resObj) {
                $("#loading").hide();
                if (resObj.data.error === undefined) {
                    validationTrue("Customer Specific Translation Copied Successfully");
                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });
        }
        else {
            $("#loading").hide();
            validationFalse("Tranlation Not Available In English Language");
        }
    });

    $scope.uITranslation = function () {
        $("#loading").show();

        fillcommonUITranslationList();
        $('.commonuitranslations').removeClass('hide').addClass('show');
        $('.specificuitranslations').removeClass('show').addClass('hide');
        $('.generaltranslations').removeClass('show').addClass('hide');
        $('.customerspecifictranslations').removeClass('show').addClass('hide');
    }

    $scope.commonUITranslation = function () {
        $("#loading").show();

        fillcommonUITranslationList();
        $('.commonuitranslations').removeClass('hide').addClass('show');
        $('.specificuitranslations').removeClass('show').addClass('hide');
        $('.generaltranslations').removeClass('show').addClass('hide');
        $('.customerspecifictranslations').removeClass('show').addClass('hide');
    }

    $scope.specificUITranslation = function () {

        $("#loading").show();

        fillspecificUITranslationList();
        $('.specificuitranslations').removeClass('hide').addClass('show');
        $('.commonuitranslations').removeClass('show').addClass('hide');
        $('.generaltranslations').removeClass('show').addClass('hide');
        $('.customerspecifictranslations').removeClass('show').addClass('hide');
    }


    function fillcommonUITranslationList() {

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetCommonUITranslation',
            success: function (data) {
                if (data.Error === "") {
                    var dataManager = ej.DataManager({
                        json: data.data,
                        batchUrl: "/Translator/SaveCommonUITranslation?data=" + data + "",
                        removeUrl: "/Grid/UrlDelete",
                        adaptor: "remoteSaveAdaptor",
                    });
                }
                fillcommonUITranslationGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }

    function fillcommonUITranslationGrid(data) {

        $("#loading").show();

        var docHeight = $(document).height();
        if (docHeight <= 750)
            docHeight = parseInt(((docHeight * 45) / 100), 10);
        else if (docHeight <= 1000)
            docHeight = parseInt(((docHeight * 60) / 100), 10);
        else
            docHeight = parseInt(((docHeight * 65) / 100), 10);

        $("#commonuitranslationsgrid").ejGrid({
            dataSource: ej.DataManager(data),
            minWidth: 600,
            isResponsive: true,
            enablelAltRow: true,
            allowTextWrap: true,
            allowSorting: true,
            allowFiltering: false,
            allowScrolling: true,
            enableHeaderHover: true,
            allowResizeToFit: true,
            editSettings: { allowEditing: true, allowCancel: true, editMode: ej.Grid.EditMode.Batch },
            allowPaging: true,
            scrollSettings: { height: parseInt(docHeight), frozenRows: 0 },
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            pageSettings: {
                pageSize: 25
            },
            queryCellInfo: "queryCellInfo",
            columns: [
                { field: "uITranslationDetailId", visible: false, isPrimaryKey: true },
                { field: "english", headerText: 'English', width: 150, allowEditing: false },
                { field: "arabic", headerText: 'Arabic', width: 150 },
                { field: "belarusian", headerText: 'Belarusian', width: 150 },
                { field: "bulgarian", headerText: 'Bulgarian', width: 150 },
                { field: "croatian", headerText: 'Croatian', width: 150 },
                { field: "czech", headerText: 'Czech', width: 150 },
                { field: "danish", headerText: 'Danish', width: 150 },
                { field: "dutch", headerText: 'Dutch', width: 150 },
                { field: "estonian", headerText: 'Estonian', width: 150 },
                { field: "finnish", headerText: 'Finnish', width: 150 },
                { field: "french", headerText: 'French', width: 150 },
                { field: "german", headerText: 'German', width: 150 },
                { field: "greek", headerText: 'Greek', width: 150 },
                { field: "hungarian", headerText: 'Hungarian', width: 150 },
                { field: "italian", headerText: 'Italian', width: 150 },
                { field: "latvian", headerText: 'Latvian', width: 150 },
                { field: "lithuanian", headerText: 'Lithuanian', width: 150 },
                { field: "macedonian", headerText: 'Macedonian', width: 150 },
                { field: "norwegian", headerText: 'Norwegian', width: 150 },
                { field: "polish", headerText: 'Polish', width: 150 },
                { field: "portuguese", headerText: 'Portuguese', width: 150 },
                { field: "romanian", headerText: 'Romanian', width: 150 },
                { field: "russian", headerText: 'Russian', width: 150 },
                { field: "slovak", headerText: 'Slovak', width: 150 },
                { field: "slovene", headerText: 'Slovene', width: 150 },
                { field: "spanish", headerText: 'Spanish', width: 150 },
                { field: "swedish", headerText: 'Swedish', width: 150 },
                { field: "turkish", headerText: 'Turkish', width: 150 },
                { field: "ukrainian", headerText: 'Ukrainian', width: 150 }
            ], actionComplete: function (args) {
                $("#loading").hide();
                $("#commonuitranslationsgrid_searchbar").css("height", "20px").css("width", "500px");

                var obj = $("#commonuitranslationsgrid").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');
            },
            beforeBatchSave: function (args, e) {
                var gridObj = $("#commonuitranslationsgrid").data("ejGrid");
                var changed = args.batchChanges.changed;
                BatchSaveCommonUITranslation(changed)
            }
        });

        $('.e-pagercontainer').addClass("pull-left");
        $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
        $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');
    }

    function BatchSaveCommonUITranslation(changed) {
        $("#loading").show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "SaveCommonUITranslation",
            data: { changedList: JSON.stringify(changed) },
            success: function (data) {
                $("#loading").hide();
                fillcommonUITranslationList();
                if (data == "success")
                    validationTrue("UI Translation Saved Successfully.");
                if (data == "error")
                    validationFalse("UI Translation Already Exist");
            }
        })
    }

    function fillspecificUITranslationList() {

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetSpecificUITranslation',
            success: function (data) {
                if (data.Error === "") {
                    var dataManager = ej.DataManager({
                        json: data.data,
                        batchUrl: "/Translator/SaveSpecificUITranslation?data=" + data + "",
                        removeUrl: "/Grid/UrlDelete",
                        adaptor: "remoteSaveAdaptor",
                    });
                }
                fillspecificUITranslationGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }

    function fillspecificUITranslationGrid(data) {

        $("#loading").show();

        var docHeight = $(document).height();
        if (docHeight <= 750)
            docHeight = parseInt(((docHeight * 45) / 100), 10);
        else if (docHeight <= 1000)
            docHeight = parseInt(((docHeight * 60) / 100), 10);
        else
            docHeight = parseInt(((docHeight * 65) / 100), 10);

        $("#specificuitranslationsgrid").ejGrid({
            dataSource: ej.DataManager(data),
            minWidth: 600,
            isResponsive: true,
            enablelAltRow: true,
            allowTextWrap: true,
            allowSorting: true,
            allowFiltering: false,
            allowScrolling: true,
            enableHeaderHover: true,
            allowResizeToFit: true,
            editSettings: { allowEditing: true, allowCancel: true, editMode: ej.Grid.EditMode.Batch },
            allowPaging: true,
            scrollSettings: { height: parseInt(docHeight), frozenRows: 0 },
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            pageSettings: {
                pageSize: 25
            },
            queryCellInfo: "queryCellInfo",
            columns: [
                { field: "uITranslationId", visible: false, isPrimaryKey: true },
                { field: "uITranslationDetailRefId", visible: false, isPrimaryKey: true },
                { field: "title", headerText: 'Name', width: 300 },
                { field: "english", headerText: 'English', width: 150 },
                { field: "arabic", headerText: 'Arabic', width: 150 },
                { field: "belarusian", headerText: 'Belarusian', width: 150 },
                { field: "bulgarian", headerText: 'Bulgarian', width: 150 },
                { field: "croatian", headerText: 'Croatian', width: 150 },
                { field: "czech", headerText: 'Czech', width: 150 },
                { field: "danish", headerText: 'Danish', width: 150 },
                { field: "dutch", headerText: 'Dutch', width: 150 },
                { field: "estonian", headerText: 'Estonian', width: 150 },
                { field: "finnish", headerText: 'Finnish', width: 150 },
                { field: "french", headerText: 'French', width: 150 },
                { field: "german", headerText: 'German', width: 150 },
                { field: "greek", headerText: 'Greek', width: 150 },
                { field: "hungarian", headerText: 'Hungarian', width: 150 },
                { field: "italian", headerText: 'Italian', width: 150 },
                { field: "latvian", headerText: 'Latvian', width: 150 },
                { field: "lithuanian", headerText: 'Lithuanian', width: 150 },
                { field: "macedonian", headerText: 'Macedonian', width: 150 },
                { field: "norwegian", headerText: 'Norwegian', width: 150 },
                { field: "polish", headerText: 'Polish', width: 150 },
                { field: "portuguese", headerText: 'Portuguese', width: 150 },
                { field: "romanian", headerText: 'Romanian', width: 150 },
                { field: "russian", headerText: 'Russian', width: 150 },
                { field: "slovak", headerText: 'Slovak', width: 150 },
                { field: "slovene", headerText: 'Slovene', width: 150 },
                { field: "spanish", headerText: 'Spanish', width: 150 },
                { field: "swedish", headerText: 'Swedish', width: 150 },
                { field: "turkish", headerText: 'Turkish', width: 150 },
                { field: "ukrainian", headerText: 'Ukrainian', width: 150 }
            ], actionComplete: function (args) {
                $("#loading").hide();
                $("#specificuitranslationsgrid_searchbar").css("height", "20px").css("width", "500px");

                var obj = $("#specificuitranslationsgrid").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');
            },
            beforeBatchSave: function (args, e) {
                var gridObj = $("#specificuitranslationsgrid").data("ejGrid");
                var changed = args.batchChanges.changed;
                BatchSaveSpecificUITranslation(changed)
            }
        });

        $('.e-pagercontainer').addClass("pull-left");
        $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
        $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');
    }

    function BatchSaveSpecificUITranslation(changed) {
        $("#loading").show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "SaveSpecificUITranslation",
            data: { changedList: JSON.stringify(changed) },
            success: function (data) {
                $("#loading").hide();
                fillspecificUITranslationList();
                if (data == "success")
                    validationTrue("UI Translation Saved Successfully.");
                if (data == "error")
                    validationFalse("UI Translation Already Exist");
            }
        })
    }

    $("#UploadGeneralTranslation").ejUploadbox({
        saveUrl: "UploadGeneralTranslations",
        extensionsAllow: ".xlsx",
        multipleFilesSelection: false,
        dialogText: { title: "Import Translation" },
        dialogPosition: { X: 200, Y: 5 },
        dialogAction: { drag: false },
        dialogAction: { closeOnComplete: true },
        buttonText: { browse: "Import Excel", upload: "Upload", cancel: "Cancel" },
        complete: function (args) {
            if (args.responseText == "") {
                validationTrue("General Translation Imported Successfully.");
            }
            else {
                validationFalse(args.responseText);
            }
            $("#loading").hide();
            fillGeneralTranslationList();
        },
        begin: function (args) {
            $("#loading").show();
        }
    });

    $("#UploadCustomerSpecificTranslation").ejUploadbox({
        saveUrl: "UploadCustomerSpecificTranslations",
        extensionsAllow: ".xlsx",
        multipleFilesSelection: false,
        dialogText: { title: "Import Translation" },
        dialogPosition: { X: 200, Y: 5 },
        dialogAction: { drag: false },
        dialogAction: { closeOnComplete: true },
        buttonText: { browse: "Import Excel", upload: "Upload", cancel: "Cancel" },
        complete: function (args) {
            if (args.responseText == "")
                validationTrue("Customer Specific Translation Imported Successfully.");
            else
                validationFalse(args.responseText);

            $("#loading").hide();
            fillCustomerSpecificTranslationList($('#CustomerSpecific').ejDropDownList("getSelectedValue"));
        },
        begin: function (args) {
            $("#loading").show();
        }
    });

    $("#UploadCommonUITranslation").ejUploadbox({
        saveUrl: "UploadCommonUITranslations",
        extensionsAllow: ".xlsx",
        multipleFilesSelection: false,
        dialogText: { title: "Import Translation" },
        dialogPosition: { X: 200, Y: 5 },
        dialogAction: { drag: false },
        dialogAction: { closeOnComplete: true },
        buttonText: { browse: "Import Excel", upload: "Upload", cancel: "Cancel" },
        complete: function (args) {
            if (args.responseText == "")
                validationTrue("Common UI Translation Imported Successfully.");
            else
                validationFalse(args.responseText);

            $("#loading").hide();
            fillcommonUITranslationList();
        },
        begin: function (args) {
            $("#loading").show();
        }
    });
    $("#UploadSpecificUITranslation").ejUploadbox({
        saveUrl: "UploadSpecificUITranslations",
        extensionsAllow: ".xlsx",
        multipleFilesSelection: false,
        dialogText: { title: "Import Translation" },
        dialogPosition: { X: 200, Y: 5 },
        dialogAction: { drag: false },
        dialogAction: { closeOnComplete: true },
        buttonText: { browse: "Import Excel", upload: "Upload", cancel: "Cancel" },
        complete: function (args) {
            if (args.responseText == "")
                validationTrue("Specific UI Translation Imported Successfully.");
            else
                validationFalse(args.responseText);

            $("#loading").hide();
            fillspecificUITranslationList();
        },
        begin: function (args) {
            $("#loading").show();
        }
    });

    $scope.GeneralExportTranslations = function () {
        window.open('/tx-creatis/Translator/ExportCentralTranslations?CustomerId=0', '_blank');
    }
    $scope.CustomerSpecificExportTranslations = function () {
        var customerId = $("#CustomerSpecific").val();

        window.open('/tx-creatis/Translator/ExportCentralTranslations?CustomerId=' + customerId, '_blank');
    }
    $scope.CommonUIExportTranslations = function () {
        window.open('/tx-creatis/Translator/ExportUITranslations?IsCommonUI=1', '_blank');
    }
    $scope.SpecificUIExportTranslations = function () {
        window.open('/tx-creatis/Translator/ExportUITranslations?IsCommonUI=0', '_blank');
    }
}]);

function CustomerSpecificDropDown(customers) {

    $('#CustomerSpecific').ejDropDownList({
        dataSource: customers,
        watermarkText: "Select Customer",
        fields: { text: "cust", value: 'customerId' },
        width: 260,
        showCheckbox: false,
        select: function (sender) {
            customerId = sender.value;

        },
        enableFilterSearch: true,
        enablePopupResize: true
    });

    $('#CustomerSpecific_popup_list_wrapper').addClass("syncdropdown");
}

function queryCellInfo(args) {
    $(args.cell).attr({
        "data-toggle": "tooltip",
        "data-container": "body",
        "title": args.data[args.column.field]
    });
}

﻿var customerId = "";
CREATIS.controller('HomeController', ['$scope', '$http', 'dataFactory', '$uibModal', '$window', function ($scope, $http, dataFactory, $uibModal, $window) {
    $scope.customer = "";
    $scope.recentActivities = "";
    var target;
    $scope.loading = true;

    $scope.layoutidforDelete = 0;
    $scope.customerforDelete = "";
    $scope.layoutNameforDelete = "";


    $scope.GoHome = function () {
        dataFactory.GoHomePage();
    };


    dataFactory.GetCustomer().then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.customer = resObj.data;
            CustomerDropDown(resObj.data);
            dataFactory.GetLoginCustomerId().then(function (resObj) {
                if (resObj.data.error === undefined) {
                    var roobj = $("#Customer").data("ejDropDownList");
                    roobj.setSelectedValue(resObj.data);
                }
            }, function () {
                $scope.error = "Some Error.";
            });
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetRecentActivities().then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.loading = false;
            LoadRecentActivities($scope, dataFactory, resObj.data);
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    $scope.CustomerOverview = function () {
        if (customerId == undefined || customerId == "") {
            validationFalse("Please Select Customer");
        } else {
            //alert($scope.DropCust);
            dataFactory.RedirectToCustomerOverview(customerId);
        }
    };

    $(document).on("click", ".fa-minus-circle", function () {
        var layoutid = $(this).attr("id");
        $scope.loading = true;
        dataFactory.RemoveRecentActivities(layoutid).then(function (resObj) {
            if (resObj.data.error === undefined) {
                dataFactory.GetRecentActivities().then(function (response) {
                    if (response.data.error === undefined) {
                        LoadRecentActivities($scope, dataFactory, response.data);
                        validationTrue("Layout Removed From Your Recent Activities");
                        $scope.loading = false;
                    }
                    else {
                        $scope.loading = false;
                        validationFalse(response.data.error);
                    }
                }, function () {
                    $scope.error = "Some Error.";
                });
            }
            else
                validationFalse(resObj.data.error);
        }, function () {
            $scope.error = "Some Error.";
        });

    });

    $scope.deletelayout = function () {
        $('.divrecentactivities').removeClass('show').addClass('hide');
        $('#RecentActivitesModal').modal('hide');
        $scope.loading = true;
        dataFactory.DeleteLayout($scope.layoutidforDelete).then(function (resObj) {
            if (resObj.data.error === undefined) {
                dataFactory.GetRecentActivities().then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        validationTrue("Layout Deleted Successfully");
                        LoadRecentActivities($scope, dataFactory, resObj.data);
                    }
                    else
                        validationFalse(resObj.data.error);
                }, function () {
                    $scope.error = "Some Error.";
                });
            }
            else {
                $scope.loading = false;
                validationFalse(resObj.data.error);
            }

        }, function () {
            $scope.error = "Some Error.";
        });
    }

    $scope.cancel = function () {
        $('.divrecentactivities').removeClass('show').addClass('hide');
        $('#RecentActivitesModal').modal('hide');
    };

    function GotoDashboard(dataFactory, layoutid) { dataFactory.GetRecentActivitiestoDashboard(layoutid); }
}]);

function LoadRecentActivities($scope, dataFactory, RecentActivitie) {
    var gridheight;
    var x = screen.height;
    //alert(x);
    if (x == 1024) {
        gridheight = 480;
    } else

        if (x == 768) {
            gridheight = 210;
        }
        else { gridheight = 520; }
    $("#RecentActivities").ejGrid({
        dataSource: RecentActivitie,
        isResponsive: true,
        enablelAltRow: true,
        allowTextWrap: true,
        pageSettings: {
            pageSize: 15
        },
        //allowResizing: true,
        //resizeSettings: { resizeMode: "nextcolumn" },
        scrollSettings: {
            // width: "auto",
            height: gridheight
        },
        allowPaging: true,
        allowSorting: false,
        allowFiltering: false,
        enableHeaderHover: true,
        allowResizeToFit: true,
        editSettings: {
            allowEditing: true,
            allowAdding: true,
            allowDeleting: true,
            allowEditOnDblClick: false
        },
        toolbarSettings: {
            showToolbar: true,
            toolbarItems: [ej.Grid.ToolBarItems.Search]
        },
        allowScrolling: false,
        columns: [
            { field: "source", headerText: "Customer", width: 30 + "%" },
            { field: "environment", headerText: "Type", width: 10 + "%" },
            { field: "name", headerText: "Name", width: 20 + "%" },
            { field: "createdOn", headerText: "Created Date", format: "{0:dd/MM/yyyy}", type: "date", width: 10 + "%" },
            { field: "status", headerText: "Status", width: 10 + "%" },
            { field: "updatedOn", headerText: "Date Last Status", format: "{0:dd/MM/yyyy}", type: "date", width: 10 + "%" },
            { headerText: "Remove", template: "#Delete", textAlign: "center", width: 10 + "%" },
            {
                field: "layoutRefId",
                headerText: "PK",
                visible: false
            }

        ],
        recordDoubleClick: function (args) {
            dataFactory.GetRecentActivitiestoDashboard(args.data.layoutRefId);
        },
        querycellinfo: function (args) {
            if (args.data.status == "In Progress") {

            }
        },
        rowSelected: function (args) {
            var selectedrowindex = this.selectedRowsIndexes;  // get selected row indexes             
            var selectedrecords = this.getSelectedRecords();  // get selected records   
        },
        actionComplete: function (args) {
            $("#RecentActivities_searchbar").css("height", "20px").css("width", "500px");
            $scope.loading = false;
        },
        endDelete: function (args) {
            if (args.data.status == "In Progress") {
                $scope.loading = true;
                $("#RecentActivities_delete").addClass("e-disable");
                dataFactory.DeleteLayout(args.data.layoutRefId).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        validationTrue("Layout Deleted Successfully");
                    }
                    else
                        validationFalse(resObj.data.error);
                    $scope.loading = false;
                }, function () {
                    $scope.error = "Some Error.";
                });
            }
        }
    });
    //$(".e-gridcontent").css('border-radius', '5px');
    $('.e-pagercontainer').html('<span>* Last 15 Activities</span>');
    $('.e-pagercontainer').addClass("pull-left");
    $('.e-pagercontainer').css("border", "0px");
    //$('.e-pagercontainer').hide();
    //$('.e-content').css('width', 'auto');e-gridheader e-textover e-scrollcss
    $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
    $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');

}

function CustomerDropDown(customers) {

    $('#Customer').ejDropDownList({
        dataSource: customers,
        watermarkText: "Select Customer",
        fields: { text: "cust", value: 'customerId' },
        width: 260,
        showCheckbox: false,
        select: function (sender) {
            customerId = sender.value;
        },
        enableFilterSearch: true,
        enablePopupResize: true,
        allowVirtualScrolling: true,
            virtualScrollMode: ej.VirtualScrollMode.Normal
    });

    $('#Customer_popup_list_wrapper').addClass("syncdropdown");
    //$('#Customer_popup_list_wrapper').css('width', '260px !important');
    //$('.e-ddl-popup .e-box .e-widget .e-popup .e-resizable').css('height', '397px !important');
    //$('.e-ddl-popup .e-box .e-widget .e-popup .e-resizable').css('width', '260px !important');
}





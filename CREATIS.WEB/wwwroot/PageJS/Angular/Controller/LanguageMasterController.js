﻿CREATIS.controller('LanguageMasterController', ['$scope', 'dataFactory', '$uibModal', '$http', function ($scope, dataFactory, $uibModal, $http, modalService) {
    //LanguageMaster
    $scope.loading = true;
    $scope.active = [{ text: "Active", value: "Active" }, { text: "InActive", value: " InActive" }];
    fillLanList();
    function fillLanList() {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetLanguageMaster',
            success: function (data) {
                if (data.Error === "") {
                    var dataManager = ej.DataManager({
                        json: data.data,
                        batchUrl: "/LanguageMaster/SaveLanguageMaster?data=" + data + "",
                        removeUrl: "/Grid/UrlDelete",
                        adaptor: "remoteSaveAdaptor",
                    });
                }
                fillGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }
    function fillGrid(data) {
        $("#grdLang").ejGrid({
            dataSource: data,
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Add, ej.Grid.ToolBarItems.Update,  ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            editSettings: { allowEditing: true, allowAdding: true, allowDeleting: false, allowCancel: true, editMode: ej.Grid.EditMode.Batch },
            allowPaging: true,
           
            columns: [
                { field: "languageId", visible: false, isPrimaryKey: true },
                { field: "languageDescription", headerText: 'Language', width: "80%", validationRules: { required: true, maxlength: 25 } },
                { field: "languageCode", headerText: 'Language Code', width: "80%", validationRules: { required: true, maxlength: 25 } },
                { field: "active", headerText: 'Status', editType: ej.Grid.EditingType.Dropdown, dataSource: $scope.active, width: "80%", validationRules: { required: true }  },
            ], actionComplete: function (args) {
                $("grdLang_searchbar").css("height", "20px").css("width", "500px");
                $scope.loading = false;
            },
            beforeBatchSave: function (args, e) {
                var gridObj = $("#grdLang").data("ejGrid");
                var updated = args.batchChanges.changed;
                var added = args.batchChanges.added;
                $.each(updated, function (index, value) {
                    if (value.active == "Active")
                        value.is_Active = 1;
                    else
                        value.is_Active = 0;
                    value.createdBy = 4;
                    var d = new Date();
                    value.modifiedOn = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                });
                $.each(added, function (index, value) {
                    if (value.active == "Active")
                        value.is_Active = 1;
                    else
                        value.is_Active = 0;
                    value.createdBy = 4;
                    var d = new Date();
                    value.createdOn = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                });
                data = gridObj.getBatchChanges();
                BatchSaveLanguageMaster(data)
            }, actionComplete: function (args) {
                $("#grdLang_searchbar").css("height", "20px").css("width", "500px");
            }
        });
        $scope.GoLanguageMaster = function () {
            dataFactory.GoLanguageMaster();
        };
        $('.e-pagercontainer').addClass("pull-left");
    }
    function BatchSaveLanguageMaster(data) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "SaveLanguageMaster",
            data: data,
            success: function (data) {
                fillLanList();
                if (data == "success")
                    validationTrue("Language Saved Successfully.");
                if (data == "error")
                    validationFalse("Error.");
             
            }
        })
    }
}]);
﻿CREATIS.controller('InfoMessageIconTypeController', ['$scope', 'dataFactory', '$uibModal', '$http', function ($scope, dataFactory, $uibModal, $http, modalService) {
    //InfoMessageIconType
    $scope.loading = true;
    $scope.active = [{ text: "Active", value: "Active" }, { text: "InActive", value: "InActive" }];

    fillInfoMessageIconTypeList();

    function fillInfoMessageIconTypeList() {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetInfoMessageIconType',
            success: function (data) {
                if (data.Error === "") {
                    var dataManager = ej.DataManager({
                        json: data.data,
                        batchUrl: "/InfoMessageIconType/SaveInfoMessageIconType?data=" + data + "",
                        removeUrl: "/Grid/UrlDelete",
                        adaptor: "remoteSaveAdaptor",
                    });
                }
                fillGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }

    function fillGrid(data) {
        $("#grdInfoMessageIconType").ejGrid({
            dataSource: data,

            editSettings: { allowEditing: true, allowAdding: true, allowDeleting: false, allowCancel: true, editMode: ej.Grid.EditMode.Batch },
            allowPaging: true,
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Add, ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            columns: [
                { field: "infoMessageIconTypeId", visible: false, isPrimaryKey: true },
                { field: "infoMessageIconTypeName", headerText: 'Icon Type', width: "60%", validationRules: { required: true, maxlength: 25 } },
                { field: "active", headerText: 'Status', editType: ej.Grid.EditingType.Dropdown, dataSource: $scope.active, width: "80%", validationRules: { required: true } },
            ], actionComplete: function (args) {
                $scope.loading = false;
                $("#grdInfoMessageIconType_searchbar").css("height", "20px").css("width", "500px");

                var obj = $("#grdInfoMessageIconType").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');
            },
            beforeBatchSave: function (args, e) {
                var gridObj = $("#grdInfoMessageIconType").data("ejGrid");
                var updated = args.batchChanges.changed;
                var added = args.batchChanges.added;
                $.each(updated, function (index, value) {
                    debugger;
                    if (value.active == "Active")
                        value.isActive = true;
                    else if (value.active == "InActive")
                        value.isActive = false;
                    else {
                        validationFalse("Choose the status");
                        args.cancel = true;
                    }
                    var d = new Date();
                    value.modifiedOn = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                });
                $.each(added, function (index, value) {
                    debugger;
                    if (value.active == "Active")
                        value.isActive = true;
                    else if (value.active == "InActive")
                        value.isActive = false;
                    var d = new Date();
                    value.createdOn = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                });
                data = gridObj.getBatchChanges();
                BatchSaveInfoMessageIconType(data)
            }
        });
        $scope.GoInfoMessageIconType = function () {
            dataFactory.GoInfoMessageIconType();
        };

        $('.e-pagercontainer').addClass("pull-left");
        $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
        $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');
    }

    function BatchSaveInfoMessageIconType(data) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "SaveInfoMessageIconType",
            data: data,
            success: function (data) {
                fillInfoMessageIconTypeList();
                if (data == "success")
                    validationTrue("Icon Type Saved Successfully.");
                if (data == "error")
                    validationFalse("Icon Type Already Exist");
            }
        })
    }
}]);
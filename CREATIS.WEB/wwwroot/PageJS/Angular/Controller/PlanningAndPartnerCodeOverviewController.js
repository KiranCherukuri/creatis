﻿var jsdatafactory;
CREATIS.controller('PlanningAndPartnerCodeOverviewController', ['$scope', 'dataFactory', '$uibModal', '$location', '$http', function ($scope, dataFactory, $uibModal, $location, $http, modalService) {
    jsdatafactory = dataFactory;
    $scope.loading = true;

    var absUrl = $location.absUrl();
    urldata = absUrl.toString();

    var layoutId = urldata.split('=')[1];
    if (layoutId.toString().indexOf("#") >= 0)
        layoutId = layoutId.split('#')[0];


    $scope.layoutId = layoutId;

    $(".loading").show();

    $scope.Dashboard = function () {
        dataFactory.GetRecentActivitiestoDashboard($scope.layoutId);
    }

    $scope.GoHome = function () {
        dataFactory.GoHomePage();
    };

    $scope.RedirecttoScreenLayout = function () {
        dataFactory.RedirecttoScreenLayout($scope.layoutId);
    }

    $scope.RedirecttoShowLayout = function () {
        dataFactory.RedirecttoShowLayout($scope.layoutId);
    }

    $scope.RedirecttoTranslation = function () {
        dataFactory.RedirecttoTranslation($scope.layoutId);
    }

    dataFactory.GetAlertCopiedLayout($scope.layoutId).then(function (resObj) {

        var result = resObj.data.split("ƒ");

        if (result[0] == "SL")
            $('#GOScreenLayout').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GOScreenLayout').removeClass('btn-warning').addClass('btn-primary');

        if (result[1] == "TR")
            $('#GoTranslation').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GoTranslation').removeClass('btn-warning').addClass('btn-primary');

    });

    fillgridPlanningandPartnerCodeOverviewList(0, $scope.layoutId);

    function fillgridPlanningandPartnerCodeOverviewList(customerId, layuotId) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            data: { customerRefId: customerId, layoutRefId: layuotId },
            url: 'GetPlanningAndPartnerCodeOverview',
            success: function (data) {
                $(".loading").hide();
                fillGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }
    function fillGrid(data) {

        $("#gridPlanningandPartnerCodeOverview").ejGrid({
            dataSource: ej.DataManager(data),
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Search] },
            allowPaging: true,
            allowSorting: true,
            queryCellInfo: "queryCellInfo",
            pageSettings: {
                pageSize: 25
            },
            columns: [
                { field: "questionId", isPrimaryKey: true, visible: false },
                { field: "activitiesRegistrationName", headerText: 'Activity / Registration', width: '10%' },
                { field: "item", headerText: 'Item', width: '10%' },
                { field: "location", headerText: 'Location', width :'30%' },
                { field: "planningLevel", headerText: 'Planning Level', width: '10%' },
                { field: "extraInfo", headerText: 'Extra Info', width: '10%' },
                { field: "pallets", headerText: 'Pallets', width: '10%'},
                { field: "anomaly", headerText: 'Anomaly', width: '10%' },
                { field: "partnerCode", headerText: 'Partner Code', width: '10%' },
            ], actionComplete: function (args) {

                $("#gridPlanningandPartnerCodeOverview_searchbar").css("height", "20px").css("width", "500px");
                $scope.loading = false;
            },
        });

        $('.e-pagercontainer').addClass("pull-left");
    }

}]);

function queryCellInfo(args) {

    $(args.cell).attr({
        "data-toggle": "tooltip",
        "data-container": "body",
        "title": args.data[args.column.field]
    });
}

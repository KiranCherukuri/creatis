﻿var workingcode = "";
var Planning = "";
var PlanningOverview = "";
var Specific = "";
var Pto = "";
var Interby = "";
var initialworkingcode = [];
var initialworkingcodebytxsmart = [];
var initialPlanning = [];
var initialPlanningwithoutreceive = [];
var initialPlanningwithreceive = [];

var initialPlanningAll = [];

var initialPlanningOverview = [];

var initialPto = [];
var initialInterby = [];
var LayoutID = "";
var oldpTMData = "";
var InsertPTMData = "";
var DeletePTMData = "";
var InsertPTSData = "";
var DeletePTSData = "";
var updateobc = 0;
var countoftransporttype = [];
var obcassignedtypeid = '';
var layoutOrgin = '';
var gridheighton = '';
var layoutStatusId = 1;
var requestLayoutId = 7; // 7  - request type - 'Support'
var CopyLayoutPopupCount = 0;
var counttransporttype = 0;
var planningtypeloopcount = 0;
var transporttypeloopcount = 0;


if (screen.width == 1920) {
    gridheighton = 288;
}
if (screen.width == 1366) {
    gridheighton = 150;
}
CREATIS.controller('DashboardController', ['$scope', '$http', 'dataFactory', '$uibModal', '$location', '$rootScope', function ($scope, $http, dataFactory, $uibModal, $location, $rootScope) {

    $scope.loading = true;
    var absUrl = $location.absUrl();
    urldata = absUrl.toString();
    $scope.LayoutID = urldata.split('=')[1];
    LayoutID = $scope.LayoutID;
    $scope.LayoutTitle = "";
    $scope.OBCTypeMaster = "";
    $scope.OBCTypeAssignedValues = "";
    //$scope.UsedLanguages = "";
    $rootScope.UsedLanguages = [];
    $rootScope.TransportTypes = "";
    $rootScope.Language = "";
    $scope.PlanningTypeMaster = "";
    $rootScope.Allowancestype = [];
    $scope.PlanningSpecifics = "";
    $scope.obcvalues = 0;
    $scope.obctypeids = 0;
    $scope.AssignedPlanningType = "";
    $rootScope.TransportType = "";
    $scope.Customerid;


    $rootScope.DashboardTransportId = 0;
    $rootScope.DashboardUsedLanguageId = 0;
    $rootScope.DashboardAllowanceId = 0;
    $rootScope.DashboardDocumentTypeFlexId = 0;
    $scope.DefaultLanguageName = "";
    $scope.rolesAccessList = [];
    $rootScope.AllowancesMast = "";
    $rootScope.AllowanceCodes = "";
    $rootScope.AllowancesMastId = 0;
    $scope.rolesAccessList = [];
    $scope.layoutStatusAccessList = [];
    $scope.roleWithLayoutStatusAccessList = [];
    $scope.specificvalue = "";

    $scope.deletedocumenttypeflex = function (param) {

        $rootScope.DashboardDocumentTypeFlexId = param;

        var modalInstance = $uibModal.open({
            templateUrl: 'DocumentTypeFlexDelete.html',
            controller: 'DocumentTypeFlexDeleteCtl',
            scope: $scope,
            backdrop: false
        });


    };

    $scope.deleteusedlan = function (param) {

        $rootScope.DashboardUsedLanguageId = param;


        var modalInstance = $uibModal.open({
            templateUrl: 'UsedLanguageDelete.html',
            controller: 'UsedLanguageDeleteCtl',
            scope: $scope,
            backdrop: false
        });


    };

    $scope.panelcollapse = function (param) {
        if (screen.width == 1920) {
            if ($('#WagetPanel').hasClass('in')) { gridheighton = 588; }
            else if ($('#WagetPanel').hasClass('collapse')) { gridheighton = 288; }
        }
        if (screen.width == 1366) {
            if ($('#WagetPanel').hasClass('in')) { gridheighton = 328; }
            else if ($('#WagetPanel').hasClass('collapse')) {
                gridheighton = 150;
            }
        }
        GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
    };

    $scope.editusedlan = function (param1, param2) {
        $scope.loading = true;
        dataFactory.SetDefaultMainLanguage(LayoutID, param1).then(function (resObj) {
            if (resObj.data.error === undefined) {
                GetUsedLanguage($scope, dataFactory, LayoutID, $rootScope);
                validationTrue(param2 + " Changed as a Main Language");
                $scope.DefaultLanguageName = param2;
                GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
            }
            else
                validationFalse(resObj.data.error);
        }, function () {
            $scope.error = "Some Error.";
        });
    };

    dataFactory.GetLayoutorgin(LayoutID).then(function (resObj) {
        layoutOrgin = JSON.stringify(resObj.data);
        $scope.loading = false;
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetRolesAccessList().then(function (resObj) {
        $scope.rolesAccessList = resObj.data;
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetLayoutStatusAccessList(LayoutID).then(function (resObj) {
        $scope.layoutStatusAccessList = resObj.data;
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetRoleWithLayoutStatusAccessList(LayoutID).then(function (resObj) {
        $scope.roleWithLayoutStatusAccessList = resObj.data;
    }, function () {
        $scope.error = "Some Error.";
    });

    GetPlanningOverview($scope, dataFactory, $scope.LayoutID);

    dataFactory.GetLayoutStatus(1).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.LayoutRequestType = resObj.data;
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetLayoutStatus(0).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.LayoutStatus = resObj.data;

        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetDefaultLanguage(LayoutID).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.DefaultLanguageName = resObj.data;
        }
    });

    $scope.goToPlanningCodes = function () {
        window.open("/tx-creatis/PlanningAndPartnerCodeOverview/Index?layoutId=" + $scope.LayoutID, '_self');
    };

    $scope.goToSavedValues = function () {
        window.open("/tx-creatis/SavedValuesOverview/Index?layoutId=" + $scope.LayoutID, '_self');
    };

    $scope.transporttypedelete = function (param) {
        $rootScope.DashboardTransportId = param;


        var modalInstance = $uibModal.open({
            templateUrl: 'TransportDelete.html',
            controller: 'TransportDeleteCtl',
            scope: $scope,
            backdrop: false
        });


    };

    $scope.allowancedelete = function (param) {

        $rootScope.DashboardAllowanceId = param;

        var modalInstance = $uibModal.open({
            templateUrl: 'AllowanceDelete.html',
            controller: 'AllowanceDeleteCtl',
            scope: $scope,
            backdrop: false
        });

    };

    $scope.UpdateLayoutStatus = function () {
        if (($scope.rolesAccessList.indexOf("DULB") !== -1 && $scope.layoutStatusAccessList.indexOf("DULB") !== -1) || ($scope.roleWithLayoutStatusAccessList.indexOf("DULB") !== -1)) {

            $scope.layoutStatusId = "";
            var modalInstance = $uibModal.open({
                templateUrl: 'LayoutNote.html',
                controller: 'LayoutNotesetCtl',
                scope: $scope,
                backdrop: false
            });

            var layid = $scope.LayoutID;
            var laystat = layoutStatusId;

            modalInstance.result.then(function (obj) {
            }, function (obj) { });

            setTimeout(function () {
                $('#dpLayoutStatus').val("");
                $('#dpLayoutStatus').focus();
            }, 300);
        }
        else
            validationFalse("Access Denied");
    };

    $scope.RequestLayout = function () {

        if (($scope.rolesAccessList.indexOf("DRB") !== -1 && $scope.layoutStatusAccessList.indexOf("DRB") !== -1) || ($scope.roleWithLayoutStatusAccessList.indexOf("DRB") !== -1)) {
            $scope.requestTypeId = "";

            var modalInstance = $uibModal.open({
                templateUrl: 'RequestLayout.html',
                controller: 'RequestLayoutCtl',
                scope: $scope,
                backdrop: false
            });

            var layid = $scope.LayoutID;
            var laystat = requestLayoutId;

            modalInstance.result.then(function (obj) {
            }, function (obj) { });

            setTimeout(function () {
                $('#dpRequestType').val("");
                $('#dpRequestType').focus();
            }, 300);
        }
        else
            validationFalse("Access Denied");
    };

    $scope.TXDesignConvertion = function () {

        if (($scope.rolesAccessList.indexOf("DXMLCB") !== -1 && $scope.layoutStatusAccessList.indexOf("DXMLCB") !== -1) || ($scope.roleWithLayoutStatusAccessList.indexOf("DXMLCB") !== -1)) {
            $scope.requestTypeId = "";

            var modalInstance = $uibModal.open({
                templateUrl: 'TXDesignConvertion.html',
                controller: 'TXDesignConvertionCtl',
                scope: $scope,
                backdrop: false
            });

            setTimeout(function () {
                $("#divTXDesignConvertionStatus").empty();
                $("#StartTXDesignConvertion").prop("disabled", false);
                $("#GetTXDesignConvertionStatus").prop("disabled", true);
                $("#GetTXDesignConvertionStatus").click();
            }, 100);

        }
        else
            validationFalse("Access Denied");
    };

    $scope.LayoutNotes = function () {

        if (($scope.rolesAccessList.indexOf("DLNB") !== -1 && $scope.layoutStatusAccessList.indexOf("DLNB") !== -1) || ($scope.roleWithLayoutStatusAccessList.indexOf("DLNB") !== -1)) {
            var modalInstance = $uibModal.open({
                templateUrl: 'DisplayLayoutNode.html',
                controller: 'SaveNotesetCtl',
                scope: $scope,
                backdrop: false
            });



            var layid = $scope.LayoutID;
            var laystat = layoutStatusId;

            modalInstance.result.then(function (obj) {
            }, function (obj) { });

            setTimeout(function () {
                $('#txtDisplayLayoutNote').val("");
                $('#hidLayoutNoteId').val(0);
                $('#txtDisplayLayoutNote').focus();

                $('#editLayoutNotes').hide();
                $('#newLayoutNotes').hide();
                $('#saveLayoutNotes').show();
                $('#cancelLayoutNotes').show();
            }, 300);
        }
        else
            validationFalse("Access Denied");

    };

    $scope.transporttypeedit = function (transportid, transportname) {
        //
        var modalInstance = $uibModal.open({
            templateUrl: 'AddTransPortType.html',
            controller: 'TransPortTypeCtl',
            scope: $scope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) {

        }, function (obj) { });

        $rootScope.TransportTypeId = transportid;
        $rootScope.TransportType = transportname;

        setTimeout(function () {
            $('#txtTransportType').focus();
        }, 500);

    };

    $scope.allowanceedit = function (allowancemasid, allowancemas, allowancemascode) {

        var modalInstance = $uibModal.open({
            templateUrl: 'Allowance.html',
            controller: 'AllowanceCtl',
            scope: $scope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) { }, function (obj) { });

        $rootScope.AllowancesMast = allowancemas;
        $rootScope.AllowanceCodes = allowancemascode;
        $rootScope.AllowancesMastId = allowancemasid;

        setTimeout(function () {
            $('#txtAllowances').focus();
        }, 300);
    };

    dataFactory.PanelTitle($scope.LayoutID).then(function (resObj) {
        if (resObj.data.error === undefined) {
            var layoutdetails = resObj.data.split('[');
            $scope.LayoutTitle = layoutdetails[0];
            $scope.Customerid = layoutdetails[1];
        }
        else
            validationFalse(resObj.data.error);
        $scope.loading = false;
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetPTO().then(function (resObj) {
        if (resObj.data.error === undefined) {
            initialPto = [];
            Pto = resObj.data;
            for (i = 0; i < Pto.length; i++) {
                var Pt = {
                    "text": Pto[i].ptoDescription,
                    "value": Pto[i].ptoDescription,
                    "id": Pto[i].ptoId
                };
                initialPto.push(Pt);
            }
            $scope.loading = false;
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetInterruptibleBy().then(function (resObj) {
        if (resObj.data.error === undefined) {
            initialInterby = [];
            Interby = resObj.data;
            for (i = 0; i < Interby.length; i++) {
                var Interb = {
                    "text": Interby[i].interruptibleByDescription,
                    "value": Interby[i].interruptibleByDescription,
                    "id": Interby[i].interruptibleById
                };
                initialInterby.push(Interb);
            }
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    $scope.Redirecttocustomer = function () {
        dataFactory.RedirectToCustomerOverview($scope.Customerid);
    }

    $scope.RedirecttoScreenLayout = function () {
        dataFactory.RedirecttoScreenLayout($scope.LayoutID);
    }

    $scope.RedirecttoShowLayout = function () {
        dataFactory.RedirecttoShowLayout($scope.LayoutID);
    }

    $scope.RedirecttoTranslation = function () {
        dataFactory.RedirecttoTranslation($scope.LayoutID);
    }

    GetGetOBCTypeMaster($scope, dataFactory, $rootScope);

    GetAssignedTransportTypes($scope, dataFactory, $scope.LayoutID, $rootScope);

    dataFactory.GetRemainingLanguages($scope.LayoutID).then(function (resObj) {
        $rootScope.Language = resObj.data;
        GetUsedLanguage($scope, dataFactory, $scope.LayoutID, $rootScope);
        $scope.loading = false;
    }, function () {
        $scope.error = "Some Error.";
    });

    GetPlanningTypeMaster($scope, dataFactory);




    GetDocumentTypeFlex($scope, dataFactory, $scope.LayoutID, $rootScope);

    $scope.SaveOBCTypes = function () {
        $scope.loading = true;
        if (updateobc !== 0) {
            dataFactory.UpdateOBCTypes($scope.obctypeids, $scope.OBCMvalue, $scope.LayoutID).then(function (resObj) {
                if (resObj.data.error === undefined) {

                    if (parseInt($scope.OBCMvalue, 10) == 3) {
                        $("input[name='PlanningType_3']:checkbox").prop('checked', true);
                        $("input[name='PlanningType_4']:checkbox").prop('checked', true);
                        $("input[name='PlanningType_6']:checkbox").prop('checked', true);
                        $("input[name='PlanningType_7']:checkbox").prop('checked', true);

                        $("input[name='PlanningType_3']").prop("disabled", true);
                        $("input[name='PlanningType_4']").prop("disabled", true);
                        $("input[name='PlanningType_6']").prop("disabled", true);
                        $("input[name='PlanningType_7']").prop("disabled", true);

                        $("input[name='PlanningSpec_1']").prop("disabled", true);
                        $("input[name='PlanningSpec_2']").prop("disabled", true);

                        $("#divtransp").addClass("disabled");
                        $("#divtransp").addClass("disabled");


                        InsertPTMData = [];
                        $('#divPlan input:checked').each(function () {
                            InsertPTMData.push(this.value);
                        });

                        dataFactory.SavePlanningType($scope.LayoutID, InsertPTMData).then(function (resObj) {
                        }, function () {
                            $scope.error = "Some Error.";
                        });

                        gridLoadFunction();
                        validationTrue("OBC Type Updated Successfully");
                    }
                    else if (parseInt($scope.OBCMvalue, 10) != 3) {
                        $("#divtransp").removeClass("disabled");
                        $("#divtransp").removeClass("divAllow");

                        $("input[name='PlanningType_3']:checkbox").prop('checked', false);
                        $("input[name='PlanningType_4']:checkbox").prop('checked', false);
                        $("input[name='PlanningType_6']:checkbox").prop('checked', false);
                        $("input[name='PlanningType_7']:checkbox").prop('checked', false);

                        $("input[name='PlanningType_1']").prop("disabled", false);
                        $("input[name='PlanningType_3']").prop("disabled", false);
                        $("input[name='PlanningType_4']").prop("disabled", true);
                        $("input[name='PlanningType_6']").prop("disabled", true);
                        $("input[name='PlanningType_7']").prop("disabled", true);

                        $("input[name='PlanningSpec_1']").prop("disabled", false);
                        $("input[name='PlanningSpec_2']").prop("disabled", false);

                        InsertPTMData = [];
                        $('#divPlan input:checked').each(function () {
                            InsertPTMData.push(this.value);
                        });

                        dataFactory.SavePlanningType($scope.LayoutID, InsertPTMData).then(function (resObj) {
                            gridLoadFunction();
                            validationTrue("OBC Type Updated Successfully");
                        }, function () {
                            $scope.error = "Some Error.";
                        });
                    }
                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });
        } else {
            dataFactory.SaveOBCType($scope.LayoutID, $scope.OBCMvalue).then(function (resObj) {
                if (resObj.data.error === undefined) {

                    if (parseInt($scope.OBCMvalue, 10) == 3) {
                        $("input[name='PlanningType_3']:checkbox").prop('checked', true);
                        $("input[name='PlanningType_4']:checkbox").prop('checked', true);
                        $("input[name='PlanningType_6']:checkbox").prop('checked', true);
                        $("input[name='PlanningType_7']:checkbox").prop('checked', true);

                        $("input[name='PlanningType_1']").prop("disabled", true);
                        $("input[name='PlanningType_3']").prop("disabled", true);
                        $("input[name='PlanningType_4']").prop("disabled", true);
                        $("input[name='PlanningType_6']").prop("disabled", true);
                        $("input[name='PlanningType_7']").prop("disabled", true);

                        $("input[name='PlanningSpec_1']").prop("disabled", true);
                        $("input[name='PlanningSpec_2']").prop("disabled", true);

                        $("#divtransp").addClass("disabled");
                        $("#divtransp").addClass("disabled");


                        InsertPTMData = [];
                        $('#divPlan input:checked').each(function () {
                            InsertPTMData.push(this.value);
                        });

                        dataFactory.SavePlanningType($scope.LayoutID, InsertPTMData).then(function (resObj) {
                            gridLoadFunction();
                            validationTrue("OBC Type Saved Successfully");
                        }, function () {
                            $scope.error = "Some Error.";
                        });
                    }
                    else if (parseInt($scope.OBCMvalue, 10) == 4) {
                        $scope.loading = false;
                        validationTrue("OBC Type Saved Successfully");
                    }
                    else {
                        gridLoadFunction();
                        validationTrue("OBC Type Saved Successfully");
                    }
                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });
        }
    };

    var gridLoadFunction = function () {
        GetAllowancesMaster($scope, dataFactory, $scope.LayoutID, $rootScope);
        GetAssignedOBCTypes($scope, dataFactory, $rootScope);
        GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
    }

    $scope.GetPlanningSpec = function (event) {

        $scope.loading = true;

        if ($("input[name='PlanningType_1']").is(":checked")) {
            $("input[name='PlanningType_3']:checkbox").prop('checked', false);
            $("input[name='PlanningType_4']:checkbox").prop('checked', false);
            $("input[name='PlanningType_6']:checkbox").prop('checked', false);
            $("input[name='PlanningType_7']:checkbox").prop('checked', false);

            $("input[name='PlanningType_3']").prop("disabled", true);
            $("input[name='PlanningType_4']").prop("disabled", true);
            $("input[name='PlanningType_6']").prop("disabled", true);
            $("input[name='PlanningType_7']").prop("disabled", true);

        } else {
            $("input[name='PlanningType_3']").prop("disabled", false);
            $("input[name='PlanningType_4']").prop("disabled", false);
            $("input[name='PlanningType_6']").prop("disabled", true);
            $("input[name='PlanningType_7']").prop("disabled", true);
        }

        if ($("input[name='PlanningType_3']").is(":checked")) {
            $("input[name='PlanningType_4']:checkbox").prop('checked', true);
            $("input[name='PlanningType_4']").prop("disabled", true);
            $("input[name='PlanningType_1']").prop("disabled", true);
        }
        else if (!($("input[name='PlanningType_1']").is(":checked"))) {
            $("input[name='PlanningType_4']").prop("disabled", false);
        }

        if ($("input[name='PlanningType_4']").is(":checked")) {
            $("input[name='PlanningType_1']").prop("disabled", true);
            $("input[name='PlanningType_6']").prop("disabled", false);
            $("input[name='PlanningType_7']").prop("disabled", false);
        } else {
            $("input[name='PlanningType_6']:checkbox").prop('checked', false);
            $("input[name='PlanningType_7']:checkbox").prop('checked', false);
            $("input[name='PlanningType_6']").prop("disabled", true);
            $("input[name='PlanningType_7']").prop("disabled", true);
        }

        if (!$("input[name='PlanningType_3']").is(":checked") && !$("input[name='PlanningType_4']").is(":checked") && !$("input[name='PlanningType_6']").is(":checked") && !$("input[name='PlanningType_7']").is(":checked")) {
            $("input[name='PlanningType_1']").prop("disabled", false);
        } else {
            $("input[name='PlanningType_1']").prop("disabled", true);
        }


        InsertPTMData = [];
        DeletePTMData = [];
        $('#divPlan input:checked').each(function () {
            if ($(this).data("code") == "PT")
                InsertPTMData.push(this.value);
        });
        $('#divPlan input:unchecked').each(function () {
            if ($(this).data("code") == "PT")
                DeletePTMData.push(this.value);
        });


        if (DeletePTMData.length > 0 && InsertPTMData.length > 0) {
            dataFactory.DeletPlanningType($scope.LayoutID, DeletePTMData).then(function (resObj) {
                if (resObj.data.error === undefined) {
                    $scope.loading = true;
                    if (InsertPTMData.length > 0) {
                        dataFactory.SavePlanningType($scope.LayoutID, InsertPTMData).then(function (resObj) {
                            if (resObj.data.error === undefined) {
                                validationTrue("Planning Type Saved Sucessfully");
                                //if (!($("input[name='PlanningType_3']").is(":checked"))) {
                                //    GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
                                //}
                                GetPlanningTypeMaster($scope, dataFactory, InsertPTMData);
                                $(':radio:not(:checked)').attr('disabled', true);
                            }
                            else
                                validationFalse(resObj.data.error);
                        }, function () {
                            $scope.error = "Some Error.";
                        });
                    }
                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });
        }
        else if (InsertPTMData.length > 0) {
            $scope.loading = true;
            dataFactory.SavePlanningType($scope.LayoutID, InsertPTMData).then(function (resObj) {
                if (resObj.data.error === undefined) {
                    validationTrue("Planning Type Saved Sucessfully");
                    //if (!($("input[name='PlanningType_3']").is(":checked"))) {
                    //    GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
                    //}
                    GetPlanningTypeMaster($scope, dataFactory, InsertPTMData);
                    $(':radio:not(:checked)').attr('disabled', true);
                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });
        }
        else if (DeletePTMData.length > 0) {
            dataFactory.DeletPlanningType($scope.LayoutID, DeletePTMData).then(function (resObj) {
                if (resObj.data.error === undefined) {
                    //GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
                    GetPlanningTypeMaster($scope, dataFactory);
                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });
        }


        $scope.PlanningSpecifics = "";

        if ($("input[name='PlanningType_3']").is(":checked")) {
            $scope.loading = true;
            dataFactory.SaveTripDefault($scope.LayoutID, $scope.OBCMvalue).then(function (resObj) {
                if (resObj.data.error === undefined) {
                    GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });
        }
    };

    $(document).on("click", "#deletactivity", function () {
        if (layoutOrgin != 'New' && $('#isModification').val() != 0) {
            dataFactory.DeleteActivitiesRegistration($scope.LayoutID, $('#actid').val()).then(function (resObj) {
                GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
                if (resObj.data.error === undefined) {

                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });

            $('#toolboxModal').modal('hide');
        }
        else {
            dataFactory.DeleteActivityNew($scope.LayoutID, $('#actid').val()).then(function (resObj) {
                GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
                if (resObj.data.error === undefined) {

                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });
            $('#toolboxModal').modal('hide');
        }
    });

    $(document).on("click", "#deletecancel", function () {
        GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
        $('#toolboxModal').modal('hide');
    });

    $scope.GetAllowancetype = function (event) {
        $scope.loading = true;
        var AllowanceData = "";
        var DeleteAllowanceData = "";
        AllowanceData = [];
        $('#divAllow input:checked').each(function () {
            AllowanceData.push(this.value);
        });
        if (AllowanceData !== "") {
            $(':radio:not(:checked)').attr('disabled', true);
        } else {

            $(':radio:not(:checked)').attr('disabled', false);
        }
        DeleteAllowanceData = [];
        $('#divAllow input:unchecked').each(function () {
            DeleteAllowanceData.push(this.value);
        });

        dataFactory.SaveAllowances($scope.LayoutID, AllowanceData).then(function (resObj) {
            if (resObj.data.error === undefined) {
                validationTrue("Allowances Saved Successfully.");
                GetAllowancesMaster($scope, dataFactory, $scope.LayoutID, $rootScope);
                $scope.loading = false;
            }
            else
                validationFalse(resObj.data.error);
        }, function () {
            $scope.error = "Some Error.";
        });

        dataFactory.DeletAllowances($scope.LayoutID, DeleteAllowanceData).then(function (resObj) {
            if (resObj.data.error === undefined) {
                //validationTrue("Allowances Removed Successfully.")
            }
            else
                validationFalse(resObj.data.error);
        }, function () {
            $scope.error = "Some Error.";
        });

    };

    $scope.GetPlanningSpecsub = function (event) {
        $scope.loading = true;

        if ($("input[name='" + event.target.name + "']").is(":checked")) {
            $("input[name='PlanningType_" + event.target.id + "']").prop("disabled", true);
        } else {
            if ($("input[name='PlanningType_3']").is(":checked")) {
                $("input[name='PlanningType_4']:checkbox").prop('checked', true);
                $("input[name='PlanningType_4']").prop("disabled", true);
                $("input[name='PlanningType_3']").prop("disabled", false);
            }
            else
                $("input[name='PlanningType_" + event.target.id + "']").prop("disabled", false);
        }

        setTimeout(function () {

            InsertPTSData = [];
            DeletePTSData = [];
            $('.divPlanSpec input:checked').each(function () {
                InsertPTSData.push(this.value);
            });
            $('.divPlanSpec input:unchecked').each(function () {
                DeletePTSData.push(this.value);
            });

            if (DeletePTSData.length > 0 && InsertPTSData.length > 0) {
                dataFactory.DeletPlanningSpecification($scope.LayoutID, DeletePTSData).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        dataFactory.SavePlanningSpecification($scope.LayoutID, InsertPTSData).then(function (resObj) {
                            if (resObj.data.error === undefined) {
                                GetPlanningTypeMaster($scope, dataFactory);
                            }
                            else {
                                $scope.loading = false;
                                validationFalse(resObj.data.error);
                            }
                        }, function () {
                            $scope.error = "Some Error.";
                        });
                    }
                    else {
                        $scope.loading = false;
                        validationFalse(resObj.data.error);
                    }

                }, function () {
                    $scope.error = "Some Error.";
                });
            }
            else if (InsertPTSData.length > 0) {
                dataFactory.SavePlanningSpecification($scope.LayoutID, InsertPTSData).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        GetPlanningTypeMaster($scope, dataFactory);
                    }
                    else {
                        $scope.loading = false;
                        validationFalse(resObj.data.error);
                    }
                }, function () {
                    $scope.error = "Some Error.";
                });
            }
            else if (DeletePTSData.length > 0) {
                dataFactory.DeletPlanningSpecification($scope.LayoutID, DeletePTSData).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        GetPlanningTypeMaster($scope, dataFactory);
                    }
                    else {
                        $scope.loading = false;
                        validationFalse(resObj.data.error);
                    }

                }, function () {
                    $scope.error = "Some Error.";
                });
            }
        }, 200);
    };

    $scope.AddNewLanguage = function () {

        $scope.Languagevalu = "";
        var modalInstance = $uibModal.open({
            templateUrl: 'AddLanguage.html',
            controller: 'LanguageCtl',
            scope: $scope,
            rootscope: $rootScope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) { }, function (obj) { });

        setTimeout(function () {
            $('#dpLanguage').val("");
            $('#dpLanguage').focus();
        }, 300);
    };

    $scope.AddDocumentTypeFlex = function () {

        $scope.Languagevalu = "";
        var modalInstance = $uibModal.open({
            templateUrl: 'AddDocumentTypeFlex.html',
            controller: 'DocumentTypeFlexCtl',
            scope: $scope,
            rootscope: $rootScope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) { }, function (obj) { });

        GetDocumentType($scope, dataFactory, $scope.LayoutID);

        setTimeout(function () {
            $('#dpDocumentType').val("");
            $('#dpDocumentType').focus();
        }, 300);
    };

    $scope.AddTransportTypes = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'AddTransPortType.html',
            controller: 'TransPortTypeCtl',
            scope: $scope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) {

        }, function (obj) { });
        setTimeout(function () {
            $('#txtTransportType').focus();
        }, 300);
    };

    //////////
    $scope.CopyLayoutPopup = function () {

        if (($scope.rolesAccessList.indexOf("DCLB") !== -1 && $scope.layoutStatusAccessList.indexOf("DCLB") !== -1) || ($scope.roleWithLayoutStatusAccessList.indexOf("DCLB") !== -1)) {

            $('.divcopylayout').removeClass('show').addClass('hide');
            $('#CopyLayoutModal').modal();
            $('#frmcopylayout').removeClass('hide').addClass('show');
            $(".popupheader").html('<h4 class="pull- left"><b>Copy Layout</b></h4>');

            $("#hidcustomerid").val($scope.Customerid);

            dataFactory.GetCopyLayoutName($scope.LayoutID).then(function (resObj) {
                if (resObj.data.error === undefined) {
                    $scope.existinglayoutname = resObj.data;
                    $('#txtNewLayout').val($scope.existinglayoutname);
                }
                else
                    validationFalse(resObj.data.error);
            });

            CopyLayoutPopupCount = 0;

            dataFactory.GetUsedLanguage($scope.LayoutID).then(function (resObj) {
                if (resObj.data.error === undefined) {
                    LanguagesDropDown(resObj.data);
                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });

            dataFactory.GetCustomerForCopyLayout().then(function (resObj) {
                if (resObj.data.error === undefined) {
                    CopyCustomerDropDown(resObj.data);
                    dataFactory.GetLoginCustomerId().then(function (resObj) {
                        if (resObj.data.error === undefined) {
                            var roobj = $("#CopyCustomer").data("ejDropDownList");
                            roobj.setSelectedValue(resObj.data);
                        }
                    }, function () {
                        $scope.error = "Some Error.";
                    });

                    $scope.loading = false;
                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });

            //dataFactory.GetLayoutfordrop().then(function (resObj) {
            //    if (resObj.data.error === undefined) {
            //        existingLayout(resObj.data);
            //    }
            //    else
            //        validationFalse(resObj.data.error);
            //}, function () {
            //    $scope.error = "Some Error.";
            //});

            setTimeout(function () { $('#txtNewLayout').focus(); }, 500);
        }
        else
            validationFalse("Access Denied");
    };

    $scope.submitCopyLayout = function () {

        var NewLayoutName = $("#txtNewLayout").val();
        var CustomerId = $("#CopyCustomer").val();
        var LanguageIds = $("#dpCopyLanguages").val();
        var InfoColumn = $('#chkinfocolumn').is(':checked');
        if (NewLayoutName.trim().length > 0) {
            if (NewLayoutName.trim().length >= 2) {
                if (NewLayoutName.trim().length <= 35) {
                    dataFactory.GetExsistingLayoutName(CustomerId, NewLayoutName).then(function (resObj) {
                        if (resObj.data.error === undefined) {
                            if (parseInt(CustomerId, 10) > 0) {
                                if (LanguageIds.trim().length > 0) {
                                    $scope.loading = true;
                                    $('.divcopylayout').removeClass('show').addClass('hide');
                                    $('#CopyLayoutModal').modal('hide');
                                    dataFactory.SaveCopyLayout($scope.LayoutID, NewLayoutName, CustomerId, LanguageIds, InfoColumn).then(function (resObj) {
                                        if (resObj.data.error === undefined) {
                                            window.location = "/tx-creatis/CustomerOverview/CustomerOverview?customerid=" + CustomerId;
                                            validationTrue("Copy Layout Saved Successfully");
                                        }
                                        else
                                            validationFalse(resObj.data.error);
                                        $scope.loading = false;

                                    }, function () {
                                        validationFalse("Request error.");
                                        $scope.error = "Some Error.";
                                        $scope.loading = false;
                                    });
                                }
                                else {
                                    $("#dpCopyLanguages").focus();
                                    validationFalse("Please Select Languages");
                                }
                            }
                            else {
                                $("#CopyCustomer").focus();
                                validationFalse("Please Select Customer");
                            }
                        }
                        else
                            validationFalse(resObj.data.error);

                    }, function () {
                        validationFalse("Request error.");
                        $scope.error = "Some Error.";
                    });
                }
                else {
                    $("#txtNewLayout").focus();
                    validationFalse("Please Give New Layout Name Maximum 35 Letters");
                }
            }
            else {
                $("#txtNewLayout").focus();
                validationFalse("Please Give New Layout Name Minimum 2 Letters");
            }
        }
        else {
            $("#txtNewLayout").focus();
            validationFalse("Please Enter Valid New Layout Name");
        }

    };

    $scope.cancelcopylayout = function () {
        $('.divcopylayout').removeClass('show').addClass('hide');
        $('#CopyLayoutModal').modal('hide');
    };

    function LanguagesDropDown(language) {

        var obj = $('#dpCopyLanguages').data("ejDropDownList");
        if (!obj) {
            $('#dpCopyLanguages').ejDropDownList({
                dataSource: language,
                watermarkText: "Select Language",
                fields: { text: "language", value: 'languageRefId' },
                width: "100%",
                showCheckbox: true,
                enableFilterSearch: true,
                enablePopupResize: true,
                delimiterChar: "-"
            });
        } else {
            obj.enableItemsByIndices("0");
        }
        var dropDownObj = $("#dpCopyLanguages").ejDropDownList("instance");
        for (var i = 0; i < language.length; i++) {
            dropDownObj.setSelectedValue(parseInt(language[i].languageRefId, 10));
        }
        dropDownObj.disableItemsByIndices("0");
        $scope.loading = false;
    }

    function CopyCustomerDropDown(customers) {



        $('#CopyCustomer').ejDropDownList({
            dataSource: customers,
            watermarkText: "Select Customer",
            fields: { text: "cust", value: 'customerId' },
            width: "100%",
            showCheckbox: false,
            select: function (sender) {
                customerId = sender.value;
            },

            change: function (sender) {
                if (CopyLayoutPopupCount > 0) {

                    dataFactory.GetUsedLanguage($scope.LayoutID).then(function (resObj) {
                        var language = resObj.data;


                        var obj = $('#dpCopyLanguages').data("ejDropDownList");
                        if (!obj) {
                            $('#dpCopyLanguages').ejDropDownList({
                                dataSource: language,
                                watermarkText: "Select Language",
                                fields: { text: "language", value: 'languageRefId' },
                                width: "100%",
                                showCheckbox: true,
                                enableFilterSearch: true,
                                enablePopupResize: true,
                                delimiterChar: "-"
                            });
                        }
                        else {
                            obj.enableItemsByIndices("0");
                        }
                        var dropDownObj = $("#dpCopyLanguages").ejDropDownList("instance");
                        dropDownObj.setSelectedValue(66);
                        if (sender.value == $scope.Customerid) {
                            for (var i = 0; i < language.length; i++) {
                                dropDownObj.setSelectedValue(parseInt(language[i].languageRefId, 10));
                            }
                        }
                        else {
                            for (var i = 0; i < language.length; i++) {
                                if (language[i].languageRefId != 66)
                                    dropDownObj.unselectItemByValue(parseInt(language[i].languageRefId, 10));
                            }
                        }

                        dropDownObj.disableItemsByIndices("0");


                    });
                }
                else
                    CopyLayoutPopupCount = 1;

                dataFactory.GetInfoColumnDetails(sender.value, $scope.Customerid).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        $("#chkinfocolumn").attr("disabled", false);
                    }
                    else {
                        $("#chkinfocolumn").attr("disabled", true);
                    }

                }, function () {
                    validationFalse("Request error.");
                    $scope.error = "Some Error.";
                });
            },
            enableFilterSearch: true,
            enablePopupResize: true
        }).data("ejDropDownList").setSelectedValue($scope.Customerid);

        $('#CopyCustomer_popup_list_wrapper').addClass("syncdropdown");
    }


    ///////////////

    $scope.AddNewAllowance = function () {

        var modalInstance = $uibModal.open({
            templateUrl: 'Allowance.html',
            controller: 'AllowanceCtl',
            scope: $scope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) { }, function (obj) { });
        setTimeout(function () {
            $('#txtAllowances').focus();
        }, 500);
    };

    dataFactory.GetWorkingCode().then(function (resObj) {
        if (resObj.data.error === undefined) {
            initialworkingcode = [];
            initialworkingcodebytxsmart = [];

            workingcode = resObj.data;
            for (i = 0; i < workingcode.length; i++) {

                var Workingco = {
                    "text": workingcode[i].workingCodeDescription,
                    "value": workingcode[i].workingCodeDescription,
                    "id": workingcode[i].workingCodeId
                };

                initialworkingcode.push(Workingco);
                if (workingcode[i].workingCodeDescription != "Registration")
                    initialworkingcodebytxsmart.push(Workingco);
            }
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetPlanning().then(function (resObj) {
        if (resObj.data.error === undefined) {
            initialPlanningAll = [];
            initialPlanning = [];
            initialPlanningwithoutreceive = [];
            initialPlanningwithreceive = [];
            Planning = resObj.data;
            for (i = 0; i < Planning.length; i++) {
                var Plannin = {
                    "text": Planning[i].defaultActivitiesRegistrationDescription,
                    "value": Planning[i].defaultActivitiesRegistrationDescription,
                    "id": Planning[i].planningId
                };
                initialPlanningAll.push(Plannin);

                if (Planning[i].defaultActivitiesRegistrationDescription != "Use Previous") {
                    initialPlanning.push(Plannin);
                }
                if (Planning[i].defaultActivitiesRegistrationDescription != "Receive" && Planning[i].defaultActivitiesRegistrationDescription != "Use Previous") {
                    initialPlanningwithoutreceive.push(Plannin);
                }

                if (Planning[i].defaultActivitiesRegistrationDescription != "Use" && Planning[i].defaultActivitiesRegistrationDescription != "Use Previous") {
                    initialPlanningwithreceive.push(Plannin);
                }

            }
            $scope.loading = false;
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    $scope.GoHome = function () {
        dataFactory.GoHomePage();
    };

    function recallgrid() {
        GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
    }

    $scope.loading = false;

    $(document).on("click", ".fa-trash", function () {
        $scope.loading = true;
        var layoutnoteid = $(this).attr("id");

        dataFactory.DeleteLayoutNotes(layoutnoteid, $scope.LayoutID).then(function (resObj) {
            if (resObj.data.error === undefined) {
                $rootScope.LayoutNoteRecords = resObj.data;
                validationTrue("Layout Notes Deleted Successfully");
            }
            else
                validationFalse(resObj.data.error);
            $scope.loading = false;
        }, function () {
            $scope.loading = false;
            $scope.error = "Some Error.";
        });
    });

    $(document).on("click", ".fa-edit", function () {

        var layoutnotes = $(this).data("notes");
        var layoutnoteid = $(this).attr("id");

        $('#saveLayoutNotes').hide();
        $('#cancelLayoutNotes').hide();
        $('#editLayoutNotes').show();
        $('#newLayoutNotes').show();

        $("#txtDisplayLayoutNote").val(layoutnotes);
        $("#hidLayoutNoteId").val(layoutnoteid);
    });

    $scope.editLayoutNotes = function () {

        var layoutnotes = $("#txtDisplayLayoutNote").val();
        var layoutnoteid = $("#hidLayoutNoteId").val();

        if (layoutnotes.trim().length > 0) {
            dataFactory.EditLayoutNotes($scope.LayoutID, layoutnoteid, layoutnotes).then(function (resObj) {
                if (resObj.data.error === undefined) {

                    $rootScope.LayoutNoteRecords = resObj.data;
                    $("#txtDisplayLayoutNote").val("");
                    $("#hidLayoutNoteId").val(0);

                    $('#editLayoutNotes').hide();
                    $('#newLayoutNotes').hide();
                    $('#saveLayoutNotes').show();
                    $('#cancelLayoutNotes').show();

                    validationTrue("Layout Notes Updated Successfully");
                }
                else
                    validationFalse(resObj.data.error);

                $scope.loading = false;
            }, function () {
                $scope.loading = false;
                $scope.error = "Some Error.";
            });
        }
        else {
            validationFalse("Enter Valid Layout Notes.");
        }

    };

    $scope.newLayoutNotes = function () {
        $("#txtDisplayLayoutNote").val('');
        $("#hidLayoutNoteId").val(0);
        $('#editLayoutNotes').hide();
        $('#newLayoutNotes').hide();
        $('#saveLayoutNotes').show();
        $('#cancelLayoutNotes').show();
    };

    dataFactory.GetAlertCopiedLayout(LayoutID).then(function (resObj) {

        var result = resObj.data.split("ƒ");

        if (result[0] == "SL")
            $('#GOScreenLayout').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GOScreenLayout').removeClass('btn-warning').addClass('btn-primary');

        if (result[1] == "TR")
            $('#GoTranslation').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GoTranslation').removeClass('btn-warning').addClass('btn-primary');
    });

    $scope.revertdocumenttypeflex = function (documentTypeFlexId) {
        $scope.loading = true;
        dataFactory.RevertDocumentTypeFlex(documentTypeFlexId).then(function (resObj) {
            if (resObj.data.error === undefined) {
                GetDocumentTypeFlex($scope, dataFactory, LayoutID, $rootScope);
                validationTrue("Document Type Flex Reverted Successfully");
            }
            else
                validationFalse(resObj.data.error);
            $scope.loading = false;
        }, function () {
            $scope.error = "Some Error.";
        });
    }

    $scope.transporttyperevert = function (TransportTypeId) {
        var LayoutId = $scope.LayoutID;
        dataFactory.RevertTransportTypes(LayoutId, TransportTypeId).then(function (resObj) {
            if (resObj.data.error === undefined) {
                validationTrue("Transport Type Reverted Successfully");
                $rootScope.TransportTypeId = 0;

                if (resObj.data.length !== 0) {
                    setTimeout(function () {
                        GetAssignedTransportTypes($scope, dataFactory, $scope.LayoutID, $rootScope);
                        $("#divtransp").html();
                    }, 300);
                }
            }
            else
                validationFalse(resObj.data.error);
        }, function () {
            validationFalse("Request error.");
            $scope.error = "Some Error.";
        });
    };

    $scope.revertusedlanguage = function (languageId) {
        var LayoutId = $scope.LayoutID;
        dataFactory.RevertUsedLanguage(LayoutId, languageId).then(function (resObj) {
            if (resObj.data.error === undefined) {
                validationTrue("Language Reverted Successfully");
                if (resObj.data.length !== 0) {
                    setTimeout(function () {
                        GetUsedLanguage($scope, dataFactory, $scope.LayoutID, $rootScope);
                    }, 300);
                }
            }
            else
                validationFalse(resObj.data.error);
        }, function () {
            validationFalse("Request error.");
            $scope.error = "Some Error.";
        });
    };



}]);

CREATIS.controller('DocumentTypeFlexCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory', '$rootScope',
    function ($scope, $http, $uibModalInstance, dataFactory, $rootScope) {

        $scope.submitDocumentTypeFlex = function () {

            var DocumentTypeIds = $("#dpDocumentType").val();
            var LayoutId = $scope.LayoutID;
            if (DocumentTypeIds.trim().length > 0) {
                dataFactory.SaveDocumentTypeFlex(LayoutId, DocumentTypeIds).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        GetDocumentTypeFlex($scope, dataFactory, LayoutId, $rootScope);
                        validationTrue("Document Type Flex Saved Successfully");
                        $uibModalInstance.dismiss('cancel');
                    }
                    else
                        validationFalse(resObj.data.error);

                }, function () {
                    validationFalse("Request error.");
                    $scope.error = "Some Error.";
                });
            }
            else {
                validationFalse("Please Select Document Type");
                $("#dpDocumentType").focus();
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);

CREATIS.controller('LanguageCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory', '$rootScope',
    function ($scope, $http, $uibModalInstance, dataFactory, $rootScope) {

        $scope.submitLanguage = function () {

            var LanguageId = $scope.Languagevalu;
            var LayoutId = $scope.LayoutID;
            if (parseInt(LanguageId, 10) > 0) {
                dataFactory.DashboardSaveUsedLanguage(LayoutId, LanguageId).then(function (resObj) {
                    if (resObj.data.error === undefined) {

                        dataFactory.GetRemainingLanguages($scope.LayoutID).then(function (resObj) {
                            $rootScope.Language = resObj.data;
                            GetUsedLanguage($scope, dataFactory, $scope.LayoutID, $rootScope);
                            $scope.loading = false;
                        }, function () {
                            $scope.error = "Some Error.";
                        });

                        validationTrue("Language Saved Successfully");
                        $uibModalInstance.dismiss('cancel');
                    }
                    else
                        validationFalse(resObj.data.error);

                }, function () {
                    validationFalse("Request error.");
                    $scope.error = "Some Error.";
                });
            }
            else {
                validationFalse("Please Select Language");
                $("#dpLanguage").focus();
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);

CREATIS.controller('TransPortTypeCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory', '$rootScope',
    function ($scope, $http, $uibModalInstance, dataFactory, $rootScope) {
        $scope.submitTransPortType = function (TransportTypeId, TransportType) {
            var LayoutId = $scope.LayoutID;
            if (TransportType.toString().trim().length > 0) {

                dataFactory.SaveTransportTypes(LayoutId, TransportType.replace(/\+/g, '`'), TransportTypeId).then(function (resObj) {
                    //
                    if (resObj.data.error === undefined) {
                        validationTrue("Transport Type Saved Successfully");
                        $rootScope.TransportTypeId = 0;
                        $rootScope.TransportType = "";

                        if (resObj.data.length !== 0) {
                            setTimeout(function () {
                                GetAssignedTransportTypes($scope, dataFactory, $scope.LayoutID, $rootScope);
                                $("#divtransp").html();
                            }, 300);
                        }
                        $uibModalInstance.dismiss('cancel');
                    }
                    else
                        validationFalse(resObj.data.error);
                }, function () {
                    validationFalse("Request error.");
                    $scope.error = "Some Error.";
                });

            }
            else {
                validationFalse("Please Enter Valid Transport Type");
                $("#txtTransportType").focus();
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }

]);

CREATIS.controller('AllowanceCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory', '$rootScope',
    function ($scope, $http, $uibModalInstance, dataFactory, $rootScope) {
        $scope.submitAllowance = function (AllowanceMasId, Allowances, AllowanceCode) {
            if (Allowances != undefined && Allowances.trim().length > 0) {
                if (AllowanceCode != undefined && AllowanceCode.trim().length > 0) {
                    dataFactory.SaveAllowancesMaster($scope.OBCMvalue, Allowances, AllowanceCode, $scope.Customerid, AllowanceMasId, $scope.LayoutID).then(function (resObj) {
                        if (resObj.data.error === undefined) {
                            dataFactory.GetAllowancesMaster($scope.OBCMvalue, $scope.Customerid, $scope.LayoutID).then(function (resObj) {

                                if (resObj.data.error === undefined) {
                                    validationTrue("Allowance Saved Successfully");
                                    $rootScope.Allowancestype = resObj.data;
                                    GetAllowances($scope, dataFactory, $scope.LayoutID);
                                    $uibModalInstance.dismiss('cancel');
                                }
                                else
                                    validationFalse(resObj.data.error);
                            }, function () {
                                $scope.error = "Some Error.";
                            });
                        }
                        else
                            validationFalse(resObj.data.error);

                    }, function () {
                        validationFalse("Request error.");
                        $scope.error = "Some Error.";
                    });
                }
                else {
                    $("#txtAllowancesCode").focus();
                    validationFalse("Plase Enter Valid  Allowances Code");
                }

            }
            else {
                $("#txtAllowances").focus();
                validationFalse("Plase Enter Valid Allowances");
            }

        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);

function DocumentTypeDropDown(documenttype) {

    $('#dpDocumentType').ejDropDownList({
        dataSource: documenttype,
        watermarkText: "Select Document Type",
        fields: { text: "name", value: 'documentTypeId' },
        width: "100%",
        showCheckbox: true,
        enableFilterSearch: true,
        enablePopupResize: true,
        delimiterChar: "-",
    });

    $('#dpDocumentType_popup_list_wrapper').addClass("syncdropdown");
}

function GetGetOBCTypeMaster($scope, dataFactory, $rootScope) {
    $scope.loading = true;
    dataFactory.GetOBCTypeMaster($scope.LayoutID).then(function (resObj) {

        if (resObj.data.error === undefined) {
            $scope.OBCTypeMaster = resObj.data;
            setTimeout(function () {
                GetAssignedOBCTypes($scope, dataFactory, $rootScope);
                $scope.loading = false;
                $("#divOBCM").find(".Grey").each(function () {
                    $(this).closest("li").css("backgroundColor", "white").css("color", "black");
                });
                $("#divOBCM").find(".Green").each(function () {
                    $(this).closest("li").css("backgroundColor", "#004d00").css("color", "white");
                });
                $("#divOBCM").find(".Yellow").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cccc00").css("color", "white");
                });
                $("#divOBCM").find(".Red").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cc0000").css("color", "white");
                });
            }
                , 100);
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

}

function GetPlanningTypeMaster($scope, dataFactory, InsertPTMData) {

    dataFactory.GetPlanningTypeMaster($scope.LayoutID).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.PlanningTypeMaster = resObj.data;

            setTimeout(function () {

                if ((parseInt($scope.OBCMvalue, 10) > 0) && (parseInt($scope.OBCMvalue, 10) != 3) && (($scope.rolesAccessList.indexOf("DPTC") !== -1 && $scope.layoutStatusAccessList.indexOf("DPTC") !== -1) || ($scope.roleWithLayoutStatusAccessList.indexOf("DPTC") !== -1))) {
                    $("input[name='PlanningType_1']").prop("disabled", false);
                    $("input[name='PlanningType_3']").prop("disabled", false);
                    $("input[name='PlanningType_4']").prop("disabled", false);
                    $("input[name='PlanningType_6']").prop("disabled", true);
                    $("input[name='PlanningType_7']").prop("disabled", true);
                }
                else {
                    $("input[name='PlanningType_1']").prop("disabled", true);
                    $("input[name='PlanningType_3']").prop("disabled", true);
                    $("input[name='PlanningType_4']").prop("disabled", true);
                    $("input[name='PlanningType_6']").prop("disabled", true);
                    $("input[name='PlanningType_7']").prop("disabled", true);
                }

                $("#divPlan").find(".Grey").each(function () {
                    $(this).closest("li").css("backgroundColor", "white").css("color", "black");
                });
                $("#divPlan").find(".Green").each(function () {
                    $(this).closest("li").css("backgroundColor", "#004d00").css("color", "white");
                });
                $("#divPlan").find(".Yellow").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cccc00").css("color", "white");
                });
                $("#divPlan").find(".Red").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cc0000").css("color", "white");
                });

                if (InsertPTMData === undefined) {
                    dataFactory.GetPlanningType($scope.LayoutID).then(function (resObj) {
                        if (resObj.data.error === undefined) {
                            if (resObj.data.length !== 0) {
                                InsertPTMData = [];
                                angular.forEach(resObj.data, function (value, key) {
                                    $("input[name='PlanningType_" + value.planningTypeRefId + "']:checkbox").prop('checked', true);
                                    InsertPTMData.push(value.planningTypeRefId);
                                });

                                GetPlanningSpecificationMaster($scope, dataFactory, InsertPTMData);
                            }
                            $scope.loading = false;
                        }
                        else
                            validationFalse(resObj.data.error);

                    }, function () {
                        $scope.error = "Some Error.";
                    });
                }
                else {
                    angular.forEach(InsertPTMData, function (value, key) {
                        $("input[name='PlanningType_" + value + "']:checkbox").prop('checked', true);
                    });

                    GetPlanningSpecificationMaster($scope, dataFactory, InsertPTMData);
                }
                if (planningtypeloopcount > 0)
                    GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
                else
                    planningtypeloopcount = 1;

            }, 500);
        }
        else
            validationFalse(resObj.data.error);

    }, function () {
        $scope.error = "Some Error.";
    });
}

function GetAssignedOBCTypes($scope, dataFactory, $rootScope) {

    dataFactory.GetAssignedOBCTypes($scope.LayoutID).then(function (resObj) {
        if (resObj.data.error === undefined) {
            if (resObj.data.length > 0) {
                $("#OBC_1").attr('disabled', true);
                $("#OBC_2").attr('disabled', true);
                $("#OBC_3").attr('disabled', true);
                $("#OBC_4").attr('disabled', true);
            }


            $scope.OBCTypeAssignedValues = resObj.data;
            if (resObj.data.length !== 0)
                obcassignedtypeid = resObj.data[0].obcTypeRefId;
            if (resObj.data.length !== 0) {
                angular.forEach($scope.OBCTypeAssignedValues, function (value, key) {
                    $scope.OBCMvalue = value.obcTypeRefId;
                    updateobc = value.obcTypeRefId;
                    $scope.obcvalues = value.obcTypeRefId;
                    $scope.obctypeids = value.obcTypeId;
                    $("#OBC_" + value.obcTypeRefId).attr('disabled', false);
                });

                setTimeout(function () {
                    $("#OBC_" + $scope.obcvalues).prop('checked', true);
                    GetAllowancesMaster($scope, dataFactory, $scope.LayoutID, $rootScope);
                    // if ((parseInt($scope.OBCMvalue, 10) != 4)) {
                    GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
                    //}
                }
                    , 500);


            } else {
                updateobc = 0;
                $scope.obcvalues = 0;
                $scope.obctypeids = 0;
            }
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

}

function GetAllowancesMaster($scope, dataFactory, LayoutID, $rootScope) {

    dataFactory.GetAllowancesMaster($scope.OBCMvalue, $scope.Customerid, $scope.LayoutID).then(function (resObj) {

        if (resObj.data.error === undefined) {
            $rootScope.Allowancestype = resObj.data;
            GetAllowances($scope, dataFactory, LayoutID);

            setTimeout(function () {
                $("#divAllow").find(".Grey").each(function () {
                    $(this).closest("li").css("backgroundColor", "white").css("color", "black");
                });
                $("#divAllow").find(".Green").each(function () {
                    $(this).closest("li").css("backgroundColor", "#004d00").css("color", "white");
                });
                $("#divAllow").find(".Yellow").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cccc00").css("color", "white");
                });
                $("#divAllow").find(".Red").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cc0000").css("color", "white");
                });
            }, 100);
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });
}

function GetPlanningSpecificationMaster($scope, dataFactory, InsertPTMData) {

    dataFactory.GetPlanningSpecificationMaster(InsertPTMData, $scope.LayoutID).then(function (resObj) {

        if (resObj.data.error === undefined) {
            $scope.PlanningSpecifics = resObj.data;

            GetPlanningSpecification($scope, dataFactory, $scope.LayoutID);

            setTimeout(function () {

                $(".divPlanSpec").find(".Grey").each(function () {
                    $(this).closest("li").css("backgroundColor", "white").css("color", "black");
                });
                $(".divPlanSpec").find(".Green").each(function () {
                    $(this).closest("li").css("backgroundColor", "#004d00").css("color", "white");
                });
                $(".divPlanSpec").find(".Yellow").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cccc00").css("color", "white");
                });
                $(".divPlanSpec").find(".Red").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cc0000").css("color", "white");
                });

                if ((parseInt($scope.OBCMvalue, 10) == 3))
                    $(".divPlanSpec").hide();

            }, 500);
        }
        else
            validationFalse(resObj.data.error);

    }, function () {
        $scope.error = "Some Error.";
    });
    if (InsertPTMData === "") {
        $(':radio:not(:checked)').attr('disabled', false);
    }
}

function GetPlanningSpecification($scope, dataFactory, Layoutid) {
    $scope.PlanningSpecification = [];
    dataFactory.GetPlanningSpecification($scope.LayoutID).then(function (resObj) {
        if (resObj.data.error === undefined) {

            $scope.PlanningSpecification = resObj.data;

            angular.forEach(resObj.data, function (value, key) {
                $("input[name='PlanningSpec_" + value.planningSpecificationRefId + "']:checkbox").prop('checked', true);
            });

            if ((parseInt($scope.OBCMvalue, 10) != 3) && ($scope.rolesAccessList.indexOf("DPTC") !== -1 && $scope.layoutStatusAccessList.indexOf("DPTC") !== -1) || ($scope.roleWithLayoutStatusAccessList.indexOf("DPTC") !== -1)) {

                if ($("input[name='PlanningType_1']").is(":checked")) {

                    $("input[name='PlanningType_3']:checkbox").prop('checked', false);
                    $("input[name='PlanningType_4']:checkbox").prop('checked', false);
                    $("input[name='PlanningType_6']:checkbox").prop('checked', false);
                    $("input[name='PlanningType_7']:checkbox").prop('checked', false);

                    $("input[name='PlanningType_3']").prop("disabled", true);
                    $("input[name='PlanningType_4']").prop("disabled", true);
                    $("input[name='PlanningType_6']").prop("disabled", true);
                    $("input[name='PlanningType_7']").prop("disabled", true);

                } else {
                    $("input[name='PlanningType_3']").prop("disabled", false);
                    $("input[name='PlanningType_4']").prop("disabled", false);
                    $("input[name='PlanningType_6']").prop("disabled", true);
                    $("input[name='PlanningType_7']").prop("disabled", true);
                }

                if ($("input[name='PlanningType_3']").is(":checked")) {
                    $("input[name='PlanningType_4']:checkbox").prop('checked', true);
                    $("input[name='PlanningType_4']").prop("disabled", true);
                    $("input[name='PlanningType_1']").prop("disabled", true);
                }
                else if (!($("input[name='PlanningType_1']").is(":checked"))) {
                    $("input[name='PlanningType_4']").prop("disabled", false);
                }

                if ($("input[name='PlanningType_4']").is(":checked")) {
                    $("input[name='PlanningType_1']").prop("disabled", true);
                    $("input[name='PlanningType_6']").prop("disabled", false);
                    $("input[name='PlanningType_7']").prop("disabled", false);
                } else {
                    $("input[name='PlanningType_6']:checkbox").prop('checked', false);
                    $("input[name='PlanningType_7']:checkbox").prop('checked', false);
                    $("input[name='PlanningType_6']").prop("disabled", true);
                    $("input[name='PlanningType_7']").prop("disabled", true);
                }

                if (!$("input[name='PlanningType_3']").is(":checked") && !$("input[name='PlanningType_4']").is(":checked") && !$("input[name='PlanningType_6']").is(":checked") && !$("input[name='PlanningType_7']").is(":checked")) {
                    $("input[name='PlanningType_1']").prop("disabled", false);
                } else {
                    $("input[name='PlanningType_1']").prop("disabled", true);
                }
            }
            else {
                $("input[name='PlanningType_1']").prop("disabled", true);
                $("input[name='PlanningType_3']").prop("disabled", true);
                $("input[name='PlanningType_4']").prop("disabled", true);
                $("input[name='PlanningType_6']").prop("disabled", true);
                $("input[name='PlanningType_7']").prop("disabled", true);
            }
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });
}

function GetAllowances($scope, dataFactory, Layoutid) {

    dataFactory.GetAllowances(Layoutid).then(function (resObj) {
        if (resObj.data.error === undefined) {
            angular.forEach(resObj.data, function (value) {
                $("input[name='Allowancestype_" + value.allowancesRefId + "']:checkbox").prop('checked', true);
                if (value.isQPUsed == 1) {
                    $("input[name='Allowancestype_" + value.allowancesRefId + "']:checkbox").attr('disabled', true);
                    $("#aEdit_" + value.allowancesRefId).hide();
                    $("#aRemove_" + value.allowancesRefId).hide();
                }
                else {
                    $("input[name='Allowancestype_" + value.allowancesRefId + "']:checkbox").attr('disabled', false);
                    $("#aEdit_" + value.allowancesRefId).show();
                    $("#aRemove_" + value.allowancesRefId).show();
                }

            });
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });
}


function UpdateLayoutNotesMail($scope, dataFactory, Layoutid, $rootScope, notes, date, layoutStatusId, urldata, loadcustomeroverview) {
    dataFactory.UpdateLayoutNotes(Layoutid, notes, date, layoutStatusId, urldata).then(function (resObj) {
        if (resObj.data.error === undefined) {
            if (loadcustomeroverview == 1)
                dataFactory.RedirectToCustomerOverview($scope.Customerid);
            else
                dataFactory.RedirectToDashboard(Layoutid);

            validationTrue("Layout updated Successfully");
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });
}

function RequestLayoutMail($scope, dataFactory, Layoutid, $rootScope, notes, date, layoutStatusId, urldata, loaddashboard) {
    dataFactory.RequestLayoutMail(Layoutid, notes, date, layoutStatusId, urldata).then(function (resObj) {

        if (resObj.data.error === undefined) {
            if (loaddashboard == 1)
                dataFactory.RedirectToDashboard(Layoutid);
            else
                dataFactory.RedirectToCustomerOverview($scope.Customerid);

            validationTrue("Request Send Successfully");
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });
}

function GetAssignedTransportTypes($scope, dataFactory, Layoutid, $rootScope) {

    dataFactory.GetAssignedTransportTypes(Layoutid).then(function (resObj) {
        if (resObj.data.error === undefined) {
            counttransporttype = 0;
            angular.forEach(resObj.data, function (value, key) {
                if (value.copyColorCode != "Red")
                    counttransporttype++;
            });

            $rootScope.TransportTypes = resObj.data;
            setTimeout(function () {
                $scope.loading = false;
                $("#divtransp").find(".Grey").each(function () {
                    $(this).closest("li").css("backgroundColor", "white").css("color", "black");
                });
                $("#divtransp").find(".Green").each(function () {
                    $(this).closest("li").css("backgroundColor", "#004d00").css("color", "white");
                });
                $("#divtransp").find(".Yellow").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cccc00").css("color", "white");
                });
                $("#divtransp").find(".Red").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cc0000").css("color", "white");
                });
                if (transporttypeloopcount > 0) 
                    GetActivitiesRegistrationgrid($scope, dataFactory, $scope.LayoutID);
                else
                    transporttypeloopcount++;
                    
            }
                , 100);

        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });
}

function fnGetIndex(arr, key, value) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i][key] == value) { return i; }
    }
    return -1;
}

function GetUsedLanguage($scope, dataFactory, layoutId, $rootScope) {
    dataFactory.GetUsedLanguage(layoutId).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $rootScope.UsedLanguages = resObj.data;
            setTimeout(function () {
                $("#divUsedlanguage").find(".Grey").each(function () {
                    $(this).closest("li").css("backgroundColor", "white").css("color", "black");
                });
                $("#divUsedlanguage").find(".Green").each(function () {
                    $(this).closest("li").css("backgroundColor", "#004d00").css("color", "white");
                });
                $("#divUsedlanguage").find(".Yellow").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cccc00").css("color", "white");
                });
                $("#divUsedlanguage").find(".Red").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cc0000").css("color", "white");
                });
            }, 100);
        }
        else
            validationFalse(resObj.data.error);


    }, function () {
        $scope.error = "Some Error.";
    });

}

function GetDocumentTypeFlex($scope, dataFactory, Layoutid, $rootScope) {
    dataFactory.GetDocumentTypeFlex(Layoutid).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $rootScope.DocumentTypeFlex = resObj.data;
            debugger
            setTimeout(function () {
                $("#divDocumentTypeFlex").find(".Grey").each(function () {
                    $(this).closest("li").css("backgroundColor", "white").css("color", "black");
                });
                $("#divDocumentTypeFlex").find(".Green").each(function () {
                    $(this).closest("li").css("backgroundColor", "#004d00").css("color", "white");
                });
                $("#divDocumentTypeFlex").find(".Yellow").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cccc00").css("color", "white");
                });
                $("#divDocumentTypeFlex").find(".Red").each(function () {
                    $(this).closest("li").css("backgroundColor", "#cc0000").css("color", "white");
                });
            }, 100);
        }
        else
            validationFalse(resObj.data.error);

    }, function () {
        $scope.error = "Some Error.";
    });

}

function GetDocumentType($scope, dataFactory, Layoutid) {
    dataFactory.GetDocumentType(Layoutid).then(function (resObj) {
        if (resObj.data.error === undefined) {
            DocumentTypeDropDown(resObj.data);
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });
}

function GetPlanningOverview($scope, dataFactory, Layoutid) {
    dataFactory.GetPlanningOverview(Layoutid).then(function (resObj) {
        if (resObj.data.error === undefined) {
            initialPlanningOverview = [];
            PlanningOverview = resObj.data;
            for (i = 0; i < PlanningOverview.length; i++) {
                var PlanningOver = {
                    "text": PlanningOverview[i].activitiesRegistrationName,
                    "value": PlanningOverview[i].activitiesRegistrationName,
                    "id": PlanningOverview[i].activitiesRegistrationId
                };
                initialPlanningOverview.push(PlanningOver);
            }
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });
}

function GetActivitiesRegistrationgrid($scope, dataFactory, Layoutid) {
    $scope.loading = true;
    dataFactory.GetActivitiesRegistration($scope.LayoutID, $scope.DefaultLanguageName).then(function (resObj) {
        if (resObj.data.error === undefined) {
            GetPlanningOverview($scope, dataFactory, $scope.LayoutID);
            LoadRegistrationActivities($scope, dataFactory, resObj.data, counttransporttype);
        }
        else {
            $scope.loading = false;
            validationFalse(resObj.data.error);
        }

    }, function () {
        $scope.error = "Some Error.";
    });
}

function LayoutStausDropDown(status) {
    $('#LayoutStatus').ejDropDownList({
        dataSource: status,
        watermarkText: "Select New Status",
        fields: { text: "name", value: 'id' },
        width: 260,
        showCheckbox: false,
        select: function (sender) {
            layoutStatusId = sender.value;
        },
        enableFilterSearch: false,
        enablePopupResize: true
    });

    $('#LayoutStatus_popup_list_wrapper').addClass("syncdropdown");
}

CREATIS.controller('SaveNotesetCtl', ['$scope', '$rootScope', '$http', '$uibModalInstance', "$location", 'dataFactory',
    function ($scope, $rootScope, $http, $uibModalInstance, $location, dataFactory) {
        var absUrl = $location.absUrl();
        urldata = absUrl.toString();
        $scope.SaveLayoutNotes = function () {
            $scope.loading = true;
            var notes = $("#txtDisplayLayoutNote").val();
            if (notes.trim().length > 0) {
                dataFactory.AddLayoutNotes($scope.LayoutID, notes).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        $rootScope.LayoutNoteRecords = resObj.data;
                        $("#txtDisplayLayoutNote").val("");
                        validationTrue("Layout Notes Saved Successfully");
                    }
                    else
                        validationFalse(resObj.data.error);
                    $scope.loading = false;
                }, function () {
                    $scope.error = "Some Error.";
                });
            }
            else {

                validationFalse("Please Enter Valid Notes");
                $('#txtDisplayLayoutNote').focus();
            }
        }

        dataFactory.ViewLayoutNote(LayoutID).then(function (resObj) {
            if (resObj.data.error === undefined) {
                $rootScope.LayoutNoteRecords = resObj.data;
            }
        });

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

]);

CREATIS.controller('RequestLayoutCtl', ['$scope', '$rootScope', '$http', '$uibModalInstance', "$location", 'dataFactory',
    function ($scope, $rootScope, $http, $uibModalInstance, $location, dataFactory) {
        var absUrl = $location.absUrl();
        urldata = absUrl.toString();
        $scope.RequestLayout = function () {
            $scope.loading = true;
            if (parseInt(($scope.requestTypeId), 10) > 0) {
                if ((parseInt(($scope.requestTypeId), 10) == 9 && $("#txtRequestNote").val().toString().trim().length != 0) || parseInt(($scope.requestTypeId), 10) != 9) // Support Need Id from Layout Status Table
                {
                    var notes = ($("#txtRequestNote").val().toString().trim().length == 0 ? "-" : $("#txtRequestNote").val());
                    var date = $("#txtDate").val();

                    var requestTypeId = 0;
                    if (parseInt(($scope.requestTypeId), 10) == 8) // 8 is Only display purpose
                        requestTypeId = 3;
                    if (parseInt(($scope.requestTypeId), 10) == 9)// 9 is Only display purpose
                        requestTypeId = 7;

                    dataFactory.RequestLayout($scope.LayoutID, requestTypeId).then(function (resObj) {
                        if (resObj.data.error === undefined) {

                            if (parseInt(($scope.requestTypeId), 10) == 9)
                                RequestLayoutMail($scope, dataFactory, $scope.LayoutID, $rootScope, notes, date, $scope.requestTypeId, urldata, 1);
                            else
                                RequestLayoutMail($scope, dataFactory, $scope.LayoutID, $rootScope, notes, date, $scope.requestTypeId, urldata, 0);

                            $uibModalInstance.dismiss('cancel');
                        }
                        else
                            validationFalse(resObj.data.error);
                        $scope.loading = false;
                    }, function () {
                        $scope.error = "Some Error.";
                    });

                }
                else {
                    $('#txtRequestNote').focus();
                    validationFalse("Please Enter Valid Request Note");
                }
            }
            else {
                $('#dpRequestType').focus();
                validationFalse("Please Select Request Type");
            }

        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

]);

CREATIS.controller('LayoutNotesetCtl', ['$scope', '$rootScope', '$http', '$uibModalInstance', "$location", 'dataFactory',
    function ($scope, $rootScope, $http, $uibModalInstance, $location, dataFactory) {
        var absUrl = $location.absUrl();
        urldata = absUrl.toString();
        $scope.UpdateLayoutStatusNotes = function () {
            $scope.loading = true;
            if (parseInt(($scope.layoutStatusId), 10) > 0) {
                var notes = ($("#txtLayoutNote").val().toString().trim().length == 0 ? "-" : $("#txtLayoutNote").val());
                var date = $("#txtDate").val();
                dataFactory.UpdateLayoutStatus($scope.LayoutID, $scope.layoutStatusId).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        if (parseInt(($scope.layoutStatusId), 10) == 6)
                            UpdateLayoutNotesMail($scope, dataFactory, $scope.LayoutID, $rootScope, notes, date, $scope.layoutStatusId, urldata, 1);
                        else
                            UpdateLayoutNotesMail($scope, dataFactory, $scope.LayoutID, $rootScope, notes, date, $scope.layoutStatusId, urldata, 0);


                        $uibModalInstance.dismiss('cancel');
                    }
                    else
                        validationFalse(resObj.data.error);
                    $scope.loading = false;
                }, function () {
                    $scope.error = "Some Error.";
                });
            }
            else {
                $('#dpLayoutStatus').focus();
                validationFalse("Please Select Layout Status");
            }

        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');

        };

    }

]);

CREATIS.controller('TransportDeleteCtl', ['$scope', '$rootScope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $rootScope, $http, $uibModalInstance, dataFactory) {
        $scope.deleteTransportType = function () {
            $scope.loading = true;
            dataFactory.DeleteTransportType(LayoutID, $rootScope.DashboardTransportId).then(function (resObj) {
                if (resObj.data.error === undefined) {
                    GetAssignedTransportTypes($scope, dataFactory, $scope.LayoutID, $rootScope);
                    validationTrue("Transport Type Removed Successfully");
                    $uibModalInstance.dismiss('cancel');
                }
                else
                    validationFalse(resObj.data.error);
                $scope.loading = false;
            }, function () {
                $scope.error = "Some Error.";
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');

        };

    }

]);

CREATIS.controller('UsedLanguageDeleteCtl', ['$scope', '$rootScope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $rootScope, $http, $uibModalInstance, dataFactory) {
        $scope.deleteLanguage = function () {
            $scope.loading = true;
            dataFactory.DeleteUsedLanguage(LayoutID, $rootScope.DashboardUsedLanguageId).then(function (resObj) {
                if (resObj.data.error === undefined) {

                    dataFactory.GetRemainingLanguages($scope.LayoutID).then(function (resObj) {
                        $rootScope.Language = resObj.data;
                        GetUsedLanguage($scope, dataFactory, $scope.LayoutID, $rootScope);
                        $scope.loading = false;
                    }, function () {
                        $scope.error = "Some Error.";
                    });

                    validationTrue("Language Removed Successfully");
                    $uibModalInstance.dismiss('cancel');
                }
                else
                    validationFalse(resObj.data.error);
                $scope.loading = false;
            }, function () {
                $scope.error = "Some Error.";
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');

        };

    }

]);

CREATIS.controller('DocumentTypeFlexDeleteCtl', ['$scope', '$rootScope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $rootScope, $http, $uibModalInstance, dataFactory) {
        $scope.deleteDocumentTypeFlex = function () {

            $scope.loading = true;
            dataFactory.DeleteDocumentTypeFlex($rootScope.DashboardDocumentTypeFlexId).then(function (resObj) {
                if (resObj.data.error === undefined) {
                    GetDocumentTypeFlex($scope, dataFactory, LayoutID, $rootScope);
                    validationTrue("Document Type Flex Removed Successfully");
                    $uibModalInstance.dismiss('cancel');
                }
                else
                    validationFalse(resObj.data.error);
                $scope.loading = false;
            }, function () {
                $scope.error = "Some Error.";
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

]);

CREATIS.controller('AllowanceDeleteCtl', ['$scope', '$rootScope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $rootScope, $http, $uibModalInstance, dataFactory) {
        $scope.deleteAllowance = function () {
            $scope.loading = true;
            dataFactory.DeleteAllowance(LayoutID, $rootScope.DashboardAllowanceId).then(function (resObj) {
                if (resObj.data.error === undefined) {
                    GetAllowancesMaster($scope, dataFactory, $scope.LayoutID, $rootScope);
                    validationTrue("Allowance Removed Successfully");
                    $uibModalInstance.dismiss('cancel');
                }
                else
                    validationFalse(resObj.data.error);

                $scope.loading = false;
            }, function () {
                $scope.error = "Some Error.";
            });
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);

CREATIS.controller('TXDesignConvertionCtl', ['$scope', '$rootScope', '$http', '$uibModalInstance', "$location", 'dataFactory',
    function ($scope, $rootScope, $http, $uibModalInstance, $location, dataFactory) {

        $scope.StartTXDesignConvertion = function () {

            $scope.loading = true;
            dataFactory.TXDesignConvertion($scope.LayoutID, "1").then(function (resObj) {

                if (resObj.data.error === undefined) {
                    $scope.loading = false;

                    $("#GetTXDesignConvertionStatus").prop("disabled", false);
                    validationTrue("TX-Design Convertion Started Successfully");

                    dataFactory.TXDesignConvertion($scope.LayoutID, "2").then(function (response) {
                        if (response.data.error === undefined) {

                            if (response.data == "1" || response.data == "0")
                                $("#StartTXDesignConvertion").prop("disabled", true);
                            else
                                $("#StartTXDesignConvertion").prop("disabled", false);

                            $("#divTXDesignConvertionStatus").html("<p>TX-Design Convertion Status : <b> " + response.data + " </b></p>")
                        }
                        else
                            validationFalse(response.data.error);

                        $scope.loading = false;
                    }, function () {
                        $scope.loading = false;
                        validationFalse("Internal Server Error");
                    });

                }
                else
                    validationFalse(resObj.data.error);

                $scope.loading = false;
            }, function () {
                $scope.loading = false;
                validationFalse("Internal Server Error");
            });

        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.GetTXDesignConvertionStatus = function () {
            $scope.loading = true;
            dataFactory.TXDesignConvertion($scope.LayoutID, "2").then(function (resObj) {

                if (resObj.data.error === undefined) {
                    $scope.loading = false;

                    if (resObj.data == "1" || resObj.data == "0")
                        $("#StartTXDesignConvertion").prop("disabled", true);
                    else
                        $("#StartTXDesignConvertion").prop("disabled", false);

                    $("#GetTXDesignConvertionStatus").prop("disabled", false);
                    validationTrue("TX-Design Convertion Status : " + resObj.data);
                    $("#divTXDesignConvertionStatus").html("<p>TX-Design Convertion Status : <b> " + resObj.data + " </b></p>")
                }
                else
                    validationFalse(resObj.data.error);

                $scope.loading = false;
            }, function () {
                $scope.loading = false;
                validationFalse("Internal Server Error");
            });

        };
    }

]);


//divRegistrationActivitytransportType
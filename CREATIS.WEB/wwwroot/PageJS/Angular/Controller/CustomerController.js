﻿CREATIS.controller('CustomerController', ['$scope', 'dataFactory', '$uibModal', '$http', function ($scope, dataFactory, $uibModal, $http, modalService) {
    //Customers
    $scope.loading = true;
    $scope.active = [{ text: "Active", value: "Active" }, { text: "InActive", value: "InActive" }];
    var customersystemtypedata = [];
    dataFactory.GetCustomerSystemType().then(function (resObj) {
        customersystemtypedata = [];
        var result = resObj.data;
        for (i = 0; i < resObj.data.length; i++) {
            var value = {
                "text": result[i].name,
                "value": result[i].name
            };
            customersystemtypedata.push(value);
        }
        fillCusList();
    }, function () {
        $scope.error = "Some Error.";
        });

    
    function fillCusList() {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetCustomer',
            success: function (data) {
                if (data.Error === "") {
                    var dataManager = ej.DataManager({
                        json: data.data,
                        batchUrl: "/Customer/SaveCustomer?data=" + data + "",
                        removeUrl: "/Grid/UrlDelete",
                        adaptor: "remoteSaveAdaptor",
                    });
                }
                fillGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }
    function fillGrid(data) {
        $("#grdCus").ejGrid({
            dataSource: data,
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Add, ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            editSettings: { allowEditing: true, allowAdding: true, allowDeleting: false, allowCancel: true, editMode: ej.Grid.EditMode.Batch },
            allowPaging: true,
            queryCellInfo: "queryCellInfo",
            columns: [
                { field: "customerId", visible: false, isPrimaryKey: true },
                { headerText: "Remove", template: "#CustomerDelete", textAlign: "center", width: "10%", allowEditing: false },
                { field: "name", headerText: 'Name', width: "30%", validationRules: { required: true, maxlength: 35 } },
                { field: "environment", headerText: 'Environment', width: "10%", validationRules: { required: true, maxlength: 35 } },
                { field: "source", headerText: 'Source', width: "10%", validationRules: { required: true, maxlength: 35 } },
                { field: "isActive", headerText: 'Status', editType: ej.Grid.EditingType.Dropdown, dataSource: $scope.status, width: "10%", validationRules: { required: true } },
                {
                    field: "customerSystemTypeName",
                    headerText: "System Type",
                    editType: ej.Grid.EditingType.Dropdown,
                    dataSource: customersystemtypedata,
                    width: "20%",
                    validationRules: { required: true }
                },
                { field: "customCustomer", headerText: 'Custom Customer', width: "80%", visible: false }
            ], beforeBatchAdd: function (args) {
                args.defaultData.customCustomer = 1;
            }, cellEdit: function (args) {
                debugger
                var obj = $("#grdCus").ejGrid("instance");
                if (obj.getSelectedRecords()[0].customCustomer == 0 && args.columnName != "customerSystemTypeName") {
                    args.cancel = true;
                }
                else{
                    args.cancel = false;
                }
            }, actionComplete: function (args) {
                $("#grdCus_searchbar").css("height", "20px").css("width", "500px");
                $scope.loading = false;

                var obj = $("#grdCus").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');
            },
            rowDataBound: function (args) {
                setTimeout(function () {
                    if (args.data.customCustomer == 1) {
                        $("#customer_" + args.data.customerId).show();
                    }
                    else {
                        $("#customer_" + args.data.customerId).hide();
                    }
                        
                }, 10);
            },
            beforeBatchSave: function (args, e) {
                var gridObj = $("#grdCus").data("ejGrid");
                var updated = args.batchChanges.changed;
                var added = args.batchChanges.added;
                $.each(updated, function (index, value) {
                    if (value.isActive == "Active")
                        value.active = 1;
                    else
                        value.active = 0;
                    var d = new Date();
                    value.modifiedOn = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                });
                $.each(added, function (index, value) {
                    if (value.isActive == "Active")
                        value.active = 1;
                    else
                        value.active = 0;
                    var d = new Date();
                    value.createdOn = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                });
                data = gridObj.getBatchChanges();
                BatchSaveCustomer(data)
            }
        });
        $scope.GoCustomer = function () {
            dataFactory.GoCustomer();
        };
        $('.e-pagercontainer').addClass("pull-left");
    }
    function BatchSaveCustomer(data) {

        debugger
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "SaveCustomer",
            data: data,
            success: function (data) {
                fillCusList();
                if (data == "success")
                    validationTrue("Customer Saved Successfully.");
                if (data == "error")
                    validationFalse("Name Already Exist");
            }
        })
    }


    $(document).on("click", ".fa-trash", function () {

        var customerid = $(this).data("customerid");
        var customername = $(this).data("customername");
        $scope.customerid = customerid;

        $(".deletemsg").empty();
        $(".deletemsg").html("<p> Are you sure you want to remove this customer (<b>" + customername + "</b>)</p>");
        $('.divCustomer').removeClass('show').addClass('hide');
        $('#CustomerModal').modal();
        $('#frmCustomerdelete').removeClass('hide').addClass('show');
    });

    $scope.deletelayout = function () {
        $('.divCustomer').removeClass('show').addClass('hide');
        $('#CustomerModal').modal('hide');
        $scope.loading = true;

        dataFactory.DeleteCustomer($scope.customerid).then(function (resObj) {
            if (resObj.data.error === undefined) {
                validationTrue("Customer Deleted Successfully");
                $scope.loading = false;
                fillCusList();
            }
            else {
                $scope.loading = false;
                validationFalse(resObj.data.error);
            }
        }, function () {
            $scope.error = "Some Error.";
        });
    }

    $scope.cancel = function () {
        $('.divCustomer').removeClass('show').addClass('hide');
        $('#CustomerModal').modal('hide');
    };
}]);

function queryCellInfo(args) {
    $(args.cell).attr({
        "data-toggle": "tooltip",
        "data-container": "body",
        "title": args.data[args.column.field]
    });
}

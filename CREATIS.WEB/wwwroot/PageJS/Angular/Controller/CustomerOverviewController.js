﻿

CREATIS.controller('CustomerOverviewController', ['$scope', '$http', 'dataFactory', '$uibModal', '$location', '$window', function ($scope, $http, dataFactory, $uibModal, $location, $window) {
    $scope.Language = "";
    var absUrl = $location.absUrl();
    urldata = absUrl.toString();
    $scope.customerId = urldata.split('=')[1];
    $scope.integrator = "";
    $scope.iId == "0";
    $scope.itegratordata = "";
    $scope.customerNote = "";
    $scope.cnId = "0";
    $scope.customerNotes = "";
    $scope.layoutDatas = "";
    $scope.duplicate = "";
    $scope.customername = "";
    $scope.rolesAccessList = [];

    $scope.updatelayoutid = 0;
    $scope.updatelayout = "";


    $scope.loading = true;
    var celledit = false;
    var isVissibleOrigin = false;


    $(".modal-content").on("show", function () {
        $("body").addClass("modal-open");
    }).on("hidden", function () {
        $("body").removeClass("modal-open")
    });
    $scope.GoCustomer = function () {
        dataFactory.RedirectToCustomerOverview(urldata.split('=')[1]);
    }

    dataFactory.GetRolesAccessList().then(function (resObj) {
        $scope.rolesAccessList = resObj.data;

        if ($scope.rolesAccessList.indexOf("COICU") !== -1) {
            celledit = true;
        }

        if ($scope.rolesAccessList.indexOf("COO") !== -1) {
            isVissibleOrigin = true;
        }

        dataFactory.GetLayout(urldata.split('=')[1]).then(function (resObj) {
            if (resObj.data.error === undefined) {
                $scope.layoutDatas = resObj.data;
                $scope.loading = false;
                LoadCustomerlayout($scope, dataFactory, resObj.data, isVissibleOrigin);
            }
            else
                validationFalse(resObj.data.error);
        }, function () {
            $scope.error = "Some Error.";
        });

        $scope.InfoColumn = {
            data: 'Info',
            enableCellSelection: false,
            enableRowSelection: false,
            enableCellEditOnFocus: true,
            columnDefs: [{
                field: 'iC_ID',
                displayName: 'S.No',
                enableCellEdit: false,
                visible: false
            }, {
                field: 'icM_Column_Name',
                displayName: 'Column Name',
                enableCellEdit: celledit
            }, {
                field: 'customerRefId',
                displayName: 'Customer ID',
                enableCellEdit: false,
                visible: false
            }
                //    ,
                //    {
                //    displayName: 'Actions',
                //    cellTemplate:
                //        '<div class="grid-action-cell" >' +
                //        '<a ng-click="deleteThisRow(row.entity.iC_ID,row.entity.customerRefId);"  ng-disabled="row.entity.customerRefId" >Clear</a></div>',
                //    enableCellEdit: false
                //}

            ]
        };


    }, function () {
        $scope.error = "Some Error.";
    });

    dataFactory.GetCustomerName(urldata.split('=')[1]).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.customername = resObj.data;
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    $scope.GoHome = function () {
        dataFactory.GoHomePage();
    };

    dataFactory.GetInfocolumn(urldata.split('=')[1]).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.Info = resObj.data;
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    $scope.$on('ngGridEventEndCellEdit', function (evt) {

        if (evt.targetScope.row.entity.customerRefId == null) {
            dataFactory.SaveInfocolumn(evt.targetScope.row.entity.iC_ID, evt.targetScope.row.entity.icM_Column_Name, urldata.split('=')[1]).then(function (resObj) {
                if (resObj.data.error === undefined) {
                    validationTrue("Info Columns Saved Successfully.");
                    $scope.CallInfoColumn();
                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });
        } else {
            dataFactory.UpdateInfocolumn(evt.targetScope.row.entity.iC_ID, evt.targetScope.row.entity.icM_Column_Name, evt.targetScope.row.entity.customerRefId).then(function (resObj) {
                if (resObj.data.error === undefined) {
                    validationTrue("Info Columns Saved Successfully");
                    $scope.CallInfoColumn();
                }
                else
                    validationFalse(resObj.data.error);
            }, function () {
                $scope.error = "Some Error.";
            });
        }

    });

    $scope.deleteThisRow = function (iC_ID, customerRefId) {
        if ($scope.rolesAccessList.indexOf("COICC") !== -1) {
            if (customerRefId != '') {
                dataFactory.DeleteInfocolumn(iC_ID).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        validationTrue("Info Column Cleared");
                        $scope.CallInfoColumn();
                    }
                    else
                        validationFalse(resObj.data.error);
                }, function () {
                    $scope.error = "Some Error.";
                });

            }
            $scope.CallInfoColumn();

        }
        else
            validationFalse("Access Denied");
    }

    $scope.CallInfoColumn = function () {
        dataFactory.GetInfocolumn(urldata.split('=')[1]).then(function (resObj) {
            if (resObj.data.error === undefined) {
                $scope.Info = resObj.data;
            }
            else
                validationFalse(resObj.data.error);
        }, function () {
            $scope.error = "Some Error.";
        });
    };

    $scope.Infocolumn = function () {

        var modalInstance = $uibModal.open({
            templateUrl: 'Infocolumn.html',
            controller: 'InfocolumnCtl',
            scope: $scope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) { }, function (obj) { });
    };

    dataFactory.GetIntegrator(urldata.split('=')[1]).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.integrator = resObj.data;
            if (resObj.data.length != 0) {
                var gridData = $scope.integrator;
                angular.forEach(gridData, function (value, key) {
                    $scope.iId = value.i_ID;
                    $scope.itegratordata = value.i_Details;
                    //value.i_Details;

                });
            } else {
                $scope.iId = "0";
                $scope.itegratordata = "";
            }
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    $scope.SaveIntegrator = function () {

        if ($scope.rolesAccessList.indexOf("COIU") !== -1) {

            if ($scope.itegratordata != "") {

                if (($scope.iId == "undefined") || ($scope.iId == "") || ($scope.iId == "0")) {
                    dataFactory.SaveIntegrator($scope.itegratordata, urldata.split('=')[1]).then(function (resObj) {
                        if (resObj.data.error === undefined) {
                            $scope.integrator = resObj.data;
                            validationTrue("Integrator Saved Successfully");
                            dataFactory.GetIntegrator(urldata.split('=')[1]).then(function (resObj) {
                                if (resObj.data.error === undefined) {
                                    $scope.integrator = resObj.data;
                                    if (resObj.data.length != 0) {
                                        var gridData = $scope.integrator;
                                        angular.forEach(gridData, function (value, key) {
                                            $scope.iId = value.i_ID;
                                            $scope.itegratordata = value.i_Details;
                                            //value.i_Details;

                                        });
                                    } else {
                                        $scope.iId = "0";
                                        $scope.itegratordata = "";
                                    }
                                }
                                else
                                    validationFalse(resObj.data.error);
                            }, function () {
                                $scope.error = "Some Error.";
                            });
                        }
                        else
                            validationFalse(resObj.data.error);
                    }, function () {
                        $scope.error = "Some Error.";
                    });
                } else {

                    dataFactory.UpdateIntegrator($scope.iId, $scope.itegratordata).then(function (resObj) {
                        if (resObj.data.error === undefined) {
                            validationTrue("Integrator Saved Successfully");
                            $scope.integrator = resObj.data;
                            dataFactory.GetIntegrator(urldata.split('=')[1]).then(function (resObj) {
                                if (resObj.data.error === undefined) {
                                    $scope.integrator = resObj.data;
                                    if (resObj.data.length != 0) {
                                        var gridData = $scope.integrator;
                                        angular.forEach(gridData, function (value, key) {
                                            $scope.iId = value.i_ID;
                                            $scope.itegratordata = value.i_Details;
                                            //value.i_Details;

                                        });
                                    } else {
                                        $scope.iId = "0";
                                        $scope.itegratordata = "";
                                    }
                                }
                                else
                                    validationFalse(resObj.data.error);
                            }, function () {
                                $scope.error = "Some Error.";
                            });
                        }
                        else
                            validationFalse(resObj.data.error);
                    }, function () {
                        $scope.error = "Some Error.";
                    });
                }
            } else {
                if ($scope.iId != "0") {
                    dataFactory.DeleteIntegrator($scope.iId).then(function (resObj) {
                        if (resObj.data.error === undefined) {
                            $scope.integrator = resObj.data;
                            dataFactory.GetIntegrator(urldata.split('=')[1]).then(function (resObj) {
                                if (resObj.data.error === undefined) {
                                    $scope.integrator = resObj.data;
                                    if (resObj.data.length != 0) {
                                        var gridData = $scope.integrator;
                                        angular.forEach(gridData, function (value, key) {
                                            $scope.iId = value.i_ID;
                                            $scope.itegratordata = value.i_Details;
                                            //value.i_Details;

                                        });
                                    } else {
                                        $scope.iId = "0";
                                        $scope.itegratordata = "";
                                    }
                                }
                                else
                                    validationFalse(resObj.data.error);
                            }, function () {
                                $scope.error = "Some Error.";
                            });
                        }
                        else
                            validationFalse(resObj.data.error);
                    }, function () {
                        $scope.error = "Some Error.";
                    });
                }
            }
        }
        else
            validationFalse("Access Denied");
    };

    $scope.Itegrator = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'Integrator.html',
            controller: 'IntegratorCtl',
            scope: $scope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) { }, function (obj) { });
    };

    $scope.CustomerNotespopup = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'CustomerNotes.html',
            controller: 'CustomerNotesCtl',
            scope: $scope,
            backdrop: false
        });
        modalInstance.result.then(function (obj) { }, function (obj) { });
    };

    dataFactory.GetCustomerNotes(urldata.split('=')[1]).then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.customerNote = resObj.data;
            if (resObj.data.length != 0) {
                var gridData = $scope.customerNote;
                angular.forEach(gridData, function (value, key) {
                    $scope.cnId = value.cN_ID;
                    $scope.customerNotes = value.cN_Details;

                });
            } else {
                $scope.cnId = "0";
                $scope.customerNotes = "";
            }
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    $scope.SaveCustomerNotes = function () {

        if ($scope.rolesAccessList.indexOf("COCNU") !== -1) {
            if ($scope.customerNotes != "") {

                if (($scope.cnId == "undefined") || ($scope.cnId == "") || ($scope.cnId == "0")) {
                    dataFactory.SaveCustomerNotes($scope.customerNotes, urldata.split('=')[1]).then(function (resObj) {
                        if (resObj.data.error === undefined) {
                            validationTrue("Customer Notes Saved Successfully");
                            $scope.customerNote = resObj.data;
                            dataFactory.GetCustomerNotes(urldata.split('=')[1]).then(function (resObj) {
                                if (resObj.data.error === undefined) {
                                    $scope.customerNote = resObj.data;
                                    if (resObj.data.length != 0) {
                                        var gridData = $scope.customerNote;
                                        angular.forEach(gridData, function (value, key) {
                                            $scope.cnId = value.cN_ID;
                                            $scope.customerNotes = value.cN_Details;

                                        });
                                    } else {
                                        $scope.cnId = "0";
                                        $scope.customerNotes = "";
                                    }
                                }
                                else
                                    validationFalse(resObj.data.error);
                            }, function () {
                                $scope.error = "Some Error.";
                            });
                        }
                        else
                            validationFalse(resObj.data.error);

                    }, function () {
                        $scope.error = "Some Error.";
                    });
                } else {

                    dataFactory.UpdateCustomerNotes($scope.cnId, $scope.customerNotes).then(function (resObj) {
                        if (resObj.data.error === undefined) {
                            validationTrue("Customer Notes Saved Successfully");
                            $scope.customerNote = resObj.data;
                            dataFactory.GetCustomerNotes(urldata.split('=')[1]).then(function (resObj) {
                                if (resObj.data.error === undefined) {
                                    $scope.customerNote = resObj.data;
                                    if (resObj.data.length != 0) {
                                        var gridData = $scope.customerNote;
                                        angular.forEach(gridData, function (value, key) {
                                            $scope.cnId = value.cN_ID;
                                            $scope.customerNotes = value.cN_Details;

                                        });
                                    } else {
                                        $scope.cnId = "0";
                                        $scope.customerNotes = "";
                                    }
                                }
                                else
                                    validationFalse(resObj.data.error);
                            }, function () {
                                $scope.error = "Some Error.";
                            });
                        }
                        else
                            validationFalse(resObj.data.error);
                    }, function () {
                        $scope.error = "Some Error.";
                    });
                }
            } else {
                if ($scope.cnId != "0") {
                    dataFactory.DeleteCustomerNotes($scope.cnId).then(function (resObj) {
                        if (resObj.data.error === undefined) {
                            validationTrue("Info Column Saved Successfully");
                            $scope.customerNote = resObj.data;
                            dataFactory.GetCustomerNotes(urldata.split('=')[1]).then(function (resObj) {
                                if (resObj.data.error === undefined) {
                                    $scope.customerNote = resObj.data;
                                    if (resObj.data.length != 0) {
                                        var gridData = $scope.customerNote;
                                        angular.forEach(gridData, function (value, key) {
                                            $scope.cnId = value.cN_ID;
                                            $scope.customerNotes = value.cN_Details;

                                        });
                                    } else {
                                        $scope.cnId = "0";
                                        $scope.customerNotes = "";
                                    }
                                }
                                else
                                    validationFalse(resObj.data.error);
                            }, function () {
                                $scope.error = "Some Error.";
                            });
                        }
                        else
                            validationFalse(resObj.data.error);
                    }, function () {
                        $scope.error = "Some Error.";
                    });
                }
            }
        }
        else
            validationFalse("Access Denied");

    };

    $scope.Validateduplicatelayout = function () {
        var gridData = $scope.layoutDatas;
        var i = 0;
        angular.forEach(gridData, function (value, key) {
            if (value.customerRefId == $scope.customerId && value.layoutName == $scope.LayoutName) {
                i = 1;

            }

        });
        if (i == 1) {
            $scope.duplicate = 'The entering Layout Name is duplicate';
        } else {
            $scope.duplicate = '';
        }
    }

    $scope.NewInstruction = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'GoToInstructionset.html',
            controller: 'InstructionsetCtl',
            scope: $scope,
            backdrop: false
        });

        modalInstance.result.then(function (obj) { }, function (obj) { });
        setTimeout(function () {
            $("#txyLayoutName").focus();
        }, 500);
    };

    dataFactory.GetLanguages().then(function (resObj) {
        if (resObj.data.error === undefined) {
            $scope.Language = resObj.data;
        }
        else
            validationFalse(resObj.data.error);
    }, function () {
        $scope.error = "Some Error.";
    });

    $(document).on("click", ".fa-trash-o", function () {

        var layoutId = $(this).attr("id");
        var layoutName = $(this).data("layoutname");
        var customerName = $(this).data("customername");

        $scope.layoutidforDelete = layoutId;
        $scope.layoutNameforDelete = layoutName;

        $(".deletemsg").empty();
        $(".deletemsg").html("<p> Are you sure you want to remove <b>" + layoutName + "</b> for <b>" + customerName + "</b></p><p>(Item will be permanently deleted)</p>");
        $('.divCustomerOverview').removeClass('show').addClass('hide');
        $('#CustomerOverviewModal').modal();
        $('#frmCustomerOverviewdelete').removeClass('hide').addClass('show');
    });

    $(document).on("click", ".fa-trash", function () {
        validationFalse("Access Denied");
    });

    $(document).on("click", ".fa-pencil-square-o", function () {
        debugger;

        var layoutId = $(this).attr("id");
        var layoutName = $(this).data("layoutname");
        var layoutStatusRefId = $(this).data("layoutstatusrefid");

        if (layoutStatusRefId == 1 || layoutStatusRefId == 4) {
            $('.divEditLayout').removeClass('show').addClass('hide');
            $('#EditLayoutModal').modal();
            $('#frmLayoutEdit').removeClass('hide').addClass('show');

            $("#hidupdatelayoutid").val(layoutId);
            $("#txtupdatelayout").val(layoutName);
            $("#hfOldLayoutName").val(layoutName);
        }
        else
            validationFalse("Process Failed, This Layout Status Not in InProgress (or) CheckNeeded");
    });
    $(document).on("click", ".fa-pencil-square", function () {
        validationFalse("Access Denied");
    });


    $scope.updateLayout = function () {
        $scope.loading = true;
        debugger
        dataFactory.ValidateLayoutName($("#txtupdatelayout").val(), $scope.customerId, $("#hidupdatelayoutid").val()).then(function (resObj) {
            if (resObj.data.error === undefined) {
                var layoutName = $("#txtupdatelayout").val();
                if (layoutName.length >= 2) {
                    if (layoutName.length <= 35) {
                        dataFactory.updateLayout($("#hidupdatelayoutid").val(), layoutName, $("#hfOldLayoutName").val()).then(function (resObj) {
                            if (resObj.data.error === undefined) {
                                validationTrue("Layout Updated Successfully");
                                $('.divEditLayout').removeClass('show').addClass('hide');
                                $('#EditLayoutModal').modal('hide');
                                $scope.loading = false;
                                dataFactory.GetLayout(urldata.split('=')[1]).then(function (resObj) {
                                    if (resObj.data.error === undefined) {
                                        $scope.layoutDatas = resObj.data;
                                        LoadCustomerlayout($scope, dataFactory, resObj.data, isVissibleOrigin);
                                    }
                                    else
                                        validationFalse(resObj.data.error);
                                }, function () {
                                    $scope.error = "Some Error.";
                                });
                            }
                            else {
                                $scope.loading = false;
                                validationFalse(resObj.data.error);
                            }
                        }, function () {
                            $scope.error = "Some Error.";
                        });
                    }
                    else {
                        validationFalse("Please Give Layout Name Maximum 35 Letters");
                        $("#txyLayoutName").focus();
                    }
                }
                else {
                    validationFalse("Please Give Layout Name Minimum 2 Letters");
                    $("#txyLayoutName").focus();
                }
            }
            else
                validationFalse(resObj.data.error);

        }, function () {
            validationFalse("Request error");
            $scope.error = "Some Error.";
        });


    }

    $scope.deletelayout = function () {
        $('.divCustomerOverview').removeClass('show').addClass('hide');
        $('#CustomerOverviewModal').modal('hide');
        $scope.loading = true;
        dataFactory.DeleteLayout($scope.layoutidforDelete).then(function (resObj) {
            if (resObj.data.error === undefined) {
                validationTrue("Layout Deleted Successfully");
                $scope.loading = false;
                dataFactory.GetLayout(urldata.split('=')[1]).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        $scope.layoutDatas = resObj.data;
                        LoadCustomerlayout($scope, dataFactory, resObj.data, isVissibleOrigin);
                    }
                    else
                        validationFalse(resObj.data.error);
                }, function () {
                    $scope.error = "Some Error.";
                });
            }
            else {
                $scope.loading = false;
                validationFalse(resObj.data.error);
            }
        }, function () {
            $scope.error = "Some Error.";
        });
    }

    $scope.cancel = function () {
        $('.divCustomerOverview').removeClass('show').addClass('hide');
        $('#CustomerOverviewModal').modal('hide');
    };

    $scope.canceleditlayout = function () {
        $('.divEditLayout').removeClass('show').addClass('hide');
        $('#EditLayoutModal').modal('hide');
    };

}
]);

CREATIS.controller('InfocolumnCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $http, $uibModalInstance, dataFactory) {
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);

CREATIS.controller('IntegratorCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $http, $uibModalInstance, dataFactory) {

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');

        };

    }

]);

CREATIS.controller('CustomerNotesCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $http, $uibModalInstance, dataFactory) {

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');

        };

    }

]);

CREATIS.controller('InstructionsetCtl', ['$scope', '$http', '$uibModalInstance', 'dataFactory',
    function ($scope, $http, $uibModalInstance, dataFactory) {

        $scope.submitinstructionset = function () {
            if ($scope.LayoutName != undefined) {
                if ($scope.LayoutName.length >= 2) {
                    if ($scope.LayoutName.length <= 35) {
                        if ($('#dpLanguage option:selected').val() != '') {
                            var LanguageId = $scope.DefaultLanguage;
                            var LayoutName = $scope.LayoutName;
                            var CustomerId = $scope.customerId;

                            if (LanguageId == undefined) {
                                LanguageId = 66;
                            }
                            dataFactory.ValidateLayoutName($scope.LayoutName, CustomerId, 0).then(function (resObj) {
                                if (resObj.data.error === undefined) {
                                    dataFactory.SaveLayoutDetails(LanguageId, LayoutName.replace(/\+/g, '`'), CustomerId, 1).then(function (resObj) {
                                        if (resObj.data.error === undefined) {
                                            dataFactory.RedirectToDashboard(resObj.data);
                                            $uibModalInstance.dismiss('cancel');
                                        }
                                        else
                                            validationFalse(resObj.data.error);

                                    }, function () {
                                        validationFalse("Request error");
                                        $scope.error = "Some Error.";
                                    });
                                }
                                else
                                    validationFalse(resObj.data.error);

                            }, function () {
                                validationFalse("Request error");
                                $scope.error = "Some Error.";
                            });


                        }
                        else {
                            validationFalse("Please Select Language");
                            $("#dpLanguage").focus();
                        }
                    }
                    else {
                        validationFalse("Please Give Layout Name Maximum 35 Letters");
                        $("#txyLayoutName").focus();
                    }
                }
                else {
                    validationFalse("Please Give Layout Name Minimum 2 Letters");
                    $("#txyLayoutName").focus();
                }
            } else {
                validationFalse("Please Enter Layout Name");
                $("#txyLayoutName").focus();
            }
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }

]);

function LoadCustomerlayout($scope, dataFactory, layout, isVissibleOrigin) {
    var gridheight;
    var x = screen.height;
    //alert(x);
    if (x == 1024) {
        gridheight = 480;
    } else

        if (x == 768) {
            gridheight = 210;
        }
        else { gridheight = 520; }
    $("#CustomerOverview").ejGrid({
        dataSource: ej.DataManager(layout),
        allowScrolling: false,
        isResponsive: true,
        allowResizeToFit: true,
        allowPaging: true,
        pageSettings: {
            pageSize: 15
        },
        scrollSettings: {
            height: gridheight
        },
        toolbarSettings: {
            showToolbar: true,
            toolbarItems: [ej.Grid.ToolBarItems.Delete, ej.Grid.ToolBarItems.Search]
        },
        allowTextWrap: true,
        columns: [{
            field: "layoutType",
            headerText: "Type",
            width: 20 + "%"
        }, {
            field: "layoutName",
            headerText: "Name",
            width: 30 + "%"
        }, {
            field: "createdOn",
            headerText: "Created Date",
            format: "{0:MM/dd/yyyy}",
            type: "date",
            width: 8 + "%"
        }, {
            field: "layoutStatus",
            headerText: "Status",
            width: 8 + "%"
        }, {
            field: "updatedOn",
            headerText: "Date Last Status",
            format: "{0:MM/dd/yyyy}",
            type: "date",
            width: 10 + "%"
        }, {
            field: "layoutOrigin",
            headerText: "Origin",
            width: 8 + "%",
            visible: isVissibleOrigin
        }, {
            field: "layoutId",
            headerText: "PK",
            visible: false
        },
        {
            field: "layoutStatusRefId",
            headerText: "Layout Status",
            visible: false
        }, {
            template: true,
            templateID: "#Edit",
            headerText: "Edit",
            width: 8 + "%",
            textAlign: "center"
        }, {
            template: true,
            templateID: "#Delete",
            headerText: "Delete",
            width: 8 + "%",
            textAlign: "center"
        }

        ],
        recordDoubleClick: function (args) {

            dataFactory.GetRecentActivitiestoDashboard(args.data.layoutId);

        },
        rowSelected: function (args) {
            if (args.data.layoutStatus == "In Progress") {
                $("#CustomerOverview_delete").removeClass("e-disable");
            } else {
                $("#CustomerOverview_delete").addClass("e-disable");
            }
            var selectedrowindex = this.selectedRowsIndexes; // get selected row indexes
            var selectedrecords = this.getSelectedRecords(); // get selected records
        },
        actionComplete: function (args) {
            $("#CustomerOverview_searchbar").css("height", "20px").css("width", "500px");
            $scope.loading = false;
        },
        endDelete: function (args) {
            if (args.data.layoutStatus == "In Progress") {
                $("#CustomerOverview_delete").addClass("e-disable");
                dataFactory.DeleteLayout(args.data.layoutId).then(function (resObj) {
                    if (resObj.data.error === undefined) {
                        validationTrue("Layout Deleted Successfully");
                    }
                    else
                        validationFalse(resObj.data.error);
                }, function () {
                    $scope.error = "Some Error.";
                });

            }

        }
    });

    $(".e-pagercontainer").addClass('pull-left');
    $(".e-gridcontent").css('border-radius', '5px');
    $("#CustomerOverview_delete").hide();
    $(".fa-trash-o").css("cursor", "pointer");
}
﻿




var jsdatafactory;
CREATIS.controller('SavedValuesOverviewController', ['$scope', 'dataFactory', '$uibModal', '$location', '$http', function ($scope, dataFactory, $uibModal, $location, $http, modalService) {
    jsdatafactory = dataFactory;

    var absUrl = $location.absUrl();
    urldata = absUrl.toString();

    var layoutId = urldata.split('=')[1];
    if (layoutId.toString().indexOf("#") >= 0)
        layoutId = layoutId.split('#')[0];


    $scope.layoutId = layoutId;

    $scope.Dashboard = function () {
        dataFactory.GetRecentActivitiestoDashboard($scope.layoutId);
    }

    $scope.GoHome = function () {
        dataFactory.GoHomePage();
    };

    $scope.RedirecttoScreenLayout = function () {
        dataFactory.RedirecttoScreenLayout($scope.layoutId);
    }

    $scope.RedirecttoShowLayout = function () {
        dataFactory.RedirecttoShowLayout($scope.layoutId);
    }

    $scope.RedirecttoTranslation = function () {
        dataFactory.RedirecttoTranslation($scope.layoutId);
    }

    $('.savedvaluesgrid').removeClass('hide').addClass('show');
    $('.locationgrid').removeClass('show').addClass('hide');


    dataFactory.GetAlertCopiedLayout($scope.layoutId).then(function (resObj) {

        var result = resObj.data.split("ƒ");

        if (result[0] == "SL")
            $('#GOScreenLayout').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GOScreenLayout').removeClass('btn-warning').addClass('btn-primary');

        if (result[1] == "TR")
            $('#GoTranslation').removeClass('btn-primary').addClass('btn-warning');
        else
            $('#GoTranslation').removeClass('btn-warning').addClass('btn-primary');

    });

    fillSavedValuesList($scope.layoutId);

    $scope.loading = true;

    $scope.goSavedValues = function () {
        $(".loading").show();
        fillSavedValuesList($scope.layoutId);
        $('.savedvaluesgrid').removeClass('hide').addClass('show');
        $('.locationgrid').removeClass('show').addClass('hide');
    }

    function fillSavedValuesList(layuotId) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            data: { layoutRefId: layuotId },
            url: 'GetAliasList',
            success: function (data) {
                fillSavedValuesGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }

    function fillSavedValuesGrid(data) {

        $("#gridSavedValues").ejGrid({
            dataSource: data,
            editSettings: { allowEditing: true, allowAdding: true, allowDeleting: false, allowCancel: true, editMode: ej.Grid.EditMode.Batch },
            allowPaging: true,
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Add, ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            columns: [
                { field: "aliasNameDetailsId", visible: false, isPrimaryKey: true },
                { field: "layoutRefId", visible: false },
                { field: "aliasName", headerText: 'Name', width: "40%" },
                { field: "comment", headerText: 'Comment', width: "60%" },
            ], actionComplete: function (args) {
                $(".loading").hide();
                $("#gridSavedValues_searchbar").css("height", "20px").css("width", "500px");

                var obj = $("#gridSavedValues").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');
            },
            beforeBatchSave: function (args, e) {
                var gridObj = $("#gridSavedValues").data("ejGrid");

                var added = args.batchChanges.added;
                $.each(added, function (index, value) {
                    //value.layoutRefId = $('#svLayout').val();
                    value.layoutRefId = $scope.layoutId;
                });
                debugger;
                data = gridObj.getBatchChanges();
                BatchSaveSavedValues(data);
            }
        });

        $('.e-pagercontainer').addClass("pull-left");
        $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
        $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');
    }

    function BatchSaveSavedValues(data) {
        debugger;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "SaveSavedValues",
            data: data,
            success: function (data) {
                //fillSavedValuesList($('#svLayout').val());
                fillSavedValuesList($scope.layoutId);
                if (data == "success")
                    validationTrue("Saved Values Saved Successfully.");
                if (data == "error")
                    validationFalse("Saved Values Already Exist");
            }
        });
    }


    $scope.goLocation = function () {
        $(".loading").show();
        fillgridLocationList(0, $scope.layoutId);
        $('.locationgrid').removeClass('hide').addClass('show');
        $('.savedvaluesgrid').removeClass('show').addClass('hide');
    }

    function fillgridLocationList(customerId, layuotId) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            data: { customerRefId: customerId, layoutRefId: layuotId },
            url: 'GetSavedValues',
            success: function (data) {
                fillLocationGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }

    function fillLocationGrid(data) {

        $("#gridLocation").ejGrid({
            dataSource: ej.DataManager(data),
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Search] },
            allowPaging: true,
            allowSorting: true,
            queryCellInfo: "queryCellInfo",
            pageSettings: {
                pageSize: 25
            },
            columns: [
                { field: "questionId", isPrimaryKey: true, visible: false },
                { field: "id", isPrimaryKey: true, visible: false },
                { field: "item", headerText: 'Item', width: '25%' },
                { field: "location", headerText: 'Location', width: '75%' },
            ], actionComplete: function (args) {
                $("#gridLocation_searchbar").css("height", "20px").css("width", "500px");
                $(".loading").hide();
               
            },
        });

        $('.e-pagercontainer').addClass("pull-left");
    }

}]);

function queryCellInfo(args) {

    $(args.cell).attr({
        "data-toggle": "tooltip",
        "data-container": "body",
        "title": args.data[args.column.field]
    });
}

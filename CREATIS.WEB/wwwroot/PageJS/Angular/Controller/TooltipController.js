﻿var jsdatafactory;
CREATIS.controller('TooltipController', ['$scope', 'dataFactory', '$uibModal', '$location', '$http', function ($scope, dataFactory, $uibModal, $location, $http, modalService) {
    jsdatafactory = dataFactory;

    $('.tooltipgrid').removeClass('hide').addClass('show');
    $('.wizardtooltipgrid').removeClass('show').addClass('hide');
    $('.extendedtooltipgrid').removeClass('show').addClass('hide');
    $('.divtooltip').removeClass('show').addClass('hide');

    fillTooltipList();



    $scope.loading = true;

    $scope.goTooltip = function () {
        $(".loading").show();
        fillTooltipList();
        $('.tooltipgrid').removeClass('hide').addClass('show');
        $('.wizardtooltipgrid').removeClass('show').addClass('hide');
        $('.extendedtooltipgrid').removeClass('show').addClass('hide');
    }

    function fillTooltipList() {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetTooltip',
            success: function (data) {
                fillTooltipGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }

    function fillTooltipGrid(data) {
        $(".loading").hide();
        $("#gridTooltip").ejGrid({
            dataSource: ej.DataManager(data),
            isResponsive: true,
            enablelAltRow: true,
            allowTextWrap: true,
            pageSettings: {
                pageSize: 25
            },
            queryCellInfo: "queryCellInfo",
            allowPaging: true,
            allowSorting: true,
            allowFiltering: false,
            enableHeaderHover: true,
            allowResizeToFit: true,
            editSettings: {
                allowEditing: false,
                allowAdding: false,
                allowDeleting: false,
                allowEditOnDblClick: false
            },
            toolbarSettings: {
                showToolbar: true,
                toolbarItems: [ej.Grid.ToolBarItems.Search]
            },
            allowScrolling: false,
            columns: [
                { field: "propertyName", headerText: "Name", width: 10 + "%" },
                { field: "title", headerText: "Path", width: 15 + "%" },
                { field: "wizardTooltips", headerText: "Wizard Tooltips", width: 20 + "%" },
                { field: "extendedHelp", headerText: "Extended Help", width: 35 + "%" },
                { headerText: "Edit", template: "#EditTooltip", textAlign: "center", width: 10 + "%" },
                {
                    field: "tooltipId",
                    headerText: "PK",
                    visible: false
                }
            ],
            actionComplete: function (args) {
                $("#gridTooltip_searchbar").css("height", "20px").css("width", "500px");

            }
        });

        $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
        $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');
    }

    $scope.goWizardTooltip = function () {
        $(".loading").show();
        fillWizardTooltipList();
        $('.wizardtooltipgrid').removeClass('hide').addClass('show');
        $('.tooltipgrid').removeClass('show').addClass('hide');
        $('.extendedtooltipgrid').removeClass('show').addClass('hide');
    }

    function fillWizardTooltipList() {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetWizardTooltip',
            success: function (data) {
                $("#loading").hide();
                fillWizardTooltipGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }

    function fillWizardTooltipGrid(data) {

        var docHeight = $(document).height();
        if (docHeight <= 750)
            docHeight = parseInt(((docHeight * 35) / 100), 10);
        else if (docHeight <= 1000)
            docHeight = parseInt(((docHeight * 50) / 100), 10);
        else
            docHeight = parseInt(((docHeight * 55) / 100), 10);

        $("#gridWizardTooltip").ejGrid({
            dataSource: ej.DataManager(data),
            minWidth: 600,
            isResponsive: true,
            enablelAltRow: true,
            allowTextWrap: true,
            allowSorting: true,
            allowFiltering: false,
            allowScrolling: true,
            enableHeaderHover: true,
            allowResizeToFit: true,
            editSettings: { allowEditing: true, allowCancel: true, editMode: ej.Grid.EditMode.Batch },
            allowPaging: true,
            scrollSettings: { height: parseInt(docHeight), frozenRows: 0 },
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            pageSettings: {
                pageSize: 25
            },
            queryCellInfo: "queryCellInfo",
            columns: [
                { field: "tooltipTranslationId", visible: false, isPrimaryKey: true },
                { field: "tooltipId", visible: false },
                { field: "propertyName", headerText: "Name", width: 100, allowEditing: false },
                { field: "title", headerText: 'Path', width: 300, allowEditing: false },
                { field: "english", headerText: 'English', width: 200, allowEditing: false  },
                { field: "arabic", headerText: 'Arabic', width: 200 },
                { field: "belarusian", headerText: 'Belarusian', width: 200 },
                { field: "bulgarian", headerText: 'Bulgarian', width: 200 },
                { field: "croatian", headerText: 'Croatian', width: 200 },
                { field: "czech", headerText: 'Czech', width: 200 },
                { field: "danish", headerText: 'Danish', width: 200 },
                { field: "dutch", headerText: 'Dutch', width: 200 },
                { field: "estonian", headerText: 'Estonian', width: 200 },
                { field: "finnish", headerText: 'Finnish', width: 200 },
                { field: "french", headerText: 'French', width: 200 },
                { field: "german", headerText: 'German', width: 200 },
                { field: "greek", headerText: 'Greek', width: 200 },
                { field: "hungarian", headerText: 'Hungarian', width: 200 },
                { field: "italian", headerText: 'Italian', width: 200 },
                { field: "latvian", headerText: 'Latvian', width: 200 },
                { field: "lithuanian", headerText: 'Lithuanian', width: 200 },
                { field: "macedonian", headerText: 'Macedonian', width: 200 },
                { field: "norwegian", headerText: 'Norwegian', width: 200 },
                { field: "polish", headerText: 'Polish', width: 200 },
                { field: "portuguese", headerText: 'Portuguese', width: 200 },
                { field: "romanian", headerText: 'Romanian', width: 200 },
                { field: "russian", headerText: 'Russian', width: 200 },
                { field: "slovak", headerText: 'Slovak', width: 200 },
                { field: "slovene", headerText: 'Slovene', width: 200 },
                { field: "spanish", headerText: 'Spanish', width: 200 },
                { field: "swedish", headerText: 'Swedish', width: 200 },
                { field: "turkish", headerText: 'Turkish', width: 200 },
                { field: "ukrainian", headerText: 'Ukrainian', width: 200 }
            ], actionComplete: function (args) {
                $("#loading").hide();
                $("#gridWizardTooltip_searchbar").css("height", "20px").css("width", "500px");
                var obj = $("#gridWizardTooltip").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');
            },
            beforeBatchSave: function (args, e) {
                var changed = args.batchChanges.changed;
                BatchSaveTooltipTransation(changed, "WT")
            }
        });

        $('.e-pagercontainer').addClass("pull-left");
        $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
        $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');
    }

    function BatchSaveTooltipTransation(changed, code) {
        $("#loading").show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "SaveTooltipTranslation",
            data: { changedList: JSON.stringify(changed) },
            success: function (data) {
                $("#loading").hide();
                if (code == "ET")
                    fillWizardTooltipList();
                else if (code == "WT")
                    fillExtendedTooltipList();

                if (data == "success")
                    validationTrue("Tooltip Translation Saved Successfully.");
                if (data == "error")
                    validationFalse("Process Failed Please Try Again.");
            }
        })
    }

    $scope.goExtendedTooltip = function () {
        $(".loading").show();
        fillExtendedTooltipList();
        $('.extendedtooltipgrid').removeClass('hide').addClass('show');
        $('.tooltipgrid').removeClass('show').addClass('hide');
        $('.wizardtooltipgrid').removeClass('show').addClass('hide');
    }

    function fillExtendedTooltipList() {

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'GetExtendedTooltip',
            success: function (data) {
                $("#loading").hide();
                fillExtendedTooltipGrid(data);
            }, error: function (data) {
                alert(data);
            }
        });
    }

    function fillExtendedTooltipGrid(data) {
        var docHeight = $(document).height();
        if (docHeight <= 750)
            docHeight = parseInt(((docHeight * 35) / 100), 10);
        else if (docHeight <= 1000)
            docHeight = parseInt(((docHeight * 50) / 100), 10);
        else
            docHeight = parseInt(((docHeight * 55) / 100), 10);

        $("#gridExtendedTooltip").ejGrid({
            dataSource: ej.DataManager(data),
            minWidth: 600,
            isResponsive: true,
            enablelAltRow: true,
            allowTextWrap: true,
            allowSorting: true,
            allowFiltering: false,
            allowScrolling: true,
            enableHeaderHover: true,
            allowResizeToFit: true,
            editSettings: { allowEditing: true, allowCancel: true, editMode: ej.Grid.EditMode.Batch },
            allowPaging: true,
            scrollSettings: { height: parseInt(docHeight), frozenRows: 0 },
            toolbarSettings: { showToolbar: true, toolbarItems: [ej.Grid.ToolBarItems.Update, ej.Grid.ToolBarItems.Cancel, ej.Grid.ToolBarItems.Search] },
            pageSettings: {
                pageSize: 25
            },
            queryCellInfo: "queryCellInfo",
            columns: [
                { field: "tooltipTranslationId", visible: false, isPrimaryKey: true },
                { field: "tooltipId", visible: false },
                { field: "propertyName", headerText: "Name", width: 100, allowEditing: false },
                { field: "title", headerText: 'Path', width: 300, allowEditing: false },
                { field: "english", headerText: 'English', width: 200, allowEditing: false  },
                { field: "arabic", headerText: 'Arabic', width: 200 },
                { field: "belarusian", headerText: 'Belarusian', width: 200 },
                { field: "bulgarian", headerText: 'Bulgarian', width: 200 },
                { field: "croatian", headerText: 'Croatian', width: 200 },
                { field: "czech", headerText: 'Czech', width: 200 },
                { field: "danish", headerText: 'Danish', width: 200 },
                { field: "dutch", headerText: 'Dutch', width: 200 },
                { field: "estonian", headerText: 'Estonian', width: 200 },
                { field: "finnish", headerText: 'Finnish', width: 200 },
                { field: "french", headerText: 'French', width: 200 },
                { field: "german", headerText: 'German', width: 200 },
                { field: "greek", headerText: 'Greek', width: 200 },
                { field: "hungarian", headerText: 'Hungarian', width: 200 },
                { field: "italian", headerText: 'Italian', width: 200 },
                { field: "latvian", headerText: 'Latvian', width: 200 },
                { field: "lithuanian", headerText: 'Lithuanian', width: 200 },
                { field: "macedonian", headerText: 'Macedonian', width: 200 },
                { field: "norwegian", headerText: 'Norwegian', width: 200 },
                { field: "polish", headerText: 'Polish', width: 200 },
                { field: "portuguese", headerText: 'Portuguese', width: 200 },
                { field: "romanian", headerText: 'Romanian', width: 200 },
                { field: "russian", headerText: 'Russian', width: 200 },
                { field: "slovak", headerText: 'Slovak', width: 200 },
                { field: "slovene", headerText: 'Slovene', width: 200 },
                { field: "spanish", headerText: 'Spanish', width: 200 },
                { field: "swedish", headerText: 'Swedish', width: 200 },
                { field: "turkish", headerText: 'Turkish', width: 200 },
                { field: "ukrainian", headerText: 'Ukrainian', width: 200 }
            ], actionComplete: function (args) {
                $("#gridExtendedTooltip_searchbar").css("height", "20px").css("width", "500px");
                var obj = $("#gridExtendedTooltip").ejGrid("instance");
                obj.element.find(".e-gridtoolbar li").removeAttr('data-content');
            },
            beforeBatchSave: function (args, e) {
                var changed = args.batchChanges.changed;
                BatchSaveTooltipTransation(changed, "ET")
            }
        });

        $('.e-pagercontainer').addClass("pull-left");
        $('.grid .e-gridheader').css('border-top-left-radius', '10px !important');
        $('.grid .e-gridheader').css('border-top-right-radius', '10px !important');
    }


    $(document).on("click", ".fa-edit", function () {

        var tooltipId = $(this).attr("id");
        var wizardtooltip = $(this).data("wizardtooltip");
        var extendedhelp = $(this).data("extendedhelp");

        $scope.loading = true;

        $('.divtooltip').removeClass('show').addClass('hide');
        $('#TooltipModal').modal();
        $('#frmTooltip').removeClass('hide').addClass('show');
        $(".popupheader").html('<h4 class="pull- left"><b>Update Tooltip</b></h4>');

        $("#ttdetailsdiv").addClass("in").removeClass("collapse");
        $("#ttudetailsdiv").addClass("collapse").removeClass("in");
        $("#iTTDetails").addClass("fa-minus").removeClass("fa-plus");
        $("#iTTUpload").addClass("fa-plus").removeClass("fa-minus");
        $("#ttdetailsdiv").removeAttr("style");

        $("#hidTooltipId").val(tooltipId);
        $("#txtWizardTooltip").val(wizardtooltip);
        $("#txtExtendedHelp").val(extendedhelp);

        UploadTooltipImage();

        dataFactory.GetTooltipImage(parseInt(tooltipId, 10)).then(function (resObj) {
            if (resObj.data.error === undefined) {
                ImageUploadGrid(resObj.data);
            }
            else
                validationFalse(resObj.data.error);
        });

        setTimeout(function () { $('#txtWizardTooltip').focus(); }, 500);
    });

    function UploadTooltipImage() {

        $("#UploadTooltipImage").ejUploadbox({
            saveUrl: "UploadTooltipImage",
            extensionsAllow: ".png,.jpg",
            fileSize: 5242880,
            multipleFilesSelection: false,
            dialogText: { title: "Upload Image" },
            dialogPosition: { X: -200, Y: -5 },
            dialogAction: { drag: false },
            dialogAction: { closeOnComplete: true },
            buttonText: { browse: "Upload Image", upload: "Upload", cancel: "Cancel" },
            complete: function (args) {
                if (parseInt(args.responseText, 10) > 0) {
                    dataFactory.UpdateTooltipImageTitle($("#txtimagetitle").val(), parseInt($("#hidTooltipId").val(), 10), parseInt(args.responseText, 10)).then(function (resObj) {
                        if (resObj.data.error === undefined) {
                            ImageUploadGrid(resObj.data);

                            $("#txtimagetitle").val('');
                            $("#UploadTooltipImage").ejUploadbox("disable");
                        }
                        else
                            validationFalse(resObj.data.error);
                    });
                }
                else
                    validationFalse(args.responseText);

                $("#loading").hide();
            },
            begin: function (args) {
                $("#loading").show();
            }
        });

        $("#UploadTooltipImage").ejUploadbox("disable");
    }

    $scope.imagetitleblur = function () {

        var title = $("#txtimagetitle").val();
        if (title.trim().length > 0)
            $("#UploadTooltipImage").ejUploadbox("enable");
        else
            $("#UploadTooltipImage").ejUploadbox("disable");
    }

    $scope.cancelTooltip = function () {
        $('#TooltipModal').modal('hide');
        $("#txtimagetitle").val('');
        $("#UploadTooltipImage").ejUploadbox("disable");
    }

    function ImageUploadGrid(data) {

        $('#gridUploadTooltip').ejGrid({
            dataSource: data,
            allowResizing: true,
            allowPaging: true,
            pageSettings: { pageSize: 5 },
            isResponsive: true,
            enableResponsiveRow: false,
            scrollSettings: { width: 100 + '%' },
            editSettings: {
                allowEditing: false,
                allowDeleting: true,
                allowEditOnDblClick: false,
                showDeleteConfirmDialog: true
            },
            columns: [
                { field: "tooltipRefId", visible: false },
                { field: "tooltipImageId", isPrimaryKey: true, visible: false },
                { field: "filePath", visible: false },
                { field: "title", headerText: 'Image Title', width: "25%" },
                { field: "originalFileName", headerText: 'File Name', width: "40%" },
                { field: "fileName", headerText: 'View', template: "#ViewTooltipImage", width: "20%" },
                {
                    headerText: "Action",
                    commands: [
                        { type: ej.Grid.UnboundType.Delete, buttonOptions: { text: "Delete" } }
                    ],
                    isUnbound: true, width: "15%"
                }
            ],
            actionComplete: function (args) {
                if (args.requestType == "delete") {
                    dataFactory.DeleteTooltipImage(args.data.tooltipImageId, args.data.tooltipRefId).then(function success(response) {
                        if (resObj.data.error === undefined) {
                            ImageUploadGrid(resObj.data);
                            $("#txtimagetitle").val('');
                            $("#UploadTooltipImage").ejUploadbox("disable");
                        }
                        else
                            validationFalse(resObj.data.error);

                    }, function error(response) {
                        console.error(response.headers, response.status, response.statusText);
                    });
                }
            }
        });
    }

    $scope.saveTooltip = function () {

        $("#loading").show();
        var wizardTooltip = $("#txtWizardTooltip").val();
        var extendedHelp = $("#txtExtendedHelp").val();
        var toolipId = $("#hidTooltipId").val();

        if (wizardTooltip.length > 0) {
            dataFactory.UpdateTooltip(toolipId, wizardTooltip, extendedHelp).then(function success(response) {
                if (response.data.error === undefined) {
                    $("#loading").hide();
                    fillTooltipList();
                    validationTrue("Tooltip Updated Successfully.");
                    $('#TooltipModal').modal('hide');
                    $("#txtimagetitle").val('');
                    $("#UploadTooltipImage").ejUploadbox("disable");
                }
                else
                    validationFalse(response.data.error);

            }, function error(response) {
                console.error(response.headers, response.status, response.statusText);
            });
        }
        else {
            validationFalse("Please Enter Valid Wizard Tooltip.")
        }
    }

}]);

function queryCellInfo(args) {

    $(args.cell).attr({
        "data-toggle": "tooltip",
        "data-container": "body",
        "title": args.data[args.column.field]
    });
}
﻿CREATIS.factory('dataFactory', function ($http) {
    var dataFactory = {};
    var projectname = "/tx-creatis";
    /* Recent activities page using C# Home Controller Start Creatis*/
    dataFactory.GetCustomer = function () {
        var url = projectname + "/Home/HomeCustomer"
        return $http.post(url);
    };
    dataFactory.GetCustomerForCopyLayout = function () {
        var url = projectname + "/Home/GetCustomerForCopyLayout"
        return $http.post(url);
    };

    dataFactory.GetLoginCustomerId = function () {
        var url = projectname + "/Home/GetLoginCustomerId"
        return $http.post(url);
    };

    dataFactory.IsCustomerLayout = function () {
        var url = projectname + "/Home/IsCustomerLayout"
        return $http.post(url);
    };

    dataFactory.GetCustomerSpecificList = function () {
        var url = projectname + "/Translator/GetCustomerSpecificList"
        return $http.post(url);
    };


    dataFactory.GetRecentActivities = function () {
        var url = projectname + "/Home/HomeRecentActivities"
        return $http.post(url);
    };

    dataFactory.RedirectToCustomerOverview = function (DropCust) {
        var url = projectname + "/CustomerOverview/CustomerOverview?customerid=" + DropCust
        return window.location.href = url;
    };

    dataFactory.GetTransicsResponce = function () {
        var url = projectname + "/Home/GetTransicsResponce"
        return $http.post(url);
    };



    dataFactory.GetRecentActivitiestoDashboard = function (Layoutid) {
        var url = projectname + "/Home/Dashboard?LayoutID=";
        $http.post(url).then(function () {
            return window.location.href = url + Layoutid;
        });
    };
    dataFactory.RemoveRecentActivities = function (layoutId) {
        var url = projectname + "/Home/RemoveRecentActivities?layoutId=" + layoutId;
        return $http.post(url);
    };

    dataFactory.TXDesignConvertion = function (layoutId, functionId) {
        var url = projectname + "/Dashboard/TXDesignConvertion?layoutId=" + layoutId + "&functionId=" + functionId;
        return $http.post(url);
    };

    dataFactory.GetDefaultLanguage = function (Layoutid) {
        var url = projectname + "/Dashboard/GetDefaultLanguage?layoutId=" + Layoutid;
        return $http.post(url);
    };

    dataFactory.GetAlertCopiedLayout = function (layoutId) {
        var url = projectname + "/Dashboard/GetAlertCopiedLayout?layoutId=" + layoutId;
        return $http.post(url);
    };

    dataFactory.GetRolesAccessList = function (Layoutid) {
        var url = projectname + "/Dashboard/GetRolesAccessList";
        return $http.post(url);
    };

    dataFactory.GetLayoutStatusAccessList = function (Layoutid) {
        var url = projectname + "/Dashboard/GetLayoutStatusAccessList?layoutId=" + Layoutid;
        return $http.post(url);
    };

    dataFactory.GetRoleWithLayoutStatusAccessList = function (Layoutid) {
        var url = projectname + "/Dashboard/GetRoleWithLayoutStatusAccessList?layoutId=" + Layoutid;
        return $http.post(url);
    };

    dataFactory.DeleteLayoutNotes = function (layoutNotesId, layoutId) {
        var url = projectname + "/Dashboard/DeleteLayoutNotes?layoutNotesId=" + layoutNotesId + "&layoutId=" + layoutId;
        return $http.post(url);
    };



    dataFactory.ViewLayoutNote = function (Layoutid) {
        var url = projectname + "/Dashboard/GetLayoutNotes?LayoutID=" + Layoutid;
        return $http.post(url);
    };

    dataFactory.DeleteLayout = function (Layoutid) {
        var url = projectname + "/Home/DeleteLayout?layoutId=" + Layoutid
        return $http.post(url)
    };

    dataFactory.ViewLayoutNote = function (Layoutid) {
        var url = projectname + "/Dashboard/GetLayoutNotes?LayoutID=" + Layoutid;
        return $http.post(url);
    };

    dataFactory.GetLayoutStatus = function (param) {
        var url = projectname + "/Dashboard/GetLayoutStatus?Status=" + param;
        return $http.post(url);
    };

    dataFactory.updateLayout = function (layoutid, layoutname,oldLayoutName) {
        var url = projectname + "/Home/UpdateLayout?LayoutId=" + layoutid + "&LayoutName=" + layoutname + "&OldLayoutName=" + oldLayoutName;
        return $http.post(url)
    };

    dataFactory.GetLayout = function (customerId) {
        var url = projectname + "/Dashboard/GetLayout?customerId=" + customerId;
        return $http.post(url);
    };

    dataFactory.UpdateLayoutStatus = function (Layoutid, layoutStatusId) {
        var url = projectname + "/Dashboard/UpdateLayoutStatusNote?LayoutId=" + Layoutid + "&LayoutStatusId=" + layoutStatusId;
        return $http.post(url);
    };

    dataFactory.RequestLayout = function (Layoutid, requestTypeId) {
        var url = projectname + "/Dashboard/RequestLayout?LayoutId=" + Layoutid + "&RequestTypeId=" + requestTypeId;
        return $http.post(url);
    };

    dataFactory.UpdateLayoutNotes = function (Layoutid, layoutNotes, date, layoutStatusId, urldata) {
        var url = projectname + "/Dashboard/SaveLayoutNotes?LayoutID=" + Layoutid + "&Notes=" + layoutNotes + "&date=" + date + "&layoutStatusId=" + layoutStatusId + "&urlString=" + urldata;
        return $http.post(url);
    };

    dataFactory.RequestLayoutMail = function (Layoutid, layoutNotes, date, layoutStatusId, urldata) {
        var url = projectname + "/Dashboard/RequestLayoutMail?LayoutID=" + Layoutid + "&Notes=" + layoutNotes + "&date=" + date + "&layoutStatusId=" + layoutStatusId + "&urlString=" + urldata;
        return $http.post(url);
    };

    dataFactory.AddLayoutNotes = function (LayoutID, Notes) {
        var url = projectname + "/Dashboard/AddLayoutNotes?LayoutId=" + LayoutID + "&Notes=" + Notes;
        return $http.post(url);
    }
    dataFactory.EditLayoutNotes = function (layoutid, layoutnoteid, layoutnotes) {

        var url = projectname + "/Dashboard/EditLayoutNotes?layoutId=" + layoutid + "&layoutNoteId=" + layoutnoteid + "&layoutNotes=" + layoutnotes;
        return $http.post(url);
    }

    //dataFactory.updateLayoutStatus = function (Layoutid, layoutStatusId) {
    //    var url = projectname + "/Dashboard/UpdateLayoutStatus?LayoutId=" + Layoutid + "&LayoutStatusId=" + layoutStatusId;
    //    return $http.post(url);
    //};
    /* Recent activities page using C# Home Controller End */

    /* Customer Overview page using C# CustomerOverview Controller start */

    dataFactory.GetLayout = function (DropCust) {
        var url = projectname + "/CustomerOverview/CustomerOverviewAvailableLayout?customerid=" + DropCust

        return $http.post(url);
    };

    dataFactory.DeleteCustomer = function (customerId) {
        var url = projectname + "/Customer/DeleteCustomer?customerId=" + customerId
        return $http.post(url);
    };

    dataFactory.GetCustomerName = function (customerid) {
        var url = projectname + "/CustomerOverview/GetCustomerName?customerid=" + customerid
        return $http.post(url);
    };
    dataFactory.ValidateLayoutName = function (layoutname, customerid, layoutid) {
        var url = projectname + "/CustomerOverview/ValidateLayoutName?LayoutName=" + layoutname + "&CustomerId=" + customerid + "&LayoutId=" + layoutid;
        return $http.post(url);
    };


    dataFactory.GetInfocolumn = function (DropCust) {
        var url = projectname + "/CustomerOverview/InfoColumnDetails?customerid=" + DropCust
        return $http.post(url);
    };

    dataFactory.SaveInfocolumn = function (iCID, icMColumnName, customerRefId) {
        var url = projectname + "/CustomerOverview/SaveInfoColumnDetails?ID=" + iCID + "&ColumnName=" + icMColumnName + "&customerRefId=" + customerRefId
        return $http.post(url);
    };

    dataFactory.UpdateInfocolumn = function (iCID, icMColumnName, customerRefId) {
        var url = projectname + "/CustomerOverview/UpdateInfoColumnDetails?ID=" + iCID + "&ColumnName=" + icMColumnName + "&customerRefId=" + customerRefId
        return $http.post(url);
    };

    dataFactory.DeleteInfocolumn = function (iCID) {
        var url = projectname + "/CustomerOverview/DeleteInfoColumnDetails?ID=" + iCID
        return $http.post(url);
    };

    dataFactory.GetIntegrator = function (customerid) {
        var url = projectname + "/CustomerOverview/GetIntegrator?customerid=" + customerid
        return $http.post(url);
    };

    dataFactory.SaveIntegrator = function (I_Details, customerRefId) {
        var url = projectname + "/CustomerOverview/SaveIntegrator?I_Details=" + I_Details + "&customerRefId=" + customerRefId
        return $http.post(url);
    };

    dataFactory.UpdateIntegrator = function (I_ID, I_Details) {
        var url = projectname + "/CustomerOverview/UpdateIntegrator?I_ID=" + I_ID + "&I_Details=" + I_Details
        return $http.post(url);
    };

    dataFactory.DeleteIntegrator = function (I_ID) {
        var url = projectname + "/CustomerOverview/DeleteIntegrator?I_ID=" + I_ID
        return $http.post(url);
    };

    dataFactory.GetCustomerNotes = function (customerid) {
        var url = projectname + "/CustomerOverview/GetCustomerNotes?customerid=" + customerid
        return $http.post(url);
    };

    dataFactory.SaveCustomerNotes = function (CN_Details, customerRefId) {
        var url = projectname + "/CustomerOverview/SaveCustomerNotes?CN_Details=" + CN_Details + "&customerRefId=" + customerRefId
        return $http.post(url);
    };

    dataFactory.UpdateCustomerNotes = function (CN_ID, CN_Details) {
        var url = projectname + "/CustomerOverview/UpdateCustomerNotes?CN_ID=" + CN_ID + "&CN_Details=" + CN_Details
        return $http.post(url);
    };

    dataFactory.DeleteCustomerNotes = function (CN_ID) {
        var url = projectname + "/CustomerOverview/DeleteCustomerNotes?CN_ID=" + CN_ID
        return $http.post(url);
    };


    dataFactory.GoHomePage = function () {
        var url = projectname + "/Home/Index"
        return window.location.href = url;
    };



    dataFactory.GetLanguages = function () {
        var url = projectname + "/CustomerOverview/GetLanguages"
        return $http.post(url);
    };
    dataFactory.GetRemainingLanguages = function (layoutId) {
        var url = projectname + "/Dashboard/GetRemainingLanguages?layoutId=" + layoutId;
        return $http.post(url);
    };

    dataFactory.SaveLayoutDetails = function (LanguageID, LayoutName, customerRefId, defaultlan) {
        var url = projectname + "/CustomerOverview/SaveLayoutDetails?LanguageId=" + LanguageID + "&LayoutName=" + LayoutName + "&customerRefId=" + customerRefId + "&defaultlan=" + defaultlan
        return $http.post(url);
    };

    /* Customer Overview page using C# CustomerOverview Controller End */


    /* Dashboard page using C# Dashboard Controller Start */

    dataFactory.GetRecentActivitiestoDashboard = function (Layoutid) {
        var url = projectname + "/Dashboard/Dashboard?LayoutID=" + Layoutid;
        $http.post(url).then(function () {
            return window.location.href = url;
        });
    };



    dataFactory.RedirectToDashboard = function (LayoutID) {
        var url = projectname + "/Dashboard/Dashboard?LayoutID=" + LayoutID
        return window.location.href = url;
    };

    dataFactory.PanelTitle = function (LayoutID) {
        var url = projectname + "/Dashboard/GetLayoutName?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetOBCTypeMaster = function (LayoutID) {
        var url = projectname + "/Dashboard/GetOBCTypeMaster?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetAssignedOBCTypes = function (LayoutID) {
        var url = projectname + "/Dashboard/GetOBCType?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.SaveOBCType = function (LayoutID, OBCTypeId) {
        var url = projectname + "/Dashboard/SaveOBCType?LayoutID=" + LayoutID + "&OBCTypeId=" + OBCTypeId
        return $http.post(url);
    };

    dataFactory.UpdateOBCTypes = function (OBCTypeId, OBCTypeRefId, LayoutID) {
        var url = projectname + "/Dashboard/UpdateOBCTypes?OBCTypeId=" + OBCTypeId + "&OBCTypeRefId=" + OBCTypeRefId + "&LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetAssignedTransportTypes = function (LayoutID) {
        var url = projectname + "/Dashboard/GetTransportType?LayoutID=" + LayoutID
        return $http.post(url);
    };

    
    
    dataFactory.RevertTransportTypes = function (LayoutID, TransportTypeId) {

        var url = projectname + "/Dashboard/RevertTransportTypes?layoutId=" + LayoutID + "&transportTypeId=" + TransportTypeId;
        return $http.post(url);
    };

    dataFactory.SaveTransportTypes = function (LayoutID, TransportDescription, TransportTypeId) {

        var url = projectname + "/Dashboard/SaveTransportTypes?LayoutID=" + LayoutID + "&TransportDescription=" + encodeURI(TransportDescription) + "&TransportTypeId=" + TransportTypeId;
        return $http.post(url);
    };

    dataFactory.GetUsedLanguage = function (LayoutID) {
        var url = projectname + "/Dashboard/GetUsedLanguage?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.DashboardSaveUsedLanguage = function (LayoutID, LanguageID) {
        var url = projectname + "/Dashboard/DashboardSaveUsedLanguage?LayoutID=" + LayoutID + "&LanguageID=" + LanguageID
        return $http.post(url);
    };

    dataFactory.GetPlanningTypeMaster = function (LayoutID) {
        var url = projectname + "/Dashboard/GetPlanningTypeMaster?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetPlanningType = function (LayoutID) {
        var url = projectname + "/Dashboard/GetPlanningType?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.SavePlanningType = function (LayoutID, PlanningTypeRefId) {
        var url = projectname + "/Dashboard/SavePlanningType?LayoutID=" + LayoutID + "&PlanningTypeRefId=" + PlanningTypeRefId
        return $http.post(url);
    };

    dataFactory.DeletPlanningType = function (LayoutID, PlanningTypeRefId) {
        var url = projectname + "/Dashboard/DeletPlanningType?LayoutID=" + LayoutID + "&PlanningTypeRefId=" + PlanningTypeRefId
        return $http.post(url);
    };
    dataFactory.GetAllowancesMaster = function (OBCTypeRefId, CustomerId, layoutId) {

        var url = projectname + "/Dashboard/GetAllowancesMaster?OBCTypeRefId=" + OBCTypeRefId + "&CustomerId=" + CustomerId + "&layoutId=" + layoutId;
        return $http.post(url);
    };

    dataFactory.GetPlanningSpecificationMaster = function (PlanningTypeMasterRefId, LayoutID) {
        var url = projectname + "/Dashboard/GetPlanningSpecificationMaster?PlanningTypeMasterRefId=" + PlanningTypeMasterRefId + "&LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetPlanningSpecification = function (LayoutID) {
        var url = projectname + "/Dashboard/GetPlanningSpecification?LayoutID=" + LayoutID
        return $http.post(url);
    };
    dataFactory.SavePlanningSpecification = function (LayoutID, PlanningSpecificationRefId) {
        var url = projectname + "/Dashboard/SavePlanningSpecification?LayoutID=" + LayoutID + "&PlanningSpecificationRefId=" + PlanningSpecificationRefId
        return $http.post(url);
    };

    dataFactory.GetDocumentType = function (LayoutID) {
        var url = projectname + "/Dashboard/GetDocumentType?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.RevertDocumentTypeFlex = function (documentTypeFlexId) {
        var url = projectname + "/Dashboard/RevertDocumentTypeFlex?documentTypeFlexId=" + documentTypeFlexId
        return $http.post(url);
    };

    

    dataFactory.GetDocumentTypeFlex = function (LayoutID) {
        var url = projectname + "/Dashboard/GetDocumentTypeFlex?LayoutID=" + LayoutID
        return $http.post(url);
    };
    dataFactory.SaveDocumentTypeFlex = function (LayoutId, DocumentTypeIds) {
        var url = projectname + "/Dashboard/SaveDocumentTypeFlex?LayoutID=" + LayoutId + "&DocumentTypeIds=" + DocumentTypeIds
        return $http.post(url);
    };
    dataFactory.DeleteDocumentTypeFlex = function (DocumentTypeFlexId) {
        var url = projectname + "/Dashboard/DeleteDocumentTypeFlex?DocumentTypeFlexId=" + DocumentTypeFlexId
        return $http.post(url);
    };


    dataFactory.DeletPlanningSpecification = function (LayoutID, PlanningSpecificationRefId) {
        var url = projectname + "/Dashboard/DeletPlanningSpecification?LayoutID=" + LayoutID + "&PlanningSpecificationRefId=" + PlanningSpecificationRefId
        return $http.post(url);
    };

    dataFactory.GetAllowances = function (LayoutID) {
        var url = projectname + "/Dashboard/GetAllowances?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.SaveAllowances = function (LayoutID, AllowancesRefId) {
        var url = projectname + "/Dashboard/SaveAllowances?LayoutID=" + LayoutID + "&AllowancesRefId=" + AllowancesRefId
        return $http.post(url);
    };

    dataFactory.DeletAllowances = function (LayoutID, AllowancesRefId) {
        var url = projectname + "/Dashboard/DeletAllowances?LayoutID=" + LayoutID + "&AllowancesRefId=" + AllowancesRefId
        return $http.post(url);
    };


    dataFactory.SaveAllowancesMaster = function (OBCTypeRefId, AllowancesMasterDescription, AllowanceCode, CustomerId, AllowanceMasId, layoutId) {
        var url = projectname + "/Dashboard/SaveAllowancesMaster?oBCTypeRefId=" + OBCTypeRefId + "&allowancesMasterDescription=" + AllowancesMasterDescription + "&allowanceCode=" + AllowanceCode + "&customerId=" + CustomerId + "&allowanceMasId=" + AllowanceMasId + "&layoutId=" + layoutId;
        return $http.post(url);
    };

    dataFactory.GetWorkingCode = function () {
        var url = projectname + "/Dashboard/GetWorkingCode"
        return $http.post(url);
    };

    dataFactory.GetPlanning = function () {
        var url = projectname + "/Dashboard/GetPlanning"
        return $http.post(url);
    };

    dataFactory.GetPlanningOverview = function (layoutid) {
        var url = projectname + "/Dashboard/GetPlanningOverview?LayoutId=" + layoutid
        return $http.post(url);
    };

    dataFactory.GetSpecific = function (defaultactivitiesid, ispausetrip, obctypeid) {

        var url = projectname + "/Dashboard/GetSpecific?DefaultActivitiesId=" + defaultactivitiesid + "&IsPauseTrip=" + ispausetrip + "&OBCTypeId=" + obctypeid;
        return $http.post(url);
    };

    dataFactory.GetPTO = function () {
        var url = projectname + "/Dashboard/GetPTO"
        return $http.post(url);
    };

    dataFactory.GetInterruptibleBy = function () {
        var url = projectname + "/Dashboard/GetInterruptibleBy"
        return $http.post(url);
    };

    dataFactory.GetActivitiesRegistration = function (LayoutID, LanguageName) {
        var url = projectname + "/Dashboard/GetActivitiesRegistration?LayoutID=" + LayoutID + "&LanguageName=" + LanguageName;
        return $http.post(url);
    };

    dataFactory.SaveActivitiesRegistration = function (LayoutID, ACTReg) {
        //ActivitiesRegistrationName,WorkingCodeRefId,PlanningRefId,PlanningOverviewRefId,SpecificRefId,Visible,ConfirmButton,PTORefId,InterruptibleByRefId,TransportType,EmptyFullSolo,Flexactivity,ISModification) {
        //var url = projectname+"SaveActivitiesRegistration?LayoutID=" + LayoutID+"&ActivitiesRegistrationName="+ActivitiesRegistrationName+"&WorkingCodeRefId="+WorkingCodeRefId+"&PlanningRefId="+PlanningRefId+"&PlanningOverviewRefId="+"&SpecificRefId="+SpecificRefId+"&Visible="+Visible+"&ConfirmButton="+ConfirmButton+"&PTORefId="+PTORefId+"&InterruptibleByRefId="+InterruptibleByRefId+"&TransportType="+TransportType+"&EmptyFullSolo="+EmptyFullSolo+"&Flexactivity="+Flexactivity+"&ISModification="+ISModification
        var url = projectname + "/Dashboard/SaveActivitiesRegistration?LayoutID=" + LayoutID + "&ACTReg=" + ACTReg
        return $http.post(url);
    };

    dataFactory.DeleteActivitiesRegistration = function (LayoutID, activityId) {

        return $http.post(projectname + '/Dashboard/DeleteActivitiesRegistration?LayoutID=' + LayoutID + "&activityId=" + activityId);

    };

    dataFactory.DeleteActivityNew = function (LayoutID, activityId) {

        return $http.post(projectname + '/Dashboard/DeleteActivityNew?LayoutID=' + LayoutID + "&activityId=" + activityId);

    };

    dataFactory.SaveTripDefault = function (LayoutID, OBCTypeId) {
        var url = projectname + "/Dashboard/SaveTripDefault?LayoutID=" + LayoutID + "&OBCTypeId=" + OBCTypeId;
        return $http.post(url);
    };

    dataFactory.DeletPlanningTrip = function (LayoutID, trip) {
        var url = projectname + "/Dashboard/DeletPlanningTrip?LayoutID=" + LayoutID + "&trip=" + trip
        return $http.post(url);
    };

    dataFactory.SetDefaultMainLanguage = function (layoutID, languageId) {
        var url = projectname + "/Dashboard/SetDefaultMainLanguage?layoutId=" + layoutID + "&languageId=" + languageId
        return $http.post(url);
    };
    dataFactory.DeleteUsedLanguage = function (layoutID, languageId) {
        var url = projectname + "/Dashboard/DeleteUsedLanguage?layoutId=" + layoutID + "&languageId=" + languageId
        return $http.post(url);
    };
    dataFactory.DeleteTransportType = function (layoutID, transporttypeId) {
        var url = projectname + "/Dashboard/DeleteTransportType?layoutId=" + layoutID + "&transporttypeId=" + transporttypeId
        return $http.post(url);
    };
    dataFactory.DeleteAllowance = function (layoutID, allowancemasterId) {
        var url = projectname + "/Dashboard/DeleteAllowance?LayoutId=" + layoutID + "&AllowanceMasterId=" + allowancemasterId
        return $http.post(url);
    };

    dataFactory.GetLayoutorgin = function (layoutId) {
        var url = projectname + "/Dashboard/GetLayoutorgin?layoutId=" + layoutId
        return $http.post(url);
    };

    dataFactory.GetLayoutfordrop = function () {
        var url = projectname + "/Dashboard/GetLayout"
        return $http.post(url);
    };

    dataFactory.RevertUsedLanguage = function (layoutId, languageId) {
        var url = projectname + "/Dashboard/RevertUsedLanguage?layoutId=" + layoutId + "&languageId=" + languageId
        return $http.post(url);
    };
    

    /* Dashboard page using C# Dashboard Controller End */


    /* Dashboard page using C# Dashboard Controller End */



    /* CopyLayout page using C# CopyLayout Controller Start */



    dataFactory.GetInfoColumnDetails = function (ToCustomerId, FromCustomerId) {
        var url = projectname + "/CopyLayout/GetInfoColumnDetails?ToCustomerId=" + ToCustomerId + "&FromCustomerId=" + FromCustomerId
        return $http.post(url);
    };

    dataFactory.GetExsistingLayoutName = function (CustomerId, LayoutName) {
        var url = projectname + "/CopyLayout/GetExsistingLayoutName?CustomerId=" + CustomerId + "&LayoutName=" + LayoutName;
        return $http.post(url);
    };

    dataFactory.GetCopyLayoutName = function (LayoutID) {
        var url = projectname + "/CopyLayout/GetCopyLayoutName?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.SaveCopyLayout = function (param1, param2, param3, param4, param5) {

        return $http.post(projectname + "/CopyLayout/SaveCopyLayout?LayoutId=" + param1 + "&NewLayoutName=" + param2 + "&CustomerId=" + param3 + "&LanguageIds=" + param4 + "&InfoColumn=" + param5);
    }

    /* CopyLayout page using C# CopyLayout Controller End */


    /* ScreenLayout page using C# ScreenLayout Controller End */

    dataFactory.RedirecttoScreenLayout = function (Layoutid) {
        var url = projectname + "/ScreenLayout/ScreenLayout?LayoutID=";
        $http.post(url).then(function () {
            return window.location.href = url + Layoutid;
        });
    };

    dataFactory.GetRegistration = function (LayoutID) {
        var url = projectname + "/ScreenLayout/GetRegistration?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetActivities = function (LayoutID) {
        var url = projectname + "/ScreenLayout/GetActivities?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetRegistrationPosition = function (LayoutID) {
        var url = projectname + "/ScreenLayout/GetRegistrationPosition?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetActivitiesPosition = function (LayoutID) {
        var url = projectname + "/ScreenLayout/GetActivitiesPosition?LayoutID=" + LayoutID
        return $http.post(url);
    };


    dataFactory.UpdateActivitiesRegistrationPosition = function (layoutId, actRegId) {
        //alert('entering');
        var url = projectname + "/ScreenLayout/UpdateActivitiesRegistrationPosition?layoutId=" + layoutId + "&actRegId=" + actRegId
        return $http.post(url);
    };

    dataFactory.UpdateDeActivitiesRegistrationPosition = function (layoutId, actRegId) {
        //alert(actRegId);
        var url = projectname + "/ScreenLayout/UpdateDeActivitiesRegistrationPosition?layoutId=" + layoutId + "&actRegId=" + actRegId
        return $http.post(url);
    };

    /* ScreenLayout page using C# ScreenLayout Controller End */


    /* ShowLayout page using C# ShowLayout Controller Start */

    dataFactory.RedirecttoShowLayout = function (Layoutid) {
        var url = projectname + "/ShowLayout/ShowLayout?LayoutID=" + Layoutid;
        return window.location.href = url;
    };

    dataFactory.GetLanguagesByLayoutId = function (LayoutId) {
        var url = projectname + "/ShowLayout/GetLanguagesByLayoutId?LayoutId=" + LayoutId
        return $http.post(url);
    };


    dataFactory.GetAllActivityRegistrationByLayoutId = function (LayoutID) {
        var url = projectname + "/ShowLayout/GetAllActivityandRegByLayoutId?LayoutID=" + LayoutID
        //alert(JSON.stringify($http.post(url)));
        return $http.post(url);
    };

    dataFactory.GetTransportTypes = function (layoutId) {
        var url = projectname + "/QuestionPath/GetTransportTypes?layoutId=" + layoutId
        return $http.post(url);
    };

    dataFactory.GetActivitiesList = function (LayoutID) {
        var url = projectname + "/QuestionPath/GetActivitiesList?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetAllShowLayoutView = function (activityId) {
        var url = projectname + "/ShowLayout/GetAllShowLayoutDetails?activityId=" + activityId
        //alert(JSON.stringify($http.post(url)));
        return $http.post(url);
    };
    dataFactory.GetShowLayotDataByLanguageId = function (langauage, layoutId, activitiesRegistrationId, loopId, endLoopId) {
        var url = projectname + "/ShowLayout/GetShowLayotDataByLanguageId?langauage=" + langauage + "&layoutId=" + layoutId + "&activitiesRegistrationId=" + activitiesRegistrationId + "&loopId=" + loopId + "&endLoopId=" + endLoopId;
        return $http.post(url);
    };
    dataFactory.GetActivitiesRegistrationByLayoutId = function (layoutId) {
        var url = projectname + "/ShowLayout/GetActivitiesRegistrationByLayoutId?layoutId=" + layoutId
        return $http.post(url);
    };



    /* ShowLayout page using C# ShowLayout Controller End */



    /* Translation page using C# Translation Controller End */

    dataFactory.RedirecttoTranslation = function (Layoutid) {

        var url = projectname + "/Translator/Translation?LayoutID=";
        $http.post(url).then(function () {
            return window.location.href = url + Layoutid;
        });
    };
    dataFactory.RedirecttoCentralTranslation = function () {

        var url = projectname + "/Translator/Index";
        $http.post(url).then(function () {
            return window.location.href = url;
        });
    };



    dataFactory.GetTranslation = function (LayoutID, tableid) {
        return $http.post(projectname + '/Translator/GetLangActivities?LayoutID=' + LayoutID + '&TableId=' + tableid);
    };

    dataFactory.GetRegistrationTranslation = function (LayoutID, tableid) {
        return $http.post(projectname + '/Translator/GetLangRegistration?LayoutID=' + LayoutID + '&TableId=' + tableid);
    };

    dataFactory.GetAllLangDetail = function (LayoutID, tableid) {
        return $http.post(projectname + '/Translator/GetAllLangDetail?LayoutID=' + LayoutID + '&TableId=' + tableid);
    };

    dataFactory.updatelangdetail = function (TableId, ColumnName, Id, Header, layoutId) {
        return $http.post(projectname + '/Translator/UpdateLangDetail?TableId=' + TableId + '&ColumnName=' + ColumnName + '&Id=' + Id + '&Header=' + Header + "&layoutId=" + layoutId);
    }

    dataFactory.addlanguage = function (LayoutID, activeLanguage, tableid) {
        return $http.post(projectname + '/Translator/AddLanguage?LayoutID=' + LayoutID + '&ActiveLanguage=' + activeLanguage + '&TableId=' + tableid);
    }

    dataFactory.saveactivitieslangdetail = function (paramdata) {
        return $http.post(projectname + '/Translator/SaveActivitiesLangDetail?paramdata=' + paramdata);
    }
    dataFactory.saveregistrationlangdetail = function (paramdata) {
        return $http.post(projectname + '/Translator/SaveRegistrationLangDetail?paramdata=' + paramdata);
    }

    dataFactory.savealllangdetail = function (paramdata) {

        return $http.post(projectname + '/Translator/SaveAllLangDetail?paramdata=' + paramdata);
    }

    dataFactory.saveInfoMessageTitleLangDetail = function (paramdata) {

        return $http.post(projectname + '/Translator/SaveInfoMessageTitleLangDetail?paramdata=' + paramdata);
    }

    dataFactory.saveInfoMessageContentLangDetail = function (paramdata) {

        return $http.post(projectname + '/Translator/SaveInfoMessageContentLangDetail?paramdata=' + paramdata);
    }


    dataFactory.saveUsedLanguages = function (paramdata) {
        return $http.post(projectname + '/Translator/SaveUsedLanguage?paramdata=' + paramdata);
    }

    dataFactory.copyTranslationFromCustomerSpecificToGeneral = function (langdetailid) {
        return $http.post(projectname + '/Translator/CopyTranslationFromCustomerSpecificToGeneral?langDetailId=' + langdetailid);
    }

    dataFactory.DeleteGeneralTranslation = function (langdetailid) {
        return $http.post(projectname + '/Translator/DeleteGeneralTranslation?langDetailId=' + langdetailid);
    }


    /* QuestionPath page using C# QuestionPath Controller Start */


    dataFactory.GetCustomerIdByLayoutId = function (layoutid) {
        var url = projectname + "/QuestionPath/GetCustomerIdByLayoutId?layoutId=" + layoutid
        return $http.post(url)
    };

    dataFactory.GetReadoutDetail = function (tockenid, layoutid) {

        var url = projectname + "/QuestionPath/GetReadoutDetail?TockenId=" + tockenid + "&LayoutId=" + layoutid
        return $http.post(url)
    };
    dataFactory.GetpReadoutDetail = function (planningselectrefid, layoutid) {
        var url = projectname + "/QuestionPath/GetpReadoutDetail?PlanningSelectRefId=" + planningselectrefid + "&LayoutId=" + layoutid
        return $http.post(url)
    };





    dataFactory.DeleteSpecificQuestionTree = function (questionid) {
        var url = projectname + "/QuestionPath/DeleteSpecificQuestionTree?QuestionID=" + questionid
        return $http.post(url)
    };
    //selvam
    dataFactory.GetCehckOffPlanningLevel = function (LayoutId) {
        var url = projectname + "/QuestionPath/GetCehckOffPlanningLevel?LayoutId=" + LayoutId
        return $http.post(url)
    };
    //selvam -end
    dataFactory.GetEmptyFullSoloByActivitiesRegistration = function (ActivitiyID) {
        var url = projectname + "/QuestionPath/GetEmptyFullSoloByActivitiesRegistration?ActivityID=" + ActivitiyID
        return $http.post(url)
    };
    dataFactory.GetTransportTypeByActivitiesRegistration = function (ActivitiyID) {
        var url = projectname + "/QuestionPath/GetTransportTypeByActivitiesRegistration?ActivityID=" + ActivitiyID
        return $http.post(url)
    };

    dataFactory.RedirectToQuestionPath = function (LayoutID, ActivitiyID) {
        //alert(LayoutID);
        var url = projectname + "/QuestionPath/QuestionPath?LayoutID=" + LayoutID + "&ActivityID=" + ActivitiyID
        return window.location.href = url;
    };

    dataFactory.GetSetActionValueType = function (layoutId) {
        var url = projectname + "/QuestionPath/GetSetActionValueType?layoutId=" + layoutId;
        return $http.post(url)
    };
    dataFactory.GetQuestionPathToolBox = function (layoutId) {
        var url = projectname + "/QuestionPath/GetToolBox?layoutId=" + layoutId;
        return $http.post(url)
    };

    dataFactory.GetToolBoxParentNodes = function () {
        var url = projectname + "/QuestionPath/GetToolBoxParentNodes"
        return $http.post(url)
    };

    dataFactory.GetToolBoxChildNodes = function () {
        var url = projectname + "/QuestionPath/GetToolBoxChildNodes"
        return $http.post(url)
    };
    dataFactory.GetQuestions = function (LayoutID, activityId, transportId, questionRefId) {
        var url = projectname + "/QuestionPath/GetQuestions?layoutId=" + LayoutID + "&activityId=" + activityId + "&transportId=" + transportId + "&questionRefId=" + questionRefId;
        return $http.post(url);
    };

    dataFactory.FlexGetQuestions = function (LayoutID, activityId, transportId, questionId) {
        var url = projectname + "/QuestionPath/FlexGetQuestions?layoutId=" + LayoutID + "&activityId=" + activityId + "&transportId=" + transportId + "&questionId=" + questionId;
        return $http.post(url);
    };

    dataFactory.GetSmartQuestions = function (LayoutID, activityId, transportId) {
        var url = projectname + "/QuestionPath/GetSmartQuestions?layoutId=" + LayoutID + "&activityId=" + activityId + "&transportId=" + transportId
        return $http.post(url);
    };

    dataFactory.SaveQuestions = function (LayoutID, transportTypeId, Question, propertyName) {
        var url = projectname + "/QuestionPath/SaveQuestions?LayoutID=" + LayoutID + "&transportTypeId=" + transportTypeId + "&Question=" + Question + "&propertyName=" + propertyName
        return $http.post(url);
    };

    dataFactory.SaveQuestionsDefault = function (LayoutID, transportTypeId, Question, activityId) {
        var url = projectname + "/QuestionPath/SaveQuestionsDefault?LayoutID=" + LayoutID + "&transportTypeId=" + transportTypeId + "&Question=" + Question + "&activityId=" + activityId
        return $http.post(url);
    };

    dataFactory.GetEntryProperties = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetEntryProperties?questionRefId=" + questionRefId + "&layoutId=" + layoutId;
        return $http.post(url);
    };

    dataFactory.GetParticularActivitiesRegistration = function (activitiesId) {
        var url = projectname + "/QuestionPath/GetParticularActivitiesRegistration?activitiesId=" + activitiesId
        return $http.post(url);
    };

    dataFactory.GetDocumetScanProperties = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetDocumetScanProperties?questionRefId=" + questionRefId + "&layoutId=" + layoutId;
        return $http.post(url);
    };
    dataFactory.GetVariableListProperties = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetVariableListProperties?questionRefId=" + questionRefId + "&layoutId=" + layoutId;
        return $http.post(url);
    };
    dataFactory.GetTextMessageProperties = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetTextMessageProperties?questionRefId=" + questionRefId + "&layoutId=" + layoutId;
        return $http.post(url);
    };
    dataFactory.GetInfomessageProperties = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetInfomessageProperties?questionRefId=" + questionRefId + "&layoutId=" + layoutId;
        return $http.post(url);
    };

    dataFactory.GetCheckoffProperties = function (questionRefId) {
        var url = projectname + "/QuestionPath/GetCheckoffProperties?questionRefId=" + questionRefId
        return $http.post(url);
    };
    dataFactory.GetPlanningselectProperties = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetPlanningselectProperties?questionRefId=" + questionRefId + "&layoutId=" + layoutId
        return $http.post(url);
    };

    dataFactory.GetSetActionValueProperties = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetSetActionValueProperties?questionRefId=" + questionRefId + "&layoutId=" + layoutId
        return $http.post(url);
    };

    dataFactory.GetChioceProperties = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetChioceProperties?questionRefId=" + questionRefId + "&layoutId=" + layoutId
        return $http.post(url);
    };

    dataFactory.GetConditionalProperties = function (questionRefId) {
        var url = projectname + "/QuestionPath/GetConditionalProperties?questionRefId=" + questionRefId
        return $http.post(url);
    };




    dataFactory.GetFlexForActivities = function (questionRefId) {
        var url = projectname + "/QuestionPath/GetFlexForActivities?questionRefId=" + questionRefId
        return $http.post(url);
    };

    dataFactory.DeleteReadoutDetail = function (readoutdetailid) {
        var url = projectname + "/QuestionPath/DeleteReadoutDetail?ReadoutDetailId=" + readoutdetailid
        return $http.post(url);
    };



    dataFactory.DeletePlanningSelectReadoutDetail = function (readoutdetailid) {
        var url = projectname + "/QuestionPath/DeletePlanningSelectReadoutDetail?PlanningSelectReadoutDetailId=" + readoutdetailid
        return $http.post(url);
    };



    dataFactory.EditReadoutDetail = function (readoutdetailid) {
        var url = projectname + "/QuestionPath/EditReadoutDetail?ReadoutDetailId=" + readoutdetailid
        return $http.post(url);
    };

    dataFactory.EditPlanningSelectReadoutDetail = function (readoutdetailid) {
        var url = projectname + "/QuestionPath/EditPlanningSelectReadoutDetail?PlanningSelectReadoutDetailId=" + readoutdetailid
        return $http.post(url);
    };

    dataFactory.SaveAddChioceProperties = function (AddChioce, layoutId, activityId, transportTypeId) {
        var url = projectname + "/QuestionPath/SaveAddChioceProperties?layoutId=" + LayoutID + "&activityId=" + ActivityID + "&transportTypeId=" + transportTypeId + "&questionidchoice=" + questionidchoice + "&AddChioce=" + AddChioce
        return $http.post(url);
    };

    dataFactory.SaveReadoutDetail = function (readoutDetail, layoutid, activityid, aliasname) {
        var url = projectname + "/QuestionPath/SaveReadoutDetail?ReadoutDetail=" + readoutDetail + "&LayoutRefId=" + layoutid + "&ActivitiesRefId=" + activityid + "&AliasName=" + aliasname;
        return $http.post(url);
    };

    dataFactory.UpdatePlanningSelectReadoutDetail = function (readoutDetail, layoutid, activityid, aliasname) {
        var url = projectname + "/QuestionPath/UpdatePlanningSelectReadoutDetail?ReadoutDetail=" + readoutDetail + "&LayoutRefId=" + layoutid + "&ActivitiesRefId=" + activityid + "&AliasName=" + aliasname;
        return $http.post(url);
    };

    dataFactory.UpdateDocumetScanProperties = function (DocumetScanProperties) {
        var url = projectname + "/QuestionPath/UpdateDocumetScanProperties?DocumetScanProperties=" + DocumetScanProperties
        return $http.post(url);
    };

    dataFactory.UpdateConditionalProperties = function (ConditionalProperties, pConditionalActionnodeid) {
        var url = projectname + "/QuestionPath/UpdateConditionalProperties?ConditionalProperties=" + ConditionalProperties + "&pConditionalActionnodeid=" + pConditionalActionnodeid
        return $http.post(url);
    };


    dataFactory.UpdateEntryProperties = function (EntryPropertiesProperties, pnodeid) {
        var url = projectname + "/QuestionPath/UpdateEntryProperties?EntryPropertiesProperties=" + EntryPropertiesProperties + "&pnodeid=" + pnodeid
        return $http.post(url);
    };


    dataFactory.UpdateVariableListProperties = function (VariableListProperties, pVariableListnodeid) {
        var url = projectname + "/QuestionPath/UpdateVariableListProperties?VariableListProperties=" + VariableListProperties + "&pVariableListnodeid=" + pVariableListnodeid
        return $http.post(url);
    };

    dataFactory.UpdateTextMessageProperties = function (TextMessageProperties) {
        var url = projectname + "/QuestionPath/UpdateTextMessageProperties?TextMessageProperties=" + TextMessageProperties
        return $http.post(url);
    };

    dataFactory.UpdateInfomessageProperties = function (InfomessageProperties, pInfomessagenodeid) {
        var url = projectname + "/QuestionPath/UpdateInfomessageProperties?InfomessageProperties=" + InfomessageProperties + "&pInfomessagenodeid=" + pInfomessagenodeid
        return $http.post(url);
    };

    dataFactory.UpdateCheckoffProperties = function (CheckoffProperties, pCheckoffnodeid) {
        var url = projectname + "/QuestionPath/UpdateCheckoffProperties?CheckoffProperties=" + CheckoffProperties + "&pCheckoffnodeid=" + pCheckoffnodeid
        return $http.post(url);
    };

    dataFactory.UpdatePlanningselectProperties = function (PlanningselectProperties, pPlanningselectnodeid) {
        var url = projectname + "/QuestionPath/UpdatePlanningselectProperties?PlanningselectProperties=" + PlanningselectProperties + "&pPlanningselectnodeid=" + pPlanningselectnodeid
        return $http.post(url);
    };

    dataFactory.UpdateSetActionValueProperties = function (SetActionValueProperties, pSetactionnodeid) {
        var url = projectname + "/QuestionPath/UpdateSetActionValueProperties?SetActionValueProperties=" + SetActionValueProperties + "&pSetactionnodeid=" + pSetactionnodeid
        return $http.post(url);
    };

    dataFactory.UpdateChioceProperties = function (ChioceProperties) {
        var url = projectname + "/QuestionPath/UpdateChioceProperties?ChioceProperties=" + ChioceProperties
        return $http.post(url);
    };
    dataFactory.UpdateTransportTypeProperties = function (TransportTypeProperties) {
        var url = projectname + "/QuestionPath/UpdateTransportTypeProperties?TransportTypeProperties=" + TransportTypeProperties
        return $http.post(url);
    };



    dataFactory.UpdateAddChioceProperties = function (AddChioceProperties) {
        var url = projectname + "/QuestionPath/UpdateAddChioceProperties?AddChioceProperties=" + AddChioceProperties
        return $http.post(url);
    };


    dataFactory.UpdateFlex = function (Flex, pFlexnodeid) {
        var url = projectname + "/QuestionPath/UpdateFlex?Flex=" + Flex + "&pFlexnodeid=" + pFlexnodeid
        return $http.post(url);
    };

    dataFactory.GetQuestionOBCTypes = function (LayoutID) {
        var url = projectname + "/QuestionPath/OBCTypes?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetQuestionAllowancesDescription = function (LayoutID) {
        var url = projectname + "/QuestionPath/GetAllowancesDescription?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.GetQuestionInfoColumns = function (LayoutID) {
        var url = projectname + "/QuestionPath/GetInfoColumns?LayoutID=" + LayoutID
        return $http.post(url);
    };


    dataFactory.ISPlanningType = function (LayoutID) {
        var url = projectname + "/QuestionPath/ISPlanningType?LayoutID=" + LayoutID
        return $http.post(url);
    };

    dataFactory.UpdateDescription = function (Description) {
        var url = projectname + "/QuestionPath/UpdateDescription?Description=" + Description
        return $http.post(url);
    };



    dataFactory.GetAddChioceProperties = function (ChioceRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetAddChioceProperties?ChioceRefId=" + ChioceRefId + "&layoutId=" + layoutId;
        return $http.post(url);
    };

    dataFactory.GetAddChiocePopup = function (ChioceRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetAddChiocePopup?ChioceRefId=" + ChioceRefId + "&layoutId=" + layoutId;
        return $http.post(url);
    };


    dataFactory.GetAliasNameDetails = function (activitiesId, layoutId) {
        var url = projectname + "/QuestionPath/GetAliasNameDetails?activitiesId=" + activitiesId + "&layoutId=" + layoutId
        return $http.post(url);
    };

    dataFactory.GetAliasNameDetailsForPSReadOut = function (activitiesId, layoutId) {
        var url = projectname + "/QuestionPath/GetAliasNameDetailsForPSReadOut?activitiesId=" + activitiesId + "&layoutId=" + layoutId
        return $http.post(url);
    };


    dataFactory.GetAliasNameDetailsByLayout = function (layoutId) {
        var url = projectname + "/QuestionPath/GetAliasNameDetailsByLayout?layoutId=" + layoutId
        return $http.post(url);
    };

    dataFactory.GetConditionCompareMethodWithOutSavedVal = function () {
        var url = projectname + "/QuestionPath/GetConditionCompareMethodWithOutSavedVal"
        return $http.post(url);
    };
    dataFactory.GetConditionCompareMethodWithSavedVal = function () {
        var url = projectname + "/QuestionPath/GetConditionCompareMethodWithSavedVal"
        return $http.post(url);
    };

    dataFactory.GetConditionActionCompareMethodWithSavedVal = function () {
        var url = projectname + "/QuestionPath/GetConditionActionCompareMethodWithSavedVal"
        return $http.post(url);
    };
    dataFactory.GetConditionActionCompareMethodWithOutSavedVal = function () {
        var url = projectname + "/QuestionPath/GetConditionActionCompareMethodWithOutSavedVal"
        return $http.post(url);
    };


    dataFactory.GetConditionCompareTo = function () {
        var url = projectname + "/QuestionPath/GetConditionCompareTo"
        return $http.post(url);
    };
    dataFactory.GetConditionCompareToNotSavedValue = function () {
        var url = projectname + "/QuestionPath/GetConditionCompareToNotSavedValue"
        return $http.post(url);
    };
    dataFactory.GetConditionCompareToSavedValue = function () {
        var url = projectname + "/QuestionPath/GetConditionCompareToSavedValue"
        return $http.post(url);
    };

    dataFactory.GetConditionvalue = function (layoutId) {
        var url = projectname + "/QuestionPath/GetConditionvalue?layoutId=" + layoutId
        return $http.post(url);
    };


    dataFactory.GetInfoColumnAction = function () {
        var url = projectname + "/QuestionPath/GetInfoColumnAction"
        return $http.post(url);
    };

    dataFactory.DeleteAliasName = function (savevalue) {
        var url = projectname + "/QuestionPath/DeleteAliasName?savevalue=" + savevalue
        return $http.post(url);
    };

    dataFactory.DeleteAddChioceProperties = function (AddChioceId) {
        var url = projectname + "/QuestionPath/DeleteAddChioceProperties?AddChioceId=" + AddChioceId
        return $http.post(url);
    };

    dataFactory.GetConditionItemvalue = function (LayoutRefId) {
        var url = projectname + "/QuestionPath/GetConditionItemvalue?LayoutRefId=" + LayoutRefId
        return $http.post(url);
    };

    //dataFactory.GetSpecificQuestionTree = function (QuestionID) {
    //    var url = projectname + "/QuestionPath/GetSpecificQuestionTree?QuestionID=" + QuestionID
    //    return $http.post(url);
    //};

    dataFactory.GetPlanningTypedata = function (Layout_ID) {
        var url = projectname + "/QuestionPath/GetPlanningType?Layout_ID=" + Layout_ID
        return $http.post(url);
    };

    dataFactory.GetPlanningTypeLevelMaster = function () {
        var url = projectname + "/QuestionPath/GetPlanningTypeLevelMaster"
        return $http.post(url);
    };

    dataFactory.PlanningFeedbackLeveldropdowns = function (ActivityRefId) {
        var url = projectname + "/QuestionPath/PlanningFeedbackLevel?ActivityRefId=" + ActivityRefId
        return $http.post(url);
    };


    dataFactory.GetUsedPlanningType = function (Layout_ID) {
        var url = projectname + "/QuestionPath/GetUsedPlanningType?Layout_ID=" + Layout_ID
        return $http.post(url);
    };


    dataFactory.GetConFilter = function () {
        var url = projectname + "/QuestionPath/GetConFilter"
        return $http.post(url);
    };
    dataFactory.GetInfoMessageIconType = function () {
        var url = projectname + "/QuestionPath/GetInfoMessageIconType"
        return $http.post(url);
    };

    dataFactory.GetConPSCompareto = function () {
        var url = projectname + "/QuestionPath/GetConPSCompareto"
        return $http.post(url);
    };

    dataFactory.GetReadoutValue = function () {
        var url = projectname + "/QuestionPath/GetReadoutValue"
        return $http.post(url);
    };


    dataFactory.GetActivitiesRegistrationRecive = function (LayoutID) {
        var url = projectname + "/QuestionPath/GetActivitiesRegistrationRecive?LayoutID=" + LayoutID
        return $http.post(url);
    };


    dataFactory.GetFlexPalletsForActivities = function (layOutRefID, activitiyRefID, QuesstionRefID) {
        var url = projectname + "/QuestionPath/GetFlexPalletsForActivities?layOutRefID=" + layOutRefID + "&activitiyRefID=" + activitiyRefID + "&QuesstionRefID=" + QuesstionRefID
        return $http.post(url);
    };
    dataFactory.InsertFlexPallets = function (questionidPallet, Pallets) {
        var url = projectname + "/QuestionPath/InsertFlexPallets?questionidPallet=" + questionidPallet + "&Pallets=" + Pallets
        return $http.post(url);
    };
    dataFactory.UpdateFlexPallets = function (Pallets) {
        var url = projectname + "/QuestionPath/UpdateFlexPallets?Pallets=" + Pallets
        return $http.post(url);
    };
    dataFactory.DeleteFlexPallets = function (PalletsID) {
        var url = projectname + "/QuestionPath/DeleteFlexPallets?PalletsID=" + PalletsID
        return $http.post(url);
    };

    dataFactory.GetFlexProblemsForActivities = function (layOutRefID, activitiyRefID, QuesstionRefID) {
        var url = projectname + "/QuestionPath/GetFlexProblemsForActivities?layOutRefID=" + layOutRefID + "&activitiyRefID=" + activitiyRefID + "&QuesstionRefID=" + QuesstionRefID
        return $http.post(url);
    };
    dataFactory.InsertFlexProblems = function (questionidProblem, Problems) {
        var url = projectname + "/QuestionPath/InsertFlexProblems?questionidProblem=" + questionidProblem + "&Problems=" + Problems
        return $http.post(url);
    };
    dataFactory.UpdateFlexProblems = function (Problems) {
        var url = projectname + "/QuestionPath/UpdateFlexProblems?Problems=" + Problems
        return $http.post(url);
    };
    dataFactory.DeleteFlexProblems = function (ProblemsID) {
        var url = projectname + "/QuestionPath/DeleteFlexProblems?ProblemsID=" + ProblemsID
        return $http.post(url);
    };


    dataFactory.GetFlexExtraInfoForActivities = function (layOutRefID, activitiyRefID, QuesstionRefID) {
        var url = projectname + "/QuestionPath/GetFlexExtraInfoForActivities?layOutRefID=" + layOutRefID + "&activitiyRefID=" + activitiyRefID + "&QuesstionRefID=" + QuesstionRefID
        return $http.post(url);
    };
    dataFactory.InsertFlexExtraInfo = function (questionidExtra, ExtraInfo) {
        var url = projectname + "/QuestionPath/InsertFlexExtraInfo?questionidExtra=" + questionidExtra + "&ExtraInfo=" + ExtraInfo
        return $http.post(url);
    };
    dataFactory.UpdateFlexExtraInfo = function (ExtraInfo) {
        var url = projectname + "/QuestionPath/UpdateFlexExtraInfo?ExtraInfo=" + ExtraInfo
        return $http.post(url);
    };
    dataFactory.DeleteFlexExtraInfo = function (ExtraInfoID) {
        var url = projectname + "/QuestionPath/DeleteFlexExtraInfo?ExtraInfoID=" + ExtraInfoID
        return $http.post(url);
    };


    dataFactory.GetFlexSignaturesForActivities = function (layOutRefID, activitiyRefID, QuesstionRefID) {
        var url = projectname + "/QuestionPath/GetFlexSignaturesForActivities?layOutRefID=" + layOutRefID + "&activitiyRefID=" + activitiyRefID + "&QuesstionRefID=" + QuesstionRefID
        return $http.post(url);
    };
    dataFactory.InsertFlexSignatures = function (questionidSignatures, Signatures) {
        var url = projectname + "/QuestionPath/InsertFlexSignatures?questionidSignatures=" + questionidSignatures + "&Signatures=" + Signatures
        return $http.post(url);
    };
    dataFactory.UpdateFlexSignatures = function (Signatures) {
        var url = projectname + "/QuestionPath/UpdateFlexSignatures?Signatures=" + Signatures
        return $http.post(url);
    };
    dataFactory.DeleteFlexSignatures = function (SignaturesID) {
        var url = projectname + "/QuestionPath/DeleteFlexSignatures?SignaturesID=" + SignaturesID
        return $http.post(url);
    };


    dataFactory.GetFlexPicureForActivities = function (layOutRefID, activitiyRefID, QuesstionRefID) {
        var url = projectname + "/QuestionPath/GetFlexPicureForActivities?layOutRefID=" + layOutRefID + "&activitiyRefID=" + activitiyRefID + "&QuesstionRefID=" + QuesstionRefID
        return $http.post(url);
    };
    dataFactory.InsertFlexPicure = function (Picure) {
        var url = projectname + "/QuestionPath/InsertFlexPicure?Picure=" + Picure
        return $http.post(url);
    };
    dataFactory.UpdateFlexPicure = function (Picure) {
        var url = projectname + "/QuestionPath/UpdateFlexPicure?Picure=" + Picure
        return $http.post(url);
    };
    dataFactory.DeleteFlexPicure = function (PicureID) {
        var url = projectname + "/QuestionPath/DeleteFlexPicure?PicureID=" + PicureID
        return $http.post(url);
    };


    dataFactory.GetCurrentActivitiesRegistration = function (activityRegistrationID) {
        var url = projectname + "/QuestionPath/GetCurrentActivitiesRegistration?activityRegistrationID=" + activityRegistrationID
        return $http.post(url);
    };

    dataFactory.PlanningFeedbackCodedropdowns = function (layoutId) {
        var url = projectname + "/QuestionPath/GetPlanningFeedbackCode?LayoutId=" + layoutId
        return $http.post(url);
    };
    dataFactory.GetChoicePlanningFeedbackCode = function (layoutId) {
        var url = projectname + "/QuestionPath/GetChoicePlanningFeedbackCode?LayoutId=" + layoutId
        return $http.post(url);
    };
    dataFactory.GetTransportTypePlanningFeedbackCode = function (layoutId) {
        var url = projectname + "/QuestionPath/GetTransportTypePlanningFeedbackCode?LayoutId=" + layoutId
        return $http.post(url);
    };

    dataFactory.GetAllDocumentType = function () {
        var url = projectname + "/QuestionPath/GetAllDocumentType";
        return $http.post(url);
    };
    dataFactory.GetAddDocumentScanProperties = function (questioniddocumentscan) {
        var url = projectname + "/QuestionPath/GetAddDocumentScanProperties?DocumentScanTitleId=" + questioniddocumentscan;
        return $http.post(url);
    };



    dataFactory.GetTransportTypePropertiesDetail = function (transportTypeRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetTransportTypePropertiesDetail?transportTypeRefId=" + transportTypeRefId + "&layoutId=" + layoutId;
        return $http.post(url);
    };


    dataFactory.GetTransportTypeProperties = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetTransportTypeProperties?questionRefId=" + questionRefId + "&layoutId=" + layoutId
        return $http.post(url);
    };


    /* QuestionPath page using C# QuestionPath Controller End */

    dataFactory.GetUsersList = function () {
        var url = projectname + "/Users/GetUsersList";
        return $http.post(url);
    };

    dataFactory.SaveUsers = function (data) {

        var url = projectname + "/SaveUsers?userData=" + data;
        return $http.post(url);
    };


    dataFactory.GetOBCTypeMasterlist = function () {
        return $http.post(projectname + "GetOBCTypeMaster");
    };
    dataFactory.GoObcType = function () {
        var url = projectname + "/OBCTypeMaster/Index"
        return window.location.href = url;
    };
    dataFactory.GoAllowanceMaster = function () {
        var url = projectname + "/AllowanceMaster/Index"
        return window.location.href = url;
    };
    dataFactory.GoLanguageMaster = function () {
        var url = projectname + "/LanguageMaster/Index"
        return window.location.href = url;
    };
    dataFactory.GoCustomer = function () {
        var url = projectname + "/Customer/Index"
        return window.location.href = url;
    };
    dataFactory.GoPlanningMaster = function () {
        var url = projectname + "/PlanningMaster/Index"
        return window.location.href = url;
    };
    dataFactory.GoUsers = function () {
        var url = projectname + "/Users/Index"
        return window.location.href = url;
    };
    dataFactory.GoWorkingCode = function () {
        var url = projectname + "/WorkingCode/Index"
        return window.location.href = url;
    };
    dataFactory.GoInput = function () {
        var url = projectname + "/InputMaster/Index"
        return window.location.href = url;
    };
    dataFactory.GoPlanningandPartnerCodeOverview = function () {
        var url = projectname + "/PlanningandPartnerCodeOverview/Index"
        return window.location.href = url;
    };
    dataFactory.SaveInputMaster = function (inList) {
        var datalist = inList;
        var url = projectname + "/SaveInputMaster?query=" + datalist;
        return $http.post(url);
    };

    dataFactory.GoDocumentType = function () {
        var url = projectname + "/DocumentTypeMaster/Index"
        return window.location.href = url;
    };

    dataFactory.pastequestion = function (ActivityID, LayoutID) {
        var url = projectname + "/Pastequestion?ActivityID=" + ActivityID + "&LayoutID=" + LayoutID;
        return $http.post(url);
    };



    dataFactory.UpdateExtraInfoProperties = function (ExtraInfoProperties, pExtraInfoPropertiesnodeid) {
        var url = projectname + "/QuestionPath/UpdateExtraInfoProperties?ExtraInfoProperties=" + ExtraInfoProperties + "&pExtraInfoPropertiesnodeid=" + pExtraInfoPropertiesnodeid
        return $http.post(url);
    };


    dataFactory.UpdatePalletsProperties = function (PalletsProperties, pPalletsPropertiesnodeid) {
        var url = projectname + "/QuestionPath/UpdatePalletsProperties?PalletsProperties=" + PalletsProperties + "&pPalletsPropertiesnodeid=" + pPalletsPropertiesnodeid
        return $http.post(url);
    };

    dataFactory.UpdateProblemsProperties = function (ProblemsProperties, pProblemsPropertiesnodeid) {
        var url = projectname + "/QuestionPath/UpdateProblemsProperties?ProblemsProperties=" + ProblemsProperties + "&pProblemsPropertiesnodeid=" + pProblemsPropertiesnodeid
        return $http.post(url);
    };

    dataFactory.UpdateSignaturesProperties = function (SignaturesProperties, pSignaturesPropertiesnodeid) {
        var url = projectname + "/QuestionPath/UpdateSignaturesProperties?SignaturesProperties=" + SignaturesProperties + "&pSignaturesPropertiesnodeid=" + pSignaturesPropertiesnodeid
        return $http.post(url);
    };

    dataFactory.UpdatePictureProperties = function (PictureProperties, pPicturePropertiesnodeid) {
        var url = projectname + "/QuestionPath/UpdatePictureProperties?PictureProperties=" + PictureProperties + "&pPicturePropertiesnodeid=" + pPicturePropertiesnodeid
        return $http.post(url);
    };


    dataFactory.GetExtraInfoProperties = function (questionRefId, layoutRefId) {
        var url = projectname + "/QuestionPath/GetExtraInfoProperties?questionRefId=" + questionRefId + "&layoutRefId=" + layoutRefId;
        return $http.post(url);
    };

    dataFactory.GetPalletsProperties = function (questionRefId, layoutRefId) {
        var url = projectname + "/QuestionPath/GetPalletsProperties?questionRefId=" + questionRefId + "&layoutRefId=" + layoutRefId;
        return $http.post(url);
    };


    dataFactory.GetProblemsProperties = function (questionRefId, layoutRefId) {
        var url = projectname + "/QuestionPath/GetProblemsProperties?questionRefId=" + questionRefId + "&layoutRefId=" + layoutRefId;
        return $http.post(url);
    };

    dataFactory.GetSignaturesProperties = function (questionRefId, layoutRefId) {
        var url = projectname + "/QuestionPath/GetSignaturesProperties?questionRefId=" + questionRefId + "&layoutRefId=" + layoutRefId;
        return $http.post(url);
    };
    dataFactory.GetPictureProperties = function (questionRefId, layoutRefId) {
        var url = projectname + "/QuestionPath/GetPictureProperties?questionRefId=" + questionRefId + "&layoutRefId=" + layoutRefId;
        return $http.post(url);
    };

    dataFactory.GetPicure = function (questionRefId) {
        var url = projectname + "/QuestionPath/GetPicure?questionRefId=" + questionRefId
        return $http.post(url);
    };



    dataFactory.GetSmartExtraInfoForActivities = function (layOutRefID, activitiyRefID, QuesstionRefID) {
        var url = projectname + "/QuestionPath/GetSmartExtraInfoForActivities?layOutRefID=" + layOutRefID + "&activitiyRefID=" + activitiyRefID + "&QuesstionRefID=" + QuesstionRefID
        return $http.post(url);
    };
    dataFactory.InsertSmartExtraInfo = function (questionidExtra, ExtraInfo) {
        var url = projectname + "/QuestionPath/InsertSmartExtraInfo?questionidExtra=" + questionidExtra + "&ExtraInfo=" + ExtraInfo
        return $http.post(url);
    };
    dataFactory.UpdateSmartExtraInfo = function (ExtraInfo) {
        var url = projectname + "/QuestionPath/UpdateSmartExtraInfo?ExtraInfo=" + ExtraInfo
        return $http.post(url);
    };
    dataFactory.DeleteSmartExtraInfo = function (ExtraInfoID) {
        var url = projectname + "/QuestionPath/DeleteSmartExtraInfo?ExtraInfoID=" + ExtraInfoID
        return $http.post(url);
    };



    dataFactory.GetSmartPalletsForActivities = function (layOutRefID, activitiyRefID, QuesstionRefID) {
        var url = projectname + "/QuestionPath/GetSmartPalletsForActivities?layOutRefID=" + layOutRefID + "&activitiyRefID=" + activitiyRefID + "&QuesstionRefID=" + QuesstionRefID
        return $http.post(url);
    };
    dataFactory.InsertSmartPallets = function (questionidPallet, Pallets) {
        var url = projectname + "/QuestionPath/InsertSmartPallets?questionidPallet=" + questionidPallet + "&Pallets=" + Pallets
        return $http.post(url);
    };
    dataFactory.UpdateSmartPallets = function (Pallets) {
        var url = projectname + "/QuestionPath/UpdateSmartPallets?Pallets=" + Pallets
        return $http.post(url);
    };
    dataFactory.DeleteSmartPallets = function (PalletsID) {
        var url = projectname + "/QuestionPath/DeleteSmartPallets?PalletsID=" + PalletsID
        return $http.post(url);
    };



    dataFactory.GetSmartProblemsForActivities = function (layOutRefID, activitiyRefID, QuesstionRefID) {
        var url = projectname + "/QuestionPath/GetSmartProblemsForActivities?layOutRefID=" + layOutRefID + "&activitiyRefID=" + activitiyRefID + "&QuesstionRefID=" + QuesstionRefID
        return $http.post(url);
    };
    dataFactory.InsertSmartProblems = function (questionidProblem, Problems) {
        var url = projectname + "/QuestionPath/InsertSmartProblems?questionidProblem=" + questionidProblem + "&Problems=" + Problems
        return $http.post(url);
    };
    dataFactory.UpdateSmartProblems = function (Problems) {
        var url = projectname + "/QuestionPath/UpdateSmartProblems?Problems=" + Problems
        return $http.post(url);
    };
    dataFactory.DeleteSmartProblems = function (ProblemsID) {
        var url = projectname + "/QuestionPath/DeleteSmartProblems?ProblemsID=" + ProblemsID
        return $http.post(url);
    };


    dataFactory.GetCopyPaste = function () {
        var url = projectname + "/QuestionPath/GetCopyPaste"
        return $http.post(url);
    };
    dataFactory.DeleteCopyPaste = function () {
        var url = projectname + "/QuestionPath/DeleteCopyPaste"
        return $http.post(url);
    };
    dataFactory.InsertCopyPaste = function (CopyPaste) {
        var url = projectname + "/QuestionPath/InsertCopyPaste?CopyPaste=" + CopyPaste
        return $http.post(url);
    };

    dataFactory.Pastecopiedquestions = function (layoutid, activityid, transportid, selectedNode) {
        var url = projectname + "/QuestionPath/Pastecopiedquestions?layoutid=" + layoutid + "&activityid=" + activityid + "&transportid=" + transportid + "&selectedNode=" + selectedNode
        return $http.post(url);
    };


    dataFactory.LayoutInfoColumnDetails = function (layouid) {
        var url = projectname + "/QuestionPath/LayoutInfoColumnDetails?layouid=" + layouid
        return $http.post(url);
    };


    dataFactory.GetLayoutforCopy = function (CustomerRefId, obctypeid) {
        var url = projectname + "/QuestionPath/GetLayoutforCopy?CustomerRefId=" + CustomerRefId + "&obctypeid=" + obctypeid
        return $http.post(url);
    };

    dataFactory.GetLayoutforCopywithoutCustomer = function () {
        var url = projectname + "/QuestionPath/GetLayoutforCopywithoutCustomer"
        return $http.post(url);
    };

    dataFactory.GetActivitieslistCopy = function (LayoutId) {
        var url = projectname + "/QuestionPath/GetActivitieslistCopy?LayoutId=" + LayoutId
        return $http.post(url);
    };

    dataFactory.GetTransporttypelistCopy = function (LayoutId, activityid) {
        var url = projectname + "/QuestionPath/GetTransporttypelistCopy?LayoutId=" + LayoutId + "&activityid=" + activityid
        return $http.post(url);
    };

    // Code added for Divakar - If value is 'Next Qp' and Start , End and New field will not be shown in select QP dropdownlist
    dataFactory.GetActivitiesRegistrationForSetActionValue = function (LayoutId) {
        var url = projectname + "/QuestionPath/GetActivitiesRegistrationForSetActionValue?LayoutId=" + LayoutId
        return $http.post(url);
    };
    // Code added for Divakar - If value is 'Next Qp' and Start , End and New field will not be shown in select QP dropdownlist
    dataFactory.GetActivitiesRegistrationForSetActionNextViewValue = function () {
        var url = projectname + "/QuestionPath/GetActivitiesRegistrationForSetActionNextViewValue"
        return $http.post(url);
    };


    dataFactory.GetDocumetScanTitle = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetDocumetScanTitle?questionRefId=" + questionRefId + "&layoutId=" + layoutId;
        return $http.post(url);
    };

    dataFactory.UpdateDocumentScanTitle = function (documentscantitleid, questionid, title) {
        var url = projectname + "/QuestionPath/UpdateDocumentScanTitle?DocumentScanTitleId=" + documentscantitleid + "&QuestionId=" + questionid + "&Title=" + title;
        return $http.post(url);
    };
    dataFactory.getOBCTypeForLayout = function (LayoutId) {
        var url = projectname + "/QuestionPath/GetOBCTypeForLayout?LayoutId=" + LayoutId
        return $http.post(url);
    };

    dataFactory.GetIsTripPalaningType = function (LayoutId) {
        var url = projectname + "/QuestionPath/GetIsTripPalaningType?LayoutId=" + LayoutId
        return $http.post(url);
    };
    dataFactory.GetActivitiesRegistrationPlanning = function (ActivityId) {
        var url = projectname + "/QuestionPath/GetActivitiesRegistrationPlanning?ActivityId=" + ActivityId
        return $http.post(url);
    };

    dataFactory.GetExportExcelValidation = function (LayoutId) {
        var url = projectname + "/Translator/GetExportExcelValidation?LayoutId=" + LayoutId
        return $http.post(url);
    };

    dataFactory.GetLayoutDefaultLanguageId = function (LayoutId) {
        var url = projectname + "/Translator/GetLayoutDefaultLanguageId?LayoutId=" + LayoutId
        return $http.post(url);
    };

    dataFactory.GetFlexDocumentScannerForActivities = function (layOutRefID, activitiyRefID, QuesstionRefID) {
        var url = projectname + "/QuestionPath/GetFlexDocumentScannerForActivities?layOutRefID=" + layOutRefID + "&activitiyRefID=" + activitiyRefID + "&QuesstionRefID=" + QuesstionRefID
        return $http.post(url);
    };
    dataFactory.InsertFlexDocumentScanner = function (questionidDocumentScanner, DocumentScanner) {

        var url = projectname + "/QuestionPath/InsertFlexDocumentScanner?questionidDocumentScanner=" + questionidDocumentScanner + "&DocumentScanner=" + DocumentScanner
        return $http.post(url);
    };
    dataFactory.UpdateFlexDocumentScanner = function (DocumentScanner) {
        var url = projectname + "/QuestionPath/UpdateFlexDocumentScanner?DocumentScanner=" + DocumentScanner
        return $http.post(url);
    };
    dataFactory.DeleteFlexDocumentScanner = function (DocumentScannerID) {
        var url = projectname + "/QuestionPath/DeleteFlexDocumentScanner?DocumentScannerID=" + DocumentScannerID
        return $http.post(url);
    };

    dataFactory.UpdateDocumentScannerProperties = function (DocumentScannerProperties, pDocumentScannerPropertiesnodeid) {
        var url = projectname + "/QuestionPath/UpdateDocumentScannerProperties?DocumentScannerProperties=" + DocumentScannerProperties + "&pDocumentScannerPropertiesnodeid=" + pDocumentScannerPropertiesnodeid
        return $http.post(url);
    };

    dataFactory.GetDocumentScannerProperties = function (questionRefId, layoutRefId) {
        var url = projectname + "/QuestionPath/GetDocumentScannerProperties?questionRefId=" + questionRefId + "&layoutRefId=" + layoutRefId;
        return $http.post(url);
    };

    dataFactory.UpdateSmartQuestionPath = function (title, nodeId) {
        var url = projectname + "/QuestionPath/UpdateSmartQuestionPath?title=" + title + "&nodeId=" + nodeId
        return $http.post(url);
    };
    dataFactory.GetPropertyNameByQuestionId = function (questionid) {
        var url = projectname + "/QuestionPath/GetPropertyNameByQuestionId?QuestionID=" + questionid
        return $http.post(url)
    };
    dataFactory.GetQuestionsByQuestionId = function (questionid) {
        var url = projectname + "/QuestionPath/GetQuestionsByQuestionId?QuestionID=" + questionid
        return $http.post(url)
    };
    dataFactory.GetAddChoiceDetailsByChoiceId = function (choiceId) {
        var url = projectname + "/QuestionPath/GetAddChoiceDetailsByChoiceId?choiceId=" + choiceId
        return $http.post(url)
    };
    dataFactory.CheckEndRecursiveCheckedOrNot = function (choiceId) {
        var url = projectname + "/QuestionPath/CheckEndRecursiveCheckedOrNot?choiceId=" + choiceId
        return $http.post(url)
    };

    dataFactory.GetTrailerProperties = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetTrailerProperties?questionRefId=" + questionRefId + "&layoutId=" + layoutId;
        return $http.post(url);
    };

    dataFactory.UpdateTrailerProperties = function (trailerProperties, pnodeid) {
        var url = projectname + "/QuestionPath/UpdateTrailerProperties?trailerProperties=" + trailerProperties + "&pnodeid=" + pnodeid
        return $http.post(url);
    };

    dataFactory.GetChoiceIdByQuestionId = function (quesionId) {
        var url = projectname + "/QuestionPath/GetChoiceIdByQuestionId?quesionId=" + quesionId;
        return $http.post(url);
    };
    dataFactory.GetChoiceIdByAddChoiceQuestionId = function (quesionId) {
        var url = projectname + "/QuestionPath/GetChoiceIdByAddChoiceQuestionId?quesionId=" + quesionId;
        return $http.post(url);
    };
    dataFactory.CheckEndRecCheckedOrNotForChoiceid = function (quesionId) {
        var url = projectname + "/QuestionPath/CheckEndRecCheckedOrNotForChoiceid?choiceId=" + quesionId;
        return $http.post(url);
    };
    dataFactory.GetTransTypeIdByTTQuestionId = function (quesionId) {
        var url = projectname + "/QuestionPath/GetTransTypeIdByTTQuestionId?quesionId=" + quesionId;
        return $http.post(url);
    };
    dataFactory.UpdateTransportTypePropertiesDetail = function (transportTypeDetails) {
        var url = projectname + "/QuestionPath/UpdateTransportTypePropertiesDetail?TransportTypePropertiesDetails=" + transportTypeDetails;
        return $http.post(url);
    };
    dataFactory.UpdateAddChioceProperties = function (choiceDetails) {
        var url = projectname + "/QuestionPath/UpdateAddChioceProperties?AddChioceProperties=" + choiceDetails;
        return $http.post(url);
    };
    dataFactory.getSystemValuesList = function () {
        var url = projectname + "/QuestionPath/GetSystemValuesList";
        return $http.post(url);
    };

    dataFactory.getSystemValuesFormatList = function (getSystemValuesId) {
        var url = projectname + "/QuestionPath/GetSystemValuesFormatList?getSystemValuesId=" + getSystemValuesId;
        return $http.post(url);
    };

    dataFactory.GetSystemValuesPropertiesList = function (questionRefId) {
        var url = projectname + "/QuestionPath/GetSystemValuesPropertiesList?questionRefId=" + questionRefId;
        return $http.post(url);
    };

    dataFactory.UpdateGetSystemValuesProperties = function (getSystemValuesProperties, pGetSystemValuesnodeid) {
        var url = projectname + "/QuestionPath/UpdateGetSystemValuesProperties?getSystemValuesProperties=" + getSystemValuesProperties + "&pGetSystemValuesnodeid=" + pGetSystemValuesnodeid
        return $http.post(url);
    };

    dataFactory.DocumentScanValidation = function (documentScanTitleRefId) {
        var url = projectname + "/QuestionPath/DocumentScanValidation?documentScanTitleRefId=" + documentScanTitleRefId;
        return $http.post(url);
    };


    // VarList Changes Start

    dataFactory.GetCommentReadoutDetail = function (tockenid, layoutid) {
        debugger
        var url = projectname + "/QuestionPath/GetCommentReadoutDetail?TockenId=" + tockenid + "&LayoutId=" + layoutid
        return $http.post(url)
    };

    dataFactory.GetFilterDetail = function (tockenid, layoutid) {
        debugger
        var url = projectname + "/QuestionPath/GetFilterDetail?TockenId=" + tockenid + "&LayoutId=" + layoutid
        return $http.post(url)
    };

    dataFactory.SaveCommentReadoutDetail = function (readoutDetail, layoutid, activityid, aliasname) {
        var url = projectname + "/QuestionPath/SaveCommentReadoutDetail?commentReadoutDetail=" + readoutDetail + "&layoutRefId=" + layoutid + "&activitiesRefId=" + activityid + "&aliasName=" + aliasname;
        return $http.post(url);
    };

    dataFactory.GetAliasNameDetailsForVLReadOut = function (activitiesId, layoutId) {
        var url = projectname + "/QuestionPath/GetAliasNameDetailsForVLReadOut?activitiesId=" + activitiesId + "&layoutId=" + layoutId
        return $http.post(url);
    };

    dataFactory.DeleteCommentReadoutDetail = function (commentdetailid) {
        var url = projectname + "/QuestionPath/DeleteCommentReadoutDetail?commentDetailId=" + commentdetailid
        return $http.post(url);
    };
    dataFactory.EditCommentReadoutDetail = function (commentdetailid) {
        var url = projectname + "/QuestionPath/EditCommentReadoutDetail?commentDetailId=" + commentdetailid
        return $http.post(url);
    };

    dataFactory.SaveFilterDetail = function (readoutDetail, layoutid, activityid) {
        var url = projectname + "/QuestionPath/SaveFilterDetail?filterDetail=" + readoutDetail + "&layoutRefId=" + layoutid + "&activitiesRefId=" + activityid;
        return $http.post(url);
    };

    dataFactory.DeleteFilterDetail = function (filterdetailid) {
        var url = projectname + "/QuestionPath/DeleteFilterDetail?filterDetailId=" + filterdetailid
        return $http.post(url);
    };
    dataFactory.EditFilterDetail = function (filterdetailid) {
        var url = projectname + "/QuestionPath/EditFilterDetail?filterDetailId=" + filterdetailid
        return $http.post(url);
    };

    dataFactory.GetVarListCommentReadoutDetail = function (varListPropertiesId, layoutid) {
        var url = projectname + "/QuestionPath/GetVarListCommentReadoutDetail?varListPropertiesId=" + varListPropertiesId + "&layoutId=" + layoutid
        return $http.post(url)
    };

    dataFactory.GetVarListFilterDetail = function (varListPropertiesId, layoutid) {
        var url = projectname + "/QuestionPath/GetVarListFilterDetail?varListPropertiesId=" + varListPropertiesId + "&layoutId=" + layoutid
        return $http.post(url)
    };

    dataFactory.GetVarListProperties = function (questionRefId, layoutId) {
        var url = projectname + "/QuestionPath/GetVarListProperties?questionRefId=" + questionRefId + "&layoutId=" + layoutId;
        return $http.post(url);
    };


    dataFactory.GetVarListCommentReadoutDetail = function (varListPropertiesId, layoutid) {
        debugger
        var url = projectname + "/QuestionPath/GetVarListCommentReadoutDetail?varListPropertiesId=" + varListPropertiesId + "&LayoutId=" + layoutid
        return $http.post(url)
    };

    dataFactory.GetVarListFilterDetail = function (varListPropertiesId, layoutid) {
        debugger
        var url = projectname + "/QuestionPath/GetVarListFilterDetail?varListPropertiesId=" + varListPropertiesId + "&LayoutId=" + layoutid
        return $http.post(url)
    };

    dataFactory.SaveVarListCommentReadoutDetail = function (readoutDetail, layoutid, activityid, aliasname) {
        var url = projectname + "/QuestionPath/SaveVarListCommentReadoutDetail?commentReadoutDetail=" + readoutDetail + "&layoutRefId=" + layoutid + "&activitiesRefId=" + activityid + "&aliasName=" + aliasname;
        return $http.post(url);
    };

    dataFactory.DeleteVarListCommentReadoutDetail = function (commentdetailid) {
        var url = projectname + "/QuestionPath/DeleteVarListCommentReadoutDetail?commentDetailId=" + commentdetailid
        return $http.post(url);
    };
    dataFactory.EditVarListCommentReadoutDetail = function (commentdetailid) {
        var url = projectname + "/QuestionPath/EditVarListCommentReadoutDetail?commentDetailId=" + commentdetailid
        return $http.post(url);
    };

    dataFactory.SaveVarListFilterDetail = function (readoutDetail, layoutid, activityid) {
        var url = projectname + "/QuestionPath/SaveVarListFilterDetail?filterDetail=" + readoutDetail + "&layoutRefId=" + layoutid + "&activitiesRefId=" + activityid;
        return $http.post(url);
    };

    dataFactory.DeleteVarListFilterDetail = function (filterdetailid) {
        var url = projectname + "/QuestionPath/DeleteVarListFilterDetail?filterDetailId=" + filterdetailid
        return $http.post(url);
    };
    dataFactory.EditVarListFilterDetail = function (filterdetailid) {
        var url = projectname + "/QuestionPath/EditVarListFilterDetail?filterDetailId=" + filterdetailid
        return $http.post(url);
    };

    dataFactory.UpdateVarListProperties = function (VarListProperties, pVariableListnodeid, transportTypeId) {
        var url = projectname + "/QuestionPath/UpdateVarListProperties?varListProperties=" + VarListProperties + "&variableListNodeId=" + pVariableListnodeid + "&transportTypeId=" + transportTypeId
        return $http.post(url);
    };

    dataFactory.GetQuestionPathComment = function (activityId, layoutId) {
        var url = projectname + "/QuestionPath/GetQuestionPathComment?layoutId=" + layoutId + "&activityId=" + activityId
        return $http.post(url);
    };

    dataFactory.SaveQuestionPathComment = function (activityId, comment) {
        var url = projectname + "/QuestionPath/SaveQuestionPathComment?activityId=" + activityId + "&comment=" + comment
        return $http.post(url);
    };

    dataFactory.GetCustomerSystemType = function () {
        var url = projectname + "/Customer/GetCustomerSystemType";
        return $http.post(url);
    };

    dataFactory.UpdateTooltipImageTitle = function (title, tooltipId, tooltipImageId) {
        var url = projectname + "/Tooltip/UpdateTooltipImageTitle?title=" + title + "&tooltipId=" + tooltipId + "&tooltipImageId=" + tooltipImageId;
        return $http.post(url);
    }
    dataFactory.GetTooltipImage = function (tooltipId) {
        var url = projectname + "/Tooltip/GetTooltipImage?tooltipId=" + tooltipId;
        return $http.post(url);
    };
    dataFactory.DeleteTooltipImage = function (tooltipImageId, tooltipId) {
        var url = projectname + "/Tooltip/DeleteTooltipImage?tooltipImageId=" + tooltipImageId + "&tooltipId=" + tooltipId;
        return $http.post(url);
    };

    dataFactory.UpdateTooltip = function (tooltipId, wizardTooltip, extendedHelp) {
        var url = projectname + "/Tooltip/UpdateTooltip?tooltipId=" + tooltipId + "&wizardTooltip=" + wizardTooltip + "&extendedHelp=" + extendedHelp;
        return $http.post(url);
    }
    dataFactory.GetTooltipList = function (layoutId) {
        var url = projectname + "/Tooltip/GetTooltipList?layoutId=" + layoutId;
        return $http.post(url);
    };
    
    return dataFactory;
});
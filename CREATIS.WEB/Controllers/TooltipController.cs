﻿using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using CREATIS.Controllers;
using CREATIS.Infrastructure.IRepository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CREATIS.WEB.Controllers
{
    public class TooltipController : BaseController
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ITooltipRepository tooltipRepository;
        public ConnectionStrings connectionStrings { get; }
        public OthersSettings othersSettings { get; }
        public TooltipController(ITooltipRepository InRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings, IHostingEnvironment hostingEnvironment)
        {
            this.tooltipRepository = InRepo;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
            _hostingEnvironment = hostingEnvironment;
        }
        public ActionResult Index()
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();
                    if (HasPrivilege(userCredentials.permissions).Contains("QP"))
                    {
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        return View();
                    }
                    else
                        return View("Unauthorised");
                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");
        }
        public ActionResult GetTooltip()
        {
            try
            {
                List<Tooltip> resultList;
                resultList = tooltipRepository.GetTooltip();
                return Json(resultList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
        public ActionResult GetWizardTooltip()
        {
            try
            {
                List<WizardTooltipTransation> resultList;
                resultList = tooltipRepository.GetWizardTooltip();
                return Json(resultList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
        public ActionResult GetExtendedTooltip()
        {
            try
            {
                List<ExtendedTooltipTransation> resultList;
                resultList = tooltipRepository.GetExtendedTooltip();
                return Json(resultList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
        public ActionResult UploadTooltipImage(IList<IFormFile> UploadTooltipImage)
        {
            try
            {
                var userCredentials = GetUserCredentials();
                var resultId = 0;
                foreach (var file in UploadTooltipImage)
                {
                    if (file.Length > 0)
                    {
                        string webRootFolder = _hostingEnvironment.WebRootPath + $@"\Translations\Tooltip";
                        string Root = "http://" + Request.Host.Value + "/" + Request.PathBase.Value + "/Translations/Tooltip";

                        string[] filesplit = file.FileName.Split('.');
                        string fileName = filesplit[0] + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + "." + filesplit[1];
                        string orginalFileName = file.FileName;
                        var RootPath = Path.Combine(Root, fileName);
                        var filePath = Path.Combine(webRootFolder, fileName);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                            TooltipImage tooltip = new TooltipImage();
                            tooltip.Path = RootPath;
                            tooltip.RootPath = filePath;
                            tooltip.OriginalFileName = orginalFileName;
                            tooltip.SystemFileName = fileName;
                            tooltip.CreatedBy = userCredentials.id;
                            tooltip.CreatedOn = DateTime.Now;
                            tooltip.UpdatedBy = userCredentials.id;
                            tooltip.UpdatedOn = DateTime.Now;
                            resultId = tooltipRepository.SaveTooltipImage(tooltip);
                        }
                    }
                }
                return Json(resultId);
            }
            catch (Exception ex)
            {
                return Json("0");
            }
        }
        public ActionResult UpdateTooltipImageTitle(string title, int tooltipId, int tooltipImageId)
        {
            try
            {
                var userCredentials = GetUserCredentials();
                TooltipImage tooltip = new TooltipImage();
                tooltip.Title = title;
                tooltip.TooltipRefId = tooltipId;
                tooltip.TooltipImageId = tooltipImageId;
                tooltip.UpdatedBy = userCredentials.id;
                tooltip.UpdatedOn = DateTime.Now;
                tooltipRepository.SaveTooltipImage(tooltip);

                return Json(tooltipRepository.GetTooltipImage(tooltipId));
            }
            catch (Exception ex)
            {
                return Json(new List<TooltipImage>());
            }
        }
        public ActionResult GetTooltipImage(int tooltipId)
        {
            try
            {
                return Json(tooltipRepository.GetTooltipImage(tooltipId));
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E02)
                });
            }
        }
        public ActionResult DeleteTooltipImage(int tooltipImageId, int tooltipId)
        {
            try
            {
                return Json(tooltipRepository.DeleteTooltipImage(tooltipImageId, tooltipId));
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E02)
                });
            }
        }
        public ActionResult UpdateTooltip(int tooltipId, string wizardTooltip, string extendedHelp)
        {
            try
            {
                var userCredentials = GetUserCredentials();
                Tooltip tooltip = new Tooltip();
                tooltip.TooltipId = tooltipId;
                tooltip.WizardTooltips = wizardTooltip;
                tooltip.ExtendedHelp = extendedHelp ?? "";
                tooltip.UpdatedBy = userCredentials.id;
                tooltip.UpdatedOn = DateTime.Now;
                var resultTooltipId = tooltipRepository.UpdateTooltip(tooltip);
                if (resultTooltipId > 0)
                    return Json(resultTooltipId);
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E02)
                    });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E02)
                });
            }
        }
        public ActionResult SaveTooltipTranslation(string changedList)
        {
            System.DateTime time = DateTime.Now;
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };
                    List<TooltipTranslation> changedLangDetails = JsonConvert.DeserializeObject<List<TooltipTranslation>>(WebUtility.UrlDecode(changedList), settings);
                    string result = "";

                    if (changedLangDetails.Count > 0)
                    {
                        var userCredentials = GetUserCredentials();
                        foreach (TooltipTranslation d in changedLangDetails)
                        {
                            d.UpdatedBy = userCredentials.id.ToString();
                            d.UpdatedOn = DateTime.Now;
                            result = tooltipRepository.SaveTooltipTranslation(d);
                        }
                    }

                    return Json(result);
                }
                catch (Exception ex)
                {
                    string e = ex.ToString();
                    return Json(e);
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult ExtendedHelp(int uiId, int layoutId)
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();

                    ViewBag.username = userCredentials.displayName;
                    ViewBag.SessionLink = othersSettings.SessionLink;
                    ViewData["ExtendedHelp"] = tooltipRepository.GetExtendedHelpText(uiId, layoutId);
                    ViewData["PropertyTitle"] = tooltipRepository.GetTooltipTitle(uiId);
                    ViewData["ExtendedHelpTooltipImage"] = tooltipRepository.GetExtendedHelpTooltipImage(uiId);

                    return View();
                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");
        }

        public ActionResult GetTooltipList(int layoutId)
        {
            try
            {
                List<Tooltip> resultList;
                resultList = tooltipRepository.GetTooltipList(layoutId);
                return Json(resultList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
    }
}
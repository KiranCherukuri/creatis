﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using CREATIS.Infrastructure.Repository;
using CREATIS.Infrastructure.IRepository;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;

using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using CREATIS.Controllers;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.Text;
using System.Text.RegularExpressions;

namespace CREATIS.Controllers
{
    public class TranslatorController : BaseController
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public readonly ICustomerOverviewRepository CustomerLogicRepository;
        public readonly IDashboardRepository DashboardLogicRepository;
        public readonly ITranslatorRepository translatorRepository;
        public ConnectionStrings connectionStrings { get; }
        public OthersSettings othersSettings { get; }
        public TranslatorController(ICustomerOverviewRepository CustomerOverviewLogicRepo, IDashboardRepository DashboardLogicRepo, ITranslatorRepository translatorRepo, IHostingEnvironment hostingEnvironment, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings)
        {
            this.CustomerLogicRepository = CustomerOverviewLogicRepo;
            this.DashboardLogicRepository = DashboardLogicRepo;
            this.translatorRepository = translatorRepo;
            _hostingEnvironment = hostingEnvironment;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
        }

        #region General Translation
        public ActionResult Index()
        {
            if (IsServerConnected(connectionStrings))
            {
                var userCredentials = GetUserCredentials();
                if (HasPrivilege(userCredentials.permissions).Contains("CT") && !IsCustomerLogin(userCredentials.permissions))
                {
                    ViewBag.username = userCredentials.displayName;
                    ViewBag.SessionLink = othersSettings.SessionLink;
                    return View();
                }
                else
                    return View("Unauthorised");
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetGeneralTranslation()
        {
            if (IsServerConnected(connectionStrings))
            {
                List<LangDetail> ResultofLangDetail = new List<LangDetail>();
                ResultofLangDetail = translatorRepository.GetGeneralTranslation(0);
                return Json(ResultofLangDetail);
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetCustomerSpecificTranslation(int CustomerId)
        {
            if (IsServerConnected(connectionStrings))
            {
                List<LangDetail> ResultofLangDetail = new List<LangDetail>();
                ResultofLangDetail = translatorRepository.GetCustomerSpecificTranslation(CustomerId);
                return Json(ResultofLangDetail);
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveGeneralTranslation(string changedList, string addedList,string deletedList)
        {
            System.DateTime time = DateTime.Now;
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    string result = "";
                    var userCredentials = GetUserCredentials();
                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };
                    List<LangDetail> addedLangDetails = JsonConvert.DeserializeObject<List<LangDetail>>(WebUtility.UrlDecode(addedList), settings);
                    List<LangDetail> changedLangDetails = JsonConvert.DeserializeObject<List<LangDetail>>(WebUtility.UrlDecode(changedList), settings);
                    List<LangDetail> deletedLangDetails = JsonConvert.DeserializeObject<List<LangDetail>>(WebUtility.UrlDecode(deletedList), settings);

                    if (changedLangDetails.Count > 0)
                    {
                        foreach (LangDetail d in changedLangDetails)
                        {
                            var Creatis = translatorRepository.GetGeneralTranslation(0).Where(r => r.Id != d.Id && r.English == d.English && r.English != null).ToList();
                            if (Creatis.Count > 0)
                                return Json("error");
                            else
                            {
                                d.UpdatedBy = userCredentials.id.ToString();
                                d.UpdatedOn = DateTime.Now;
                                d.IsDefaultTranslation = 1;
                                translatorRepository.SaveCentralTranslation(d);
                            }
                        }
                    }
                    if (addedLangDetails.Count > 0)
                    {
                        foreach (LangDetail d in addedLangDetails)
                        {
                            var langDetails = translatorRepository.GetGeneralTranslation(0).Where(r => r.Id != d.Id && r.English == d.English && r.English != null).ToList();

                            if (langDetails.Count > 0)
                                return Json("error");
                            else
                            {
                                d.CreatedBy = userCredentials.id.ToString();
                                d.CreatedOn = DateTime.Now;
                                d.IsDefaultTranslation = 1;
                                translatorRepository.SaveCentralTranslation(d);
                            }
                        }
                    }
                    StringBuilder usedGeneralTranslationCode = new StringBuilder();
                    StringBuilder notExistGeneralTranslationCode = new StringBuilder();
                    if (deletedLangDetails.Count > 0)
                    {
                        foreach (LangDetail d in deletedLangDetails)
                        {
                            result = translatorRepository.DeleteGeneralTranslation(d.Id);
                            if (result == "Used")
                                usedGeneralTranslationCode.Append(d.Code + " | ");
                            else if (result == "Not Exist")
                                notExistGeneralTranslationCode.Append(d.Code + " | ");
                        }

                        if(usedGeneralTranslationCode.Length == 0 && notExistGeneralTranslationCode.Length == 0)
                            return Json("success");
                        else if(usedGeneralTranslationCode.Length != 0 && notExistGeneralTranslationCode.Length == 0)
                            return Json(usedGeneralTranslationCode + " Given General Translations Are Used");
                        else if (usedGeneralTranslationCode.Length == 0 && notExistGeneralTranslationCode.Length != 0)
                            return Json(notExistGeneralTranslationCode + " Given General Translations Are Already Exist");
                    }

                    return Json("success");
                }
                catch (Exception ex)
                {
                    string e = ex.ToString();
                    return Json(e);
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveCustomerSpecificTranslation(string changedList)
        {
            System.DateTime time = DateTime.Now;
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };
                    List<LangDetail> changedLangDetails = JsonConvert.DeserializeObject<List<LangDetail>>(WebUtility.UrlDecode(changedList), settings);
                    string result = "";

                    if (changedLangDetails.Count > 0)
                    {
                        var userCredentials = GetUserCredentials();
                        foreach (LangDetail d in changedLangDetails)
                        {
                            d.UpdatedBy = userCredentials.id.ToString();
                            d.UpdatedOn = DateTime.Now;
                            d.IsDefaultTranslation = 0;
                            result = translatorRepository.SaveCentralTranslation(d);
                        }
                    }

                    return Json(result);
                }
                catch (Exception ex)
                {
                    string e = ex.ToString();
                    return Json(e);
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetCustomerSpecificList()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<Customers> List = translatorRepository.GetCustomerSpecificList();
                    var CustomerDropDown = (from Customerdata in List orderby Customerdata.Name select new { Customerdata.CustomerId, Cust = string.Format("{0} ({1}-{2})", Customerdata.Name, Customerdata.Source, Customerdata.Environment) }).ToList();
                    return Json(CustomerDropDown);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult CopyTranslationFromCustomerSpecificToGeneral(int langDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };
                    var userCredentials = GetUserCredentials();
                    var result = translatorRepository.CopyTranslationFromCustomerSpecificToGeneral(langDetailId, userCredentials.id.ToString(), DateTime.Now);
                    if (result == "success")
                        return Json(result);
                    else
                        return Json(new
                        {
                            error = result
                        });
                }
                catch (Exception ex)
                {
                    string e = ex.ToString();
                    return Json(e);
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetCommonUITranslation()
        {
            if (IsServerConnected(connectionStrings))
            {
                List<UITranslationDetail> ResultofLangDetail = new List<UITranslationDetail>();
                ResultofLangDetail = translatorRepository.GetCommonUITranslation();
                return Json(ResultofLangDetail);
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveCommonUITranslation(string changedList)
        {
            System.DateTime time = DateTime.Now;
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };
                    List<UITranslationDetail> changedLangDetails = JsonConvert.DeserializeObject<List<UITranslationDetail>>(WebUtility.UrlDecode(changedList), settings);
                    string result = "";

                    if (changedLangDetails.Count > 0)
                    {
                        var userCredentials = GetUserCredentials();
                        foreach (UITranslationDetail d in changedLangDetails)
                        {
                            d.UpdatedBy = userCredentials.id.ToString();
                            d.UpdatedOn = DateTime.Now;
                            result = translatorRepository.SaveCommonUITranslation(d);
                        }
                    }

                    return Json(result);
                }
                catch (Exception ex)
                {
                    string e = ex.ToString();
                    return Json(e);
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetSpecificUITranslation()
        {
            if (IsServerConnected(connectionStrings))
            {
                List<UILangDetail> ResultofLangDetail = new List<UILangDetail>();
                ResultofLangDetail = translatorRepository.GetSpecificUITranslation();
                return Json(ResultofLangDetail);
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveSpecificUITranslation(string changedList)
        {
            System.DateTime time = DateTime.Now;
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };
                    List<UILangDetail> changedLangDetails = JsonConvert.DeserializeObject<List<UILangDetail>>(WebUtility.UrlDecode(changedList), settings);
                    string result = "";

                    if (changedLangDetails.Count > 0)
                    {
                        var userCredentials = GetUserCredentials();
                        foreach (UILangDetail d in changedLangDetails)
                        {
                            d.UpdatedBy = userCredentials.id.ToString();
                            d.UpdatedOn = DateTime.Now;
                            result = translatorRepository.SaveSpecificUITranslation(d);
                        }
                    }

                    return Json(result);
                }
                catch (Exception ex)
                {
                    string e = ex.ToString();
                    return Json(e);
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

      
        #endregion General Translation

        #region Translation
        public ActionResult Translation(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                var userCredentials = GetUserCredentials();
                if ((HasPrivilege(userCredentials.permissions).Contains("T") || HasPrivilege(userCredentials.permissions).Contains("DTB") || HasPrivilege(userCredentials.permissions).Contains("LT") || HasPrivilege(userCredentials.permissions).Contains("SLT") || HasPrivilege(userCredentials.permissions).Contains("QPT") || HasPrivilege(userCredentials.permissions).Contains("QPT")) && IsCustomerLogin(userCredentials.permissions))
                {
                    var customerId = GetLayoutCustomerId(LayoutID);
                    if (GetCustomerLayoutList(userCredentials.company, customerId).Any(x => x == LayoutID))
                    {
                        LayoutStatusPrivilege(LayoutID);
                        RoleWithLayoutStatusPrivilege(LayoutID, userCredentials.permissions);
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        AddRecentActivity(LayoutID, userCredentials.id);
                        return View();
                    }
                    else
                        return View("Unauthorised");
                }
                if (HasPrivilege(userCredentials.permissions).Contains("T") || HasPrivilege(userCredentials.permissions).Contains("DTB") || HasPrivilege(userCredentials.permissions).Contains("LT") || HasPrivilege(userCredentials.permissions).Contains("SLT") || HasPrivilege(userCredentials.permissions).Contains("QPT") || HasPrivilege(userCredentials.permissions).Contains("QPT"))
                {
                    LayoutStatusPrivilege(LayoutID);
                    RoleWithLayoutStatusPrivilege(LayoutID, userCredentials.permissions);
                    ViewBag.username = userCredentials.displayName;
                    ViewBag.SessionLink = othersSettings.SessionLink;
                    AddRecentActivity(LayoutID, userCredentials.id);
                    return View();
                }
                else
                    return View("Unauthorised");
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetLangActivities(int LayoutId, int TableId)
        {
            if (LayoutId == 0)
            {
                var SplitValue = TempData["LayoutIds"].ToString().Split("`");

                Int32.TryParse(SplitValue[0], out LayoutId);
                Int32.TryParse(SplitValue[1], out TableId);
            }

            if (IsServerConnected(connectionStrings))
            {
                var ResultofActReg = new LangDetail();

                var usedLanguages = translatorRepository.GetUsedLanguage(LayoutId);

                ResultofActReg.UsedLanguageList = usedLanguages.Select(x => new UsedLanguageList
                {
                    IdDefault = x.IsDefault,
                    LanguageId = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageId,
                    LanguageRefId = x.LanguageRefId,
                    LanguageName = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageDescription
                }).OrderByDescending(x => x.IdDefault).ThenBy(x => x.LanguageName).ToList();

                ResultofActReg.langDetailList = translatorRepository.GetLangActivities(TableId, LayoutId);

                ResultofActReg = UpdateLangDetailmodal(ResultofActReg);

                return Json(ResultofActReg);
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetLangRegistration(int LayoutId, int TableId)
        {
            if (LayoutId == 0)
            {
                var SplitValue = TempData["LayoutIds"].ToString().Split("`");

                Int32.TryParse(SplitValue[0], out LayoutId);
                Int32.TryParse(SplitValue[1], out TableId);
            }

            if (IsServerConnected(connectionStrings))
            {
                var ResultofActReg = new LangDetail();

                ResultofActReg.langDetailList = translatorRepository.GetLangRegistration(TableId, LayoutId);
                var usedLanguages = translatorRepository.GetUsedLanguage(LayoutId);
                ResultofActReg.UsedLanguageList = usedLanguages.Select(x => new UsedLanguageList
                {
                    IdDefault = x.IsDefault,
                    LanguageId = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageId,
                    LanguageName = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageDescription
                }).ToList();

                ResultofActReg = UpdateLangDetailmodal(ResultofActReg);

                return Json(ResultofActReg);
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetAllLangDetail(int LayoutId, int TableId)
        {
            if (LayoutId == 0)
            {
                var SplitValue = TempData["LayoutIds"].ToString().Split("`");

                Int32.TryParse(SplitValue[0], out LayoutId);
                Int32.TryParse(SplitValue[1], out TableId);
            }

            if (IsServerConnected(connectionStrings))
            {
                var ResultofLanguage = new LangDetail();

                List<Layout> LayoutDetails = DashboardLogicRepository.GetLayoutDetails(LayoutId);
                ResultofLanguage.langDetailList = translatorRepository.GetAllLangDetail(TableId, LayoutId);
                var usedLanguages = translatorRepository.GetUsedLanguage(LayoutId);
                ResultofLanguage.UsedLanguageList = usedLanguages.Select(x => new UsedLanguageList
                {
                    IdDefault = x.IsDefault,
                    LanguageId = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageId,
                    LanguageName = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageDescription
                }).ToList();

                ResultofLanguage = UpdateLangDetailmodal(ResultofLanguage);

                return Json(ResultofLanguage);
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdateLangDetail(int TableId, string ColumnName, int Id, string Header, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                var langdetail = translatorRepository.GetLangDetail(0, layoutId, Id, false, true).FirstOrDefault();

                langdetail = langdetail ?? new LangDetail();

                langdetail = translatorRepository.GetLangInfo(langdetail, TableId, ColumnName);

                langdetail.Id = Id;

                if (LanguagesEnum.English.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.English;
                }
                else if (LanguagesEnum.Arabic.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Arabic;
                }
                else if (LanguagesEnum.Bulgarian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Bulgarian;
                }
                else if (LanguagesEnum.Croatian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Croatian;
                }
                else if (LanguagesEnum.Czech.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Czech;
                }
                else if (LanguagesEnum.Danish.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Danish;
                }
                else if (LanguagesEnum.Dutch.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Dutch;
                }
                else if (LanguagesEnum.Estonian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Estonian;
                }
                else if (LanguagesEnum.Finnish.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Finnish;
                }
                else if (LanguagesEnum.French.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.French;
                }
                else if (LanguagesEnum.German.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.German;
                }
                else if (LanguagesEnum.Greek.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Greek;
                }
                else if (LanguagesEnum.Hungarian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Hungarian;
                }
                else if (LanguagesEnum.Italian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Italian;
                }
                else if (LanguagesEnum.Latvian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Latvian;
                }
                else if (LanguagesEnum.Lithuanian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Lithuanian;
                }
                else if (LanguagesEnum.Macedonian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Macedonian;
                }
                else if (LanguagesEnum.Norwegian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Norwegian;
                }
                else if (LanguagesEnum.Polish.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Polish;
                }
                else if (LanguagesEnum.Portuguese.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Portuguese;
                }
                else if (LanguagesEnum.Romanian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Romanian;
                }
                else if (LanguagesEnum.Russian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Russian;
                }
                else if (LanguagesEnum.Slovak.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Slovak;
                }
                else if (LanguagesEnum.Slovene.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Slovene;
                }
                else if (LanguagesEnum.Spanish.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Spanish;
                }
                else if (LanguagesEnum.Swedish.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Swedish;
                }
                else if (LanguagesEnum.Turkish.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Turkish;
                }
                else if (LanguagesEnum.Ukrainian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Ukrainian;
                }
                else if (LanguagesEnum.Belarusian.ToString() == Header)
                {
                    langdetail.Language = Header;
                    langdetail.SelectedLanguage = langdetail.Belarusian;
                }
                return Json(langdetail);
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public LangDetail UpdateLangDetailmodal(LangDetail LangInfo)
        {
            foreach (var usedlanguage in LangInfo.UsedLanguageList)
            {
                if (!LangInfo.IsShowEnglish && LanguagesEnum.English.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowEnglish = true;
                    LangInfo.IsDefaultEnglish = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowArabic && LanguagesEnum.Arabic.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowArabic = true;
                    LangInfo.IsDefaultArabic = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowBulgarian && LanguagesEnum.Bulgarian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowBulgarian = true;
                    LangInfo.IsDefaultBulgarian = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowCroatian && LanguagesEnum.Croatian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowCroatian = true;
                    LangInfo.IsDefaultCroatian = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowCzech && LanguagesEnum.Czech.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowCzech = true;
                    LangInfo.IsDefaultCzech = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowDanish && LanguagesEnum.Danish.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowDanish = true;
                    LangInfo.IsDefaultDanish = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowDutch && LanguagesEnum.Dutch.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowDutch = true;
                    LangInfo.IsDefaultDutch = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowEstonian && LanguagesEnum.Estonian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowEstonian = true;
                    LangInfo.IsDefaultEstonian = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowFinnish && LanguagesEnum.Finnish.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowFinnish = true;
                    LangInfo.IsDefaultFinnish = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowFrench && LanguagesEnum.French.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowFrench = true;
                    LangInfo.IsDefaultFrench = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowGerman && LanguagesEnum.German.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowGerman = true;
                    LangInfo.IsDefaultGerman = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowGreek && LanguagesEnum.Greek.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowGreek = true;
                    LangInfo.IsDefaultGreek = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowHungarian && LanguagesEnum.Hungarian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowHungarian = true;
                    LangInfo.IsDefaultHungarian = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowItalian && LanguagesEnum.Italian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowItalian = true;
                    LangInfo.IsDefaultItalian = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowLatvian && LanguagesEnum.Latvian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowLatvian = true;
                    LangInfo.IsDefaultLatvian = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowLithuanian && LanguagesEnum.Lithuanian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowLithuanian = true;
                    LangInfo.IsDefaultLithuanian = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowMacedonian && LanguagesEnum.Macedonian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowMacedonian = true;

                }
                else if (!LangInfo.IsShowNorwegian && LanguagesEnum.Norwegian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowNorwegian = true;
                    LangInfo.IsDefaultNorwegian = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowPolish && LanguagesEnum.Polish.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowPolish = true;
                    LangInfo.IsDefaultPolish = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowPortuguese && LanguagesEnum.Portuguese.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowPortuguese = true;
                    LangInfo.IsDefaultPortuguese = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowRomanian && LanguagesEnum.Romanian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowRomanian = true;
                    LangInfo.IsDefaultRomanian = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowRussian && LanguagesEnum.Russian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowRussian = true;
                    LangInfo.IsDefaultRussian = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowSlovak && LanguagesEnum.Slovak.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowSlovak = true;
                    LangInfo.IsDefaultSlovak = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowSlovene && LanguagesEnum.Slovene.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowSlovene = true;
                    LangInfo.IsDefaultSlovene = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowSpanish && LanguagesEnum.Spanish.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowSpanish = true;
                    LangInfo.IsDefaultSpanish = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowSwedish && LanguagesEnum.Swedish.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowSwedish = true;
                    LangInfo.IsDefaultSwedish = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowTurkish && LanguagesEnum.Turkish.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowTurkish = true;
                    LangInfo.IsDefaultTurkish = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowUkrainian && LanguagesEnum.Ukrainian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowUkrainian = true;
                    LangInfo.IsDefaultUkrainian = usedlanguage.IdDefault;
                }
                else if (!LangInfo.IsShowBelarusian && LanguagesEnum.Belarusian.ToString() == usedlanguage.LanguageName)
                {
                    LangInfo.IsShowBelarusian = true;
                    LangInfo.IsDefaultBelarusian = usedlanguage.IdDefault;
                }
            }
            return LangInfo;
        }
        public ActionResult SaveActivitiesLangDetail(string paramdata)
        {
            if (IsServerConnected(connectionStrings))
            {
                LangDetail model = JsonConvert.DeserializeObject<LangDetail>(paramdata.Replace("&nbsp;", " "));
                model.UpdatedBy = GetUserCredentials().id;
                var Result = translatorRepository.UpdateLangDetail(model);

                var ResultofActReg = new LangDetail();
                var usedLanguages = translatorRepository.GetUsedLanguage((int)Result.LayoutRefId);

                ResultofActReg.UsedLanguageList = usedLanguages.Select(x => new UsedLanguageList
                {
                    IdDefault = x.IsDefault,
                    LanguageId = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageId,
                    LanguageRefId = x.LanguageRefId,
                    LanguageName = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageDescription
                }).OrderByDescending(x => x.IdDefault).ThenBy(x => x.LanguageName).ToList();

                ResultofActReg.langDetailList = translatorRepository.GetLangActivities(Result.LangRefTableId, (int)Result.LayoutRefId);

                ResultofActReg = UpdateLangDetailmodal(ResultofActReg);

                return Json(ResultofActReg);
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveRegistrationLangDetail(string paramdata)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    LangDetail model = JsonConvert.DeserializeObject<LangDetail>(paramdata.Replace("&nbsp;", " "));
                    model.UpdatedBy = GetUserCredentials().id;
                    var Result = translatorRepository.UpdateLangDetail(model);

                    var ResultofActReg = new LangDetail();
                    var usedLanguages = translatorRepository.GetUsedLanguage((int)Result.LayoutRefId);

                    ResultofActReg.UsedLanguageList = usedLanguages.Select(x => new UsedLanguageList
                    {
                        IdDefault = x.IsDefault,
                        LanguageId = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageId,
                        LanguageRefId = x.LanguageRefId,
                        LanguageName = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageDescription
                    }).OrderByDescending(x => x.IdDefault).ThenBy(x => x.LanguageName).ToList();

                    ResultofActReg.langDetailList = translatorRepository.GetLangRegistration(Result.LangRefTableId, (int)Result.LayoutRefId);

                    ResultofActReg = UpdateLangDetailmodal(ResultofActReg);

                    return Json(ResultofActReg);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveAllLangDetail(string paramdata)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    LangDetail model = JsonConvert.DeserializeObject<LangDetail>(paramdata.Replace("&nbsp;"," ").Replace("ƒ", "&"));
                    if (model.SelectedLanguage.Contains("<p>"))
                        model.SelectedLanguage = "<p>" + model.SelectedLanguage.Replace("<p>", "").Replace("</p>", "").Replace("<div>", "").Replace("</div>", "").Replace("<span>", "").Replace("</span>", "").Replace("<br>", "") + "</p>";

                    var userCredentials = GetUserCredentials();
                    model.UpdatedBy = userCredentials.id;
                    var Result = translatorRepository.UpdateLangDetail(model);

                    if (Result.LangRefColTableId == (int)LangRefColTable.Questions_Name)
                    {
                        var getPropertiesModel = translatorRepository.GetPropertiesLangDetail(Result);
                        if (getPropertiesModel.Id > 0)
                        {

                            getPropertiesModel.CreatedBy = userCredentials.id.ToString();
                            getPropertiesModel.CreatedOn = DateTime.Now;
                            getPropertiesModel.UpdatedBy = userCredentials.id.ToString();
                            getPropertiesModel.UpdatedOn = DateTime.Now;
                            translatorRepository.UpdateLangDetail(getPropertiesModel);
                        }
                    }
                    else
                    {
                        var getQuestionsModel = translatorRepository.GetQuestionsLangDetail(Result, (model.QuestionRefId ?? 0));
                        if (getQuestionsModel.Id > 0)
                        {
                            getQuestionsModel.CreatedBy = userCredentials.id.ToString();
                            getQuestionsModel.CreatedOn = DateTime.Now;
                            getQuestionsModel.UpdatedBy = userCredentials.id.ToString();
                            getQuestionsModel.UpdatedOn = DateTime.Now;

                            translatorRepository.SaveTranslations(getQuestionsModel);
                        }
                    }

                    var ResultofLanguage = new LangDetail();

                    var usedLanguages = translatorRepository.GetUsedLanguage((int)Result.LayoutRefId);

                    ResultofLanguage.UsedLanguageList = usedLanguages.Select(x => new UsedLanguageList
                    {
                        IdDefault = x.IsDefault,
                        LanguageId = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageId,
                        LanguageRefId = x.LanguageRefId,
                        LanguageName = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageDescription
                    }).OrderByDescending(x => x.IdDefault).ThenBy(x => x.LanguageName).ToList();

                    ResultofLanguage.langDetailList = translatorRepository.GetAllLangDetail(Result.LangRefTableId, (int)Result.LayoutRefId);

                    ResultofLanguage = UpdateLangDetailmodal(ResultofLanguage);

                    return Json(ResultofLanguage);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveInfoMessageContentLangDetail(string paramdata)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    bool isValid = true;
                    LangDetail model = JsonConvert.DeserializeObject<LangDetail>(paramdata.Replace("&nbsp;", " ").Replace("ƒ", "&"));
                    var Messagecontent = model.SelectedLanguage.Replace("<p>", "ƒ<p>").Replace("<br>", " ");
                    var MessagecontentLines = Messagecontent.Split("ƒ").Where(x => x.Trim().Length > 0).ToList();
                    int LineNo = 0;
                    foreach (var messagecontent in MessagecontentLines)
                    {
                        string resultmessagecontent = translatorRepository.StripHTML(messagecontent);
                        if (Regex.Matches(resultmessagecontent, "}").Count() > 1)
                        {
                            var messagecontentsplit = resultmessagecontent.Split("}");
                            resultmessagecontent = "";
                            foreach (var mc in messagecontentsplit)
                            {
                                resultmessagecontent += RemoveStringBetween(mc, '{', '}');
                            }
                        }
                        else
                        {
                            resultmessagecontent = RemoveStringBetween(resultmessagecontent, '{', '}');
                        }

                        if (resultmessagecontent.Trim().Length > 35)
                        {
                            isValid = false;
                            return Json(new
                            {
                                error = "Message Content Length Greater Than 35 (" + translatorRepository.StripHTML(messagecontent) + ")"
                            });
                        }

                        if (resultmessagecontent.Trim().Length > 0)
                            LineNo++;
                    }

                    if (LineNo > 12)
                        return Json(new
                        {
                            error = "Message Content Should be 12 Lines"
                        });


                    if (isValid)
                    {
                        var userCredentials = GetUserCredentials();
                        model.UpdatedBy = userCredentials.id;
                        var Result = translatorRepository.UpdateLangDetail(model);

                        if (Result.LangRefColTableId == (int)LangRefColTable.Questions_Name)
                        {
                            var getPropertiesModel = translatorRepository.GetPropertiesLangDetail(Result);
                            if (getPropertiesModel.Id > 0)
                            {

                                getPropertiesModel.CreatedBy = userCredentials.id.ToString();
                                getPropertiesModel.CreatedOn = DateTime.Now;
                                getPropertiesModel.UpdatedBy = userCredentials.id.ToString();
                                getPropertiesModel.UpdatedOn = DateTime.Now;
                                translatorRepository.UpdateLangDetail(getPropertiesModel);
                            }
                        }
                        else
                        {
                            var getQuestionsModel = translatorRepository.GetQuestionsLangDetail(Result, (model.QuestionRefId ?? 0));
                            if (getQuestionsModel.Id > 0)
                            {
                                getQuestionsModel.CreatedBy = userCredentials.id.ToString();
                                getQuestionsModel.CreatedOn = DateTime.Now;
                                getQuestionsModel.UpdatedBy = userCredentials.id.ToString();
                                getQuestionsModel.UpdatedOn = DateTime.Now;

                                translatorRepository.SaveTranslations(getQuestionsModel);
                            }
                        }

                        var ResultofLanguage = new LangDetail();

                        var usedLanguages = translatorRepository.GetUsedLanguage((int)Result.LayoutRefId);

                        ResultofLanguage.UsedLanguageList = usedLanguages.Select(x => new UsedLanguageList
                        {
                            IdDefault = x.IsDefault,
                            LanguageId = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageId,
                            LanguageRefId = x.LanguageRefId,
                            LanguageName = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageDescription
                        }).OrderByDescending(x => x.IdDefault).ThenBy(x => x.LanguageName).ToList();

                        ResultofLanguage.langDetailList = translatorRepository.GetAllLangDetail(Result.LangRefTableId, (int)Result.LayoutRefId);

                        ResultofLanguage = UpdateLangDetailmodal(ResultofLanguage);

                        return Json(ResultofLanguage);
                    }
                    else
                        return Json(new
                        {
                            error = "Enter Valid Message Content"
                        });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveInfoMessageTitleLangDetail(string paramdata)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    bool isValid = true;
                    LangDetail model = JsonConvert.DeserializeObject<LangDetail>(paramdata.Replace("&nbsp;", " ").Replace("ƒ", "&"));
                    string resulttitle = translatorRepository.StripHTML(model.SelectedLanguage);
                    string messageType = translatorRepository.GetInfoMessageType(model.LangRefDetailId);

                    if (Regex.Matches(resulttitle, "}").Count() > 1)
                    {
                        var titlesplit = resulttitle.Split("}");
                        resulttitle = "";
                        foreach (var mc in titlesplit)
                        {
                            resulttitle += RemoveStringBetween(mc, '{', '}');
                        }
                    }
                    else
                    {
                        resulttitle = RemoveStringBetween(resulttitle, '{', '}');
                    }

                    if (messageType == "Line")
                    {
                        if (resulttitle.Trim().Length > 50)
                        {
                            isValid = false;
                            return Json(new
                            {
                                error = "Title Length Greater Than 50 (" + translatorRepository.StripHTML(model.SelectedLanguage) + ")"
                            });
                        }
                    }
                    else
                    {
                        if (resulttitle.Trim().Length > 40)
                        {
                            isValid = false;
                            return Json(new
                            {
                                error = "Title Length Greater Than 40 (" + translatorRepository.StripHTML(model.SelectedLanguage) + ")"
                            });
                        }
                    }


                    if (isValid)
                    {
                        if (model.SelectedLanguage.Contains("<p>"))
                            model.SelectedLanguage = "<p>" + model.SelectedLanguage.Replace("<p>", "").Replace("</p>", "").Replace("<div>", "").Replace("</div>", "").Replace("<span>", "").Replace("</span>", "").Replace("<br>", "") + "</p>";

                        var userCredentials = GetUserCredentials();
                        model.UpdatedBy = userCredentials.id;
                        var Result = translatorRepository.UpdateLangDetail(model);

                        if (Result.LangRefColTableId == (int)LangRefColTable.Questions_Name)
                        {
                            var getPropertiesModel = translatorRepository.GetPropertiesLangDetail(Result);
                            if (getPropertiesModel.Id > 0)
                            {

                                getPropertiesModel.CreatedBy = userCredentials.id.ToString();
                                getPropertiesModel.CreatedOn = DateTime.Now;
                                getPropertiesModel.UpdatedBy = userCredentials.id.ToString();
                                getPropertiesModel.UpdatedOn = DateTime.Now;
                                translatorRepository.UpdateLangDetail(getPropertiesModel);
                            }
                        }
                        else
                        {
                            var getQuestionsModel = translatorRepository.GetQuestionsLangDetail(Result, (model.QuestionRefId ?? 0));
                            if (getQuestionsModel.Id > 0)
                            {
                                getQuestionsModel.CreatedBy = userCredentials.id.ToString();
                                getQuestionsModel.CreatedOn = DateTime.Now;
                                getQuestionsModel.UpdatedBy = userCredentials.id.ToString();
                                getQuestionsModel.UpdatedOn = DateTime.Now;

                                translatorRepository.SaveTranslations(getQuestionsModel);
                            }
                        }

                        var ResultofLanguage = new LangDetail();

                        var usedLanguages = translatorRepository.GetUsedLanguage((int)Result.LayoutRefId);

                        ResultofLanguage.UsedLanguageList = usedLanguages.Select(x => new UsedLanguageList
                        {
                            IdDefault = x.IsDefault,
                            LanguageId = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageId,
                            LanguageRefId = x.LanguageRefId,
                            LanguageName = translatorRepository.GetLanguageById(x.LanguageRefId).LanguageDescription
                        }).OrderByDescending(x => x.IdDefault).ThenBy(x => x.LanguageName).ToList();

                        ResultofLanguage.langDetailList = translatorRepository.GetAllLangDetail(Result.LangRefTableId, (int)Result.LayoutRefId);

                        ResultofLanguage = UpdateLangDetailmodal(ResultofLanguage);

                        return Json(ResultofLanguage);
                    }
                    else
                        return Json(new
                        {
                            error = "Enter Valid Title"
                        });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveUsedLanguage(string paramdata)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    UsedLanguages model = JsonConvert.DeserializeObject<UsedLanguages>(paramdata);
                    model.CreatedBy = userCredentials.id;

                    var existusedLang = translatorRepository.GetUsedLanguageByLayoutIdandLangId(model.LayoutRefId, model.LanguageRefId);
                    if (existusedLang.Count > 0)
                    {

                        return Json(new
                        {
                            error = "Selected Language Already Exists."
                        });
                    }
                    var usedLanguage = translatorRepository.SaveUsedLanguage(model);

                    TempData["LayoutIds"] = usedLanguage.LayoutRefId + "`" + usedLanguage.TableId;

                    if (usedLanguage.ActiveList == "Activities")
                        return RedirectToAction("GetLangActivities");
                    else if (usedLanguage.ActiveList == "Registration")
                        return RedirectToAction("GetLangRegistration");
                    else
                        return RedirectToAction("GetAllLangDetail");
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult AddLanguage(int LayoutID, string ActiveLanguage, int TableId)
        {
            if (IsServerConnected(connectionStrings))
            {
                UsedLanguages usedLanguage = new UsedLanguages();

                usedLanguage.LayoutRefId = LayoutID;
                usedLanguage.ActiveList = ActiveLanguage;
                usedLanguage.TableId = TableId;

                var usedlanguageList = translatorRepository.GetUsedLanguage(LayoutID);
                usedLanguage.LanguageMasters = translatorRepository.GetAllLanguage();
                return Json(usedLanguage);
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public FileResult ExportTranslations(int LayoutId, string LanguageIds)
        {
            var URL = ExportTranslation(LayoutId, LanguageIds);
            byte[] fileBytes = System.IO.File.ReadAllBytes(URL);
            string fileName = translatorRepository.GetExportFileName(LayoutId);
            return File(fileBytes, "application/vnd.ms-excel", (fileName ?? DateTime.Now.ToString("yyyyMMddhhmmss")) + ".xlsx");
        }
        public string ExportTranslation(int LayoutId, string LanguageIds)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath + $@"\Translations\Export";
            UserBusinessModal userCredentials = GetUserCredentials();
            string sFileName = userCredentials.id + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
            string URL = Path.Combine(sWebRootFolder, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            if (file.Exists)
            {
                file.Delete();
                file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            }
            using (ExcelPackage package = new ExcelPackage(file))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Translations");

                var IsCopyLayout = translatorRepository.GetIsCopyLayout(LayoutId);
                var langDetail = translatorRepository.GetIsDefaultLayoutLanguage(LayoutId);
                var translationList = translatorRepository.GetLangDetail(9999999, LayoutId, 0, IsCopyLayout, false).Where(x=> x.LangRefColTableId != (int)LangRefColTable.Questions_Name).ToList();
                var languageList = translatorRepository.GetSelectedLanguages(LanguageIds, LayoutId);

                int col = 5;

                worksheet.Cells[1, 1].Value = "S.No";
                worksheet.Cells[1, 2].Value = "Reference No";
                worksheet.Cells[1, 3].Value = "Item";
                worksheet.Cells[1, 4].Value = "Detail";
                foreach (LanguageMaster lang in languageList.OrderByDescending(x => x.IsDefault).ThenBy(x => x.LanguageDescription))
                {
                    if (LanguagesEnum.English.ToString() == lang.LanguageDescription && langDetail.IsDefaultEnglish == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Arabic.ToString() == lang.LanguageDescription && langDetail.IsDefaultArabic == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Bulgarian.ToString() == lang.LanguageDescription && langDetail.IsDefaultBulgarian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Croatian.ToString() == lang.LanguageDescription && langDetail.IsDefaultCroatian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Czech.ToString() == lang.LanguageDescription && langDetail.IsDefaultCzech == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Danish.ToString() == lang.LanguageDescription && langDetail.IsDefaultDanish == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Dutch.ToString() == lang.LanguageDescription && langDetail.IsDefaultDutch == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Estonian.ToString() == lang.LanguageDescription && langDetail.IsDefaultEstonian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Finnish.ToString() == lang.LanguageDescription && langDetail.IsDefaultFinnish == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.French.ToString() == lang.LanguageDescription && langDetail.IsDefaultFrench == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.German.ToString() == lang.LanguageDescription && langDetail.IsDefaultGerman == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Greek.ToString() == lang.LanguageDescription && langDetail.IsDefaultGreek == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Hungarian.ToString() == lang.LanguageDescription && langDetail.IsDefaultHungarian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Italian.ToString() == lang.LanguageDescription && langDetail.IsDefaultItalian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Latvian.ToString() == lang.LanguageDescription && langDetail.IsDefaultLatvian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Lithuanian.ToString() == lang.LanguageDescription && langDetail.IsDefaultLithuanian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Macedonian.ToString() == lang.LanguageDescription && langDetail.IsDefaultMacedonian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Norwegian.ToString() == lang.LanguageDescription && langDetail.IsDefaultNorwegian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Polish.ToString() == lang.LanguageDescription && langDetail.IsDefaultPolish == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Portuguese.ToString() == lang.LanguageDescription && langDetail.IsDefaultPortuguese == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Romanian.ToString() == lang.LanguageDescription && langDetail.IsDefaultRomanian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Russian.ToString() == lang.LanguageDescription && langDetail.IsDefaultRussian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Slovak.ToString() == lang.LanguageDescription && langDetail.IsDefaultSlovak == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Slovene.ToString() == lang.LanguageDescription && langDetail.IsDefaultSlovene == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Spanish.ToString() == lang.LanguageDescription && langDetail.IsDefaultSpanish == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Swedish.ToString() == lang.LanguageDescription && langDetail.IsDefaultSwedish == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Turkish.ToString() == lang.LanguageDescription && langDetail.IsDefaultTurkish == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Ukrainian.ToString() == lang.LanguageDescription && langDetail.IsDefaultUkrainian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Belarusian.ToString() == lang.LanguageDescription && langDetail.IsDefaultBelarusian == 1)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.English.ToString() == lang.LanguageDescription && langDetail.IsDefaultEnglish == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Arabic.ToString() == lang.LanguageDescription && langDetail.IsDefaultArabic == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Bulgarian.ToString() == lang.LanguageDescription && langDetail.IsDefaultBulgarian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Croatian.ToString() == lang.LanguageDescription && langDetail.IsDefaultCroatian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Czech.ToString() == lang.LanguageDescription && langDetail.IsDefaultCzech == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Danish.ToString() == lang.LanguageDescription && langDetail.IsDefaultDanish == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Dutch.ToString() == lang.LanguageDescription && langDetail.IsDefaultDutch == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Estonian.ToString() == lang.LanguageDescription && langDetail.IsDefaultEstonian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Finnish.ToString() == lang.LanguageDescription && langDetail.IsDefaultFinnish == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.French.ToString() == lang.LanguageDescription && langDetail.IsDefaultFrench == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.German.ToString() == lang.LanguageDescription && langDetail.IsDefaultGerman == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Greek.ToString() == lang.LanguageDescription && langDetail.IsDefaultGreek == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Hungarian.ToString() == lang.LanguageDescription && langDetail.IsDefaultHungarian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Italian.ToString() == lang.LanguageDescription && langDetail.IsDefaultItalian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Latvian.ToString() == lang.LanguageDescription && langDetail.IsDefaultLatvian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Lithuanian.ToString() == lang.LanguageDescription && langDetail.IsDefaultLithuanian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Macedonian.ToString() == lang.LanguageDescription && langDetail.IsDefaultMacedonian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Norwegian.ToString() == lang.LanguageDescription && langDetail.IsDefaultNorwegian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Polish.ToString() == lang.LanguageDescription && langDetail.IsDefaultPolish == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Portuguese.ToString() == lang.LanguageDescription && langDetail.IsDefaultPortuguese == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Romanian.ToString() == lang.LanguageDescription && langDetail.IsDefaultRomanian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Russian.ToString() == lang.LanguageDescription && langDetail.IsDefaultRussian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Slovak.ToString() == lang.LanguageDescription && langDetail.IsDefaultSlovak == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Slovene.ToString() == lang.LanguageDescription && langDetail.IsDefaultSlovene == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Spanish.ToString() == lang.LanguageDescription && langDetail.IsDefaultSpanish == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Swedish.ToString() == lang.LanguageDescription && langDetail.IsDefaultSwedish == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Turkish.ToString() == lang.LanguageDescription && langDetail.IsDefaultTurkish == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Ukrainian.ToString() == lang.LanguageDescription && langDetail.IsDefaultUkrainian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Belarusian.ToString() == lang.LanguageDescription && langDetail.IsDefaultBelarusian == 0)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    worksheet.Cells[1, col].Value = lang.LanguageDescription;
                    col++;
                }

                int sno = 1;
                int row = 2;
                foreach (LangDetail langinfo in translationList)
                {
                    col = 0;
                    worksheet.Cells[row, ++col].Value = sno;
                    sno++;
                    worksheet.Cells[row, ++col].Value = "TR" + langinfo.LangRefDetailId.ToString("000000");
                    worksheet.Cells[row, ++col].Value = langinfo.PropertiesName;
                    worksheet.Cells[row, ++col].Value = langinfo.PropertiesPath;
                    foreach (LanguageMaster languagename in languageList.OrderByDescending(x => x.IsDefault).ThenBy(x => x.LanguageDescription))
                    {
                        if (LanguagesEnum.English.ToString() == languagename.LanguageDescription && langDetail.IsDefaultEnglish == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.English; }
                        else if (LanguagesEnum.Arabic.ToString() == languagename.LanguageDescription && langDetail.IsDefaultArabic == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Arabic; }
                        else if (LanguagesEnum.Bulgarian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultBulgarian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Bulgarian; }
                        else if (LanguagesEnum.Croatian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultCroatian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Croatian; }
                        else if (LanguagesEnum.Czech.ToString() == languagename.LanguageDescription && langDetail.IsDefaultCzech == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Czech; }
                        else if (LanguagesEnum.Danish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultDanish == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Danish; }
                        else if (LanguagesEnum.Dutch.ToString() == languagename.LanguageDescription && langDetail.IsDefaultDutch == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Dutch; }
                        else if (LanguagesEnum.Estonian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultEstonian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Estonian; }
                        else if (LanguagesEnum.Finnish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultFinnish == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Finnish; }
                        else if (LanguagesEnum.French.ToString() == languagename.LanguageDescription && langDetail.IsDefaultFrench == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.French; }
                        else if (LanguagesEnum.German.ToString() == languagename.LanguageDescription && langDetail.IsDefaultGerman == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.German; }
                        else if (LanguagesEnum.Greek.ToString() == languagename.LanguageDescription && langDetail.IsDefaultGreek == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Greek; }
                        else if (LanguagesEnum.Hungarian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultHungarian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Hungarian; }
                        else if (LanguagesEnum.Italian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultItalian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Italian; }
                        else if (LanguagesEnum.Latvian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultLatvian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Latvian; }
                        else if (LanguagesEnum.Lithuanian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultLithuanian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Lithuanian; }
                        else if (LanguagesEnum.Macedonian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultMacedonian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Macedonian; }
                        else if (LanguagesEnum.Norwegian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultNorwegian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Norwegian; }
                        else if (LanguagesEnum.Polish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultPolish == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Polish; }
                        else if (LanguagesEnum.Portuguese.ToString() == languagename.LanguageDescription && langDetail.IsDefaultPortuguese == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Portuguese; }
                        else if (LanguagesEnum.Romanian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultRomanian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Romanian; }
                        else if (LanguagesEnum.Russian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultRussian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Russian; }
                        else if (LanguagesEnum.Slovak.ToString() == languagename.LanguageDescription && langDetail.IsDefaultSlovak == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Slovak; }
                        else if (LanguagesEnum.Slovene.ToString() == languagename.LanguageDescription && langDetail.IsDefaultSlovene == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Slovene; }
                        else if (LanguagesEnum.Spanish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultSpanish == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Spanish; }
                        else if (LanguagesEnum.Swedish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultSwedish == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Swedish; }
                        else if (LanguagesEnum.Turkish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultTurkish == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Turkish; }
                        else if (LanguagesEnum.Ukrainian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultUkrainian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Ukrainian; }
                        else if (LanguagesEnum.Belarusian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultBelarusian == 1)
                        { worksheet.Cells[row, ++col].Value = langinfo.Belarusian; }
                        else if (LanguagesEnum.English.ToString() == languagename.LanguageDescription && langDetail.IsDefaultEnglish == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.English; }
                        else if (LanguagesEnum.Arabic.ToString() == languagename.LanguageDescription && langDetail.IsDefaultArabic == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Arabic; }
                        else if (LanguagesEnum.Bulgarian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultBulgarian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Bulgarian; }
                        else if (LanguagesEnum.Croatian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultCroatian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Croatian; }
                        else if (LanguagesEnum.Czech.ToString() == languagename.LanguageDescription && langDetail.IsDefaultCzech == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Czech; }
                        else if (LanguagesEnum.Danish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultDanish == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Danish; }
                        else if (LanguagesEnum.Dutch.ToString() == languagename.LanguageDescription && langDetail.IsDefaultDutch == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Dutch; }
                        else if (LanguagesEnum.Estonian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultEstonian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Estonian; }
                        else if (LanguagesEnum.Finnish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultFinnish == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Finnish; }
                        else if (LanguagesEnum.French.ToString() == languagename.LanguageDescription && langDetail.IsDefaultFrench == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.French; }
                        else if (LanguagesEnum.German.ToString() == languagename.LanguageDescription && langDetail.IsDefaultGerman == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.German; }
                        else if (LanguagesEnum.Greek.ToString() == languagename.LanguageDescription && langDetail.IsDefaultGreek == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Greek; }
                        else if (LanguagesEnum.Hungarian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultHungarian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Hungarian; }
                        else if (LanguagesEnum.Italian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultItalian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Italian; }
                        else if (LanguagesEnum.Latvian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultLatvian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Latvian; }
                        else if (LanguagesEnum.Lithuanian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultLithuanian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Lithuanian; }
                        else if (LanguagesEnum.Macedonian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultMacedonian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Macedonian; }
                        else if (LanguagesEnum.Norwegian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultNorwegian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Norwegian; }
                        else if (LanguagesEnum.Polish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultPolish == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Polish; }
                        else if (LanguagesEnum.Portuguese.ToString() == languagename.LanguageDescription && langDetail.IsDefaultPortuguese == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Portuguese; }
                        else if (LanguagesEnum.Romanian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultRomanian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Romanian; }
                        else if (LanguagesEnum.Russian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultRussian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Russian; }
                        else if (LanguagesEnum.Slovak.ToString() == languagename.LanguageDescription && langDetail.IsDefaultSlovak == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Slovak; }
                        else if (LanguagesEnum.Slovene.ToString() == languagename.LanguageDescription && langDetail.IsDefaultSlovene == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Slovene; }
                        else if (LanguagesEnum.Spanish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultSpanish == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Spanish; }
                        else if (LanguagesEnum.Swedish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultSwedish == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Swedish; }
                        else if (LanguagesEnum.Turkish.ToString() == languagename.LanguageDescription && langDetail.IsDefaultTurkish == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Turkish; }
                        else if (LanguagesEnum.Ukrainian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultUkrainian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Ukrainian; }
                        else if (LanguagesEnum.Belarusian.ToString() == languagename.LanguageDescription && langDetail.IsDefaultBelarusian == 0)
                        { worksheet.Cells[row, ++col].Value = langinfo.Belarusian; }
                    }

                    row++;
                }

                package.Save();
            }
            return URL;
        }
        public IActionResult UploadTranslations(IList<IFormFile> UploadTranslation)
        {
            foreach (var file in UploadTranslation)
            {
                string sWebRootFolder = _hostingEnvironment.WebRootPath + $@"\Translations\Import";
                var userCredentials = GetUserCredentials();
                string sFileName = userCredentials.id + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";

                using (FileStream fs = System.IO.File.Create(Path.Combine(sWebRootFolder, sFileName)))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                FileInfo fileinfo = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                try
                {
                    var returnresult = "";
                    using (ExcelPackage package = new ExcelPackage(fileinfo))
                    {
                        StringBuilder sb = new StringBuilder();
                        ExcelWorksheet worksheet = package.Workbook.Worksheets["Translations"];
                        int rowCount = worksheet.Dimension.Rows;
                        int ColCount = worksheet.Dimension.Columns;
                        List<string> Languages = new List<string>();
                        for (int col = 5; col <= ColCount; col++)
                        {
                            Languages.Add(worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString());
                        }

                        for (int row = 2; row <= rowCount; row++)
                        {
                            LangDetail langdetail = new LangDetail();
                            int LangRefDetailId = 0;
                            Int32.TryParse(worksheet.Cells[row, 2].Value.ToString().Replace("TR", ""), out LangRefDetailId);
                            var langRefdetail = translatorRepository.GetLangRefDetail(LangRefDetailId);

                            if (langRefdetail != null)
                            {
                                langdetail.Id = langRefdetail.LangDetailId ?? 0;
                                langdetail.LangRefDetailId = LangRefDetailId;
                                langdetail.CustomerRefId = langRefdetail.CustomerRefId ?? 0;
                                langdetail.LayoutRefId = langRefdetail.LayoutRefId;
                                langdetail.LangRefColTableId = langRefdetail.LangRefColTableId;
                                langdetail.CreatedBy = userCredentials.id;
                                langdetail.UpdatedBy = userCredentials.id;

                                for (int col = 5; col <= ColCount; col++)
                                {
                                    if (LanguagesEnum.English.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.English = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Arabic.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Arabic = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Bulgarian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Bulgarian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Croatian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Croatian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Czech.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Czech = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Danish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Danish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Dutch.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Dutch = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Estonian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Estonian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Finnish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Finnish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.French.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.French = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.German.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.German = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Greek.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Greek = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Hungarian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Hungarian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Italian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Italian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Latvian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Latvian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Lithuanian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Lithuanian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Macedonian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Macedonian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Norwegian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Norwegian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Polish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Polish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Portuguese.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Portuguese = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Romanian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Romanian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Russian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Russian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Slovak.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Slovak = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Slovene.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Slovene = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Spanish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Spanish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Swedish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Swedish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Turkish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Turkish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Ukrainian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Ukrainian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Belarusian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        langdetail.Belarusian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                }
                                if (langRefdetail.LangRefColTableId == (int)LangRefColTable.InfoMessageContentDetail_MessageContent)
                                {
                                    if (langdetail.Arabic != null)
                                        langdetail.Arabic = SplitMessageContentString(langdetail.Arabic, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Bulgarian != null)
                                        langdetail.Bulgarian = SplitMessageContentString(langdetail.Bulgarian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Croatian != null)
                                        langdetail.Croatian = SplitMessageContentString(langdetail.Croatian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Danish != null)
                                        langdetail.Danish = SplitMessageContentString(langdetail.Danish, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Dutch != null)
                                        langdetail.Dutch = SplitMessageContentString(langdetail.Dutch, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.English != null)
                                        langdetail.English = SplitMessageContentString(langdetail.English, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Estonian != null)
                                        langdetail.Estonian = SplitMessageContentString(langdetail.Estonian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Finnish != null)
                                        langdetail.Finnish = SplitMessageContentString(langdetail.Finnish, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.French != null)
                                        langdetail.French = SplitMessageContentString(langdetail.French, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.German != null)
                                        langdetail.German = SplitMessageContentString(langdetail.German, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Greek != null)
                                        langdetail.Greek = SplitMessageContentString(langdetail.Greek, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Hungarian != null)
                                        langdetail.Hungarian = SplitMessageContentString(langdetail.Hungarian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Italian != null)
                                        langdetail.Italian = SplitMessageContentString(langdetail.Italian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Latvian != null)
                                        langdetail.Latvian = SplitMessageContentString(langdetail.Latvian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Lithuanian != null)
                                        langdetail.Lithuanian = SplitMessageContentString(langdetail.Lithuanian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Macedonian != null)
                                        langdetail.Macedonian = SplitMessageContentString(langdetail.Macedonian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Norwegian != null)
                                        langdetail.Norwegian = SplitMessageContentString(langdetail.Norwegian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Polish != null)
                                        langdetail.Polish = SplitMessageContentString(langdetail.Polish, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Portuguese != null)
                                        langdetail.Portuguese = SplitMessageContentString(langdetail.Portuguese, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Romanian != null)
                                        langdetail.Romanian = SplitMessageContentString(langdetail.Romanian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Russian != null)
                                        langdetail.Russian = SplitMessageContentString(langdetail.Russian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Slovak != null)
                                        langdetail.Slovak = SplitMessageContentString(langdetail.Slovak, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Slovene != null)
                                        langdetail.Slovene = SplitMessageContentString(langdetail.Slovene, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Spanish != null)
                                        langdetail.Spanish = SplitMessageContentString(langdetail.Spanish, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Swedish != null)
                                        langdetail.Swedish = SplitMessageContentString(langdetail.Swedish, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Turkish != null)
                                        langdetail.Turkish = SplitMessageContentString(langdetail.Turkish, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Ukrainian != null)
                                        langdetail.Ukrainian = SplitMessageContentString(langdetail.Ukrainian, 35).Aggregate((a, b) => a + " " + b);
                                    if (langdetail.Belarusian != null)
                                        langdetail.Belarusian = SplitMessageContentString(langdetail.Belarusian, 35).Aggregate((a, b) => a + " " + b);
                                }

                                var result = translatorRepository.SaveTranslations(langdetail);
                                if (result == "Error")
                                    returnresult = "Process Failed, Some Translation Changes Not updated, Please Try Again";
                            }
                            else
                                return Content("Process Failed, The Translation Record (" + worksheet.Cells[row, 2].Value.ToString() + ") Not Exist");
                        }
                        return Content(returnresult);
                    }
                }
                catch (Exception ex)
                {
                    return Content("Process Failed, Some Translation Changes Not updated, Please Try Again");
                }
            }
            return Content("");
        }
        public ActionResult SaveTranslations(string changedList)
        {
            var returnresult = "success";
            try
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                List<LangDetail> changedLangDetails = JsonConvert.DeserializeObject<List<LangDetail>>(WebUtility.UrlDecode(changedList), settings);
                string result = "";

                if (changedLangDetails.Count > 0)
                {
                    var userCredentials = GetUserCredentials();
                    foreach (LangDetail d in changedLangDetails)
                    {
                        d.CreatedBy = userCredentials.id.ToString();
                        d.CreatedOn = DateTime.Now;
                        d.UpdatedBy = userCredentials.id.ToString();
                        d.UpdatedOn = DateTime.Now;
                        d.IsDefaultTranslation = 0;
                        result = translatorRepository.SaveTranslations(d);

                        if (d.LangRefColTableId == (int)LangRefColTable.Questions_Name)
                        {
                            var getPropertiesModel = translatorRepository.GetPropertiesLangDetail(d);
                            if (getPropertiesModel.Id > 0)
                            {
                                getPropertiesModel.CreatedBy = userCredentials.id.ToString();
                                getPropertiesModel.CreatedOn = DateTime.Now;
                                getPropertiesModel.UpdatedBy = userCredentials.id.ToString();
                                getPropertiesModel.UpdatedOn = DateTime.Now;

                                translatorRepository.SaveTranslations(getPropertiesModel);
                            }
                        }
                        else if (d.LangRefColTableId == (int)LangRefColTable.PlanningselectProperties_Title)
                        {
                            var getQuestionsModel = translatorRepository.GetPlanningSelectLangDetail(d, (d.QuestionRefId ?? 0));
                            if (getQuestionsModel.Id > 0)
                            {
                                getQuestionsModel.CreatedBy = userCredentials.id.ToString();
                                getQuestionsModel.CreatedOn = DateTime.Now;
                                getQuestionsModel.UpdatedBy = userCredentials.id.ToString();
                                getQuestionsModel.UpdatedOn = DateTime.Now;

                                translatorRepository.SaveTranslations(getQuestionsModel);
                            }
                        }
                        else if (d.LangRefColTableId == (int)LangRefColTable.PlanningselectProperties_OtherTitle)
                        {
                            var questionRefId = translatorRepository.GetPlanningSelectEntryQuestionId((d.QuestionRefId ?? 0));
                            if(questionRefId > 0)
                            {
                                var getQuestionsModel = translatorRepository.GetQuestionsLangDetail(d, questionRefId);
                                if (getQuestionsModel.Id > 0)
                                {
                                    getQuestionsModel.CreatedBy = userCredentials.id.ToString();
                                    getQuestionsModel.CreatedOn = DateTime.Now;
                                    getQuestionsModel.UpdatedBy = userCredentials.id.ToString();
                                    getQuestionsModel.UpdatedOn = DateTime.Now;

                                    translatorRepository.SaveTranslations(getQuestionsModel);
                                }
                            }
                        }
                        else
                        {
                            var getQuestionsModel = translatorRepository.GetQuestionsLangDetail(d, (d.QuestionRefId ?? 0));
                            if (getQuestionsModel.Id > 0)
                            {
                                getQuestionsModel.CreatedBy = userCredentials.id.ToString();
                                getQuestionsModel.CreatedOn = DateTime.Now;
                                getQuestionsModel.UpdatedBy = userCredentials.id.ToString();
                                getQuestionsModel.UpdatedOn = DateTime.Now;

                                translatorRepository.SaveTranslations(getQuestionsModel);
                            }

                        }


                        if (result == "Error")
                            returnresult = "Process Failed, Some Translation Changes Not updated, Please Try Again";
                    }
                }
                return Json(returnresult);
            }
            catch (Exception ex)
            {
                return Json("Process Failed, Some Translation Changes Not updated, Please Try Again");
            }
        }
        public ActionResult GetExportExcelValidation(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = translatorRepository.GetExportExcelValidation(LayoutId);

                    if (result)
                        return Json(new
                        {
                            error = "Process Faild. The Main Language Not Fully Translated"
                        });
                    else
                        return Json("Success");
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetLayoutDefaultLanguageId(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = translatorRepository.GetLayoutDefaultLanguageId(LayoutId);
                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public IActionResult UploadGeneralTranslations(IList<IFormFile> UploadGeneralTranslation)
        {
            foreach (var file in UploadGeneralTranslation)
            {
                string sWebRootFolder = _hostingEnvironment.WebRootPath + $@"\Translations\Import";
                var userCredentials = GetUserCredentials();
                string sFileName = "CT_" + userCredentials.id + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";

                using (FileStream fs = System.IO.File.Create(Path.Combine(sWebRootFolder, sFileName)))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                FileInfo fileinfo = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                try
                {
                    var returnresult = "";
                    using (ExcelPackage package = new ExcelPackage(fileinfo))
                    {
                        StringBuilder sb = new StringBuilder();
                        returnresult = ImportCentralTranslation(package, userCredentials.id, 1);
                        return Content(returnresult);
                    }
                }
                catch (Exception ex)
                {
                    return Content("Process Failed, Some Translation Changes Not updated, Please Try Again");
                }
            }
            return Content("");
        }
        public IActionResult UploadCustomerSpecificTranslations(IList<IFormFile> UploadCustomerSpecificTranslation)
        {
            foreach (var file in UploadCustomerSpecificTranslation)
            {
                string sWebRootFolder = _hostingEnvironment.WebRootPath + $@"\Translations\Import";
                var userCredentials = GetUserCredentials();
                string sFileName = "CT_" + userCredentials.id + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";

                using (FileStream fs = System.IO.File.Create(Path.Combine(sWebRootFolder, sFileName)))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                FileInfo fileinfo = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                try
                {
                    var returnresult = "";
                    using (ExcelPackage package = new ExcelPackage(fileinfo))
                    {
                        StringBuilder sb = new StringBuilder();
                        returnresult = ImportCentralTranslation(package, userCredentials.id, 0);
                        return Content(returnresult);
                    }
                }
                catch (Exception ex)
                {
                    return Content("Process Failed, Some Translation Changes Not updated, Please Try Again");
                }
            }
            return Content("");
        }
        public IActionResult UploadCommonUITranslations(IList<IFormFile> UploadCommonUITranslation)
        {
            foreach (var file in UploadCommonUITranslation)
            {
                string sWebRootFolder = _hostingEnvironment.WebRootPath + $@"\Translations\Import";
                var userCredentials = GetUserCredentials();
                string sFileName = "CUI_" + userCredentials.id + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";

                using (FileStream fs = System.IO.File.Create(Path.Combine(sWebRootFolder, sFileName)))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                FileInfo fileinfo = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                try
                {
                    var returnresult = "";
                    using (ExcelPackage package = new ExcelPackage(fileinfo))
                    {
                        StringBuilder sb = new StringBuilder();
                        ExcelWorksheet worksheet = package.Workbook.Worksheets["Translations"];
                        int rowCount = worksheet.Dimension.Rows;
                        int ColCount = worksheet.Dimension.Columns;
                        List<string> Languages = new List<string>();
                        for (int col = 3; col <= ColCount; col++)
                        {
                            Languages.Add(worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString());
                        }

                        for (int row = 2; row <= rowCount; row++)
                        {
                            UITranslationDetail translationdetail = new UITranslationDetail();
                            int translationDetailId = 0;
                            Int32.TryParse(worksheet.Cells[row, 2].Value.ToString().Replace("CUI", ""), out translationDetailId);

                            var langDetailModel = translatorRepository.GetUITranslationDetail(translationDetailId);

                            if (langDetailModel != null)
                            {
                                translationdetail.UITranslationDetailId = translationDetailId;
                                translationdetail.CreatedBy = userCredentials.id;
                                translationdetail.UpdatedBy = userCredentials.id;

                                for (int col = 2; col <= ColCount; col++)
                                {
                                    if (LanguagesEnum.English.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.English = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Arabic.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Arabic = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Bulgarian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Bulgarian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Croatian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Croatian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Czech.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Czech = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Danish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Danish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Dutch.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Dutch = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Estonian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Estonian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Finnish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Finnish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.French.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.French = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.German.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.German = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Greek.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Greek = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Hungarian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Hungarian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Italian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Italian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Latvian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Latvian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Lithuanian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Lithuanian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Macedonian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Macedonian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Norwegian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Norwegian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Polish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Polish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Portuguese.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Portuguese = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Romanian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Romanian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Russian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Russian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Slovak.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Slovak = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Slovene.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Slovene = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Spanish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Spanish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Swedish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Swedish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Turkish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Turkish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Ukrainian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Ukrainian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Belarusian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Belarusian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                }

                                var result = translatorRepository.SaveCommonUITranslation(translationdetail);
                                if (result == "Error")
                                    returnresult = "Process Failed, Some Translation Changes Not updated, Please Try Again";
                            }
                            else
                                return Content("Process Failed, The Translation Record (" + worksheet.Cells[row, 2].Value.ToString() + ") Not Exist");

                        }
                        return Content(returnresult);
                    }
                }
                catch (Exception ex)
                {
                    return Content("Process Failed, Some Translation Changes Not updated, Please Try Again");
                }
            }
            return Content("");
        }
        public IActionResult UploadSpecificUITranslations(IList<IFormFile> UploadSpecificUITranslation)
        {
            foreach (var file in UploadSpecificUITranslation)
            {
                string sWebRootFolder = _hostingEnvironment.WebRootPath + $@"\Translations\Import";
                var userCredentials = GetUserCredentials();
                string sFileName = "SUI_" + userCredentials.id + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";

                using (FileStream fs = System.IO.File.Create(Path.Combine(sWebRootFolder, sFileName)))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                FileInfo fileinfo = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                try
                {
                    var returnresult = "";
                    using (ExcelPackage package = new ExcelPackage(fileinfo))
                    {
                        StringBuilder sb = new StringBuilder();
                        ExcelWorksheet worksheet = package.Workbook.Worksheets["Translations"];
                        int rowCount = worksheet.Dimension.Rows;
                        int ColCount = worksheet.Dimension.Columns;
                        List<string> Languages = new List<string>();
                        for (int col = 4; col <= ColCount; col++)
                        {
                            Languages.Add(worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString());
                        }

                        for (int row = 2; row <= rowCount; row++)
                        {
                            UILangDetail translationdetail = new UILangDetail();
                            int translationDetailId = 0;
                            Int32.TryParse(worksheet.Cells[row, 2].Value.ToString().Replace("SUI", ""), out translationDetailId);

                            var langDetailModel = translatorRepository.GetUILangDetail(translationDetailId);

                            if (langDetailModel != null)
                            {
                                translationdetail.UITranslationDetailRefId = langDetailModel.UITranslationDetailRefId;
                                translationdetail.UITranslationId = translationDetailId;
                                translationdetail.UpdatedBy = userCredentials.id;

                                for (int col = 4; col <= ColCount; col++)
                                {
                                    if (LanguagesEnum.English.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.English = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Arabic.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Arabic = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Bulgarian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Bulgarian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Croatian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Croatian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Czech.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Czech = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Danish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Danish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Dutch.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Dutch = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Estonian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Estonian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Finnish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Finnish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.French.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.French = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.German.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.German = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Greek.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Greek = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Hungarian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Hungarian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Italian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Italian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Latvian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Latvian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Lithuanian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Lithuanian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Macedonian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Macedonian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Norwegian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Norwegian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Polish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Polish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Portuguese.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Portuguese = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Romanian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Romanian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Russian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Russian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Slovak.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Slovak = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Slovene.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Slovene = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Spanish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Spanish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Swedish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Swedish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Turkish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Turkish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Ukrainian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Ukrainian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                    else if (LanguagesEnum.Belarusian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                                    {
                                        translationdetail.Belarusian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                                    }
                                }

                                var result = translatorRepository.SaveSpecificUITranslation(translationdetail);
                                if (result == "Error")
                                    returnresult = "Process Failed, Some Translation Changes Not updated, Please Try Again";
                            }
                            else
                                return Content("Process Failed, The Translation Record (" + worksheet.Cells[row, 2].Value.ToString() + ") Not Exist");

                        }
                        return Content(returnresult);
                    }
                }
                catch (Exception ex)
                {
                    return Content("Process Failed, Some Translation Changes Not updated, Please Try Again");
                }
            }
            return Content("");
        }
        public FileResult ExportCentralTranslations(int CustomerId)
        {
            var URL = ExportCentralTranslation(CustomerId);
            byte[] fileBytes = System.IO.File.ReadAllBytes(URL);
            var userCredentials = GetUserCredentials();
            string fileName = (CustomerId > 0 ? translatorRepository.GetExportFileNameByCustomer(CustomerId) : "GeneralTranslation");
            //string fileName = userCredentials.id + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
            return File(fileBytes, "application/vnd.ms-excel", (fileName ?? DateTime.Now.ToString("yyyyMMddhhmmss")) + ".xlsx");
        }
        public string ExportCentralTranslation(int CustomerId)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath + $@"\Translations\Export";
            var userCredentials = GetUserCredentials();
            string sFileName = (CustomerId > 0 ? "CST_" : "GT_") + userCredentials.id + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
            string URL = Path.Combine(sWebRootFolder, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            if (file.Exists)
            {
                file.Delete();
                file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            }
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Translations");

                List<LangDetail> translationList = new List<LangDetail>();

                if (CustomerId == 0)
                    translationList = translatorRepository.GetGeneralTranslation(0);
                else
                    translationList = translatorRepository.GetCustomerSpecificTranslation(CustomerId);

                var languageList = ((IEnumerable<int>)Enum.GetValues(typeof(LanguagesEnum))).Where(ev => ev == 1).Select(ev => new { Value = ev, LanguageDescription = Enum.GetName(typeof(LanguagesEnum), ev) }).ToList();
                languageList.AddRange(((IEnumerable<int>)Enum.GetValues(typeof(LanguagesEnum))).Where(ev => ev != 1).Select(ev => new { Value = ev, LanguageDescription = Enum.GetName(typeof(LanguagesEnum), ev) }).OrderBy(ev => ev.LanguageDescription).ToList());
                int col = 3;

                worksheet.Cells[1, 1].Value = "S.No";
                worksheet.Cells[1, 2].Value = "Reference No";
                foreach (var lang in languageList)
                {
                    if (LanguagesEnum.English.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Arabic.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Bulgarian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Croatian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Czech.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Danish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Dutch.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Estonian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Finnish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.French.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.German.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Greek.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Hungarian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Italian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Latvian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Lithuanian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Macedonian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Norwegian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Polish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Portuguese.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Romanian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Russian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Slovak.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Slovene.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Spanish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Swedish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Turkish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Ukrainian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Belarusian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }

                    col++;
                }

                int sno = 1;
                int row = 2;
                foreach (var langinfo in translationList)
                {
                    col = 0;
                    worksheet.Cells[row, ++col].Value = sno;
                    sno++;
                    worksheet.Cells[row, ++col].Value = (CustomerId > 0 ? "CST" : "GT") + langinfo.Id.ToString("000000");
                    foreach (var languagename in languageList)
                    {
                        if (LanguagesEnum.English.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.English; }
                        else if (LanguagesEnum.Arabic.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Arabic; }
                        else if (LanguagesEnum.Bulgarian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Bulgarian; }
                        else if (LanguagesEnum.Croatian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Croatian; }
                        else if (LanguagesEnum.Czech.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Czech; }
                        else if (LanguagesEnum.Danish.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Danish; }
                        else if (LanguagesEnum.Dutch.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Dutch; }
                        else if (LanguagesEnum.Estonian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Estonian; }
                        else if (LanguagesEnum.Finnish.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Finnish; }
                        else if (LanguagesEnum.French.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.French; }
                        else if (LanguagesEnum.German.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.German; }
                        else if (LanguagesEnum.Greek.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Greek; }
                        else if (LanguagesEnum.Hungarian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Hungarian; }
                        else if (LanguagesEnum.Italian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Italian; }
                        else if (LanguagesEnum.Latvian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Latvian; }
                        else if (LanguagesEnum.Lithuanian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Lithuanian; }
                        else if (LanguagesEnum.Macedonian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Macedonian; }
                        else if (LanguagesEnum.Norwegian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Norwegian; }
                        else if (LanguagesEnum.Polish.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Polish; }
                        else if (LanguagesEnum.Portuguese.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Portuguese; }
                        else if (LanguagesEnum.Romanian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Romanian; }
                        else if (LanguagesEnum.Russian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Russian; }
                        else if (LanguagesEnum.Slovak.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Slovak; }
                        else if (LanguagesEnum.Slovene.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Slovene; }
                        else if (LanguagesEnum.Spanish.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Spanish; }
                        else if (LanguagesEnum.Swedish.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Swedish; }
                        else if (LanguagesEnum.Turkish.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Turkish; }
                        else if (LanguagesEnum.Ukrainian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Ukrainian; }
                        else if (LanguagesEnum.Belarusian.ToString() == languagename.LanguageDescription)
                        { worksheet.Cells[row, ++col].Value = langinfo.Belarusian; }
                    }

                    row++;
                }

                package.Save();
            }
            return URL;
        }
        public FileResult ExportUITranslations(int IsCommonUI)
        {
            var URL = ExportUITranslation(IsCommonUI);
            byte[] fileBytes = System.IO.File.ReadAllBytes(URL);
            var userCredentials = GetUserCredentials();
            string fileName = "UITranslation";
            //string fileName = userCredentials.id + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
            return File(fileBytes, "application/vnd.ms-excel", (fileName ?? DateTime.Now.ToString("yyyyMMddhhmmss")) + ".xlsx");
        }
        public string ExportUITranslation(int isCommonUI)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath + $@"\Translations\Export";
            var userCredentials = GetUserCredentials();
            string sFileName = (isCommonUI > 0 ? "CUI_" : "SUI_") + userCredentials.id + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
            string URL = Path.Combine(sWebRootFolder, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            if (file.Exists)
            {
                file.Delete();
                file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            }
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Translations");

                List<UITranslationDetail> commonTranslationList = new List<UITranslationDetail>();
                List<UILangDetail> specificTranslationList = new List<UILangDetail>();

                if (isCommonUI > 0)
                    commonTranslationList = translatorRepository.GetCommonUITranslation();
                else
                    specificTranslationList = translatorRepository.GetSpecificUITranslation();

                var languageList = ((IEnumerable<int>)Enum.GetValues(typeof(LanguagesEnum))).Where(ev => ev == 1).Select(ev => new { Value = ev, LanguageDescription = Enum.GetName(typeof(LanguagesEnum), ev) }).ToList();
                languageList.AddRange(((IEnumerable<int>)Enum.GetValues(typeof(LanguagesEnum))).Where(ev => ev != 1).Select(ev => new { Value = ev, LanguageDescription = Enum.GetName(typeof(LanguagesEnum), ev) }).OrderBy(ev => ev.LanguageDescription).ToList());
                int col = (isCommonUI > 0 ? 3 : 4);

                worksheet.Cells[1, 1].Value = "S.No";
                worksheet.Cells[1, 2].Value = "Reference No";

                if (isCommonUI == 0)
                    worksheet.Cells[1, 3].Value = "Path";

                foreach (var lang in languageList)
                {
                    if (LanguagesEnum.English.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Arabic.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Bulgarian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Croatian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Czech.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Danish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Dutch.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Estonian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Finnish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.French.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.German.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Greek.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Hungarian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Italian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Latvian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Lithuanian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Macedonian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Norwegian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Polish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Portuguese.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Romanian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Russian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Slovak.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Slovene.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Spanish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Swedish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Turkish.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Ukrainian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }
                    else if (LanguagesEnum.Belarusian.ToString() == lang.LanguageDescription)
                    { worksheet.Cells[1, col].Value = lang.LanguageDescription; }

                    col++;
                }

                int sno = 1;
                int row = 2;

                if (isCommonUI > 0)
                {
                    foreach (var langinfo in commonTranslationList)
                    {
                        col = 0;
                        worksheet.Cells[row, ++col].Value = sno;
                        sno++;
                        worksheet.Cells[row, ++col].Value = "CUI" + langinfo.UITranslationDetailId.ToString("000000");

                        foreach (var languagename in languageList)
                        {
                            if (LanguagesEnum.English.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.English; }
                            else if (LanguagesEnum.Arabic.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Arabic; }
                            else if (LanguagesEnum.Bulgarian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Bulgarian; }
                            else if (LanguagesEnum.Croatian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Croatian; }
                            else if (LanguagesEnum.Czech.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Czech; }
                            else if (LanguagesEnum.Danish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Danish; }
                            else if (LanguagesEnum.Dutch.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Dutch; }
                            else if (LanguagesEnum.Estonian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Estonian; }
                            else if (LanguagesEnum.Finnish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Finnish; }
                            else if (LanguagesEnum.French.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.French; }
                            else if (LanguagesEnum.German.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.German; }
                            else if (LanguagesEnum.Greek.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Greek; }
                            else if (LanguagesEnum.Hungarian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Hungarian; }
                            else if (LanguagesEnum.Italian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Italian; }
                            else if (LanguagesEnum.Latvian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Latvian; }
                            else if (LanguagesEnum.Lithuanian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Lithuanian; }
                            else if (LanguagesEnum.Macedonian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Macedonian; }
                            else if (LanguagesEnum.Norwegian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Norwegian; }
                            else if (LanguagesEnum.Polish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Polish; }
                            else if (LanguagesEnum.Portuguese.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Portuguese; }
                            else if (LanguagesEnum.Romanian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Romanian; }
                            else if (LanguagesEnum.Russian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Russian; }
                            else if (LanguagesEnum.Slovak.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Slovak; }
                            else if (LanguagesEnum.Slovene.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Slovene; }
                            else if (LanguagesEnum.Spanish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Spanish; }
                            else if (LanguagesEnum.Swedish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Swedish; }
                            else if (LanguagesEnum.Turkish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Turkish; }
                            else if (LanguagesEnum.Ukrainian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Ukrainian; }
                            else if (LanguagesEnum.Belarusian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Belarusian; }
                        }
                        row++;
                    }
                }
                else
                {
                    foreach (var langinfo in specificTranslationList)
                    {
                        col = 0;
                        worksheet.Cells[row, ++col].Value = sno;
                        sno++;
                        worksheet.Cells[row, ++col].Value = "SUI" + langinfo.UITranslationId.ToString("000000");
                        worksheet.Cells[row, ++col].Value = langinfo.Title;
                        foreach (var languagename in languageList)
                        {
                            if (LanguagesEnum.English.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.English; }
                            else if (LanguagesEnum.Arabic.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Arabic; }
                            else if (LanguagesEnum.Bulgarian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Bulgarian; }
                            else if (LanguagesEnum.Croatian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Croatian; }
                            else if (LanguagesEnum.Czech.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Czech; }
                            else if (LanguagesEnum.Danish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Danish; }
                            else if (LanguagesEnum.Dutch.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Dutch; }
                            else if (LanguagesEnum.Estonian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Estonian; }
                            else if (LanguagesEnum.Finnish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Finnish; }
                            else if (LanguagesEnum.French.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.French; }
                            else if (LanguagesEnum.German.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.German; }
                            else if (LanguagesEnum.Greek.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Greek; }
                            else if (LanguagesEnum.Hungarian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Hungarian; }
                            else if (LanguagesEnum.Italian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Italian; }
                            else if (LanguagesEnum.Latvian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Latvian; }
                            else if (LanguagesEnum.Lithuanian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Lithuanian; }
                            else if (LanguagesEnum.Macedonian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Macedonian; }
                            else if (LanguagesEnum.Norwegian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Norwegian; }
                            else if (LanguagesEnum.Polish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Polish; }
                            else if (LanguagesEnum.Portuguese.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Portuguese; }
                            else if (LanguagesEnum.Romanian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Romanian; }
                            else if (LanguagesEnum.Russian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Russian; }
                            else if (LanguagesEnum.Slovak.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Slovak; }
                            else if (LanguagesEnum.Slovene.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Slovene; }
                            else if (LanguagesEnum.Spanish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Spanish; }
                            else if (LanguagesEnum.Swedish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Swedish; }
                            else if (LanguagesEnum.Turkish.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Turkish; }
                            else if (LanguagesEnum.Ukrainian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Ukrainian; }
                            else if (LanguagesEnum.Belarusian.ToString() == languagename.LanguageDescription)
                            { worksheet.Cells[row, ++col].Value = langinfo.Belarusian; }
                        }
                        row++;
                    }
                }

                package.Save();
            }
            return URL;
        }
        public string ImportCentralTranslation(ExcelPackage package, string userId, int isGeneralTranslation)
        {
            var returnresult = "";

            ExcelWorksheet worksheet = package.Workbook.Worksheets["Translations"];
            int rowCount = worksheet.Dimension.Rows;
            int ColCount = worksheet.Dimension.Columns;
            List<string> Languages = new List<string>();
            for (int col = 3; col <= ColCount; col++)
            {
                Languages.Add(worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString());
            }

            for (int row = 2; row <= rowCount; row++)
            {
                LangDetail langdetail = new LangDetail();
                int LangDetailId = 0;
                if (isGeneralTranslation > 0)
                    Int32.TryParse(worksheet.Cells[row, 2].Value.ToString().Replace("GT", ""), out LangDetailId);
                else
                    Int32.TryParse(worksheet.Cells[row, 2].Value.ToString().Replace("CST", ""), out LangDetailId);

                var langDetailModel = translatorRepository.GetLangDetail(LangDetailId);

                if (langDetailModel != null)
                {
                    langdetail.Id = LangDetailId;
                    langdetail.CustomerRefId = langDetailModel.CustomerRefId;
                    langdetail.LangRefColTableId = langDetailModel.LangRefColTableId;
                    langdetail.CreatedBy = userId;
                    langdetail.UpdatedBy = userId;

                    for (int col = 2; col <= ColCount; col++)
                    {
                        if (LanguagesEnum.English.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.English = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Arabic.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Arabic = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Bulgarian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Bulgarian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Croatian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Croatian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Czech.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Czech = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Danish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Danish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Dutch.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Dutch = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Estonian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Estonian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Finnish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Finnish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.French.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.French = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.German.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.German = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Greek.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Greek = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Hungarian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Hungarian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Italian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Italian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Latvian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Latvian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Lithuanian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Lithuanian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Macedonian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Macedonian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Norwegian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Norwegian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Polish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Polish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Portuguese.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Portuguese = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Romanian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Romanian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Russian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Russian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Slovak.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Slovak = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Slovene.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Slovene = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Spanish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Spanish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Swedish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Swedish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Turkish.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Turkish = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Ukrainian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Ukrainian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                        else if (LanguagesEnum.Belarusian.ToString() == (worksheet.Cells[1, col].Value == null ? "" : worksheet.Cells[1, col].Value.ToString().Trim()))
                        {
                            langdetail.Belarusian = worksheet.Cells[row, col].Value == null ? "" : worksheet.Cells[row, col].Value.ToString().Trim();
                        }
                    }

                    var result = translatorRepository.ImportCentralTranslations(langdetail);
                    if (result == "Error")
                        returnresult = "Process Failed, Some Translation Changes Not updated, Please Try Again";
                }
                else
                    return "Process Failed, The Translation Record (" + worksheet.Cells[row, 2].Value.ToString() + ") Not Exist";

            }

            return returnresult;
        }
        public string ImportUITranslation(ExcelPackage package, string userId, int isCommonUI)
        {
            var returnresult = "";



            return returnresult;
        }
        string RemoveStringBetween(string str, char begin, char end)
        {
            Regex regex = new Regex(string.Format("\\{0}.*?\\{1}", begin, end));
            return regex.Replace(str, string.Empty);
        }
        public static List<string> SplitMessageContentString(string str, int size)
        {
            List<string> result = new List<string>();
            List<string> wordslist = new List<string>();
            wordslist = str.Split(' ').Where(x => x.Trim().Length > 0).ToList();
            var words = "";
            var finalline = "";
            foreach (var wd in wordslist)
            {
                finalline = "";
                var tempword = words + " " + wd;
                if (tempword.Trim().Length <= size)
                {
                    words += " " + wd;
                    finalline = words;
                }
                else
                {
                    if (wd.Trim().Length > size)
                    {
                        var wholestr = (words + " " + wd);
                        for (int i = 0; i < wholestr.Trim().Length; i += size)
                        {
                            if ((i + size) < wholestr.Trim().Length)
                                result.Add("<p>" + wholestr.Trim().Substring(i, size) + "</p>");
                            else
                            {
                                words = wholestr.Trim().Substring(i);
                                finalline = words;
                            }
                        }
                    }
                    else
                    {
                        result.Add("<p>" + words.Trim() + "</p>");
                        words = " " + wd;
                        finalline = words;
                    }
                }
            }

            if (finalline.Trim().Length > 0)
                result.Add("<p>" + finalline.Trim() + "</p>");

            return result;
        }

        #endregion Translation
    }
}
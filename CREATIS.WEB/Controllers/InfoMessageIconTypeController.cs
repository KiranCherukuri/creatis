﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using CREATIS.Controllers;
using CREATIS.Infrastructure.IRepository;
using Microsoft.AspNetCore.Mvc;

namespace CREATIS.WEB.Controllers
{
    public class InfoMessageIconTypeController : BaseController
    {
        // GET: /<controller>/

        private readonly IInfoMessageIconTypeRepository InfoMessageIconTypeRepository;
        public ConnectionStrings connectionStrings { get; }

        public InfoMessageIconTypeController(IInfoMessageIconTypeRepository InfoMessageIconTypeRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate)
        {
            this.InfoMessageIconTypeRepository = InfoMessageIconTypeRepo;
            this.connectionStrings = connectionTemplate.Value;
        }
        public ActionResult Index()

        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();

                    if (HasPrivilege(userCredentials.permissions).Contains("S") && !IsCustomerLogin(userCredentials.permissions))
                    {
                        ViewBag.username = userCredentials.displayName;
                        return View();
                    }
                    else
                        return View("Unauthorised");

                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");
        }
        public ActionResult SaveInfoMessageIconType(string action, List<InfoMessageIconType> added, List<InfoMessageIconType> changed)
        {
            System.DateTime time = DateTime.Now;
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    string result = "";

                    if (changed != null)
                    {
                        if (changed.Count > 0)
                        {
                            foreach (InfoMessageIconType d in changed)
                            {
                                var Creatis = InfoMessageIconTypeRepository.GetInfoMessageIconType().Where(r => r.InfoMessageIconTypeId != d.InfoMessageIconTypeId && r.InfoMessageIconTypeName == d.InfoMessageIconTypeName).ToList();
                                if (Creatis.Count > 0)
                                    return Json("error");
                                else
                                {
                                    var userCredentials = GetUserCredentials();
                                    d.UpdatedBy = userCredentials.id.ToString();
                                    d.UpdatedOn = DateTime.Now;
                                    result = InfoMessageIconTypeRepository.SaveInfoMessageIconType(d);
                                }
                            }
                        }
                    }
                    if (added != null)
                    {
                        if (added.Count > 0)
                        {
                            foreach (InfoMessageIconType d in added)
                            {
                                var Creatis = InfoMessageIconTypeRepository.GetInfoMessageIconType().Where(r => r.InfoMessageIconTypeId != d.InfoMessageIconTypeId && r.InfoMessageIconTypeName == d.InfoMessageIconTypeName).ToList();
                                if (Creatis.Count > 0)
                                    return Json("error");
                                else
                                {
                                    var userCredentials = GetUserCredentials();
                                    d.CreatedBy = userCredentials.id.ToString();
                                    d.CreatedOn = DateTime.Now;
                                    result = InfoMessageIconTypeRepository.SaveInfoMessageIconType(d);
                                }
                            }
                        }
                    }
                    return Json("success");
                }
                catch (Exception ex)
                {
                    string e = ex.ToString();
                    return Json(e);
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetInfoMessageIconType()
        {
            try
            {
                List<InfoMessageIconType> InfoMessageIconTypeList = new List<InfoMessageIconType>();
                InfoMessageIconTypeList = InfoMessageIconTypeRepository.GetInfoMessageIconType();
                return Json(InfoMessageIconTypeList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
    }
}

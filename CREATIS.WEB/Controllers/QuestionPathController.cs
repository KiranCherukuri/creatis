﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using CREATIS.Infrastructure.Repository;
using CREATIS.Infrastructure.IRepository;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using CREATIS.Web;

namespace CREATIS.Controllers
{
    public class QuestionPathController : BaseController
    {

        public readonly IQuestionPathRepository QuestionLogicRepository;
        public readonly ICopyLayoutRepository _copyLayoutRepository;
        private readonly ICustomerOverviewRepository CustomerLogicRepository;
        public readonly ITranslatorRepository _translatorRepository;
        public readonly ITooltipRepository _tooltipRepository;
        
        public ConnectionStrings connectionStrings { get; }
        public OthersSettings othersSettings { get; }
        public QuestionPathController(ITooltipRepository tooltipRepository, ICustomerOverviewRepository CustomerOverviewLogicRepo, IQuestionPathRepository QuestionLogicRepo, ICopyLayoutRepository copylayoutRepository, ITranslatorRepository translatorRepository, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings)
        {
            this.CustomerLogicRepository = CustomerOverviewLogicRepo;
            this.QuestionLogicRepository = QuestionLogicRepo;
            this._copyLayoutRepository = copylayoutRepository;
            this._translatorRepository = translatorRepository;
            this._tooltipRepository = tooltipRepository;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
        }

        ///<summary>
        ///Question path Page Methods Starts here
        ///</summary>

        #region 
        public ActionResult QuestionPath(int LayoutID)
        {
           
                if (IsServerConnected(connectionStrings))
            {
                var userCredentials = GetUserCredentials();
                if ((HasPrivilege(userCredentials.permissions).Contains("QP")) && IsCustomerLogin(userCredentials.permissions))
                {
                    var customerId = GetLayoutCustomerId(LayoutID);
                    if (GetCustomerLayoutList(userCredentials.company, customerId).Any(x => x == LayoutID))
                    {
                        LayoutStatusPrivilege(LayoutID);
                        RoleWithLayoutStatusPrivilege(LayoutID, userCredentials.permissions);
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        AddRecentActivity(LayoutID, userCredentials.id);
                        return View();
                    }
                    else
                        return View("Unauthorised");
                }
                else if (HasPrivilege(userCredentials.permissions).Contains("QP"))
                {
                    LayoutStatusPrivilege(LayoutID);
                    RoleWithLayoutStatusPrivilege(LayoutID, userCredentials.permissions);
                    ViewBag.username = userCredentials.displayName;
                    ViewBag.SessionLink = othersSettings.SessionLink;
                    AddRecentActivity(LayoutID, userCredentials.id);
                    return View();
                }
                else
                    return View("Unauthorised");
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetToolBox(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetToolbox(layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetToolBoxParentNodes()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetToolBoxParentNodes());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetToolBoxChildNodes()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetToolBoxChildNodes());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetQuestions(int layoutId, int activityId, int transportId, int questionRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.Questions(layoutId, activityId, transportId, questionRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult FlexGetQuestions(int layoutId, int activityId, int transportId, int questionId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.FlexQuestions(layoutId, activityId, transportId, questionId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetSmartQuestions(int layoutId, int activityId, int transportId, int questionId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetSmartQuestions(layoutId, activityId, transportId, questionId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        string RemoveStringBetween(string str, char begin, char end)
        {
            Regex regex = new Regex(string.Format("\\{0}.*?\\{1}", begin, end));
            return regex.Replace(str, string.Empty);
        }
        public ActionResult SaveQuestions(int LayoutID, int transportTypeId, string Question, string propertyName)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();


                    if (propertyName == "Entry")
                    {
                        int AliasNameDetailsId = 0;
                        string ANameDetailsId = "0";
                        boEntryProperties objEntryProperties = JsonConvert.DeserializeObject<boEntryProperties>(WebUtility.UrlDecode(Question));
                        objEntryProperties.Title = objEntryProperties.Title.Replace("ƒ", "+");

                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objEntryProperties.ActivitiesRefId);
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objEntryProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objEntryProperties.Title;
                        objQuestion.ParentId = objEntryProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        if (objEntryProperties.Save == 1)
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = objEntryProperties.LayoutRefId;
                            AND.ActivitiesRefId = objEntryProperties.ActivitiesRefId;
                            AND.AliasName = objEntryProperties.SavetoAlaiasVal;
                            //AND.AliasName = listActivitiesRegistrations + "." + objEntryProperties.Title;
                            AND.PropertyName = propertyName;
                            AND.CreatedBy = userCredentials.id;
                            AND.CreatedOn = time;
                            AND.IsTempData = false;
                            var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                            AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                            ANameDetailsId = aliasNameId.AliasNameDetailsId.ToString();
                        }

                        EntryProperties objEntries = new EntryProperties();
                        objEntries.QuestionRefId = questionsresult.QuestionId;
                        objEntries.LayoutRefId = LayoutID;
                        objEntries.ActivitiesRefId = objEntryProperties.ActivitiesRefId;
                        objEntries.Title = objEntryProperties.Title;
                        objEntries.MaskText = objEntryProperties.MaskText;
                        objEntries.Size = objEntryProperties.Size;
                        objEntries.NotNull = objEntryProperties.NotNull;
                        objEntries.MinimumofCharacter = objEntryProperties.MinimumofCharacter;
                        objEntries.MinimumOfCharterValue = objEntryProperties.MinimumOfCharterValue;
                        objEntries.ExactCharacter = objEntryProperties.ExactCharacter;
                        objEntries.Zeronotallowed = objEntryProperties.Zeronotallowed;
                        objEntries.Regex = objEntryProperties.Regex;
                        objEntries.Predefined = objEntryProperties.Predefined;
                        objEntries.Save = objEntryProperties.Save;
                        //objEntries.Propose = objEntryProperties.Propose;
                        objEntries.ProposeFixedValue = objEntryProperties.ProposeFixedValue;
                        if (objEntryProperties.Propose == 0)
                        {
                            objEntries.Propose = Convert.ToInt32(ANameDetailsId);
                        }
                        else
                        {
                            objEntries.Propose = objEntryProperties.Propose.GetValueOrDefault();
                        }
                        objEntries.Planningfeedback = objEntryProperties.Planningfeedback;
                        objEntries.FeedbackType = objEntryProperties.FeedbackType;
                        objEntries.PlanningfeedbackCode = objEntryProperties.PlanningfeedbackCode;
                        objEntries.EntryType = objEntryProperties.EntryType;
                        objEntries.CreatedBy = userCredentials.id;
                        objEntries.CreatedOn = time;
                        objEntries.Comment = objEntryProperties.Comment;
                        objEntries.DatetimeFormat = objEntryProperties.DatetimeFormat;
                        objEntries.Conditionitem = objEntryProperties.Conditionitem;
                        objEntries.ConCompareMethod = objEntryProperties.ConCompareMethod;
                        objEntries.Convalue = objEntryProperties.Convalue;
                        objEntries.Infocolumn = objEntryProperties.Infocolumn;
                        objEntries.InfoColumnAction = objEntryProperties.InfoColumnAction;
                        objEntries.FileNumber = objEntryProperties.FileNumber;
                        objEntries.AutoGenerate = objEntryProperties.AutoGenerate;
                        objEntries.ComFixedText = objEntryProperties.ComFixedText;


                        if (objEntryProperties.ConItemSavedValued == 0)
                        {
                            objEntries.ConItemSavedValued = AliasNameDetailsId;
                        }
                        else
                        {
                            objEntries.ConItemSavedValued = objEntryProperties.ConItemSavedValued;
                        }
                        if (objEntryProperties.ConvalueSavedValue == 0)
                        {
                            objEntries.ConvalueSavedValue = AliasNameDetailsId;
                        }
                        else
                        {
                            objEntries.ConvalueSavedValue = objEntryProperties.ConvalueSavedValue;
                        }
                        objEntries.PartnerCode = objEntryProperties.PartnerCode;
                        objEntries.SaveValue = objEntryProperties.SaveValue;

                        //if (objEntryProperties.SaveTo == 0)
                        //{
                        //    objEntries.SaveTo = AliasNameDetailsId;
                        //}
                        //else
                        //{
                        //    objEntries.SaveTo = objEntryProperties.SaveTo;
                        //}
                        int intSaveTo;
                        if (objEntryProperties.Saveto == "0" || ((int.TryParse(objEntryProperties.Saveto, out intSaveTo)) == false))
                        {
                            objEntries.SaveTo = AliasNameDetailsId;
                        }
                        else
                        {
                            objEntries.SaveTo = Convert.ToInt32(objEntryProperties.Saveto);
                        }

                        string defaultAliasName = listActivitiesRegistrations + "." + objEntryProperties.Title;
                        if (defaultAliasName == objEntryProperties.SavetoAlaiasVal && objEntryProperties.SavetoAlaiasVal != null && objEntryProperties.SavetoAlaiasVal != string.Empty)
                        {
                            objEntries.IsDefaultAlaisName = true;
                        }
                        else
                        {
                            objEntries.IsDefaultAlaisName = false;
                        }

                        var entries = QuestionLogicRepository.SaveEntryProperties(objEntries);
                        if (objEntryProperties.Save == 1)
                        {
                            QuestionLogicRepository.UpdateAliasPropertyTableId(AliasNameDetailsId, entries.EntryId.GetValueOrDefault());
                        }

                        return Json(questionsresult.QuestionId);
                    }
                    else if (propertyName == "Document Scan")
                    {
                        int AliasNameDetailsId = 0;
                        DocumetScanProperties objDocumentScanProperties = JsonConvert.DeserializeObject<DocumetScanProperties>(WebUtility.UrlDecode(Question));
                        objDocumentScanProperties.Title = objDocumentScanProperties.Title.Replace("ƒ", "+");
                        objDocumentScanProperties.DocumentName = objDocumentScanProperties.DocumentName.Replace("ƒ", "+");

                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objDocumentScanProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objDocumentScanProperties.Title;
                        objQuestion.ParentId = objDocumentScanProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);
                        var questionid = questionsresult.QuestionId;

                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objDocumentScanProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "On Scan";
                        objQuestion.ParentId = questionsresult.QuestionId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "On Scan";
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = true;
                        var questions = QuestionLogicRepository.SaveQuestions(objQuestion);

                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objDocumentScanProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Type Of Document ?";
                        objQuestion.ParentId = questions.QuestionId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Type Of Document";
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        var documentquestions = QuestionLogicRepository.SaveQuestions(objQuestion);

                        DocumentScanTitle objDocumenTitle = new DocumentScanTitle();

                        objDocumenTitle.ActivitiesRefId = objDocumentScanProperties.ActivitiesRefId;
                        objDocumenTitle.CreatedBy = userCredentials.id;
                        objDocumenTitle.CreatedOn = time;
                        objDocumenTitle.LayoutRefId = LayoutID;
                        objDocumenTitle.QuestionRefId = documentquestions.QuestionId;
                        objDocumenTitle.Title = "Type Of Document ?";
                        var documentscantype = QuestionLogicRepository.SaveDocumentScanTitle(objDocumenTitle);

                        DocumetScanProperties objDcoument = new DocumetScanProperties();
                        objDcoument.ActivitiesRefId = objDocumentScanProperties.ActivitiesRefId;
                        objDcoument.DocumentName = objDocumentScanProperties.DocumentName;
                        objDcoument.LayoutRefId = objDocumentScanProperties.LayoutRefId;
                        objDcoument.QuestionRefId = questionid;
                        objDcoument.Title = objDocumentScanProperties.Title;
                        objDcoument.PlanningFeedbackLevel = objDocumentScanProperties.PlanningFeedbackLevel;
                        objDcoument.CreatedBy = userCredentials.id;
                        objDcoument.CreatedOn = time;
                        objDcoument.Comment = objDocumentScanProperties.Comment;

                        objDcoument.Conditionitem = objDocumentScanProperties.Conditionitem;
                        objDcoument.ConCompareMethod = objDocumentScanProperties.ConCompareMethod;
                        objDcoument.Convalue = objDocumentScanProperties.Convalue;
                        objDcoument.ComFixedText = objDocumentScanProperties.ComFixedText;


                        if (objDocumentScanProperties.ConItemSavedValued == 0)
                        {
                            objDcoument.ConItemSavedValued = AliasNameDetailsId;
                        }
                        else
                        {
                            objDcoument.ConItemSavedValued = objDocumentScanProperties.ConItemSavedValued;
                        }
                        if (objDocumentScanProperties.ConvalueSavedValue == 0)
                        {
                            objDcoument.ConvalueSavedValue = AliasNameDetailsId;
                        }
                        else
                        {
                            objDcoument.ConvalueSavedValue = objDocumentScanProperties.ConvalueSavedValue;
                        }
                        QuestionLogicRepository.SaveDocumetScanProperties(objDcoument);

                        return Json(questionid + "," + documentscantype.DocumetScanTitleId + "," + documentquestions.QuestionId);


                    }
                    else if (propertyName == "Variable List")
                    {

                        int AliasNameDetailsId = 0;
                        int AliasNameDetailsIds = 0;
                        int savedValueAliasNameDetailId = 0;
                        VarListProperties objVariableListProperties = JsonConvert.DeserializeObject<VarListProperties>(WebUtility.UrlDecode(Question));

                        objVariableListProperties.Title = objVariableListProperties.Title.Replace("ƒ", "+");

                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objVariableListProperties.ActivitiesRefId);
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objVariableListProperties.Title + " [" + objVariableListProperties.VarListId + "]";
                        objQuestion.ParentId = objVariableListProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        objQuestion = new Questions();
                        var questionid = questionsresult.QuestionId;
                        objQuestion.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Select";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Select";
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = true;
                        QuestionLogicRepository.SaveQuestions(objQuestion);

                        if (objVariableListProperties.SaveDisplay)
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = LayoutID;
                            AND.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                            AND.AliasName = objVariableListProperties.SaveDisplayAlaiasVal;
                            AND.PropertyName = propertyName;
                            AND.CreatedBy = userCredentials.id;
                            AND.CreatedOn = time;
                            AND.IsTempData = false;
                            var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                            AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                        }

                        if (objVariableListProperties.Other == "Driver entry" || objVariableListProperties.RegisterAsTrailer == "Yes, Driver Selection")
                        {
                            objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                            objQuestion.CreatedBy = userCredentials.id;
                            objQuestion.CreatedOn = time;
                            objQuestion.LayoutRefId = LayoutID;
                            objQuestion.Name = "Other";
                            objQuestion.ParentId = questionid;
                            objQuestion.TransporttypeRefId = transportTypeId;
                            objQuestion.PropertyName = "Other";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = false;
                            objQuestion.IsDefault = true;
                            objQuestion.IsChild = true;
                            var questionsinfo = QuestionLogicRepository.SaveQuestions(objQuestion);

                            var otherquestionid = questionsinfo.QuestionId;

                            if (objVariableListProperties.RegisterAsTrailer == "Yes, Driver Selection")
                            {
                                objQuestion = new Questions();
                                objQuestion.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                                objQuestion.CreatedBy = userCredentials.id;
                                objQuestion.CreatedOn = time;
                                objQuestion.LayoutRefId = LayoutID;
                                objQuestion.Name = objVariableListProperties.OtherTitle;
                                objQuestion.ParentId = otherquestionid;
                                objQuestion.TransporttypeRefId = transportTypeId;
                                objQuestion.PropertyName = "Trailer From Variable List";
                                objQuestion.ISModification = 0;
                                objQuestion.IsQuestionPath = true;
                                objQuestion.IsParent = true;
                                objQuestion.IsDefault = false;
                                objQuestion.IsChild = false;
                                var trailerQueations = QuestionLogicRepository.SaveQuestions(objQuestion);

                                TrailerProperties objTrailer = new TrailerProperties();
                                objTrailer.ActionType = "Connect";

                                objTrailer.QuestionRefId = trailerQueations.QuestionId;
                                objTrailer.LayoutRefId = LayoutID;
                                objTrailer.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                                objTrailer.TrailerFormat = "Alphanumeric";

                                objTrailer.Title = objVariableListProperties.OtherTitle.Replace("ƒ", "+");
                                objTrailer.MaskText = objVariableListProperties.OtherMaskText;
                                objTrailer.Size = 100;
                                objTrailer.NullNotAllowed = false;
                                objTrailer.MinimumofCharacter = false;
                                objTrailer.ExactOfCharacter = false;
                                objTrailer.ZeroNotAllowed = false;
                                objTrailer.Save = true;

                                objTrailer.PlanningFeedback = "Not Set";
                                objTrailer.FeedbackType = "";
                                objTrailer.PlanningFeedbackCode = "";
                                objTrailer.AutoGenerate = false;
                                objTrailer.PartnerCode = "";

                                int intSaveTo;
                                if (objVariableListProperties.SavedDisplayValues == "0" || (int.TryParse(objVariableListProperties.SavedDisplayValues, out intSaveTo)))
                                    objTrailer.SaveTo = AliasNameDetailsId;
                                else
                                    objTrailer.SaveTo = Convert.ToInt32(objVariableListProperties.SaveDisplayTo);


                                objTrailer.Name = "Trailer Connect";
                                objTrailer.Comment = "";
                                objTrailer.CreatedBy = userCredentials.id;
                                objTrailer.CreatedOn = time;

                                QuestionLogicRepository.SaveTrailerProperties(objTrailer);
                            }
                            else if (objVariableListProperties.Other == "Driver entry")
                            {

                                objQuestion = new Questions();
                                objQuestion.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                                objQuestion.CreatedBy = userCredentials.id;
                                objQuestion.CreatedOn = time;
                                objQuestion.LayoutRefId = LayoutID;
                                objQuestion.Name = objVariableListProperties.OtherTitle;
                                objQuestion.ParentId = otherquestionid;
                                objQuestion.TransporttypeRefId = transportTypeId;
                                objQuestion.PropertyName = "Entry From Variable List";
                                objQuestion.ISModification = 0;
                                objQuestion.IsQuestionPath = true;
                                objQuestion.IsParent = true;
                                objQuestion.IsDefault = false;
                                objQuestion.IsChild = false;
                                var entryplanningSelect = QuestionLogicRepository.SaveQuestions(objQuestion);

                                int ANameDetailsId = 0;
                                AliasNameDetails AND = new AliasNameDetails();
                                AND.LayoutRefId = LayoutID;
                                AND.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                                AND.PropertyName = "Entry From Variable List";
                                AND.CreatedBy = userCredentials.id;
                                AND.CreatedOn = time;
                                AND.IsTempData = false;
                                AND.AliasName = listActivitiesRegistrations + "." + objVariableListProperties.Title + ".DriverCreated";
                                var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                                ANameDetailsId = aliasNameId.AliasNameDetailsId;


                                EntryProperties objEntries = new EntryProperties();
                                objEntries.QuestionRefId = entryplanningSelect.QuestionId;
                                objEntries.LayoutRefId = LayoutID;
                                objEntries.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                                objEntries.Title = objVariableListProperties.OtherTitle;
                                objEntries.MaskText = objVariableListProperties.OtherMaskText;
                                objEntries.Size = "100";
                                objEntries.NotNull = 1;
                                objEntries.Save = 1;
                                objEntries.Propose = 0;
                                objEntries.EntryType = "Alphanumeric";
                                objEntries.CreatedBy = userCredentials.id;
                                objEntries.CreatedOn = time;
                                objEntries.FileNumber = "Not Set";
                                objEntries.AutoGenerate = 0;
                                objEntries.Conditionitem = 1;
                                objEntries.SaveTo = ANameDetailsId;

                                var entries = QuestionLogicRepository.SaveEntryProperties(objEntries);
                                QuestionLogicRepository.UpdateAliasPropertyTableId(ANameDetailsId, entries.EntryId.GetValueOrDefault());
                            }
                        }
                        else
                        {
                            objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                            objQuestion.CreatedBy = userCredentials.id;
                            objQuestion.CreatedOn = time;
                            objQuestion.LayoutRefId = LayoutID;
                            objQuestion.Name = "Other";
                            objQuestion.ParentId = questionid;
                            objQuestion.TransporttypeRefId = transportTypeId;
                            objQuestion.PropertyName = "Other";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = false;
                            objQuestion.IsDefault = true;
                            objQuestion.IsChild = true;
                            QuestionLogicRepository.SaveQuestions(objQuestion);
                        }

                        VarListProperties varListObj = new VarListProperties();

                        varListObj.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                        varListObj.QuestionRefId = questionsresult.QuestionId;
                        varListObj.LayoutRefId = LayoutID;
                        varListObj.Name = objVariableListProperties.Title;
                        varListObj.Title = objVariableListProperties.Title;
                        varListObj.VarListId = objVariableListProperties.VarListId;
                        varListObj.Other = objVariableListProperties.Other;
                        varListObj.OtherTitle = objVariableListProperties.OtherTitle;
                        varListObj.OtherMaskText = objVariableListProperties.OtherMaskText;
                        varListObj.SaveDisplay = objVariableListProperties.SaveDisplay;

                        int SaveDisplayTo;
                        if (objVariableListProperties.SavedDisplayValues == "0" || (!int.TryParse(objVariableListProperties.SavedDisplayValues, out SaveDisplayTo)))
                            varListObj.SaveDisplayTo = AliasNameDetailsId;
                        else
                            varListObj.SaveDisplayTo = Convert.ToInt32(objVariableListProperties.SavedDisplayValues);

                        varListObj.Comment = objVariableListProperties.Comment;
                        varListObj.ConditionItemValue = objVariableListProperties.ConditionItemValue;
                        varListObj.ConditionSavedValue = objVariableListProperties.ConditionSavedValue;
                        varListObj.CompareMethod = objVariableListProperties.CompareMethod;
                        varListObj.CompareTo = objVariableListProperties.CompareTo;
                        varListObj.CompareToSavedValue = objVariableListProperties.CompareToSavedValue;
                        varListObj.CompareToFixedText = objVariableListProperties.CompareToFixedText;
                        varListObj.PlanningFeedback = objVariableListProperties.PlanningFeedback;
                        varListObj.FeedbackType = objVariableListProperties.FeedbackType;
                        varListObj.PlanningFeedbackCode = objVariableListProperties.PlanningFeedbackCode;
                        varListObj.Infocolumn = objVariableListProperties.Infocolumn;
                        varListObj.InfoColumnAction = objVariableListProperties.InfoColumnAction;
                        varListObj.FileNumber = objVariableListProperties.FileNumber;
                        varListObj.PartnerCode = objVariableListProperties.PartnerCode;
                        varListObj.RegisterAsTrailer = objVariableListProperties.RegisterAsTrailer;
                        varListObj.CreatedBy = userCredentials.id;
                        varListObj.CreatedOn = time;
                        varListObj.UpdatedBy = userCredentials.id;
                        varListObj.UpdatedOn = time;

                        var varList = QuestionLogicRepository.SaveVarListProperties(varListObj);
                        QuestionLogicRepository.SaveVarListCommentReadoutDetail(varList.VarListPropertiesId ?? 0, objVariableListProperties.CommentTokenId, userCredentials.id, time);
                        QuestionLogicRepository.SaveVarListFilterDetail(varList.VarListPropertiesId ?? 0, objVariableListProperties.FilterTokenId, userCredentials.id, time);

                        return Json(questionid);
                    }
                    else if (propertyName == "Text Message")
                    {
                        int AliasNameDetailsId = 0;
                        TextMessageProperties objTextMessageProperties = JsonConvert.DeserializeObject<TextMessageProperties>(WebUtility.UrlDecode(Question));
                        objTextMessageProperties.Messagecontent = objTextMessageProperties.Messagecontent.Replace("ƒ", "+");
                        int countofText = QuestionLogicRepository.GetCountofTextMessage(objTextMessageProperties.ActivitiesRefId) + 1;
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objTextMessageProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objTextMessageProperties.Name + " # " + countofText;
                        objQuestion.ParentId = objTextMessageProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        TextMessageProperties objTextMessage = new TextMessageProperties();
                        objTextMessage.ActivitiesRefId = objTextMessageProperties.ActivitiesRefId;
                        objTextMessage.Name = objTextMessageProperties.Name + " # " + countofText;
                        objTextMessage.LayoutRefId = objTextMessageProperties.LayoutRefId;
                        objTextMessage.QuestionRefId = questionsresult.QuestionId;
                        objTextMessage.SendMethod = objTextMessageProperties.SendMethod;
                        objTextMessage.Messagecontent = objTextMessageProperties.Messagecontent;
                        objTextMessage.CreatedBy = userCredentials.id;
                        objTextMessage.CreatedOn = time;
                        objTextMessage.Comment = objTextMessageProperties.Comment;

                        objTextMessage.Conditionitem = objTextMessageProperties.Conditionitem;
                        objTextMessage.ConCompareMethod = objTextMessageProperties.ConCompareMethod;
                        objTextMessage.Convalue = objTextMessageProperties.Convalue;
                        objTextMessage.ComFixedText = objTextMessageProperties.ComFixedText;


                        if (objTextMessageProperties.ConItemSavedValued == 0)
                        {
                            objTextMessage.ConItemSavedValued = AliasNameDetailsId;
                        }
                        else
                        {
                            objTextMessage.ConItemSavedValued = objTextMessageProperties.ConItemSavedValued;
                        }
                        if (objTextMessageProperties.ConvalueSavedValue == 0)
                        {
                            objTextMessage.ConvalueSavedValue = AliasNameDetailsId;
                        }
                        else
                        {
                            objTextMessage.ConvalueSavedValue = objTextMessageProperties.ConvalueSavedValue;
                        }

                        QuestionLogicRepository.SaveTextMessageProperties(objTextMessage);
                        return Json(questionsresult.QuestionId);
                    }
                    else if (propertyName == "Info Message")
                    {

                        int AliasNameDetailsId = 0;
                        InfomessageProperties objInfomessageProperties = JsonConvert.DeserializeObject<InfomessageProperties>(WebUtility.UrlDecode(Question));
                        objInfomessageProperties.Title = objInfomessageProperties.Title.Replace("ƒ", "+").Replace("&nbsp;", " ").Replace("<br>", " ");
                        objInfomessageProperties.Messagecontent = objInfomessageProperties.Messagecontent.Replace("ƒ", "+").Replace("&nbsp;", " ").Replace("<br>", " ");

                        bool isValid = true;

                        if (objInfomessageProperties.Messagecontent.Trim().Length > 0)
                        {

                            var Messagecontent = objInfomessageProperties.Messagecontent.Replace("<p>", "ƒ<p>");
                            var MessagecontentLines = Messagecontent.Split("ƒ").Where(x => x.Trim().Length > 0).ToList();
                            int LineNo = 0;
                            foreach (var messagecontent in MessagecontentLines)
                            {
                                string resultmessagecontent = _translatorRepository.StripHTML(messagecontent);
                                if (Regex.Matches(resultmessagecontent, "}").Count() > 1)
                                {
                                    var messagecontentsplit = resultmessagecontent.Split("}");
                                    resultmessagecontent = "";
                                    foreach (var mc in messagecontentsplit)
                                    {
                                        resultmessagecontent += RemoveStringBetween(mc, '{', '}');
                                    }
                                }
                                else
                                {
                                    resultmessagecontent = RemoveStringBetween(resultmessagecontent, '{', '}');
                                }

                                if (resultmessagecontent.Trim().Length > 35)
                                {
                                    isValid = false;
                                    return Json(new
                                    {
                                        error = "Message Content Length Greater Than 35 (" + _translatorRepository.StripHTML(messagecontent) + ")"
                                    });
                                }

                                if (resultmessagecontent.Trim().Length > 0)
                                    LineNo++;
                            }

                            if (LineNo > 12)
                                return Json(new
                                {
                                    error = "Message Content Should be 12 Lines"
                                });
                        }
                        if (_translatorRepository.StripHTML(objInfomessageProperties.Title).Trim().Length > 0)
                        {

                            string resulttitle = _translatorRepository.StripHTML(objInfomessageProperties.Title);

                            if (Regex.Matches(resulttitle, "}").Count() > 1)
                            {
                                var titlesplit = resulttitle.Split("}");
                                resulttitle = "";
                                foreach (var mc in titlesplit)
                                {
                                    resulttitle += RemoveStringBetween(mc, '{', '}');
                                }
                            }
                            else
                            {
                                resulttitle = RemoveStringBetween(resulttitle, '{', '}');
                            }

                            if (objInfomessageProperties.Messagetype == "Line")
                            {
                                if (resulttitle.Trim().Length > 50)
                                {
                                    isValid = false;
                                    return Json(new
                                    {
                                        error = "Title Length Greater Than 50 (" + _translatorRepository.StripHTML(objInfomessageProperties.Title) + ")"
                                    });
                                }
                            }
                            else
                            {
                                if (resulttitle.Trim().Length > 40)
                                {
                                    isValid = false;
                                    return Json(new
                                    {
                                        error = "Title Length Greater Than 40 (" + _translatorRepository.StripHTML(objInfomessageProperties.Title) + ")"
                                    });
                                }
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                error = "Enter Valid Info Message Title"
                            });
                        }

                        if (isValid)
                        {
                            int countofinfo = QuestionLogicRepository.GetCountofInfoMessage(objInfomessageProperties.ActivitiesRefId) + 1;
                            Questions objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = objInfomessageProperties.ActivitiesRefId;
                            objQuestion.CreatedBy = userCredentials.id;
                            objQuestion.CreatedOn = time;
                            objQuestion.LayoutRefId = LayoutID;
                            objQuestion.Name = objInfomessageProperties.Name + countofinfo;
                            objQuestion.ParentId = objInfomessageProperties.QuestionRefId;
                            objQuestion.TransporttypeRefId = transportTypeId;
                            objQuestion.PropertyName = propertyName;
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = true;
                            objQuestion.IsDefault = false;
                            objQuestion.IsChild = false;
                            var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                            InfomessageProperties objInfomessage = new InfomessageProperties();
                            objInfomessage.ActivitiesRefId = objInfomessageProperties.ActivitiesRefId;
                            objInfomessage.Name = objInfomessageProperties.Name + countofinfo;
                            objInfomessage.LayoutRefId = objInfomessageProperties.LayoutRefId;
                            objInfomessage.QuestionRefId = questionsresult.QuestionId;
                            objInfomessage.Messagetype = objInfomessageProperties.Messagetype;
                            var title = objInfomessageProperties.Title;
                            objInfomessage.Title = "<p>" + title.Replace("<p>", "").Replace("</p>", "").Replace("<div>", "").Replace("</div>", "").Replace("<span>", "").Replace("</span>", "").Replace("<br>", "").Replace("&nbsp;", " ") + "</p>";
                            objInfomessage.Titlestyle = objInfomessageProperties.Titlestyle;
                            objInfomessage.InfoMessageIconTypeRefId = objInfomessageProperties.InfoMessageIconTypeRefId;
                            objInfomessage.Fontstyle = objInfomessageProperties.Fontstyle;
                            objInfomessage.Messagecontent = objInfomessageProperties.Messagecontent;
                            objInfomessage.CreatedBy = userCredentials.id;
                            objInfomessage.CreatedOn = time;
                            objInfomessage.Comment = objInfomessageProperties.Comment;

                            objInfomessage.Conditionitem = objInfomessageProperties.Conditionitem;
                            objInfomessage.ConCompareMethod = objInfomessageProperties.ConCompareMethod;
                            objInfomessage.Convalue = objInfomessageProperties.Convalue;
                            objInfomessage.ComFixedText = objInfomessageProperties.ComFixedText;


                            if (objInfomessageProperties.ConItemSavedValued == 0)
                            {
                                objInfomessage.ConItemSavedValued = AliasNameDetailsId;
                            }
                            else
                            {
                                objInfomessage.ConItemSavedValued = objInfomessageProperties.ConItemSavedValued;
                            }
                            if (objInfomessageProperties.ConvalueSavedValue == 0)
                            {
                                objInfomessage.ConvalueSavedValue = AliasNameDetailsId;
                            }
                            else
                            {
                                objInfomessage.ConvalueSavedValue = objInfomessageProperties.ConvalueSavedValue;
                            }

                            QuestionLogicRepository.SaveInfomessageProperties(objInfomessage);
                            return Json(questionsresult.QuestionId);
                        }
                        else
                        {
                            return Json(new
                            {
                                error = GetStatusMessage(StatusCode.E02)
                            });
                        }

                    }
                    else if (propertyName == "Check Off")
                    {
                        CheckoffProperties objCheckoffProperties = JsonConvert.DeserializeObject<CheckoffProperties>(WebUtility.UrlDecode(Question));
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objCheckoffProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objCheckoffProperties.Name;
                        objQuestion.ParentId = objCheckoffProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        CheckoffProperties objCheckoff = new CheckoffProperties();
                        objCheckoff.ActivitiesRefId = objCheckoffProperties.ActivitiesRefId;
                        objCheckoff.Name = objCheckoffProperties.Name;
                        objCheckoff.LayoutRefId = objCheckoffProperties.LayoutRefId;
                        objCheckoff.QuestionRefId = questionsresult.QuestionId;
                        objCheckoff.Planninglevel = objCheckoffProperties.Planninglevel;
                        objCheckoff.CreatedBy = userCredentials.id;
                        objCheckoff.CreatedOn = time;
                        objCheckoff.Comment = objCheckoffProperties.Comment;
                        QuestionLogicRepository.SaveCheckoffProperties(objCheckoff);
                        return Json(questionsresult.QuestionId);
                    }
                    else if (propertyName == "Planning Select")
                    {
                        PlanningselectProperties objPlanningselectProperties = JsonConvert.DeserializeObject<PlanningselectProperties>(WebUtility.UrlDecode(Question));
                        objPlanningselectProperties.Title = objPlanningselectProperties.Title.Replace("ƒ", "+");
                        objPlanningselectProperties.OtherTitle = objPlanningselectProperties.OtherTitle.Replace("ƒ", "+");

                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objPlanningselectProperties.ActivitiesRefId);
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objPlanningselectProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objPlanningselectProperties.Title + " [ " + objPlanningselectProperties.Planninglevel + " ]";
                        objQuestion.ParentId = objPlanningselectProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        objQuestion = new Questions();
                        var questionid = questionsresult.QuestionId;
                        objQuestion.ActivitiesRefId = objPlanningselectProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Select";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Select";
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = true;
                        QuestionLogicRepository.SaveQuestions(objQuestion);

                        if (objPlanningselectProperties.Other == "Driver entry (A50)" || objPlanningselectProperties.Other == "No other Driver entry (A50)")
                        {
                            objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = objPlanningselectProperties.ActivitiesRefId;
                            objQuestion.CreatedBy = userCredentials.id;
                            objQuestion.CreatedOn = time;
                            objQuestion.LayoutRefId = LayoutID;
                            objQuestion.Name = "Other";
                            objQuestion.ParentId = questionid;
                            objQuestion.TransporttypeRefId = transportTypeId;
                            objQuestion.PropertyName = "Other";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = false;
                            objQuestion.IsDefault = true;
                            objQuestion.IsChild = true;
                            var questionsinfo = QuestionLogicRepository.SaveQuestions(objQuestion);

                            objQuestion = new Questions();
                            var otherquestionid = questionsinfo.QuestionId;

                            objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = objPlanningselectProperties.ActivitiesRefId;
                            objQuestion.CreatedBy = userCredentials.id;
                            objQuestion.CreatedOn = time;
                            objQuestion.LayoutRefId = LayoutID;
                            objQuestion.Name = objPlanningselectProperties.OtherTitle;
                            objQuestion.ParentId = otherquestionid;
                            objQuestion.TransporttypeRefId = transportTypeId;
                            objQuestion.PropertyName = "Planning Select Other Title";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = true;
                            objQuestion.IsDefault = false;
                            objQuestion.IsChild = false;
                            var entryplanningSelect = QuestionLogicRepository.SaveQuestions(objQuestion);

                            //Kiran Starts
                            //Planning Select Entry
                            int ANameDetailsId = 0;
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = LayoutID;
                            AND.ActivitiesRefId = objPlanningselectProperties.ActivitiesRefId;
                            AND.AliasName = "DriverCreated";
                            AND.PropertyName = "Planning Select Other Title";
                            AND.CreatedBy = userCredentials.id;
                            AND.CreatedOn = time;
                            AND.IsTempData = false;
                            var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                            ANameDetailsId = aliasNameId.AliasNameDetailsId;

                            EntryProperties objEntries = new EntryProperties();
                            objEntries.QuestionRefId = entryplanningSelect.QuestionId;
                            objEntries.LayoutRefId = LayoutID;
                            objEntries.ActivitiesRefId = objPlanningselectProperties.ActivitiesRefId;
                            objEntries.Title = objPlanningselectProperties.OtherTitle;
                            objEntries.MaskText = objPlanningselectProperties.OtherMaskText;
                            objEntries.Size = "50";
                            objEntries.NotNull = 1;
                            objEntries.Save = 1;
                            objEntries.Propose = 0;
                            objEntries.EntryType = "Alphanumeric";
                            objEntries.CreatedBy = userCredentials.id;
                            objEntries.CreatedOn = time;
                            objEntries.FileNumber = "Not Set";
                            objEntries.AutoGenerate = 0;
                            objEntries.Conditionitem = 1;
                            objEntries.SaveTo = ANameDetailsId;

                            var entries = QuestionLogicRepository.SaveEntryProperties(objEntries);
                            QuestionLogicRepository.UpdateAliasPropertyTableId(ANameDetailsId, entries.EntryId.GetValueOrDefault());

                            //kiran Ends
                        }
                        else
                        {
                            objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = objPlanningselectProperties.ActivitiesRefId;
                            objQuestion.CreatedBy = userCredentials.id;
                            objQuestion.CreatedOn = time;
                            objQuestion.LayoutRefId = LayoutID;
                            objQuestion.Name = "Other";
                            objQuestion.ParentId = questionid;
                            objQuestion.TransporttypeRefId = transportTypeId;
                            objQuestion.PropertyName = "Other";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = false;
                            objQuestion.IsDefault = true;
                            objQuestion.IsChild = true;
                            QuestionLogicRepository.SaveQuestions(objQuestion);
                        }
                        int AliasNameDetailsId = 0;
                        if (objPlanningselectProperties.Other == "QP (save to DriverCreated)" || objPlanningselectProperties.Other == "No other QP (save to DriverCreated)")
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = objPlanningselectProperties.LayoutRefId;
                            AND.ActivitiesRefId = objPlanningselectProperties.ActivitiesRefId;
                            AND.AliasName = listActivitiesRegistrations + "." + objPlanningselectProperties.Planninglevel + ".DriverCreated";
                            AND.PropertyName = propertyName;
                            AND.CreatedBy = userCredentials.id;
                            AND.CreatedOn = time;
                            AND.IsTempData = false;
                            var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                            AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                        }

                        PlanningselectProperties objPlanningselect = new PlanningselectProperties();
                        objPlanningselect.ActivitiesRefId = objPlanningselectProperties.ActivitiesRefId;
                        objPlanningselect.Title = objPlanningselectProperties.Title;
                        objPlanningselect.LayoutRefId = objPlanningselectProperties.LayoutRefId;
                        objPlanningselect.QuestionRefId = questionid;
                        objPlanningselect.Planninglevel = objPlanningselectProperties.Planninglevel;
                        objPlanningselect.Planningspecific = objPlanningselectProperties.Planningspecific;
                        objPlanningselect.Chooseactivity = objPlanningselectProperties.Chooseactivity;
                        objPlanningselect.Startselecteditem = objPlanningselectProperties.Startselecteditem;
                        objPlanningselect.PlanningSelectionMethod = objPlanningselectProperties.PlanningSelectionMethod;
                        objPlanningselect.Other = objPlanningselectProperties.Other;
                        objPlanningselect.OtherTitle = objPlanningselectProperties.OtherTitle;
                        //objPlanningselect.Fixedvalue = objPlanningselectProperties.Fixedvalue;
                        //objPlanningselect.QP = objPlanningselectProperties.QP;
                        objPlanningselect.Planningreadout = objPlanningselectProperties.Planningreadout;
                        objPlanningselect.Comment = objPlanningselectProperties.Comment;

                        objPlanningselect.CheckOffPlanning = objPlanningselectProperties.CheckOffPlanning;
                        objPlanningselect.FilterRefId = objPlanningselectProperties.FilterRefId;
                        objPlanningselect.CompareMethodRefId = objPlanningselectProperties.CompareMethodRefId;
                        objPlanningselect.PSComparetoRefId = objPlanningselectProperties.PSComparetoRefId;
                        objPlanningselect.InstructionSetSavedValues = objPlanningselectProperties.InstructionSetSavedValues;

                        objPlanningselect.OtherMaskText = objPlanningselectProperties.OtherMaskText;
                        objPlanningselect.StartSelection = objPlanningselectProperties.StartSelection;
                        objPlanningselect.CreatedBy = userCredentials.id;
                        objPlanningselect.CreatedOn = time;
                        var planningSelect = QuestionLogicRepository.SavePlanningselectProperties(objPlanningselect);

                        if (objPlanningselectProperties.Other == "QP (save to DriverCreated)" || objPlanningselectProperties.Other == "No other QP (save to DriverCreated)")
                        {
                            QuestionLogicRepository.UpdateAliasPropertyTableId(AliasNameDetailsId, planningSelect.PlanningselectId.GetValueOrDefault());
                        }

                        QuestionLogicRepository.SavePlanningSelectReadoutDetail(objPlanningselect.PlanningselectId ?? 0, objPlanningselectProperties.TokenId, userCredentials.id, time);


                        return Json(questionid);
                    }
                    else if (propertyName == "Set Action / Value")
                    {
                        int AliasNameDetailsId = 0;
                        BOSetActionValueProperties objSetActionValueProperties = JsonConvert.DeserializeObject<BOSetActionValueProperties>(WebUtility.UrlDecode(Question));
                        objSetActionValueProperties.Title = objSetActionValueProperties.Title.Replace("ƒ", "+");
                        objSetActionValueProperties.Value = objSetActionValueProperties.Value.Replace("ƒ", "+");
                        objSetActionValueProperties.InfoCoumnFixedText = objSetActionValueProperties.InfoCoumnFixedText.Replace("ƒ", "+");

                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objSetActionValueProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objSetActionValueProperties.Title;
                        objQuestion.ParentId = objSetActionValueProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        if (objSetActionValueProperties.TargetAlaiasVal != "" && objSetActionValueProperties.TargetAlaiasVal != null)
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = objSetActionValueProperties.LayoutRefId;
                            AND.ActivitiesRefId = objSetActionValueProperties.ActivitiesRefId;
                            //AND.AliasName = listActivitiesRegistrations + "." + objChioceProperties.Title;
                            AND.AliasName = objSetActionValueProperties.TargetAlaiasVal;
                            AND.PropertyName = propertyName;
                            //AND.PropertyTableId
                            AND.CreatedBy = userCredentials.id;
                            AND.CreatedOn = time;
                            AND.IsTempData = false;
                            var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                            AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                            //ANameDetailsId = aliasNameId.AliasNameDetailsId.ToString();
                        }

                        SetActionValueProperties objSetActionValue = new SetActionValueProperties();
                        objSetActionValue.ActivitiesRefId = objSetActionValueProperties.ActivitiesRefId;
                        objSetActionValue.Title = objSetActionValueProperties.Title;
                        objSetActionValue.LayoutRefId = objSetActionValueProperties.LayoutRefId;
                        objSetActionValue.QuestionRefId = questionsresult.QuestionId;
                        objSetActionValue.SetActionValueTypeId = objSetActionValueProperties.SetActionValueTypeId;
                        objSetActionValue.SavedValue = objSetActionValueProperties.SavedValue;
                        if (objSetActionValue.SetActionValueTypeId == (int)EnumSetActionValueType.NextQP)
                        {
                            objSetActionValue.SelectQP = objSetActionValueProperties.SelectQP;
                            objSetActionValue.CancelcurrentQP = objSetActionValueProperties.CancelcurrentQP;
                        }
                        else if (objSetActionValue.SetActionValueTypeId == (int)EnumSetActionValueType.NextView)
                        {
                            objSetActionValue.SelectView = objSetActionValueProperties.SelectView;
                            objSetActionValue.PlanningIDOBC = objSetActionValueProperties.PlanningIDOBC > 0 ? objSetActionValueProperties.PlanningIDOBC : null;
                            objSetActionValue.Longitude = objSetActionValueProperties.Longitude > 0 ? objSetActionValueProperties.Longitude : null;
                            objSetActionValue.Latitude = objSetActionValueProperties.Latitude > 0 ? objSetActionValueProperties.Latitude : null;
                        }
                        else if (objSetActionValue.SetActionValueTypeId == (int)EnumSetActionValueType.Allowance)
                        {
                            objSetActionValue.SelectAllowance = objSetActionValueProperties.SelectAllowance;

                            // Code added by divakar
                            var resultAllowance = QuestionLogicRepository.Insertallowances(LayoutID, objSetActionValue.SelectAllowance, userCredentials.id.ToString());

                        }
                        else if (objSetActionValue.SetActionValueTypeId == (int)EnumSetActionValueType.InfoColumn)
                        {
                            objSetActionValue.SelectInfoColumn = objSetActionValueProperties.SelectInfoColumn;
                            objSetActionValue.SetReset = objSetActionValueProperties.SetReset;
                            objSetActionValue.InfoCoumnFixedText = objSetActionValueProperties.InfoCoumnFixedText;
                            objSetActionValue.InfoColumnSavedValue = objSetActionValueProperties.InfoColumnSavedValue;
                        }
                        else if (objSetActionValue.SetActionValueTypeId == (int)EnumSetActionValueType.SavedValue)
                        {
                            objSetActionValue.Value = ((objSetActionValueProperties.Value != null && objSetActionValueProperties.Value.Trim().Length > 0) ? objSetActionValueProperties.Value : null);
                            objSetActionValue.CopySavedValue = objSetActionValueProperties.CopySavedValue > 0 ? objSetActionValueProperties.CopySavedValue : null;


                            int intSaveTo;
                            if (objSetActionValueProperties.SelectTargetSavedValue == "0" || ((int.TryParse(objSetActionValueProperties.SelectTargetSavedValue, out intSaveTo)) == false))
                            {
                                objSetActionValue.SelectTargetSavedValue = AliasNameDetailsId;
                            }
                            else
                            {
                                objSetActionValue.SelectTargetSavedValue = Convert.ToInt32(objSetActionValueProperties.SelectTargetSavedValue);
                            }

                            objSetActionValue.CopyRange = objSetActionValueProperties.CopyRange;
                            objSetActionValue.StartChar = objSetActionValueProperties.StartChar;
                            objSetActionValue.CharLength = objSetActionValueProperties.CharLength;
                        }
                        else if (objSetActionValue.SetActionValueTypeId == (int)EnumSetActionValueType.EmptyFullSoloState)
                        {
                            objSetActionValue.SetState = objSetActionValueProperties.SetState;
                        }

                        objSetActionValue.CreatedBy = userCredentials.id;
                        objSetActionValue.CreatedOn = time;
                        objSetActionValue.Comment = objSetActionValueProperties.Comment;

                        objSetActionValue.Conditionitem = objSetActionValueProperties.Conditionitem;
                        objSetActionValue.ConCompareMethod = objSetActionValueProperties.ConCompareMethod;
                        objSetActionValue.Convalue = objSetActionValueProperties.Convalue;
                        objSetActionValue.ComFixedText = objSetActionValueProperties.ComFixedText;




                        if (objSetActionValueProperties.ConItemSavedValued == 0)
                        {
                            objSetActionValue.ConItemSavedValued = AliasNameDetailsId;
                        }
                        else
                        {
                            objSetActionValue.ConItemSavedValued = objSetActionValueProperties.ConItemSavedValued;
                        }
                        if (objSetActionValueProperties.ConvalueSavedValue == 0)
                        {
                            objSetActionValue.ConvalueSavedValue = AliasNameDetailsId;
                        }
                        else
                        {
                            objSetActionValue.ConvalueSavedValue = objSetActionValueProperties.ConvalueSavedValue;
                        }


                        QuestionLogicRepository.SaveSetActionValueProperties(objSetActionValue);
                        return Json(questionsresult.QuestionId);
                    }
                    else if (propertyName == "Choice")
                    {
                        int AliasNameDetailsId = 0;
                        string ANameDetailsId = "0";
                        BOChioceProperties objChioceProperties = JsonConvert.DeserializeObject<BOChioceProperties>(WebUtility.UrlDecode(Question));
                        objChioceProperties.Title = objChioceProperties.Title.Replace("ƒ", "+");

                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objChioceProperties.ActivitiesRefId);
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objChioceProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objChioceProperties.Title;
                        objQuestion.ParentId = objChioceProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        if (objChioceProperties.Save == 1)
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = objChioceProperties.LayoutRefId;
                            AND.ActivitiesRefId = objChioceProperties.ActivitiesRefId;
                            AND.AliasName = objChioceProperties.SavetoAlaiasVal;
                            AND.PropertyName = propertyName;
                            AND.CreatedBy = userCredentials.id;
                            AND.CreatedOn = time;
                            AND.IsTempData = false;
                            var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                            AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                            ANameDetailsId = aliasNameId.AliasNameDetailsId.ToString();
                        }

                        ChioceProperties objChioce = new ChioceProperties();
                        objChioce.ActivitiesRefId = objChioceProperties.ActivitiesRefId;
                        objChioce.Title = objChioceProperties.Title;
                        objChioce.LayoutRefId = objChioceProperties.LayoutRefId;
                        objChioce.QuestionRefId = questionsresult.QuestionId;
                        objChioce.Recursiveloop = objChioceProperties.Recursiveloop;
                        objChioce.Save = objChioceProperties.Save;
                        objChioce.Comment = objChioceProperties.Comment;
                        if (objChioceProperties.Propose == "0")
                        {
                            objChioce.Propose = ANameDetailsId;
                        }
                        else
                        {
                            objChioce.Propose = objChioceProperties.Propose;
                        }
                        objChioce.Planningfeedback = objChioceProperties.Planningfeedback ?? "";
                        objChioce.FeedbackType = objChioceProperties.FeedbackType ?? "";
                        objChioce.Planningfeedbackcode = objChioceProperties.Planningfeedbackcode;
                        objChioce.ConditionValue = objChioceProperties.ConditionValue;
                        objChioce.ConCompareMethod = objChioceProperties.ConCompareMethod;
                        objChioce.ConCompareTo = objChioceProperties.ConCompareTo;
                        objChioce.Infocolumn = objChioceProperties.Infocolumn;
                        objChioce.InfoColumnAction = objChioceProperties.InfoColumnAction;
                        objChioce.FileNumber = objChioceProperties.FileNumber;
                        objChioce.SaveValue = AliasNameDetailsId;

                        if (objChioceProperties.ConSavedValued == 0)
                        {
                            objChioce.ConSavedValued = AliasNameDetailsId;
                        }
                        else
                        {
                            objChioce.ConSavedValued = objChioceProperties.ConSavedValued;
                        }
                        if (objChioceProperties.comtoSavedValue == 0)
                        {
                            objChioce.comtoSavedValue = AliasNameDetailsId;
                        }
                        else
                        {
                            objChioce.comtoSavedValue = objChioceProperties.comtoSavedValue;
                        }

                        objChioce.ComFixedText = objChioceProperties.ComFixedText;
                        objChioce.AutoGenerate = objChioceProperties.AutoGenerate;

                        if (objChioceProperties.InfoActionQP == 0)
                        {
                            objChioce.InfoActionQP = AliasNameDetailsId;
                        }
                        else
                        {
                            objChioce.InfoActionQP = objChioceProperties.InfoActionQP;
                        }


                        objChioce.CreatedBy = userCredentials.id;
                        objChioce.CreatedOn = time;
                        int intSaveTo;
                        if (objChioceProperties.Saveto == "0" || ((int.TryParse(objChioceProperties.Saveto, out intSaveTo)) == false))
                        {
                            objChioce.Saveto = AliasNameDetailsId;
                        }
                        else
                        {
                            objChioce.Saveto = Convert.ToInt32(objChioceProperties.Saveto);
                        }

                        objChioce.PartnerCode = objChioceProperties.PartnerCode;
                        string defaultAliasName = listActivitiesRegistrations + "." + objChioceProperties.Title;
                        if (defaultAliasName == objChioceProperties.SavetoAlaiasVal && objChioceProperties.SavetoAlaiasVal != null && objChioceProperties.SavetoAlaiasVal != string.Empty)
                        {
                            objChioce.IsDefaultAlaisName = true;
                        }
                        else
                        {
                            objChioce.IsDefaultAlaisName = false;
                        }
                        //objChioce.IsDefaultAlaisName=

                        var choiceResult = QuestionLogicRepository.SaveChioceProperties(objChioce);

                        if (objChioceProperties.Save == 1)
                        {
                            QuestionLogicRepository.UpdateAliasPropertyTableId(AliasNameDetailsId, choiceResult.ChioceId.GetValueOrDefault());
                        }
                        return Json(choiceResult.ChioceId + "," + choiceResult.QuestionRefId);
                    }
                    else if (propertyName == "Conditional Action")
                    {
                        ConditionalProperties objConditionalProperties = JsonConvert.DeserializeObject<ConditionalProperties>(WebUtility.UrlDecode(Question));
                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objConditionalProperties.ActivitiesRefId);
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objConditionalProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objConditionalProperties.Name;
                        objQuestion.ParentId = objConditionalProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);


                        var questionid = questionsresult.QuestionId;
                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objConditionalProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "True";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "True";
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = true;
                        QuestionLogicRepository.SaveQuestions(objQuestion);

                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objConditionalProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "False";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "False";
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = true;
                        QuestionLogicRepository.SaveQuestions(objQuestion);


                        ConditionalProperties objConditionalProp = new ConditionalProperties();
                        objConditionalProp.ActivitiesRefId = objConditionalProperties.ActivitiesRefId;
                        objConditionalProp.Name = objConditionalProperties.Name;
                        objConditionalProp.LayoutRefId = objConditionalProperties.LayoutRefId;
                        objConditionalProp.QuestionRefId = questionid;

                        objConditionalProp.ConditionValue = objConditionalProperties.ConditionValue;
                        objConditionalProp.ConCompareMethod = objConditionalProperties.ConCompareMethod;
                        objConditionalProp.ConCompareTo = objConditionalProperties.ConCompareTo;
                        objConditionalProp.ConSavedValued = objConditionalProperties.ConSavedValued;
                        objConditionalProp.comtoSavedValue = objConditionalProperties.comtoSavedValue;
                        objConditionalProp.ComFixedText = objConditionalProperties.ComFixedText;
                        objConditionalProp.Comment = objConditionalProperties.Comment;
                        objConditionalProp.Repeat = objConditionalProperties.Repeat;

                        objConditionalProp.CreatedBy = userCredentials.id;
                        objConditionalProp.CreatedOn = time;
                        QuestionLogicRepository.SaveConditionalProperties(objConditionalProp);
                        return Json(questionid);



                    }
                    else if (propertyName == "Transport Type")
                    {
                        TransportTypeProperties objTransportTypeProperties = JsonConvert.DeserializeObject<TransportTypeProperties>(WebUtility.UrlDecode(Question));
                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objTransportTypeProperties.ActivitiesRefId);
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objTransportTypeProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objTransportTypeProperties.Title;
                        objQuestion.ParentId = objTransportTypeProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        TransportTypeProperties objTransportType = new TransportTypeProperties();
                        objTransportType.ActivitiesRefId = objTransportTypeProperties.ActivitiesRefId;
                        objTransportType.Title = objTransportTypeProperties.Title;
                        objTransportType.LayoutRefId = objTransportTypeProperties.LayoutRefId;
                        objTransportType.QuestionRefId = questionsresult.QuestionId;
                        objTransportType.Save = objTransportTypeProperties.Save;
                        objTransportType.SaveTo = objTransportTypeProperties.SaveTo;
                        objTransportType.Comment = objTransportTypeProperties.Comment;
                        objTransportType.PlanningFeedback = objTransportTypeProperties.PlanningFeedback ?? "";
                        objTransportType.FeedbackType = objTransportTypeProperties.FeedbackType ?? "";
                        objTransportType.PlanningFeedbackCode = objTransportTypeProperties.PlanningFeedbackCode;
                        objTransportType.ConditionValue = objTransportTypeProperties.ConditionValue;
                        objTransportType.CompareMethod = objTransportTypeProperties.CompareMethod;
                        objTransportType.CompareTo = objTransportTypeProperties.CompareTo;
                        objTransportType.InfoColumn = objTransportTypeProperties.InfoColumn;
                        objTransportType.InfoColumnAction = objTransportTypeProperties.InfoColumnAction;
                        objTransportType.ConditionSavedValue = objTransportTypeProperties.ConditionSavedValue;
                        objTransportType.CompareSavedValue = objTransportTypeProperties.CompareSavedValue;
                        objTransportType.CompareFixedText = objTransportTypeProperties.CompareFixedText;
                        objTransportType.Action = objTransportTypeProperties.Action;
                        objTransportType.CreatedBy = userCredentials.id;
                        objTransportType.CreatedOn = time;
                        objTransportType.PartnerCode = objTransportTypeProperties.PartnerCode;
                        objTransportType.DashboardTransportTypeId = transportTypeId;
                        objTransportType.AddDefault = objTransportTypeProperties.AddDefault;

                        var TransportTypeResult = QuestionLogicRepository.SaveTransportTypeProperties(objTransportType);



                        return Json(TransportTypeResult.TransportTypeId + "," + TransportTypeResult.QuestionRefId);
                    }
                    if (propertyName == "Trailer")
                    {
                        int AliasNameDetailsId = 0;
                        string ANameDetailsId = "0";
                        TrailerProperties objTrailerProperties = JsonConvert.DeserializeObject<TrailerProperties>(WebUtility.UrlDecode(Question));
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objTrailerProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        if (objTrailerProperties.ActionType == "Disconnect")
                            objQuestion.Name = objTrailerProperties.Name;
                        else
                            objQuestion.Name = objTrailerProperties.Title.Replace("ƒ", "+");
                        objQuestion.ParentId = objTrailerProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        if (objTrailerProperties.Save == true && objTrailerProperties.SaveTo == 0)
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = objTrailerProperties.LayoutRefId;
                            AND.ActivitiesRefId = objTrailerProperties.ActivitiesRefId;
                            AND.AliasName = "Trailer";
                            AND.PropertyName = propertyName;
                            AND.CreatedBy = userCredentials.id;
                            AND.CreatedOn = time;
                            AND.IsTempData = false;
                            var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                            AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                            ANameDetailsId = aliasNameId.AliasNameDetailsId.ToString();
                        }

                        TrailerProperties objTrailer = new TrailerProperties();
                        objTrailer.ActionType = objTrailerProperties.ActionType;

                        objTrailer.QuestionRefId = questionsresult.QuestionId;
                        objTrailer.LayoutRefId = LayoutID;
                        objTrailer.ActivitiesRefId = objTrailerProperties.ActivitiesRefId;
                        objTrailer.TrailerFormat = objTrailerProperties.TrailerFormat;
                        if (objTrailer.ActionType == "Connect")
                        {
                            objTrailer.Title = objTrailerProperties.Title.Replace("ƒ", "+");
                            objTrailer.MaskText = objTrailerProperties.MaskText;
                            objTrailer.Size = objTrailerProperties.Size;
                            objTrailer.NullNotAllowed = objTrailerProperties.NullNotAllowed;
                            objTrailer.MinimumofCharacter = objTrailerProperties.MinimumofCharacter;
                            objTrailer.MinimumofCharacterValue = objTrailerProperties.MinimumofCharacterValue;
                            objTrailer.ExactOfCharacter = objTrailerProperties.ExactOfCharacter;
                            objTrailer.ZeroNotAllowed = objTrailerProperties.ZeroNotAllowed;
                            objTrailer.Regex = objTrailerProperties.Regex;
                            objTrailer.Save = objTrailerProperties.Save;

                            if (objTrailerProperties.Propose == 0)
                                objTrailer.Propose = Convert.ToInt32(ANameDetailsId);
                            else
                                objTrailer.Propose = objTrailerProperties.Propose.GetValueOrDefault();

                            objTrailer.PlanningFeedback = objTrailerProperties.PlanningFeedback;
                            objTrailer.FeedbackType = objTrailerProperties.FeedbackType;
                            objTrailer.PlanningFeedbackCode = objTrailerProperties.PlanningFeedbackCode;
                            objTrailer.AutoGenerate = objTrailerProperties.AutoGenerate;
                            objTrailer.PartnerCode = objTrailerProperties.PartnerCode;

                            if (objTrailerProperties.Save == true && objTrailerProperties.SaveTo == 0)
                                objTrailer.SaveTo = AliasNameDetailsId;
                            else
                                objTrailer.SaveTo = objTrailerProperties.SaveTo;
                        }
                        else if (objTrailer.ActionType == "Disconnect")
                        {
                            objTrailer.Title = objTrailerProperties.Name;
                        }
                        else if (objTrailer.ActionType == "WTR")
                        {
                            objTrailer.Title = objTrailerProperties.Title.Replace("ƒ", "+");
                            objTrailer.Other = objTrailerProperties.Other;
                            objTrailer.Save = objTrailerProperties.Save;
                            if (objTrailerProperties.Save == true && objTrailerProperties.SaveTo == 0)
                                objTrailer.SaveTo = AliasNameDetailsId;
                            else
                                objTrailer.SaveTo = objTrailerProperties.SaveTo;

                            objQuestion = new Questions();
                            var questionid = questionsresult.QuestionId;
                            objQuestion.ActivitiesRefId = objTrailerProperties.ActivitiesRefId;
                            objQuestion.CreatedBy = userCredentials.id;
                            objQuestion.CreatedOn = time;
                            objQuestion.LayoutRefId = LayoutID;
                            objQuestion.Name = "Select";
                            objQuestion.ParentId = questionid;
                            objQuestion.TransporttypeRefId = transportTypeId;
                            objQuestion.PropertyName = "Select";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = false;
                            objQuestion.IsDefault = true;
                            objQuestion.IsChild = true;
                            QuestionLogicRepository.SaveQuestions(objQuestion);

                            objQuestion = new Questions();
                            objQuestion.ActivitiesRefId = objTrailerProperties.ActivitiesRefId;
                            objQuestion.CreatedBy = userCredentials.id;
                            objQuestion.CreatedOn = time;
                            objQuestion.LayoutRefId = LayoutID;
                            objQuestion.Name = "Other";
                            objQuestion.ParentId = questionid;
                            objQuestion.TransporttypeRefId = transportTypeId;
                            objQuestion.PropertyName = "Other";
                            objQuestion.ISModification = 0;
                            objQuestion.IsQuestionPath = true;
                            objQuestion.IsParent = false;
                            objQuestion.IsDefault = true;
                            objQuestion.IsChild = true;
                            QuestionLogicRepository.SaveQuestions(objQuestion);
                        }

                        objTrailer.Name = objTrailerProperties.Name;
                        objTrailer.Comment = objTrailerProperties.Comment;
                        objTrailer.CreatedBy = userCredentials.id;
                        objTrailer.CreatedOn = time;

                        var Trailer = QuestionLogicRepository.SaveTrailerProperties(objTrailer);
                        if (objTrailerProperties.Save == true && objTrailerProperties.SaveTo == 0)
                        {
                            QuestionLogicRepository.UpdateAliasPropertyTableId(AliasNameDetailsId, Trailer.TrailerId);
                        }

                        return Json(questionsresult.QuestionId);
                    }
                    else if (propertyName == "Flex")
                    {
                        Flex objFlex = JsonConvert.DeserializeObject<Flex>(WebUtility.UrlDecode(Question));

                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objFlex.ActivityRefID);
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objFlex.Title;
                        objQuestion.ParentId = 0;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);


                        var questionid = questionsresult.QuestionId;

                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Extra Info";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Extra Info";
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        QuestionLogicRepository.SaveQuestions(objQuestion);

                        ExtraInfoProperties objExtraInfoProperties = new ExtraInfoProperties();
                        objExtraInfoProperties.ActivityRefID = objFlex.ActivityRefID;
                        objExtraInfoProperties.LayoutRefID = objFlex.LayoutRefID;
                        objExtraInfoProperties.Title = "Extra Info";
                        objExtraInfoProperties.AlternativetextonJoblevel = "";
                        objExtraInfoProperties.AlternativetextonPlacelevel = "";
                        objExtraInfoProperties.Alternativetextonproductlevel = "";
                        objExtraInfoProperties.CreatedBy = userCredentials.id;
                        objExtraInfoProperties.CreatedOn = time;
                        objExtraInfoProperties.QuestionRefID = objQuestion.QuestionId;
                        QuestionLogicRepository.InsertExtraInfoProperties(objExtraInfoProperties);


                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Pallets";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Pallets";
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        QuestionLogicRepository.SaveQuestions(objQuestion);

                        PalletsProperties objPalletsProperties = new PalletsProperties();
                        objPalletsProperties.ActivityRefID = objFlex.ActivityRefID;
                        objPalletsProperties.LayoutRefID = objFlex.LayoutRefID;
                        objPalletsProperties.Title = "Pallets";
                        objPalletsProperties.AlternativetextonJoblevel = "";
                        objPalletsProperties.AlternativetextonPlacelevel = "";
                        objPalletsProperties.Alternativetextonproductlevel = "";
                        objPalletsProperties.CreatedBy = userCredentials.id;
                        objPalletsProperties.CreatedOn = time;
                        objPalletsProperties.QuestionRefID = objQuestion.QuestionId;
                        QuestionLogicRepository.InsertPalletsProperties(objPalletsProperties);

                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Problem";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Problem";
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        QuestionLogicRepository.SaveQuestions(objQuestion);

                        ProblemsProperties objProblemsProperties = new ProblemsProperties();
                        objProblemsProperties.ActivityRefID = objFlex.ActivityRefID;
                        objProblemsProperties.LayoutRefID = objFlex.LayoutRefID;
                        objProblemsProperties.Title = "Problem";
                        objProblemsProperties.AlternativetextonJoblevel = "";
                        objProblemsProperties.AlternativetextonPlacelevel = "";
                        objProblemsProperties.Alternativetextonproductlevel = "";
                        objProblemsProperties.CreatedBy = userCredentials.id;
                        objProblemsProperties.CreatedOn = time;
                        objProblemsProperties.QuestionRefID = objQuestion.QuestionId;
                        QuestionLogicRepository.InsertProblemsProperties(objProblemsProperties);

                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Signature";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Signature";
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        QuestionLogicRepository.SaveQuestions(objQuestion);


                        SignaturesProperties objSignaturesProperties = new SignaturesProperties();
                        objSignaturesProperties.ActivityRefID = objFlex.ActivityRefID;
                        objSignaturesProperties.LayoutRefID = objFlex.LayoutRefID;
                        objSignaturesProperties.Title = "Signature";
                        objSignaturesProperties.AlternativetextonJoblevel = "";
                        objSignaturesProperties.AlternativetextonPlacelevel = "";
                        objSignaturesProperties.Alternativetextonproductlevel = "";
                        objSignaturesProperties.JobLevel = false;
                        objSignaturesProperties.PlaceLevel = false;
                        objSignaturesProperties.CreatedBy = userCredentials.id;
                        objSignaturesProperties.CreatedOn = time;
                        objSignaturesProperties.QuestionRefID = objQuestion.QuestionId;
                        QuestionLogicRepository.InsertSignaturesProperties(objSignaturesProperties);


                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Picture";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Picture";
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        QuestionLogicRepository.SaveQuestions(objQuestion);

                        PictureProperties objPictureProperties = new PictureProperties();
                        objPictureProperties.ActivityRefID = objFlex.ActivityRefID;
                        objPictureProperties.LayoutRefID = objFlex.LayoutRefID;
                        objPictureProperties.Title = "Picture";
                        objPictureProperties.AlternativetextonJoblevel = "";
                        objPictureProperties.AlternativetextonPlacelevel = "";
                        objPictureProperties.Alternativetextonproductlevel = "";
                        objPictureProperties.CreatedBy = userCredentials.id;
                        objPictureProperties.CreatedOn = time;
                        objPictureProperties.QuestionRefID = objQuestion.QuestionId;
                        QuestionLogicRepository.InsertPictureProperties(objPictureProperties);


                        Flex objflexproperties = new Flex();
                        objflexproperties.ActivityRefID = objFlex.ActivityRefID;
                        objflexproperties.LayoutRefID = objFlex.LayoutRefID;
                        objflexproperties.Title = objFlex.Title;
                        objflexproperties.AlternativetextonJoblevel = objFlex.AlternativetextonJoblevel;
                        objflexproperties.AlternativetextonPlacelevel = objFlex.AlternativetextonPlacelevel;
                        objflexproperties.Alternativetextonproductlevel = objFlex.Alternativetextonproductlevel;
                        objflexproperties.Status = 1;
                        objflexproperties.CreatedBy = userCredentials.id;
                        objflexproperties.CreatedOn = time;
                        objflexproperties.QuestionRefID = questionid;

                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "DocumentScanner";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "DocumentScanner";
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        QuestionLogicRepository.SaveQuestions(objQuestion);

                        DocumentScannerProperties objDocumentScannerProperties = new DocumentScannerProperties();
                        objDocumentScannerProperties.ActivityRefID = objFlex.ActivityRefID;
                        objDocumentScannerProperties.LayoutRefID = objFlex.LayoutRefID;
                        objDocumentScannerProperties.Title = "DocumentScanner";
                        objDocumentScannerProperties.AlternativetextonJoblevel = "";
                        objDocumentScannerProperties.AlternativetextonPlacelevel = "";
                        objDocumentScannerProperties.Alternativetextonproductlevel = "";
                        objDocumentScannerProperties.CreatedBy = userCredentials.id;
                        objDocumentScannerProperties.CreatedOn = time;
                        objDocumentScannerProperties.QuestionRefID = objQuestion.QuestionId;
                        QuestionLogicRepository.InsertDocumentScannerProperties(objDocumentScannerProperties);

                        return Json(QuestionLogicRepository.InsertFlex(objflexproperties));
                    }
                    else if (propertyName == "Smart")
                    {
                        Flex objFlex = JsonConvert.DeserializeObject<Flex>(WebUtility.UrlDecode(Question));

                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objFlex.ActivityRefID);
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objFlex.Title;
                        objQuestion.ParentId = 0;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsSmart = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveSmartQuestions(objQuestion);


                        var questionid = questionsresult.QuestionId;

                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Extra Info";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Smart Extra Info";
                        objQuestion.ISModification = 0;
                        objQuestion.IsSmart = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        QuestionLogicRepository.SaveSmartQuestions(objQuestion);


                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Pallets";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Smart Pallets";
                        objQuestion.ISModification = 0;
                        objQuestion.IsSmart = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        QuestionLogicRepository.SaveSmartQuestions(objQuestion);


                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Problem";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Smart Problem";
                        objQuestion.ISModification = 0;
                        objQuestion.IsSmart = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        QuestionLogicRepository.SaveSmartQuestions(objQuestion);


                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Signature";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Smart Signature";
                        objQuestion.ISModification = 0;
                        objQuestion.IsSmart = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        QuestionLogicRepository.SaveSmartQuestions(objQuestion);



                        objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objFlex.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = "Picture";
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Smart Picture";
                        objQuestion.ISModification = 0;
                        objQuestion.IsSmart = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        QuestionLogicRepository.SaveSmartQuestions(objQuestion);


                        return Json("Smart created");
                    }
                    else if (propertyName == "Get System Values")
                    {
                        int AliasNameDetailsId = 0;
                        string ANameDetailsId = "0";

                        GetSystemValuesProperties objGetSystemValuesProperties = JsonConvert.DeserializeObject<GetSystemValuesProperties>(WebUtility.UrlDecode(Question));
                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objGetSystemValuesProperties.ActivitiesRefId);
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objGetSystemValuesProperties.ActivitiesRefId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = LayoutID;
                        objQuestion.Name = objGetSystemValuesProperties.Name;
                        objQuestion.ParentId = objGetSystemValuesProperties.QuestionRefId;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = propertyName;
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = false;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        AliasNameDetails AND = new AliasNameDetails();
                        AND.LayoutRefId = objGetSystemValuesProperties.LayoutRefId;
                        AND.ActivitiesRefId = objGetSystemValuesProperties.ActivitiesRefId;
                        AND.AliasName = objGetSystemValuesProperties.SaveToText;
                        AND.PropertyName = propertyName;
                        AND.CreatedBy = userCredentials.id;
                        AND.CreatedOn = time;
                        AND.IsTempData = false;
                        var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                        AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                        ANameDetailsId = aliasNameId.AliasNameDetailsId.ToString();

                        GetSystemValuesProperties objGSVProperties = new GetSystemValuesProperties();
                        objGSVProperties.ActivitiesRefId = objGetSystemValuesProperties.ActivitiesRefId;
                        objGSVProperties.Name = objGetSystemValuesProperties.Name;
                        objGSVProperties.LayoutRefId = objGetSystemValuesProperties.LayoutRefId;
                        objGSVProperties.QuestionRefId = questionsresult.QuestionId;
                        objGSVProperties.GetSystemValuesFormatRefId = (objGetSystemValuesProperties.GetSystemValuesFormatRefId > 0 ? objGetSystemValuesProperties.GetSystemValuesFormatRefId : null);
                        objGSVProperties.GetSystemValuesRefId = objGetSystemValuesProperties.GetSystemValuesRefId;

                        int intSaveTo;
                        if (objGetSystemValuesProperties.SaveToValue == "0" || ((int.TryParse(objGetSystemValuesProperties.SaveToValue, out intSaveTo)) == false))
                        {
                            objGSVProperties.SaveTo = AliasNameDetailsId;
                        }
                        else
                        {
                            Int32.TryParse(objGetSystemValuesProperties.SaveToValue, out intSaveTo);
                            objGSVProperties.SaveTo = intSaveTo;
                        }

                        string defaultAliasName = objGetSystemValuesProperties.Name.Replace("Get ", listActivitiesRegistrations + ".");
                        if (defaultAliasName == objGetSystemValuesProperties.SaveToText)
                        {
                            objGSVProperties.IsDefaultAlaisName = true;
                        }
                        else
                        {
                            objGSVProperties.IsDefaultAlaisName = false;
                        }

                        objGSVProperties.CreatedBy = userCredentials.id;
                        objGSVProperties.CreatedOn = time;
                        objGSVProperties.Comment = objGetSystemValuesProperties.Comment;
                        QuestionLogicRepository.SaveGetSystemValuesProperties(objGSVProperties);

                        return Json(questionsresult.QuestionId);
                    }
                    else
                    {
                        return Json("Not in option");
                    }
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdateDescription(string Description)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    boQuestion objQuestionsProperties = JsonConvert.DeserializeObject<boQuestion>(WebUtility.UrlDecode(Description));
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = objQuestionsProperties.QuestionId;
                    objQuestions.Name = objQuestionsProperties.Name;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    return Json(QuestionLogicRepository.UpdateQuestions(objQuestions));
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateDocumetScanProperties(string DocumetScanProperties)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DocumetScanProperties objDocumentScanProperties = JsonConvert.DeserializeObject<DocumetScanProperties>(WebUtility.UrlDecode(DocumetScanProperties));
                    objDocumentScanProperties.Title = objDocumentScanProperties.Title.Replace("ƒ", "+");
                    objDocumentScanProperties.DocumentName = objDocumentScanProperties.DocumentName.Replace("ƒ", "+");

                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objDocumentScanProperties.QuestionRefId);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();

                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = objDocumentScanProperties.QuestionRefId;
                        objQuestions.Name = objDocumentScanProperties.Title;
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);

                        DocumetScanProperties objDcoument = new DocumetScanProperties();
                        objDcoument.DocumetScanId = objDocumentScanProperties.DocumetScanId;
                        objDcoument.ActivitiesRefId = objDocumentScanProperties.ActivitiesRefId;
                        objDcoument.DocumentName = objDocumentScanProperties.DocumentName;
                        objDcoument.LayoutRefId = objDocumentScanProperties.LayoutRefId;
                        objDcoument.Title = objDocumentScanProperties.Title;
                        objDcoument.PlanningFeedbackLevel = objDocumentScanProperties.PlanningFeedbackLevel;
                        objDcoument.UpdatedBy = userCredentials.id;
                        objDcoument.UpdatedOn = time;
                        objDcoument.Comment = objDocumentScanProperties.Comment;

                        objDcoument.Conditionitem = objDocumentScanProperties.Conditionitem;
                        objDcoument.ConCompareMethod = objDocumentScanProperties.ConCompareMethod;
                        objDcoument.Convalue = objDocumentScanProperties.Convalue;
                        objDcoument.ComFixedText = objDocumentScanProperties.ComFixedText;

                        objDcoument.ConItemSavedValued = objDocumentScanProperties.ConItemSavedValued;
                        objDcoument.ConvalueSavedValue = objDocumentScanProperties.ConvalueSavedValue;
                        var documentScanProperties = QuestionLogicRepository.UpdateDocumetScanProperties(objDcoument);
                        return Json(documentScanProperties.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Document Scan Property Already Deleted" });
                    }
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdateTextMessageProperties(string TextMessageProperties)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {

                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    TextMessageProperties objTextMessageProperties = JsonConvert.DeserializeObject<TextMessageProperties>(WebUtility.UrlDecode(TextMessageProperties));
                    objTextMessageProperties.Messagecontent = objTextMessageProperties.Messagecontent.Replace("ƒ", "+");
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objTextMessageProperties.QuestionRefId);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        TextMessageProperties objTextMessage = new TextMessageProperties();
                        objTextMessage.TextMessageId = objTextMessageProperties.TextMessageId;
                        objTextMessage.ActivitiesRefId = objTextMessageProperties.ActivitiesRefId;
                        objTextMessage.Name = objTextMessageProperties.Name;
                        objTextMessage.LayoutRefId = objTextMessageProperties.LayoutRefId;
                        objTextMessage.SendMethod = objTextMessageProperties.SendMethod;
                        objTextMessage.Messagecontent = objTextMessageProperties.Messagecontent;
                        objTextMessage.UpdatedBy = userCredentials.id;
                        objTextMessage.UpdatedOn = time;
                        objTextMessage.Comment = objTextMessageProperties.Comment;

                        objTextMessage.Conditionitem = objTextMessageProperties.Conditionitem;
                        objTextMessage.ConCompareMethod = objTextMessageProperties.ConCompareMethod;
                        objTextMessage.Convalue = objTextMessageProperties.Convalue;
                        objTextMessage.ComFixedText = objTextMessageProperties.ComFixedText;



                        objTextMessage.ConItemSavedValued = objTextMessageProperties.ConItemSavedValued;
                        objTextMessage.ConvalueSavedValue = objTextMessageProperties.ConvalueSavedValue;
                        var textMessageProperties = QuestionLogicRepository.UpdateTextMessageProperties(objTextMessage);
                        return Json(textMessageProperties.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Text Message Property Already Deleted" });
                    }

                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdateInfomessageProperties(string InfomessageProperties, int pInfomessagenodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(pInfomessagenodeid);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();
                        InfomessageProperties objInfomessageProperties = JsonConvert.DeserializeObject<InfomessageProperties>(WebUtility.UrlDecode(InfomessageProperties));
                        objInfomessageProperties.Title = objInfomessageProperties.Title.Replace("ƒ", "+").Replace("&nbsp;", " ");
                        objInfomessageProperties.Messagecontent = objInfomessageProperties.Messagecontent.Replace("ƒ", "+").Replace("&nbsp;", " ");

                        bool isValid = true;

                        if (_translatorRepository.StripHTML(objInfomessageProperties.Messagecontent).Trim().Length > 0)
                        {

                            var Messagecontent = objInfomessageProperties.Messagecontent.Replace("<p>", "ƒ<p>");
                            var MessagecontentLines = Messagecontent.Split("ƒ").Where(x => x.Trim().Length > 0).ToList();
                            int LineNo = 0;
                            foreach (var messagecontent in MessagecontentLines)
                            {

                                string resultmessagecontent = _translatorRepository.StripHTML(messagecontent);
                                if (Regex.Matches(resultmessagecontent, "}").Count() > 1)
                                {
                                    var messagecontentsplit = resultmessagecontent.Split("}");
                                    resultmessagecontent = "";
                                    foreach (var mc in messagecontentsplit)
                                    {
                                        resultmessagecontent += RemoveStringBetween(mc, '{', '}');
                                    }
                                }
                                else
                                {
                                    resultmessagecontent = RemoveStringBetween(resultmessagecontent, '{', '}');
                                }

                                if (resultmessagecontent.Trim().Length > 35)
                                {
                                    isValid = false;
                                    return Json(new
                                    {
                                        error = "Message Content Length Greater Than 35 (" + _translatorRepository.StripHTML(messagecontent) + ")"
                                    });
                                }

                                if (resultmessagecontent.Trim().Length > 0)
                                    LineNo++;
                            }

                            if (LineNo > 12)
                                return Json(new
                                {
                                    error = "Message Content Should be 12 Lines"
                                });
                        }

                        if (_translatorRepository.StripHTML(objInfomessageProperties.Title).Trim().Length > 0)
                        {

                            string resulttitle = _translatorRepository.StripHTML(objInfomessageProperties.Title);

                            if (Regex.Matches(resulttitle, "}").Count() > 1)
                            {
                                var titlesplit = resulttitle.Split("}");
                                resulttitle = "";
                                foreach (var mc in titlesplit)
                                {
                                    resulttitle += RemoveStringBetween(mc, '{', '}');
                                }
                            }
                            else
                            {
                                resulttitle = RemoveStringBetween(resulttitle, '{', '}');
                            }

                            if (objInfomessageProperties.Messagetype == "Line")
                            {
                                if (resulttitle.Trim().Length > 50)
                                {
                                    isValid = false;
                                    return Json(new
                                    {
                                        error = "Title Length Greater Than 50 (" + _translatorRepository.StripHTML(objInfomessageProperties.Title) + ")"
                                    });
                                }
                            }
                            else
                            {
                                if (resulttitle.Trim().Length > 40)
                                {
                                    isValid = false;
                                    return Json(new
                                    {
                                        error = "Title Length Greater Than 40 (" + _translatorRepository.StripHTML(objInfomessageProperties.Title) + ")"
                                    });
                                }
                            }

                        }
                        else
                        {
                            return Json(new
                            {
                                error = "Enter Valid Info Message Title"
                            });
                        }



                        if (isValid)
                        {
                            boQuestion objQuestions = new boQuestion();
                            objQuestions.QuestionId = pInfomessagenodeid;
                            objQuestions.Name = objInfomessageProperties.Name;
                            objQuestions.UpdatedBy = userCredentials.id;
                            objQuestions.UpdatedOn = time;
                            QuestionLogicRepository.UpdateQuestions(objQuestions);

                            InfomessageProperties objInfomessage = new InfomessageProperties();
                            objInfomessage.InfomessageId = objInfomessageProperties.InfomessageId;
                            objInfomessage.ActivitiesRefId = objInfomessageProperties.ActivitiesRefId;
                            objInfomessage.Name = objInfomessageProperties.Name;
                            objInfomessage.LayoutRefId = objInfomessageProperties.LayoutRefId;
                            objInfomessage.Messagetype = objInfomessageProperties.Messagetype;
                            var title = objInfomessageProperties.Title;
                            objInfomessage.Title = "<p>" + title.Replace("<p>", "").Replace("</p>", "").Replace("<div>", "").Replace("</div>", "").Replace("<span>", "").Replace("</span>", "").Replace("<br>", "") + "</p>";
                            objInfomessage.Titlestyle = objInfomessageProperties.Titlestyle;
                            objInfomessage.InfoMessageIconTypeRefId = objInfomessageProperties.InfoMessageIconTypeRefId;
                            objInfomessage.Fontstyle = objInfomessageProperties.Fontstyle;
                            objInfomessage.Messagecontent = objInfomessageProperties.Messagecontent;
                            objInfomessage.UpdatedBy = userCredentials.id;
                            objInfomessage.UpdatedOn = time;
                            objInfomessage.Comment = objInfomessageProperties.Comment;


                            objInfomessage.Conditionitem = objInfomessageProperties.Conditionitem;
                            objInfomessage.ConCompareMethod = objInfomessageProperties.ConCompareMethod;
                            objInfomessage.Convalue = objInfomessageProperties.Convalue;
                            objInfomessage.ComFixedText = objInfomessageProperties.ComFixedText;


                            objInfomessage.ConItemSavedValued = objInfomessageProperties.ConItemSavedValued;
                            objInfomessage.ConvalueSavedValue = objInfomessageProperties.ConvalueSavedValue;
                            var infoMessageProperties = QuestionLogicRepository.UpdateInfomessageProperties(objInfomessage);
                            return Json(infoMessageProperties.QuestionRefId);
                        }
                        else
                        {
                            return Json(new
                            {
                                error = GetStatusMessage(StatusCode.E02)
                            });
                        }

                    }
                    else
                    {
                        return Json(new { error = "Info Message Property Already Deleted" });
                    }

                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdateCheckoffProperties(string CheckoffProperties, int pCheckoffnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(pCheckoffnodeid);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();
                        CheckoffProperties objCheckoffProperties = JsonConvert.DeserializeObject<CheckoffProperties>(WebUtility.UrlDecode(CheckoffProperties));

                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = pCheckoffnodeid;
                        objQuestions.Name = objCheckoffProperties.Name;
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);

                        CheckoffProperties objCheckoff = new CheckoffProperties();
                        objCheckoff.CheckoffId = objCheckoffProperties.CheckoffId;
                        objCheckoff.ActivitiesRefId = objCheckoffProperties.ActivitiesRefId;
                        objCheckoff.Name = objCheckoffProperties.Name;
                        objCheckoff.LayoutRefId = objCheckoffProperties.LayoutRefId;
                        //objCheckoff.QuestionRefId = questionsresult.QuestionId;
                        objCheckoff.Planninglevel = objCheckoffProperties.Planninglevel;
                        objCheckoff.UpdatedBy = userCredentials.id;
                        objCheckoff.UpdatedOn = time;
                        objCheckoff.Comment = objCheckoffProperties.Comment;
                        var checkoffProperties = QuestionLogicRepository.UpdateCheckoffProperties(objCheckoff);
                        return Json(checkoffProperties.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Check Off Property Already Deleted" });
                    }

                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdatePlanningselectProperties(string PlanningselectProperties, int pPlanningselectnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(pPlanningselectnodeid);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();
                        PlanningselectProperties objPlanningselectProperties = JsonConvert.DeserializeObject<PlanningselectProperties>(WebUtility.UrlDecode(PlanningselectProperties));
                        objPlanningselectProperties.Title = objPlanningselectProperties.Title.Replace("ƒ", "+");
                        objPlanningselectProperties.OtherTitle = objPlanningselectProperties.OtherTitle.Replace("ƒ", "+");

                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = pPlanningselectnodeid;
                        objQuestions.Name = objPlanningselectProperties.Title + " [ " + objPlanningselectProperties.Planninglevel + " ]";
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);

                        PlanningselectProperties objPlanningselect = new PlanningselectProperties();
                        objPlanningselect.PlanningselectId = objPlanningselectProperties.PlanningselectId;
                        objPlanningselect.ActivitiesRefId = objPlanningselectProperties.ActivitiesRefId;
                        objPlanningselect.Title = objPlanningselectProperties.Title;

                        objPlanningselect.LayoutRefId = objPlanningselectProperties.LayoutRefId;
                        objPlanningselect.Planninglevel = objPlanningselectProperties.Planninglevel;
                        objPlanningselect.Planningspecific = objPlanningselectProperties.Planningspecific;
                        objPlanningselect.Chooseactivity = objPlanningselectProperties.Chooseactivity;
                        objPlanningselect.Startselecteditem = objPlanningselectProperties.Startselecteditem;
                        objPlanningselect.Other = objPlanningselectProperties.Other;
                        objPlanningselect.PlanningSelectionMethod = objPlanningselectProperties.PlanningSelectionMethod;
                        objPlanningselect.OtherTitle = objPlanningselectProperties.OtherTitle;
                        objPlanningselect.OtherMaskText = objPlanningselectProperties.OtherMaskText;
                        objPlanningselect.StartSelection = objPlanningselectProperties.StartSelection;
                        objPlanningselect.Planningreadout = objPlanningselectProperties.Planningreadout;
                        objPlanningselect.Comment = objPlanningselectProperties.Comment;

                        objPlanningselect.CheckOffPlanning = objPlanningselectProperties.CheckOffPlanning;
                        objPlanningselect.FilterRefId = objPlanningselectProperties.FilterRefId;
                        objPlanningselect.CompareMethodRefId = objPlanningselectProperties.CompareMethodRefId;
                        objPlanningselect.PSComparetoRefId = objPlanningselectProperties.PSComparetoRefId;
                        objPlanningselect.InstructionSetSavedValues = objPlanningselectProperties.InstructionSetSavedValues;

                        objPlanningselect.UpdatedBy = userCredentials.id;
                        objPlanningselect.UpdatedOn = time;
                        var planningSelectProperties = QuestionLogicRepository.UpdatePlanningselectProperties(objPlanningselect);

                        return Json(planningSelectProperties.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Planning Select Property Already Deleted" });
                    }

                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdateSetActionValueProperties(string SetActionValueProperties, int pSetactionnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(pSetactionnodeid);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();
                        BOSetActionValueProperties objSetActionValuePropertiess = JsonConvert.DeserializeObject<BOSetActionValueProperties>(WebUtility.UrlDecode(SetActionValueProperties));
                        objSetActionValuePropertiess.Title = objSetActionValuePropertiess.Title.Replace("ƒ", "+");
                        objSetActionValuePropertiess.Value = objSetActionValuePropertiess.Value.Replace("ƒ", "+");
                        objSetActionValuePropertiess.InfoCoumnFixedText = objSetActionValuePropertiess.InfoCoumnFixedText.Replace("ƒ", "+");

                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = pSetactionnodeid;
                        objQuestions.Name = objSetActionValuePropertiess.Title;
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);
                        int AliasNameDetailsId = 0;
                        if (objSetActionValuePropertiess.TargetAlaiasVal != "" && objSetActionValuePropertiess.TargetAlaiasVal != null)
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = objSetActionValuePropertiess.LayoutRefId;
                            AND.ActivitiesRefId = objSetActionValuePropertiess.ActivitiesRefId;
                            //AND.AliasName = listActivitiesRegistrations + "." + objChioceProperties.Title;
                            AND.AliasName = objSetActionValuePropertiess.TargetAlaiasVal;
                            AND.PropertyName = "Set Action / Value";
                            //AND.PropertyTableId
                            AND.CreatedBy = userCredentials.id;
                            AND.CreatedOn = time;
                            AND.IsTempData = false;
                            var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                            AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                            //ANameDetailsId = aliasNameId.AliasNameDetailsId.ToString();
                        }

                        SetActionValueProperties objSetActionValueProperties = new SetActionValueProperties();
                        objSetActionValueProperties.SetActionValueId = objSetActionValuePropertiess.SetActionValueId;
                        objSetActionValueProperties.ActivitiesRefId = objSetActionValuePropertiess.ActivitiesRefId;
                        objSetActionValueProperties.Title = objSetActionValuePropertiess.Title;
                        objSetActionValueProperties.LayoutRefId = objSetActionValuePropertiess.LayoutRefId;
                        //objSetActionValueProperties.QuestionRefId = questionsresult.QuestionId;
                        objSetActionValueProperties.SetActionValueTypeId = objSetActionValuePropertiess.SetActionValueTypeId;
                        objSetActionValueProperties.SavedValue = objSetActionValuePropertiess.SavedValue;
                        if (objSetActionValueProperties.SetActionValueTypeId == 1)
                        {
                            objSetActionValueProperties.SelectQP = objSetActionValuePropertiess.SelectQP;
                            objSetActionValueProperties.CancelcurrentQP = objSetActionValuePropertiess.CancelcurrentQP;
                        }
                        else if (objSetActionValueProperties.SetActionValueTypeId == 2)
                        {
                            objSetActionValueProperties.SelectView = objSetActionValuePropertiess.SelectView;
                            objSetActionValueProperties.PlanningIDOBC = objSetActionValuePropertiess.PlanningIDOBC > 0 ? objSetActionValuePropertiess.PlanningIDOBC : null;
                            objSetActionValueProperties.Longitude = objSetActionValuePropertiess.Longitude > 0 ? objSetActionValuePropertiess.Longitude : null;
                            objSetActionValueProperties.Latitude = objSetActionValuePropertiess.Latitude > 0 ? objSetActionValuePropertiess.Latitude : null;
                        }
                        else if (objSetActionValueProperties.SetActionValueTypeId == 3)
                        {
                            objSetActionValueProperties.SelectAllowance = objSetActionValuePropertiess.SelectAllowance;
                            var resultAllowance = QuestionLogicRepository.Updateallowances(objSetActionValuePropertiess.LayoutRefId, objSetActionValueProperties.SelectAllowance, objSetActionValueProperties.SetActionValueId ?? 0, userCredentials.id.ToString());
                        }
                        else if (objSetActionValueProperties.SetActionValueTypeId == 4)
                        {
                            objSetActionValueProperties.SelectInfoColumn = objSetActionValuePropertiess.SelectInfoColumn;
                            objSetActionValueProperties.SetReset = objSetActionValuePropertiess.SetReset;
                            objSetActionValueProperties.InfoCoumnFixedText = objSetActionValuePropertiess.InfoCoumnFixedText;
                            objSetActionValueProperties.InfoColumnSavedValue = objSetActionValuePropertiess.InfoColumnSavedValue;
                        }
                        else if (objSetActionValueProperties.SetActionValueTypeId == 5)
                        {
                            objSetActionValueProperties.Value = ((objSetActionValuePropertiess.Value != null && objSetActionValuePropertiess.Value.Trim().Length > 0) ? objSetActionValuePropertiess.Value : null);
                            objSetActionValueProperties.CopySavedValue = objSetActionValuePropertiess.CopySavedValue > 0 ? objSetActionValuePropertiess.CopySavedValue : null;
                            //objSetActionValueProperties.SelectTargetSavedValue = objSetActionValuePropertiess.SelectTargetSavedValue;

                            int intSaveTo;
                            if (objSetActionValuePropertiess.SelectTargetSavedValue == "0" || ((int.TryParse(objSetActionValuePropertiess.SelectTargetSavedValue, out intSaveTo)) == false))
                            {
                                objSetActionValueProperties.SelectTargetSavedValue = AliasNameDetailsId;
                            }
                            else
                            {
                                objSetActionValueProperties.SelectTargetSavedValue = Convert.ToInt32(objSetActionValuePropertiess.SelectTargetSavedValue);
                            }

                            objSetActionValueProperties.CopyRange = objSetActionValuePropertiess.CopyRange;
                            objSetActionValueProperties.StartChar = objSetActionValuePropertiess.StartChar;
                            objSetActionValueProperties.CharLength = objSetActionValuePropertiess.CharLength;
                        }
                        else if (objSetActionValueProperties.SetActionValueTypeId == 6)
                        {
                            objSetActionValueProperties.SetState = objSetActionValuePropertiess.SetState;
                        }

                        objSetActionValueProperties.UpdatedBy = userCredentials.id;
                        objSetActionValueProperties.UpdatedOn = time;
                        objSetActionValueProperties.Comment = objSetActionValuePropertiess.Comment;


                        objSetActionValueProperties.Conditionitem = objSetActionValuePropertiess.Conditionitem;
                        objSetActionValueProperties.ConCompareMethod = objSetActionValuePropertiess.ConCompareMethod;
                        objSetActionValueProperties.Convalue = objSetActionValuePropertiess.Convalue;
                        objSetActionValueProperties.ComFixedText = objSetActionValuePropertiess.ComFixedText;
                        objSetActionValueProperties.ConItemSavedValued = objSetActionValuePropertiess.ConItemSavedValued;
                        objSetActionValueProperties.ConvalueSavedValue = objSetActionValuePropertiess.ConvalueSavedValue;

                        var setActionValueProperties = QuestionLogicRepository.UpdateSetActionValueProperties(objSetActionValueProperties);
                        return Json(setActionValueProperties.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Set Action/Value Property Already Deleted" });
                    }
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdateChioceProperties(string ChioceProperties)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    BOChioceProperties objChioceProperties = JsonConvert.DeserializeObject<BOChioceProperties>(WebUtility.UrlDecode(ChioceProperties));
                    objChioceProperties.Title = objChioceProperties.Title.Replace("ƒ", "+");

                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objChioceProperties.QuestionRefId);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = objChioceProperties.QuestionRefId;
                        objQuestions.Name = objChioceProperties.Title;
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);

                        int AliasNameDetailsId = 0;
                        string ANameDetailsId = "0";
                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objChioceProperties.ActivitiesRefId);
                        if (objChioceProperties.Save == 1)
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = objChioceProperties.LayoutRefId;
                            AND.ActivitiesRefId = objChioceProperties.ActivitiesRefId;
                            //AND.AliasName = listActivitiesRegistrations + "." + objChioceProperties.Title;
                            AND.AliasName = objChioceProperties.SavetoAlaiasVal;
                            AND.PropertyName = "Choice";
                            //AND.PropertyTableId
                            AND.IsTempData = false;
                            AND.CreatedBy = userCredentials.id;
                            AND.CreatedOn = time;
                            var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                            AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                            ANameDetailsId = aliasNameId.AliasNameDetailsId.ToString();
                        }
                        int intSaveTo;
                        if (objChioceProperties.Saveto == "0" || ((int.TryParse(objChioceProperties.Saveto, out intSaveTo)) == false))
                        {
                            objChioceProperties.Saveto = AliasNameDetailsId.ToString();
                        }
                        else
                        {
                            objChioceProperties.Saveto = objChioceProperties.Saveto;
                        }

                        string defaultAliasName = listActivitiesRegistrations + "." + objChioceProperties.Title;
                        if (defaultAliasName == objChioceProperties.SavetoAlaiasVal && objChioceProperties.SavetoAlaiasVal != null && objChioceProperties.SavetoAlaiasVal != string.Empty)
                        {
                            objChioceProperties.IsDefaultAlaisName = true;
                        }
                        else
                        {
                            objChioceProperties.IsDefaultAlaisName = false;
                        }
                        objChioceProperties.UpdatedBy = userCredentials.id;
                        objChioceProperties.UpdatedOn = time;
                        var choiceResult = QuestionLogicRepository.UpdateChioceProperties(objChioceProperties);

                        if (objChioceProperties.Save == 1)
                        {
                            QuestionLogicRepository.UpdateAliasPropertyTableId(AliasNameDetailsId, choiceResult.ChioceId.GetValueOrDefault());
                        }
                        return Json(choiceResult.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Choice Property Already Deleted" });
                    }
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdateAddChioceProperties(string AddChioceProperties)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    AddChioceProperties objAddChioceProperties = JsonConvert.DeserializeObject<AddChioceProperties>(WebUtility.UrlDecode(AddChioceProperties));
                    objAddChioceProperties.Title = objAddChioceProperties.Title.Replace("ƒ", "+");
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objAddChioceProperties.QuestionRefId);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = objAddChioceProperties.QuestionRefId;
                        objQuestions.Name = objAddChioceProperties.Title;
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);

                        if (objAddChioceProperties.CreatedBy == null)
                        {
                            objAddChioceProperties.CreatedBy = userCredentials.id;
                        }
                        if (objAddChioceProperties.CreatedOn == null)
                        {
                            objAddChioceProperties.CreatedOn = time;
                        }

                        objAddChioceProperties.UpdatedBy = userCredentials.id;
                        objAddChioceProperties.UpdatedOn = time;

                        AddChioceProperties addChoiceDetails = QuestionLogicRepository.UpdateAddChioceProperties(objAddChioceProperties);

                        return Json(addChoiceDetails.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Choice Detail Already Deleted" });
                    }

                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateConditionalProperties(string ConditionalProperties, int pConditionalActionnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(pConditionalActionnodeid);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();
                        ConditionalProperties objConditionalProperties = JsonConvert.DeserializeObject<ConditionalProperties>(WebUtility.UrlDecode(ConditionalProperties));
                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = pConditionalActionnodeid;
                        objQuestions.Name = objConditionalProperties.Name;
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);


                        ConditionalProperties objConditionalProp = new ConditionalProperties();
                        objConditionalProp.ConditionalID = objConditionalProperties.ConditionalID;
                        objConditionalProp.ActivitiesRefId = objConditionalProperties.ActivitiesRefId;
                        objConditionalProp.Name = objConditionalProperties.Name;
                        objConditionalProp.LayoutRefId = objConditionalProperties.LayoutRefId;
                        //objConditionalProp.QuestionRefId = questionsresult.QuestionId;

                        objConditionalProp.ConditionValue = objConditionalProperties.ConditionValue;
                        objConditionalProp.ConCompareMethod = objConditionalProperties.ConCompareMethod;
                        objConditionalProp.ConCompareTo = objConditionalProperties.ConCompareTo;
                        objConditionalProp.ConSavedValued = objConditionalProperties.ConSavedValued;
                        objConditionalProp.comtoSavedValue = objConditionalProperties.comtoSavedValue;
                        objConditionalProp.ComFixedText = objConditionalProperties.ComFixedText;
                        objConditionalProp.Comment = objConditionalProperties.Comment;
                        objConditionalProp.Repeat = objConditionalProperties.Repeat;
                        objConditionalProp.UpdatedBy = userCredentials.id;
                        objConditionalProp.UpdatedOn = time;
                        var conditionalActionProperties = QuestionLogicRepository.UpdateConditionalProperties(objConditionalProp);
                        return Json(conditionalActionProperties.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Conditional Action Property Already Deleted" });
                    }
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdateEntryProperties(string EntryPropertiesProperties, int pnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(pnodeid);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();
                        int AliasNameDetailsId = 0;
                        string ANameDetailsId = "0";
                        boEntryProperties objEntryProperties = JsonConvert.DeserializeObject<boEntryProperties>(WebUtility.UrlDecode(EntryPropertiesProperties));
                        objEntryProperties.Title = objEntryProperties.Title.Replace("ƒ", "+");

                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objEntryProperties.ActivitiesRefId);

                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = pnodeid;
                        objQuestions.Name = objEntryProperties.Title;
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);


                        if (objEntryProperties.Save == 1)
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = objEntryProperties.LayoutRefId;
                            AND.ActivitiesRefId = objEntryProperties.ActivitiesRefId;
                            AND.AliasName = objEntryProperties.SavetoAlaiasVal;
                            AND.PropertyName = "Entry";
                            AND.CreatedBy = userCredentials.id;
                            AND.IsTempData = false;
                            AND.CreatedOn = time;
                            var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                            AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                            ANameDetailsId = aliasNameId.AliasNameDetailsId.ToString();
                        }

                        EntryProperties objEntries = new EntryProperties();
                        objEntries.EntryId = objEntryProperties.EntryId;
                        objEntries.LayoutRefId = objEntryProperties.LayoutRefId;
                        objEntries.Title = objEntryProperties.Title;
                        objEntries.MaskText = objEntryProperties.MaskText;
                        objEntries.Size = objEntryProperties.Size;
                        objEntries.NotNull = objEntryProperties.NotNull;
                        objEntries.MinimumofCharacter = objEntryProperties.MinimumofCharacter;
                        objEntries.MinimumOfCharterValue = objEntryProperties.MinimumOfCharterValue;
                        objEntries.ExactCharacter = objEntryProperties.ExactCharacter;
                        objEntries.Zeronotallowed = objEntryProperties.Zeronotallowed;
                        objEntries.Regex = objEntryProperties.Regex;
                        objEntries.Predefined = objEntryProperties.Predefined;
                        objEntries.Save = objEntryProperties.Save;
                        if (objEntryProperties.Propose == 0)
                        {
                            objEntries.Propose = Convert.ToInt32(ANameDetailsId);
                        }
                        else
                        {
                            objEntries.Propose = objEntryProperties.Propose;
                        }
                        objEntries.ProposeFixedValue = objEntryProperties.ProposeFixedValue;
                        objEntries.Planningfeedback = objEntryProperties.Planningfeedback;
                        objEntries.FeedbackType = objEntryProperties.FeedbackType;
                        objEntries.PlanningfeedbackCode = objEntryProperties.PlanningfeedbackCode;
                        objEntries.EntryType = objEntryProperties.EntryType;
                        objEntries.CreatedBy = userCredentials.id;
                        objEntries.CreatedOn = time;
                        objEntries.UpdatedBy = userCredentials.id;
                        objEntries.UpdatedOn = time;
                        objEntries.Comment = objEntryProperties.Comment;
                        objEntries.DatetimeFormat = objEntryProperties.DatetimeFormat;
                        objEntries.Conditionitem = objEntryProperties.Conditionitem;
                        objEntries.ConCompareMethod = objEntryProperties.ConCompareMethod;
                        objEntries.Convalue = objEntryProperties.Convalue;
                        objEntries.Infocolumn = objEntryProperties.Infocolumn;
                        objEntries.InfoColumnAction = objEntryProperties.InfoColumnAction;
                        objEntries.FileNumber = objEntryProperties.FileNumber;
                        objEntries.AutoGenerate = objEntryProperties.AutoGenerate;
                        objEntries.ComFixedText = objEntryProperties.ComFixedText;
                        objEntries.QuestionRefId = pnodeid;
                        objEntries.PropertyName = objEntryProperties.PropertyName;

                        if (objEntryProperties.ConItemSavedValued == 0)
                        {
                            objEntries.ConItemSavedValued = AliasNameDetailsId;
                        }
                        else
                        {
                            objEntries.ConItemSavedValued = objEntryProperties.ConItemSavedValued;
                        }
                        if (objEntryProperties.ConvalueSavedValue == 0)
                        {
                            objEntries.ConvalueSavedValue = AliasNameDetailsId;
                        }
                        else
                        {
                            objEntries.ConvalueSavedValue = objEntryProperties.ConvalueSavedValue;
                        }

                        int intSaveTo;
                        if (objEntryProperties.Saveto == "0" || ((int.TryParse(objEntryProperties.Saveto, out intSaveTo)) == false))
                        {
                            objEntries.SaveTo = AliasNameDetailsId;
                        }
                        else
                        {
                            objEntries.SaveTo = Convert.ToInt32(objEntryProperties.Saveto);
                        }

                        string defaultAliasName = listActivitiesRegistrations + "." + objEntryProperties.Title;
                        if (defaultAliasName == objEntryProperties.SavetoAlaiasVal && objEntryProperties.SavetoAlaiasVal != null && objEntryProperties.SavetoAlaiasVal != string.Empty)
                        {
                            objEntries.IsDefaultAlaisName = true;
                        }
                        else
                        {
                            objEntries.IsDefaultAlaisName = false;
                        }
                        objEntries.PartnerCode = objEntryProperties.PartnerCode;
                        objEntries.SaveValue = objEntryProperties.SaveValue;

                        var entryResult = QuestionLogicRepository.UpdateEntryProperties(objEntries);

                        if (objEntryProperties.Save == 1)
                        {
                            QuestionLogicRepository.UpdateAliasPropertyTableId(AliasNameDetailsId, entryResult.EntryId.GetValueOrDefault());
                        }

                        return Json(entryResult.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Entry Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult DeleteAddChioceProperties(int AddChioceId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteAddchioce(AddChioceId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult SaveAddChioceProperties(int layoutId, int activityId, int transportTypeId, int questionidchoice, string AddChioce)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    AddChioceProperties objAddChioceProperties = JsonConvert.DeserializeObject<AddChioceProperties>(WebUtility.UrlDecode(AddChioce));
                    objAddChioceProperties.Title = objAddChioceProperties.Title.Replace("ƒ", "+");
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(questionidchoice);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = activityId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = layoutId;
                        objQuestion.Name = objAddChioceProperties.Title;
                        objQuestion.ParentId = questionidchoice;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Add Chioce";
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = true;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        AddChioceProperties objAddChioce = new AddChioceProperties();
                        objAddChioce.Title = objAddChioceProperties.Title;
                        objAddChioce.PlanningfeedbackCode = objAddChioceProperties.PlanningfeedbackCode;
                        objAddChioce.ChioceRefId = objAddChioceProperties.ChioceRefId;
                        objAddChioce.Endrecursiveloop = objAddChioceProperties.Endrecursiveloop;
                        objAddChioce.Infocolumn = objAddChioceProperties.Infocolumn;
                        //objAddChioce.InfoColumnAction = objAddChioceProperties.InfoColumnAction;
                        objAddChioce.InfoColumnAction = null;
                        objAddChioce.FileNumber = objAddChioceProperties.FileNumber;
                        objAddChioce.CreatedBy = userCredentials.id;
                        objAddChioce.CreatedOn = time;
                        objAddChioce.Dynamicp = objAddChioceProperties.Dynamicp;
                        objAddChioce.PartCode = objAddChioceProperties.PartCode;
                        objAddChioce.QuestionRefId = questionsresult.QuestionId;
                        return Json(QuestionLogicRepository.SaveAddChioceProperties(objAddChioce));
                    }
                    else
                    {
                        return Json(new { error = "Choice Detail Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult SaveQuestionsDefault(int LayoutID, int transportTypeId, string Question, int activityId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    //boEntryProperties objEntryProperties = JsonConvert.DeserializeObject<boEntryProperties>(WebUtility.UrlDecode(Question));
                    Questions objQuestion = new Questions();
                    objQuestion.ActivitiesRefId = activityId;
                    objQuestion.CreatedBy = userCredentials.id;
                    objQuestion.CreatedOn = time;
                    objQuestion.LayoutRefId = LayoutID;
                    objQuestion.Name = Question;
                    objQuestion.ParentId = 0;
                    objQuestion.TransporttypeRefId = transportTypeId;
                    objQuestion.PropertyName = "Root"; //Question
                    objQuestion.ISModification = 0;
                    objQuestion.IsQuestionPath = true;
                    return Json(QuestionLogicRepository.SaveQuestions(objQuestion));
                }
                catch (Exception ex)
                {
                    string error = ex.ToString();
                    return Json(error);
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetEntryProperties(int questionRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetEntryProperties(questionRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetParticularActivitiesRegistration(int activitiesId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetParticularActivitiesRegistration(activitiesId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetDocumetScanProperties(int questionRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetDocumetScanProperties(questionRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetTextMessageProperties(int questionRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetTextMessageProperties(questionRefId, layoutId)); // ts
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetInfomessageProperties(int questionRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetInfomessageProperties(questionRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetCheckoffProperties(int questionRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var checkoffProperties = QuestionLogicRepository.GetCheckoffProperties(questionRefId);
                    return Json(checkoffProperties);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetPlanningselectProperties(int questionRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var planningSelectProperties = QuestionLogicRepository.GetPlanningselectProperties(questionRefId, layoutId);
                    return Json(planningSelectProperties);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetSetActionValueProperties(int questionRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetSetActionValueProperties(questionRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetChioceProperties(int questionRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetChioceProperties(questionRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetAddChioceProperties(int ChioceRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetAddChioceProperties(ChioceRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetAddChiocePopup(int ChioceRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetAddChiocePopup(ChioceRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetAddChoiceDetailsByChoiceId(int choiceId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetAddChoiceDetailsByChoiceId(choiceId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        //public ActionResult GetChoiceIdByAddChoiceQuestionId(int choiceId)
        //{
        //    if (IsServerConnected(connectionStrings))
        //    {
        //        try
        //        {
        //            return Json(QuestionLogicRepository.GetAddChoiceDetailsByChoiceId(choiceId));
        //        }
        //        catch
        //        {
        //            return Json(new { error = GetStatusMessage(StatusCode.E02) });
        //        }
        //    }
        //    else
        //        return Json(new
        //        {
        //            error = GetStatusMessage(StatusCode.E01)
        //        });

        //}
        public ActionResult OBCTypes(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.OBCTypes(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetAllowancesDescription(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetAllowancesDescription(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetInfoColumns(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetInfoColumns(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult ISPlanningType(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(Convert.ToString(QuestionLogicRepository.ISPlanningType(LayoutID)));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetAliasNameDetails(int activitiesId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetAliasNameDetails(activitiesId, layoutId, "A"));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetAliasNameDetailsForPSReadOut(int activitiesId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetAliasNameDetailsForReadOut(activitiesId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetConditionCompareMethodWithOutSavedVal()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConditionCompareMethodWithOutSavedVal());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetConditionCompareMethodWithSavedVal()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConditionCompareMethodWithSavedVal());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetConditionActionCompareMethodWithSavedVal()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConditionActionCompareMethodWithSavedVal());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetConditionActionCompareMethodWithOutSavedVal()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConditionActionCompareMethodWithOutSavedVal());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetConditionCompareTo()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConditionCompareTo());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetConditionCompareToNotSavedValue()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConditionCompareToNotSavedValue());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetConditionCompareToSavedValue()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConditionCompareToSavedValue());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetConditionvalue(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConditionvalue(layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetInfoColumnAction()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetInfoColumnAction());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteAliasName(int savevalue)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteAliasName(savevalue));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetConditionItemvalue(int LayoutRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConditionItemvalue(LayoutRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetConditionalProperties(int questionRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConditionalProperties(questionRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        //public ActionResult GetSpecificQuestionTree(int QuestionID, int newParentID)
        //{
        //    if (IsServerConnected(connectionStrings))
        //    {
        //        try
        //        {
        //            return Json(QuestionLogicRepository.GetSpecificQuestionTree(QuestionID, newParentID));
        //        }
        //        catch
        //        {
        //            return Json(new { error = GetStatusMessage(StatusCode.E02) });
        //        }
        //    }
        //    else
        //        return Json(new
        //        {
        //            error = GetStatusMessage(StatusCode.E01)
        //        });

        //}
        public ActionResult DeleteSpecificQuestionTree(int QuestionID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteSpecificQuestionTree(QuestionID));
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = ex.Message
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetPlanningType(int Layout_ID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetPlanningType(Layout_ID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetPlanningTypeLevelMaster()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetPlanningTypeLevelMaster());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult PlanningFeedbackLevel(int ActivityRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.PlanningFeedbackLevel(ActivityRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetUsedPlanningType(int Layout_ID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetUsedPlanningType(Layout_ID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetConFilter()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConFilter());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetConPSCompareto()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetConPSCompareto());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetReadoutValue()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetReadoutValue());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetActivitiesRegistrationRecive(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetActivitiesRegistrationRecive(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        /// <summary>
        /// Flex Start
        /// </summary>
        /// <param name="layOutRefID"></param>
        /// <param name="activitiyRefID"></param>
        /// <param name="flexID"></param>
        /// <returns></returns>
        public ActionResult GetFlexForActivities(int questionRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetFlexForActivities(questionRefId));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult InsertFlex(string Flex)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    Flex objFlex = JsonConvert.DeserializeObject<Flex>(WebUtility.UrlDecode(Flex));
                    Flex objflexproperties = new Flex();
                    objflexproperties.ActivityRefID = objFlex.ActivityRefID;
                    objflexproperties.LayoutRefID = objFlex.LayoutRefID;
                    objflexproperties.Title = objFlex.Title;
                    objflexproperties.AlternativetextonJoblevel = objFlex.AlternativetextonJoblevel;
                    objflexproperties.AlternativetextonPlacelevel = objFlex.AlternativetextonPlacelevel;
                    objflexproperties.Alternativetextonproductlevel = objFlex.Alternativetextonproductlevel;
                    objflexproperties.Status = 1;
                    objflexproperties.CreatedBy = userCredentials.id;
                    objflexproperties.CreatedOn = time;
                    return Json(QuestionLogicRepository.InsertFlex(objflexproperties));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateFlex(string Flex, int pFlexnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    Flex objFlex = JsonConvert.DeserializeObject<Flex>(WebUtility.UrlDecode(Flex));

                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = pFlexnodeid;
                    objQuestions.Name = objFlex.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    Flex objflexproperties = new Flex();
                    objflexproperties.FlexId = objFlex.FlexId;
                    objflexproperties.ActivityRefID = objFlex.ActivityRefID;
                    objflexproperties.LayoutRefID = objFlex.LayoutRefID;
                    objflexproperties.Title = objFlex.Title;
                    objflexproperties.AlternativetextonJoblevel = objFlex.AlternativetextonJoblevel;
                    objflexproperties.AlternativetextonPlacelevel = objFlex.AlternativetextonPlacelevel;
                    objflexproperties.Alternativetextonproductlevel = objFlex.Alternativetextonproductlevel;
                    objflexproperties.Status = 1;
                    objflexproperties.UpdatedBy = userCredentials.id;
                    objflexproperties.UpdatedOn = time;
                    return Json(QuestionLogicRepository.UpdateFlex(objflexproperties));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetFlexPalletsForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetFlexPalletsForActivities(layOutRefID, activitiyRefID, QuesstionRefID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult InsertFlexPallets(int questionidPallet, string Pallets)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    Pallets objPallets = JsonConvert.DeserializeObject<Pallets>(WebUtility.UrlDecode(Pallets));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objPallets.QuestionRefID);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objPallets.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = objPallets.LayoutRefID;
                        objQuestion.Name = objPallets.Title;
                        objQuestion.ParentId = questionidPallet;
                        objQuestion.TransporttypeRefId = 0;
                        objQuestion.PropertyName = "Palletsp";
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        Pallets objPalletsproperties = new Pallets();
                        objPalletsproperties.ActivityRefID = objPallets.ActivityRefID;
                        objPalletsproperties.LayoutRefID = objPallets.LayoutRefID;
                        objPalletsproperties.QuestionRefID = objPallets.QuestionRefID;
                        objPalletsproperties.ScanLevel = objPallets.ScanLevel;
                        objPalletsproperties.PlaceLevel = objPallets.PlaceLevel;
                        objPalletsproperties.PlanningFeedbackcode = objPallets.PlanningFeedbackcode;
                        objPalletsproperties.ProductLevel = objPallets.ProductLevel;
                        objPalletsproperties.JobLevel = objPallets.JobLevel;
                        objPalletsproperties.PartnerCode = objPallets.PartnerCode;
                        objPalletsproperties.CreatedBy = userCredentials.id;
                        objPalletsproperties.CreatedOn = time;
                        objPalletsproperties.Title = objPallets.Title;
                        objPalletsproperties.PalletsRefId = questionsresult.QuestionId;
                        return Json(QuestionLogicRepository.InsertFlexPallets(objPalletsproperties));
                    }
                    else
                    {
                        return Json(new { error = "Pallets Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateFlexPallets(string Pallets)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    Pallets objPallets = JsonConvert.DeserializeObject<Pallets>(WebUtility.UrlDecode(Pallets));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objPallets.QuestionRefID);

                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = objPallets.PalletsRefId ?? 0;
                    objQuestions.Name = objPallets.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Pallets objPalletsproperties = new Pallets();
                        objPalletsproperties.PalletsID = objPallets.PalletsID;
                        objPalletsproperties.ActivityRefID = objPallets.ActivityRefID;
                        objPalletsproperties.LayoutRefID = objPallets.LayoutRefID;
                        objPalletsproperties.QuestionRefID = objPallets.QuestionRefID;
                        objPalletsproperties.ScanLevel = objPallets.ScanLevel;
                        objPalletsproperties.PlaceLevel = objPallets.PlaceLevel;
                        objPalletsproperties.PlanningFeedbackcode = objPallets.PlanningFeedbackcode;
                        objPalletsproperties.ProductLevel = objPallets.ProductLevel;
                        objPalletsproperties.JobLevel = objPallets.JobLevel;
                        objPalletsproperties.PartnerCode = objPallets.PartnerCode;
                        objPalletsproperties.CreatedBy = userCredentials.id;
                        objPalletsproperties.CreatedOn = time;
                        objPalletsproperties.UpdatedBy = userCredentials.id;
                        objPalletsproperties.UpdatedOn = time;
                        objPalletsproperties.Title = objPallets.Title;
                        objPalletsproperties.PalletsRefId = objPallets.PalletsRefId;
                        return Json(QuestionLogicRepository.UpdateFlexPallets(objPalletsproperties));
                    }
                    else
                    {
                        return Json(new { error = "Pallets Property Already Deleted" });
                    }

                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteFlexPallets(int PalletsID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteFlexPallets(PalletsID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetFlexExtraInfoForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetFlexExtraInfoForActivities(layOutRefID, activitiyRefID, QuesstionRefID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult InsertFlexExtraInfo(int questionidExtra, string ExtraInfo)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    ExtraInfo objExtraInfo = JsonConvert.DeserializeObject<ExtraInfo>(WebUtility.UrlDecode(ExtraInfo));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objExtraInfo.QuestionRefID);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objExtraInfo.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = objExtraInfo.LayoutRefID;
                        objQuestion.Name = objExtraInfo.Title;
                        objQuestion.ParentId = questionidExtra;
                        objQuestion.TransporttypeRefId = 0;
                        objQuestion.PropertyName = "Extra Infop";
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);


                        ExtraInfo objExtraInfosproperties = new ExtraInfo();
                        objExtraInfosproperties.ActivityRefID = objExtraInfo.ActivityRefID;
                        objExtraInfosproperties.LayoutRefID = objExtraInfo.LayoutRefID;
                        objExtraInfosproperties.QuestionRefID = objExtraInfo.QuestionRefID;
                        objExtraInfosproperties.ExtraInfoRefId = questionsresult.QuestionId;
                        objExtraInfosproperties.MaskText = objExtraInfo.MaskText;
                        objExtraInfosproperties.PlaceLevel = objExtraInfo.PlaceLevel;
                        objExtraInfosproperties.PlanningFeedbackcode = objExtraInfo.PlanningFeedbackcode;
                        objExtraInfosproperties.ProductLevel = objExtraInfo.ProductLevel;
                        objExtraInfosproperties.JobLevel = objExtraInfo.JobLevel;
                        objExtraInfosproperties.Format = objExtraInfo.Format;
                        objExtraInfosproperties.Size = objExtraInfo.Size;
                        objExtraInfosproperties.RegEx = objExtraInfo.RegEx;
                        objExtraInfosproperties.PartnerCode = objExtraInfo.PartnerCode;
                        objExtraInfosproperties.CreatedBy = userCredentials.id;
                        objExtraInfosproperties.CreatedOn = time;
                        objExtraInfosproperties.Title = objExtraInfo.Title;
                        return Json(QuestionLogicRepository.InsertFlexExtraInfo(objExtraInfosproperties));
                    }
                    else
                    {
                        return Json(new { error = "Extra Info Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateFlexExtraInfo(string ExtraInfo)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {

                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    ExtraInfo objExtraInfo = JsonConvert.DeserializeObject<ExtraInfo>(WebUtility.UrlDecode(ExtraInfo));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objExtraInfo.QuestionRefID);

                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = objExtraInfo.ExtraInfoRefId ?? 0;
                    objQuestions.Name = objExtraInfo.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        ExtraInfo objExtraInfosproperties = new ExtraInfo();
                        objExtraInfosproperties.ExtraInfoID = objExtraInfo.ExtraInfoID;
                        objExtraInfosproperties.ActivityRefID = objExtraInfo.ActivityRefID;
                        objExtraInfosproperties.LayoutRefID = objExtraInfo.LayoutRefID;
                        objExtraInfosproperties.QuestionRefID = objExtraInfo.QuestionRefID;
                        objExtraInfosproperties.MaskText = objExtraInfo.MaskText;
                        objExtraInfosproperties.PlaceLevel = objExtraInfo.PlaceLevel;
                        objExtraInfosproperties.PlanningFeedbackcode = objExtraInfo.PlanningFeedbackcode;
                        objExtraInfosproperties.ProductLevel = objExtraInfo.ProductLevel;
                        objExtraInfosproperties.JobLevel = objExtraInfo.JobLevel;
                        objExtraInfosproperties.Format = objExtraInfo.Format;
                        objExtraInfosproperties.Size = objExtraInfo.Size;
                        objExtraInfosproperties.RegEx = objExtraInfo.RegEx;
                        objExtraInfosproperties.PartnerCode = objExtraInfo.PartnerCode;
                        objExtraInfosproperties.CreatedBy = userCredentials.id;
                        objExtraInfosproperties.CreatedOn = time;
                        objExtraInfosproperties.UpdatedBy = userCredentials.id;
                        objExtraInfosproperties.UpdatedOn = time;
                        objExtraInfosproperties.Title = objExtraInfo.Title;
                        objExtraInfosproperties.ExtraInfoRefId = objExtraInfo.ExtraInfoRefId;
                        return Json(QuestionLogicRepository.UpdateFlexExtraInfo(objExtraInfosproperties));
                    }
                    else
                    {
                        return Json(new { error = "Extra Info Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteFlexExtraInfo(int ExtraInfoID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteFlexExtraInfo(ExtraInfoID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetFlexProblemsForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetFlexProblemsForActivities(layOutRefID, activitiyRefID, QuesstionRefID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult InsertFlexProblems(int questionidProblem, string Problems)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    Problems objProblemsInfo = JsonConvert.DeserializeObject<Problems>(WebUtility.UrlDecode(Problems));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objProblemsInfo.QuestionRefID);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objProblemsInfo.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = objProblemsInfo.LayoutRefID;
                        objQuestion.Name = objProblemsInfo.Title;
                        objQuestion.ParentId = questionidProblem;
                        objQuestion.TransporttypeRefId = 0;
                        objQuestion.PropertyName = "Problemp";
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);


                        Problems objProblemsproperties = new Problems();
                        objProblemsproperties.ActivityRefID = objProblemsInfo.ActivityRefID;
                        objProblemsproperties.LayoutRefID = objProblemsInfo.LayoutRefID;
                        objProblemsproperties.QuestionRefID = objProblemsInfo.QuestionRefID;
                        objProblemsproperties.MaskText = objProblemsInfo.MaskText;
                        objProblemsproperties.PlaceLevel = objProblemsInfo.PlaceLevel;
                        objProblemsproperties.PlanningFeedbackcode = objProblemsInfo.PlanningFeedbackcode;
                        objProblemsproperties.ProductLevel = objProblemsInfo.ProductLevel;
                        objProblemsproperties.JobLevel = objProblemsInfo.JobLevel;
                        objProblemsproperties.Format = objProblemsInfo.Format;
                        objProblemsproperties.Size = objProblemsInfo.Size;
                        objProblemsproperties.RegEx = objProblemsInfo.RegEx;
                        objProblemsproperties.PartnerCode = objProblemsInfo.PartnerCode;
                        objProblemsproperties.CreatedBy = userCredentials.id;
                        objProblemsproperties.CreatedOn = time;
                        objProblemsproperties.Title = objProblemsInfo.Title;
                        objProblemsproperties.ProblemsRefId = questionsresult.QuestionId;
                        return Json(QuestionLogicRepository.InsertFlexProblems(objProblemsproperties));
                    }
                    else
                    {
                        return Json(new { error = "Problems Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateFlexProblems(string Problems)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    Problems objProblemsInfo = JsonConvert.DeserializeObject<Problems>(WebUtility.UrlDecode(Problems));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objProblemsInfo.QuestionRefID);

                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = objProblemsInfo.ProblemsRefId ?? 0;
                    objQuestions.Name = objProblemsInfo.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Problems objProblemsproperties = new Problems();
                        objProblemsproperties.ProblemsID = objProblemsInfo.ProblemsID;
                        objProblemsproperties.ActivityRefID = objProblemsInfo.ActivityRefID;
                        objProblemsproperties.LayoutRefID = objProblemsInfo.LayoutRefID;
                        objProblemsproperties.QuestionRefID = objProblemsInfo.QuestionRefID;
                        objProblemsproperties.MaskText = objProblemsInfo.MaskText;
                        objProblemsproperties.PlaceLevel = objProblemsInfo.PlaceLevel;
                        objProblemsproperties.PlanningFeedbackcode = objProblemsInfo.PlanningFeedbackcode;
                        objProblemsproperties.ProductLevel = objProblemsInfo.ProductLevel;
                        objProblemsproperties.JobLevel = objProblemsInfo.JobLevel;
                        objProblemsproperties.Format = objProblemsInfo.Format;
                        objProblemsproperties.Size = objProblemsInfo.Size;
                        objProblemsproperties.RegEx = objProblemsInfo.RegEx;
                        objProblemsproperties.PartnerCode = objProblemsInfo.PartnerCode;
                        objProblemsproperties.CreatedBy = userCredentials.id;
                        objProblemsproperties.CreatedOn = time;
                        objProblemsproperties.UpdatedBy = userCredentials.id;
                        objProblemsproperties.UpdatedOn = time;
                        objProblemsproperties.Title = objProblemsInfo.Title;
                        objProblemsproperties.ProblemsRefId = objProblemsInfo.ProblemsRefId;
                        return Json(QuestionLogicRepository.UpdateFlexProblems(objProblemsproperties));
                    }
                    else
                    {
                        return Json(new { error = "Problems Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteFlexProblems(int ProblemsID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteFlexProblems(ProblemsID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetFlexSignaturesForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetFlexSignaturesForActivities(layOutRefID, activitiyRefID, QuesstionRefID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetFlexDocumentScannerForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetFlexDocumentScannerForActivities(layOutRefID, activitiyRefID, QuesstionRefID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult InsertFlexSignatures(int questionidSignatures, string Signatures)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    Signatures objSignatures = JsonConvert.DeserializeObject<Signatures>(WebUtility.UrlDecode(Signatures));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objSignatures.QuestionRefID);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objSignatures.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = objSignatures.LayoutRefID;
                        objQuestion.Name = objSignatures.Title;
                        objQuestion.ParentId = questionidSignatures;
                        objQuestion.TransporttypeRefId = 0;
                        objQuestion.PropertyName = "Signaturep";
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);


                        Signatures objSignaturesproperties = new Signatures();
                        objSignaturesproperties.ActivityRefID = objSignatures.ActivityRefID;
                        objSignaturesproperties.LayoutRefID = objSignatures.LayoutRefID;
                        objSignaturesproperties.QuestionRefID = objSignatures.QuestionRefID;
                        objSignaturesproperties.PlaceLevel = objSignatures.PlaceLevel;
                        objSignaturesproperties.JobLevel = objSignatures.JobLevel;
                        objSignaturesproperties.CreatedBy = userCredentials.id;
                        objSignaturesproperties.CreatedOn = time;
                        objSignaturesproperties.Title = objSignatures.Title;
                        objSignaturesproperties.SignaturesRefId = questionsresult.QuestionId;
                        return Json(QuestionLogicRepository.InsertFlexSignatures(objSignaturesproperties));
                    }
                    else
                    {
                        return Json(new { error = "Signature Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult InsertFlexDocumentScanner(int questionidDocumentScanner, string DocumentScanner)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    DocumentScanner objDocumentScanner = JsonConvert.DeserializeObject<DocumentScanner>(WebUtility.UrlDecode(DocumentScanner));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objDocumentScanner.QuestionRefID ?? 0);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objDocumentScanner.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = objDocumentScanner.LayoutRefID;
                        objQuestion.Name = objDocumentScanner.Title;
                        objQuestion.ParentId = questionidDocumentScanner;
                        objQuestion.TransporttypeRefId = 0;
                        objQuestion.PropertyName = "DocumentScannerp";
                        objQuestion.ISModification = 0;
                        objQuestion.IsFlex = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);


                        DocumentScanner objDocumentScannerproperties = new DocumentScanner();
                        objDocumentScannerproperties.ActivityRefID = objDocumentScanner.ActivityRefID;
                        objDocumentScannerproperties.LayoutRefID = objDocumentScanner.LayoutRefID;
                        objDocumentScannerproperties.QuestionRefID = objDocumentScanner.QuestionRefID;
                        objDocumentScannerproperties.PlaceLevel = objDocumentScanner.PlaceLevel;
                        objDocumentScannerproperties.JobLevel = objDocumentScanner.JobLevel;
                        objDocumentScannerproperties.ProductLevel = objDocumentScanner.ProductLevel;
                        objDocumentScannerproperties.DocumentType = objDocumentScanner.DocumentType;
                        objDocumentScannerproperties.CreatedBy = userCredentials.id;
                        objDocumentScannerproperties.CreatedOn = time;
                        objDocumentScannerproperties.Title = objDocumentScanner.Title;
                        objDocumentScannerproperties.DocumentScannerRefId = questionsresult.QuestionId;
                        return Json(QuestionLogicRepository.InsertFlexDocumentScanner(objDocumentScannerproperties));
                    }
                    else
                    {
                        return Json(new { error = "Document Scanner Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateFlexSignatures(string Signatures)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    Signatures objSignatures = JsonConvert.DeserializeObject<Signatures>(WebUtility.UrlDecode(Signatures));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objSignatures.QuestionRefID);

                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = objSignatures.SignaturesRefId ?? 0;
                    objQuestions.Name = objSignatures.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Signatures objSignaturesproperties = new Signatures();
                        objSignaturesproperties.SignaturesID = objSignatures.SignaturesID;
                        objSignaturesproperties.ActivityRefID = objSignatures.ActivityRefID;
                        objSignaturesproperties.LayoutRefID = objSignatures.LayoutRefID;
                        objSignaturesproperties.QuestionRefID = objSignatures.QuestionRefID;
                        objSignaturesproperties.PlaceLevel = objSignatures.PlaceLevel;
                        objSignaturesproperties.JobLevel = objSignatures.JobLevel;
                        objSignaturesproperties.CreatedBy = userCredentials.id;
                        objSignaturesproperties.CreatedOn = time;
                        objSignaturesproperties.UpdatedBy = userCredentials.id;
                        objSignaturesproperties.UpdatedOn = time;
                        objSignaturesproperties.Title = objSignatures.Title;
                        objSignaturesproperties.SignaturesRefId = objSignatures.SignaturesRefId;
                        return Json(QuestionLogicRepository.UpdateFlexSignatures(objSignaturesproperties));
                    }
                    else
                    {
                        return Json(new { error = "Signature Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteFlexSignatures(int SignaturesID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteFlexSignatures(SignaturesID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateFlexDocumentScanner(string DocumentScanner)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    DocumentScanner objDocumentScanner = JsonConvert.DeserializeObject<DocumentScanner>(WebUtility.UrlDecode(DocumentScanner));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objDocumentScanner.QuestionRefID ?? 0);

                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = objDocumentScanner.DocumentScannerRefId ?? 0;
                    objQuestions.Name = objDocumentScanner.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        DocumentScanner objDocumentScannerproperties = new DocumentScanner();
                        objDocumentScannerproperties.DocumentScannerID = objDocumentScanner.DocumentScannerID;
                        objDocumentScannerproperties.ActivityRefID = objDocumentScanner.ActivityRefID;
                        objDocumentScannerproperties.LayoutRefID = objDocumentScanner.LayoutRefID;
                        objDocumentScannerproperties.QuestionRefID = objDocumentScanner.QuestionRefID;
                        objDocumentScannerproperties.PlaceLevel = objDocumentScanner.PlaceLevel;
                        objDocumentScannerproperties.JobLevel = objDocumentScanner.JobLevel;
                        objDocumentScannerproperties.ProductLevel = objDocumentScanner.ProductLevel;
                        objDocumentScannerproperties.DocumentType = objDocumentScanner.DocumentType;
                        objDocumentScannerproperties.CreatedBy = userCredentials.id;
                        objDocumentScannerproperties.CreatedOn = time;
                        objDocumentScannerproperties.UpdatedBy = userCredentials.id;
                        objDocumentScannerproperties.UpdatedOn = time;
                        objDocumentScannerproperties.Title = objDocumentScanner.Title;
                        objDocumentScannerproperties.DocumentScannerRefId = objDocumentScanner.DocumentScannerRefId;

                        return Json(QuestionLogicRepository.UpdateFlexDocumentScanner(objDocumentScannerproperties));
                    }
                    else
                    {
                        return Json(new { error = "Document Scanner Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteFlexDocumentScanner(int DocumentScannerID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteFlexDocumentScanner(DocumentScannerID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetFlexPicureForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetFlexPicureForActivities(layOutRefID, activitiyRefID, QuesstionRefID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult InsertFlexPicure(string Picure)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    Picure objPicure = JsonConvert.DeserializeObject<Picure>(WebUtility.UrlDecode(Picure));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objPicure.QuestionRefID);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Picure objPicureproperties = new Picure();
                        objPicureproperties.ActivityRefID = objPicure.ActivityRefID;
                        objPicureproperties.LayoutRefID = objPicure.LayoutRefID;
                        objPicureproperties.QuestionRefID = objPicure.QuestionRefID;

                        objPicureproperties.CreatedBy = userCredentials.id;
                        objPicureproperties.CreatedOn = time;
                        objPicureproperties.Title = objPicure.Title;
                        objPicureproperties.AlternativetextonJoblevel = objPicure.AlternativetextonJoblevel;
                        objPicureproperties.AlternativetextonPlacelevel = objPicure.AlternativetextonPlacelevel;
                        objPicureproperties.Alternativetextonproductlevel = objPicure.Alternativetextonproductlevel;
                        return Json(QuestionLogicRepository.InsertFlexPicure(objPicureproperties));
                    }
                    else
                    {
                        return Json(new { error = "Picture Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateFlexPicure(string Picure)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    Picure objPicure = JsonConvert.DeserializeObject<Picure>(WebUtility.UrlDecode(Picure));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objPicure.QuestionRefID);

                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = objPicure.QuestionRefID;
                    objQuestions.Name = objPicure.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Picure objPicureproperties = new Picure();
                        objPicureproperties.PicureID = objPicure.PicureID;
                        objPicureproperties.ActivityRefID = objPicure.ActivityRefID;
                        objPicureproperties.LayoutRefID = objPicure.LayoutRefID;
                        objPicureproperties.QuestionRefID = objPicure.QuestionRefID;

                        objPicureproperties.CreatedBy = userCredentials.id;
                        objPicureproperties.CreatedOn = time;
                        objPicureproperties.Title = objPicure.Title;
                        objPicureproperties.AlternativetextonJoblevel = objPicure.AlternativetextonJoblevel;
                        objPicureproperties.AlternativetextonPlacelevel = objPicure.AlternativetextonPlacelevel;
                        objPicureproperties.Alternativetextonproductlevel = objPicure.Alternativetextonproductlevel;
                        return Json(QuestionLogicRepository.UpdateFlexPicure(objPicureproperties));
                    }
                    else
                    {
                        return Json(new { error = "Picture Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteFlexPicure(int PicureID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteFlexPicure(PicureID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        /// Flex End
        /// 

        public ActionResult GetCurrentActivitiesRegistration(int activityRegistrationID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetCurrentActivitiesRegistration(activityRegistrationID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetPlanningFeedbackCode(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetPlanningFeedbackCode(LayoutId));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetChoicePlanningFeedbackCode(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetChoicePlanningFeedbackCode(LayoutId));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetSetActionValueType(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var setActionTypeList = QuestionLogicRepository.GetSetActionValueType(layoutId);
                    return Json(setActionTypeList);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetAliasNameDetailsByLayout(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetAliasNameDetailsByLayout(layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetEmptyFullSoloByActivitiesRegistration(int ActivityId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetEmptyFullSoloByActivitiesRegistration(ActivityId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetTransportTypeByActivitiesRegistration(int ActivityId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetTransportTypeByActivitiesRegistration(ActivityId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }


        public ActionResult UpdateExtraInfoProperties(string ExtraInfoProperties, int pExtraInfoPropertiesnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    ExtraInfoProperties objExtraInfoProperties = JsonConvert.DeserializeObject<ExtraInfoProperties>(WebUtility.UrlDecode(ExtraInfoProperties));
                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = pExtraInfoPropertiesnodeid;
                    objQuestions.Name = objExtraInfoProperties.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    ExtraInfoProperties objExtraInfoProp = new ExtraInfoProperties();
                    objExtraInfoProp.ExtraInfoPropertiesID = objExtraInfoProperties.ExtraInfoPropertiesID;
                    objExtraInfoProp.Title = objExtraInfoProperties.Title;
                    objExtraInfoProp.AlternativetextonJoblevel = objExtraInfoProperties.AlternativetextonJoblevel;
                    objExtraInfoProp.AlternativetextonPlacelevel = objExtraInfoProperties.AlternativetextonPlacelevel;
                    objExtraInfoProp.Alternativetextonproductlevel = objExtraInfoProperties.Alternativetextonproductlevel;
                    objExtraInfoProp.Updatedby = userCredentials.id;
                    objExtraInfoProp.UpdatedOn = time;
                    var ExtraInfoProp = QuestionLogicRepository.UpdateExtraInfoProperties(objExtraInfoProp);
                    return Json("Updated");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult UpdatePalletsProperties(string PalletsProperties, int pPalletsPropertiesnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    PalletsProperties objPalletsProperties = JsonConvert.DeserializeObject<PalletsProperties>(WebUtility.UrlDecode(PalletsProperties));
                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = pPalletsPropertiesnodeid;
                    objQuestions.Name = objPalletsProperties.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    PalletsProperties objPalletsProp = new PalletsProperties();
                    objPalletsProp.PalletsPropertiesID = objPalletsProperties.PalletsPropertiesID;
                    objPalletsProp.Title = objPalletsProperties.Title;
                    objPalletsProp.AlternativetextonJoblevel = objPalletsProperties.AlternativetextonJoblevel;
                    objPalletsProp.AlternativetextonPlacelevel = objPalletsProperties.AlternativetextonPlacelevel;
                    objPalletsProp.Alternativetextonproductlevel = objPalletsProperties.Alternativetextonproductlevel;
                    objPalletsProp.UpdatedBy = userCredentials.id;
                    objPalletsProp.UpdatedOn = time;
                    var Pallets = QuestionLogicRepository.UpdatePalletsProperties(objPalletsProp);
                    return Json("Updated");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult UpdateProblemsProperties(string ProblemsProperties, int pProblemsPropertiesnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    ProblemsProperties objProblemsProperties = JsonConvert.DeserializeObject<ProblemsProperties>(WebUtility.UrlDecode(ProblemsProperties));
                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = pProblemsPropertiesnodeid;
                    objQuestions.Name = objProblemsProperties.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    ProblemsProperties objProblemsProp = new ProblemsProperties();
                    objProblemsProp.ProblemsPropertiesID = objProblemsProperties.ProblemsPropertiesID;
                    objProblemsProp.Title = objProblemsProperties.Title;
                    objProblemsProp.AlternativetextonJoblevel = objProblemsProperties.AlternativetextonJoblevel;
                    objProblemsProp.AlternativetextonPlacelevel = objProblemsProperties.AlternativetextonPlacelevel;
                    objProblemsProp.Alternativetextonproductlevel = objProblemsProperties.Alternativetextonproductlevel;
                    objProblemsProp.UpdatedBy = userCredentials.id;
                    objProblemsProp.UpdatedOn = time;
                    var Pallets = QuestionLogicRepository.UpdateProblemsProperties(objProblemsProp);
                    return Json("Updated");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult UpdateSignaturesProperties(string SignaturesProperties, int pSignaturesPropertiesnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    SignaturesProperties objSignaturesProperties = JsonConvert.DeserializeObject<SignaturesProperties>(WebUtility.UrlDecode(SignaturesProperties));
                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = pSignaturesPropertiesnodeid;
                    objQuestions.Name = objSignaturesProperties.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    SignaturesProperties objSignaturesProp = new SignaturesProperties();
                    objSignaturesProp.SignaturesPropertiesID = objSignaturesProperties.SignaturesPropertiesID;
                    objSignaturesProp.Title = objSignaturesProperties.Title;
                    objSignaturesProp.AlternativetextonJoblevel = objSignaturesProperties.AlternativetextonJoblevel;
                    objSignaturesProp.AlternativetextonPlacelevel = objSignaturesProperties.AlternativetextonPlacelevel;
                    objSignaturesProp.Alternativetextonproductlevel = objSignaturesProperties.Alternativetextonproductlevel;
                    objSignaturesProp.PlaceLevel = objSignaturesProperties.PlaceLevel;
                    objSignaturesProp.JobLevel = objSignaturesProperties.JobLevel;
                    objSignaturesProp.UpdatedBy = userCredentials.id;
                    objSignaturesProp.UpdatedOn = time;
                    var Pallets = QuestionLogicRepository.UpdateSignaturesProperties(objSignaturesProp);
                    return Json("Updated");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdatePictureProperties(string PictureProperties, int pPicturePropertiesnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    PictureProperties objPictureProperties = JsonConvert.DeserializeObject<PictureProperties>(WebUtility.UrlDecode(PictureProperties));
                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = pPicturePropertiesnodeid;
                    objQuestions.Name = objPictureProperties.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    PictureProperties objPictureProp = new PictureProperties();
                    objPictureProp.PicturePropertiesID = objPictureProperties.PicturePropertiesID;
                    objPictureProp.Title = objPictureProperties.Title;
                    objPictureProp.AlternativetextonJoblevel = objPictureProperties.AlternativetextonJoblevel;
                    objPictureProp.AlternativetextonPlacelevel = objPictureProperties.AlternativetextonPlacelevel;
                    objPictureProp.Alternativetextonproductlevel = objPictureProperties.Alternativetextonproductlevel;
                    objPictureProp.UpdatedBy = userCredentials.id;
                    objPictureProp.UpdatedOn = time;
                    var Pallets = QuestionLogicRepository.UpdatePictureProperties(objPictureProp);
                    return Json("Updated");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        //public ActionResult UpdatePictureProperties(string picTitle, int pPicturenodeid)
        //{
        //    if (IsServerConnected(connectionStrings))
        //    {
        //        try
        //        {
        //            DateTime time = DateTime.Now;
        //            var userCredentials = GetUserCredentials();
        //            boQuestion objQuestions = new boQuestion();
        //            objQuestions.QuestionId = pPicturenodeid;
        //            objQuestions.Name = picTitle;
        //            objQuestions.UpdatedBy = userCredentials.id;
        //            objQuestions.UpdatedOn = time;
        //            QuestionLogicRepository.UpdateQuestions(objQuestions);
        //            return Json("Updated");
        //        }
        //        catch
        //        {
        //            return Json(new { error = GetStatusMessage(StatusCode.E02) });
        //        }
        //    }
        //    else
        //        return Json(new
        //        {
        //            error = GetStatusMessage(StatusCode.E01)
        //        });

        //}

        public ActionResult UpdateDocumentScannerProperties(string DocumentScannerProperties, int pDocumentScannerPropertiesnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    DocumentScannerProperties objDocumentScannerProperties = JsonConvert.DeserializeObject<DocumentScannerProperties>(WebUtility.UrlDecode(DocumentScannerProperties));
                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = pDocumentScannerPropertiesnodeid;
                    objQuestions.Name = objDocumentScannerProperties.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    DocumentScannerProperties objDocumentScannerProp = new DocumentScannerProperties();
                    objDocumentScannerProp.DocumentScannerPropertiesID = objDocumentScannerProperties.DocumentScannerPropertiesID;
                    objDocumentScannerProp.Title = objDocumentScannerProperties.Title;
                    objDocumentScannerProp.AlternativetextonJoblevel = objDocumentScannerProperties.AlternativetextonJoblevel;
                    objDocumentScannerProp.AlternativetextonPlacelevel = objDocumentScannerProperties.AlternativetextonPlacelevel;
                    objDocumentScannerProp.Alternativetextonproductlevel = objDocumentScannerProperties.Alternativetextonproductlevel;
                    objDocumentScannerProp.UpdatedBy = userCredentials.id;
                    objDocumentScannerProp.UpdatedOn = time;
                    var Pallets = QuestionLogicRepository.UpdateDocumentScannerProperties(objDocumentScannerProp);
                    return Json("Updated");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult GetExtraInfoProperties(int questionRefId, int layoutRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetExtraInfoProperties(questionRefId, layoutRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult GetPalletsProperties(int questionRefId, int layoutRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetPalletsProperties(questionRefId, layoutRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult GetProblemsProperties(int questionRefId, int layoutRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetProblemsProperties(questionRefId, layoutRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult GetSignaturesProperties(int questionRefId, int layoutRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetSignaturesProperties(questionRefId, layoutRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult GetPicure(int questionRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetPicure(questionRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetPictureProperties(int questionRefId, int layoutRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetPictureProperties(questionRefId, layoutRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetDocumentScannerProperties(int questionRefId, int layoutRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetDocumentScannerProperties(questionRefId, layoutRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }


        public ActionResult GetSmartExtraInfoForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetSmartExtraInfoForActivities(layOutRefID, activitiyRefID, QuesstionRefID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult UpdateSmartQuestionPath(string title, int nodeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = nodeId;
                    objQuestions.Name = title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    return Json(nodeId);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult InsertSmartExtraInfo(int questionidExtra, string ExtraInfo)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    SmartExtraInfo objSmartExtraInfo = JsonConvert.DeserializeObject<SmartExtraInfo>(WebUtility.UrlDecode(ExtraInfo));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objSmartExtraInfo.QuestionRefID);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objSmartExtraInfo.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = objSmartExtraInfo.LayoutRefID;
                        objQuestion.Name = objSmartExtraInfo.Title;
                        objQuestion.ParentId = questionidExtra;
                        objQuestion.TransporttypeRefId = 0;
                        objQuestion.PropertyName = "Smart Extra Infop";
                        objQuestion.ISModification = 0;
                        objQuestion.IsSmart = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);


                        SmartExtraInfo objSmartExtraInfoproperties = new SmartExtraInfo();
                        objSmartExtraInfoproperties.ActivityRefID = objSmartExtraInfo.ActivityRefID;
                        objSmartExtraInfoproperties.LayoutRefID = objSmartExtraInfo.LayoutRefID;
                        objSmartExtraInfoproperties.QuestionRefID = objSmartExtraInfo.QuestionRefID;
                        objSmartExtraInfoproperties.SmartExtraInfoRefID = questionsresult.QuestionId;
                        objSmartExtraInfoproperties.Size = objSmartExtraInfo.Size;
                        objSmartExtraInfoproperties.RegEx = objSmartExtraInfo.RegEx;
                        objSmartExtraInfoproperties.Partnercode = objSmartExtraInfo.Partnercode;
                        objSmartExtraInfoproperties.PlaceLevel = objSmartExtraInfo.PlaceLevel;
                        objSmartExtraInfoproperties.PlanningFeedbackcode = objSmartExtraInfo.PlanningFeedbackcode;
                        objSmartExtraInfoproperties.ProductLevel = objSmartExtraInfo.ProductLevel;
                        objSmartExtraInfoproperties.JobLevel = objSmartExtraInfo.JobLevel;
                        objSmartExtraInfoproperties.Format = objSmartExtraInfo.Format;
                        objSmartExtraInfoproperties.CreatedBy = userCredentials.id;
                        objSmartExtraInfoproperties.CreatedOn = time;
                        objSmartExtraInfoproperties.Title = objSmartExtraInfo.Title;
                        return Json(QuestionLogicRepository.InsertSmartExtraInfo(objSmartExtraInfoproperties));
                    }
                    else
                    {
                        return Json(new { error = "Extra Info Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateSmartExtraInfo(string ExtraInfo)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {

                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    SmartExtraInfo objSmartExtraInfo = JsonConvert.DeserializeObject<SmartExtraInfo>(WebUtility.UrlDecode(ExtraInfo));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objSmartExtraInfo.QuestionRefID);

                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = objSmartExtraInfo.SmartExtraInfoRefID ?? 0;
                    objQuestions.Name = objSmartExtraInfo.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        SmartExtraInfo objSmartExtraInfosproperties = new SmartExtraInfo();
                        objSmartExtraInfosproperties.SmartExtraInfoID = objSmartExtraInfo.SmartExtraInfoID;
                        objSmartExtraInfosproperties.ActivityRefID = objSmartExtraInfo.ActivityRefID;
                        objSmartExtraInfosproperties.LayoutRefID = objSmartExtraInfo.LayoutRefID;
                        objSmartExtraInfosproperties.QuestionRefID = objSmartExtraInfo.QuestionRefID;
                        objSmartExtraInfosproperties.Size = objSmartExtraInfo.Size;
                        objSmartExtraInfosproperties.RegEx = objSmartExtraInfo.RegEx;
                        objSmartExtraInfosproperties.Partnercode = objSmartExtraInfo.Partnercode;
                        objSmartExtraInfosproperties.PlaceLevel = objSmartExtraInfo.PlaceLevel;
                        objSmartExtraInfosproperties.PlanningFeedbackcode = objSmartExtraInfo.PlanningFeedbackcode;
                        objSmartExtraInfosproperties.ProductLevel = objSmartExtraInfo.ProductLevel;
                        objSmartExtraInfosproperties.JobLevel = objSmartExtraInfo.JobLevel;
                        objSmartExtraInfosproperties.Format = objSmartExtraInfo.Format;
                        objSmartExtraInfosproperties.CreatedBy = userCredentials.id;
                        objSmartExtraInfosproperties.CreatedOn = time;
                        objSmartExtraInfosproperties.Title = objSmartExtraInfo.Title;
                        objSmartExtraInfosproperties.SmartExtraInfoRefID = objSmartExtraInfo.SmartExtraInfoRefID;
                        return Json(QuestionLogicRepository.UpdateSmartExtraInfo(objSmartExtraInfosproperties));
                    }
                    else
                    {
                        return Json(new { error = "Extra Info Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteSmartExtraInfo(int ExtraInfoID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteSmartExtraInfo(ExtraInfoID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetSmartPalletsForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetSmartPalletsForActivities(layOutRefID, activitiyRefID, QuesstionRefID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult InsertSmartPallets(int questionidPallet, string Pallets)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    SmartPallets objSmartPallets = JsonConvert.DeserializeObject<SmartPallets>(WebUtility.UrlDecode(Pallets));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objSmartPallets.QuestionRefID);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objSmartPallets.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = objSmartPallets.LayoutRefID;
                        objQuestion.Name = objSmartPallets.Title;
                        objQuestion.ParentId = questionidPallet;
                        objQuestion.TransporttypeRefId = 0;
                        objQuestion.PropertyName = "Smart Palletsp";
                        objQuestion.ISModification = 0;
                        objQuestion.IsSmart = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        SmartPallets objSmartPalletsproperties = new SmartPallets();
                        objSmartPalletsproperties.ActivityRefID = objSmartPallets.ActivityRefID;
                        objSmartPalletsproperties.LayoutRefID = objSmartPallets.LayoutRefID;
                        objSmartPalletsproperties.QuestionRefID = objSmartPallets.QuestionRefID;
                        objSmartPalletsproperties.Partnercode = objSmartPallets.Partnercode;
                        objSmartPalletsproperties.PlaceLevel = objSmartPallets.PlaceLevel;
                        objSmartPalletsproperties.PlanningFeedbackcode = objSmartPallets.PlanningFeedbackcode;
                        objSmartPalletsproperties.ProductLevel = objSmartPallets.ProductLevel;
                        objSmartPalletsproperties.JobLevel = objSmartPallets.JobLevel;
                        objSmartPalletsproperties.CreatedBy = userCredentials.id;
                        objSmartPalletsproperties.CreatedOn = time;
                        objSmartPalletsproperties.Title = objSmartPallets.Title;
                        objSmartPalletsproperties.SmartPalletsRefID = questionsresult.QuestionId;
                        return Json(QuestionLogicRepository.InsertSmartPallets(objSmartPalletsproperties));
                    }
                    else
                    {
                        return Json(new { error = "Pallets Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateSmartPallets(string Pallets)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    SmartPallets objSmartPallets = JsonConvert.DeserializeObject<SmartPallets>(WebUtility.UrlDecode(Pallets));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objSmartPallets.QuestionRefID);

                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = objSmartPallets.SmartPalletsRefID ?? 0;
                    objQuestions.Name = objSmartPallets.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        SmartPallets objSmartPalletsproperties = new SmartPallets();
                        objSmartPalletsproperties.SmartPalletsID = objSmartPallets.SmartPalletsID;
                        objSmartPalletsproperties.ActivityRefID = objSmartPallets.ActivityRefID;
                        objSmartPalletsproperties.LayoutRefID = objSmartPallets.LayoutRefID;
                        objSmartPalletsproperties.QuestionRefID = objSmartPallets.QuestionRefID;
                        objSmartPalletsproperties.Partnercode = objSmartPallets.Partnercode;
                        objSmartPalletsproperties.PlaceLevel = objSmartPallets.PlaceLevel;
                        objSmartPalletsproperties.PlanningFeedbackcode = objSmartPallets.PlanningFeedbackcode;
                        objSmartPalletsproperties.ProductLevel = objSmartPallets.ProductLevel;
                        objSmartPalletsproperties.JobLevel = objSmartPallets.JobLevel;
                        objSmartPalletsproperties.Updatedby = userCredentials.id;
                        objSmartPalletsproperties.UpdatedOn = time;
                        objSmartPalletsproperties.Title = objSmartPallets.Title;
                        objSmartPalletsproperties.SmartPalletsRefID = objSmartPallets.SmartPalletsRefID;
                        return Json(QuestionLogicRepository.UpdateSmartPallets(objSmartPalletsproperties));
                    }
                    else
                    {
                        return Json(new { error = "Pallets Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteSmartPallets(int PalletsID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteSmartPallets(PalletsID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetSmartProblemsForActivities(int layOutRefID, int activitiyRefID, int QuesstionRefID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetSmartProblemsForActivities(layOutRefID, activitiyRefID, QuesstionRefID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult InsertSmartProblems(int questionidProblem, string Problems)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    SmartProblems objSmartProblems = JsonConvert.DeserializeObject<SmartProblems>(WebUtility.UrlDecode(Problems));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objSmartProblems.QuestionRefID);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = objSmartProblems.ActivityRefID;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = objSmartProblems.LayoutRefID;
                        objQuestion.Name = objSmartProblems.Title;
                        objQuestion.ParentId = questionidProblem;
                        objQuestion.TransporttypeRefId = 0;
                        objQuestion.PropertyName = "Smart Problemp";
                        objQuestion.ISModification = 0;
                        objQuestion.IsSmart = true;
                        objQuestion.IsParent = true;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = false;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);


                        SmartProblems objSmartProblemsproperties = new SmartProblems();
                        objSmartProblemsproperties.ActivityRefID = objSmartProblems.ActivityRefID;
                        objSmartProblemsproperties.LayoutRefID = objSmartProblems.LayoutRefID;
                        objSmartProblemsproperties.QuestionRefID = objSmartProblems.QuestionRefID;
                        objSmartProblemsproperties.Size = objSmartProblems.Size;
                        objSmartProblemsproperties.RegEx = objSmartProblems.RegEx;
                        objSmartProblemsproperties.Partnercode = objSmartProblems.Partnercode;
                        objSmartProblemsproperties.PlaceLevel = objSmartProblems.PlaceLevel;
                        objSmartProblemsproperties.PlanningFeedbackcode = objSmartProblems.PlanningFeedbackcode;
                        objSmartProblemsproperties.ProductLevel = objSmartProblems.ProductLevel;
                        objSmartProblemsproperties.JobLevel = objSmartProblems.JobLevel;
                        objSmartProblemsproperties.Format = objSmartProblems.Format;
                        objSmartProblemsproperties.CreatedBy = userCredentials.id;
                        objSmartProblemsproperties.CreatedOn = time;
                        objSmartProblemsproperties.Title = objSmartProblems.Title;
                        objSmartProblemsproperties.SmartProblemsRefID = questionsresult.QuestionId;
                        return Json(QuestionLogicRepository.InsertSmartProblems(objSmartProblemsproperties));
                    }
                    else
                    {
                        return Json(new { error = "Problems Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateSmartProblems(string Problems)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    SmartProblems objSmartProblems = JsonConvert.DeserializeObject<SmartProblems>(WebUtility.UrlDecode(Problems));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objSmartProblems.QuestionRefID);

                    boQuestion objQuestions = new boQuestion();
                    objQuestions.QuestionId = objSmartProblems.SmartProblemsRefID ?? 0;
                    objQuestions.Name = objSmartProblems.Title;
                    objQuestions.UpdatedBy = userCredentials.id;
                    objQuestions.UpdatedOn = time;
                    QuestionLogicRepository.UpdateQuestions(objQuestions);

                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        SmartProblems objSmartProblemsproperties = new SmartProblems();
                        objSmartProblemsproperties.SmartProblemsID = objSmartProblems.SmartProblemsID;
                        objSmartProblemsproperties.ActivityRefID = objSmartProblems.ActivityRefID;
                        objSmartProblemsproperties.LayoutRefID = objSmartProblems.LayoutRefID;
                        objSmartProblemsproperties.QuestionRefID = objSmartProblems.QuestionRefID;
                        objSmartProblemsproperties.Size = objSmartProblems.Size;
                        objSmartProblemsproperties.RegEx = objSmartProblems.RegEx;
                        objSmartProblemsproperties.Partnercode = objSmartProblems.Partnercode;
                        objSmartProblemsproperties.PlaceLevel = objSmartProblems.PlaceLevel;
                        objSmartProblemsproperties.PlanningFeedbackcode = objSmartProblems.PlanningFeedbackcode;
                        objSmartProblemsproperties.ProductLevel = objSmartProblems.ProductLevel;
                        objSmartProblemsproperties.JobLevel = objSmartProblems.JobLevel;
                        objSmartProblemsproperties.Format = objSmartProblems.Format;
                        objSmartProblemsproperties.CreatedBy = userCredentials.id;
                        objSmartProblemsproperties.CreatedOn = time;
                        objSmartProblemsproperties.Title = objSmartProblems.Title;
                        objSmartProblemsproperties.SmartProblemsRefID = objSmartProblems.SmartProblemsRefID;
                        return Json(QuestionLogicRepository.UpdateSmartProblems(objSmartProblemsproperties));
                    }
                    else
                    {
                        return Json(new { error = "Problems Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteSmartProblems(int ProblemsID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteSmartProblems(ProblemsID));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetCopyPaste()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    return Json(QuestionLogicRepository.GetCopyPaste(userCredentials.id));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult InsertCopyPaste(string CopyPaste)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    QuestionLogicRepository.DeleteCopyPaste(userCredentials.id);
                    CopyPaste objCopyPastes = new CopyPaste();
                    objCopyPastes.CopiedItem = CopyPaste;
                    objCopyPastes.CreatedBy = userCredentials.id;
                    objCopyPastes.CreatedOn = time;
                    return Json(QuestionLogicRepository.InsertCopyPaste(objCopyPastes));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult DeleteCopyPaste()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    return Json(QuestionLogicRepository.DeleteCopyPaste(userCredentials.id));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult Pastecopiedquestions(int layoutid, int activityid, int transportid, int selectedNode)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    var questions = QuestionLogicRepository.GetCopyPaste(userCredentials.id);
                    var QuseiontsIds = questions.Split(",").ToList().Select(s => Int32.Parse(s.ToString())).ToList();
                    var result = _copyLayoutRepository.SaveQuestionPath(0, 0, 0, QuseiontsIds, layoutid, activityid, transportid, selectedNode, userCredentials.id, "MC");

                    return Json(selectedNode);
                }
                catch (Exception ex)
                {
                    return Json(new { error = "Process Failed. Some Questions Not Saved Properly, Please Try Again" });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult LayoutInfoColumnDetails(int layouid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var customerid = QuestionLogicRepository.GetLayoutDetails(layouid);
                    List<InfocolumnList> List = CustomerLogicRepository.GetInfoColumnQuestionpath(customerid.CustomerRefId).ToList();
                    var infodetails =
                       (
                            from n in List
                            select new
                            {
                                n.IC_ID,
                                n.CustomerRefId,
                                ICM_Column_Name = (n.ICM_Column_Name == "Not set" ? GetInfoColumnName(n.IC_ID) : n.ICM_Column_Name)
                            }
                        ).ToList();
                    return Json(infodetails);

                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public string GetInfoColumnName(int InfoColumnId)
        {
            var InfoColumnName = "";
            if (InfoColumnId == 1)
                InfoColumnName = "Info 1";
            else if (InfoColumnId == 2 || InfoColumnId == 6)
                InfoColumnName = "Info 2";
            else if (InfoColumnId == 3)
                InfoColumnName = "Info 3";
            else if (InfoColumnId == 4)
                InfoColumnName = "Info 4";
            else if (InfoColumnId == 5)
                InfoColumnName = "Info 5";
            return InfoColumnName;
        }

        public ActionResult GetLayoutforCopy(int CustomerRefId, int OBCTypeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    var isCustomerLogin = IsCustomerLogin(userCredentials.permissions);

                    var result = QuestionLogicRepository.GetLayoutforCopy(CustomerRefId, OBCTypeId, isCustomerLogin, userCredentials.company);

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }


        public ActionResult GetLayoutforCopywithoutCustomer()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = QuestionLogicRepository.GetLayoutforCopywithoutCustomer();

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetActivitieslistCopy(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = QuestionLogicRepository.GetActivitieslistCopy(LayoutId);

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetActivitiesList(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<ActivitiesRegistration> activitiesList = new List<ActivitiesRegistration>();
                    var result = QuestionLogicRepository.GetActivitiesList(LayoutId);
                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetTransporttypelistCopy(int LayoutId, int activityid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = QuestionLogicRepository.GetTransporttypelistCopy(LayoutId, activityid);

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        [HttpPost]
        public ActionResult Session(string nodes)
        {
            List<PositionedQuestions> sessionList = new List<PositionedQuestions>();
            if (HttpContext.Session.GetComplexData<List<PositionedQuestions>>("PositionedQuestions") != null)
                sessionList = HttpContext.Session.GetComplexData<List<PositionedQuestions>>("PositionedQuestions");
            List<PositionedQuestions> questns = JsonConvert.DeserializeObject<List<PositionedQuestions>>(nodes);
            sessionList.AddRange(questns);

            HttpContext.Session.SetComplexData("PositionedQuestions", sessionList);
            return Json(new { success = "success" });
        }

        [HttpPost]
        public ActionResult UpdateTreeViewNodes()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<PositionedQuestions> questns = HttpContext.Session.GetComplexData<List<PositionedQuestions>>("PositionedQuestions");
                    string result = QuestionLogicRepository.UpdateTreeViewDraggingNodes(questns);
                    //if (result == "Updated")
                    //{
                    List<PositionedQuestions> sessionList = new List<PositionedQuestions>();
                    HttpContext.Session.SetComplexData("PositionedQuestions", sessionList);
                    //}

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        //selvam
        public ActionResult GetCehckOffPlanningLevel(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = QuestionLogicRepository.GetCehckOffPlanningLevel(LayoutId);

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetAllDocumentType()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = QuestionLogicRepository.GetAllDocumentType();

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetAddDocumentScanProperties(int DocumentScanTitleId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetAddDocumentScanProperties(DocumentScanTitleId));
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult SaveAddDocumentScanProperties(int layoutId, int activityId, int transportTypeId, int questioniddocumentscan, int questionid, string AddDocumentScan)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    DocumentScanType objAddDocumentProperties = JsonConvert.DeserializeObject<DocumentScanType>(WebUtility.UrlDecode(AddDocumentScan));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(questioniddocumentscan);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        Questions objQuestion = new Questions();
                        objQuestion.ActivitiesRefId = activityId;
                        objQuestion.CreatedBy = userCredentials.id;
                        objQuestion.CreatedOn = time;
                        objQuestion.LayoutRefId = layoutId;
                        objQuestion.Name = objAddDocumentProperties.DocumentTypeTitle;
                        objQuestion.ParentId = questionid;
                        objQuestion.TransporttypeRefId = transportTypeId;
                        objQuestion.PropertyName = "Add Document Scan";
                        objQuestion.ISModification = 0;
                        objQuestion.IsQuestionPath = true;
                        objQuestion.IsParent = false;
                        objQuestion.IsDefault = true;
                        objQuestion.IsChild = true;
                        var questionsresult = QuestionLogicRepository.SaveQuestions(objQuestion);

                        DocumentScanType objAddDocumentScan = new DocumentScanType();
                        objAddDocumentScan.DocumentTypeTitle = objAddDocumentProperties.DocumentTypeTitle;
                        objAddDocumentScan.DocumentType = objAddDocumentProperties.DocumentType;
                        objAddDocumentScan.DocumentScanTitleRefId = questioniddocumentscan;
                        objAddDocumentScan.QuestionRefID = questionsresult.QuestionId;
                        objAddDocumentScan.CreatedBy = userCredentials.id;
                        objAddDocumentScan.CreatedOn = time;
                        return Json(QuestionLogicRepository.SaveAddDocumentProperties(objAddDocumentScan));
                    }
                    else
                    {
                        return Json(new { error = "Document Scan Property Already Deleted" });
                    }

                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }


        public ActionResult DeleteAddDocumentScanProperties(int AddDocumentScanTypeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteAddDocumentScanProperties(AddDocumentScanTypeId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }


        public ActionResult GetDocumetScanTitle(int questionRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetDocumetScanTitle(questionRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult UpdateDocumentScanTitle(int DocumentScanTitleId, int QuestionId, string Title)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    return Json(QuestionLogicRepository.UpdateDocumentScanTitle(DocumentScanTitleId, QuestionId, Title, userCredentials.id));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult UpdateAddDocumentScanProperties(string AddDocumentScan)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    DocumentScanType objAddDocumentProperties = JsonConvert.DeserializeObject<DocumentScanType>(WebUtility.UrlDecode(AddDocumentScan));

                    objAddDocumentProperties.UpdatedBy = userCredentials.id;
                    objAddDocumentProperties.UpdatedOn = time;
                    return Json(QuestionLogicRepository.UpdateAddDocumentScanProperties(objAddDocumentProperties));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        //selvam - end
        #endregion
        //Divakar
        ///<summary>
        ///
        ///Question path Page Methods End here
        ///</summary>
        ///
        public ActionResult GetActivitiesRegistrationForSetActionValue(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = QuestionLogicRepository.GetActivitiesRegistrationForSetActionValue(LayoutId);

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetActivitiesRegistrationForSetActionNextViewValue()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = QuestionLogicRepository.GetActivitiesRegistrationForSetActionNextViewValue();

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetOBCTypeForLayout(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = QuestionLogicRepository.GetOBCTypeForLayout(LayoutId);

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetIsTripPalaningType(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = QuestionLogicRepository.GetIsTripPalaningType(LayoutId);

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetActivitiesRegistrationPlanning(int ActivityId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var result = QuestionLogicRepository.GetActivitiesRegistrationPlanning(ActivityId);

                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetReadoutDetail(string TockenId, int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<ReadoutDetail> returnresult = QuestionLogicRepository.GetReadoutDetail(TockenId ?? "", LayoutId).ToList();
                    return Json(returnresult);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult SaveReadoutDetail(string ReadoutDetail, int LayoutRefId, int ActivitiesRefId, string AliasName)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    BOReadoutDetail objReadoutDetail = JsonConvert.DeserializeObject<BOReadoutDetail>(WebUtility.UrlDecode(ReadoutDetail));
                    ReadoutDetail objReadout = new ReadoutDetail();
                    objReadout.ReadoutDetailId = objReadoutDetail.ReadoutDetailId;
                    objReadout.ReadoutValueRefId = objReadoutDetail.ReadoutValueRefId;
                    objReadout.CommentID = objReadoutDetail.CommentID;
                    objReadout.ConfigID = objReadoutDetail.ConfigID;
                    objReadout.ReadoutRange = objReadoutDetail.ReadoutRange;
                    objReadout.StartChar = objReadoutDetail.StartChar;
                    objReadout.CharLength = objReadoutDetail.CharLength;
                    objReadout.ReadoutExample = objReadoutDetail.ReadoutExample;

                    objReadout.CreatedBy = userCredentials.id;
                    objReadout.CreatedOn = time;
                    if (objReadoutDetail.TokenId == null || objReadoutDetail.TokenId.Trim().Length == 0)
                        objReadout.TokenId = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    else
                        objReadout.TokenId = objReadoutDetail.TokenId;


                    //if (objReadoutDetail.Saveto == "0" || ((int.TryParse(objReadoutDetail.Saveto, out intSaveTo)) == false))
                    //{
                    //    objChioceProperties.Saveto = AliasNameDetailsId.ToString();
                    //}
                    //else
                    //{
                    //    objChioceProperties.Saveto = objChioceProperties.Saveto;
                    //}

                    //string defaultAliasName = listActivitiesRegistrations + "." + objChioceProperties.Title;
                    //if (defaultAliasName == objChioceProperties.SavetoAlaiasVal && objChioceProperties.SavetoAlaiasVal != null && objChioceProperties.SavetoAlaiasVal != string.Empty)
                    //{
                    //    objChioceProperties.IsDefaultAlaisName = true;
                    //}
                    //else
                    //{
                    //    objChioceProperties.IsDefaultAlaisName = false;
                    //}

                    int intSaveTo;
                    if (objReadoutDetail.Saveto == "0" || ((int.TryParse(objReadoutDetail.Saveto, out intSaveTo)) == false))
                    {
                        AliasNameDetails AND = new AliasNameDetails();
                        AND.LayoutRefId = LayoutRefId;
                        AND.ActivitiesRefId = ActivitiesRefId;
                        AND.AliasName = AliasName;
                        AND.PropertyName = "Planning Select";
                        AND.CreatedBy = userCredentials.id;
                        AND.CreatedOn = time;
                        // IS temp value = 1;
                        AND.IsTempData = true;
                        var aliasDetail = QuestionLogicRepository.SaveAliasNameDetails(AND);
                        objReadout.SaveTo = aliasDetail.AliasNameDetailsId;
                    }

                    else
                    {
                        objReadout.SaveTo = Convert.ToInt32(objReadoutDetail.Saveto);
                    }

                    if (objReadoutDetail.ReadoutValueRefId != 1)
                    {
                        if (AliasName == objReadoutDetail.DefaultAliasName)
                        {
                            objReadout.IsDefaultAlaisName = true;
                        }
                        else
                        {
                            objReadout.IsDefaultAlaisName = false;
                        }
                    }

                    var result = QuestionLogicRepository.SaveReadoutDetail(objReadout);
                    return Json(result.TokenId);
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult DeleteReadoutDetail(int ReadoutDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteReadoutDetail(ReadoutDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult EditReadoutDetail(int ReadoutDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.EditReadoutDetail(ReadoutDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetpReadoutDetail(int PlanningSelectRefId, int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<PlanningSelectReadoutDetail> returnresult = QuestionLogicRepository.GetpReadoutDetail(PlanningSelectRefId, LayoutId).ToList();
                    return Json(returnresult);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult DeletePlanningSelectReadoutDetail(int PlanningSelectReadoutDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeletePlanningSelectReadoutDetail(PlanningSelectReadoutDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult EditPlanningSelectReadoutDetail(int PlanningSelectReadoutDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.EditPlanningSelectReadoutDetail(PlanningSelectReadoutDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdatePlanningSelectReadoutDetail(string ReadoutDetail, int LayoutRefId, int ActivitiesRefId, string AliasName)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    BOReadoutDetail objReadoutDetail = JsonConvert.DeserializeObject<BOReadoutDetail>(WebUtility.UrlDecode(ReadoutDetail));
                    PlanningSelectReadoutDetail objReadout = new PlanningSelectReadoutDetail();
                    objReadout.PlanningSelectReadoutDetailId = objReadoutDetail.PlanningSelectReadoutDetailId;
                    objReadout.ReadoutValueRefId = objReadoutDetail.ReadoutValueRefId;
                    objReadout.CommentID = objReadoutDetail.CommentID;
                    objReadout.ConfigID = objReadoutDetail.ConfigID;
                    objReadout.ReadoutRange = objReadoutDetail.ReadoutRange;
                    objReadout.StartChar = objReadoutDetail.StartChar;
                    objReadout.CharLength = objReadoutDetail.CharLength;
                    objReadout.ReadoutExample = objReadoutDetail.ReadoutExample;
                    objReadout.CreatedBy = userCredentials.id;
                    objReadout.CreatedOn = time;
                    objReadout.UpdatedBy = userCredentials.id;
                    objReadout.UpdatedOn = time;
                    objReadout.PlanningSelectRefId = objReadoutDetail.PlanningSelectRefId;

                    int intSaveTo;
                    if (objReadoutDetail.Saveto == "0" || ((int.TryParse(objReadoutDetail.Saveto, out intSaveTo)) == false))
                    {
                        AliasNameDetails AND = new AliasNameDetails();
                        AND.LayoutRefId = LayoutRefId;
                        AND.ActivitiesRefId = ActivitiesRefId;
                        AND.AliasName = AliasName;
                        AND.PropertyName = "Planning Select";
                        AND.CreatedBy = userCredentials.id;
                        AND.CreatedOn = time;
                        // IS temp value = 1;
                        AND.IsTempData = true;
                        var aliasDetail = QuestionLogicRepository.SaveAliasNameDetails(AND);
                        objReadout.SaveTo = aliasDetail.AliasNameDetailsId;
                    }
                    else
                    {
                        objReadout.SaveTo = Convert.ToInt32(objReadoutDetail.Saveto);
                    }

                    if (objReadoutDetail.ReadoutValueRefId != 1)
                    {
                        if (AliasName == objReadoutDetail.DefaultAliasName)
                        {
                            objReadout.IsDefaultAlaisName = true;
                        }
                        else
                        {
                            objReadout.IsDefaultAlaisName = false;
                        }
                    }


                    var result = QuestionLogicRepository.UpdatePlanningSelectReadoutDetail(objReadout);

                    return Json(result.PlanningSelectRefId);
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetInfoMessageIconType()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetInfoMessageIconType());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetPropertyNameByQuestionId(int QuestionID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetPropertyNameByQuestionId(QuestionID));
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = ex.Message
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetQuestionsByQuestionId(int QuestionID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetQuestionsByQuestionId(QuestionID));
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = ex.Message
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetTransportTypePropertiesDetail(int transportTypeRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetTransportTypePropertiesDetail(transportTypeRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdateTransportTypePropertiesDetail(string TransportTypePropertiesDetails)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    TransportTypePropertiesDetail objTransportTypePropertiesDetails = JsonConvert.DeserializeObject<TransportTypePropertiesDetail>(WebUtility.UrlDecode(TransportTypePropertiesDetails));
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objTransportTypePropertiesDetails.QuestionRefId);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        if (objTransportTypePropertiesDetails.CreatedBy == null)
                        {
                            objTransportTypePropertiesDetails.CreatedBy = userCredentials.id;
                        }
                        if (objTransportTypePropertiesDetails.CreatedOn == null)
                        {
                            objTransportTypePropertiesDetails.CreatedOn = time;
                        }

                        objTransportTypePropertiesDetails.UpdatedBy = userCredentials.id;
                        objTransportTypePropertiesDetails.UpdatedOn = time;
                        return Json(QuestionLogicRepository.UpdateTransportTypePropertiesDetail(objTransportTypePropertiesDetails));
                    }
                    else
                    {
                        return Json(new { error = "Choice Detail Already Deleted" });
                    }

                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetTransportTypeProperties(int questionRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetTransportTypeProperties(questionRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult UpdateTransportTypeProperties(string TransportTypeProperties)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    TransportTypeProperties objTransportTypeProperties = JsonConvert.DeserializeObject<TransportTypeProperties>(WebUtility.UrlDecode(TransportTypeProperties));

                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(objTransportTypeProperties.QuestionRefId);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = objTransportTypeProperties.QuestionRefId;
                        objQuestions.Name = objTransportTypeProperties.Title;
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);

                        objTransportTypeProperties.UpdatedBy = userCredentials.id;
                        objTransportTypeProperties.UpdatedOn = time;
                        var TransportTypeResult = QuestionLogicRepository.UpdateTransportTypeProperties(objTransportTypeProperties);

                        TransportTypeProperties objTransportType = new TransportTypeProperties();
                        objTransportType.ActivitiesRefId = objTransportTypeProperties.ActivitiesRefId;
                        objTransportType.LayoutRefId = objTransportTypeProperties.LayoutRefId;
                        objTransportType.QuestionRefId = objTransportTypeProperties.QuestionRefId;
                        objTransportType.PlanningFeedbackCode = objTransportTypeProperties.PlanningFeedbackCode;
                        objTransportType.CreatedBy = userCredentials.id;
                        objTransportType.CreatedOn = time;
                        objTransportType.PartnerCode = objTransportTypeProperties.PartnerCode;
                        objTransportType.TransportTypeId = objTransportTypeProperties.TransportTypeId;
                        objTransportType.AddDefault = objTransportTypeProperties.AddDefault;

                        QuestionLogicRepository.SaveDefaultTransportTypePropertiesDetail(objTransportType, 9999999, 999999, "Default", "-");

                        return Json(TransportTypeResult.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "TransportType Property Already Deleted" });
                    }
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult CheckEndRecursiveCheckedOrNot(int choiceId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.CheckEndRecursiveCheckedOrNot(choiceId));
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = ex.Message
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult CheckEndRecCheckedOrNotForChoiceid(int choiceId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.CheckEndRecCheckedOrNotForChoiceid(choiceId));
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        error = ex.Message
                    });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetTrailerProperties(int questionRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetTrailerProperties(questionRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateTrailerProperties(string trailerProperties, int pnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(pnodeid);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();
                        int AliasNameDetailsId = 0;
                        string ANameDetailsId = "0";
                        TrailerProperties objTrailerProperties = JsonConvert.DeserializeObject<TrailerProperties>(WebUtility.UrlDecode(trailerProperties));
                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objTrailerProperties.ActivitiesRefId);

                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = pnodeid;
                        if (objTrailerProperties.ActionType == "Disconnect")
                            objQuestions.Name = objTrailerProperties.Name;
                        else
                            objQuestions.Name = objTrailerProperties.Title.Replace("ƒ", "+");
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);




                        if (objTrailerProperties.Save == true && objTrailerProperties.SaveTo == 0)
                        {
                            AliasNameDetails AND = new AliasNameDetails();
                            AND.LayoutRefId = objTrailerProperties.LayoutRefId;
                            AND.ActivitiesRefId = objTrailerProperties.ActivitiesRefId;
                            AND.AliasName = listActivitiesRegistrations + "." + objTrailerProperties.Title.Replace("ƒ", "+");
                            AND.PropertyName = "Trailer";
                            AND.CreatedBy = userCredentials.id;
                            AND.CreatedOn = time;
                            var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                            AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                            ANameDetailsId = aliasNameId.AliasNameDetailsId.ToString();
                        }

                        TrailerProperties objTrailer = new TrailerProperties();
                        objTrailer.TrailerId = objTrailerProperties.TrailerId;
                        objTrailer.LayoutRefId = objTrailerProperties.LayoutRefId;
                        objTrailer.Title = objTrailerProperties.Title.Replace("ƒ", "+");
                        objTrailer.Name = objTrailerProperties.Name;
                        objTrailer.MaskText = objTrailerProperties.MaskText;
                        objTrailer.Size = objTrailerProperties.Size;
                        objTrailer.NullNotAllowed = objTrailerProperties.NullNotAllowed;
                        objTrailer.MinimumofCharacter = objTrailerProperties.MinimumofCharacter;
                        objTrailer.MinimumofCharacterValue = objTrailerProperties.MinimumofCharacterValue;
                        objTrailer.ExactOfCharacter = objTrailerProperties.ExactOfCharacter;
                        objTrailer.ZeroNotAllowed = objTrailerProperties.ZeroNotAllowed;
                        objTrailer.Regex = objTrailerProperties.Regex;
                        objTrailer.ActionType = objTrailerProperties.ActionType;
                        objTrailer.Save = objTrailerProperties.Save;
                        objTrailer.TrailerFormat = objTrailerProperties.TrailerFormat;

                        if (objTrailerProperties.Propose == 0)
                            objTrailer.Propose = Convert.ToInt32(ANameDetailsId);
                        else
                            objTrailer.Propose = objTrailerProperties.Propose;

                        if (objTrailerProperties.Save == true && objTrailerProperties.SaveTo == 0)
                            objTrailer.SaveTo = Convert.ToInt32(ANameDetailsId);
                        else
                            objTrailer.SaveTo = objTrailerProperties.SaveTo;

                        objTrailer.PlanningFeedback = objTrailerProperties.PlanningFeedback;
                        objTrailer.FeedbackType = objTrailerProperties.FeedbackType;
                        objTrailer.PlanningFeedbackCode = objTrailerProperties.PlanningFeedbackCode;
                        objTrailer.CreatedBy = userCredentials.id;
                        objTrailer.CreatedOn = time;
                        objTrailer.UpdatedBy = userCredentials.id;
                        objTrailer.UpdatedOn = time;
                        objTrailer.Comment = objTrailerProperties.Comment;
                        objTrailer.AutoGenerate = objTrailerProperties.AutoGenerate;
                        objTrailer.PartnerCode = objTrailerProperties.PartnerCode;
                        objTrailer.Other = objTrailerProperties.Other;
                        objTrailer.QuestionRefId = pnodeid;
                        objTrailer.PropertyName = objTrailerProperties.PropertyName;

                        var TrailerResult = QuestionLogicRepository.UpdateTrailerProperties(objTrailer);

                        if (objTrailerProperties.Save == true && objTrailerProperties.SaveTo == 0)
                        {
                            QuestionLogicRepository.UpdateAliasPropertyTableId(AliasNameDetailsId, TrailerResult.TrailerId);
                        }

                        return Json(TrailerResult.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Trailer Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetChoiceIdByQuestionId(int quesionId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetChoiceIdByQuestionId(quesionId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetChoiceIdByAddChoiceQuestionId(int quesionId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetChoiceIdByAddChoiceQuestionId(quesionId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetTransTypeIdByTTQuestionId(int quesionId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetTransTypeIdByTTQuestionId(quesionId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdateGetSystemValuesProperties(string getSystemValuesProperties, int pGetSystemValuesnodeid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(pGetSystemValuesnodeid);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();
                        GetSystemValuesProperties objGetSystemValuesProperties = JsonConvert.DeserializeObject<GetSystemValuesProperties>(WebUtility.UrlDecode(getSystemValuesProperties));
                        var listActivitiesRegistrations = QuestionLogicRepository.GetActivitiesRegistration(objGetSystemValuesProperties.ActivitiesRefId);

                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = pGetSystemValuesnodeid;
                        objQuestions.Name = objGetSystemValuesProperties.Name;
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);

                        int AliasNameDetailsId = 0;
                        string ANameDetailsId = "0";

                        AliasNameDetails AND = new AliasNameDetails();
                        AND.LayoutRefId = objGetSystemValuesProperties.LayoutRefId;
                        AND.ActivitiesRefId = objGetSystemValuesProperties.ActivitiesRefId;
                        AND.AliasName = objGetSystemValuesProperties.SaveToText;
                        AND.PropertyName = "Get System Values";
                        AND.CreatedBy = userCredentials.id;
                        AND.IsTempData = false;
                        AND.CreatedOn = time;
                        var aliasNameId = QuestionLogicRepository.SaveAliasNameDetails(AND);
                        AliasNameDetailsId = aliasNameId.AliasNameDetailsId;
                        ANameDetailsId = aliasNameId.AliasNameDetailsId.ToString();

                        GetSystemValuesProperties objGetSystemValues = new GetSystemValuesProperties();
                        objGetSystemValues.GetSystemValuesId = objGetSystemValuesProperties.GetSystemValuesId;
                        objGetSystemValues.ActivitiesRefId = objGetSystemValuesProperties.ActivitiesRefId;
                        objGetSystemValues.Name = objGetSystemValuesProperties.Name;
                        objGetSystemValues.LayoutRefId = objGetSystemValuesProperties.LayoutRefId;
                        objGetSystemValues.GetSystemValuesRefId = objGetSystemValuesProperties.GetSystemValuesRefId;
                        objGetSystemValues.GetSystemValuesFormatRefId = (objGetSystemValuesProperties.GetSystemValuesFormatRefId > 0 ? objGetSystemValuesProperties.GetSystemValuesFormatRefId : null);

                        int intSaveTo;
                        if (objGetSystemValuesProperties.SaveToValue == "0" || ((int.TryParse(objGetSystemValuesProperties.SaveToValue, out intSaveTo)) == false))
                        {
                            objGetSystemValues.SaveTo = AliasNameDetailsId;
                        }
                        else
                        {
                            Int32.TryParse(objGetSystemValuesProperties.SaveToValue, out intSaveTo);
                            objGetSystemValues.SaveTo = intSaveTo;
                        }

                        string defaultAliasName = objGetSystemValuesProperties.Name.Replace("Get ", listActivitiesRegistrations + ".");
                        if (defaultAliasName == objGetSystemValuesProperties.SaveToText)
                        {
                            objGetSystemValues.IsDefaultAlaisName = true;
                        }
                        else
                        {
                            objGetSystemValues.IsDefaultAlaisName = false;
                        }

                        objGetSystemValues.UpdatedBy = userCredentials.id;
                        objGetSystemValues.UpdatedOn = time;
                        objGetSystemValues.Comment = objGetSystemValuesProperties.Comment;
                        var GetSystemValuesProperties = QuestionLogicRepository.UpdateGetSystemValuesProperties(objGetSystemValues);
                        return Json(GetSystemValuesProperties.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Check Off Property Already Deleted" });
                    }
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetSystemValuesPropertiesList(int questionRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetSystemValuesProperties(questionRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetSystemValuesList()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetSystemValuesDetails());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetSystemValuesFormatList(int getSystemValuesId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetSystemValuesFormatDetails(getSystemValuesId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult DocumentScanValidation(int documentScanTitleRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DocumentScanValidation(documentScanTitleRefId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetCustomerIdByLayoutId(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetCustomerIdByLayoutId(layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetCommentReadoutDetail(string TockenId, int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<CommentDetail> returnresult = QuestionLogicRepository.GetCommentReadoutDetail(TockenId ?? "", LayoutId).ToList();
                    return Json(returnresult);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetFilterDetail(string TockenId, int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<FilterDetail> returnresult = QuestionLogicRepository.GetFilterDetail(TockenId ?? "", LayoutId).ToList();
                    return Json(returnresult);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetAliasNameDetailsForVLReadOut(int activitiesId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetAliasNameDetailsForReadOut(activitiesId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveCommentReadoutDetail(string commentReadoutDetail, int layoutRefId, int activitiesRefId, string aliasName)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    CommentDetail objReadoutDetail = JsonConvert.DeserializeObject<CommentDetail>(WebUtility.UrlDecode(commentReadoutDetail));

                    objReadoutDetail.CreatedBy = userCredentials.id;
                    objReadoutDetail.CreatedOn = time;
                    if (objReadoutDetail.TokenId == null || objReadoutDetail.TokenId.Trim().Length == 0)
                        objReadoutDetail.TokenId = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                    int intSaveTo;
                    if (objReadoutDetail.SaveToValue == "0" || (!(int.TryParse(objReadoutDetail.SaveToValue, out intSaveTo))))
                    {
                        AliasNameDetails AND = new AliasNameDetails();
                        AND.LayoutRefId = layoutRefId;
                        AND.ActivitiesRefId = activitiesRefId;
                        AND.AliasName = aliasName;
                        AND.PropertyName = "Variable List Comment";
                        AND.CreatedBy = userCredentials.id;
                        AND.CreatedOn = time;
                        AND.IsTempData = true;
                        var aliasDetail = QuestionLogicRepository.SaveAliasNameDetails(AND);
                        objReadoutDetail.SaveTo = aliasDetail.AliasNameDetailsId;
                    }
                    else
                        objReadoutDetail.SaveTo = Convert.ToInt32(objReadoutDetail.SaveToValue);


                    if (aliasName == objReadoutDetail.DefaultAliasName)
                        objReadoutDetail.IsDefaultAlaisName = true;
                    else
                        objReadoutDetail.IsDefaultAlaisName = false;


                    var result = QuestionLogicRepository.SaveCommentReadoutDetail(objReadoutDetail);
                    return Json(result.TokenId);
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult DeleteCommentReadoutDetail(int commentDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteCommentReadoutDetail(commentDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult EditCommentReadoutDetail(int commentDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.EditCommentReadoutDetail(commentDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveFilterDetail(string filterDetail, int layoutRefId, int activitiesRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    FilterDetail objDetail = JsonConvert.DeserializeObject<FilterDetail>(WebUtility.UrlDecode(filterDetail));

                    objDetail.CreatedBy = userCredentials.id;
                    objDetail.CreatedOn = time;
                    if (objDetail.TokenId == null || objDetail.TokenId.Trim().Length == 0)
                        objDetail.TokenId = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                    var result = QuestionLogicRepository.SaveFilterDetail(objDetail);
                    return Json(result.TokenId);
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteFilterDetail(int filterDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteFilterDetail(filterDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult EditFilterDetail(int filterDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.EditFilterDetail(filterDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetVarListProperties(int questionRefId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetVarListProperties(questionRefId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetVarListCommentReadoutDetail(int varListPropertiesId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<VarListCommentReadoutDetail> returnresult = QuestionLogicRepository.GetVarListCommentReadoutDetail(varListPropertiesId, layoutId).ToList();
                    return Json(returnresult);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetVarListFilterDetail(int varListPropertiesId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<VarListFilterDetail> returnresult = QuestionLogicRepository.GetVarListFilterDetail(varListPropertiesId, layoutId).ToList();
                    return Json(returnresult);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult SaveVarListCommentReadoutDetail(string commentReadoutDetail, int layoutRefId, int activitiesRefId, string aliasName)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    VarListCommentReadoutDetail objReadoutDetail = JsonConvert.DeserializeObject<VarListCommentReadoutDetail>(WebUtility.UrlDecode(commentReadoutDetail));

                    objReadoutDetail.CreatedBy = userCredentials.id;
                    objReadoutDetail.CreatedOn = time;

                    int intSaveTo;
                    if (objReadoutDetail.SaveToValue == "0" || (!int.TryParse(objReadoutDetail.SaveToValue, out intSaveTo)))
                    {
                        AliasNameDetails AND = new AliasNameDetails();
                        AND.LayoutRefId = layoutRefId;
                        AND.ActivitiesRefId = activitiesRefId;
                        AND.AliasName = aliasName;
                        AND.PropertyName = "Variable List Comment";
                        AND.CreatedBy = userCredentials.id;
                        AND.CreatedOn = time;
                        AND.IsTempData = true;
                        var aliasDetail = QuestionLogicRepository.SaveAliasNameDetails(AND);
                        objReadoutDetail.SaveTo = aliasDetail.AliasNameDetailsId;
                    }
                    else
                        objReadoutDetail.SaveTo = Convert.ToInt32(objReadoutDetail.SaveToValue);


                    if (aliasName == objReadoutDetail.DefaultAliasName)
                        objReadoutDetail.IsDefaultAlaisName = true;
                    else
                        objReadoutDetail.IsDefaultAlaisName = false;


                    var result = QuestionLogicRepository.UpdateVarListCommentReadoutDetail(objReadoutDetail);
                    return Json(result.VarListCommentReadoutDetailId);
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult DeleteVarListCommentReadoutDetail(int commentDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteVarListCommentReadoutDetail(commentDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult EditVarListCommentReadoutDetail(int commentDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.EditVarListCommentReadoutDetail(commentDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveVarListFilterDetail(string filterDetail, int layoutRefId, int activitiesRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    VarListFilterDetail objDetail = JsonConvert.DeserializeObject<VarListFilterDetail>(WebUtility.UrlDecode(filterDetail));

                    objDetail.CreatedBy = userCredentials.id;
                    objDetail.CreatedOn = time;


                    var result = QuestionLogicRepository.UpdateVarListFilterDetail(objDetail);
                    return Json(result.VarListFilterDetailId);
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteVarListFilterDetail(int filterDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.DeleteVarListFilterDetail(filterDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult EditVarListFilterDetail(int filterDetailId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.EditVarListFilterDetail(filterDetailId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult UpdateVarListProperties(string varListProperties, int variableListNodeId, int transportTypeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var questionStatus = QuestionLogicRepository.GetQuestionsStatus(variableListNodeId);
                    if (questionStatus != (int)EnumCopyColorCode.Red)
                    {
                        int AliasNameDetailsId = 0;
                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();
                        VarListProperties objVariableListProperties = JsonConvert.DeserializeObject<VarListProperties>(WebUtility.UrlDecode(varListProperties));

                        boQuestion objQuestions = new boQuestion();
                        objQuestions.QuestionId = objVariableListProperties.QuestionRefId;
                        objQuestions.Name = objVariableListProperties.Title + " [" + objVariableListProperties.VarListId + "]";//KC
                        objQuestions.UpdatedBy = userCredentials.id;
                        objQuestions.UpdatedOn = time;
                        QuestionLogicRepository.UpdateQuestions(objQuestions);

                        VarListProperties varListObj = new VarListProperties();
                        varListObj.VarListPropertiesId = objVariableListProperties.VarListPropertiesId;
                        varListObj.QuestionRefId = objVariableListProperties.QuestionRefId;
                        varListObj.LayoutRefId = objVariableListProperties.LayoutRefId;
                        varListObj.ActivitiesRefId = objVariableListProperties.ActivitiesRefId;
                        varListObj.SaveDisplayAlaiasVal = objVariableListProperties.SaveDisplayAlaiasVal;

                        varListObj.Name = objVariableListProperties.Title;
                        varListObj.Title = objVariableListProperties.Title;
                        varListObj.VarListId = objVariableListProperties.VarListId;
                        varListObj.Other = objVariableListProperties.Other;
                        varListObj.OtherTitle = objVariableListProperties.OtherTitle;
                        varListObj.OtherMaskText = objVariableListProperties.OtherMaskText;
                        varListObj.SaveDisplay = objVariableListProperties.SaveDisplay;
                        varListObj.SavedDisplayValues = objVariableListProperties.SavedDisplayValues;
                        varListObj.Comment = objVariableListProperties.Comment;
                        varListObj.ConditionItemValue = objVariableListProperties.ConditionItemValue;
                        varListObj.ConditionSavedValue = objVariableListProperties.ConditionSavedValue;
                        varListObj.CompareMethod = objVariableListProperties.CompareMethod;
                        varListObj.CompareTo = objVariableListProperties.CompareTo;
                        varListObj.CompareToSavedValue = objVariableListProperties.CompareToSavedValue;
                        varListObj.CompareToFixedText = objVariableListProperties.CompareToFixedText;
                        varListObj.PlanningFeedback = objVariableListProperties.PlanningFeedback;
                        varListObj.FeedbackType = objVariableListProperties.FeedbackType;
                        varListObj.PlanningFeedbackCode = objVariableListProperties.PlanningFeedbackCode;
                        varListObj.Infocolumn = objVariableListProperties.Infocolumn;
                        varListObj.InfoColumnAction = objVariableListProperties.InfoColumnAction;
                        varListObj.FileNumber = objVariableListProperties.FileNumber;
                        varListObj.PartnerCode = objVariableListProperties.PartnerCode;
                        varListObj.RegisterAsTrailer = objVariableListProperties.RegisterAsTrailer;
                        varListObj.UpdatedBy = userCredentials.id;
                        varListObj.UpdatedOn = time;
                        varListObj.CreatedBy = userCredentials.id;
                        varListObj.CreatedOn = time;

                        varListObj = QuestionLogicRepository.UpdateVarListProperties(varListObj, transportTypeId);

                        return Json(varListObj.QuestionRefId);
                    }
                    else
                    {
                        return Json(new { error = "Variable List Property Already Deleted" });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult GetQuestionPathComment(int layoutId, int activityId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetQuestionPathComment(layoutId, activityId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveQuestionPathComment(int activityId, string comment)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.SaveQuestionPathComment(activityId, comment));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult ExtendedHelp(int uiId, int layoutId)
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();

                    ViewBag.username = userCredentials.displayName;
                    ViewBag.SessionLink = othersSettings.SessionLink;
                    ViewData["ExtendedHelp"] = _tooltipRepository.GetExtendedHelpText(uiId, layoutId);
                    ViewData["PropertyTitle"] = _tooltipRepository.GetTooltipTitle(uiId);
                    ViewData["ExtendedHelpTooltipImage"] = _tooltipRepository.GetExtendedHelpTooltipImage(uiId);

                    return View();
                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");
        }

        public ActionResult GetTransportTypes(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(QuestionLogicRepository.GetTransportTypes(layoutId));
    }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
}
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
    }
}


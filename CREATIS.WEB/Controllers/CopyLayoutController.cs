﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using CREATIS.Infrastructure.Repository;
using CREATIS.Infrastructure.IRepository;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace CREATIS.Controllers
{
    public class CopyLayoutController : BaseController
    {

        public readonly ICopyLayoutRepository copylayoutRepository;
        public readonly IDashboardRepository DashboardLogicRepository;
        public ConnectionStrings connectionStrings { get; }
        public CopyLayoutController(ICopyLayoutRepository copylayoutrepo, IDashboardRepository DashboardLogicRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate)
        {
            this.copylayoutRepository = copylayoutrepo;
            this.DashboardLogicRepository = DashboardLogicRepo;
            this.connectionStrings = connectionTemplate.Value;
        }

        ///<summary>
        ///Copy Layout Page Methods Starts here
        ///</summary>

        #region 


        // Copy Layout By Selvam

        public ActionResult GetCopyLayoutName(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetCopyLayoutName(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        [HttpPost]
        public ActionResult SaveCopyLayout(int LayoutId, string NewLayoutName, int CustomerId, string LanguageIds, bool InfoColumn)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    var EmailId = (userCredentials.emails.Count() == 0 ? "" : userCredentials.emails.Where(x => x.Trim().Length > 0).Select(x => x).Distinct().ToList().Aggregate((a, b) => a + "," + b));
                    var copylayoutresult = copylayoutRepository.SaveCopyLayout(LayoutId, NewLayoutName, CustomerId, LanguageIds, userCredentials.id, InfoColumn, EmailId, userCredentials.displayName);
                    return Json(copylayoutresult);
                }
                catch
                {
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E02)
                    });
                }

            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetExsistingLayoutName(int CustomerId, string LayoutName)
        {
            if (IsServerConnected(connectionStrings))
            {

                var result = DashboardLogicRepository.GetExsistingLayoutName(CustomerId, LayoutName);

                if (result)
                    return Json("");
                else
                    return Json(new
                    {
                        error = "Layout Name Already Exist."
                    });
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetInfoColumnDetails(int ToCustomerId, int FromCustomerId)
        {
            if (IsServerConnected(connectionStrings))
            {

                var result = DashboardLogicRepository.GetInfoColumnDetails(ToCustomerId, FromCustomerId);

                if (result)
                    return Json("");
                else
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                

            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }



        // Copy Layout By Selvam




        #endregion

        ///<summary>
        ///Copy Layout Page Methods End here
        ///</summary>
    }
}
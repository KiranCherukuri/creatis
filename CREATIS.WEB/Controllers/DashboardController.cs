﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using CREATIS.Infrastructure.Repository;
using CREATIS.Infrastructure.IRepository;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Xml.Linq;
using System.IO;
using System.Dynamic;

namespace CREATIS.Controllers
{
    public class DashboardController : BaseController
    {

        public readonly IDashboardRepository DashboardLogicRepository;
        public readonly ICustomerOverviewRepository CustomerLogicRepository;
        public readonly ITranslatorRepository translatorRepository;
        public readonly IEmailRepository emailRepository;
        public ConnectionStrings connectionStrings { get; }
        public QuestionPathTemplate questionpathtemplateConfiguration { get; }
        public EmailConfiguration emailConfiguration { get; }
        public ServiceSettings serviceSettings { get; }
        public OthersSettings othersSettings { get; }
        public DashboardController(IDashboardRepository DashboardLogicRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<QuestionPathTemplate> questionPathTemplate, Microsoft.Extensions.Options.IOptions<EmailConfiguration> emailConfiguration, Microsoft.Extensions.Options.IOptions<ServiceSettings> serviceSettings, ICustomerOverviewRepository CustomerOverviewLogicRepo, ITranslatorRepository translatorRepo, IEmailRepository emailRepository, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings)
        {
            this.DashboardLogicRepository = DashboardLogicRepo;
            this.CustomerLogicRepository = CustomerOverviewLogicRepo;
            this.translatorRepository = translatorRepo;
            this.emailRepository = emailRepository;
            this.questionpathtemplateConfiguration = questionPathTemplate.Value;
            this.emailConfiguration = emailConfiguration.Value;
            this.connectionStrings = connectionTemplate.Value;
            this.serviceSettings = serviceSettings.Value;
            this.othersSettings = othersSettings.Value;
        }

        ///<summary>
        /// Dashboard Page Methods Starts here
        ///</summary>

        #region 

        ///<summary>
        /// Dashboard View render method start
        ///</summary>
        public ActionResult Dashboard(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                var userCredentials = GetUserCredentials();

                var IsTemplateLayout = translatorRepository.ValidateTemplateLayoutId(questionpathtemplateConfiguration.CustomerId, LayoutID);

                if (((HasPrivilege(userCredentials.permissions).Contains("D") || HasPrivilege(userCredentials.permissions).Contains("LD") || HasPrivilege(userCredentials.permissions).Contains("QPD") || HasPrivilege(userCredentials.permissions).Contains("SLD") || HasPrivilege(userCredentials.permissions).Contains("TD")) && (IsTemplateLayout == true ? HasPrivilege(userCredentials.permissions).Contains("DTQP") : (1 == 1))) && IsCustomerLogin(userCredentials.permissions))
                {
                    var customerId = GetLayoutCustomerId(LayoutID);
                    if (GetCustomerLayoutList(userCredentials.company, customerId).Any(x => x == LayoutID))
                    {
                        LayoutStatusPrivilege(LayoutID);
                        RoleWithLayoutStatusPrivilege(LayoutID, userCredentials.permissions);
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        AddRecentActivity(LayoutID, userCredentials.id);
                        return View();
                    }
                    else
                        return View("Unauthorised");
                }
                else if ((HasPrivilege(userCredentials.permissions).Contains("D") || HasPrivilege(userCredentials.permissions).Contains("LD") || HasPrivilege(userCredentials.permissions).Contains("QPD") || HasPrivilege(userCredentials.permissions).Contains("SLD") || HasPrivilege(userCredentials.permissions).Contains("TD")) && (IsTemplateLayout == true ? HasPrivilege(userCredentials.permissions).Contains("DTQP") : (1 == 1)))
                {
                    LayoutStatusPrivilege(LayoutID);
                    RoleWithLayoutStatusPrivilege(LayoutID, userCredentials.permissions);
                    ViewBag.username = userCredentials.displayName;
                    ViewBag.SessionLink = othersSettings.SessionLink;
                    AddRecentActivity(LayoutID, userCredentials.id);
                    return View();
                }
                else
                    return View("Unauthorised");
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Dashboard View render method End
        ///</summary>

        ///<summary>
        /// Dashboard View get layout name for to show in collapsable panel header from layout table and also status method start
        ///</summary>
        public ActionResult GetLayoutName(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetCustomerandLayoutName(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Dashboard View get layout name for to show in collapsable panel header from layout table and also status method end
        ///</summary>
        ///
        public ActionResult GetLayout(int customerId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetLayout(customerId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }


        ///<summary>
        /// Dashboard View update layout type based on obc type selection to layout table method start
        ///</summary>
        public ActionResult UpdateLayoutName(int LayoutID, string LayoutType)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.UpdateLayoutName(LayoutID, LayoutType));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Dashboard View update layout type based on obc type selection to layout table method end
        ///</summary>

        ///<summary>
        /// Dashboard View get obc type master table method start
        ///</summary>
        public ActionResult GetOBCTypeMaster(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.OBCTypesMaster(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Dashboard View get obc type master table method end
        ///</summary>


        ///<summary>
        /// Dashboard View get obc type user selected from obctype table method start
        ///</summary>
        public ActionResult GetOBCType(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.OBCTypes(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }


        public ActionResult GetLayoutStatus(int Status)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetLayoutStatus(Status));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult UpdateLayoutStatusNote(int LayoutID, int layoutStatusId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    var emails = (!userCredentials.emails.Any() ? null : userCredentials.emails.Where(x => x.Trim().Length > 0).Select(x => x).Distinct().AsEnumerable().Aggregate((a, b) => a + "," + b));
                    return Json(DashboardLogicRepository.UpdateLayoutStatus(LayoutID, layoutStatusId, userCredentials.id.ToString(), emails));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult RequestLayout(int LayoutID, int RequestTypeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    var emails = (!userCredentials.emails.Any() ? null : userCredentials.emails.Where(x => x.Trim().Length > 0).Select(x => x).Distinct().AsEnumerable().Aggregate((a, b) => a + "," + b));
                    return Json(DashboardLogicRepository.UpdateLayoutStatus(LayoutID, RequestTypeId, userCredentials.id.ToString(), emails));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult AddLayoutNotes(int LayoutID, string Notes)//, string date
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    LayoutNotes layoutNotes = new LayoutNotes();
                    layoutNotes.LayoutRefId = LayoutID;
                    layoutNotes.Notes = Notes;
                    layoutNotes.CreatedBy = userCredentials.id.ToString();
                    layoutNotes.CreatedOn = DateTime.Now;
                    layoutNotes.CreatedByName = userCredentials.displayName;
                    DashboardLogicRepository.UpdateLayoutNotes(layoutNotes);
                    return Json(DashboardLogicRepository.GetLayoutNotes(LayoutID));
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }

            }
            return Json(new
            {
                error = GetStatusMessage(StatusCode.E01)
            });

        }
        public ActionResult DeleteLayoutNotes(int layoutNotesId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var resultLayoutNote = DashboardLogicRepository.DeleteLayoutNotes(layoutNotesId);
                    if (resultLayoutNote.Length == 0)
                        return Json(DashboardLogicRepository.GetLayoutNotes(layoutId));
                    else
                        return Json(new
                        {
                            error = GetStatusMessage(StatusCode.E02)
                        });
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }

            }
            return Json(new
            {
                error = GetStatusMessage(StatusCode.E01)
            });

        }

        public ActionResult SaveLayoutNotes(int LayoutID, string Notes, string date, int layoutStatusId, string urlString)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    string fromAddress = string.Empty;
                    string toAddress = string.Empty;
                    var userCredentials = GetUserCredentials();
                    var fromemail = DashboardLogicRepository.GetLayoutFromEMail(LayoutID, layoutStatusId, emailConfiguration.ToEmailId);

                    LayoutNotes layoutNotes = new LayoutNotes();
                    layoutNotes.LayoutRefId = LayoutID;
                    layoutNotes.Notes = Notes;
                    layoutNotes.CreatedBy = userCredentials.id.ToString();
                    layoutNotes.CreatedOn = DateTime.Now;
                    layoutNotes.CreatedByName = userCredentials.displayName;
                    fromAddress = emailConfiguration.EmailAddress;
                    toAddress = fromemail;

                    List<Layout> lstLayout = DashboardLogicRepository.GetLayoutDetails(LayoutID);
                    int customerRefId = lstLayout.FirstOrDefault().CustomerRefId;
                    string LayoutName = lstLayout.FirstOrDefault().LayoutName;
                    Customers customer = DashboardLogicRepository.GetCustomer(customerRefId);
                    string customerName = customer.Name;
                    string LayoutStatus = DashboardLogicRepository.GetLayoutStatus(0).FirstOrDefault(x => x.Id == layoutStatusId).Name;
                    DashboardLogicRepository.UpdateLayoutNotes(layoutNotes);
                    emailRepository.SendLayoutUpdateStatusMail(layoutNotes, fromAddress, toAddress, customerName, LayoutName, LayoutStatus, urlString, LayoutID);

                    return Json("Layout Notes Inserted");
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            return Json(new
            {
                error = GetStatusMessage(StatusCode.E01)
            });
        }

        public ActionResult RequestLayoutMail(int LayoutID, string Notes, string date, int layoutStatusId, string urlString)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    int requestTypeId = 0;
                    if (layoutStatusId == 8) // 8 is Only display purpose
                        requestTypeId = (int)EnumLayoutStatus.ValidationRequested;
                    if (layoutStatusId == 9)// 9 is Only display purpose
                        requestTypeId = (int)EnumLayoutStatus.SupportRequested;

                    var fromemail = DashboardLogicRepository.GetLayoutFromEMail(LayoutID, requestTypeId, emailConfiguration.ToEmailId);

                    string fromAddress = string.Empty;
                    string toAddress = string.Empty;

                    LayoutNotes layoutNotes = new LayoutNotes();
                    layoutNotes.LayoutRefId = LayoutID;
                    layoutNotes.Notes = Notes;
                    layoutNotes.CreatedBy = userCredentials.id.ToString();
                    layoutNotes.CreatedOn = DateTime.Now;
                    layoutNotes.CreatedByName = userCredentials.displayName;
                    fromAddress = emailConfiguration.EmailAddress;
                    toAddress = fromemail;

                    List<Layout> lstLayout = DashboardLogicRepository.GetLayoutDetails(LayoutID);
                    int customerRefId = lstLayout.FirstOrDefault().CustomerRefId;
                    string LayoutName = lstLayout.FirstOrDefault().LayoutName;
                    Customers customer = DashboardLogicRepository.GetCustomer(customerRefId);
                    string customerName = customer.Name;
                    string LayoutStatus = DashboardLogicRepository.GetLayoutStatus(1).FirstOrDefault(x => x.Id == layoutStatusId).Name;
                    DashboardLogicRepository.UpdateLayoutNotes(layoutNotes);
                    emailRepository.SendRequestMail(layoutNotes, fromAddress, toAddress, customerName, LayoutName, LayoutStatus, urlString, userCredentials.displayName.ToString());

                    return Json("Layout Notes Inserted");
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            return Json(new
            {
                error = GetStatusMessage(StatusCode.E01)
            });
        }
        ///<summary>
        /// Dashboard View get obc type user selected from obctype table method end
        ///</summary>

        ///<summary>
        /// Dashboard View save obc type user selected to obctype table method start
        ///</summary>
        public ActionResult SaveOBCType(int LayoutID, int OBCTypeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    List<Layout> LayoutDetails = DashboardLogicRepository.GetLayoutDetails(LayoutID);
                    Layout LayoutDetailsupdate = new Layout();
                    LayoutDetailsupdate.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                    LayoutDetailsupdate.LayoutId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.LayoutId).First();
                    LayoutDetailsupdate.UpdatedBy = userCredentials.id.ToString();
                    LayoutDetailsupdate.UpdatedOn = time;
                    CustomerLogicRepository.updateLayout(LayoutDetailsupdate);
                    List<DefaultActivities> DA = DashboardLogicRepository.GetDefaultActivitiesOBC(OBCTypeId);
                    foreach (var Values in DA)
                    {
                        ActivitiesRegistration RA = new ActivitiesRegistration();
                        RA.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                        RA.LayoutRefId = LayoutID;
                        RA.ActivitiesRegistrationName = Values.ActivitiesRegistrationName;
                        RA.WorkingCodeRefId = Values.WorkingCodeRefId;
                        RA.PlanningRefId = Values.PlanningRefId;
                        RA.PlanningOverviewRefId = Values.PlanningOverviewRefId;
                        RA.SpecificRefId = Values.SpecificRefId;
                        RA.Visible = Values.Visible;
                        RA.ConfirmButton = Values.ConfirmButton;
                        RA.PTORefId = Values.PTORefId;
                        RA.InterruptibleByRefId = Values.InterruptibleByRefId;
                        RA.TransportType = Values.TransportType;
                        RA.EmptyFullSolo = Values.EmptyFullSolo;
                        RA.Flexactivity = Values.Flexactivity;
                        RA.ISModification = Values.ISModification;
                        RA.ActRegPosition = 0;
                        RA.CreatedBy = userCredentials.id.ToString();
                        RA.CreatedOn = time;
                        RA.OBCDefault = Values.OBCDefault;
                        RA.DefaultActivitiesRefId = Values.ActivitiesRegistrationId;
                        DashboardLogicRepository.SaveActivitiesRegistration(RA);
                    }

                    OBCType OBType = new OBCType();
                    OBType.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                    OBType.LayoutRefId = LayoutID;
                    OBType.OBCTypeRefId = OBCTypeId;
                    OBType.CreatedBy = userCredentials.id.ToString();
                    OBType.CreatedOn = time;
                    var result = DashboardLogicRepository.SaveOBCTypes(OBType);
                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }



        ///<summary>
        /// Dashboard View save obc type user selected to obctype table method end
        ///</summary>

        ///<summary>
        /// Dashboard View in planning type selection trip default insert start trip and stop trip in activities registration table method start
        ///</summary>
        public ActionResult SaveTripDefault(int LayoutID, int OBCTypeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    List<Layout> LayoutDetails = DashboardLogicRepository.GetLayoutDetails(LayoutID);
                    Layout LayoutDetailsupdate = new Layout();
                    LayoutDetailsupdate.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                    LayoutDetailsupdate.LayoutId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.LayoutId).First();
                    LayoutDetailsupdate.UpdatedBy = userCredentials.id.ToString();
                    LayoutDetailsupdate.UpdatedOn = time;
                    CustomerLogicRepository.updateLayout(LayoutDetailsupdate);
                    List<DefaultActivities> DA = DashboardLogicRepository.GetDefaultActivitiesTrip(LayoutID);
                    bool isCopyLayout = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.IsCopyLayout).First();

                    foreach (var Values in DA)
                    {
                        ActivitiesRegistration RA = new ActivitiesRegistration();
                        //RA.ActivitiesRegistrationId = Convert.ToInt16(DA[i].ActivitiesRegistrationId);
                        RA.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                        RA.LayoutRefId = LayoutID;
                        RA.ActivitiesRegistrationName = Values.ActivitiesRegistrationName;
                        RA.WorkingCodeRefId = Values.WorkingCodeRefId;
                        RA.PlanningRefId = Values.PlanningRefId;
                        RA.PlanningOverviewRefId = Values.PlanningOverviewRefId;
                        RA.SpecificRefId = Values.SpecificRefId;
                        RA.Visible = Values.Visible;
                        RA.ConfirmButton = Values.ConfirmButton;
                        RA.PTORefId = Values.PTORefId;
                        RA.InterruptibleByRefId = Values.InterruptibleByRefId;
                        RA.TransportType = Values.TransportType;
                        RA.EmptyFullSolo = Values.EmptyFullSolo;
                        RA.Flexactivity = Values.Flexactivity;
                        RA.ISModification = Values.ISModification;
                        RA.ActRegPosition = 0;
                        RA.CreatedBy = userCredentials.id.ToString();
                        RA.CreatedOn = time;
                        RA.OBCDefault = Values.OBCDefault;
                        RA.DefaultActivitiesRefId = Values.ActivitiesRegistrationId;

                        if (isCopyLayout)
                        {
                            bool isTripDefaultExist = false;
                            isTripDefaultExist = DashboardLogicRepository.UpdateTripDefaultActivitiesRegisration(LayoutID, Values.ActivitiesRegistrationId);

                            if (isTripDefaultExist)
                            {
                                var resultDashboard = DashboardLogicRepository.SaveActivitiesRegistration(RA);
                            }
                        }
                        else
                        {
                            var resultDashboard = DashboardLogicRepository.SaveActivitiesRegistration(RA);
                        }

                    }


                    return Json("Inserted Default trips");
                }
                catch (Exception ex)
                {
                    return Json(ex.ToString());
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View in planning type selection trip default insert start trip and stop trip in activities registration table method end
        ///</summary>

        ///<summary>
        /// Dashboard View update obc type user selected to obctype table method start
        ///</summary>
        public ActionResult UpdateOBCTypes(int OBCTypeId, int OBCTypeRefId, int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var removeResult = DashboardLogicRepository.DeleteActiviesRegistrationByOBCType(LayoutID);

                    if (removeResult)
                    {
                        List<DefaultActivities> DA = DashboardLogicRepository.GetDefaultActivitiesOBC(OBCTypeRefId);
                        //string mainlanguage = translatorRepository.LayoutDefaultLanguage(LayoutID);
                        foreach (var Values in DA)
                        {
                            DateTime time = DateTime.Now;
                            var userCredentials = GetUserCredentials();
                            List<Layout> LayoutDetails = DashboardLogicRepository.GetLayoutDetails(LayoutID);

                            ActivitiesRegistration RA = new ActivitiesRegistration();
                            //RA.ActivitiesRegistrationId = Convert.ToInt16(DA[i].ActivitiesRegistrationId);
                            RA.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                            RA.LayoutRefId = LayoutID;
                            RA.ActivitiesRegistrationName = Values.ActivitiesRegistrationName;
                            RA.WorkingCodeRefId = Values.WorkingCodeRefId;
                            RA.PlanningRefId = Values.PlanningRefId;
                            RA.PlanningOverviewRefId = Values.PlanningOverviewRefId;
                            RA.SpecificRefId = Values.SpecificRefId;
                            RA.Visible = Values.Visible;
                            RA.ConfirmButton = Values.ConfirmButton;
                            RA.PTORefId = Values.PTORefId;
                            RA.InterruptibleByRefId = Values.InterruptibleByRefId;
                            RA.TransportType = Values.TransportType;
                            RA.EmptyFullSolo = Values.EmptyFullSolo;
                            RA.Flexactivity = Values.Flexactivity;
                            RA.ISModification = Values.ISModification;
                            RA.ActRegPosition = 0;
                            RA.CreatedBy = userCredentials.id.ToString();
                            RA.CreatedOn = time;
                            RA.OBCDefault = Values.OBCDefault;
                            RA.DefaultActivitiesRefId = Values.ActivitiesRegistrationId;
                            var resultDashboard = DashboardLogicRepository.SaveActivitiesRegistration(RA);
                        }

                        return Json(DashboardLogicRepository.UpdateOBCTypes(OBCTypeId, OBCTypeRefId, LayoutID));
                    }
                    else
                        return Json(new { error = GetStatusMessage(StatusCode.E02) });

                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View update obc type user selected to obctype table method end
        ///</summary>

        ///<summary>
        /// Dashboard View get transport type inserted by user from transport type table method start
        ///</summary>
        public ActionResult GetTransportType(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetTransportTypes(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View get transport type inserted by user from transport type table method end
        ///</summary>

        ///<summary>
        /// Dashboard View save transport type to transport type table method start
        ///</summary>
        public ActionResult SaveTransportTypes(int LayoutID, string TransportDescription, int TransportTypeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    List<Layout> LayoutDetails = DashboardLogicRepository.GetLayoutDetails(LayoutID);
                    TransportTypes transportType = new TransportTypes();
                    transportType.TransportTypeId = TransportTypeId;
                    transportType.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                    transportType.LayoutRefId = LayoutID;
                    transportType.TransportTypeName = TransportDescription.Replace("`", "+");
                    transportType.CreatedBy = userCredentials.id.ToString();
                    transportType.CreatedOn = time;
                    transportType.CreatedOn = DateTime.Now;
                    transportType.IsModification = (int)EnumCopyColorCode.Green;
                    transportType.IsActive = true;

                    var result = DashboardLogicRepository.ValidateTransportType(LayoutID, TransportDescription, TransportTypeId);
                    if (!result)
                        return Json(DashboardLogicRepository.SaveTransportTypes(transportType, "New"));
                    else
                        return Json(new { error = "Transport Type Already Exist" });
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult RevertTransportTypes(int layoutId, int transportTypeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    TransportTypes transportType = new TransportTypes();
                    transportType.TransportTypeId = transportTypeId;
                    transportType.UpdatedBy = userCredentials.id.ToString();
                    transportType.UpdatedOn = time;
                    transportType.IsModification = (int)EnumCopyColorCode.Empty;
                    transportType.IsActive = true;
                    var result = DashboardLogicRepository.RevertTransportTypes(transportType);
                    return Json(DashboardLogicRepository.SaveTransportTypes(result, "Revert"));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }



        ///<summary>
        /// Dashboard View save transport type to transport type table method end
        ///</summary>

        ///<summary>
        /// Dashboard View get used language from used language table method start
        ///</summary>
        public ActionResult GetRemainingLanguages(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<LanguageMaster> List = DashboardLogicRepository.GetRemainingLanguages(layoutId).ToList();
                    return Json(List);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetUsedLanguage(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetUsedLanguages(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult RevertUsedLanguage(int layoutId, int languageId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    UsedLanguages usedLang = new UsedLanguages();
                    usedLang.UsedLanguagesId = languageId;
                    usedLang.UpdatedBy = userCredentials.id.ToString();
                    usedLang.UpdatedOn = time;
                    usedLang.IsModification = (int)EnumCopyColorCode.Empty;
                    usedLang.IsActive = true;
                    var result = DashboardLogicRepository.RevertUsedLanguage(usedLang);
                    return Json(result);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View get used language from used language table method end
        ///</summary>

        ///<summary>
        /// Dashboard View get layout from layout table method start
        ///</summary>
        public ActionResult GetLayout()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetLayout());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Dashboard View get layout from layout table method end
        ///</summary>


        ///<summary>
        /// Dashboard View save used language to used language table method start
        ///</summary>
        public ActionResult DashboardSaveUsedLanguage(int LayoutID, int LanguageID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    List<Layout> LayoutDetails = DashboardLogicRepository.GetLayoutDetails(LayoutID);

                    var existusedLang = translatorRepository.GetUsedLanguageByLayoutIdandLangId(LayoutID, LanguageID);
                    if (existusedLang.Count > 0)
                    {
                        return Json(new
                        {
                            error = "Selected Language Already Exists."
                        });
                    }

                    UsedLanguages UsedLan = new UsedLanguages();

                    var LayoutInfo = LayoutDetails.FirstOrDefault(x => x.LayoutId == LayoutID);

                    if (LayoutInfo != null)
                    {
                        UsedLan.CustomerRefId = LayoutInfo.CustomerRefId;
                        UsedLan.LayoutRefId = LayoutID;
                        UsedLan.LanguageRefId = LanguageID;
                        UsedLan.IsDefault = 0;
                        UsedLan.IsModification = (int)EnumCopyColorCode.Green;
                        UsedLan.IsActive = true;
                        UsedLan.CreatedBy = userCredentials.id.ToString();
                        UsedLan.CreatedOn = time;
                        UsedLan = CustomerLogicRepository.SaveUsedLanguage(UsedLan);
                    }
                    return Json(UsedLan);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View save used language to used language table method end
        ///</summary>

        ///<summary>
        /// Dashboard View get Planning Type Master data from PlanningTypeMaster table method start
        ///</summary>
        public ActionResult GetPlanningTypeMaster(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetPlanningTypeMaster(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View get Planning Type Master data from PlanningTypeMaster table method end
        ///</summary>


        ///<summary>
        /// Dashboard View get Planning Type data by user for specific layout selected from PlanningType table method start
        ///</summary>
        public ActionResult GetPlanningType(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetPlanningType(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View get Planning Type data by user for specific layout selected from PlanningType table method end
        ///</summary>

        ///<summary>
        /// Dashboard View save Planning Type data by user for specific layout selected from PlanningType table method start
        ///</summary>
        public ActionResult SavePlanningType(int LayoutID, string PlanningTypeRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var Plantype = PlanningTypeRefId.Split(',').ToList();
                    var planningTypeList = DashboardLogicRepository.ConsolidatedPlanningType(LayoutID, Plantype);
                    foreach (var planningType in planningTypeList)
                    {
                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();
                        List<Layout> LayoutDetails = DashboardLogicRepository.GetLayoutDetails(LayoutID);
                        Layout LayoutDetailsupdate = new Layout();
                        LayoutDetailsupdate.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                        LayoutDetailsupdate.LayoutId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.LayoutId).First();
                        LayoutDetailsupdate.UpdatedBy = userCredentials.id.ToString();
                        LayoutDetailsupdate.UpdatedOn = time;
                        CustomerLogicRepository.updateLayout(LayoutDetailsupdate);
                        PlanningType Planningtypedetails = new PlanningType();
                        Planningtypedetails.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                        Planningtypedetails.LayoutRefId = LayoutID;
                        Planningtypedetails.PlanningTypeRefId = planningType.PlanningTypeId;
                        Planningtypedetails.CreatedBy = userCredentials.id.ToString();
                        Planningtypedetails.CreatedOn = time;
                        Planningtypedetails.IsModification = planningType.IsModification;
                        Planningtypedetails.IsActive = planningType.IsActive;

                        DashboardLogicRepository.SavePlanningType(Planningtypedetails);
                    }
                    return Json("Inserted");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View save Planning Type data by user for specific layout selected from PlanningType table method end
        ///</summary>

        ///<summary>
        /// Dashboard View delete Planning Type data by user for specific layout selected from PlanningType table method start
        ///</summary>
        public ActionResult DeletPlanningType(int LayoutID, string PlanningTypeRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    int planningTypeId = 0;
                    var Plantype = PlanningTypeRefId.Split(',').Where(x => x.Trim().Length > 0).Select(x => x).ToList();
                    foreach (string i in Plantype)
                    {
                        Int32.TryParse(i, out planningTypeId);
                        DashboardLogicRepository.DeletePlanningType(LayoutID, planningTypeId);
                    }
                    return Json("Deleted");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View delete Planning Type data by user for specific layout selected from PlanningType table method end
        ///</summary>


        ///<summary>
        /// Dashboard View get Planning Specification Master data from PlanningSpecificationMaster table method start
        ///</summary>
        public ActionResult GetPlanningSpecificationMaster(string PlanningTypeMasterRefId, int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var numbers = PlanningTypeMasterRefId.Split(',').Select(Int32.Parse).ToList();
                    int[] PlanningTypeMastRefId = numbers.ToArray();
                    return Json(DashboardLogicRepository.GetPlanningSpecificationMaster(PlanningTypeMastRefId, LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Dashboard View get Planning Specification Master data from PlanningSpecificationMaster table method end
        ///</summary>

        ///<summary>
        /// Dashboard View save Planning Specification data by user for specific layout selected from PlanningSpecification table method start
        ///</summary>
        public ActionResult SavePlanningSpecification(int LayoutID, string PlanningSpecificationRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var planSpecificationList = PlanningSpecificationRefId.Split(',').ToList();
                    var planningSpecificationList = DashboardLogicRepository.ConsolidatedPlanningSpecification(LayoutID, planSpecificationList);

                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    List<Layout> LayoutDetails = DashboardLogicRepository.GetLayoutDetails(LayoutID);
                    Layout LayoutDetailsupdate = new Layout();
                    LayoutDetailsupdate.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                    LayoutDetailsupdate.LayoutId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.LayoutId).First();
                    LayoutDetailsupdate.UpdatedBy = userCredentials.id.ToString();
                    LayoutDetailsupdate.UpdatedOn = time;
                    CustomerLogicRepository.updateLayout(LayoutDetailsupdate);
                    bool isCopyLayout = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.IsCopyLayout).First();
                    foreach (var planningSpecification in planningSpecificationList)
                    {
                        if (planningSpecification.PlanningSpecificationId == 1 && planningSpecification.IsActive)
                        {
                            List<DefaultActivities> DA = DashboardLogicRepository.GetDefaultActivitiesOBCPauseTrip(LayoutID);
                            foreach (var Values in DA)
                            {
                                ActivitiesRegistration RA = new ActivitiesRegistration();
                                RA.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                                RA.LayoutRefId = LayoutID;
                                RA.ActivitiesRegistrationName = Values.ActivitiesRegistrationName;
                                RA.WorkingCodeRefId = Values.WorkingCodeRefId;
                                RA.PlanningRefId = Values.PlanningRefId;
                                RA.PlanningOverviewRefId = Values.PlanningOverviewRefId;
                                RA.SpecificRefId = Values.SpecificRefId;
                                RA.Visible = Values.Visible;
                                RA.ConfirmButton = Values.ConfirmButton;
                                RA.PTORefId = Values.PTORefId;
                                RA.InterruptibleByRefId = Values.InterruptibleByRefId;
                                RA.TransportType = Values.TransportType;
                                RA.EmptyFullSolo = Values.EmptyFullSolo;
                                RA.Flexactivity = Values.Flexactivity;
                                RA.ISModification = Values.ISModification;
                                RA.ActRegPosition = 0;
                                RA.CreatedBy = userCredentials.id.ToString();
                                RA.CreatedOn = time;
                                RA.OBCDefault = Values.OBCDefault;
                                RA.DefaultActivitiesRefId = Values.ActivitiesRegistrationId;

                                if (isCopyLayout)
                                {
                                    bool isTripDefaultExist = false;
                                    isTripDefaultExist = DashboardLogicRepository.UpdateTripDefaultActivitiesRegisration(LayoutID, Values.ActivitiesRegistrationId);

                                    if (isTripDefaultExist)
                                        DashboardLogicRepository.SaveActivitiesRegistration(RA);
                                }
                                else
                                    DashboardLogicRepository.SaveActivitiesRegistration(RA);

                            }
                        }
                        if (planningSpecification.PlanningSpecificationId == 2 && planningSpecification.IsActive)
                        {
                            List<DefaultActivities> DA = DashboardLogicRepository.GetDefaultActivitiesOBCPlanningspec(LayoutID);
                            foreach (var Values in DA)
                            {
                                ActivitiesRegistration RA = new ActivitiesRegistration();
                                RA.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                                RA.LayoutRefId = LayoutID;
                                RA.ActivitiesRegistrationName = Values.ActivitiesRegistrationName;
                                RA.WorkingCodeRefId = Values.WorkingCodeRefId;
                                RA.PlanningRefId = Values.PlanningRefId;
                                RA.PlanningOverviewRefId = Values.PlanningOverviewRefId;
                                RA.SpecificRefId = Values.SpecificRefId;
                                RA.Visible = Values.Visible;
                                RA.ConfirmButton = Values.ConfirmButton;
                                RA.PTORefId = Values.PTORefId;
                                RA.InterruptibleByRefId = Values.InterruptibleByRefId;
                                RA.TransportType = Values.TransportType;
                                RA.EmptyFullSolo = Values.EmptyFullSolo;
                                RA.Flexactivity = Values.Flexactivity;
                                RA.ISModification = Values.ISModification;
                                RA.ActRegPosition = 0;
                                RA.CreatedBy = userCredentials.id.ToString();
                                RA.CreatedOn = time;
                                RA.OBCDefault = Values.OBCDefault;
                                RA.DefaultActivitiesRefId = Values.ActivitiesRegistrationId;

                                if (isCopyLayout)
                                {
                                    bool isTripDefaultExist = false;
                                    isTripDefaultExist = DashboardLogicRepository.UpdateTripDefaultActivitiesRegisration(LayoutID, Values.ActivitiesRegistrationId);

                                    if (isTripDefaultExist)
                                        DashboardLogicRepository.SaveActivitiesRegistration(RA);
                                }
                                else
                                    DashboardLogicRepository.SaveActivitiesRegistration(RA);
                            }
                        }

                        PlanningSpecification PlanningSpecificationdetails = new PlanningSpecification();
                        PlanningSpecificationdetails.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First(); ;
                        PlanningSpecificationdetails.LayoutRefId = LayoutID;
                        PlanningSpecificationdetails.PlanningSpecificationRefId = planningSpecification.PlanningSpecificationId;
                        PlanningSpecificationdetails.CreatedBy = userCredentials.id.ToString();
                        PlanningSpecificationdetails.CreatedOn = time;
                        PlanningSpecificationdetails.IsModification = planningSpecification.IsModification;
                        PlanningSpecificationdetails.IsActive = planningSpecification.IsActive;
                        DashboardLogicRepository.SavePlanningSpecification(PlanningSpecificationdetails);
                    }
                    return Json("Insertedss");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View save Planning Specification data by user for specific layout selected from PlanningSpecification table method end
        ///</summary>

        ///<summary>
        /// Dashboard View delete Planning Specification data by user for specific layout selected from PlanningSpecification table method start
        ///</summary>
        public ActionResult DeletPlanningSpecification(int LayoutID, string PlanningSpecificationRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var planningSpecificationList = PlanningSpecificationRefId.Split(',').Where(x => x.Trim().Length > 0).ToList();
                    foreach (string i in planningSpecificationList)
                    {
                        DashboardLogicRepository.DeletPlanningSpecification(LayoutID, Convert.ToInt16(i));
                    }
                    return Json("Deleted");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Dashboard View delete Planning Specification data by user for specific layout selected from PlanningSpecification table method end
        ///</summary>

        ///<summary>
        /// Dashboard View delete Planning Trip data by user for specific layout selected from Activitiesregistration table method start
        ///</summary>
        public ActionResult DeletPlanningTrip(int LayoutID, string trip)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    string[] Plantype = trip.Split(',');
                    foreach (string i in Plantype)
                    {
                        DashboardLogicRepository.DeletActivities(LayoutID, i);
                    }
                    return Json("Deleted");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Dashboard View delete Planning Trip data by user for specific layout selected from Activitiesregistration table method end
        ///</summary>

        ///<summary>
        /// Dashboard View show Planning Specification data by user for specific layout selected from PlanningSpecification table method start
        ///</summary>
        public ActionResult GetPlanningSpecification(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetPlanningSpecification(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetDocumentType(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetDocumentType(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetDocumentTypeFlex(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetDocumentTypeFlex(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult SaveDocumentTypeFlex(int LayoutID, string DocumentTypeIds)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {

                    List<int> documentTypeList = DocumentTypeIds.Split('-').Where(x => x.Trim().Length > 0).Select(x => Int32.Parse(x)).ToList();

                    var userCredentials = GetUserCredentials();
                    foreach (var docmenttypeid in documentTypeList)
                    {
                        DocumentTypeFlex documenttypeflexmodal = new DocumentTypeFlex();
                        documenttypeflexmodal.DocumentTypeRefId = docmenttypeid;
                        documenttypeflexmodal.LayoutRefId = LayoutID;
                        documenttypeflexmodal.IsActive = true;
                        documenttypeflexmodal.CreatedBy = userCredentials.id.ToString();
                        documenttypeflexmodal.CreatedOn = DateTime.Now;
                        documenttypeflexmodal.IsModification = (int)EnumCopyColorCode.Green;

                        DashboardLogicRepository.SaveDocumentTypeFlex(documenttypeflexmodal);
                    }

                    return Json("Success");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteDocumentTypeFlex(int DocumentTypeFlexId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.DeleteDocumentTypeFlex(DocumentTypeFlexId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult RevertDocumentTypeFlex(int documentTypeFlexId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.RevertDocumentTypeFlex(documentTypeFlexId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        ///<summary>
        /// Dashboard View show Planning Specification data by user for specific layout selected from PlanningSpecification table method end
        ///</summary>

        ///<summary>
        /// Dashboard View get Allowances Master data from AllowancesMaster based on OBC type selection table method start
        ///</summary>
        public ActionResult GetAllowancesMaster(int OBCTypeRefId, int CustomerId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetAllowancesMaster(OBCTypeRefId, CustomerId, layoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View get Allowances Master data from AllowancesMaster based on OBC type selection table method end
        ///</summary>

        ///<summary>
        /// Dashboard View get Allowances  data based on layout from Allowances table method start
        ///</summary>
        public ActionResult GetAllowances(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetAllowances(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View get Allowances  data based on layout from Allowances table method end
        ///</summary>

        ///<summary>
        /// Dashboard View save new allowance type for specific customer entered by users to Allowances master table method start
        ///</summary>
        public ActionResult SaveAllowancesMaster(int oBCTypeRefId, string allowancesMasterDescription, string allowanceCode, int customerId, int allowanceMasId, int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    AllowancesMaster AllowanceMas = new AllowancesMaster();
                    AllowanceMas.AllowancesMasterId = allowanceMasId;
                    AllowanceMas.AllowancesMasterDescription = allowancesMasterDescription;
                    AllowanceMas.AllowancesCode = allowanceCode;
                    AllowanceMas.OBCTypeRefId = oBCTypeRefId;
                    AllowanceMas.CustomerRefId = customerId;
                    AllowanceMas.IS_Active = 1;
                    AllowanceMas.CreatedBy = userCredentials.id.ToString();
                    AllowanceMas.CreatedOn = time;

                    var allawanceResult = DashboardLogicRepository.SaveAllowancesMaster(AllowanceMas);
                    Allowances Allowancesdetails = new Allowances();
                    Allowancesdetails.CustomerRefId = customerId;
                    Allowancesdetails.LayoutRefId = layoutId;
                    Allowancesdetails.AllowancesRefId = allawanceResult.AllowancesMasterId;
                    Allowancesdetails.CreatedBy = userCredentials.id.ToString();
                    Allowancesdetails.CreatedOn = time;
                    Allowancesdetails.IsModification = (int)EnumCopyColorCode.Green;
                    Allowancesdetails.IsActive = true;
                    DashboardLogicRepository.SaveAllowances(Allowancesdetails);

                    return Json(allawanceResult);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Dashboard View save new allowance type for specific customer entered by users to Allowances master table method end
        ///</summary>

        ///<summary>
        /// Dashboard View save allowance for specific layout to Allowances table method start
        ///</summary>
        public ActionResult SaveAllowances(int LayoutID, string AllowancesRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    if (AllowancesRefId != null)
                    {

                        var allowanceIdList = AllowancesRefId.Split(',').ToList();
                        var allowanceList = DashboardLogicRepository.ConsolidatedAllowances(LayoutID, allowanceIdList);

                        DateTime time = DateTime.Now;
                        var userCredentials = GetUserCredentials();
                        List<Layout> LayoutDetails = DashboardLogicRepository.GetLayoutDetails(LayoutID);
                        Layout LayoutDetailsupdate = new Layout();
                        LayoutDetailsupdate.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                        LayoutDetailsupdate.LayoutId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.LayoutId).First();
                        LayoutDetailsupdate.UpdatedBy = userCredentials.id.ToString();
                        LayoutDetailsupdate.UpdatedOn = time;
                        CustomerLogicRepository.updateLayout(LayoutDetailsupdate);
                        foreach (Allowances allowance in allowanceList)
                        {
                            Allowances Allowancesdetails = new Allowances();
                            Allowancesdetails.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                            Allowancesdetails.LayoutRefId = LayoutID;
                            Allowancesdetails.AllowancesRefId = allowance.AllowancesRefId;
                            Allowancesdetails.CreatedBy = userCredentials.id.ToString();
                            Allowancesdetails.CreatedOn = time;
                            Allowancesdetails.IsModification = allowance.IsModification;
                            Allowancesdetails.IsActive = allowance.IsActive;
                            DashboardLogicRepository.SaveAllowances(Allowancesdetails);
                        }
                    }
                    return Json("Inserted");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View save allowance for specific layout to Allowances table method end
        ///</summary>

        ///<summary>
        /// Dashboard View delete allowance for specific layout to Allowances table method start
        ///</summary>
        public ActionResult DeletAllowances(int LayoutID, string AllowancesRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    string[] Allowances = AllowancesRefId.Split(',');
                    foreach (string i in Allowances)
                    {
                        DashboardLogicRepository.DeletAllowances(LayoutID, Convert.ToInt16(i));
                    }
                    return Json("Deleted");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard View delete allowance for specific layout to Allowances table method end
        ///</summary>


        ///<summary>
        /// Dashboard Page set the default language for the specific layout Starts here
        ///</summary>
        public ActionResult SetDefaultMainLanguage(int layoutId, int languageId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    return Json(DashboardLogicRepository.SetMainLanguage(layoutId, languageId, userCredentials.id, time));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page set the default language for the specific layout end here
        ///</summary>

        ///<summary>
        /// Dashboard Page delete the used language for the specific layout Starts here
        ///</summary>
        public ActionResult DeleteUsedLanguage(int layoutId, int languageId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    return Json(DashboardLogicRepository.DeleteUsedLanguage(layoutId, languageId, userCredentials.id, time));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page delete the used language for the specific layout end here
        ///</summary>

        ///<summary>
        /// Dashboard Page delete the transport type for the specific layout Starts here
        ///</summary>
        public ActionResult DeleteTransportType(int layoutId, int transporttypeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    var result = (DashboardLogicRepository.DeleteTransportType(layoutId, transporttypeId, userCredentials.id, time));
                    if (result.Trim().Length > 0)
                        return Json(new { error = result });
                    else
                        return Json(result);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult DeleteAllowance(int LayoutId, int AllowanceMasterId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    var result = (DashboardLogicRepository.DeleteAllowance(LayoutId, AllowanceMasterId, userCredentials.id, time));
                    if (result.Trim().Length > 0)
                        return Json(new { error = result });
                    else
                        return Json(result);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult UpdatelayoutStatus(int layoutId, int transporttypeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    var result = (DashboardLogicRepository.DeleteTransportType(layoutId, transporttypeId, userCredentials.id, time));
                    if (result.Trim().Length > 0)
                        return Json(new { error = result });
                    else
                        return Json(result);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetLayoutNotes(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetLayoutNotes(LayoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page delete the transport type for the specific layout end here
        ///</summary>

        ///<summary>
        /// Dashboard Page get layout is copied or new layout for the specific layout Starts here
        ///</summary>
        public ActionResult GetLayoutorgin(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    var result = (DashboardLogicRepository.GetLayoutorgin(layoutId));
                    return Json(result);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page get layout is copied or new layout for the specific layout end here
        ///</summary>

        #region 
        ///<summary>
        /// Dashboard Page grid drop down data binding Starts here
        ///</summary>

        ///<summary>
        /// Dashboard Page activities registration grid working code drop down data binding Starts here
        ///</summary>
        public ActionResult GetWorkingCode()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetWorkingCode());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page activities registration grid working code drop down data binding end here
        ///</summary>

        ///<summary>
        /// Dashboard Page activities registration grid planning drop down data binding Starts here
        ///</summary>
        public ActionResult GetPlanning()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetPlanning());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page activities registration grid planning drop down data binding end here
        ///</summary>

        ///<summary>
        /// Dashboard Page activities registration grid planning overview drop down data binding Starts here
        ///</summary>
        public ActionResult GetPlanningOverview(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetPlanningOverview(LayoutId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page activities registration grid planning overview drop down data binding end here
        ///</summary>

        ///<summary>
        /// Dashboard Page activities registration grid specific drop down data binding Starts here
        ///</summary>
        public ActionResult GetSpecific(int DefaultActivitiesId, string IsPauseTrip, int OBCTypeId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    bool ispausetrip = IsPauseTrip == "true" ? true : false;

                    var resultList = DashboardLogicRepository.GetSpecific(DefaultActivitiesId, ispausetrip, OBCTypeId);
                    return Json(resultList);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page activities registration grid specific overview drop down data binding end here
        ///</summary>

        ///<summary>
        /// Dashboard Page activities registration grid PTO drop down data binding Starts here
        ///</summary>
        public ActionResult GetPTO()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetPTO());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page activities registration grid PTO drop down data binding end here
        ///</summary>

        ///<summary>
        /// Dashboard Page activities registration grid Interruptible By drop down data binding Starts here
        ///</summary>
        public ActionResult GetInterruptibleBy()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetInterruptibleBy());
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page activities registration grid Interruptible By drop down data binding end here
        ///</summary>

        ///<summary>
        /// Dashboard Page grid drop down data binding end here
        ///</summary>
        #endregion


        ///<summary>
        /// Dashboard Page activities registration grid activities registration data binding from activities registration table Starts here
        ///</summary>
        public ActionResult GetActivitiesRegistration(int LayoutID, string LanguageName)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.GetActivitiesRegistration(LayoutID, LanguageName));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page activities registration grid activities registration data binding from activities registration table end here
        ///</summary>

        ///<summary>
        /// Dashboard Page activities registration grid activities registration data save to activities registration table Starts here
        ///</summary>
        public ActionResult SaveActivitiesRegistration(int LayoutID, string ACTReg)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    ActivitiesRegistrationDetails actRegModal = JsonConvert.DeserializeObject<ActivitiesRegistrationDetails>(WebUtility.UrlDecode(ACTReg));
                    if (actRegModal.Flexactivity == true)
                    {
                        if (DashboardLogicRepository.GetOBCTypeByLayout(LayoutID) == (int)EnumOBCType.TX_SKY)
                            DashboardLogicRepository.UpdateOBCTypesFlex((int)EnumOBCType.TX_SKY_FLEX, LayoutID);
                    }
                    List<Layout> LayoutDetails = DashboardLogicRepository.GetLayoutDetails(LayoutID);
                    Layout LayoutDetailsupdate = new Layout();
                    LayoutDetailsupdate.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                    LayoutDetailsupdate.LayoutId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.LayoutId).First();
                    LayoutDetailsupdate.UpdatedBy = userCredentials.id.ToString();
                    LayoutDetailsupdate.UpdatedOn = time;
                    CustomerLogicRepository.updateLayout(LayoutDetailsupdate);
                    if (actRegModal.ActivitiesRegistrationId == null)
                    {
                        ActivitiesRegistration RA = new ActivitiesRegistration();
                        RA.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                        RA.LayoutRefId = LayoutID;
                        RA.ActivitiesRegistrationName = (actRegModal.ActivitiesRegistrationName.Contains("ƒ") ? actRegModal.ActivitiesRegistrationName.Replace("ƒ", "+") : actRegModal.ActivitiesRegistrationName);
                        RA.WorkingCodeRefId = actRegModal.WorkingCodeRefId;
                        RA.PlanningRefId = actRegModal.PlanningRefId;
                        RA.PlanningOverviewRefId = actRegModal.PlanningOverviewRefId;
                        RA.SpecificRefId = actRegModal.SpecificRefId;
                        RA.Visible = actRegModal.Visible;
                        RA.ConfirmButton = actRegModal.ConfirmButton;
                        RA.PTORefId = actRegModal.PTORefId;
                        RA.InterruptibleByRefId = actRegModal.InterruptibleByRefId;
                        RA.TransportType = actRegModal.TransportType;
                        RA.EmptyFullSolo = actRegModal.EmptyFullSolo;
                        RA.Flexactivity = actRegModal.Flexactivity;
                        RA.ActRegPosition = 0;
                        RA.ISModification = 0;
                        RA.CreatedBy = userCredentials.id.ToString();
                        RA.CreatedOn = time;

                        var resultDashboard = DashboardLogicRepository.SaveActivitiesRegistration(RA);
                        return Json(resultDashboard.ActivitiesRegistrationId);
                    }
                    else
                    {
                        ActivitiesRegistration RA = new ActivitiesRegistration();
                        RA.ActivitiesRegistrationId = Convert.ToInt16(actRegModal.ActivitiesRegistrationId);
                        RA.CustomerRefId = LayoutDetails.Where(y => y.LayoutId == LayoutID).Select(x => x.CustomerRefId).First();
                        RA.LayoutRefId = LayoutID;
                        RA.ActivitiesRegistrationName = (actRegModal.ActivitiesRegistrationName.Contains("ƒ") ? actRegModal.ActivitiesRegistrationName.Replace("ƒ", "+") : actRegModal.ActivitiesRegistrationName);
                        RA.WorkingCodeRefId = actRegModal.WorkingCodeRefId;
                        RA.PlanningRefId = actRegModal.PlanningRefId;
                        RA.PlanningOverviewRefId = actRegModal.PlanningOverviewRefId;
                        RA.SpecificRefId = actRegModal.SpecificRefId;
                        RA.Visible = actRegModal.Visible;
                        RA.ConfirmButton = actRegModal.ConfirmButton;
                        RA.PTORefId = actRegModal.PTORefId;
                        RA.InterruptibleByRefId = actRegModal.InterruptibleByRefId;
                        RA.TransportType = actRegModal.TransportType;
                        RA.EmptyFullSolo = actRegModal.EmptyFullSolo;
                        RA.Flexactivity = actRegModal.Flexactivity;
                        if (actRegModal.ISModification == 1)
                        {
                            RA.ISModification = 2;
                        }
                        else if (actRegModal.ISModification == 2)
                        {
                            RA.ISModification = 2;
                        }
                        else if (actRegModal.ISModification == 3)
                        {
                            RA.ISModification = 3;
                        }
                        else
                        {
                            RA.ISModification = 0;
                        }
                        RA.CreatedBy = userCredentials.id.ToString();
                        RA.CreatedOn = time;
                        RA.UpdatedBy = userCredentials.id.ToString();
                        RA.UpdatedOn = time;
                        var resultDashboard = DashboardLogicRepository.UpdateActivitiesRegistration(RA);

                        return Json(resultDashboard.ActivitiesRegistrationId);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Dashboard Page activities registration grid activities registration data save to activities registration table end here
        ///</summary>

        ///<summary>
        /// Dashboard Page activities registration grid activities registration data modified ismodification 3 that means delete from activities registration table Starts here
        /// That means not completely deleted from table
        ///</summary>
        public ActionResult DeleteActivitiesRegistration(int LayoutID, int activityId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.DeleteActivity(LayoutID, activityId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page activities registration grid activities registration data modified ismodification 3 that means delete from activities registration table end here
        /// That means not completely deleted from table
        ///</summary>


        ///<summary>
        /// Dashboard Page activities registration grid activities registration data delete from activities registration table Starts here
        /// That means completely deleted from table
        ///</summary>
        public ActionResult DeleteActivityNew(int LayoutID, int activityId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.DeleteActivityNew(LayoutID, activityId));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Dashboard Page activities registration grid activities registration data delete from activities registration table end here
        /// That means completely deleted from table
        ///</summary>

        public ActionResult GetDefaultLanguage(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var languagename = translatorRepository.GetDefaultLanguage(layoutId);
                    return Json(languagename);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }

        public ActionResult GetRolesAccessList()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    var rolesAccessList = GetRolesAccessList(userCredentials.permissions);
                    return Json(rolesAccessList);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetLayoutStatusAccessList(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var layoutStatusAccessList = GetLayoutStatusDetailList(layoutId);
                    return Json(layoutStatusAccessList);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetRoleWithLayoutStatusAccessList(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    var roleWithLayoutStatusAccessList = GetRoleWithLayoutStatusDetailList(layoutId, userCredentials.permissions);
                    return Json(roleWithLayoutStatusAccessList);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult TXDesignConvertion(int layoutId, string functionId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var sourceId = DashboardLogicRepository.GetCustomerSourceId(layoutId);
                    if (functionId == "1")
                    {
                        SOAPManual(layoutId.ToString(), sourceId.ToString(), functionId);
                        return Json("Success");
                    }
                    else if (functionId == "2")
                    {
                        var webserviceresult = SOAPManual(layoutId.ToString(), sourceId.ToString(), functionId);
                        XDocument doc1 = XDocument.Parse(webserviceresult, LoadOptions.SetLineInfo);
                        XNode Vehicles = GetElement(doc1, "Status");
                        string jsonTextVehicles = JsonConvert.SerializeXNode(Vehicles);
                        dynamic dynVehicles = JsonConvert.DeserializeObject<ExpandoObject>(jsonTextVehicles);
                        ExpandoObject XMLDatas = (dynVehicles);
                        if (XMLDatas != null)
                        {
                            var status = XMLDatas.Where(x => x.Key == "a:Status").Select(x => x.Value).FirstOrDefault();
                            return Json(status);
                        }
                        else
                        {
                            return Json("Not Started");
                        }
                    }
                    else
                    {
                        return Json("Success");
                    }
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        private XNode GetElement(XDocument doc, string elementName)
        {
            foreach (XNode node in doc.DescendantNodes())
            {
                if (node is XElement)
                {
                    XElement element = (XElement)node;
                    if (element.Name.LocalName.Equals(elementName))
                        return node;
                }
            }
            return null;
        }

        public String SOAPManual(string layoutId, string skyCustomerId, string functionId)
        {
            string url = serviceSettings.TxDesignurl;
            string action = GetWebServiceActionName(functionId);

            XDocument soapEnvelopeXml = CreateSoapEnvelope(layoutId, skyCustomerId, functionId);
            HttpWebRequest webRequest = CreateWebRequest(url, action, serviceSettings.SOAPAction);

            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);
            string result = string.Empty;
            try
            {

                using (WebResponse response = webRequest.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        result = rd.ReadToEnd();
                    }
                }
            }
            catch (WebException webex)
            {

                System.Text.StringBuilder xmlerrorcontent = new System.Text.StringBuilder();
                xmlerrorcontent.Append("<soap:Envelope xmlns:soap='http://www.w3.org/2003/05/soap-envelope' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'><soap:Body><Get_VehiclesResponse xmlns='http://transics.org'> " +
                    "<Get_VehiclesResult><Errors><Error><ErrorCode>ERROR_UNAUTHORIZED</ErrorCode><ErrorCodeExplanation>" + webex.Message + "</ErrorCodeExplanation><Field>Login.Dispatcher</Field></Error></Errors>" +
                    "</Get_VehiclesResult></Get_VehiclesResponse></soap:Body></soap:Envelope>");
                result = xmlerrorcontent.ToString();
            }
            return result;
        }
        private static HttpWebRequest CreateWebRequest(string WSDL, string Action, string SOAPAction)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(WSDL);
            webRequest.Host = "rnd-qa2016-4.corp.transics.com";
            webRequest.Headers["SOAPAction"] = SOAPAction + Action;
            webRequest.ContentType = "text/xml;charset=UTF-8";// Message = "The remote server returned an error: (415) Cannot process the message because the content type 'application/soap+xml; charset=UTF-8' was not the expected type 'text/xml; charset=utf-8'.."
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }
        private static void InsertSoapEnvelopeIntoWebRequest(XDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }
        private static XDocument CreateSoapEnvelope(string layoutId, string skyCustomerId, string functionId)
        {
            System.Text.StringBuilder xmlcontent = new System.Text.StringBuilder();

            if (functionId == "1")
            {
                xmlcontent.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/' xmlns:con='http://schemas.datacontract.org/2004/07/Contracts.RequestContracts'>");
                xmlcontent.Append("<soapenv:Header/><soapenv:Body><tem:StartIsLayoutConversion> <!--Optional:--> <tem:request><!--Optional:--><con:RequestUserId>0</con:RequestUserId><!--Optional:--><con:SecurityKey>0</con:SecurityKey>");
                xmlcontent.Append("<!--Optional:--><con:Version>0</con:Version><!--Optional:--><con:CreaTisVersionId>" + layoutId + "</con:CreaTisVersionId><!--Optional:--><con:SkyCustomerId>" + skyCustomerId + "</con:SkyCustomerId> ");
                xmlcontent.Append("</tem:request></tem:StartIsLayoutConversion></soapenv:Body></soapenv:Envelope>");
            }
            else if (functionId == "2")
            {
                xmlcontent.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/' xmlns:con='http://schemas.datacontract.org/2004/07/Contracts.RequestContracts'>");
                xmlcontent.Append("<soapenv:Header/><soapenv:Body><tem:GetIsLayoutStatus><!--Optional:--><tem:request><!--Optional:--><con:RequestUserId>0</con:RequestUserId>");
                xmlcontent.Append("<!--Optional:--><con:SecurityKey>0</con:SecurityKey><!--Optional:--><con:Version>0</con:Version><!--Optional:--><con:CreaTisVersionId>" + layoutId + "</con:CreaTisVersionId>");
                xmlcontent.Append("<!--Optional:--><con:SkyCustomerId>" + skyCustomerId + "</con:SkyCustomerId></tem:request></tem:GetIsLayoutStatus></soapenv:Body></soapenv:Envelope>");
            }


            XDocument soapEnvelopeXml = XDocument.Parse(xmlcontent.ToString());
            return soapEnvelopeXml;
        }
        public string GetWebServiceActionName(string functionId)
        {
            switch (functionId)
            {
                case "1":
                    return "StartIsLayoutConversion";
                case "2":
                    return "GetIsLayoutStatus";
                default:
                    return "";
            }
        }
        #endregion
        public ActionResult EditLayoutNotes(int layoutId, int layoutNoteId, string layoutNotes)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(DashboardLogicRepository.EditLayoutNotes(layoutId, layoutNoteId, layoutNotes));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        public ActionResult GetAlertCopiedLayout(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var alertCopiedLayout = DashboardLogicRepository.GetAlertCopiedLayout(layoutId);
                    return Json(alertCopiedLayout);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Dashboard Page Methods End here
        ///</summary>
    }
}
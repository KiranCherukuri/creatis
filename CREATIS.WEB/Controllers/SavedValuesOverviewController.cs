﻿using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using CREATIS.Controllers;
using CREATIS.Infrastructure.IRepository;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CREATIS.WEB.Controllers
{
    public class SavedValuesOverviewController : BaseController
    {
        private readonly ISavedValuesOverviewRepository SavedValuesOverviewRepository;
        public ConnectionStrings connectionStrings { get; }
        public OthersSettings othersSettings { get; }
        public SavedValuesOverviewController(ISavedValuesOverviewRepository InRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings)
        {
            this.SavedValuesOverviewRepository = InRepo;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
        }
        public ActionResult Index(int layoutId)
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();
                    if (HasPrivilege(userCredentials.permissions).Contains("QP"))
                    {
                        LayoutStatusPrivilege(layoutId);
                        RoleWithLayoutStatusPrivilege(layoutId, userCredentials.permissions);
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        AddRecentActivity(layoutId, userCredentials.id);
                        return View();
                    }
                    else
                        return View("Unauthorised");
                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");
        }
        public ActionResult GetSavedValues(int customerRefId, int layoutRefId)
        {
            try
            {
                List<SavedValues> resultList;
                resultList = SavedValuesOverviewRepository.GetSavedValues(customerRefId, layoutRefId);
                return Json(resultList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
        public ActionResult GetAliasList(int layoutRefId)
        {
            try
            {
                List<AliasNameDetails> resultList;
                resultList = SavedValuesOverviewRepository.GetAliasList(layoutRefId);
                return Json(resultList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
        public ActionResult SaveSavedValues(string action, List<AliasNameDetails> added, List<AliasNameDetails> changed)
        {
            System.DateTime time = DateTime.Now;
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    if (changed != null && changed.Count > 0)
                    {
                        foreach (AliasNameDetails d in changed)
                        {
                            var userCredentials = GetUserCredentials();
                            d.UpdatedBy = userCredentials.id.ToString();
                            d.UpdatedOn = time;
                            SavedValuesOverviewRepository.SaveAlias(d);
                        }
                    }
                    if (added != null && added.Count > 0)
                    {
                        foreach (AliasNameDetails d in added)
                        {
                            var userCredentials = GetUserCredentials();
                            d.CreatedBy = userCredentials.id.ToString();
                            d.CreatedOn = time;
                            SavedValuesOverviewRepository.SaveAlias(d);
                        }
                    }
                    return Json("success");
                }
                catch (Exception ex)
                {
                    string e = ex.ToString();
                    return Json(e);
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
    }
}
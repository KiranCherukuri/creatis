using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CREATIS.ApplicationCore.Modals;
using CREATIS.Infrastructure.Repository;
using CREATIS.Infrastructure.IRepository;
using System.Net;
using System.Data.SqlClient;
using CREATIS.WEB.Utility;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.Web;

namespace CREATIS.Controllers
{
    [SessionExpireFilterAttribute]
    public abstract class BaseController : Controller
    {
        private readonly ILoginRepository LoginLogicRepository;
        public BaseController(ILoginRepository LoginLogicRepo)
        {
            this.LoginLogicRepository = LoginLogicRepo;

        }
        public BaseController()
        {

        }
       
        public UserBusinessModal GetUserCredentials()
        {
            UserBusinessModal UserCredentials = HttpContext.Session.GetComplexData<UserBusinessModal>("UserLoginDet");
            return UserCredentials;
        }
        public List<string> HasPrivilege(List<string> RolesList)
        {
            ILoginRepository Login = new LoginRepository();

            var rolesAccessList = Login.GetRolesAccessList(RolesList);
            ViewData["Previlage"] = rolesAccessList;
            return rolesAccessList;
        }
        public List<string> LayoutStatusPrivilege(int layoutId)
        {
            ILoginRepository Login = new LoginRepository();

            var layoutStatusAccessList = Login.GetLayoutStatusAccessList(layoutId);
            ViewData["LayoutStatusPrevilage"] = layoutStatusAccessList;
            return layoutStatusAccessList;
        }
        public List<string> RoleWithLayoutStatusPrivilege(int layoutId, List<string> rolesList)
        {
            ILoginRepository Login = new LoginRepository();

            var roleWithLayoutStatusAccessList = Login.GetRoleWithLayoutStatusAccessList(layoutId, rolesList);
            ViewData["RoleWithLayoutStatusPrevilage"] = roleWithLayoutStatusAccessList;
            return roleWithLayoutStatusAccessList;
        }
        public static List<string> GetRolesAccessList(List<string> RolesList)
        {
            ILoginRepository Login = new LoginRepository();

            var rolesAccessList = Login.GetRolesAccessList(RolesList);
            return rolesAccessList;
        }
        public static List<string> GetLayoutStatusDetailList(int layoutId)
        {
            ILoginRepository Login = new LoginRepository();
            var layoutStatusAccessList = Login.GetLayoutStatusAccessList(layoutId);
            return layoutStatusAccessList;
        }
        public static List<string> GetRoleWithLayoutStatusDetailList(int layoutId, List<string> rolesList)
        {
            ILoginRepository Login = new LoginRepository();
            var roleWithLayoutStatusAccessList = Login.GetRoleWithLayoutStatusAccessList(layoutId, rolesList);
            return roleWithLayoutStatusAccessList;
        }
        public int GetLoginCustomer(string company, int customerId)
        {
            ILoginRepository Login = new LoginRepository();

            var customerRefId = Login.GetLoginCustomer(company, customerId);
            return customerRefId;
        }
        public List<int> GetCustomerLayoutList(string company, int customerId)
        {
            ILoginRepository Login = new LoginRepository();

            var customerRefId = Login.GetLoginCustomer(company, customerId);
            var customeLayoutList = Login.GetCustomerLayoutList(company, customerRefId);
            return customeLayoutList;
        }
        public int GetLayoutCustomerId(int layoutId)
        {
            ILoginRepository Login = new LoginRepository();

            var customerRefId = Login.GetLayoutCustomerId(layoutId);
            return customerRefId;
        }

        public bool IsCustomerLogin(List<string> RolesList)
        {
            ILoginRepository Login = new LoginRepository();

            var isCustomerLogin = Login.IsCustomerLogin(RolesList);
            return isCustomerLogin;
        }
        public string AddRecentActivity(int layoutRefId, string userId)
        {
            RecentActivity recentActivity = new RecentActivity();

            recentActivity.LayoutRefId = layoutRefId;
            recentActivity.Status = 1;
            recentActivity.CreatedBy = userId;
            recentActivity.CreatedOn = DateTime.Now;
            recentActivity.UpdatedBy = userId;
            recentActivity.UpdatedOn = DateTime.Now;

            ILoginRepository Login = new LoginRepository();
            return (Login.SaveRecentActivity(recentActivity));
        }

        public enum StatusCode
        {
            /// <summary>
            /// Data Saved Sucessfully.
            /// </summary>
            E00,
            /// <summary>
            ///  Error In Database Connection.
            /// </summary>
            E01,
            /// <summary>
            /// Internal Server Error.
            /// </summary>
            E02,
            /// <summary>
            /// No Records Found.
            /// </summary>
            E03,
            /// <summary>
            /// No Internet Connection.
            /// </summary>
            E04,
            /// <summary>
            /// Access Denied.
            /// </summary>
            E05
        }
        protected string GetStatusMessage(StatusCode code)
        {
            switch (code)
            {
                case StatusCode.E00:
                    return "Data Saved Sucessfully";
                case StatusCode.E01:
                    return "Error In Database Connection";
                case StatusCode.E02:
                    return "Internal Server Error";
                case StatusCode.E03:
                    return "No Records Found";
                case StatusCode.E04:
                    return "No Internet Connection";
                case StatusCode.E05:
                    return "Access Denied";
                default:
                    return "Invalid Exception";
            }
        }
        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return true;
            }
        }
        public bool IsServerConnected(ConnectionStrings conn)
        {

            //using (var l_oConnection = new SqlConnection(@"Data Source=SQLFIELDTEST;Initial Catalog=CREATIS;Integrated Security=True"))
            //using (var l_oConnection = new SqlConnection(@"Data Source=RND-BA-SQL2;Initial Catalog=CREATIS;Integrated Security=True"))
            //using (var l_oConnection = new SqlConnection(@"Data Source=WDEGSSQLT2;Initial Catalog=CREATIS;Persist Security Info=True;User ID=Creatis;Password=Creatis@sqlt2"))
            //using (var l_oConnection = new SqlConnection(@"Data Source=WDEGSSQLT2;Initial Catalog=TXCreatis;Persist Security Info=True;User ID=Creatis;Password=Creatis@sqlt2"))
            using (var l_oConnection = new SqlConnection(conn.DB_Context))
            {
                try
                {
                    l_oConnection.Open();
                    return true;
                }
                catch (SqlException)
                {
                    return false;
                }
            }
        }
        public enum LanguagesEnum
        {
            English = 1,
            Arabic = 2,
            Bulgarian = 3,
            Croatian = 4,
            Czech = 5,
            Danish = 6,
            Dutch = 7,
            Estonian = 8,
            Finnish = 9,
            French = 10,
            German = 11,
            Greek = 12,
            Hungarian = 13,
            Italian = 14,
            Latvian = 15,
            Lithuanian = 16,
            Macedonian = 17,
            Norwegian = 18,
            Polish = 19,
            Portuguese = 20,
            Romanian = 21,
            Russian = 22,
            Slovak = 23,
            Slovene = 24,
            Spanish = 25,
            Swedish = 26,
            Turkish = 27,
            Ukrainian = 28,
            Belarusian = 29
        }
        public enum LangRefTable
        {
            ActivitiesRegistration = 1,
            InfoColumns = 2,
            Allowances = 3,
            ChioceProperties = 4,
            AddChioceProperties = 5,
            EntryProperties = 6,
            InfomessageProperties = 7,
            TextMessageProperties = 8,
            VariableListProperties = 9,
            PlanningselectProperties = 10,
            DocumetScanProperties = 11,
            InfoMessageContentDetail = 12,
            TransportTypeProperties = 13,
            TransportTypePropertiesDetail = 14,
            TrailerProperties = 15,
            Questions = 16,
            SetActionValueProperties = 17,
            ExtraInfoProperties = 18,
            ExtraInfo = 19,
            PalletsProperties = 20,
            Pallets = 21,
            ProblemsProperties = 22,
            Problems = 23,
            SignaturesProperties = 24,
            PictureProperties = 25,
            DocumentScannerProperties = 26,
            DocumentScanner = 27
        }
        public enum LangRefColTable
        {
            ActivitiesRegistration_ActivitiesRegistrationName = 1,
            InfoColumns_IC_Column_Name = 2,
            Allowances_AllowancesMasterDescription = 3,
            ChioceProperties_Title = 4,
            AddChioceProperties_Title = 5,
            EntryProperties_Title = 6,
            EntryProperties_MaskText = 7,
            InfomessageProperties_Title = 8,
            InfoMessageContentDetail_MessageContent = 9,
            TextMessageProperties_Messagecontent = 10,
            VariableListProperties_Title = 11,
            PlanningselectProperties_Title = 12,
            PlanningselectProperties_OtherTitle = 13,
            PlanningselectProperties_Fixedvalue = 14,
            DocumetScanProperties_Title = 15,
            TransportTypeProperties_Title = 16,
            TransportTypePropertiesDetail_Title = 17,
            TrailerProperties_Title = 18,
            TrailerProperties_MaskText = 19,
            Questions_Name = 20,
            EntryProperties_FixedValue = 21,
            SetActionValueProperties_Value = 22,
            SetActionValueProperties_InfoColumnFixedText = 23,
            ExtraInfoProperties_Title = 24,
            ExtraInfoProperties_AlternativeTextOnPlaceLevel = 25,
            ExtraInfoProperties_AlternativeTextOnJobLevel = 26,
            ExtraInfoProperties_AlternativeTextOnProductLevel = 27,
            ExtraInfo_Title = 28,
            ExtraInfo_MaskText = 29,
            PalletsProperties_Title = 30,
            PalletsProperties_AlternativeTextOnPlaceLevel = 31,
            PalletsProperties_AlternativeTextOnJobLevel = 32,
            PalletsProperties_AlternativeTextOnProductLevel = 33,
            Pallets_Title = 34,
            ProblemsProperties_Title = 35,
            ProblemsProperties_AlternativeTextOnPlaceLevel = 36,
            ProblemsProperties_AlternativeTextOnJobLevel = 37,
            ProblemsProperties_AlternativeTextOnProductLevel = 38,
            Problems_Title = 39,
            Problems_MaskText = 40,
            SignaturesProperties_Title = 41,
            SignaturesProperties_AlternativeTextOnPlaceLevel = 42,
            SignaturesProperties_AlternativeTextOnJobLevel = 43,
            SignaturesProperties_AlternativeTextOnProductLevel = 44,
            PictureProperties_Title = 45,
            PictureProperties_AlternativeTextOnPlaceLevel = 46,
            PictureProperties_AlternativeTextOnJobLevel = 47,
            PictureProperties_AlternativeTextOnProductLevel = 48,
            DocumentScannerProperties_Title = 49,
            DocumentScannerProperties_AlternativeTextOnPlaceLevel = 50,
            DocumentScannerProperties_AlternativeTextOnJobLevel = 51,
            DocumentScannerProperties_AlternativeTextOnProductLevel = 52,
            DocumentScanner_Title = 53
        }
        public enum EnumLayoutStatus
        {
            InProgress = 1,
            InDevelopment = 2,
            ValidationRequested = 3,
            CheckNeeded = 4,
            ValidatedandPlanned = 5,
            Finished = 6,
            SupportRequested = 7

        }
        public enum PrivilegeConstants
        {
            Create = 1,
            Update = 2,
            Delete = 4
        }
        public enum EnumCopyColorCode
        {
            Green = 0,
            Empty = 1,
            Yellow = 2,
            Red = 3,
        }
        public enum EnumOBCType
        {
            TX_SKY = 1,
            TX_SKY_FLEX = 2,
            TX_SMART = 3,
            TX_GO_FLEX = 4
        }
        public enum EnumSetActionValueType
        {
            NextQP = 1,
            NextView = 2,
            Allowance = 3,
            InfoColumn = 4,
            SavedValue = 5,
            EmptyFullSoloState = 6
        }
    }
}
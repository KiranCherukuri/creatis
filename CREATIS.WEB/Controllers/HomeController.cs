using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using CREATIS.Infrastructure.Repository;
using CREATIS.Infrastructure.IRepository;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Configuration;
using static CREATIS.Infrastructure.Utility;

namespace CREATIS.Controllers
{
    public class HomeController : BaseController
    {

        public readonly IHomeRepository HomeLogicRepository;

        public readonly IDashboardRepository DashboardRepository;
        public ConnectionStrings connectionStrings { get; }
        public OthersSettings othersSettings { get; }
        public HomeController(IHomeRepository HomeLogicRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings, IDashboardRepository dashboardRep)
        {
            this.HomeLogicRepository = HomeLogicRepo;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
            this.DashboardRepository = dashboardRep;
        }

        ///<summary>
        /// Recent Activities Page Methods Starts here
        ///</summary>

        #region 


        ///<summary>
        /// Recent Activities View render method start
        ///</summary>
        public ActionResult Index()
        {
            if (IsServerConnected(connectionStrings))
            {
                var userCredentials = GetUserCredentials();

                if (HasPrivilege(userCredentials.permissions).Contains("H"))
                {
                    ViewBag.username = userCredentials.displayName;
                    ViewBag.SessionLink = othersSettings.SessionLink;
                    return View();
                }
                else
                    return View("Unauthorised");
            }
            else
                return View("DBConnectionError");
        }

        ///<summary>
        /// Recent Activities View render method End
        ///</summary>


        ///<summary>
        /// Recent Activities get customer detail for dropdown method start
        /// To Get from customer table for Customer dropdown list in home page
        ///</summary>
        public ActionResult HomeCustomer()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var customerEnvironment = othersSettings.CustomerEnvironment.Split(",").Where(x => x.Trim().Length > 0).ToList();
                    List<Customers> CustomerList = HomeLogicRepository.Customer().Select(x => new Customers() { CustomerId = x.CustomerId, Name = x.Name, Source = x.Source, Environment = x.Environment, CustomCustomer = x.CustomCustomer, CustomerSystemTypeRefId = x.CustomerSystemTypeRefId }).ToList();
                    var CustomerDropDown = CustomerList.Where(x => (othersSettings.CustomerEnvironment == "All" ? (othersSettings.CustomCustomer == 1 ? (1 == 1) : (x.CustomCustomer == 0)) :
                    ((othersSettings.CustomCustomer == 1 ? ((x.CustomCustomer == 1) || customerEnvironment.Contains(x.Environment)) : (customerEnvironment.Contains(x.Environment) && x.CustomCustomer == 0))))).Select(x => new
                    {
                        x.CustomerId,
                        Cust = x.Name + " (" + x.Source + " - " + x.Environment + ")",
                        x.CustomerSystemTypeRefId
                    }).OrderBy(x => x.Cust).ToList();

                    var userCredentials = GetUserCredentials();
                    if (IsCustomerLogin(userCredentials.permissions))
                    {
                        var customerId = GetLoginCustomer(userCredentials.company, 0);
                        CustomerDropDown = CustomerDropDown.Where(x => x.CustomerId == customerId || x.CustomerSystemTypeRefId == 2).ToList();
                    }

                    return Json(CustomerDropDown);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetCustomerForCopyLayout()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var customerEnvironment = othersSettings.CustomerEnvironment.Split(",").Where(x => x.Trim().Length > 0).ToList();
                    List<Customers> CustomerList = HomeLogicRepository.Customer().Select(x => new Customers() { CustomerId = x.CustomerId, Name = x.Name, Source = x.Source, Environment = x.Environment, CustomCustomer = x.CustomCustomer, CustomerSystemTypeRefId = x.CustomerSystemTypeRefId }).ToList();
                    var CustomerDropDown = CustomerList.Where(x => (othersSettings.CustomerEnvironment == "All" ? (othersSettings.CustomCustomer == 1 ? (1 == 1) : (x.CustomCustomer == 0)) :
                    ((othersSettings.CustomCustomer == 1 ? ((x.CustomCustomer == 1) || customerEnvironment.Contains(x.Environment)) : (customerEnvironment.Contains(x.Environment) && x.CustomCustomer == 0))))).Select(x => new
                    {
                        x.CustomerId,
                        Cust = x.Name + " (" + x.Source + " - " + x.Environment + ")",
                        x.CustomerSystemTypeRefId
                    }).OrderBy(x => x.Cust).ToList();

                    var userCredentials = GetUserCredentials();
                    if (IsCustomerLogin(userCredentials.permissions))
                    {
                        var customerId = GetLoginCustomer(userCredentials.company, 0);
                        CustomerDropDown = CustomerDropDown.Where(x => x.CustomerId == customerId).ToList();
                    }

                    var rolesAccessList = GetRolesAccessList(userCredentials.permissions);
                    if (!rolesAccessList.Contains("CLCND"))
                        CustomerDropDown = CustomerDropDown.Where(x => x.CustomerSystemTypeRefId != (int)EnumCustomerSystemType.Standard).ToList();

                    return Json(CustomerDropDown);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Recent Activities get customer detail for dropdown method end
        ///</summary>


        ///<summary>
        /// Recent Activities get recent 15 created layout detail shown in grid method start
        /// To Get from Recent Activities table for Recent Activities grid in home page
        ///</summary>
        public ActionResult HomeRecentActivities()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    List<UserRecentActivities> recentActivitiesList = HomeLogicRepository.GetRecentActivities(userCredentials.id.ToString()).ToList();
                    if (IsCustomerLogin(userCredentials.permissions))
                    {
                        var customerLayoutList = GetCustomerLayoutList(userCredentials.company, 0);
                        recentActivitiesList = recentActivitiesList.Where(x => customerLayoutList.Contains(x.LayoutRefId)).ToList();
                    }

                    return Json(recentActivitiesList);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Recent Activities get recent 15 created layout detail shown in grid method end
        ///</summary>

        ///<summary>
        /// Recent Activities delete particular layout from syncfusion grid method start
        ///</summary>
        public ActionResult DeleteLayout(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();

                    if (GetRolesAccessList(userCredentials.permissions).Contains("HDB"))
                    {
                        return Json(HomeLogicRepository.DeleteDashboard(layoutId));
                    }
                    else
                        return Json(new { error = GetStatusMessage(StatusCode.E05) });

                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        ///<summary>
        /// Recent Activities delete particular layout from syncfusion grid method end
        ///</summary> 

        public ActionResult UpdateLayout(int LayoutId, string LayoutName, string OldLayoutName)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    if (GetRolesAccessList(userCredentials.permissions).Contains("COEB"))
                    {
                        var returnresult = HomeLogicRepository.UpdateLayout(LayoutId, LayoutName);
                        if (returnresult.Trim().Length == 0)
                        {
                            LayoutNotes layoutNotes = new LayoutNotes();
                            layoutNotes.LayoutRefId = LayoutId;
                            layoutNotes.Notes = "Layout name " + '"' + OldLayoutName + '"' + "  is updated to " + '"' + LayoutName + '"';
                            layoutNotes.CreatedBy = userCredentials.id.ToString();
                            layoutNotes.CreatedOn = DateTime.Now;
                            layoutNotes.CreatedByName = userCredentials.displayName;
                            DashboardRepository.UpdateLayoutNotes(layoutNotes);

                            return Json(returnresult);
                        }
                        else
                            return Json(new { error = returnresult });
                    }
                    else
                        return Json(new { error = GetStatusMessage(StatusCode.E05) });

                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult RemoveRecentActivities(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();

                    var returnresult = HomeLogicRepository.RemoveRecentActivities(layoutId, userCredentials.id);
                    if (returnresult.Trim().Length == 0)
                        return Json(returnresult);
                    else
                        return Json(new { error = returnresult });

                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        #endregion


        public ActionResult GetLoginCustomerId()
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    try
                    {
                        var userCredentials = GetUserCredentials();
                        if (IsCustomerLogin(userCredentials.permissions))
                        {
                            var customerId = GetLoginCustomer(userCredentials.company, 0);
                            return Json(customerId);
                        }
                        else
                            return Json(new { error = "Normal User" });
                    }
                    catch (Exception ex)
                    {
                        return Json(new { error = GetStatusMessage(StatusCode.E02) });
                    }
                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");

        }
        public ActionResult IsCustomerLayout()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    var isCustomerLogin = IsCustomerLogin(userCredentials.permissions);
                    return Json(isCustomerLogin);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        public ActionResult GetTransicsResponce()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    return Json(userCredentials);
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }



        ///<summary>
        /// Recent Activities Page Methods End here
        ///</summary>

    }
}
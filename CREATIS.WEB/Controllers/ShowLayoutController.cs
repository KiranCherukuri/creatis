﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using CREATIS.Infrastructure.Repository;
using CREATIS.Infrastructure.IRepository;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text.html.simpleparser;
using static iTextSharp.text.Font;
using iTextSharp.text.html;


namespace CREATIS.Controllers
{
    public class ShowLayoutController : BaseController
    {
        public readonly IShowLayoutRepository showLayoutRepository;
        public readonly IQuestionPathRepository QuestionPathRepository;
        public readonly ITranslatorRepository TranslatorRepository;
        public readonly IScreenLayoutRepository ScreenLogicRepository;
        public readonly IDashboardRepository DashboardRepository;
        public readonly ICustomerOverviewRepository CustomerOverviewRepository;
        public ConnectionStrings connectionStrings { get; }
        public OthersSettings othersSettings { get; }
        public ShowLayoutController(IShowLayoutRepository showLayoutRepo, IQuestionPathRepository QuestionPathRep, ITranslatorRepository translatorRepository, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings, IScreenLayoutRepository ScreenLogicRepo, IDashboardRepository dashboardRep,
            ICustomerOverviewRepository CustomerOverviewRep)
        {
            this.showLayoutRepository = showLayoutRepo;
            this.QuestionPathRepository = QuestionPathRep;
            this.TranslatorRepository = translatorRepository;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
            this.ScreenLogicRepository = ScreenLogicRepo;
            this.DashboardRepository = dashboardRep;
            this.CustomerOverviewRepository = CustomerOverviewRep;
        }

        ///<summary>
        ///show Layout Page Methods Starts here
        ///</summary>

        #region Views
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ShowLayout(int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                var userCredentials = GetUserCredentials();
                if ((HasPrivilege(userCredentials.permissions).Contains("DLB") || HasPrivilege(userCredentials.permissions).Contains("QPL") || HasPrivilege(userCredentials.permissions).Contains("SLL") || HasPrivilege(userCredentials.permissions).Contains("TL")) && IsCustomerLogin(userCredentials.permissions))
                {
                    var customerId = GetLayoutCustomerId(LayoutId);
                    if (GetCustomerLayoutList(userCredentials.company, customerId).Any(x => x == LayoutId))
                    {
                        LayoutStatusPrivilege(LayoutId);
                        RoleWithLayoutStatusPrivilege(LayoutId, userCredentials.permissions);
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        ViewBag.DefaultShowlayoutLanguage = GetDefaultShowlayoutLanguage(LayoutId);
                        ViewBag.LayoutId = LayoutId;
                        string[] layoutTitleList = DashboardRepository.GetCustomerandLayoutName(LayoutId).Split("[");
                        ViewBag.LayoutTitle = layoutTitleList[0];
                        TempData["age"] = ViewBag.LayoutTitle;
                        AddRecentActivity(LayoutId, userCredentials.id);
                        return View();
                    }
                    else
                        return View("Unauthorised");
                }
                else if (HasPrivilege(userCredentials.permissions).Contains("DLB") || HasPrivilege(userCredentials.permissions).Contains("QPL") || HasPrivilege(userCredentials.permissions).Contains("SLL") || HasPrivilege(userCredentials.permissions).Contains("TL"))
                {
                    LayoutStatusPrivilege(LayoutId);
                    RoleWithLayoutStatusPrivilege(LayoutId, userCredentials.permissions);
                    ViewBag.username = userCredentials.displayName;
                    ViewBag.SessionLink = othersSettings.SessionLink;
                    ViewBag.DefaultShowlayoutLanguage = GetDefaultShowlayoutLanguage(LayoutId);
                    ViewBag.LayoutId = LayoutId;
                    string[] layoutTitleList = DashboardRepository.GetCustomerandLayoutName(LayoutId).Split("[");
                    ViewBag.LayoutTitle = layoutTitleList[0];
                    TempData["age"] = ViewBag.LayoutTitle;
                    AddRecentActivity(LayoutId, userCredentials.id);
                    return View();
                }
                else
                    return View("Unauthorised");

            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        #endregion

        #region ShowLayoutDataByLanguage
        [HttpPost]
        public ActionResult GetShowLayotDataByLanguageId(string langauage, int layoutId, int activitiesRegistrationId, int loopId, int endLoopId, int altercolorid)
        {
            try
            {
                ShowLayoutViewModel showlayoutViewModel = new ShowLayoutViewModel();
                List<ShowLayoutResponse> actList = showLayoutRepository.GetQuestionsByLayoutId(layoutId, activitiesRegistrationId);
                StringBuilder layoutData = ShowLayoutDataByLanguageId(actList, langauage, layoutId, loopId, endLoopId, altercolorid);
                return Json(new
                {
                    strbuilderData = layoutData
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message
                });
            }
        }
        public StringBuilder ShowLayoutDataByLanguageId(List<ShowLayoutResponse> layouList, string language, int layoutId, int loopId, int endLoopId, int altercolorid)
        {
            StringBuilder layoutBuilder = new StringBuilder();
            var userCredentials = GetUserCredentials();
            if (loopId == 0)
            {
                var activitiesScreenLayoutList = ScreenLogicRepository.GetActivitiesPosition(layoutId);
                var registrationsScreenLayoutList = ScreenLogicRepository.GetRegistrationPosition(layoutId);

                if (loopId == 0)
                {
                    layoutBuilder.Append("<div class='divBox divHeight'>");
                }
                layoutBuilder.Append("<table id='screenLayout' width='100%'>");
                layoutBuilder.Append("<tr style='background-color:hsl(0, 0%, 80%);'>");
                layoutBuilder.Append("<th style='text-align:center;width:20%'>Activities</th>");
                layoutBuilder.Append("<th style='text-align:center;width:20%'>Registration</th>");
                layoutBuilder.Append("<th style='text-align:center;width:15%'>Allowances</th>");
                layoutBuilder.Append("<th style='text-align:center;width:15%'>Info Columns</th>");
                layoutBuilder.Append("<th style='text-align:center;width:15%'>Document Type Flex</th>");
                layoutBuilder.Append("<th style='text-align:center;width:15%'>Planning Types</th>");
                layoutBuilder.Append("</tr>");
                layoutBuilder.Append("<tbody id='screenLayoutBody'>");
                layoutBuilder.Append("<tr style='background-color:hsl(0, 0%, 90%)'>");

                //Activities
                layoutBuilder.Append("<td style='text-align:center'>");
                if (activitiesScreenLayoutList.Count > 0)
                {
                    layoutBuilder.Append("<table  width='100%'>");
                    int loopno = 1;
                    foreach (var act in activitiesScreenLayoutList)
                    {
                        if (loopno == 1)
                        {
                            layoutBuilder.Append("<tr>");
                            layoutBuilder.Append("<td class=" + act.CopyLayoutColor + ">" + act.ActivitiesRegistrationName);
                            layoutBuilder.Append("</td>");
                            loopno = 2;
                        }
                        else if (loopno == 2)
                        {
                            layoutBuilder.Append("<td class=" + act.CopyLayoutColor + ">" + act.ActivitiesRegistrationName);
                            layoutBuilder.Append("</td>");
                            layoutBuilder.Append("</tr>");
                            loopno = 1;
                        }
                    }

                    if (loopno == 2)
                    {
                        layoutBuilder.Append("<td >");
                        layoutBuilder.Append("</td>");
                        layoutBuilder.Append("</tr>");
                    }
                    layoutBuilder.Append("</table>");
                    layoutBuilder.Append("</td>");
                }
                else
                {
                    layoutBuilder.Append("No Records");
                }

                //Registrations
                layoutBuilder.Append("<td style='text-align:center'>");

                if (registrationsScreenLayoutList.Count > 0)
                {
                    layoutBuilder.Append("<table width='100%'>");
                    int loopRegno = 1;
                    foreach (var reg in registrationsScreenLayoutList)
                    {
                        if (loopRegno == 1)
                        {
                            layoutBuilder.Append("<tr>");
                            layoutBuilder.Append("<td class=" + reg.CopyLayoutColor + ">" + reg.ActivitiesRegistrationName);
                            layoutBuilder.Append("</td>");
                            loopRegno = 2;
                        }
                        else if (loopRegno == 2)
                        {
                            layoutBuilder.Append("<td class=" + reg.CopyLayoutColor + ">" + reg.ActivitiesRegistrationName);
                            layoutBuilder.Append("</td>");
                            layoutBuilder.Append("</tr>");
                            loopRegno = 1;
                        }
                    }

                    if (loopRegno == 2)
                    {
                        layoutBuilder.Append("<td>");
                        layoutBuilder.Append("</td>");
                        layoutBuilder.Append("</tr>");
                    }
                    layoutBuilder.Append("</table>");
                }
                else
                {
                    layoutBuilder.Append("No Records");
                }

                layoutBuilder.Append("</td>");

                //Allowances
                layoutBuilder.Append("<td style='text-align:center'>");
                var allowancesList = DashboardRepository.GetAllowances(layoutId);
                if (allowancesList.Count > 0)
                {
                    layoutBuilder.Append("<table width='100%'>");

                    foreach (var allowance in allowancesList)
                    {
                        string CopyLayoutColor = GetCopyLayoutColor(allowance.IsModification);
                        layoutBuilder.Append("<tr >");
                        layoutBuilder.Append("<td class=" + CopyLayoutColor + " style='width:70%;'>" + allowance.AllowancesMasterDescription);
                        layoutBuilder.Append("</td>");
                        layoutBuilder.Append("<td class=" + CopyLayoutColor + " style='width:30%;'>" + allowance.AllowancesCode);
                        layoutBuilder.Append("</td>");
                        layoutBuilder.Append("</tr>");
                    }
                    layoutBuilder.Append("</table>");
                }
                else
                {
                    layoutBuilder.Append("No Records.");
                }
                layoutBuilder.Append("</td>");

                //Info Columns
                layoutBuilder.Append("<td style='text-align:center'>");
                var layoutQuestions = layouList.Select(x => x.LayQuesions).FirstOrDefault();
                int customerId = 0;
                if (layoutQuestions.Count > 0)
                    customerId = layoutQuestions.Select(x => x.CustomerRefId ?? 0).FirstOrDefault();
                var infoColumns = CustomerOverviewRepository.GetInfoColumn(customerId, userCredentials.id);

                if (infoColumns.Count > 0)
                {
                    layoutBuilder.Append("<table width='100%'>");
                    int order = 1;
                    foreach (var infocolumn in infoColumns)
                    {
                        layoutBuilder.Append("<tr >");
                        layoutBuilder.Append("<td class='Grey' style='width:70%;'>" + infocolumn.ICM_Column_Name);
                        layoutBuilder.Append("</td>");
                        layoutBuilder.Append("<td class='Grey' style='width:30%;'>" + order);
                        layoutBuilder.Append("</td>");
                        layoutBuilder.Append("</tr>");
                        order = order + 1;
                    }

                    layoutBuilder.Append("</table>");
                    layoutBuilder.Append("</td>");
                }
                else
                {
                    layoutBuilder.Append("No Records.");
                }

                //Document Type Flex
                layoutBuilder.Append("<td style='text-align:center'>");
                var docTypeFlexs = DashboardRepository.GetDocumentTypeFlex(layoutId);
                if (docTypeFlexs.Count > 0)
                {
                    layoutBuilder.Append("<table  width='100%'>");
                    foreach (var docTypeFlex in docTypeFlexs)
                    {
                        layoutBuilder.Append("<tr'>");
                        layoutBuilder.Append("<td class=" + docTypeFlex.CopyColorCode + " style='width:70%;'>" + docTypeFlex.Name.Split("(")[1].Replace("(", "").Replace(")", ""));
                        layoutBuilder.Append("</td>");
                        layoutBuilder.Append("<td class=" + docTypeFlex.CopyColorCode + " style='width:30%;'>" + docTypeFlex.Name.Split("(")[0]);
                        layoutBuilder.Append("</td>");
                        layoutBuilder.Append("</tr>");
                    }
                    layoutBuilder.Append("</table>");
                }
                else
                {
                    layoutBuilder.Append("No Records.");
                }
                layoutBuilder.Append("</td>");

                //Planning Types
                layoutBuilder.Append("<td style='text-align:center'>");
                var planningTypes = showLayoutRepository.GetPlanningTypeSL(layoutId);
                if (planningTypes.Count > 0)
                {
                    layoutBuilder.Append("<table width='100%'>");

                    foreach (var planningType in planningTypes)
                    {

                        string CopyLayoutColor = GetCopyLayoutColor(planningType.IsModification);
                        layoutBuilder.Append("<tr class=" + CopyLayoutColor + ">");
                        layoutBuilder.Append("<td>" + planningType.PlanningTypeName);
                        layoutBuilder.Append("</td>");
                        layoutBuilder.Append("</tr>");
                    }
                    layoutBuilder.Append("</table>");
                }
                else
                {
                    layoutBuilder.Append("No Records.");
                }
                layoutBuilder.Append("</td>");

                layoutBuilder.Append("</tr>");
                layoutBuilder.Append("</tbody>");
                layoutBuilder.Append("</table>");
            }



            //string activityCSS = "class='sec-table'";
            string activityCSS = "";
            string altcolor = "";
            if (altercolorid == 1)
                altcolor = ";background-color: hsl(0, 0%, 92%)";
            else
                altcolor = ";background-color: hsl(0, 0%, 97%)";
            foreach (var activities in layouList)
            {
                string questionPathColoring = GetQuetionPathColor(activities.IsModificationActivitiesRegistrationName, layoutId);
                if ((activities.IsModificationActivitiesRegistrationName == (int)EnumCopyColorCode.Green && activities.IsModificationConfirmButton == (int)EnumCopyColorCode.Green && activities.IsModificationEmptyFullSolo == (int)EnumCopyColorCode.Green &&
                    activities.IsModificationWorkingCodeRefId == (int)EnumCopyColorCode.Green && activities.IsModificationFlexactivity == (int)EnumCopyColorCode.Green && activities.IsModificationInterruptibleByRefId == (int)EnumCopyColorCode.Green &&
                    activities.IsModificationPlanningOverviewRefId == (int)EnumCopyColorCode.Green && activities.IsModificationPlanningRefId == (int)EnumCopyColorCode.Green && activities.IsModificationPTORefId == (int)EnumCopyColorCode.Green &&
                    activities.IsModificationSpecificRefId == (int)EnumCopyColorCode.Green && activities.IsModificationTransportType == (int)EnumCopyColorCode.Green && activities.IsModificationVisible == (int)EnumCopyColorCode.Green) ||
                    (activities.IsModificationActivitiesRegistrationName == (int)EnumCopyColorCode.Yellow && activities.IsModificationConfirmButton == (int)EnumCopyColorCode.Yellow && activities.IsModificationEmptyFullSolo == (int)EnumCopyColorCode.Yellow &&
                    activities.IsModificationWorkingCodeRefId == (int)EnumCopyColorCode.Yellow && activities.IsModificationFlexactivity == (int)EnumCopyColorCode.Yellow && activities.IsModificationInterruptibleByRefId == (int)EnumCopyColorCode.Yellow &&
                    activities.IsModificationPlanningOverviewRefId == (int)EnumCopyColorCode.Yellow && activities.IsModificationPlanningRefId == (int)EnumCopyColorCode.Yellow && activities.IsModificationPTORefId == (int)EnumCopyColorCode.Yellow &&
                    activities.IsModificationSpecificRefId == (int)EnumCopyColorCode.Yellow && activities.IsModificationTransportType == (int)EnumCopyColorCode.Yellow && activities.IsModificationVisible == (int)EnumCopyColorCode.Yellow ||
                    (activities.IsModificationActivitiesRegistrationName == (int)EnumCopyColorCode.Red && activities.IsModificationConfirmButton == (int)EnumCopyColorCode.Red && activities.IsModificationEmptyFullSolo == (int)EnumCopyColorCode.Red &&
                    activities.IsModificationWorkingCodeRefId == (int)EnumCopyColorCode.Red && activities.IsModificationFlexactivity == (int)EnumCopyColorCode.Red && activities.IsModificationInterruptibleByRefId == (int)EnumCopyColorCode.Red &&
                    activities.IsModificationPlanningOverviewRefId == (int)EnumCopyColorCode.Red && activities.IsModificationPlanningRefId == (int)EnumCopyColorCode.Red && activities.IsModificationPTORefId == (int)EnumCopyColorCode.Red &&
                    activities.IsModificationSpecificRefId == (int)EnumCopyColorCode.Red && activities.IsModificationTransportType == (int)EnumCopyColorCode.Red && activities.IsModificationVisible == (int)EnumCopyColorCode.Red) ||
                    (activities.IsModificationActivitiesRegistrationName == (int)EnumCopyColorCode.Empty && activities.IsModificationConfirmButton == (int)EnumCopyColorCode.Empty && activities.IsModificationEmptyFullSolo == (int)EnumCopyColorCode.Empty &&
                    activities.IsModificationWorkingCodeRefId == (int)EnumCopyColorCode.Empty && activities.IsModificationFlexactivity == (int)EnumCopyColorCode.Empty && activities.IsModificationInterruptibleByRefId == (int)EnumCopyColorCode.Empty &&
                    activities.IsModificationPlanningOverviewRefId == (int)EnumCopyColorCode.Empty && activities.IsModificationPlanningRefId == (int)EnumCopyColorCode.Empty && activities.IsModificationPTORefId == (int)EnumCopyColorCode.Empty &&
                    activities.IsModificationSpecificRefId == (int)EnumCopyColorCode.Empty && activities.IsModificationTransportType == (int)EnumCopyColorCode.Empty && activities.IsModificationVisible == (int)EnumCopyColorCode.Empty)
                    ))
                {
                    //Activities Data
                    string actColor = GetQuetionPathColor(activities.ISActivityModification, layoutId);
                    layoutBuilder.Append("<div class='col-lg-12 divBox' style='" + questionPathColoring + altcolor + "'>");
                }
                else
                {
                    //Activities Data
                    layoutBuilder.Append("<div class='col-lg-12 divBox' style='" + altcolor + "'>");
                }

                layoutBuilder.Append("<div class='col-lg-12 divTop'>");
                layoutBuilder.Append("<div class='col-lg-2'>");
                string questionPathName = string.Empty;
                ActivitiesRegistrationTranslations objActRegTranslations = showLayoutRepository.GetActivityRegistrationTranslations(activities.ActivitiesRegId, layoutId);
                questionPathName = showLayoutRepository.GetActivityRegistationName(objActRegTranslations, language);

                layoutBuilder.Append("<a href='javascript:void(0)' style='cursor: pointer;font-size:16px;" + questionPathColoring + "' onclick='return ClickActivities(" + activities.ActivitiesRegId + ")'> " + questionPathName + "</a>");
                layoutBuilder.Append("</div>");
                if (activities.Comment != "" && activities.Comment != null)
                {
                    layoutBuilder.Append("<div class='col-lg-10'><b>Comment : </b> " + activities.Comment);
                    layoutBuilder.Append("</div>");
                }
                layoutBuilder.Append("</div>");

                questionPathColoring = GetQuetionPathColor(activities.IsModificationWorkingCodeRefId, layoutId);
                layoutBuilder.Append("<div class='col-lg-12 divTop' >");
                layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Working Code :  </b>" + activities.WorkingCodeRefId);
                layoutBuilder.Append("</div>");
                questionPathColoring = GetQuetionPathColor(activities.IsModificationPlanningRefId, layoutId);
                layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Planning :  </b>" + activities.PlanningRefId);
                layoutBuilder.Append("</div>");

                questionPathColoring = GetQuetionPathColor(activities.IsModificationFlexactivity, layoutId);
                layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Flex LP :  </b>");
                if (activities.Flexactivity)
                    layoutBuilder.Append("True");
                else
                    layoutBuilder.Append("False");
                layoutBuilder.Append("</div>");

                List<boPlanningLevelDrop> returnList = showLayoutRepository.PlanningFeedbackLevel(activities.ActivitiesRegId);
                string planningLevels = string.Empty;
                foreach (var planningtype in returnList.Select(x => x.Planningfeedback).ToList().Distinct())
                {
                    planningLevels = planningtype + "," + planningLevels;
                }
                questionPathColoring = GetQuetionPathColor(activities.IsModificationPlanningRefId, layoutId);
                if (!string.IsNullOrEmpty(planningLevels))
                    layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "' ><b>Planning Levels :  </b>" + planningLevels.TrimEnd(','));
                else
                    layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2'><b>Planning Levels :  </b> -");
                layoutBuilder.Append("</div>");

                questionPathColoring = GetQuetionPathColor(activities.IsModificationPlanningOverviewRefId, layoutId);
                layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Planning Overview :  </b>" + activities.PlanningOverviewRefId);
                layoutBuilder.Append("</div>");
                layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Specific :  </b>" + activities.SpecificRefId);
                layoutBuilder.Append("</div>");
                layoutBuilder.Append("</div>");

                questionPathColoring = GetQuetionPathColor(activities.IsModificationConfirmButton, layoutId);
                layoutBuilder.Append("<div class='col-lg-12 divTop'>");

                layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Confirm Button :  </b>");
                if (activities.ConfirmButton)
                    layoutBuilder.Append("True");
                else
                    layoutBuilder.Append("False");
                layoutBuilder.Append("</div>");

                questionPathColoring = GetQuetionPathColor(activities.IsModificationVisible, layoutId);
                layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Visible :  </b>");
                if (activities.Visible.GetValueOrDefault())
                    layoutBuilder.Append("True");
                else
                    layoutBuilder.Append("False");
                layoutBuilder.Append("</div>");

                questionPathColoring = GetQuetionPathColor(activities.IsModificationTransportType, layoutId);
                layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Transport Type :  </b>");
                if (activities.IsTransportType)
                    layoutBuilder.Append(activities.TransportType);
                else
                    layoutBuilder.Append("None");
                layoutBuilder.Append("</div>");

                questionPathColoring = GetQuetionPathColor(activities.IsModificationEmptyFullSolo, layoutId);
                layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Empty/Full/Solo :  </b>");
                if (activities.EmptyFullSolo)
                    layoutBuilder.Append("True");
                else
                    layoutBuilder.Append("False");
                layoutBuilder.Append("</div>");

                questionPathColoring = GetQuetionPathColor(activities.IsModificationPTORefId, layoutId);
                if (activities.PTO != null && activities.PTO != "" && activities.PTO != "No PTO")
                    layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Linked PTO :  </b>" + activities.PTO);
                else
                    layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>No PTO</b>");
                layoutBuilder.Append("</div>");

                questionPathColoring = GetQuetionPathColor(activities.IsModificationInterruptibleByRefId, layoutId);
                if (activities.InterruptedBy != null && activities.InterruptedBy != "" && activities.InterruptedBy != "None")
                    layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Interruptible by :  </b>" + activities.InterruptedBy);
                else
                    layoutBuilder.Append("<div class='col-lg-2 col-md-4 col-sm-2' style='" + questionPathColoring + "'><b>Interruptible by :  </b>None");
                layoutBuilder.Append("</div>");
                layoutBuilder.Append("</div>");

                ////Question Path Data
                layoutBuilder.Append("<div class='col-lg-12 divTop' style='margin-top:7px' style='" + questionPathColoring + "'>");
                layoutBuilder.Append("<div id='fixed-table-container-" + loopId + "' class='fixed-table-container'>");
                layoutBuilder.Append("<table>");
                layoutBuilder.Append("<thead>");
                layoutBuilder.Append("<tr style='background-color:#f1f1f1'>");
                layoutBuilder.Append("<th class='alignment' style='width:360px;background-color: hsl(0, 0%, 80%)'>Question  Path</th>");
                layoutBuilder.Append("<th class='alignment' style='width:500px;background-color: hsl(0, 0%, 80%)'>Info</th>");
                layoutBuilder.Append("<th class='alignment' style='width:100px;background-color: hsl(0, 0%, 80%)'>Extra Info</th>");
                layoutBuilder.Append("<th class='alignment' style='width:100px;background-color: hsl(0, 0%, 80%)'>Anomaly</th>");
                layoutBuilder.Append("<th class='alignment' style='width:100px;background-color: hsl(0, 0%, 80%)'>Pallet Info</th>");
                layoutBuilder.Append("<th class='alignment' style='width:100px;background-color: hsl(0, 0%, 80%)'>Partner code</th>");
                layoutBuilder.Append("</tr>");
                layoutBuilder.Append("</thead>");
                layoutBuilder.Append("<tbody style='background-color: white;'>");
                List<ShowlayoutInfoTranslations> objInfoTranslationList = showLayoutRepository.GetInfoTranslations(layoutId, 0);

                int paddingData = 40;
                int flexdivision = 0;
                int smartDivision = 0;

                if (activities.LayQuesions.Count > 1)
                {
                    foreach (var root in activities.LayQuesions.Where(x => x.ParentId == 0))
                    {
                        foreach (var header in activities.LayQuesions.Where(x => x.ParentId == root.QuestionId))
                        {
                            if (flexdivision == 0)
                            {
                                questionPathColoring = GetQuetionPathColor(activities.IsModificationFlexactivity, layoutId);
                                if (root.PropertyName == "Flex")
                                {
                                    flexdivision = 1;
                                    layoutBuilder.Append("<tr>");
                                    layoutBuilder.Append("<td style='color:#698bca;font-weight:700;' " + activityCSS + " ><a href='javascript:void(0)' style='cursor: pointer;' onclick='return ClickActivities(" + activities.ActivitiesRegId + ")'>FLEX - Landing Pages</a></td>");
                                    layoutBuilder.Append("</td>");
                                    layoutBuilder.Append("<td style='" + activityCSS + "' ></td>");
                                    layoutBuilder.Append("<td style='" + activityCSS + "' ></td>");
                                    layoutBuilder.Append("<td style='" + activityCSS + "' ></td>");
                                    layoutBuilder.Append("<td style='" + activityCSS + "' ></td>");
                                    layoutBuilder.Append("<td style='" + activityCSS + "' ></td>");
                                    layoutBuilder.Append("</tr>");
                                }
                            }
                            if (smartDivision == 0)
                            {
                                questionPathColoring = GetQuetionPathColor(activities.IsModificationFlexactivity, layoutId);
                                if (root.PropertyName == "Smart")
                                {
                                    smartDivision = 1;
                                    layoutBuilder.Append("<tr>");
                                    layoutBuilder.Append("<td style='color:#698bca;font-weight:700;' " + activityCSS + " ><a href='javascript:void(0)' style='cursor: pointer;' onclick='return ClickActivities(" + activities.ActivitiesRegId + ")'>Landing Pages</a></td>");
                                    layoutBuilder.Append("</td>");
                                    layoutBuilder.Append("<td style='" + activityCSS + "' ></td>");
                                    layoutBuilder.Append("<td style='" + activityCSS + "' ></td>");
                                    layoutBuilder.Append("<td style='" + activityCSS + "' ></td>");
                                    layoutBuilder.Append("<td style='" + activityCSS + "' ></td>");
                                    layoutBuilder.Append("<td style='" + activityCSS + "' ></td>");
                                    layoutBuilder.Append("</tr>");
                                }
                            }

                            layoutBuilder.Append(AppendQuetionPaths(header, activityCSS, 10, language, layoutId, "", objInfoTranslationList));
                            RecursiveShowLayoutFunc(activities.LayQuesions, paddingData, header.QuestionId, activityCSS, language, layoutId, "", ref layoutBuilder, objInfoTranslationList);

                        }
                    }
                }
                else
                {
                    layoutBuilder.Append("<tr>");
                    layoutBuilder.Append("<td colspan='6' style='text-align: center;'>No Records Available.");
                    layoutBuilder.Append("</td>");
                    layoutBuilder.Append("</tr>");
                }
                layoutBuilder.Append("</tbody>");
                layoutBuilder.Append("</table>");
                layoutBuilder.Append("</div>");
                layoutBuilder.Append("</div>");
                layoutBuilder.Append("</div>");
            }

            if (endLoopId > 0)
            {
                layoutBuilder.Append("</div>");
            }
            return layoutBuilder;
        }
        public StringBuilder AppendQuetionPaths(BoShowLayout root, string activityCSS, int padding, string languageId, int layoutId, string activityColor, List<ShowlayoutInfoTranslations> objInfoTranslations)
        {
            try
            {
                StringBuilder questionPathLayBuilder = new StringBuilder();
                string extraInfo = string.Empty;
                string anomaly = string.Empty;
                string pallets = string.Empty;
                string questionPathColoring = GetQuetionPathColor(root.ISModification, layoutId);
                PlanningselectProperties objPlanningselectProperties = new PlanningselectProperties();

                questionPathLayBuilder.Append("<tr>");


                //Question Path
                LangDetail langDetails = new LangDetail();
                string questionPathName = string.Empty;
                int planningSelectId = 0;

                string translationInfoData = string.Empty;
                string translationSetActionValue = string.Empty;
                string translationSetActionInfoColumn = string.Empty;
                string infomessageTitle = string.Empty;

                if (root.PropertyName == "Choice")
                {
                    questionPathName = showLayoutRepository.GetQuestoinsTranslation(root, languageId);
                    if (root.Info.Contains("RecursiveYes"))
                    {
                        questionPathLayBuilder.Append("<td style='font-weight:bold;padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'><i class='fa fa-database' aria-hidden='true'></i><span class='span-left'> " + questionPathName.Replace("RecursiveYes", "") + "</span></p> </div> </div></td>");
                    }
                    else
                    {
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <i class='fa fa-database' aria-hidden='true'></i><span class='span-left'> " + questionPathName + "</span></p> </div> </div></td>");
                    }
                }
                else if (root.PropertyName == "Add Chioce")
                {
                    questionPathName = showLayoutRepository.GetQuestoinsTranslation(root, languageId);
                    if (root.Info.Contains("RecursiveYes"))
                    {
                        questionPathLayBuilder.Append("<td style='font-weight:bold;padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'><span class='span-left'> " + questionPathName.Replace("RecursiveYes", "") + "</span></p> </div> </div></td>");
                    }
                    else
                    {
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <span class='span-left'> " + questionPathName + "</span></p> </div> </div></td>");
                    }
                }
                else if (root.PropertyName == "Planning Select")
                {
                    questionPathName = showLayoutRepository.GetQuestoinsTranslation(root, languageId);
                    questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <span class='span-left'> <i class='fa fa-clipboard' aria-hidden='true'></i>" + questionPathName + " </span></p></div> </div></td>");
                }
                else if (root.PropertyName == "Other")
                {
                    var ParentPropertyName = showLayoutRepository.GetParentPropertyName(root.ParentId);
                    if (ParentPropertyName == "Planning Select")
                    {
                        var planningSelectOtherContent = showLayoutRepository.GetPlanningSelectOtherContent(root.ParentId);

                        bool isPSOtherContent = false;
                        if (planningSelectOtherContent != null)
                        {
                            planningSelectId = planningSelectOtherContent.PlanningselectId.GetValueOrDefault();
                            if (planningSelectOtherContent.Other == "No other" || planningSelectOtherContent.Other == "No other Driver entry (A50)" || planningSelectOtherContent.Other == "No other Fixed value" || planningSelectOtherContent.Other == "No other QP (save to DriverCreated)")
                                isPSOtherContent = true;
                            else
                                isPSOtherContent = false;
                        }

                        questionPathName = showLayoutRepository.GetQuestoinsTranslation(root, languageId);
                        if (isPSOtherContent)
                        {
                            questionPathLayBuilder.Append("<td style='text-decoration: line-through; padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <span class='span-left'>" + questionPathName + "</span></p> </div> </div> </td>");
                        }
                        else
                        {
                            questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <span class='span-left'>" + questionPathName + "</span></p></div> </div> </td>");
                        }
                    }
                    else if (ParentPropertyName == "Trailer")
                    {
                        var isTrailerOtherContent = showLayoutRepository.GetTrailerOtherContent(root.ParentId);
                        questionPathName = showLayoutRepository.GetQuestoinsTranslation(root, languageId);
                        if (isTrailerOtherContent)
                        {
                            questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <i class='fa fa-clipboard' aria-hidden='true'></i><span class='span-left'>" + questionPathName + "</span></p></div> </div> </td>");
                        }
                        else
                        {
                            questionPathLayBuilder.Append("<td style='text-decoration: line-through; padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span>  <p class='pstyle'><i class='fa fa-clipboard' aria-hidden='true'></i><span class='span-left'>" + questionPathName + "</span></p></div> </div> </td>");
                        }
                    }
                    else if (ParentPropertyName == "Variable List")
                    {
                        var isVariableListOtherContent = showLayoutRepository.GetVariableListOtherContent(root.ParentId);
                        questionPathName = showLayoutRepository.GetQuestoinsTranslation(root, languageId);
                        if (isVariableListOtherContent)
                        {
                            questionPathLayBuilder.Append("<td style='text-decoration: line-through; padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <i class='fa fa-clipboard' aria-hidden='true'></i><span class='span-left'>" + questionPathName + "</span></p></div> </div> </td>");
                        }
                        else
                        {
                            questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'><i class='fa fa-clipboard' aria-hidden='true'></i><span class='span-left'>" + questionPathName + "</span></p></div> </div> </td>");
                        }
                    }
                    else
                    {
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <span class='span-left'>" + questionPathName + "</span></p></div> </div> </td>");
                    }
                }
                else
                {
                    questionPathName = showLayoutRepository.GetQuestoinsTranslation(root, languageId);
                    if (root.PropertyName == "Entry" || root.PropertyName == "Entry From Variable List" || root.PropertyName == "Planning Select Other Title")
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'>  <p class='pstyle'> <i class='fa fa-sign-in' aria-hidden='true'></i><span class='span-left'>" + questionPathName + "</span></p> </div> </div></td>");
                    else if (root.PropertyName == "Check Off")
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span> <p class='pstyle'><i class='fa fa-check-square-o' aria-hidden='true'></i> <span class='span-left'>" + questionPathName + " </span></p></div> </div></td>");
                    else if (root.PropertyName == "Conditional Action")
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'><i class='fa fa-child' aria-hidden='true'></i>  <span class='span-left'>" + questionPathName + " </span></p></div> </div></td>");
                    else if (root.PropertyName == "Info Message")
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <i class='fa fa-info-circle' aria-hidden='true'></i><span class='span-left'> " + questionPathName + " </span></p></div> </div></td>");
                    else if (root.PropertyName == "Text Message")
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <i class='fa fa-commenting' aria-hidden='true'></i><span class='span-left'> " + questionPathName + " </span></p></div> </div></td>");
                    else if (root.PropertyName == "Set Action / Value")
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'><i class='fa fa-bolt' aria-hidden='true'></i> <span class='span-left'> " + questionPathName + " </span></p></div> </div></td>");
                    else if (root.PropertyName == "Variable List")
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <i class='fa fa-list' aria-hidden='true'></i><span class='span-left'> " + questionPathName + " </span></p></div> </div></td>");
                    else if (root.PropertyName == "Document Scan")
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span> <p class='pstyle'><i class='fa fa-file-text' aria-hidden='true'></i> <span class='span-left'>" + questionPathName + "</span></p> </div> </div></td>");
                    else if (root.PropertyName == "Transport Types")
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <i class='fa fa-truck' aria-hidden='true'></i> <span class='span-left'>" + questionPathName + " </span></p></div> </div></td>");
                    else if (root.PropertyName == "Get System Values")
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span> <p class='pstyle'><i class='fa fa-desktop' aria-hidden='true'></i> <span class='span-left'>" + questionPathName + "</span></p> </div> </div></td>");
                    else if (root.PropertyName == "Trailer" || root.PropertyName == "Trailer From Variable List")
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span> <p class='pstyle'><i class='fa fa-train' aria-hidden='true'></i> <span class='span-left'>" + questionPathName + "</span></p> </div> </div></td>");
                    else
                        questionPathLayBuilder.Append("<td style='padding-left:" + padding + "px !important;" + questionPathColoring + "' " + activityCSS + "><div class='ee-item'> <div class='e-text-wrap'><span style='margin-left:15px;'></span><p class='pstyle'> <span class='span-left'>" + questionPathName + " </span></p></div> </div></td>");
                }

                //Info Data Content

                if (root.PropertyName == "Info Message")
                {
                    translationInfoData = showLayoutRepository.GetInfoMessageContent(root.QuestionId, layoutId).Replace("'", "`");
                    if (translationInfoData != null && translationInfoData != "")
                    {
                        if (root.Info != "" && root.Info != null)
                            translationInfoData = ", " + translationInfoData;
                        else
                            translationInfoData = "" + translationInfoData;
                    }

                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.InfomessageProperties_Title && x.RefId == root.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        infomessageTitle = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, root.LanguageName).Replace("<p>", "").Replace("</p>", "").Replace("'", "`");
                    if (infomessageTitle == "@")
                    {
                        infomessageTitle = "";
                    }
                    else if (infomessageTitle != null && infomessageTitle != "")
                    {
                        if (root.Info != "" && root.Info != null)
                            infomessageTitle = " <b>Message Title: </b>" + infomessageTitle;
                        else
                            infomessageTitle = " <b>Message Title: </b>" + infomessageTitle;
                    }

                    questionPathLayBuilder.Append("<td  " + activityCSS + " style='white-space:pre-line;" + questionPathColoring + "'>" + (root.Info + infomessageTitle + translationInfoData) + " </td>");
                }
                else if (root.PropertyName == "Choice")
                {
                    if (root.Info.Contains("RecursiveYes"))
                    {
                        questionPathLayBuilder.Append("<td style='" + questionPathColoring + "' " + activityCSS + ">" + root.Info.Replace("RecursiveYes", "") + " </td>");
                    }
                    else
                    {
                        questionPathLayBuilder.Append("<td style='" + questionPathColoring + "' " + activityCSS + ">" + root.Info + " </td>");
                    }
                }
                else if (root.PropertyName == "Add Chioce")
                {
                    if (root.Info.Contains("RecursiveYes"))
                    {
                        questionPathLayBuilder.Append("<td style='" + questionPathColoring + "' " + activityCSS + ">" + root.Info.Replace("RecursiveYes", "") + "</td>");
                    }
                    else
                    {
                        questionPathLayBuilder.Append("<td style='" + questionPathColoring + "' " + activityCSS + ">" + root.Info + "</td>");
                    }
                }
                else if (root.PropertyName == "Entry" || root.PropertyName == "Entry From Variable List")
                {
                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.EntryProperties_MaskText && x.RefId == root.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationInfoData = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, root.LanguageName);
                    if (translationInfoData == "@")
                    {
                        translationInfoData = "";
                    }
                    else if (translationInfoData != null && translationInfoData != "")
                    {
                        if (root.Info != "" && root.Info != null)
                            translationInfoData = ", <b>Mask Text :</b> " + translationInfoData;
                        else
                            translationInfoData = "" + translationInfoData;
                    }

                    string translationFixedValue = string.Empty;
                    string proposeFixedValue = string.Empty;
                    objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.EntryProperties_FixedValue && x.RefId == root.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationFixedValue = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, root.LanguageName);
                    if (translationFixedValue == "@")
                    {
                        translationFixedValue = "";
                    }
                    else if (translationFixedValue != null && translationFixedValue != "")
                    {
                        proposeFixedValue = showLayoutRepository.GetProposeFixedValue(root.CopyLayoutRefId.GetValueOrDefault(), translationFixedValue);
                        //translationFixedValue = ", " + translationFixedValue;
                    }
                    questionPathLayBuilder.Append("<td style='" + questionPathColoring + "' " + activityCSS + ">" + (root.Info + translationInfoData + proposeFixedValue) + "</td>");
                }
                else if (root.PropertyName == "Planning Select Other Title")
                {
                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.EntryProperties_MaskText && x.RefId == root.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationInfoData = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, root.LanguageName);
                    if (translationInfoData == "@")
                    {
                        translationInfoData = "";
                    }
                    else if (translationInfoData != null && translationInfoData != "")
                    {
                        if (root.Info != "" && root.Info != null)
                            translationInfoData = ", <b>Mask Text :</b> " + translationInfoData;
                        else
                            translationInfoData = "" + translationInfoData;
                    }
                    questionPathLayBuilder.Append("<td style='" + questionPathColoring + "' " + activityCSS + ">" + (root.Info + translationInfoData) + " </td>");
                }
                else if (root.PropertyName == "Text Message")
                {
                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.TextMessageProperties_Messagecontent && x.RefId == root.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationInfoData = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, root.LanguageName);
                    if (translationInfoData == "@")
                    {
                        translationInfoData = "";
                    }
                    else if (translationInfoData != null && translationInfoData != "")
                    {
                        if (root.Info != "" && root.Info != null)
                            translationInfoData = ", " + translationInfoData;
                        else
                            translationInfoData = "" + translationInfoData;
                    }
                    questionPathLayBuilder.Append("<td style='" + questionPathColoring + "' " + activityCSS + ">" + (root.Info + translationInfoData) + " </td>");
                }
                else if (root.PropertyName == "Planning Select")
                {
                    string planningChoosingActivities = string.Empty;
                    planningChoosingActivities = showLayoutRepository.GetPlanChoosingActivites(root.CopyLayoutRefId.GetValueOrDefault());

                    if (planningChoosingActivities != "" && planningChoosingActivities != null)
                    {
                        planningChoosingActivities = ", Places of " + planningChoosingActivities;
                    }

                    string transFixedValue = string.Empty;

                    ShowlayoutInfoTranslations objIntoTransFixedValue = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.PlanningselectProperties_Fixedvalue && x.RefId == root.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();

                    if (objIntoTransFixedValue != null)
                    {
                        transFixedValue = showLayoutRepository.GetInfoTranslationLanguage(objIntoTransFixedValue, languageId, root.LanguageName);
                    }
                    if (transFixedValue == "@")
                    {
                        transFixedValue = "";
                    }
                    else if (transFixedValue != null && transFixedValue != "")
                    {
                        if (root.Info != "" && root.Info != null)
                            transFixedValue = ", " + transFixedValue;
                        else
                            transFixedValue = "" + transFixedValue;
                    }

                    questionPathLayBuilder.Append("<td  style='" + questionPathColoring + "' " + activityCSS + ">" + ((root.Info == "" ? "" : root.Info) + planningChoosingActivities).TrimStart(',').Replace(", ,", ",").Replace(",,", ",") + " </td>");
                }
                else if (root.PropertyName == "Other")
                {
                    string planningSelectOthers = string.Empty;
                    var ParentPropertyName = showLayoutRepository.GetParentPropertyName(root.ParentId);
                    if (ParentPropertyName == "Planning Select")
                    {
                        ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.PlanningselectProperties_OtherTitle && x.RefId == planningSelectId).FirstOrDefault();
                        if (objIntoTrans != null)
                            translationInfoData = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, root.LanguageName);
                        if (translationInfoData == "@")
                        {
                            translationInfoData = "";
                        }
                        else if (translationInfoData != null && translationInfoData != "")
                        {
                            if (root.Info != "" && root.Info != null)
                                translationInfoData = ", " + translationInfoData;
                            else
                                translationInfoData = "" + translationInfoData;
                        }
                        objPlanningselectProperties = showLayoutRepository.GetPlanningSelectLevel(planningSelectId);
                        if (objPlanningselectProperties.Other != "" && objPlanningselectProperties.Other != null)
                        {
                            if (objPlanningselectProperties.Other == "Fixed value" || objPlanningselectProperties.Other == "No other Fixed value")
                            {
                                planningSelectOthers = ", Register '" + translationInfoData.Replace(",", "") + "'";
                            }
                        }
                    }
                    questionPathLayBuilder.Append("<td  style='" + questionPathColoring + "' " + activityCSS + ">" + ((root.Info == "" ? "" : root.Info + ", ") + planningSelectOthers).TrimStart(',').Replace(", ,", ",").Replace(",,", ",") + " </td>");
                }
                else if (root.PropertyName == "Trailer" || root.PropertyName == "Trailer From Variable List")
                {
                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.TrailerProperties_MaskText && x.RefId == root.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationInfoData = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, root.LanguageName);
                    if (translationInfoData == "@")
                    {
                        translationInfoData = "";
                    }
                    else if (translationInfoData != null && translationInfoData != "")
                    {
                        if (root.Info != "" && root.Info != null)
                            translationInfoData = ", <b>Mask Text :</b> " + translationInfoData;
                        else
                            translationInfoData = "" + translationInfoData;
                    }
                    questionPathLayBuilder.Append("<td style='" + questionPathColoring + "' " + activityCSS + "><span>" + (root.Info + translationInfoData) + "</span> </td>");
                }
                else if (root.PropertyName == "Set Action / Value")
                {
                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.SetActionValueProperties_Value && x.RefId == root.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationSetActionValue = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, root.LanguageName);
                    if (translationSetActionValue == "@")
                    {
                        translationSetActionValue = "";
                    }
                    else if (translationSetActionValue != null && translationSetActionValue != "")
                    {
                        if (root.Info != "" && root.Info != null)
                            translationSetActionValue = ", " + translationSetActionValue;
                        else
                            translationSetActionValue = "" + translationSetActionValue;
                    }

                    objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.SetActionValueProperties_InfoColumnFixedText && x.RefId == root.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationSetActionInfoColumn = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, root.LanguageName);
                    if (translationSetActionInfoColumn == "@")
                    {
                        translationSetActionInfoColumn = "";
                    }
                    else if (translationSetActionInfoColumn != null && translationSetActionInfoColumn != "")
                    {
                        if (root.Info != "" && root.Info != null)
                            translationSetActionInfoColumn = ", " + translationSetActionInfoColumn;
                        else
                            translationSetActionInfoColumn = "" + translationSetActionInfoColumn;
                    }
                    questionPathLayBuilder.Append("<td style='" + questionPathColoring + "' " + activityCSS + ">" + (root.Info + translationSetActionValue + translationSetActionInfoColumn) + " </td>");
                }
                else
                {
                    questionPathLayBuilder.Append("<td style='" + questionPathColoring + "' " + activityCSS + ">" + root.Info + " </td>");
                }

                string[] feedBackArray = root.FeedBackType.Split(',');
                if (feedBackArray[0] == "Extra info")
                    extraInfo = feedBackArray[1];
                if (feedBackArray[0] == "Anomaly")
                    anomaly = feedBackArray[1];
                if (feedBackArray[0] == "Pallets")
                    pallets = feedBackArray[1];

                questionPathLayBuilder.Append("<td " + activityCSS + " style='text-align:center;" + questionPathColoring + "'>" + extraInfo + " </td>");
                questionPathLayBuilder.Append("<td " + activityCSS + " style='text-align:center;" + questionPathColoring + "'>" + anomaly + " </td>");
                questionPathLayBuilder.Append("<td " + activityCSS + " style='text-align:center;" + questionPathColoring + "'>" + pallets + " </td>");
                questionPathLayBuilder.Append("<td " + activityCSS + " style='text-align:center;" + questionPathColoring + "'>" + root.PartnerCode + " </td>");

                questionPathLayBuilder.Append("</tr>");
                return questionPathLayBuilder;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public StringBuilder RecursiveShowLayoutFunc(List<BoShowLayout> actLayQuesions, int padding, int parentId, string activityCSS, string language, int layoutId, string activityColor, ref StringBuilder childNodes, List<ShowlayoutInfoTranslations> objInfoTranslations)
        {
            try
            {
                foreach (var qstnPaths in actLayQuesions.Where(a => a.ParentId == parentId))
                {
                    childNodes.Append(AppendQuetionPaths(qstnPaths, activityCSS, padding, language, layoutId, activityColor, objInfoTranslations));

                    if (actLayQuesions.Where(x => x.ParentId == qstnPaths.QuestionId).Count() > 0)
                    {
                        RecursiveShowLayoutFunc(actLayQuesions, padding + 13, qstnPaths.QuestionId, activityCSS, language, layoutId, activityColor, ref childNodes, objInfoTranslations);
                    }
                }
                return childNodes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetQuetionPathColor(int? colorNumber, int layoutId)
        {
            try
            {
                string questionPathColoring = string.Empty;
                bool layoutColor = Convert.ToBoolean(showLayoutRepository.GetLayoutColor(layoutId));

                if (layoutColor)
                {
                    if (colorNumber == 0)
                    {
                        questionPathColoring = "background-color: #36d836 !important";
                    }
                    else if (colorNumber == 2)
                    {
                        questionPathColoring = " background-color: #e6e614 !important";
                    }
                    else if (colorNumber == 3)
                    {
                        questionPathColoring = "background-color: #e82e2e !important";
                    }
                    else
                    {
                        questionPathColoring = "";
                    }
                }
                else
                {
                    questionPathColoring = "";
                }

                return questionPathColoring;
            }
            catch (Exception ex)
            { throw ex; }
        }
        public string GetCopyLayoutColor(int isModification)
        {
            var colorname = "";
            if (isModification == (int)EnumCopyColorCode.Green)
                colorname = "Green";
            else if (isModification == (int)EnumCopyColorCode.Yellow)
                colorname = "Yellow";
            else if (isModification == (int)EnumCopyColorCode.Red)
                colorname = "Red";
            else
                colorname = "Grey";

            return colorname;
        }
        #endregion

        #region PDF 
        [HttpPost]
        public FileResult DownloadPDF(string LayoutId, string Launguage)
        {
            var userCredentials = GetUserCredentials();
            int intLayoutId = Convert.ToInt32(LayoutId);
            List<ShowLayoutResponse> actList = showLayoutRepository.GetQuestionsByLayoutId(intLayoutId, 0);
            List<ShowlayoutInfoTranslations> objInfoTranslationList = showLayoutRepository.GetInfoTranslations(intLayoutId, 0);
            string pdfFileName = string.Empty;
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                Document pdfDoc = new Document(PageSize.A4.Rotate(), 24, 10, 28, 25);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                Font fontNormal = new Font(Font.FontFamily.HELVETICA, 10f);
                Font boldFont = new Font(Font.FontFamily.HELVETICA, 10f, Font.BOLD);

                PdfPTable pDetailTable = new PdfPTable(2);
                pDetailTable.TotalWidth = 795f;
                float[] widthDetailTable = new float[] { 398f, 398f };
                pDetailTable.SetWidths(widthDetailTable);
                pDetailTable.SpacingAfter = 10;
                string[] layoutTitleList = DashboardRepository.GetCustomerandLayoutName(intLayoutId).Split("[");

                pDetailTable.AddCell(new PdfPCell(new Phrase(layoutTitleList[0], boldFont)) { BorderColor = BaseColor.WHITE, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidth = .5f });
                pDetailTable.AddCell(new PdfPCell(new Phrase(DateTime.Now.ToString(), boldFont)) { BorderColor = BaseColor.WHITE, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = .5f });
                pdfDoc.Add(pDetailTable);

                PdfPTable pTable = new PdfPTable(6);
                pTable.SpacingBefore = 15;
                pTable.HorizontalAlignment = 0;
                pTable.TotalWidth = 795f;

                pTable.LockedWidth = true;
                pTable.DefaultCell.Phrase = new Phrase() { Font = fontNormal };
                pTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                pTable.DefaultCell.BorderColor = WebColors.GetRGBColor("#DCDCDC");
                pTable.DefaultCell.Padding = 6;
                float[] widthsF = new float[] { 155f, 130f, 150f, 140f, 130f, 110f };
                pTable.SetWidths(widthsF);

                BaseColor myColor = WebColors.GetRGBColor("#DCDCDC");
                BaseColor bgCSSForCells = WebColors.GetRGBColor("#f1f1f1");

                PdfPTable pSLTable = new PdfPTable(6);
                pSLTable.DefaultCell.FixedHeight = 100f;
                pSLTable.HorizontalAlignment = 0;
                pSLTable.TotalWidth = 795f;
                pSLTable.LockedWidth = true;
                pSLTable.DefaultCell.Phrase = new Phrase() { Font = fontNormal };
                pSLTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                pSLTable.DefaultCell.BorderColor = WebColors.GetRGBColor("#DCDCDC");
                pSLTable.DefaultCell.Padding = 6;
                float[] widthSLF = new float[] { 197f, 198f, 100f, 100f, 100f, 100f };
                pSLTable.SetWidths(widthSLF);

                pSLTable.AddCell(new PdfPCell(new Phrase("Activities", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });
                pSLTable.AddCell(new PdfPCell(new Phrase("Registration", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });
                pSLTable.AddCell(new PdfPCell(new Phrase("Allowances", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });
                pSLTable.AddCell(new PdfPCell(new Phrase("Info Columns", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });
                pSLTable.AddCell(new PdfPCell(new Phrase("Document Type Flex", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });
                pSLTable.AddCell(new PdfPCell(new Phrase("Planning Types", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });

                //Activities
                var activitiesScreenLayoutList = ScreenLogicRepository.GetActivitiesPosition(intLayoutId);
                PdfPTable pActTable = new PdfPTable(2);

                if (activitiesScreenLayoutList.Count > 0)
                {
                    int loopno = 1;
                    foreach (var act in activitiesScreenLayoutList)
                    {
                        BaseColor questionPathColoring = GetColorForScreenLayou(act.CopyLayoutColor);
                        if (loopno == 1)
                        {
                            pActTable.AddCell(new PdfPCell(new Phrase(act.ActivitiesRegistrationName, boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f, BackgroundColor = questionPathColoring });
                            loopno = 2;
                        }
                        else if (loopno == 2)
                        {
                            pActTable.AddCell(new PdfPCell(new Phrase(act.ActivitiesRegistrationName, boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f, BackgroundColor = questionPathColoring });
                            loopno = 1;
                        }
                    }
                    if (loopno == 2)
                    {
                        pActTable.AddCell(new PdfPCell(new Phrase("", boldFont)) { BorderColor = BaseColor.WHITE, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidth = .5f, BackgroundColor = BaseColor.WHITE });
                    }
                    pSLTable.AddCell(new PdfPCell(pActTable) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else
                {
                    pSLTable.AddCell(new PdfPCell(new Phrase("No Records", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#eff0f2") });
                }

                //Registration
                var registrationsScreenLayoutList = ScreenLogicRepository.GetRegistrationPosition(intLayoutId);
                PdfPTable pRegTable = new PdfPTable(2);
                if (activitiesScreenLayoutList.Count > 0)
                {
                    int loopno = 1;
                    foreach (var reg in registrationsScreenLayoutList)
                    {
                        BaseColor questionPathColoring = GetColorForScreenLayou(reg.CopyLayoutColor);
                        if (loopno == 1)
                        {
                            pRegTable.AddCell(new PdfPCell(new Phrase(reg.ActivitiesRegistrationName, boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f, BackgroundColor = questionPathColoring });
                            loopno = 2;
                        }
                        else if (loopno == 2)
                        {
                            pRegTable.AddCell(new PdfPCell(new Phrase(reg.ActivitiesRegistrationName, boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f, BackgroundColor = questionPathColoring });
                            loopno = 1;
                        }
                    }
                    if (loopno == 2)
                    {
                        pRegTable.AddCell(new PdfPCell(new Phrase("", boldFont)) { BorderColor = BaseColor.WHITE, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidth = .5f, BackgroundColor = BaseColor.WHITE });
                    }
                    pSLTable.AddCell(new PdfPCell(pRegTable) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else
                {
                    pSLTable.AddCell(new PdfPCell(new Phrase("No Records", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#eff0f2") });
                }

                //Allowances
                var allowancesList = DashboardRepository.GetAllowances(intLayoutId);
                PdfPTable pAllowanceTable = new PdfPTable(2);
                BaseColor screenColor = WebColors.GetRGBColor("#ADADAD");
                if (allowancesList.Count > 0)
                {
                    foreach (var allowance in allowancesList)
                    {
                        BaseColor copyLayoutColor = ScreenlayRemColors(allowance.IsModification);
                        pAllowanceTable.AddCell(new PdfPCell(new Phrase(allowance.AllowancesMasterDescription, boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = copyLayoutColor });
                        pAllowanceTable.AddCell(new PdfPCell(new Phrase(allowance.AllowancesCode, boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = copyLayoutColor });
                    }
                    pSLTable.AddCell(new PdfPCell(pAllowanceTable) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else
                {
                    pSLTable.AddCell(new PdfPCell(new Phrase("No Records", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#eff0f2") });
                }

                //Info Columns
                var layoutQuestions = actList.Select(x => x.LayQuesions).FirstOrDefault();
                PdfPTable pInfoColumnsTable = new PdfPTable(2);
                int customerId = 0;
                if (layoutQuestions.Count > 0)
                    customerId = layoutQuestions.Select(x => x.CustomerRefId ?? 0).FirstOrDefault();
                var infoColumns = CustomerOverviewRepository.GetInfoColumn(customerId, userCredentials.id);
                if (infoColumns.Count > 0)
                {
                    int order = 1;
                    foreach (var infocolumn in infoColumns)
                    {
                        pInfoColumnsTable.AddCell(new PdfPCell(new Phrase(infocolumn.ICM_Column_Name, boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = screenColor });
                        pInfoColumnsTable.AddCell(new PdfPCell(new Phrase(order.ToString(), boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = screenColor });
                        order = order + 1;
                    }
                    pSLTable.AddCell(new PdfPCell(pInfoColumnsTable) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else
                {
                    pSLTable.AddCell(new PdfPCell(new Phrase("No Records", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#eff0f2") });
                }


                //Document Type Flex
                var docTypeFlexs = DashboardRepository.GetDocumentTypeFlex(intLayoutId);
                PdfPTable pDocTypesTable = new PdfPTable(2);
                if (docTypeFlexs.Count > 0)
                {
                    foreach (var docTypeFlex in docTypeFlexs)
                    {
                        BaseColor copyLayoutColor = GetColorForScreenLayou(docTypeFlex.CopyColorCode);
                        pDocTypesTable.AddCell(new PdfPCell(new Phrase(docTypeFlex.Name.Split("(")[1].Replace("(", "").Replace(")", ""), boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = copyLayoutColor });
                        pDocTypesTable.AddCell(new PdfPCell(new Phrase(docTypeFlex.Name.Split("(")[0], boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = copyLayoutColor });
                    }
                    pSLTable.AddCell(new PdfPCell(pDocTypesTable) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else
                {
                    pSLTable.AddCell(new PdfPCell(new Phrase("No Records", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#eff0f2") });
                }

                //Planning Types
                var planningTypes = DashboardRepository.GetPlanningType(intLayoutId);
                PdfPTable pPlanningTypesTable = new PdfPTable(1);
                if (planningTypes.Count > 0)
                {
                    foreach (var planningType in planningTypes)
                    {
                        BaseColor copyLayoutColor = ScreenlayRemColors(planningType.IsModification);
                        pPlanningTypesTable.AddCell(new PdfPCell(new Phrase(planningType.PlanningTypeName, boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = copyLayoutColor });
                    }
                    pSLTable.AddCell(new PdfPCell(pPlanningTypesTable) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else
                {
                    pSLTable.AddCell(new PdfPCell(new Phrase("No Records", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#eff0f2") });
                }

                pdfDoc.Add(pSLTable);


                string activityCSS = "class='sec-table'";
                //Activities
                foreach (var activities in actList)
                {

                    if (activityCSS == "class='sec-table'")
                    {
                        bgCSSForCells = WebColors.GetRGBColor("#ebebeb");
                        activityCSS = "";
                    }
                    else
                    {
                        bgCSSForCells = WebColors.GetRGBColor("#f7f7f7");
                        activityCSS = "class='sec-table'";
                    }

                    BaseColor questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationActivitiesRegistrationName, intLayoutId, activityCSS);



                    //Activities Registration Name
                    string questionPathName = string.Empty;
                    ActivitiesRegistrationTranslations objActRegTranslations = showLayoutRepository.GetActivityRegistrationTranslations(activities.ActivitiesRegId, intLayoutId);
                    questionPathName = showLayoutRepository.GetActivityRegistationName(objActRegTranslations, Launguage);


                    if (questionPathName == null || questionPathName == "")
                    {
                        questionPathName = "@" + activities.ActivitiesRegistrationName;
                    }

                    Font actFont = new Font(Font.FontFamily.HELVETICA, 13f, Font.BOLD);

                    if (activities.Comment != "" && activities.Comment != null)
                    {
                        pTable.AddCell(new PdfPCell(new Phrase(questionPathName, actFont))
                        {

                            //Colspan = 6,
                            BorderColorLeft = WebColors.GetRGBColor("#DCDCDC"),
                            BorderColorRight = BaseColor.WHITE,
                            BorderColorTop = WebColors.GetRGBColor("#DCDCDC"),
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });

                        pTable.AddCell(new PdfPCell(new Phrase("Comment : " + activities.Comment, boldFont))
                        {

                            Colspan = 5,
                            BorderColorLeft = BaseColor.WHITE,
                            BorderColorRight = WebColors.GetRGBColor("#DCDCDC"),
                            BorderColorTop = WebColors.GetRGBColor("#DCDCDC"),
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });
                    }
                    else
                    {
                        pTable.AddCell(new PdfPCell(new Phrase(questionPathName, boldFont))
                        {

                            Colspan = 6,
                            BorderColorLeft = WebColors.GetRGBColor("#DCDCDC"),
                            BorderColorRight = WebColors.GetRGBColor("#DCDCDC"),
                            BorderColorTop = WebColors.GetRGBColor("#DCDCDC"),
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });
                    }

                    questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationWorkingCodeRefId, intLayoutId, activityCSS);
                    pTable.AddCell(new PdfPCell(new Phrase("Working Code : " + activities.WorkingCodeRefId, boldFont))
                    {
                        BorderColorLeft = WebColors.GetRGBColor("#DCDCDC"),
                        BorderColorRight = BaseColor.WHITE,
                        BorderColorTop = BaseColor.WHITE,
                        BorderColorBottom = BaseColor.WHITE,
                        BorderWidthLeft = .5f,
                        BorderWidthRight = .5f,
                        BorderWidthTop = .5f,
                        BorderWidthBottom = .5f,
                        Padding = 6f,
                        BackgroundColor = questionPathColoring
                    });
                    questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationPlanningRefId, intLayoutId, activityCSS);
                    pTable.AddCell(new PdfPCell(new Phrase("Planning : " + activities.PlanningRefId, boldFont))
                    {
                        BorderColorLeft = BaseColor.WHITE,
                        BorderColorRight = BaseColor.WHITE,
                        BorderColorTop = BaseColor.WHITE,
                        BorderColorBottom = BaseColor.WHITE,
                        BorderWidthLeft = .5f,
                        BorderWidthRight = .5f,
                        BorderWidthTop = .5f,
                        BorderWidthBottom = .5f,
                        Padding = 6f,
                        BackgroundColor = questionPathColoring
                    });

                    questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationFlexactivity, intLayoutId, activityCSS);
                    if (activities.IsFlex)
                        pTable.AddCell(new PdfPCell(new Phrase("Flex LP : True", boldFont))
                        {
                            BorderColorLeft = BaseColor.WHITE,
                            BorderColorRight = BaseColor.WHITE,
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });
                    else
                        pTable.AddCell(new PdfPCell(new Phrase("Flex LP : False", boldFont))
                        {
                            BorderColorLeft = BaseColor.WHITE,
                            BorderColorRight = BaseColor.WHITE,
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });


                    List<boPlanningLevelDrop> returnList = showLayoutRepository.PlanningFeedbackLevel(activities.ActivitiesRegId);
                    string planningLevels = string.Empty;
                    foreach (var planningtype in returnList.Select(x => x.Planningfeedback).ToList().Distinct())
                    {
                        planningLevels = planningtype + "," + planningLevels;
                    }

                    if ((activities.IsModificationActivitiesRegistrationName == (int)EnumCopyColorCode.Green && activities.IsModificationConfirmButton == (int)EnumCopyColorCode.Green && activities.IsModificationEmptyFullSolo == (int)EnumCopyColorCode.Green &&
                        activities.IsModificationWorkingCodeRefId == (int)EnumCopyColorCode.Green && activities.IsModificationFlexactivity == (int)EnumCopyColorCode.Green && activities.IsModificationInterruptibleByRefId == (int)EnumCopyColorCode.Green &&
                        activities.IsModificationPlanningOverviewRefId == (int)EnumCopyColorCode.Green && activities.IsModificationPlanningRefId == (int)EnumCopyColorCode.Green && activities.IsModificationPTORefId == (int)EnumCopyColorCode.Green &&
                        activities.IsModificationSpecificRefId == (int)EnumCopyColorCode.Green && activities.IsModificationTransportType == (int)EnumCopyColorCode.Green && activities.IsModificationVisible == (int)EnumCopyColorCode.Green) ||
                        (activities.IsModificationActivitiesRegistrationName == (int)EnumCopyColorCode.Yellow && activities.IsModificationConfirmButton == (int)EnumCopyColorCode.Yellow && activities.IsModificationEmptyFullSolo == (int)EnumCopyColorCode.Yellow &&
                        activities.IsModificationWorkingCodeRefId == (int)EnumCopyColorCode.Yellow && activities.IsModificationFlexactivity == (int)EnumCopyColorCode.Yellow && activities.IsModificationInterruptibleByRefId == (int)EnumCopyColorCode.Yellow &&
                        activities.IsModificationPlanningOverviewRefId == (int)EnumCopyColorCode.Yellow && activities.IsModificationPlanningRefId == (int)EnumCopyColorCode.Yellow && activities.IsModificationPTORefId == (int)EnumCopyColorCode.Yellow &&
                        activities.IsModificationSpecificRefId == (int)EnumCopyColorCode.Yellow && activities.IsModificationTransportType == (int)EnumCopyColorCode.Yellow && activities.IsModificationVisible == (int)EnumCopyColorCode.Yellow ||
                        (activities.IsModificationActivitiesRegistrationName == (int)EnumCopyColorCode.Red && activities.IsModificationConfirmButton == (int)EnumCopyColorCode.Red && activities.IsModificationEmptyFullSolo == (int)EnumCopyColorCode.Red &&
                        activities.IsModificationWorkingCodeRefId == (int)EnumCopyColorCode.Red && activities.IsModificationFlexactivity == (int)EnumCopyColorCode.Red && activities.IsModificationInterruptibleByRefId == (int)EnumCopyColorCode.Red &&
                        activities.IsModificationPlanningOverviewRefId == (int)EnumCopyColorCode.Red && activities.IsModificationPlanningRefId == (int)EnumCopyColorCode.Red && activities.IsModificationPTORefId == (int)EnumCopyColorCode.Red &&
                        activities.IsModificationSpecificRefId == (int)EnumCopyColorCode.Red && activities.IsModificationTransportType == (int)EnumCopyColorCode.Red && activities.IsModificationVisible == (int)EnumCopyColorCode.Red) ||
                        (activities.IsModificationActivitiesRegistrationName == (int)EnumCopyColorCode.Empty && activities.IsModificationConfirmButton == (int)EnumCopyColorCode.Empty && activities.IsModificationEmptyFullSolo == (int)EnumCopyColorCode.Empty &&
                        activities.IsModificationWorkingCodeRefId == (int)EnumCopyColorCode.Empty && activities.IsModificationFlexactivity == (int)EnumCopyColorCode.Empty && activities.IsModificationInterruptibleByRefId == (int)EnumCopyColorCode.Empty &&
                        activities.IsModificationPlanningOverviewRefId == (int)EnumCopyColorCode.Empty && activities.IsModificationPlanningRefId == (int)EnumCopyColorCode.Empty && activities.IsModificationPTORefId == (int)EnumCopyColorCode.Empty &&
                        activities.IsModificationSpecificRefId == (int)EnumCopyColorCode.Empty && activities.IsModificationTransportType == (int)EnumCopyColorCode.Empty && activities.IsModificationVisible == (int)EnumCopyColorCode.Empty)
                        ))
                    {
                        questionPathColoring = GetQuetionPathColorForPDF(activities.ISActivityModification, intLayoutId, activityCSS);
                    }
                    else
                    {
                        questionPathColoring = GetQuetionPathColorForPDF(1, intLayoutId, activityCSS);
                    }



                    if (!string.IsNullOrEmpty(planningLevels))
                        pTable.AddCell(new PdfPCell(new Phrase("Planning Levels: " + planningLevels.TrimEnd(','), boldFont))
                        {
                            BorderColorLeft = BaseColor.WHITE,
                            BorderColorRight = BaseColor.WHITE,
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f
                            ,
                            BackgroundColor = questionPathColoring
                        });
                    else
                        pTable.AddCell(new PdfPCell(new Phrase("Planning Levels: -", boldFont))
                        {
                            BorderColorLeft = BaseColor.WHITE,
                            BorderColorRight = BaseColor.WHITE,
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f
                            ,
                            BackgroundColor = questionPathColoring
                        });

                    questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationPlanningOverviewRefId, intLayoutId, activityCSS);
                    pTable.AddCell(new PdfPCell(new Phrase("Planning Overview : " + activities.PlanningOverviewRefId, boldFont))
                    {
                        BorderColorLeft = BaseColor.WHITE,
                        BorderColorRight = BaseColor.WHITE,
                        BorderColorTop = BaseColor.WHITE,
                        BorderColorBottom = BaseColor.WHITE,
                        BorderWidthLeft = .5f,
                        BorderWidthRight = .5f,
                        BorderWidthTop = .5f,
                        BorderWidthBottom = .5f,
                        Padding = 6f,
                        BackgroundColor = questionPathColoring
                    });

                    questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationSpecificRefId, intLayoutId, activityCSS);
                    pTable.AddCell(new PdfPCell(new Phrase("Specific : " + activities.SpecificRefId, boldFont))
                    {
                        BorderColorLeft = BaseColor.WHITE,
                        BorderColorRight = BaseColor.WHITE,
                        BorderColorTop = BaseColor.WHITE,
                        BorderColorBottom = BaseColor.WHITE,
                        BorderWidthLeft = .5f,
                        BorderWidthRight = .5f,
                        BorderWidthTop = .5f,
                        BorderWidthBottom = .5f,
                        Padding = 6f,
                        BackgroundColor = questionPathColoring
                    });

                    string actConfirmButton = string.Empty;
                    if (activities.ConfirmButton)
                        actConfirmButton = "True";
                    else
                        actConfirmButton = "False";

                    questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationConfirmButton, intLayoutId, activityCSS);
                    pTable.AddCell(new PdfPCell(new Phrase("Confirm button : " + actConfirmButton, boldFont))
                    {
                        BorderColorLeft = BaseColor.WHITE,
                        BorderColorRight = BaseColor.WHITE,
                        BorderColorTop = BaseColor.WHITE,
                        BorderColorBottom = BaseColor.WHITE,
                        BorderWidthLeft = .5f,
                        BorderWidthRight = .5f,
                        BorderWidthTop = .5f,
                        BorderWidthBottom = .5f,
                        Padding = 6f,
                        BackgroundColor = questionPathColoring
                    });

                    string actVisible = string.Empty;
                    if (activities.Visible.GetValueOrDefault())
                        actVisible = "True";
                    else
                        actVisible = "False";

                    questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationVisible, intLayoutId, activityCSS);
                    pTable.AddCell(new PdfPCell(new Phrase("Visible : " + actVisible, boldFont))
                    {
                        BorderColorLeft = BaseColor.WHITE,
                        BorderColorRight = WebColors.GetRGBColor("#DCDCDC"),
                        BorderColorTop = BaseColor.WHITE,
                        BorderColorBottom = BaseColor.WHITE,
                        BorderWidthLeft = .5f,
                        BorderWidthRight = .5f,
                        BorderWidthTop = .5f,
                        BorderWidthBottom = .5f,
                        Padding = 6f,
                        BackgroundColor = questionPathColoring
                    });

                    questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationTransportType, intLayoutId, activityCSS);
                    if (activities.IsTransportType)
                        pTable.AddCell(new PdfPCell(new Phrase("Transport Type : " + activities.TransportType, boldFont))
                        {
                            BorderColorLeft = WebColors.GetRGBColor("#DCDCDC"),
                            BorderColorRight = BaseColor.WHITE,
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });
                    else
                        pTable.AddCell(new PdfPCell(new Phrase("Transport Type : None", boldFont))
                        {
                            BorderColorLeft = WebColors.GetRGBColor("#DCDCDC"),
                            BorderColorRight = BaseColor.WHITE,
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });

                   


                    questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationEmptyFullSolo, intLayoutId, activityCSS);
                    if (activities.EmptyFullSolo)
                        pTable.AddCell(new PdfPCell(new Phrase("Empty/Full/Solo : True", boldFont))
                        {
                            BorderColorLeft = BaseColor.WHITE,
                            BorderColorRight = BaseColor.WHITE,
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });
                    else
                        pTable.AddCell(new PdfPCell(new Phrase("Empty/Full/Solo : False", boldFont))
                        {
                            BorderColorLeft = BaseColor.WHITE,
                            BorderColorRight = BaseColor.WHITE,
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });


                  

                    questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationPTORefId, intLayoutId, activityCSS);
                    if (activities.PTO != null && activities.PTO != "" && activities.PTO != "No PTO")
                        pTable.AddCell(new PdfPCell(new Phrase("Linked PTO : " + activities.PTO, boldFont))
                        {
                            BorderColorLeft = BaseColor.WHITE,
                            BorderColorRight = BaseColor.WHITE,
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });
                    else
                        pTable.AddCell(new PdfPCell(new Phrase("No PTO", boldFont))
                        {
                            BorderColorLeft = BaseColor.WHITE,
                            BorderColorRight = BaseColor.WHITE,
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });

                    questionPathColoring = GetQuetionPathColorForPDF(activities.IsModificationInterruptibleByRefId, intLayoutId, activityCSS);
                    if (activities.InterruptedBy != null && activities.InterruptedBy != "" && activities.InterruptedBy != "None")
                        pTable.AddCell(new PdfPCell(new Phrase("Interruptible by : " + activities.InterruptedBy, boldFont))
                        {
                            BorderColorLeft = BaseColor.WHITE,
                            BorderColorRight = WebColors.GetRGBColor("#DCDCDC"),
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = BaseColor.WHITE,
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });
                    else
                        pTable.AddCell(new PdfPCell(new Phrase("Interruptible by : None", boldFont))
                        {
                            BorderColorLeft = BaseColor.WHITE,
                            BorderColorRight = WebColors.GetRGBColor("#DCDCDC"),
                            BorderColorTop = BaseColor.WHITE,
                            BorderColorBottom = WebColors.GetRGBColor("#DCDCDC"),
                            BorderWidthLeft = .5f,
                            BorderWidthRight = .5f,
                            BorderWidthTop = .5f,
                            BorderWidthBottom = .5f,
                            Padding = 6f,
                            BackgroundColor = questionPathColoring
                        });


                    int flexDivision = 0;
                    int smartDivision = 0;
                    int paddingData = 20;

                    PdfPTable pQPTable = new PdfPTable(6);
                    pQPTable.AddCell(new PdfPCell(new Phrase("Question  Path", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });
                    pQPTable.AddCell(new PdfPCell(new Phrase("Info", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });
                    pQPTable.AddCell(new PdfPCell(new Phrase("Extra Info", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });
                    pQPTable.AddCell(new PdfPCell(new Phrase("Anomaly", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });
                    pQPTable.AddCell(new PdfPCell(new Phrase("Pallet Info", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });
                    pQPTable.AddCell(new PdfPCell(new Phrase("partner Code", boldFont)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), HorizontalAlignment = Element.ALIGN_CENTER, BorderWidth = .5f, BackgroundColor = WebColors.GetRGBColor("#cccccc") });
                    float[] widths = new float[] { 150f, 200f, 45f, 45f, 45f, 45f };
                    pQPTable.SetWidths(widths);
                    if (activities.LayQuesions.Count > 1)
                    {
                        foreach (var root in activities.LayQuesions.Where(x => x.ParentId == 0))
                        {
                            foreach (var header in activities.LayQuesions.Where(x => x.ParentId == root.QuestionId))
                            {
                                var fontColour = new BaseColor(31, 73, 125);
                                var myFont = FontFactory.GetFont("Times New Roman", 11, fontColour);

                                Font fontStyle = new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD);
                                if (flexDivision == 0)
                                {
                                    if (root.PropertyName == "Flex")
                                    {
                                        flexDivision = 1;
                                        pQPTable.AddCell(new PdfPCell(new Phrase("Flex - Landing Pages", fontStyle)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                        pQPTable.AddCell(new PdfPCell(new Phrase("")) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                        pQPTable.AddCell(new PdfPCell(new Phrase("")) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                        pQPTable.AddCell(new PdfPCell(new Phrase("")) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                        pQPTable.AddCell(new PdfPCell(new Phrase("")) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                        pQPTable.AddCell(new PdfPCell(new Phrase("")) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                    }
                                }
                                if (smartDivision == 0)
                                {
                                    if (root.PropertyName == "Smart")
                                    {
                                        smartDivision = 1;
                                        pQPTable.AddCell(new PdfPCell(new Phrase("Landing Pages", fontStyle)) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                        pQPTable.AddCell(new PdfPCell(new Phrase("")) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                        pQPTable.AddCell(new PdfPCell(new Phrase("")) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                        pQPTable.AddCell(new PdfPCell(new Phrase("")) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                        pQPTable.AddCell(new PdfPCell(new Phrase("")) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                        pQPTable.AddCell(new PdfPCell(new Phrase("")) { BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                                    }
                                }
                                AppendQuetionPathsPDF(header, bgCSSForCells, activityCSS, 5, Launguage, intLayoutId, ref pQPTable, objInfoTranslationList);
                                RecursiveShowLayoutFuncPDF(activities.LayQuesions, paddingData, header.QuestionId, bgCSSForCells, activityCSS, Launguage, intLayoutId, ref pQPTable, objInfoTranslationList);
                            }
                        }
                        pTable.AddCell(new PdfPCell(pQPTable) { Colspan = 6, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                    else
                    {
                        pQPTable.AddCell(new PdfPCell(new Phrase("No Records available", boldFont)) { Colspan = 6, HorizontalAlignment = Element.ALIGN_CENTER, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f, BackgroundColor = questionPathColoring });
                        pTable.AddCell(new PdfPCell(pQPTable) { Colspan = 6, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                    pdfFileName = objActRegTranslations.CustomerName + " - " + objActRegTranslations.LayoutName + ".PDF";
                }
                pdfDoc.Add(pTable);
                pdfDoc.Close();

                return File(stream.ToArray(), "application/pdf", pdfFileName);
            }
        }
        public PdfPTable AppendQuetionPathsPDF(BoShowLayout questionPathRoot, BaseColor bgCSSForCells, string activityCSS, int padding, string languageId, int layoutId, ref PdfPTable qsnPathPdfTable, List<ShowlayoutInfoTranslations> objInfoTranslations)
        {
            try
            {
                PlanningselectProperties objPlanningselectProperties = new PlanningselectProperties();
                Font fontNormal = new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD);
                qsnPathPdfTable.DefaultCell.Phrase = new Phrase() { Font = fontNormal };
                qsnPathPdfTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                qsnPathPdfTable.DefaultCell.BorderColor = WebColors.GetRGBColor("#DCDCDC");
                qsnPathPdfTable.DefaultCell.Padding = 3;

                string extraInfo = string.Empty;
                string anomaly = string.Empty;
                string pallets = string.Empty;
                int planningSelectId = 0;
                BaseColor questionPathColoring = GetQuetionPathColorForPDF(questionPathRoot.ISModification, layoutId, activityCSS);

                LangDetail langDetails = new LangDetail();
                string questionPathName = string.Empty;
                Font boldFont = new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD);

                if (questionPathRoot.PropertyName == "Choice")
                {
                    questionPathName = showLayoutRepository.GetQuestoinsTranslation(questionPathRoot, languageId);
                    if (questionPathRoot.Info.Contains("RecursiveYes"))
                    {
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, boldFont)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                    else
                    {
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, fontNormal)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                }
                else if (questionPathRoot.PropertyName == "Add Chioce")
                {
                    questionPathName = showLayoutRepository.GetQuestoinsTranslation(questionPathRoot, languageId);
                    if (questionPathRoot.Info.Contains("RecursiveYes"))
                    {
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, boldFont)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                    else
                    {
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, fontNormal)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                }
                else if (questionPathRoot.PropertyName == "Planning Select")
                {
                    questionPathName = showLayoutRepository.GetQuestoinsTranslation(questionPathRoot, languageId);
                    qsnPathPdfTable.AddCell(new PdfPCell(new Phrase((questionPathName), fontNormal)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else if (questionPathRoot.PropertyName == "Other")
                {
                    var ParentPropertyName = showLayoutRepository.GetParentPropertyName(questionPathRoot.ParentId);
                    if (ParentPropertyName == "Planning Select")
                    {
                        var planningSelectOtherContent = showLayoutRepository.GetPlanningSelectOtherContent(questionPathRoot.ParentId);
                        bool isPSOtherContent = false;

                        if (planningSelectOtherContent != null)
                        {
                            planningSelectId = planningSelectOtherContent.PlanningselectId.GetValueOrDefault();
                            if (planningSelectOtherContent.Other == "No other" || planningSelectOtherContent.Other == "No other Driver entry (A50)" || planningSelectOtherContent.Other == "No other Fixed value" || planningSelectOtherContent.Other == "No other QP (save to DriverCreated)")
                                isPSOtherContent = true;
                            else
                                isPSOtherContent = false;
                        }

                        if (isPSOtherContent)
                        {
                            Font fontSTRIKETHRU = new Font(Font.FontFamily.HELVETICA, 10f, Font.STRIKETHRU);
                            questionPathName = showLayoutRepository.GetQuestoinsTranslation(questionPathRoot, languageId);
                            qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, fontSTRIKETHRU)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f, });
                        }
                        else
                        {
                            questionPathName = showLayoutRepository.GetQuestoinsTranslation(questionPathRoot, languageId);
                            qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, fontNormal)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                        }

                    }
                    else if (ParentPropertyName == "Trailer")
                    {
                        var isTrailerOtherContent = showLayoutRepository.GetTrailerOtherContent(questionPathRoot.ParentId);
                        if (isTrailerOtherContent)
                        {
                            questionPathName = showLayoutRepository.GetQuestoinsTranslation(questionPathRoot, languageId);
                            qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, fontNormal)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                        }
                        else
                        {
                            Font fontSTRIKETHRU = new Font(Font.FontFamily.HELVETICA, 10f, Font.STRIKETHRU);
                            questionPathName = showLayoutRepository.GetQuestoinsTranslation(questionPathRoot, languageId);
                            qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, fontSTRIKETHRU)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f, });
                        }

                    }
                    else if (ParentPropertyName == "Variable List")
                    {
                        var isVariableListOtherContent = showLayoutRepository.GetVariableListOtherContent(questionPathRoot.ParentId);
                        questionPathName = showLayoutRepository.GetQuestoinsTranslation(questionPathRoot, languageId);
                        if (isVariableListOtherContent)
                        {
                            Font fontSTRIKETHRU = new Font(Font.FontFamily.HELVETICA, 10f, Font.STRIKETHRU);
                            qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, fontSTRIKETHRU)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f, });
                        }
                        else
                        {
                            qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, fontNormal)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                        }
                    }
                    else
                    {
                        questionPathName = showLayoutRepository.GetQuestoinsTranslation(questionPathRoot, languageId);
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, fontNormal)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                }
                else
                {
                    questionPathName = showLayoutRepository.GetQuestoinsTranslation(questionPathRoot, languageId);
                    qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathName, fontNormal)) { BackgroundColor = questionPathColoring, PaddingLeft = padding, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                //Info Data
                string translationInfoData = string.Empty;
                string proposeFixedValue = string.Empty;
                string translationSetActionValue = string.Empty;
                string translationSetActionInfoColumn = string.Empty;
                string infomessageTitle = string.Empty;
                if (questionPathRoot.PropertyName == "Info Message")
                {
                    translationInfoData = showLayoutRepository.GetInfoMessageContent(questionPathRoot.QuestionId, layoutId).Replace("'", "`");

                    if (translationInfoData != null && translationInfoData != "")
                    {
                        if (questionPathRoot.Info != "" && questionPathRoot.Info != null)
                            translationInfoData = ", " + TranslatorRepository.StripHTML(translationInfoData);
                        else
                            translationInfoData = "" + TranslatorRepository.StripHTML(translationInfoData);
                    }

                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.InfomessageProperties_Title && x.RefId == questionPathRoot.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        infomessageTitle = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, questionPathRoot.LanguageName).Replace("<p>", "").Replace("</p>", "").Replace("'", "`");
                    if (infomessageTitle == "@")
                    {
                        infomessageTitle = "";
                    }
                    else if (infomessageTitle != null && infomessageTitle != "")
                    {
                        if (questionPathRoot.Info != "" && questionPathRoot.Info != null)
                            infomessageTitle = " Message Title: " + infomessageTitle;
                        else
                            infomessageTitle = " Message Title: " + infomessageTitle;
                    }

                    if (infomessageTitle.Contains("<u>") && infomessageTitle.Contains("<b>"))
                    {
                        Font boldFontBldUnd = new Font(Font.FontFamily.HELVETICA, 10f, Font.BOLD | Font.UNDERLINE);
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase((questionPathRoot.Info + (infomessageTitle + translationInfoData).Replace("<b>", "").Replace("</b>", "").Replace("<u>", "").Replace("</u>", "")), boldFontBldUnd)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                    else if (infomessageTitle.Contains("<u>"))
                    {
                        Font boldFontUnderLine = new Font(Font.FontFamily.HELVETICA, 10f, Font.UNDERLINE);
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase((questionPathRoot.Info + (infomessageTitle + translationInfoData).Replace("<u>", "").Replace("</u>", "")), boldFontUnderLine)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                    else if (infomessageTitle.Contains("<b>"))
                    {
                        Font boldFont1 = new Font(Font.FontFamily.HELVETICA, 10f, Font.BOLD);
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase((questionPathRoot.Info + (infomessageTitle + translationInfoData).Replace("<b>", "").Replace("</b>", "")), boldFont1)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });

                    }
                    else
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase((questionPathRoot.Info + infomessageTitle + translationInfoData), fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else if (questionPathRoot.PropertyName == "Entry" || questionPathRoot.PropertyName == "Entry From Variable List")
                {
                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.EntryProperties_MaskText && x.RefId == questionPathRoot.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationInfoData = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, questionPathRoot.LanguageName);

                    if (translationInfoData == "@")
                    {
                        translationInfoData = "";
                    }
                    else if (translationInfoData != null && translationInfoData != "")
                    {
                        if (questionPathRoot.Info != "" && questionPathRoot.Info != null)
                            translationInfoData = ", Mask Text : " + translationInfoData;
                        else
                            translationInfoData = "" + translationInfoData;

                    }

                    string translationFixedValue = string.Empty;
                    objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.EntryProperties_FixedValue && x.RefId == questionPathRoot.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationFixedValue = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, questionPathRoot.LanguageName);
                    if (translationFixedValue == "@")
                    {
                        translationFixedValue = "";
                    }
                    else if (translationFixedValue != null && translationFixedValue != "")
                    {
                        //translationFixedValue = ", " + translationFixedValue;
                        proposeFixedValue = showLayoutRepository.GetProposeFixedValue(questionPathRoot.CopyLayoutRefId.GetValueOrDefault(), translationFixedValue);
                    }
                    qsnPathPdfTable.AddCell(new PdfPCell(new Phrase((questionPathRoot.Info + translationInfoData + proposeFixedValue), fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else if (questionPathRoot.PropertyName == "Planning Select Other Title")
                {
                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.EntryProperties_MaskText && x.RefId == questionPathRoot.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationInfoData = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, questionPathRoot.LanguageName);
                    if (translationInfoData == "@")
                    {
                        translationInfoData = "";
                    }
                    else if (translationInfoData != null && translationInfoData != "")
                    {
                        if (questionPathRoot.Info != "" && questionPathRoot.Info != null)
                            translationInfoData = ", Mask Text : " + translationInfoData;
                        else
                            translationInfoData = "" + translationInfoData;
                    }

                    qsnPathPdfTable.AddCell(new PdfPCell(new Phrase((questionPathRoot.Info + translationInfoData), fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else if (questionPathRoot.PropertyName == "Text Message")
                {
                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.TextMessageProperties_Messagecontent && x.RefId == questionPathRoot.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationInfoData = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, questionPathRoot.LanguageName);
                    if (translationInfoData == "@")
                    {
                        translationInfoData = "";
                    }
                    else if (translationInfoData != null && translationInfoData != "")
                    {
                        if (questionPathRoot.Info != "" && questionPathRoot.Info != null)
                            translationInfoData = ", " + translationInfoData;
                        else
                            translationInfoData = "" + translationInfoData;
                    }
                    qsnPathPdfTable.AddCell(new PdfPCell(new Phrase((questionPathRoot.Info + translationInfoData), fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });

                }
                else if (questionPathRoot.PropertyName == "Choice")
                {
                    if (questionPathRoot.Info.Contains("RecursiveYes"))
                    {
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathRoot.Info.Replace("RecursiveYes", ""), fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                    else
                    {
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathRoot.Info, fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                }
                else if (questionPathRoot.PropertyName == "Add Chioce")
                {
                    if (questionPathRoot.Info.Contains("RecursiveYes"))
                    {
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathRoot.Info.Replace("RecursiveYes", ""), fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                    else
                    {
                        qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathRoot.Info, fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    }
                }
                else if (questionPathRoot.PropertyName == "Planning Select")
                {
                    string planningChoosingActivities = string.Empty;
                    planningChoosingActivities = showLayoutRepository.GetPlanChoosingActivites(questionPathRoot.CopyLayoutRefId.GetValueOrDefault());
                    if (planningChoosingActivities != null && planningChoosingActivities != "")
                    {
                        planningChoosingActivities = ", Places of" + planningChoosingActivities;
                    }

                    string transFixedValue = string.Empty;
                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.PlanningselectProperties_OtherTitle && x.RefId == questionPathRoot.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationInfoData = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, questionPathRoot.LanguageName);
                    if (translationInfoData == "@")
                    {
                        translationInfoData = "";
                    }
                    else if (translationInfoData != null && translationInfoData != "")
                    {
                        if (questionPathRoot.Info != "" && questionPathRoot.Info != null)
                            translationInfoData = ", " + translationInfoData;
                        else
                            translationInfoData = "" + translationInfoData;
                    }
                    ShowlayoutInfoTranslations objIntoTransFixedValue = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.PlanningselectProperties_Fixedvalue && x.RefId == questionPathRoot.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();

                    if (objIntoTransFixedValue != null)
                    {
                        transFixedValue = showLayoutRepository.GetInfoTranslationLanguage(objIntoTransFixedValue, languageId, questionPathRoot.LanguageName);
                    }
                    if (transFixedValue == "@")
                    {
                        transFixedValue = "";
                    }
                    else if (transFixedValue != null && transFixedValue != "")
                    {
                        if (questionPathRoot.Info != "" && questionPathRoot.Info != null)
                            transFixedValue = ", " + transFixedValue;
                        else
                            transFixedValue = "" + transFixedValue;
                    }
                    string planningSelectOthers = string.Empty;
                    if (objPlanningselectProperties.Other != "" && objPlanningselectProperties.Other != null)
                    {

                        if (objPlanningselectProperties.Other == "Fixed value" || objPlanningselectProperties.Other == "No other Fixed value")
                        {
                            planningSelectOthers = ", Register " + translationInfoData.Replace(",", "");
                        }
                    }

                    qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(((questionPathRoot.Info.Replace("<b>", "").Replace("</b>", "") == "" ? "" : questionPathRoot.Info + ", ") + planningChoosingActivities + planningSelectOthers).TrimStart(',').Replace(", ,", ",").Replace(",,", ","), fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });

                }
                else if (questionPathRoot.PropertyName == "Other")
                {
                    string planningSelectOthers = string.Empty;
                    var ParentPropertyName = showLayoutRepository.GetParentPropertyName(questionPathRoot.ParentId);
                    if (ParentPropertyName == "Planning Select")
                    {
                        ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.PlanningselectProperties_OtherTitle && x.RefId == planningSelectId).FirstOrDefault();
                        if (objIntoTrans != null)
                            translationInfoData = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, questionPathRoot.LanguageName);
                        if (translationInfoData == "@")
                        {
                            translationInfoData = "";
                        }
                        else if (translationInfoData != null && translationInfoData != "")
                        {
                            if (questionPathRoot.Info != "" && questionPathRoot.Info != null)
                                translationInfoData = ", " + translationInfoData;
                            else
                                translationInfoData = "" + translationInfoData;
                        }
                        objPlanningselectProperties = showLayoutRepository.GetPlanningSelectLevel(planningSelectId);
                        if (objPlanningselectProperties.Other != "" && objPlanningselectProperties.Other != null)
                        {
                            if (objPlanningselectProperties.Other == "Fixed value" || objPlanningselectProperties.Other == "No other Fixed value")
                            {
                                planningSelectOthers = ", Register '" + translationInfoData.Replace(",", "") + "'";
                            }
                        }
                    }
                    qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(((questionPathRoot.Info == "" ? "" : questionPathRoot.Info + ", ") + planningSelectOthers).TrimStart(',').Replace(", ,", ",").Replace(",,", ","), fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                    //qsnPathPdfTable.Append("<td  style='" + questionPathColoring + "' " + activityCSS + "><span title='" + ((questionPathRoot.Info == "" ? "" : questionPathRoot.Info + ", ") + planningSelectOthers).TrimStart(',').Replace(", ,", ",").Replace(",,", ",") + "'>" + ((questionPathRoot.Info == "" ? "" : questionPathRoot.Info + ", ") + planningSelectOthers).TrimStart(',').Replace(", ,", ",").Replace(",,", ",") + "</span> </td>");
                }
                else if (questionPathRoot.PropertyName == "Trailer" || questionPathRoot.PropertyName == "Trailer From Variable List")
                {
                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.TrailerProperties_MaskText && x.RefId == questionPathRoot.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationInfoData = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, questionPathRoot.LanguageName);
                    if (translationInfoData == "@")
                    {
                        translationInfoData = "";
                    }
                    else if (translationInfoData != null && translationInfoData != "")
                    {
                        if (questionPathRoot.Info != "" && questionPathRoot.Info != null)
                            translationInfoData = ", Mask Text : " + translationInfoData;
                        else
                            translationInfoData = "" + translationInfoData;
                    }


                    Font fontSTRIKETHRU = new Font(Font.FontFamily.HELVETICA, 10f, Font.STRIKETHRU);
                    string otherPart = string.Empty;

                    qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathRoot.Info + translationInfoData, fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else if (questionPathRoot.PropertyName == "Set Action / Value")
                {
                    ShowlayoutInfoTranslations objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.SetActionValueProperties_Value && x.RefId == questionPathRoot.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationSetActionValue = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, questionPathRoot.LanguageName);
                    if (translationSetActionValue == "@")
                    {
                        translationSetActionValue = "";
                    }
                    else if (translationSetActionValue != null && translationSetActionValue != "")
                    {
                        if (questionPathRoot.Info != "" && questionPathRoot.Info != null)
                            translationSetActionValue = ", " + translationSetActionValue;
                        else
                            translationSetActionValue = "" + translationSetActionValue;
                    }

                    objIntoTrans = objInfoTranslations.Where(x => x.LangRefColTableId == (int)LangRefColTable.SetActionValueProperties_InfoColumnFixedText && x.RefId == questionPathRoot.CopyLayoutRefId.GetValueOrDefault()).FirstOrDefault();
                    if (objIntoTrans != null)
                        translationSetActionInfoColumn = showLayoutRepository.GetInfoTranslationLanguage(objIntoTrans, languageId, questionPathRoot.LanguageName);
                    if (translationSetActionInfoColumn == "@")
                    {
                        translationSetActionInfoColumn = "";
                    }
                    else if (translationSetActionInfoColumn != null && translationSetActionInfoColumn != "")
                    {
                        if (questionPathRoot.Info != "" && questionPathRoot.Info != null)
                            translationSetActionInfoColumn = ", " + translationSetActionInfoColumn;
                        else
                            translationSetActionInfoColumn = "" + translationSetActionInfoColumn;
                    }
                    qsnPathPdfTable.AddCell(new PdfPCell(new Phrase((questionPathRoot.Info + translationSetActionValue + translationSetActionInfoColumn), fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }
                else
                {
                    qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathRoot.Info, fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                }

                string[] feedBackArray = questionPathRoot.FeedBackType.Split(',');
                if (feedBackArray[0] == "Extra info")
                    extraInfo = feedBackArray[1];
                if (feedBackArray[0] == "Anomaly")
                    anomaly = feedBackArray[1];
                if (feedBackArray[0] == "Pallets")
                    pallets = feedBackArray[1];

                qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(extraInfo, fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(anomaly, fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(pallets, fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });
                qsnPathPdfTable.AddCell(new PdfPCell(new Phrase(questionPathRoot.PartnerCode, fontNormal)) { BackgroundColor = questionPathColoring, BorderColor = WebColors.GetRGBColor("#DCDCDC"), BorderWidth = .5f });

                return qsnPathPdfTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public PdfPTable RecursiveShowLayoutFuncPDF(List<BoShowLayout> actLayQuesions, int padding, int parentId, BaseColor bgCSSForCells, string activityCSS, string language, int layoutId, ref PdfPTable childNodes, List<ShowlayoutInfoTranslations> objInfoTranslations)
        {
            try
            {
                foreach (var qstnPaths in actLayQuesions.Where(a => a.ParentId == parentId))
                {
                    AppendQuetionPathsPDF(qstnPaths, bgCSSForCells, activityCSS, padding, language, layoutId, ref childNodes, objInfoTranslations);

                    if (actLayQuesions.Where(x => x.ParentId == qstnPaths.QuestionId).Count() > 0)
                    {
                        RecursiveShowLayoutFuncPDF(actLayQuesions, padding + 13, qstnPaths.QuestionId, bgCSSForCells, activityCSS, language, layoutId, ref childNodes, objInfoTranslations);
                    }
                }
                return childNodes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BaseColor GetQuetionPathColorForPDF(int? colorNumber, int layoutId, string activityCss)
        {
            try
            {
                BaseColor myColor = WebColors.GetRGBColor("#FFFFFF");
                bool layoutColor = Convert.ToBoolean(showLayoutRepository.GetLayoutColor(layoutId));
                if (layoutColor)
                {
                    if (colorNumber == 0)
                    {
                        myColor = WebColors.GetRGBColor("#36d836");
                        return myColor;
                    }
                    else if (colorNumber == 2)
                    {
                        myColor = WebColors.GetRGBColor("#e6e614");
                        return myColor;
                    }
                    else if (colorNumber == 3)
                    {
                        myColor = WebColors.GetRGBColor("#e82e2e");
                        return myColor;
                    }
                    else
                    {
                        if (activityCss == "class='sec-table'")
                        {
                            myColor = WebColors.GetRGBColor("#ebebeb");
                        }
                        else
                        {
                            myColor = WebColors.GetRGBColor("#f7f7f7");
                        }

                        return myColor;
                    }
                }
                else
                {
                    if (activityCss == "class='sec-table'")
                    {
                        myColor = WebColors.GetRGBColor("#ebebeb");
                    }
                    else
                    {
                        myColor = WebColors.GetRGBColor("#f7f7f7");
                    }
                    return myColor;
                }
            }
            catch (Exception ex)
            { throw ex; }
        }
        public BaseColor GetColorForScreenLayou(string color)
        {
            try
            {
                BaseColor myColor = WebColors.GetRGBColor("#ADADAD");
                if (color == "Green")
                {
                    myColor = WebColors.GetRGBColor("#36d836");
                    return myColor;
                }
                else if (color == "Yellow")
                {
                    myColor = WebColors.GetRGBColor("#e6e614");
                    return myColor;
                }
                else if (color == "Red")
                {
                    myColor = WebColors.GetRGBColor("#e82e2e");
                    return myColor;
                }
                else
                {
                    myColor = WebColors.GetRGBColor("#cccccc");
                    return myColor;
                }
            }
            catch (Exception ex)
            { throw ex; }
        }
        public BaseColor ScreenlayRemColors(int isModification)
        {
            try
            {
                BaseColor myColor = WebColors.GetRGBColor("#ADADAD");
                if (isModification == (int)EnumCopyColorCode.Green)
                {
                    myColor = WebColors.GetRGBColor("#36d836");
                    return myColor;
                }
                else if (isModification == (int)EnumCopyColorCode.Yellow)
                {
                    myColor = WebColors.GetRGBColor("#e6e614");
                    return myColor;
                }
                else if (isModification == (int)EnumCopyColorCode.Red)
                {
                    myColor = WebColors.GetRGBColor("#e82e2e");
                    return myColor;
                }
                else
                {
                    myColor = WebColors.GetRGBColor("#cccccc");
                    return myColor;
                }
            }
            catch (Exception ex)
            { throw ex; }
        }

        #endregion

        #region CommonMethods
        public string GetDefaultShowlayoutLanguage(int layoutId)
        {
            try
            {
                string languaueId = showLayoutRepository.GetDefaultShowlayoutLanguage(layoutId);
                return languaueId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult GetAllActivityandRegByLayoutId(int LayoutId)
        {

            ShowLayoutViewModel showlayoutViewModel = new ShowLayoutViewModel();

            showlayoutViewModel.ActivitiesRegistrations = showLayoutRepository.GetActiveRegistrationByLayoutId(LayoutId);

            return Json(showlayoutViewModel);


        }
        public ActionResult GetAllShowLayoutDetails(int activityId)
        {

            var returnShowLayoutList = showLayoutRepository.GetAllShowLayoutModelByActivitesID(activityId);


            return Json(returnShowLayoutList);
        }
        public ActionResult GetLanguagesByLayoutId(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<LanguageMaster> List = showLayoutRepository.GetLanguages(layoutId);
                    return Json(List);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetActivitiesRegistrationByLayoutId(int layoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<int> activitiesRegistrationList = showLayoutRepository.GetActivitiesRegistrationByLayoutId(layoutId);
                    return Json(activitiesRegistrationList);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        #endregion

        ///<summary>
        ///show Layout Page Methods End here
        ///</summary>
    }
}

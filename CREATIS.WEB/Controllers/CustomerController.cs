﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using CREATIS.Controllers;
using CREATIS.Infrastructure.IRepository;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CREATIS.WEB.Controllers
{
    public class CustomerController : BaseController
    {
        private readonly ICustomerRepository CustomerRepository;
        public ConnectionStrings connectionStrings { get; }
        public CustomerController(ICustomerRepository CusRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate)
        {
            this.CustomerRepository = CusRepo;
            this.connectionStrings = connectionTemplate.Value;
        }
        // GET: /<controller>/
        public ActionResult Index()
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();

                    if (HasPrivilege(userCredentials.permissions).Contains("S") && !IsCustomerLogin(userCredentials.permissions))
                    {
                        ViewBag.username = userCredentials.displayName;
                        return View();
                    }
                    else
                        return View("Unauthorised");

                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");
        }
        public ActionResult SaveCustomer(string action, List<Customers> added, List<Customers> changed)
        {
            try
            {
                if (changed != null && changed.Count > 0)
                {
                    foreach (Customers d in changed)
                    {

                        var Custm = CustomerRepository.GetCustomer().Where(r => r.CustomerId != d.CustomerId && r.Name == d.Name).ToList();
                        if (Custm.Count > 0)
                            return Json("error");
                        else
                            CustomerRepository.SaveCustomer(d);
                    }
                }
                if (added != null && added.Count > 0)
                {
                    var userCredentials = GetUserCredentials();
                    foreach (Customers d in added)
                    {
                        d.CreatedBy = userCredentials.id.ToString();
                        var Custm = CustomerRepository.GetCustomer().Where(r => r.CustomerId != d.CustomerId && r.Name == d.Name).ToList();
                        if (Custm.Count > 0)
                            return Json("error");
                        else
                            CustomerRepository.SaveCustomer(d);
                    }
                    return Json("success");
                }
                return Json("success");
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
        public ActionResult GetCustomer()
        {
            try
            {
                List<Customers> cusList = new List<Customers>();
                cusList = CustomerRepository.GetCustomer();
                return Json(cusList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
        public ActionResult DeleteCustomer(int customerId)
        {
            try
            {
                List<Customers> cusList = new List<Customers>();
                CustomerRepository.DeleteCustomer(customerId);
                cusList = CustomerRepository.GetCustomer().OrderBy(x => x.Name).ToList();
                return Json(cusList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
        public ActionResult GetCustomerSystemType()
        {
            try
            {
                List<CustomerSystemType> customerSystemTypeList = new List<CustomerSystemType>();
                customerSystemTypeList = CustomerRepository.GetCustomerSystemType().OrderBy(x => x.Id).ToList();
                return Json(customerSystemTypeList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CREATIS.ApplicationCore.BusinessObjects;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using CREATIS.Web;

namespace CREATIS.WEB.Controllers
{
    public class LoginController : Controller
    {
        public UserAuthentication userAuthenticationConfiguration { get; }
        public OthersSettings othersSettings { get; }
        public LoginController(Microsoft.Extensions.Options.IOptions<UserAuthentication> UserAuthentication, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings)
        {
            userAuthenticationConfiguration = UserAuthentication.Value;
            this.othersSettings = othersSettings.Value;
        }
        public ActionResult Index()
        {
            try
            {
                if (userAuthenticationConfiguration.isLive == 1)
                {
                    StringBuilder sb = new StringBuilder();
                    var parameters = HttpContext.Request.QueryString;
                    var token = HttpContext.Request.Path.ToString().Split('/').Last();
                    if (!string.IsNullOrEmpty(token))
                    {
                        UserBusinessModal result = new UserBusinessModal();

                        string uri = userAuthenticationConfiguration.myTransicsURI; //"/v1/token/verify";
                        var querystring = "?apiKey=" + userAuthenticationConfiguration.apiKey + "&timestamp=" + GetCurrentTimestamp_Msecs() + "&token=" + token; // "alt1rgyTu3hgJ86rY0WBKIlK" 
                        var signature = HttpUtility.UrlEncode(HashHmac.GetHashHamac("GET " + uri + querystring, userAuthenticationConfiguration.secretKey)); //"th8nI8vWJPIZ0BRdgVq9cNRm"
                        string urlManual = userAuthenticationConfiguration.myTransicsBaseURI; //"https://qa.mytransics.com/api";
                        string url = String.Format("{0}{1}&signature={2}", urlManual + uri, querystring, signature);

                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                        request.Method = "GET";

                        WebResponse webResponse = request.GetResponse();
                        using (Stream webStream = webResponse.GetResponseStream())
                        {
                            if (webStream != null)
                            {
                                using (StreamReader responseReader = new StreamReader(webStream))
                                {
                                    string response = responseReader.ReadToEnd();
                                    result = JsonConvert.DeserializeObject<UserBusinessModal>(response);
                                    Console.WriteLine(response);
                                    HttpContext.Session.SetComplexData("UserLoginDet", result);
                                }
                            }
                        }
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return View("Unauthorised");
                    }

                }
                else
                {

                    //var response = "{'id':'58de51c670a394bf200cbfda','displayName':'Krishnakumar','company':'53c8d50ef50e56154a6fa416','companyName':'Transics NV(R&D)','customerId':18247,'customerName':'17111 Transit Transport Flensburg Gmbh &Co.Kg','name':{'givenName':'Krishnakumar','familyName':'Krishnakumar','middleName':'Krishnakumar'},'timezone':'Europe / Brussels','language':'en','locale':'en','emails':['selvam.thangaraj@wabco-auto.com'],'isIntegrator':false,'impersonatorId':null,'permissions':['Administrator'],'locations':[],'accessableCompanies':['53c8d50ef50e56154a6fa416']}";
                    var response = "{'id':'58de51c670a394bf200cbfda','displayName':'Krishnakumar','company':'53c8d50ef50e56154a6fa416','companyName':'Transics NV(R&D)','customerId':18247,'customerName':'17111 Transit Transport Flensburg Gmbh &Co.Kg','name':{'givenName':'Krishnakumar','familyName':'Krishnakumar','middleName':'Krishnakumar'},'timezone':'Europe / Brussels','language':'en','locale':'en','emails':['selvam.thangaraj@wabco-auto.com'],'isIntegrator':false,'impersonatorId':null,'permissions':['Administrator','CustomizingEngineer','Default','ProjectManager','Translation Coördinator'],'locations':[],'accessableCompanies':['53c8d50ef50e56154a6fa416']}";
                    var result = JsonConvert.DeserializeObject<UserBusinessModal>(response);
                    HttpContext.Session.SetComplexData("UserLoginDet", result);
                    return RedirectToAction("Index", "Home");
                }
            }
            catch(Exception ex)
            {
                return View("Unauthorised");
            }
        }
        public ActionResult SessionRecreation()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                var parameters = HttpContext.Request.QueryString;
                var token = HttpContext.Request.Path.ToString().Split('/').Last();
                if (userAuthenticationConfiguration.isLive == 1)
                {
                    UserBusinessModal result = new UserBusinessModal();

                    string uri = userAuthenticationConfiguration.myTransicsURI; //"/v1/token/verify";
                    var querystring = "?apiKey=" + userAuthenticationConfiguration.apiKey + "&timestamp=" + GetCurrentTimestamp_Msecs() + "&token=" + token; // "alt1rgyTu3hgJ86rY0WBKIlK" 
                    var signature = HttpUtility.UrlEncode(HashHmac.GetHashHamac("GET " + uri + querystring, userAuthenticationConfiguration.secretKey)); //"th8nI8vWJPIZ0BRdgVq9cNRm"
                    string urlManual = userAuthenticationConfiguration.myTransicsBaseURI; //"https://qa.mytransics.com/api";
                    string url = String.Format("{0}{1}&signature={2}", urlManual + uri, querystring, signature);

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.Method = "GET";

                    WebResponse webResponse = request.GetResponse();
                    using (Stream webStream = webResponse.GetResponseStream())
                    {
                        if (webStream != null)
                        {
                            using (StreamReader responseReader = new StreamReader(webStream))
                            {
                                string response = responseReader.ReadToEnd();
                                result = JsonConvert.DeserializeObject<UserBusinessModal>(response);
                                Console.WriteLine(response);
                                HttpContext.Session.SetComplexData("UserLoginDet", result);
                            }
                        }
                    }
                }
                else
                {
                    var response = "{'id':'58de51c670a394bf200cbfda','displayName':'Krishnakumar','company':'53c8d50ef50e56154a6fa416','companyName':'Transics NV(R&D)','customerId':18247,'customerName':'17111 Transit Transport Flensburg Gmbh &Co.Kg','name':{'givenName':'Krishnakumar','familyName':'Krishnakumar','middleName':'Krishnakumar'},'timezone':'Europe / Brussels','language':'en','locale':'en','emails':['selvam.thangaraj@wabco-auto.com'],'isIntegrator':false,'impersonatorId':null,'permissions':['Administrator','CustomizingEngineer','Default','ProjectManager','Translation Coördinator'],'locations':[],'accessableCompanies':['53c8d50ef50e56154a6fa416']}";
                    var result = JsonConvert.DeserializeObject<UserBusinessModal>(response);
                    HttpContext.Session.SetComplexData("UserLoginDet", result);
                }
                return Json(new { error = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { error = "Success" });
            }


        }
        public ActionResult SessionExpired()
        {
            ViewData["Previlage"] = new List<string>();
            ViewBag.SessionLink = othersSettings.SessionLink;
            return View();
        }
        private long GetCurrentTimestamp_Msecs()
        {
            return (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        }
    }
}

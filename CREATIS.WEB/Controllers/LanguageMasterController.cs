﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using CREATIS.Controllers;
using CREATIS.Infrastructure.IRepository;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CREATIS.WEB.Controllers
{
    public class LanguageMasterController :BaseController
    {
        // GET: /<controller>/
        private readonly ILanguageMasterRepository LanguageMasterRepository;
        public ConnectionStrings connectionStrings { get; }
        public OthersSettings othersSettings { get; }
        public LanguageMasterController(ILanguageMasterRepository LangRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings)
        {
            this.LanguageMasterRepository = LangRepo;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
        }
      

        public ActionResult Index()
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();
                    ViewBag.username = userCredentials.displayName;
                    ViewBag.SessionLink = othersSettings.SessionLink;
                    return View();
                   
                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");

        }
        public ActionResult SaveLanguageMaster(string action, List<LanguageMaster> added, List<LanguageMaster> changed)
        {
            try
            {
                string result = "";

                if (changed != null)
                {
                    if (changed.Count > 0)
                    {
                        foreach (LanguageMaster d in changed)
                        {
                           
                            var LangMas = LanguageMasterRepository.GetLanguageMaster().Where(r => r.LanguageId == d.LanguageId && r.LanguageDescription==d.LanguageDescription).ToList();
                            if (LangMas.Count > 0)
                            return Json("error"); 
                                
                            else
                            {
                                var userCredentials = GetUserCredentials();
                                d.UpdatedBy = userCredentials.id.ToString();
                                d.UpdatedOn = DateTime.Now;
                                result = LanguageMasterRepository.SaveLanguageMaster(d);
                            }
                                

                            
                        }

                    }
                }

                if (added != null)
                {
                    if (added.Count > 0)
                    {
                        foreach (LanguageMaster d in added)
                        {


                            var LangMas = LanguageMasterRepository.GetLanguageMaster().Where(r => r.LanguageId == d.LanguageId && r.LanguageDescription == d.LanguageDescription).ToList();
                            if (LangMas.Count > 0)
                                return Json("error");

                            else
                            {
                                var userCredentials = GetUserCredentials();
                                d.CreatedBy = userCredentials.id.ToString();
                                d.CreatedOn = DateTime.Now;
                                result = LanguageMasterRepository.SaveLanguageMaster(d);
                            }
                               
                          
                        }
                        return Json("success");
                    }
                }
                return Json("success");
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
        public ActionResult GetLanguageMaster()
        {
            try
            {
                List<LanguageMaster> langList = new List<LanguageMaster>();
                langList = LanguageMasterRepository.GetLanguageMaster();
                return Json(langList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
    }
}

﻿using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using CREATIS.Controllers;
using CREATIS.Infrastructure.IRepository;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CREATIS.WEB.Controllers
{
    public class InputMasterController : BaseController
    {
        private readonly IInputMasterRepository InputMasterRepository;
        public ConnectionStrings connectionStrings { get; }
        public InputMasterController(IInputMasterRepository InRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate)
        {
            this.InputMasterRepository = InRepo;
            this.connectionStrings = connectionTemplate.Value;
        }
        // GET: /<controller>/
        public ActionResult Index()
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();

                    if (HasPrivilege(userCredentials.permissions).Contains("S") && !IsCustomerLogin(userCredentials.permissions))
                    {
                        ViewBag.username = userCredentials.displayName;
                        return View();
                    }
                    else
                        return View("Unauthorised");

                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");
        }
        public ActionResult SaveInputMaster(string query)
        {
            InputMaster inList = JsonConvert.DeserializeObject<InputMaster>(query);
            string result = "";
            try
            {
                result = InputMasterRepository.SaveInputMaster(inList);

            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
            return Json(result);
        }
        public ActionResult GetInputMaster()
        {
            try
            {
                List<InputMaster> inList = new List<InputMaster>();
                inList = InputMasterRepository.GetInputMaster();
                return Json(inList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
    }
}

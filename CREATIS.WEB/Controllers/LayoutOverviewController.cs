﻿using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using CREATIS.Controllers;
using CREATIS.Infrastructure.IRepository;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CREATIS.WEB.Controllers
{
    public class LayoutOverviewController : BaseController
    {
        private readonly ILayoutOverviewRepository LayoutOverView;
        public ConnectionStrings connectionStrings { get; }
        public OthersSettings othersSettings { get; }
        public LayoutOverviewController(ILayoutOverviewRepository InRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings)
        {
            this.LayoutOverView = InRepo;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
        }
        // GET: /<controller>/
        public ActionResult Index()
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();
                    if (HasPrivilege(userCredentials.permissions).Contains("QP"))
                    {
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        return View();
                    }
                    else
                        return View("Unauthorised");
                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");
        }

        public ActionResult GetOverallLayoutValues()
        {
            try
            {
                List<LayoutOverviewValues> resultList;
                resultList = LayoutOverView.GetOverallLayoutValues();
                return Json(resultList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
    }
}

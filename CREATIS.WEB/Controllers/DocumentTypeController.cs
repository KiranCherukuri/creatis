﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using CREATIS.Controllers;
using CREATIS.Infrastructure.IRepository;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CREATIS.WEB.Controllers
{
    public class DocumentTypeMasterController : BaseController
    {
        // GET: /<controller>/

        private readonly IDocumentTypeMasterRepository DocumentTypeMasterRepository;
        public ConnectionStrings connectionStrings { get; }
        public OthersSettings othersSettings { get; }

        public DocumentTypeMasterController(IDocumentTypeMasterRepository DocumentTypeRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings)
        {
            this.DocumentTypeMasterRepository = DocumentTypeRepo;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
        }
        public ActionResult Index()
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();

                    if (HasPrivilege(userCredentials.permissions).Contains("S") && !IsCustomerLogin(userCredentials.permissions))
                    {
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        return View();
                    }
                    else
                        return View("Unauthorised");

                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");
        }
        public ActionResult SaveDocumentTypeMaster(string action, List<DocumentTypeMaster> added, List<DocumentTypeMaster> changed)
        {
            System.DateTime time = DateTime.Now;
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    string result = "";

                    if (changed != null)
                    {
                        if (changed.Count > 0)
                        {
                            foreach (DocumentTypeMaster d in changed)
                            {
                                var Creatis = DocumentTypeMasterRepository.GetDocumentTypeMaster().Where(r => r.DocumentTypeId != d.DocumentTypeId && r.DocumentTypeDescription == d.DocumentTypeDescription).ToList();
                                if (Creatis.Count > 0)
                                    return Json("error");
                                else
                                {
                                    var userCredentials = GetUserCredentials();
                                    d.UpdatedBy = userCredentials.id.ToString();
                                    d.UpdatedOn = DateTime.Now;
                                    result = DocumentTypeMasterRepository.SaveDocumentTypeMaster(d);
                                }
                            }
                        }
                    }
                    if (added != null)
                    {
                        if (added.Count > 0)
                        {
                            foreach (DocumentTypeMaster d in added)
                            {
                                var Creatis = DocumentTypeMasterRepository.GetDocumentTypeMaster().Where(r => r.DocumentTypeId != d.DocumentTypeId && r.DocumentTypeDescription == d.DocumentTypeDescription).ToList();
                                if (Creatis.Count > 0)
                                    return Json("error");
                                else
                                {
                                    var userCredentials = GetUserCredentials();
                                    d.CreatedBy = userCredentials.id.ToString();
                                    d.CreatedOn = DateTime.Now;
                                    result = DocumentTypeMasterRepository.SaveDocumentTypeMaster(d);
                                }
                            }
                        }
                    }
                    return Json("success");
                }
                catch (Exception ex)
                {
                    string e = ex.ToString();
                    return Json(e);
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult GetDocumentTypeMaster()
        {
            try
            {
                List<DocumentTypeMaster> DocumentTypeList = new List<DocumentTypeMaster>();
                DocumentTypeList = DocumentTypeMasterRepository.GetDocumentTypeMaster();
                return Json(DocumentTypeList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using CREATIS.Infrastructure.Repository;
using CREATIS.Infrastructure.IRepository;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace CREATIS.Controllers
{
    public class ScreenLayoutController : BaseController
    {
        public ConnectionStrings connectionStrings { get; }

        public readonly IScreenLayoutRepository ScreenLogicRepository;
        public OthersSettings othersSettings { get; }
        public ScreenLayoutController(IScreenLayoutRepository ScreenLogicRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings)
        {
            this.ScreenLogicRepository = ScreenLogicRepo;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
        }

        ///<summary>
        /// Screen Layout Page Methods Starts here
        ///</summary>

        #region 



        ///<summary>
        /// Screen Layout View render method start
        ///</summary>
        public ActionResult ScreenLayout(int LayoutID)
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();
                    if ((HasPrivilege(userCredentials.permissions).Contains("SL") || HasPrivilege(userCredentials.permissions).Contains("LSL") || HasPrivilege(userCredentials.permissions).Contains("QPSL") || HasPrivilege(userCredentials.permissions).Contains("TSL")) && IsCustomerLogin(userCredentials.permissions))
                    {
                        var customerId = GetLayoutCustomerId(LayoutID);
                        if (GetCustomerLayoutList(userCredentials.company, customerId).Any(x => x == LayoutID))
                        {
                            LayoutStatusPrivilege(LayoutID);
                            RoleWithLayoutStatusPrivilege(LayoutID, userCredentials.permissions);
                            ViewBag.username = userCredentials.displayName;
                            ViewBag.SessionLink = othersSettings.SessionLink;
                            AddRecentActivity(LayoutID, userCredentials.id);
                            return View();
                        }
                        else
                            return View("Unauthorised");
                    }
                    else if (HasPrivilege(userCredentials.permissions).Contains("SL") || HasPrivilege(userCredentials.permissions).Contains("LSL") || HasPrivilege(userCredentials.permissions).Contains("QPSL") || HasPrivilege(userCredentials.permissions).Contains("TSL"))
                    {
                        LayoutStatusPrivilege(LayoutID);
                        RoleWithLayoutStatusPrivilege(LayoutID, userCredentials.permissions);
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        AddRecentActivity(LayoutID, userCredentials.id);
                        return View();
                    }
                    else
                        return View("Unauthorised");
                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");
        }
        ///<summary>
        /// Screen Layout View render method end
        ///</summary>


        ///<summary>
        /// Screen Layout Get registration for the specific layout method start
        ///</summary>
        public ActionResult GetRegistration(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(ScreenLogicRepository.GetRegistration(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Screen Layout Get registration for the specific layout method end
        ///</summary>

        ///<summary>
        /// Screen Layout Get activities for the specific layout method start
        ///</summary>
        public ActionResult GetActivities(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(ScreenLogicRepository.GetActivities(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Screen Layout Get activities for the specific layout method end
        ///</summary>

        ///<summary>
        /// Screen Layout Get registration position for the specific layout method start
        ///</summary>
        public ActionResult GetRegistrationPosition(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(ScreenLogicRepository.GetRegistrationPosition(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Screen Layout Get registration position for the specific layout method end
        ///</summary>

        ///<summary>
        /// Screen Layout Get activities position for the specific layout method start
        ///</summary>
        public ActionResult GetActivitiesPosition(int LayoutID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(ScreenLogicRepository.GetActivitiesPosition(LayoutID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Screen Layout Get activities position for the specific layout method end
        ///</summary>

        ///<summary>
        /// Screen Layout  update activities registration position for the specific layout method start
        ///</summary>
        public ActionResult UpdateActivitiesRegistrationPosition(int layoutId, string actRegId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    string[] ActRegposition = actRegId.Split(',');
                    for (int i = 1; i <= ActRegposition.Length; i++)
                    {
                        ScreenLogicRepository.UpdateActivitiesRegistrationPosition(layoutId, Convert.ToInt16(ActRegposition[i - 1]), i);
                    }
                    return Json("Updated");
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Screen Layout  update activities registration position for the specific layout method end
        ///</summary>

        ///<summary>
        /// Screen Layout reset update activities registration position for the specific layout method start
        ///</summary>
        public ActionResult UpdateDeActivitiesRegistrationPosition(int layoutId, string actRegId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    string[] ActRegposition = actRegId.Split(',');
                    for (int i = 1; i <= ActRegposition.Length; i++)
                    {
                        ScreenLogicRepository.UpdateDeActivitiesRegistrationPosition(layoutId, Convert.ToInt16(ActRegposition[i - 1]), 0);
                    }
                    return Json("Updated");
                }
                catch (Exception ex)
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Screen Layout reset update activities registration position for the specific layout method end
        ///</summary>


        #endregion

        ///<summary>
        /// Screen Layout Page Methods End here
        ///</summary>
    }
}
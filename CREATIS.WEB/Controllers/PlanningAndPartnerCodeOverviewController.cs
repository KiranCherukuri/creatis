﻿using CREATIS.ApplicationCore.BusinessObjects;
using CREATIS.ApplicationCore.Modals;
using CREATIS.Controllers;
using CREATIS.Infrastructure.IRepository;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CREATIS.WEB.Controllers
{
    public class PlanningAndPartnerCodeOverviewController : BaseController
    {
        private readonly IPlanningAndPartnerCodeOverviewRepository PlanningAndPartnerCodeOverviewRepository;
        public ConnectionStrings connectionStrings { get; }
        public OthersSettings othersSettings { get; }
        public PlanningAndPartnerCodeOverviewController(IPlanningAndPartnerCodeOverviewRepository InRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings)
        {
            this.PlanningAndPartnerCodeOverviewRepository = InRepo;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
        }
        // GET: /<controller>/
        public ActionResult Index(int layoutId)
        {
            if (CheckForInternetConnection())
            {
                if (IsServerConnected(connectionStrings))
                {
                    var userCredentials = GetUserCredentials();
                    if (HasPrivilege(userCredentials.permissions).Contains("QP"))
                    {
                        LayoutStatusPrivilege(layoutId);
                        RoleWithLayoutStatusPrivilege(layoutId, userCredentials.permissions);
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        AddRecentActivity(layoutId, userCredentials.id);
                        return View();
                    }
                    else
                        return View("Unauthorised");
                }
                else
                    return Json(new
                    {
                        error = GetStatusMessage(StatusCode.E01)
                    });
            }
            else
                return View("NetworkError");
        }
        public ActionResult GetPlanningAndPartnerCodeOverview(int customerRefId, int layoutRefId)
        {
            try
            {
                List<PlanningAndPartnerCodeOverview> resultList;
                resultList = PlanningAndPartnerCodeOverviewRepository.GetPlanningAndPartnerCodeOverview(customerRefId, layoutRefId);
                return Json(resultList);
            }
            catch (Exception ex)
            {
                string e = ex.ToString();
                return Json(e);
            }
        }
    }
}

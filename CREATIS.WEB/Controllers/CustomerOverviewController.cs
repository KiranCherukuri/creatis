﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using CREATIS.Infrastructure.Repository;
using CREATIS.ApplicationCore.Modals;
using CREATIS.ApplicationCore.BusinessObjects;
using System.Web;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using CREATIS.Infrastructure.IRepository;

namespace CREATIS.Controllers
{
    public class CustomerOverviewController : BaseController
    {
        private readonly ICustomerOverviewRepository CustomerLogicRepository;
        public readonly ITranslatorRepository translatorRepository;

        public readonly IDashboardRepository dashboardRepository;
        public ConnectionStrings connectionStrings { get; }
        public OthersSettings othersSettings { get; }
        public CustomerOverviewController(ICustomerOverviewRepository CustomerOverviewLogicRepo, ITranslatorRepository translatorRepo, Microsoft.Extensions.Options.IOptions<ConnectionStrings> connectionTemplate, Microsoft.Extensions.Options.IOptions<OthersSettings> othersSettings, IDashboardRepository dashboardRepo)
        {
            this.CustomerLogicRepository = CustomerOverviewLogicRepo;
            this.translatorRepository = translatorRepo;
            this.connectionStrings = connectionTemplate.Value;
            this.othersSettings = othersSettings.Value;
            this.dashboardRepository = dashboardRepo;
        }

        ///<summary>
        /// Customer Overview Page Methods Starts here
        ///</summary>

        #region 

        ///<summary>
        /// Customer Overview View render method start
        /// Customer Overview Page view
        ///</summary>
        public ActionResult CustomerOverview(int customerid)
        {
            if (IsServerConnected(connectionStrings))
            {
                var userCredentials = GetUserCredentials();
                if (HasPrivilege(userCredentials.permissions).Contains("CO") && IsCustomerLogin(userCredentials.permissions))
                {
                    if (customerid != GetLoginCustomer(userCredentials.company, 0))
                    {
                        ViewBag.newlayout = "No";
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        return View();
                    }
                    else if (customerid == GetLoginCustomer(userCredentials.company, customerid))
                    {
                        ViewBag.newlayout = "Yes";
                        ViewBag.username = userCredentials.displayName;
                        ViewBag.SessionLink = othersSettings.SessionLink;
                        return View();
                    }
                    else
                        return View("Unauthorised");
                }
                else if (HasPrivilege(userCredentials.permissions).Contains("CO"))
                {
                    ViewBag.newlayout = "Yes";
                    ViewBag.username = userCredentials.displayName;
                    ViewBag.SessionLink = othersSettings.SessionLink;
                    return View();
                }
                else
                    return View("Unauthorised");

            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview View render method end
        ///</summary>

        ///<summary>
        /// Customer Overview TO show the layout based on customer selection from home page in grid Methods stats here
        /// To get available layout based on customer
        ///</summary>
        public ActionResult CustomerOverviewAvailableLayout(int customerid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<CustomerOverviewDetail> layoutList = CustomerLogicRepository.GetLayoutList(customerid).ToList();

                    var userCredentials = GetUserCredentials();
                    var customerId = GetLoginCustomer(userCredentials.company, 0);

                    if (IsCustomerLogin(userCredentials.permissions) && customerId != customerid)
                        layoutList = layoutList.Where(x => x.LayoutStatusRefId == (int)EnumLayoutStatus.Finished).ToList();

                    return Json(layoutList);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        public ActionResult ValidateLayoutName(string LayoutName, int CustomerId, int LayoutId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var validationresult = CustomerLogicRepository.ValidateLayoutName(LayoutName, CustomerId, LayoutId);
                    if (validationresult)
                        return Json(new { error = "Layout Name Already Exist" });
                    else
                        return Json("");
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }

        ///<summary>
        /// Customer Overview TO show the layout based on customer selection from home page in grid Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview to show customer name what was user selected Methods stats here
        ///</summary>
        public ActionResult GetCustomerName(int customerid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    //var userCredentials = GetUserCredentials();
                    string customername = CustomerLogicRepository.GetCustomerName(customerid);
                    //List<CustomerOverviewDetail> List = CustomerLogicRepository.GetLayoutList(customerid, userCredentials.id.ToString()).ToList();
                    return Json(customername);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview to show customer name what was user selected Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to show info column name based on customer selection in modal popup Methods starts here
        ///</summary>
        public ActionResult InfoColumnDetails(int customerid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    List<InfocolumnDetails> List = CustomerLogicRepository.GetInfoColumn(customerid, userCredentials.id).ToList();
                    return Json(List);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview page to show info column name based on customer selection in modal popup Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to save info column name for based on customer selection in modal popup Methods starts here
        ///</summary>
        public ActionResult SaveInfoColumnDetails(string ID, string ColumnName, string customerRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    InfoColumns InfoCols = new InfoColumns();
                    InfoCols.CreatedBy = userCredentials.id.ToString();
                    InfoCols.CreatedOn = time;
                    InfoCols.CustomerRefId = Convert.ToInt16(customerRefId);
                    InfoCols.ICM_Column_Ref_No = Convert.ToInt16(ID);
                    InfoCols.IC_Column_Name = ColumnName;
                    InfoCols.IS_Active = 1;
                    //InfoCols.IC_ID = 1;
                    InfoCols.UpdatedBy = userCredentials.id.ToString();
                    InfoCols.UpdatedOn = time;
                    var InfoColumnresult = CustomerLogicRepository.SaveInfoColumn(InfoCols);
                    //LangInfoColumns objLinfo = new LangInfoColumns();
                    //objLinfo.InfoColumnName = InfoColumnresult.IC_Column_Name;
                    //objLinfo.ICRefId = InfoColumnresult.IC_ID;
                    //objLinfo.CustomerRefId = InfoColumnresult.CustomerRefId;
                    //objLinfo.CreatedBy = userCredentials.id.ToString();
                    //objLinfo.CreatedOn = time;
                    //translatorRepository.SaveLangInfoColumns(objLinfo);
                    return Json(InfoColumnresult);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview page to save info column name for based on customer selection in modal popup Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to update info column name for based on customer selection in modal popup Methods starts here
        ///</summary>
        public ActionResult UpdateInfoColumnDetails(string ID, string ColumnName, string customerRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    return Json(CustomerLogicRepository.UpdateInfoColumn(Convert.ToInt16(ID), ColumnName, userCredentials.id.ToString(), time));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Customer Overview page to update info column name for based on customer selection in modal popup Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to delete info column name for based on customer selection in modal popup Methods starts here
        ///</summary>
        public ActionResult DeleteInfoColumnDetails(string ID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(CustomerLogicRepository.DeleteInfoColumn(Convert.ToInt16(ID)));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview page to delete info column name for based on customer selection in modal popup Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to show Integrator based on customer selection in modal popup multiline textbox Methods starts here
        ///</summary>
        public ActionResult GetIntegrator(int customerid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<Integrator> List = CustomerLogicRepository.GetIntegrator(customerid).ToList();
                    return Json(List);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview page to show Integrator based on customer selection in modal popup multiline textbox Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to save Integrator based on customer selection in modal popup multiline textbox Methods starts here
        ///</summary>
        public ActionResult SaveIntegrator(string I_Details, string customerRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    Integrator IR = new Integrator();
                    IR.CreatedBy = userCredentials.id.ToString();
                    IR.CreatedOn = time;
                    IR.CustomerRefId = Convert.ToInt16(customerRefId);
                    IR.I_Details = I_Details;
                    IR.IS_Active = 1;
                    return Json(CustomerLogicRepository.SaveIntegrator(IR));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Customer Overview page to save Integrator based on customer selection in modal popup multiline textbox Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to update Integrator based on customer selection in modal popup multiline textbox Methods starts here
        ///</summary>
        public ActionResult UpdateIntegrator(int I_ID, string I_Details)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    return Json(CustomerLogicRepository.UpdateIntegrator(I_ID, I_Details, userCredentials.id.ToString(), time));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview page to update Integrator based on customer selection in modal popup multiline textbox Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to Delete Integrator based on customer selection in modal popup multiline textbox Methods starts here
        ///</summary>
        public ActionResult DeleteIntegrator(int I_ID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(CustomerLogicRepository.DeleteIntegrator(I_ID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview page to delete Integrator based on customer selection in modal popup multiline textbox Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to show customer notes based on customer selection in modal popup multiline textbox Methods starts here
        ///</summary>
        public ActionResult GetCustomerNotes(int customerid)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<CustomerNotes> List = CustomerLogicRepository.GetCustomerNotes(customerid).ToList();
                    return Json(List);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview page to show customer notes based on customer selection in modal popup multiline textbox Methods End here
        ///</summary>

        ///<summary>
        /// Customer Overview page to save customer notes based on customer selection in modal popup multiline textbox Methods starts here
        ///</summary>
        public ActionResult SaveCustomerNotes(string CN_Details, string customerRefId)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    CustomerNotes CN = new CustomerNotes();
                    CN.CreatedBy = userCredentials.id.ToString();
                    CN.CreatedOn = time;
                    CN.CustomerRefId = Convert.ToInt16(customerRefId);
                    CN.CN_Details = CN_Details;
                    CN.IS_Active = 1;
                    return Json(CustomerLogicRepository.SaveCustomerNotes(CN));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Customer Overview page to save customer notes based on customer selection in modal popup multiline textbox Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to update customer notes based on customer selection in modal popup multiline textbox Methods starts here
        ///</summary>
        public ActionResult UpdateCustomerNotes(int CN_ID, string CN_Details)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    DateTime time = DateTime.Now;
                    var userCredentials = GetUserCredentials();
                    return Json(CustomerLogicRepository.UpdateCustomerNotes(CN_ID, CN_Details, userCredentials.id.ToString(), time));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview page to update customer notes based on customer selection in modal popup multiline textbox Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to delete customer notes based on customer selection in modal popup multiline textbox Methods starts here
        ///</summary>
        public ActionResult DeleteCustomerNotes(int CN_ID)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    return Json(CustomerLogicRepository.DeleteCustomerNotes(CN_ID));
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview page to delete customer notes based on customer selection in modal popup multiline textbox Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to show available languages from Language master table from database for create new layout in modal popup Methods starts here
        ///</summary>
        public ActionResult GetLanguages()
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    List<LanguageMaster> List = CustomerLogicRepository.GetLanguages().ToList();
                    return Json(List);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });
        }
        ///<summary>
        /// Customer Overview page to show available languages from Language master table from database for create new layout in modal popup Methods end here
        ///</summary>

        ///<summary>
        /// Customer Overview page to Accepting new layout name and main language select by user in language dropdown in modal popup Methods starts here
        ///</summary>
        public ActionResult SaveLayoutDetails(int LanguageId, string LayoutName, int customerRefId, int defaultlan)
        {
            if (IsServerConnected(connectionStrings))
            {
                try
                {
                    var userCredentials = GetUserCredentials();
                    var fromMail = (!userCredentials.emails.Any() ? null : userCredentials.emails.Where(x => x.Trim().Length > 0).Select(x => x).Distinct().AsEnumerable().Aggregate((a, b) => a + "," + b));
                    DateTime time = DateTime.Now;

                    Layout LayoutDetails = new Layout();
                    LayoutDetails.CustomerRefId = customerRefId;
                    LayoutDetails.LayoutName = LayoutName.Replace("`", "+");
                    LayoutDetails.LayoutType = "-";
                    LayoutDetails.LayoutOrigin = "New";
                    LayoutDetails.LayoutStatusRefId = (int)EnumLayoutStatus.InProgress;
                    LayoutDetails.IsLocked = 1;
                    LayoutDetails.EmailId = fromMail;
                    LayoutDetails.InitiatorEmailId = fromMail;
                    LayoutDetails.CreatedBy = userCredentials.id.ToString();
                    LayoutDetails.CreatedOn = time;
                    LayoutDetails.UpdatedBy = userCredentials.id.ToString();
                    LayoutDetails.UpdatedOn = time;
                    LayoutDetails.IsCopyLayout = false;
                    LayoutDetails = CustomerLogicRepository.SaveLayout(LayoutDetails);

                    LayoutNotes layoutNotes = new LayoutNotes();
                    layoutNotes.LayoutRefId = LayoutDetails.LayoutId;
                    layoutNotes.Notes = "Layout cretaed.";
                    layoutNotes.CreatedBy = userCredentials.id.ToString();
                    layoutNotes.CreatedOn = DateTime.Now;
                    layoutNotes.CreatedByName = userCredentials.displayName;
                    dashboardRepository.UpdateLayoutNotes(layoutNotes);

                    UsedLanguages UsedLan = new UsedLanguages();
                    UsedLan.CustomerRefId = customerRefId;
                    UsedLan.LayoutRefId = LayoutDetails.LayoutId;
                    UsedLan.LanguageRefId = LanguageId;
                    UsedLan.IsDefault = 1;
                    UsedLan.IsModification = (int)EnumCopyColorCode.Green;
                    UsedLan.IsActive = true;
                    UsedLan.CreatedBy = userCredentials.id.ToString();
                    UsedLan.CreatedOn = time;
                    CustomerLogicRepository.SaveUsedLanguage(UsedLan);

                    return Json(LayoutDetails.LayoutId);
                }
                catch
                {
                    return Json(new { error = GetStatusMessage(StatusCode.E02) });
                }
            }
            else
                return Json(new
                {
                    error = GetStatusMessage(StatusCode.E01)
                });

        }
        ///<summary>
        /// Customer Overview page to Accepting new layout name and main language select by user in language dropdown in modal popup Methods end here
        ///</summary>

        #endregion

        ///<summary>
        /// Customer Overview Page Methods End here
        ///</summary>
    }
}
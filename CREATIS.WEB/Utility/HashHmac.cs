﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace CREATIS.WEB
{
    public class HashHmac
    {
        private static readonly Encoding encoding = Encoding.UTF8;

        public static string GetHashHamac(string message, string password)
        {
            //secret = secret ?? "";
            //var encoding = new System.Text.ASCIIEncoding();
            //byte[] keyByte = encoding.GetBytes(secret);
            //byte[] messageBytes = encoding.GetBytes(message);
            //using (var hmacsha256 = new HMACSHA256(keyByte))
            //{
            //    byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
            //    return Convert.ToBase64String(hashmessage);
            //}

            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(password);
            byte[] messageBytes = encoding.GetBytes(message);
            byte[] hashmessage;
            System.Security.Cryptography.HMACSHA256 hmacsha256 = new System.Security.Cryptography.HMACSHA256(keyByte);
            hashmessage = hmacsha256.ComputeHash(messageBytes);
            return Convert.ToBase64String(hashmessage);
        }
    }
}

